package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ca.medavie.nspp.audit.service.OutlierCriteriaService;
import ca.medavie.nspp.audit.service.data.DssMsiConstant;
import ca.medavie.nspp.audit.service.data.OutlierCriteria;
import ca.medavie.nspp.audit.service.data.PractitionerProfilePeriod;
import ca.medavie.nspp.audit.service.exception.OutlierCriteriaServiceException;
import ca.medavie.nspp.audit.service.repository.OutlierCriteriaRepository;
import ca.medavie.nspp.audit.service.repository.OutlierCriteriaRepositoryImpl;

/**
 * Session Bean implementation class OutlierCriteriaServiceImpl
 */
@Stateless
@Local(OutlierCriteriaService.class)
@LocalBean
public class OutlierCriteriaServiceImpl implements OutlierCriteriaService {

	@PersistenceContext(name = "nspp-audit-ejb")
	private EntityManager em;

	private OutlierCriteriaRepository outlierCriteriaRepository;

	@PostConstruct
	public void doInitRepo() {
		outlierCriteriaRepository = new OutlierCriteriaRepositoryImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.ejbclient.criteria.service.OutlierCriteriaLocalService#getOutlierCriterias()
	 */
	@Override
	public List<OutlierCriteria> getOutlierCriterias() throws OutlierCriteriaServiceException {
		try {
			return outlierCriteriaRepository.getOutlierCriterias();
		} catch (Exception e) {
			throw new OutlierCriteriaServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.ejbclient.service.OutlierCriteriaLocalService#getPractitionerPeriods()
	 */
	@Override
	public List<PractitionerProfilePeriod> getPractitionerPeriods() throws OutlierCriteriaServiceException {
		try {
			return outlierCriteriaRepository.getPractitionerPeriods();
		} catch (Exception e) {
			throw new OutlierCriteriaServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.ejbclient.service.OutlierCriteriaLocalService#addNewPracitionerProfilePeriod(ca.medavie
	 * .nspp.audit.business.criteria.PractitionerProfilePeriod)
	 */
	@Override
	public PractitionerProfilePeriod addNewPracitionerProfilePeriod(PractitionerProfilePeriod practitionerPeriod)
			throws OutlierCriteriaServiceException {
		try {
			return outlierCriteriaRepository.addNewPracitionerProfilePeriod(practitionerPeriod);
		} catch (Exception e) {
			throw new OutlierCriteriaServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.ejbclient.service.OutlierCriteriaLocalService#getPractitionerProfileCriteria(java.lang.
	 * String)
	 */
	@Override
	public List<DssMsiConstant> getPractitionerProfileCriteria() throws OutlierCriteriaServiceException {
		try {
			return outlierCriteriaRepository.getPractitionerProfileCriteria();
		} catch (Exception e) {
			throw new OutlierCriteriaServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.ejbclient.service.OutlierCriteriaLocalService#savePractitionerProfileCriteria(ca.medavie
	 * .nspp.audit.business.criteria.PractitionerProfileCriteria)
	 */
	@Override
	public void savePractitionerProfileCriteria(DssMsiConstant aProfileCriteriaConstant)
			throws OutlierCriteriaServiceException {
		try {
			outlierCriteriaRepository.savePractitionerProfileCriteria(aProfileCriteriaConstant);
		} catch (Exception e) {
			throw new OutlierCriteriaServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.ejbclient.criteria.service.OutlierCriteriaLocalService#saveOutlierCriteria(ca.medavie.nspp
	 * .audit.ejbclient.criteria.OutlierCriteria)
	 */
	@Override
	public void saveOutlierCriteria(OutlierCriteria anOutlierCriteria) throws OutlierCriteriaServiceException {
		try {
			outlierCriteriaRepository.saveOutlierCriteria(anOutlierCriteria);
		} catch (Exception e) {
			throw new OutlierCriteriaServiceException(e.getMessage(), e);
		}
	}

	/**
	 * @return the anOutlierCriteriaRepository
	 */
	public OutlierCriteriaRepository getOutlierCriteriaRepository() throws OutlierCriteriaServiceException {
		try {
			return outlierCriteriaRepository;
		} catch (Exception e) {
			throw new OutlierCriteriaServiceException(e.getMessage(), e);
		}
	}

	/**
	 * @param outlierCriteriaRepository
	 *            the outlierCriteriaRepository to set
	 */
	public void setOutlierCriteriaRepository(OutlierCriteriaRepository anOutlierCriteriaRepository)
			throws OutlierCriteriaServiceException {
		try {
			outlierCriteriaRepository = anOutlierCriteriaRepository;
		} catch (Exception e) {
			throw new OutlierCriteriaServiceException(e.getMessage(), e);
		}
	}
}

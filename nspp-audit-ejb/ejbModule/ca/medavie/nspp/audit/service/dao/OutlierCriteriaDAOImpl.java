package ca.medavie.nspp.audit.service.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import ca.medavie.nspp.audit.service.data.DssMsiConstant;
import ca.medavie.nspp.audit.service.data.DssProfilePeriod;

/**
 * Implementation of the OutlierCriteria DAO layer
 */
public class OutlierCriteriaDAOImpl implements OutlierCriteriaDAO {

	/** Handles all DB transactions */
	public EntityManager entityManager;

	/**
	 * Constructor for OutlierCriteria with EntityManager provided
	 * 
	 * @param em
	 */
	public OutlierCriteriaDAOImpl(EntityManager em) {
		entityManager = em;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.OutlierCriteriaDAO#getOutLierCriteriaBO(javax.persistence.EntityManager)
	 */
	@Override
	public List<DssMsiConstant> getDssMsiConstants(String aSubsystem) {
		Query query = entityManager
				.createQuery("select dmc from DssMsiConstant dmc where dmc.id.constantCode <> 'OPERIOD'  and dmc.id.subsystem = :subsys");
		query.setParameter("subsys", aSubsystem);

		// Execute the Query
		@SuppressWarnings("unchecked")
		List<DssMsiConstant> result = query.getResultList();
		// Detach objects from EM before passing up the chain. Removes the auto save affect
		for (DssMsiConstant dmc : result) {
			entityManager.detach(dmc);
		}
		return new ArrayList<DssMsiConstant>(result);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.OutlierCriteriaDAO#getDssMsiConstants(java.lang.String, java.lang.String)
	 */
	@Override
	public List<DssMsiConstant> getDssMsiConstants(String aSubsystem, String aConstantCode) {
		Query query = entityManager
				.createQuery("select dmc from DssMsiConstant dmc where dmc.id.subsystem = :subsystem and dmc.id.constantCode = :constantCode");
		query.setParameter("subsystem", aSubsystem);
		query.setParameter("constantCode", aConstantCode);

		// Execute query
		@SuppressWarnings("unchecked")
		List<DssMsiConstant> result = query.getResultList();

		// Detatch records to prevent auto-save
		for (DssMsiConstant dmc : result) {
			entityManager.detach(dmc);
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.OutlierCriteriaDAO#saveDssMsiConstants(java.util.List)
	 */
	public void saveDssMsiConstants(List<DssMsiConstant> dssMsiContants) {
		for (DssMsiConstant dmc : dssMsiContants) {
			entityManager.merge(dmc);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.OutlierCriteriaDAO#saveDssMsiConstant(ca.medavie.nspp.audit.service.data.
	 * DssMsiConstant)
	 */
	public void saveDssMsiConstant(DssMsiConstant dssMsiConstant) {
		entityManager.merge(dssMsiConstant);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.OutlierCriteriaDAO#getDssProfilePeriods(javax.persistence.EntityManager)
	 */
	@Override
	public List<DssProfilePeriod> getDssProfilePeriods() {

		Query query = entityManager.createQuery("select dpp from DssProfilePeriod dpp order by dpp.periodId desc");

		// Execute the Query
		@SuppressWarnings("unchecked")
		List<DssProfilePeriod> results = query.getResultList();
		// Detach objects from EM before passing up the chain. Removes the auto save affect
		for (DssProfilePeriod dpp : results) {
			entityManager.detach(dpp);
		}
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.OutlierCriteriaDAO#saveDssProfilePeriod(ca.medavie.nspp.audit.service.data.
	 * DssProfilePeriod)
	 */
	public DssProfilePeriod saveDssProfilePeriod(DssProfilePeriod aDssProfilePeriod) {
		entityManager.persist(aDssProfilePeriod);
		return aDssProfilePeriod;
	}

	/**
	 * @return the entityManager
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * @param EntityManager
	 *            the entityManager to set
	 */
	public void setEntityManager(EntityManager aEntityManager) {
		entityManager = aEntityManager;
	}
}

package ca.medavie.nspp.audit.service.dao;

import java.util.List;
import java.util.Set;

import ca.medavie.nspp.audit.service.data.DssClaimsHistoryTransferRequest;
import ca.medavie.nspp.audit.service.data.DssDentalClaim;
import ca.medavie.nspp.audit.service.data.DssHistoryConversion;
import ca.medavie.nspp.audit.service.data.DssIndividual2;
import ca.medavie.nspp.audit.service.data.DssMedMainframeClaim;
import ca.medavie.nspp.audit.service.data.DssMedicareSe;
import ca.medavie.nspp.audit.service.data.DssPharmMainframeClaim;
import ca.medavie.nspp.audit.service.data.DssPharmacareSe;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssAuditHistoryTransferMappedEntity;

public interface DssClaimsHistoryTransferDAO {
	/**
	 * Attempts to load the matching DssIndividual2 record using the user specified search criteria
	 * 
	 * @param aHealthCardNumber
	 * @return matching DssIndividual2
	 */
	public DssIndividual2 getSearchedHealthCard(String aHealthCardNumber);

	/**
	 * @param aTransferRequest
	 * @return list of DssMedicareSe records found under the specified HCN and dates if provided in initial search
	 */
	public List<DssMedicareSe> getMedicareSeClaims(DssClaimsHistoryTransferRequest aTransferRequest);

	/**
	 * @param aTransferRequest
	 * @return list of DssMedMainframeClaim records found under the specified HCN and dates if provided in initial
	 *         search
	 */
	public List<DssMedMainframeClaim> getMedicareMainframeClaims(DssClaimsHistoryTransferRequest aTransferRequest);

	/**
	 * @param aTransferRequest
	 * @return list of DssPharmacareSe records found under the specified HCN and dates if provided in initial search
	 */
	public List<DssPharmacareSe> getPharmacareSeClaims(DssClaimsHistoryTransferRequest aTransferRequest);

	/**
	 * @param aTransferRequest
	 * @return list of DssPharmMainframeClaim records found under the specified HCN and dates if provided in initial
	 *         search
	 */
	public List<DssPharmMainframeClaim> getPharmacareMainframeClaims(DssClaimsHistoryTransferRequest aTransferRequest);

	/**
	 * @param aTransferRequest
	 * @return list of DssDentalClaim records found under the specified HCN and dates if provided in initial search
	 */
	public List<DssDentalClaim> getDentalClaims(DssClaimsHistoryTransferRequest aTransferRequest);

	/**
	 * Saves the provided DssHistoryConversion records to DSS_HISTORY_CONVERSION table. This is the staging table for
	 * Transferring claims from HCN to another.
	 * 
	 * @param aListOfConversions
	 *            - conversion to save
	 */
	public void saveClaimsHistoryTransferStagingData(Set<DssHistoryConversion> aListOfConversions);

	/**
	 * Triggers the stored procedure 'P_EXE_DSS_HISTORY_TRANSFER' to copy the claims data
	 * 
	 * @param aSourceHcn
	 * @param aTargetHcn
	 */
	public void executeClaimsHistoryTransferProcedure(String aSourceHcn, String aTargetHcn);

	/**
	 * Loads all transfer data for the specified source and target Health Card Numbers
	 * 
	 * @param aSourceHcn
	 * @param aTargetHcn
	 * @return list of transfer data from DSS_AUDIT_HISTORY_TRANSFER
	 */
	public List<DssAuditHistoryTransferMappedEntity> getDssAuditHistoryTransfers(String aSourceHcn, String aTargetHcn);
}

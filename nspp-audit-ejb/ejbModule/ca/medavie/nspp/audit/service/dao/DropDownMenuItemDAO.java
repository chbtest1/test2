package ca.medavie.nspp.audit.service.dao;

import java.util.Date;
import java.util.List;

import ca.medavie.nspp.audit.service.data.DssCodeTableAlphaEntry;
import ca.medavie.nspp.audit.service.data.DssCodeTableEntry;
import ca.medavie.nspp.audit.service.data.DssHealthServiceGroup;
import ca.medavie.nspp.audit.service.data.DssProfilePeriod;
import ca.medavie.nspp.audit.service.data.DssProviderType;

public interface DropDownMenuItemDAO {

	/**
	 * Get Dss_provider_Types Data objects, providing data for subcategory types drop down menu
	 * 
	 * @return a list of SubCategoryDataEntries Object
	 */
	public List<DssProviderType> getSubCategoryDataEntries();

	/**
	 * Get DssCodeTableAlphaEntry Objects providing data for Specialties drop down menu
	 * 
	 * @return a list of SpecialtyDataEntries Objects
	 */
	public List<DssCodeTableAlphaEntry> getSpecialtyDataEntries();

	/**
	 * Get a list of DssCodeTableEntry Objects which type is RX
	 * 
	 * @return
	 */
	public List<DssCodeTableEntry> getDssAuditRXCodeEntries();

	/**
	 * Get a list of DssCodeTableEntry Objects which type is not RX
	 * 
	 * @return
	 */
	public List<DssCodeTableEntry> getDssAuditNonRXCodeEntries();

	/**
	 * Get a list of ContractTown names
	 * 
	 * @return list of ContractTown names
	 */
	public List<String> getContractTownNames();

	/**
	 * Get a list of Qualifier codes
	 * 
	 * @return a list of Qualifier codes
	 */
	public List<String> getQualifierCodes();

	/**
	 * Get a list of AuditSourceMedCodes
	 * 
	 * @return a List of AuditSourceMedCodes
	 */
	public List<String> geAuditSourceMedCodes();

	/**
	 * Get a list of AuditSourcePharmCodes
	 * 
	 * @return a list of AuditSourcePharmCodes
	 */
	public List<String> geAuditSourcePharmCodes();

	/**
	 * Get a list of AuditPharmTypeCodes
	 * 
	 * @return a list of AuditPharmTypeCodes
	 */
	public List<String> geAuditPharmTypeCodes();

	/**
	 * Get a list of Group Types
	 * 
	 * @return
	 */
	public List<String> getGroupTypeCodes();

	/**
	 * Get a list of ProfilePeriod YearEnd dates
	 * 
	 * @return a list of ProfilePeriod YearEnd dates
	 */
	public List<Date> getPeriodYearEndDates();

	/**
	 * Get list of PeriodIds
	 * 
	 * @return list of DssProfilePeriod
	 */
	public List<DssProfilePeriod> getPeriodIdDropdown();

	/**
	 * @return list of dss unit value level code
	 */
	public List<Long> getDssUnitValueLevelCodes();

	/**
	 * @return list of dss unit value descriptions
	 */
	public List<DssCodeTableEntry> getDssUnitValueDescsCodes();

	/**
	 * @return List of data objects contains service modifiers
	 */
	public List<Object[]> getHsModifers();

	/**
	 * @return implicit modifiers objects from database
	 */
	public List<Object[]> getHsImplicitModifiers();

	/**
	 * @return programs from the database
	 */
	public List<Object[]> getHsProgramCodes();

	/**
	 * @return HealthService groups from the database
	 */
	public List<DssHealthServiceGroup> getHsGroupCodes();

	/**
	 * @return HealthService qualifiers from the database
	 */
	public List<String> getHsQualifiersCodes();

	/**
	 * @return the list of data for denticare audit type
	 */
	public List<DssCodeTableAlphaEntry> getDenticareAuditTypeCodes();

	/**
	 * @return the list of data for medicare audit type
	 */
	public List<DssCodeTableAlphaEntry> getMedicareAuditTypeCodes();

	/**
	 * @return the list of data for pharmacare audit type
	 */
	public List<DssCodeTableAlphaEntry> getPharmacareAuditTypeDropDown();

	/**
	 * @return list of responds status code data
	 */
	public List<DssCodeTableAlphaEntry> getResponseStatusCodes();

	/**
	 * Get PayeeTypes
	 * 
	 * @return payeeTypes
	 */
	public List<DssCodeTableAlphaEntry> getPayeeTypeDropDown();

	/**
	 * Get Payment Responsibilities
	 * 
	 * @return Payment Responsibilities
	 */
	public List<DssCodeTableAlphaEntry> getPaymentResponsibilityDropDown();

	/**
	 * @return GL Numbers from the database
	 */
	public List<String> getGlNumbers();

	/**
	 * @return Audit Criteria types used for Medicare and Denticare
	 */
	public List<DssCodeTableAlphaEntry> getGeneralAuditCriteriaTypeDropDown();

	/**
	 * @return Audit Criteria Types used for Pharmacare
	 */
	public List<DssCodeTableAlphaEntry> getPharmacareAuditCriteriaTypeDropDown();

	/**
	 * @return Unique list of effective from date from dss health service table
	 */
	public List<Date> getUniqueHSEffectiveDates();
}

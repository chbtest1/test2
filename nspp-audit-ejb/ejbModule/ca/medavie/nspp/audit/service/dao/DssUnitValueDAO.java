package ca.medavie.nspp.audit.service.dao;

import java.util.List;

import ca.medavie.nspp.audit.service.data.DssUnitValue;
import ca.medavie.nspp.audit.service.data.DssUnitValueBO;

public interface DssUnitValueDAO {

	/**
	 * @return List of DssUnitValue
	 */
	public List<DssUnitValue> getDssUnitValues();

	/**
	 * @param unitValueID
	 * @return
	 */
	public DssUnitValue findDssUnitValueById(Long unitValueID);

	/**
	 * @param aDssUnitValue
	 * @return DssUnitValue
	 */
	public DssUnitValue saveOrUPdateDssUnitValue(DssUnitValue aDssUnitValue);

	/**
	 * Deletes the specified Dss Unit Values from the system.
	 * 
	 * @param aListOfDssUnitValuesForDelete
	 */
	public void deleteDssUnitValues(List<DssUnitValue> aListOfDssUnitValuesForDelete);

	/**
	 * Checks if a given dss unit value overlaps any others.
	 * 
	 * @param aDssUnitValue
	 * @return
	 */
	public Boolean overlaps(DssUnitValueBO aDssUnitValue);
}

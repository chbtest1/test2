package ca.medavie.nspp.audit.service.dao;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.data.AuditCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.DataUtilities;
import ca.medavie.nspp.audit.service.data.DenticareAuditLetterResponseSearchCriteria;
import ca.medavie.nspp.audit.service.data.DssDenAuditCriteria;
import ca.medavie.nspp.audit.service.data.DssDenClaimAudit;
import ca.medavie.nspp.audit.service.data.DssDenClaimAuditPK;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssDenAuditCriteriaMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssDenticareALRMappedEntity;

public class DenticareDAOImpl implements DenticareDAO {

	/** Class logger */
	private static final Logger logger = LoggerFactory.getLogger(DenticareDAOImpl.class);

	/** Handles all DB connections and transactions */
	public EntityManager entityManager;

	public DenticareDAOImpl(EntityManager em) {

		entityManager = em;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DenticareDAO#getDenticareALRRecords(ca.medavie.nspp.audit.service.data.
	 * DenticareAuditLetterResponseSearchCriteria, int, int)
	 */
	@Override
	public List<DssDenticareALRMappedEntity> getDenticareALRRecords(
			DenticareAuditLetterResponseSearchCriteria denticareALRSearchCriteria, int startRow, int maxRows) {
		logger.debug("DenticareDAO getDenticareALRRecords(): Begin");

		StringBuilder queryString = new StringBuilder(
				"SELECT ddca.claim_num, ddca.item_code, ddca.audit_run_number, ddca.provider_number, ddca.provider_type, ddca.health_card_number, ddca.claim_selection_date, ddca.audit_letter_creation_date, "
						+ "ddca.fee_code_description, ddca.response_status_code, ddca.number_of_times_sent, ddca.chargeback_amount, ddca.number_of_claims_affected, ddca.modified_by, ddca.last_modified, "
						+ "cc.code_table_alpha_entry_desc as audit_type, ' ' as client_name, ddca.audit_note, DECODE(dp2.provider_type, 'PH', 'DR', 'OP', 'DR', 'DE', 'DR', NULL) || '. ' || "
						+ "substr(dp2.first_name, 1, 1) || '. ' || dp2.last_name as provider_name, ddc.service_date, ddc.paid_amount, di2.surname || ', ' || di2.first_name as individual_name ");

		// Append FROM/WHERE sql
		queryString.append(constructALRSqlStatement(denticareALRSearchCriteria));

		// Append order by clause
		if (denticareALRSearchCriteria.isSortingSet()) {

			// Append required ORDER BY SQL
			DataUtilities.appendSQLOrderByStatements(queryString, denticareALRSearchCriteria);
		} else {
			// Use the default sort
			queryString
					.append("ORDER BY CASE WHEN ddca.audit_letter_creation_date IS NULL THEN 1 ELSE 0 END, ddca.audit_letter_creation_date DESC");
		}

		// Apply required argument values
		Query query = entityManager.createNativeQuery(queryString.toString(), "DssDenticareALRQueryMapping");

		// Set any parameters
		setALRSearchQueryParameters(query, denticareALRSearchCriteria);

		// Apply pagination setting
		query.setFirstResult(startRow);
		query.setMaxResults(maxRows);

		@SuppressWarnings("unchecked")
		List<DssDenticareALRMappedEntity> results = query.getResultList();

		logger.debug("DenticareDAO getDenticareALRRecords(): End");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.DenticareDAO#getSearchedDenticareALRCount(ca.medavie.nspp.audit.service.data
	 * .DenticareAuditLetterResponseSearchCriteria)
	 */
	@Override
	public Integer getSearchedDenticareALRCount(DenticareAuditLetterResponseSearchCriteria denticareALRSearchCriteria) {

		StringBuilder queryString = new StringBuilder("SELECT COUNT(1) ");

		// Append common portion of SQL along with parameter AND clauses
		queryString.append(constructALRSqlStatement(denticareALRSearchCriteria));

		// Create query and set any required arguments
		Query query = entityManager.createNativeQuery(queryString.toString());

		// Append required arguments
		setALRSearchQueryParameters(query, denticareALRSearchCriteria);

		// Load count result and convert to Integer (Null safe)
		BigDecimal count = (BigDecimal) query.getSingleResult();
		Integer result = new Integer(0);
		if (count != null) {
			result = count.intValue();
		}
		return result;
	}

	/**
	 * Utility method to generate the common FROM/WHERE portion of the ALR search SQL
	 * 
	 * @param aSearchCriteria
	 * @return construct SQL based on arguments and FROM clause
	 */
	private StringBuilder constructALRSqlStatement(DenticareAuditLetterResponseSearchCriteria aSearchCriteria) {

		StringBuilder queryString = new StringBuilder(
				"FROM dss_complete_audit dca, dss_den_claim_audit ddca, dss_provider_2 dp2, dss_dental_claims ddc, dss_individual_2 di2, dss_code_table_alpha_entry cc "
						+ "WHERE (ddca.provider_number = dp2.provider_number) and (ddca.audit_run_number = dca.audit_run_number) and (ddca.provider_type = dp2.provider_type) "
						+ "and ddc.claim_number = ddca.claim_num and ddc.item_code = ddca.item_code and di2.health_card_number = ddca.health_card_number and cc.code_table_alpha_number = 125 "
						+ "and cc.code_table_alpha_entry = dca.audit_type ");

		if (aSearchCriteria.isProviderNumberSet()) {
			queryString.append("AND ddca.provider_number =?1 ");
		}
		if (aSearchCriteria.isProviderNameSet()) {
			queryString.append("AND (UPPER(dp2.organization_name) LIKE ?7 OR UPPER(dp2.last_name) LIKE ?7 ");
			queryString.append("OR UPPER(dp2.first_name) LIKE ?7 OR UPPER(dp2.middle_name) LIKE ?7 ");

			// parse string into individual words
			String[] arr = aSearchCriteria.getPractitionerName().split(" ");

			// The clause comes out like this for multiple names entered
			if (arr.length < 2) {
				// end the statement if there aren't multiple names
				queryString.append(") ");
			} else {
				queryString.append("or (");
				// add condition for each word
				for (int i = 0; i < arr.length; i++) {
					if (i > 0) {
						queryString.append(" and ");
					}

					queryString.append("(upper(dp2.last_name) LIKE ?7" + i + " or upper (dp2.middle_name) LIKE ?7" + i
							+ " or upper (dp2.first_name) LIKE ?7" + i + ") ");
				}

				queryString.append("))");
			}
		}

		if (aSearchCriteria.isHCNClientIDSet()) {
			queryString.append("AND (UPPER (ddca.health_card_number) LIKE CONCAT('%', CONCAT(?3,'%'))) ");
		}
		if (aSearchCriteria.isAuditTypeSet()) {
			queryString.append("AND (UPPER (dca.audit_type) LIKE CONCAT('%', CONCAT(?4,'%'))) ");

		}
		if (aSearchCriteria.isResponseStatusSet()) {
			queryString.append("AND (UPPER (ddca.response_status_code) LIKE CONCAT('%', CONCAT(?5,'%'))) ");
		}

		if (aSearchCriteria.isAuditFromDateSet()) {
			queryString.append("AND ddca.audit_letter_creation_date >= to_date(?6,'yyyy-mm-dd') ");
		}

		if (aSearchCriteria.isAuditToDateSet()) {
			queryString.append("AND ddca.audit_letter_creation_date <= to_date(?7,'yyyy-mm-dd') ");
		}

		if (aSearchCriteria.isAuditRunNumberSet()) {
			queryString.append("AND ddca.audit_run_number=?8 ");
		}

		return queryString;
	}

	/**
	 * Applies query arguments for Audit Criteria search/count
	 * 
	 * @param aQuery
	 * @param aSearchCriteria
	 */
	private void setALRSearchQueryParameters(Query aQuery, DenticareAuditLetterResponseSearchCriteria aSearchCriteria) {

		// Formatter used to setup date parameters
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");

		if (aSearchCriteria.isProviderNumberSet()) {

			aQuery.setParameter(1, aSearchCriteria.getPractitionerNumber());
		}

		if (aSearchCriteria.isProviderNameSet()) {

			aQuery.setParameter(7, "%" + aSearchCriteria.getPractitionerName().replaceAll("\\s+", "").toUpperCase()
					+ "%");

			// parse string into individual words
			String[] arr = aSearchCriteria.getPractitionerName().split(" ");

			// add in individual word arguments if there are more then one
			if (arr.length > 1) {
				for (int i = 0; i < arr.length; i++) {
					int position = new Integer("7" + i);
					// remove any commas
					aQuery.setParameter(position, "%" + arr[i].trim().toUpperCase().replaceAll(",", "") + "%");
				}
			}
		}

		if (aSearchCriteria.isHCNClientIDSet()) {

			aQuery.setParameter(3, aSearchCriteria.getHcnClientID().replaceAll("\\s+", "").toUpperCase());
		}

		if (aSearchCriteria.isAuditTypeSet()) {

			aQuery.setParameter(4, aSearchCriteria.getAuditType().replaceAll("\\s+", "").toUpperCase());
		}

		if (aSearchCriteria.isResponseStatusSet()) {

			aQuery.setParameter(5, aSearchCriteria.getResponseStatus().replaceAll("\\s+", "").toUpperCase());
		}

		if (aSearchCriteria.isAuditFromDateSet()) {

			aQuery.setParameter(6, dateFormater.format(aSearchCriteria.getAuditFromDate()));
		}

		if (aSearchCriteria.isAuditToDateSet()) {

			aQuery.setParameter(7, dateFormater.format(aSearchCriteria.getAuditToDate()));
		}

		if (aSearchCriteria.isAuditRunNumberSet()) {

			aQuery.setParameter(8, aSearchCriteria.getAuditRunNumber());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DenticareDAO#getDenticareALRByID(ca.medavie.nspp.audit.service.data.
	 * DssDenClaimAuditPK)
	 */
	@Override
	public DssDenClaimAudit getDenticareALRByID(DssDenClaimAuditPK aPK) {
		logger.debug("DenticareDAO getDenticareALRByID() : Begin");

		DssDenClaimAudit aDssDenClaimAudit = entityManager.find(DssDenClaimAudit.class, aPK);

		logger.debug("DenticareDAO getDenticareALRByID() : End");
		return aDssDenClaimAudit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.DenticareDAO#saveDenticareALR(ca.medavie.nspp.audit.service.data.DssDenClaimAudit
	 * )
	 */
	@Override
	public void saveDenticareALR(DssDenClaimAudit aDssDenClaimAudit) {
		logger.debug("DenticareDAO saveDenticareALR(): Begin");
		entityManager.merge(aDssDenClaimAudit);
		logger.debug("DenticareDAO saveDenticareALR(): End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.DenticareDAO#getSearchPractitionerAuditCriterias(ca.medavie.nspp.audit.service
	 * .data.AuditCriteriaSearchCriteria, int, int)
	 */
	@Override
	public List<DssDenAuditCriteriaMappedEntity> getSearchPractitionerAuditCriterias(
			AuditCriteriaSearchCriteria aSearchCriteria, int startRow, int maxRows) {
		logger.debug("DenticareDAO getSearchPractitionerAuditCriterias() : Begin");

		// Create the query String
		StringBuilder queryString = new StringBuilder(
				"SELECT ddac.audit_criteria_id, ddac.audit_type, ddac.audit_from_date, ddac.audit_to_date, ddac.from_payment_date,"
						+ "ddac.to_payment_date, ddac.provider_number, ddac.provider_type, "
						+ "CASE WHEN dp2.organization_name IS NULL THEN dp2.last_name || ', ' || dp2.first_name || ' ' || substr(dp2.middle_name, 1, 1) ELSE dp2.organization_name END as provider_name ");

		// Append common portion of SQL along with parameter AND clauses
		queryString.append(constructAuditCriteriaSqlStatement(aSearchCriteria));

		// Append any sorting criteria..
		if (aSearchCriteria.isSortingSet()) {

			// Append required ORDER BY SQL
			DataUtilities.appendSQLOrderByStatements(queryString, aSearchCriteria);
		}

		// Create query, set required parameters and and execute
		Query query = entityManager.createNativeQuery(queryString.toString(), "DssDenAuditCriteriaNativeQueryMapping");

		// Apply pagination setting
		query.setFirstResult(startRow);
		query.setMaxResults(maxRows);

		// Append required arguments
		setAuditCriteriaSearchQueryParameters(query, aSearchCriteria);

		// Execute
		@SuppressWarnings("unchecked")
		List<DssDenAuditCriteriaMappedEntity> result = query.getResultList();

		logger.debug("DenticareDAO getSearchPractitionerAuditCriterias() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.DenticareDAO#getSearchedAuditCriteriaCount(ca.medavie.nspp.audit.service.data
	 * .AuditCriteriaSearchCriteria)
	 */
	@Override
	public Integer getSearchedAuditCriteriaCount(AuditCriteriaSearchCriteria anAuditCriteriaSearchCriteria) {

		StringBuilder queryString = new StringBuilder("SELECT COUNT(1) ");

		// Append common portion of SQL along with parameter AND clauses
		queryString.append(constructAuditCriteriaSqlStatement(anAuditCriteriaSearchCriteria));

		// Create query and set any required arguments
		Query query = entityManager.createNativeQuery(queryString.toString());

		// Append required arguments
		setAuditCriteriaSearchQueryParameters(query, anAuditCriteriaSearchCriteria);

		// Load count result and convert to Integer (Null safe)
		BigDecimal count = (BigDecimal) query.getSingleResult();
		Integer result = new Integer(0);
		if (count != null) {
			result = count.intValue();
		}
		return result;
	}

	/**
	 * Utility method to generate the common FROM/WHERE portion of the Audit Criteria search SQL
	 * 
	 * @param aSearchCriteria
	 * @return construct SQL based on arguments and FROM clause
	 */
	private StringBuilder constructAuditCriteriaSqlStatement(AuditCriteriaSearchCriteria aSearchCriteria) {

		StringBuilder queryString = new StringBuilder("FROM dss_den_audit_criteria ddac, dss_provider_2 dp2 WHERE "
				+ "ddac.provider_number = dp2.provider_number and ddac.provider_type = dp2.provider_type ");

		// Append additional AND statements if parameters are set in the search criteria.
		if (aSearchCriteria.isAuditTypeSet()) {
			queryString.append("and ddac.audit_type = ?1 ");
		}
		if (aSearchCriteria.isAuditCriteriaIdSet()) {
			queryString.append("and ddac.audit_criteria_id = ?2 ");
		}
		if (aSearchCriteria.isPractitionerIdSet()) {
			queryString.append("and ddac.provider_number = ?3 ");
		}
		if (aSearchCriteria.isSubcategorySet()) {
			queryString.append("and ddac.provider_type = ?4 ");
		}
		if (aSearchCriteria.isPractitionerNameSet()) {
			queryString.append("and (UPPER(dp2.first_name) LIKE ?5 or UPPER(dp2.middle_name) LIKE ?5 "
					+ "or UPPER(dp2.last_name) LIKE ?5 or UPPER(dp2.organization_name) LIKE ?5");

			// parse string into individual words
			String[] arr = aSearchCriteria.getPractitionerName().split(" ");

			/*
			 * The clause comes out like this for multiple names entered (3 names in this example):
			 * 
			 * and (x.organization_name LIKE &5 OR x.last_name LIKE &5 OR x.first_name LIKE &5 OR ((x.last_name LIKE &50
			 * or x.first_name LIKE &50) and (x.last_name LIKE &51 or x.first_name LIKE &51) and (x.last_name LIKE &52
			 * or x.first_name LIKE &52)))
			 */
			if (arr.length < 2) {
				// end the statement if there aren't multiple names
				queryString.append(") ");
			} else {
				queryString.append("or (");
				// add condition for each word
				for (int i = 0; i < arr.length; i++) {
					if (i > 0) {
						queryString.append(" and ");
					}

					queryString.append("(upper(dp2.last_name) LIKE ?5" + i + " or upper(dp2.middle_name) LIKE ?5" + i
							+ " or upper(dp2.first_name) LIKE ?5" + i + ") ");
				}

				queryString.append("))");
			}
		}
		return queryString;
	}

	/**
	 * Applies query arguments for Audit Criteria search/count
	 * 
	 * @param aQuery
	 * @param aSearchCriteria
	 */
	private void setAuditCriteriaSearchQueryParameters(Query aQuery, AuditCriteriaSearchCriteria aSearchCriteria) {

		if (aSearchCriteria.isAuditTypeSet()) {
			aQuery.setParameter(1, aSearchCriteria.getAuditType());
		}
		if (aSearchCriteria.isAuditCriteriaIdSet()) {
			aQuery.setParameter(2, aSearchCriteria.getAuditCriteriaId());
		}
		if (aSearchCriteria.isPractitionerIdSet()) {
			aQuery.setParameter(3, aSearchCriteria.getPractitionerId());
		}
		if (aSearchCriteria.isSubcategorySet()) {
			aQuery.setParameter(4, aSearchCriteria.getSubcategory());
		}
		if (aSearchCriteria.isPractitionerNameSet()) {
			aQuery.setParameter(5, "%" + aSearchCriteria.getPractitionerName().toUpperCase() + "%");

			// parse string into individual words
			String[] arr = aSearchCriteria.getPractitionerName().split(" ");

			// add in individual word arguments if there are more then one
			if (arr.length > 1) {
				for (int i = 0; i < arr.length; i++) {
					int position = new Integer("5" + i);

					aQuery.setParameter(position, "%" + arr[i].trim().toUpperCase().replaceAll(",", "") + "%");
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DenticareDAO#getDssDenAuditCriteriaByPk(java.lang.Long)
	 */
	@Override
	public DssDenAuditCriteria getDssDenAuditCriteriaByPk(Long aPk) {
		logger.debug("DenticareDAO getDssMedAudCriteriaByPk() : Begin");

		DssDenAuditCriteria audCriteria = entityManager.find(DssDenAuditCriteria.class, aPk);

		logger.debug("DenticareDAO getDssMedAudCriteriaByPk() : End");
		return audCriteria;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DenticareDAO#getDefaultAuditCriteriaByType(java.lang.String)
	 */
	@Override
	public DssDenAuditCriteria getDefaultAuditCriteriaByType(String anAuditType) {
		logger.debug("DenticareDAO getDefaultAuditCriteriaByType() : Begin");

		Query query = entityManager
				.createQuery("select ddac from DssDenAuditCriteria ddac where ddac.auditType = :auditType and "
						+ "ddac.providerNumber is null and ddac.providerType is null");

		// Apply parameter values
		query.setParameter("auditType", anAuditType);

		// Execute query
		@SuppressWarnings("unchecked")
		List<DssDenAuditCriteria> results = query.getResultList();

		// Return nothing if there are no results
		if (results.isEmpty()) {
			return null;
		}
		// Grab the result and return
		DssDenAuditCriteria result = results.get(0);

		logger.debug("DenticareDAO getDefaultAuditCriteriaByType() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DenticareDAO#getNextDenAudCriteriaSequenceValue()
	 */
	@Override
	public Long getNextDenAudCriteriaSequenceValue() {
		logger.debug("DenticareDAO getNextDenAudCriteriaSequenceValue() : Begin");

		// Simple select of next val from a sequence
		Query query = entityManager.createNativeQuery("select dss_den_audit_criteria_seq.nextval from dual");

		// Execute query and load result
		BigDecimal result = (BigDecimal) query.getSingleResult();

		logger.debug("DenticareDAO getNextDenAudCriteriaSequenceValue() : End");
		return result.longValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.DenticareDAO#saveDenticareAuditCriteria(ca.medavie.nspp.audit.service.data.
	 * DssDenAuditCriteria)
	 */
	@Override
	public void saveDenticareAuditCriteria(DssDenAuditCriteria anAuditCriteria) {
		logger.debug("DenticareDAO saveDenticareAuditCriteria() : Begin");

		// Submit changes to DB
		entityManager.merge(anAuditCriteria);

		logger.debug("DenticareDAO saveDenticareAuditCriteria() : End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DenticareDAO#getLastAuditDate(java.lang.Long)
	 */
	@Override
	public Date getLastAuditDate(Long anAuditCriteriaId) {

		logger.debug("DenticareDAO getLastAuditDate() : Begin");

		// Simple Date select query based on an AuditCriteriaId
		Query query = entityManager
				.createNativeQuery("select max(s.audit_letter_creation_date) from dss_den_claim_audit s "
						+ "where s.audit_run_number = (select m.last_audit_run_number from DSS_DEN_AUDIT_CRITERIA m "
						+ "where m.audit_criteria_id = ?1)");

		// Apply AuditCriteriaID argument
		query.setParameter(1, anAuditCriteriaId);
		// Execute query
		Date result = (Date) query.getSingleResult();

		logger.debug("DenticareDAO getLastAuditDate() : End");
		return result;
	}
}

package ca.medavie.nspp.audit.service.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import ca.medavie.nspp.audit.service.data.DssMsiConstant;
import ca.medavie.nspp.audit.service.data.QueryConstants;

public class DssConstantsDAOImpl implements DssConstantsDAO {
	public EntityManager entityManager;

	/**
	 * Constructor for DssConstants with EntityManager provided
	 * 
	 * @param em
	 */
	public DssConstantsDAOImpl(EntityManager em) {
		entityManager = em;
	}	

	/* (non-Javadoc)
	 * @see ca.medavie.nspp.audit.service.dao.DssConstantsDAO#getDssConstants()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<DssMsiConstant> getDssConstants() {
		// Query to execute to load results		
		Query query = entityManager.createNamedQuery(QueryConstants.DSS_MSI_CONSTANT_EDIT);
		return query.getResultList();
	}

	/* (non-Javadoc)
	 * @see ca.medavie.nspp.audit.service.dao.DssConstantsDAO#updateDssConstant(ca.medavie.nspp.audit.service.data.DssMsiConstant)
	 */
	@Override
	public void updateDssConstant(DssMsiConstant aDssMsiConstant) {		
		entityManager.merge(aDssMsiConstant);
	}
}

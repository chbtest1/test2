package ca.medavie.nspp.audit.service.dao;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.data.DataUtilities;
import ca.medavie.nspp.audit.service.data.DssManualDeposit;
import ca.medavie.nspp.audit.service.data.ManualDeposit;
import ca.medavie.nspp.audit.service.data.ManualDepositSearchCriteria;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.ProviderGroup;
import ca.medavie.nspp.audit.service.data.ProviderGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssBusinessArrangementMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssManualDepositsMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssProviderGroupMappedEntity;

/**
 * Implementation of the Inquiry DAO layer
 */
public class ManualDepositsDAOImpl implements ManualDepositsDAO {

	/** Handles all DB connections and transactions */
	private EntityManager entityManager;

	/** Class logger */
	private static final Logger LOGGER = LoggerFactory.getLogger(ManualDepositsDAOImpl.class);

	public ManualDepositsDAOImpl(EntityManager em) {
		entityManager = em;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.ManualDepositsDAO#getBusinessArrangements(ca.medavie.nspp.audit.service.data
	 * .Practitioner, ca.medavie.nspp.audit.service.data.ProviderGroup)
	 */
	@Override
	public List<DssBusinessArrangementMappedEntity> getBusinessArrangements(Practitioner practitioner,
			ProviderGroup group) {
		StringBuilder queryString = new StringBuilder(
				"SELECT dss_business_arrangement.bus_arr_number, to_char(dss_business_arrangement.effective_from_date,'DD-MON-YYYY')||' '|| "
						+ "to_char(dss_business_arrangement.effective_to_date,'DD-MON-YYYY')||' '||dss_code_table_entry.code_table_entry_description bus_arr_description "
						+ "FROM dss_business_arrangement, dss_code_table_entry WHERE ( dss_business_arrangement.remuneration_method = dss_code_table_entry.code_table_entry ) "
						+ "AND ((( dss_business_arrangement.provider_number = ?1 ) AND ( dss_business_arrangement.provider_type = ?2 )) OR ( dss_business_arrangement.provider_group_id = ?3 )) "
						+ "AND ( dss_code_table_entry.code_table_number = 115 ) ORDER BY dss_business_arrangement.effective_to_date DESC, dss_business_arrangement.effective_from_date DESC");

		// Query to execute to load results
		Query query = entityManager.createNativeQuery(queryString.toString(), "DssBusinessArrangementQueryMapping");

		if (practitioner != null) {
			query.setParameter(1, practitioner.getPractitionerNumber());
			query.setParameter(2, practitioner.getPractitionerType());
		} else {
			query.setParameter(1, "-1");
			query.setParameter(2, "-1");
		}
		if (group != null) {
			query.setParameter(3, group.getProviderGroupId());
		} else {
			query.setParameter(3, "-1");
		}

		// Execute the Query and get results
		@SuppressWarnings("unchecked")
		List<DssBusinessArrangementMappedEntity> result = query.getResultList();
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.ManualDepositsDAO#getBusinessArrangements(ca.medavie.nspp.audit.service.data
	 * .ManualDeposit)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<DssBusinessArrangementMappedEntity> getBusinessArrangements(ManualDeposit manualDeposit) {
		if (manualDeposit == null)
			return null;
		StringBuilder queryString = new StringBuilder(
				"SELECT dss_business_arrangement.bus_arr_number, to_char(dss_business_arrangement.effective_from_date,'DD-MON-YYYY')||' '|| "
						+ "to_char(dss_business_arrangement.effective_to_date,'DD-MON-YYYY')||' '||dss_code_table_entry.code_table_entry_description bus_arr_description "
						+ "FROM dss_business_arrangement, dss_code_table_entry WHERE ( dss_business_arrangement.remuneration_method = dss_code_table_entry.code_table_entry ) "
						+ "AND ((( dss_business_arrangement.provider_number = ?1 ) AND ( dss_business_arrangement.provider_type = ?2 )) OR ( dss_business_arrangement.provider_group_id = ?3 )) "
						+ "AND ( dss_code_table_entry.code_table_number = 115 ) ORDER BY dss_business_arrangement.effective_to_date DESC, dss_business_arrangement.effective_from_date DESC");

		// Query to execute to load results
		Query query = entityManager.createNativeQuery(queryString.toString(), "DssBusinessArrangementQueryMapping");

		if (manualDeposit.getGroupNumber() != null) {
			query.setParameter(3, manualDeposit.getGroupNumber());
			query.setParameter(1, "-1");
			query.setParameter(2, "-1");
		} else {
			query.setParameter(1, manualDeposit.getPractitionerNumber());
			query.setParameter(2, manualDeposit.getPractitionerType());
			query.setParameter(3, "-1");
		}
		// Execute the Query and get results
		return query.getResultList();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.ManualDepositsDAO#getSearchedManualDeposits(ca.medavie.nspp.audit.service.data
	 * .ManualDepositSearchCriteria, boolean, int, int)
	 */
	@Override
	public List<DssManualDepositsMappedEntity> getSearchedManualDeposits(
			ManualDepositSearchCriteria aManualDepositSearchCriteria, boolean aPractitionerSearch, int startRow,
			int maxRows) {

		// Build the base query
		StringBuilder queryString = new StringBuilder(
				"SELECT md.deposit_number, md.provider_number, md.provider_group_id, md.bus_arr_number, pg.provider_group_name, "
						+ "p.organization_name, p.last_name, p.first_name, p.middle_name, p.provider_type, p.organization_name, md.fiscal_year_end_date, md.gl_number, md.deposit_amount, "
						+ "md.deposit_note, md.date_received, md.last_modified, md.modified_by, cte.code_table_entry_description bus_arr_description ");

		// Append remainder of SQL
		queryString.append(constructManualDepositsSqlStatement(aManualDepositSearchCriteria, aPractitionerSearch));

		// Append the order by SQL
		if (aManualDepositSearchCriteria.isSortingSet()) {

			// Append required ORDER BY SQL
			DataUtilities.appendSQLOrderByStatements(queryString, aManualDepositSearchCriteria);
		} else {
			// Default order by
			queryString.append("ORDER BY p.provider_number ASC");
		}

		// Query to execute to load results
		Query query = entityManager.createNativeQuery(queryString.toString(), "DssManualDepositsBaseInfoQueryMapping");

		// Apply pagination parameters
		query.setFirstResult(startRow);
		query.setMaxResults(maxRows);

		// Apply query arguments
		setManualSearchQueryParameters(query, aManualDepositSearchCriteria);

		// Execute the Query and get results
		@SuppressWarnings("unchecked")
		List<DssManualDepositsMappedEntity> mds = query.getResultList();
		return mds;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.ManualDepositsDAO#getSearchedAuditCriteriaCount(ca.medavie.nspp.audit.service
	 * .data.ManualDepositSearchCriteria, boolean)
	 */
	@Override
	public Integer getSearchedAuditCriteriaCount(ManualDepositSearchCriteria aManualDepositSearchCriteria,
			boolean aPractitionerSearch) {

		StringBuilder queryString = new StringBuilder("SELECT COUNT(1) ");
		queryString.append(constructManualDepositsSqlStatement(aManualDepositSearchCriteria, aPractitionerSearch));

		// Create query and set any required arguments
		Query query = entityManager.createNativeQuery(queryString.toString());

		// Apply arguments
		setManualSearchQueryParameters(query, aManualDepositSearchCriteria);

		// Load count result and convert to Integer (Null safe)
		BigDecimal count = (BigDecimal) query.getSingleResult();
		Integer result = new Integer(0);
		if (count != null) {
			result = count.intValue();
		}
		return result;
	}

	/**
	 * Utility method to generate the common FROM/WHERE portion of the ManualDeposits search SQL
	 * 
	 * @param aManualDepositSearchCriteria
	 * @param aPractitionerSearch
	 * @return construct SQL based on arguments and FROM clause
	 */
	private StringBuilder constructManualDepositsSqlStatement(ManualDepositSearchCriteria aManualDepositSearchCriteria,
			boolean aPractitionerSearch) {

		StringBuilder queryString = new StringBuilder(
				"FROM dss_code_table_entry cte, dss_business_arrangement ba, dss_manual_deposits md "
						+ "LEFT OUTER JOIN dss_provider_2 p ON md.provider_number = p.provider_number AND md.provider_type = p.provider_type "
						+ "LEFT OUTER JOIN dss_provider_group pg ON md.provider_group_id = pg.provider_group_id WHERE cte.code_table_number = 115 "
						+ "AND cte.code_table_entry = ba.remuneration_method AND ba.bus_arr_number = md.bus_arr_number ");

		if (aPractitionerSearch) {
			queryString.append("AND md.provider_number IS NOT NULL ");
		}
		if (!aPractitionerSearch) {
			queryString.append("AND pg.provider_group_id IS NOT NULL ");
		}
		// Determine if the user specified any search criteria and append to query appropriately
		if (aManualDepositSearchCriteria.isPractitionerNumberSet()) {
			queryString.append("AND md.provider_number LIKE ?1 ");
		}
		if (aManualDepositSearchCriteria.isPractitionerNameSet()) {
			queryString.append("AND ( upper(p.first_name) LIKE ?2 ");
			queryString.append("OR upper(p.middle_name) LIKE ?2 ");
			queryString.append("OR upper(p.last_name) LIKE ?2 ");
			queryString.append("OR upper(p.organization_name) LIKE ?2 ");

			// parse string into individual words
			String[] arr = aManualDepositSearchCriteria.getPractitionerName().split(" ");

			// The clause comes out like this for multiple names entered
			if (arr.length < 2) {
				// end the statement if there aren't multiple names
				queryString.append(") ");
			} else {
				queryString.append("or (");
				// add condition for each word
				for (int i = 0; i < arr.length; i++) {
					if (i > 0) {
						queryString.append(" and ");
					}

					queryString.append("(upper(p.last_name) LIKE ?2" + i + " or upper(p.middle_name) LIKE ?2" + i
							+ " or upper(p.first_name) LIKE ?2" + i + ") ");
				}

				queryString.append("))");
			}

		}
		if (aManualDepositSearchCriteria.isPractitionerTypeSet()) {
			queryString.append("AND p.provider_type LIKE ?3 ");
		}
		if (aManualDepositSearchCriteria.isGroupNumberSet()) {
			queryString.append("AND pg.provider_group_id LIKE ?4 ");
		}
		if (aManualDepositSearchCriteria.isGroupNameSet()) {
			queryString.append("AND pg.provider_group_name LIKE ?5 ");
		}
		if (aManualDepositSearchCriteria.isDepositAmountSet()) {
			queryString.append("AND md.deposit_amount = ?6 ");
		}
		if (aManualDepositSearchCriteria.isDepositDateSet()) {
			queryString.append("AND md.date_received = ?7 ");
		}

		return queryString;
	}

	/**
	 * Applies query arguments for ManualDeposits search/count
	 * 
	 * @param aQuery
	 * @param aManualDepositSearchCriteria
	 */
	private void setManualSearchQueryParameters(Query aQuery, ManualDepositSearchCriteria aManualDepositSearchCriteria) {

		// Apply the required query parameters
		// NOTE parameters are forced to upper case to ensure equality in LIKE case
		if (aManualDepositSearchCriteria.isPractitionerNumberSet()) {
			aQuery.setParameter(1, "%" + aManualDepositSearchCriteria.getPractitionerNumber().toUpperCase() + "%");
		}
		if (aManualDepositSearchCriteria.isPractitionerNameSet()) {
			aQuery.setParameter(2, "%" + aManualDepositSearchCriteria.getPractitionerName().toUpperCase() + "%");

			// parse string into individual words
			String[] arr = aManualDepositSearchCriteria.getPractitionerName().split(" ");

			// add in individual word arguments if there are more then one
			if (arr.length > 1) {
				for (int i = 0; i < arr.length; i++) {
					int position = new Integer("2" + i);
					// remove any commas
					aQuery.setParameter(position, "%" + arr[i].trim().toUpperCase().replaceAll(",", "") + "%");
				}
			}
		}
		if (aManualDepositSearchCriteria.isPractitionerTypeSet()) {
			aQuery.setParameter(3, "%" + aManualDepositSearchCriteria.getPractitionerType().toUpperCase() + "%");
		}
		if (aManualDepositSearchCriteria.isGroupNumberSet()) {
			aQuery.setParameter(4, "%" + aManualDepositSearchCriteria.getGroupNumber() + "%");
		}
		if (aManualDepositSearchCriteria.isGroupNameSet()) {
			aQuery.setParameter(5, "%" + aManualDepositSearchCriteria.getGroupName() + "%");
		}
		if (aManualDepositSearchCriteria.isDepositAmountSet()) {
			aQuery.setParameter(6, aManualDepositSearchCriteria.getDepositAmount());
		}
		if (aManualDepositSearchCriteria.isDepositDateSet()) {
			aQuery.setParameter(7, aManualDepositSearchCriteria.getDepositDate());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.ManualDepositsDAO#getManualDepositDetails(long)
	 */
	@Override
	public DssManualDeposit getManualDepositDetails(long depositNumber) {
		LOGGER.debug("Start getDssManualDepositByPK in AccountingDAOImpl");

		DssManualDeposit aDssManualDeposits = entityManager.find(DssManualDeposit.class, depositNumber);

		LOGGER.debug("AccountingDAOImpl--getDssManualDeposit(): " + aDssManualDeposits.toString());

		return aDssManualDeposits;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.ManualDepositsDAO#saveOrUpdateManualDeposit(ca.medavie.nspp.audit.service.data
	 * .DssManualDeposit)
	 */
	@Override
	public void saveOrUpdateManualDeposit(DssManualDeposit aDssManualDeposit) {
		LOGGER.debug("AccountingRepositoryImpl -- saveOrUpdateManualDeposit(): Start persisting aDssManualDeposit");
		entityManager.merge(aDssManualDeposit);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.ManualDepositsDAO#deleteManualDeposit(long)
	 */
	@Override
	public void deleteManualDeposit(long depositNumber) {
		StringBuilder hscDeleteQueryString = new StringBuilder(
				"DELETE FROM dss_manual_deposits WHERE deposit_number = ?1");
		// Create delete query
		Query deleteQuery = entityManager.createNativeQuery(hscDeleteQueryString.toString());
		// Set deposit number parameter
		deleteQuery.setParameter(1, depositNumber);
		// Execute HSC delete query
		deleteQuery.executeUpdate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.ManualDepositsDAO#getNextValueFromDssDepositNumberSeq()
	 */
	@Override
	public Long getNextValueFromDssDepositNumberSeq() {
		// dss_deposit_number_seq
		// Get sequence number if sequence number is 0.
		Query query = entityManager.createNativeQuery("SELECT dss_deposit_number_seq.NEXTVAL FROM DUAL");
		BigDecimal tempId = (BigDecimal) query.getSingleResult();
		return tempId.longValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.ManualDepositsDAO#verifyPractitioner(ca.medavie.nspp.audit.service.data.
	 * ManualDeposit)
	 */
	@Override
	public boolean verifyPractitioner(ManualDeposit selectedManualDeposit) {
		String result;
		Query query = entityManager
				.createNativeQuery("SELECT 'X' FROM dss_provider_2 p WHERE p.provider_number = ?1 AND p.provider_type = ?2");
		query.setParameter(1, selectedManualDeposit.getPractitionerNumber());
		query.setParameter(2, selectedManualDeposit.getPractitionerType());
		try {
			result = (String) query.getSingleResult();
		} catch (javax.persistence.NoResultException nre) {
			return false;
		}
		if (result != null) {
			return true;
		} else {
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.ManualDepositsDAO#getManualDepositSearchedPractitioners(ca.medavie.nspp.audit
	 * .service.data.PractitionerSearchCriteria, int, int)
	 */
	@Override
	public List<Object[]> getManualDepositSearchedPractitioners(PractitionerSearchCriteria aPractitionerSearchCriteria,
			int startRow, int maxRows) {

		// Create query string
		StringBuilder queryString = constructPractitionerSqlStatement(aPractitionerSearchCriteria);

		// Append order by
		if (aPractitionerSearchCriteria.isSortingSet()) {

			// Append required ORDER BY SQL
			DataUtilities.appendSQLOrderByStatements(queryString, aPractitionerSearchCriteria);
		} else {
			// Default order
			queryString.append("ORDER BY dp2.last_name ASC");
		}

		Query query = entityManager.createNativeQuery(queryString.toString());

		// Apply pagination arguments
		query.setFirstResult(startRow);
		query.setMaxResults(maxRows);

		// Apply search parameters
		setPractitionerSearchQueryParameters(query, aPractitionerSearchCriteria);

		// Execute query and grab results
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.getResultList();
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.ManualDepositsDAO#getManualDepositSearchedPractitionersCount(ca.medavie.nspp
	 * .audit.service.data.PractitionerSearchCriteria)
	 */
	@Override
	public Integer getManualDepositSearchedPractitionersCount(PractitionerSearchCriteria practitionerSearchCriteria) {

		StringBuilder queryString = new StringBuilder("SELECT COUNT(*) FROM (");
		queryString.append(constructPractitionerSqlStatement(practitionerSearchCriteria));
		queryString.append(")");

		// Create query and set any required arguments
		Query query = entityManager.createNativeQuery(queryString.toString());

		// Apply arguments
		setPractitionerSearchQueryParameters(query, practitionerSearchCriteria);

		// Load count result and convert to Integer (Null safe)
		BigDecimal count = (BigDecimal) query.getSingleResult();
		Integer result = new Integer(0);
		if (count != null) {
			result = count.intValue();
		}
		return result;
	}

	/**
	 * Utility method to generate the common FROM/WHERE portion of the Manual Deposits Practitioner search SQL
	 * 
	 * @param aPractitionerSearchCriteria
	 * @return construct SQL based on arguments and FROM clause
	 */
	private StringBuilder constructPractitionerSqlStatement(PractitionerSearchCriteria aPractitionerSearchCriteria) {

		StringBuilder queryString = new StringBuilder(
				"SELECT DISTINCT dp2.provider_number, dp2.provider_type, "
						+ "CASE WHEN dp2.organization_name IS NULL OR LENGTH(TRIM(dp2.organization_name)) IS NULL THEN dp2.last_name || ', ' || dp2.first_name || ' ' || substr(dp2.middle_name, 1, 1) ELSE dp2.organization_name END as provider_name, "
						+ "dp2.city, dp2.last_name FROM dss_business_arrangement ba, dss_provider_2 dp2 ");

		// Append addition table select from DSS_SPECIATLY and required WHERE statements if a specialty was selected
		if (aPractitionerSearchCriteria.isSpecialtySet()) {
			queryString
					.append(", dss_specialty s WHERE ba.provider_number = dp2.provider_number AND ba.provider_type = dp2.provider_type "
							+ "AND s.provider_number = dp2.provider_number AND s.provider_type = dp2.provider_type ");
		} else {
			/*
			 * Specialty was not provided. Do not append addition DSS_SPECIALTY table. Complete the WHERE
			 * appendqueryString
			 */
			queryString
					.append("WHERE ba.provider_number = dp2.provider_number AND ba.provider_type = dp2.provider_type ");
		}

		// Append search criteria AND statements where required.
		if (aPractitionerSearchCriteria.isPractitionerNumberSet()) {
			queryString.append("and dp2.provider_number = ?1 ");
		}
		if (aPractitionerSearchCriteria.isSubcategorySet()) {
			queryString.append("and dp2.provider_type = ?2 ");
		}
		if (aPractitionerSearchCriteria.isPractitionerNameSet()) {
			queryString.append("and (dp2.last_name LIKE ?3 OR dp2.organization_name LIKE ?3 OR ");
			queryString.append("dp2.first_name LIKE ?3 ");

			// parse string into individual words
			String[] arr = aPractitionerSearchCriteria.getPractitionerName().split(" ");

			// The clause comes out like this for multiple names entered
			if (arr.length < 2) {
				// end the statement if there aren't multiple names
				queryString.append(") ");
			} else {
				queryString.append("or (");
				// add condition for each word
				for (int i = 0; i < arr.length; i++) {
					if (i > 0) {
						queryString.append(" and ");
					}

					queryString.append("(upper(dp2.last_name) LIKE ?3" + i + " or upper(dp2.first_name) LIKE ?3" + i
							+ ") ");
				}

				queryString.append("))");
			}
		}
		if (aPractitionerSearchCriteria.isCitySet()) {
			queryString.append("and dp2.city LIKE ?4 ");
		}
		if (aPractitionerSearchCriteria.isLiscenseNumberSet()) {
			queryString.append("and dp2.provider_license_number = ?5 ");
		}
		if (aPractitionerSearchCriteria.isSpecialtySet()) {
			queryString.append("and s.specialty_code LIKE ?6 ");
		}

		return queryString;
	}

	/**
	 * Applies query arguments for Manual Deposits Practitioner search/count
	 * 
	 * @param aQuery
	 * @param aPractitionerSearchCriteria
	 */
	private void setPractitionerSearchQueryParameters(Query aQuery,
			PractitionerSearchCriteria aPractitionerSearchCriteria) {

		// Add required parameters
		if (aPractitionerSearchCriteria.isPractitionerNumberSet()) {
			aQuery.setParameter(1, aPractitionerSearchCriteria.getPractitionerNumber());
		}
		if (aPractitionerSearchCriteria.isSubcategorySet()) {
			aQuery.setParameter(2, aPractitionerSearchCriteria.getSubcategory());
		}
		if (aPractitionerSearchCriteria.isPractitionerNameSet()) {
			aQuery.setParameter(3, "%" + aPractitionerSearchCriteria.getPractitionerName().toUpperCase() + "%");

			// parse string into individual words
			String[] arr = aPractitionerSearchCriteria.getPractitionerName().split(" ");

			// add in individual word arguments if there are more then one
			if (arr.length > 1) {
				for (int i = 0; i < arr.length; i++) {
					int position = new Integer("3" + i);

					aQuery.setParameter(position, "%" + arr[i].trim().toUpperCase().replaceAll(",", "") + "%");
				}
			}
		}
		if (aPractitionerSearchCriteria.isCitySet()) {
			aQuery.setParameter(4, "%" + aPractitionerSearchCriteria.getCity().toUpperCase() + "%");
		}
		if (aPractitionerSearchCriteria.isLiscenseNumberSet()) {
			aQuery.setParameter(5, aPractitionerSearchCriteria.getLiscenseNumber());
		}
		if (aPractitionerSearchCriteria.isSpecialtySet()) {
			aQuery.setParameter(6, '%' + aPractitionerSearchCriteria.getSpecialty() + '%');
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.ManualDepositsDAO#getSearchedProviderGroups(ca.medavie.nspp.audit.service.data
	 * .ProviderGroupSearchCriteria, int, int)
	 */
	@Override
	public List<DssProviderGroupMappedEntity> getSearchedProviderGroups(
			ProviderGroupSearchCriteria aProviderGroupSearchCriteria, int startRow, int maxRows) {

		StringBuilder queryString = constructGroupSqlStatement(aProviderGroupSearchCriteria);

		// Append the proper order by
		if (aProviderGroupSearchCriteria.isSortingSet()) {

			// Append required ORDER BY SQL
			DataUtilities.appendSQLOrderByStatements(queryString, aProviderGroupSearchCriteria);
		} else {
			// Default sorting
			queryString.append("ORDER BY dpg.provider_group_name ASC");
		}

		// Query to execute to load results
		Query query = entityManager.createNativeQuery(queryString.toString(), "DssProviderGroupsBaseInfoQueryMapping");

		// Apply pagination parameters
		query.setFirstResult(startRow);
		query.setMaxResults(maxRows);

		// Set any required parameters
		setGroupSearchQueryParameters(query, aProviderGroupSearchCriteria);

		// Execute the Query and get results
		@SuppressWarnings("unchecked")
		List<DssProviderGroupMappedEntity> mds = query.getResultList();
		return mds;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.ManualDepositsDAO#getSearchedProviderGroupsCount(ca.medavie.nspp.audit.service
	 * .data.ProviderGroupSearchCriteria)
	 */
	@Override
	public Integer getSearchedProviderGroupsCount(ProviderGroupSearchCriteria aProviderGroupSearchCriteria) {

		StringBuilder queryString = new StringBuilder("SELECT COUNT(*) FROM (");
		queryString.append(constructGroupSqlStatement(aProviderGroupSearchCriteria));
		queryString.append(")");

		// Create query and set any required arguments
		Query query = entityManager.createNativeQuery(queryString.toString());

		// Apply arguments
		setGroupSearchQueryParameters(query, aProviderGroupSearchCriteria);

		// Load count result and convert to Integer (Null safe)
		BigDecimal count = (BigDecimal) query.getSingleResult();
		Integer result = new Integer(0);
		if (count != null) {
			result = count.intValue();
		}
		return result;
	}

	/**
	 * Utility method to generate the common FROM/WHERE portion of the Manual Deposits Group search SQL
	 * 
	 * @param aProviderGroupSearchCriteria
	 * @return construct SQL based on arguments and FROM clause
	 */
	private StringBuilder constructGroupSqlStatement(ProviderGroupSearchCriteria aProviderGroupSearchCriteria) {

		StringBuilder queryString = new StringBuilder(
				"SELECT DISTINCT dpg.provider_group_id, dpg.provider_group_name, dpg.provider_group_type_desc "
						+ "FROM dss_provider_group dpg, dss_provider_address dpa, dss_business_arrangement ba "
						+ "WHERE dpg.provider_group_id = dpa.provider_group_id AND SYSDATE BETWEEN dpa.effective_from_date AND dpa.effective_to_date "
						+ "AND dpg.provider_group_id = ba.provider_group_id ");

		// Determine if the user specified any search criteria and append to query appropriately
		if (aProviderGroupSearchCriteria.isProviderGroupCitySet()) {
			queryString.append("AND dpa.city LIKE ?1 ");
		}
		if (aProviderGroupSearchCriteria.isProviderGroupIdSet()) {
			queryString.append("AND dpg.provider_group_id LIKE ?2 ");
		}
		if (aProviderGroupSearchCriteria.isProviderGroupNameSet()) {
			queryString.append("AND dpg.provider_group_name LIKE ?3 ");
		}
		if (aProviderGroupSearchCriteria.isProviderGroupTypeDescSet()) {
			queryString.append("AND dpg.provider_group_type_desc LIKE ?4 ");
		}

		return queryString;
	}

	/**
	 * Applies query arguments for Manual Deposits Group search/count
	 * 
	 * @param aQuery
	 * @param aProviderGroupSearchCriteria
	 */
	private void setGroupSearchQueryParameters(Query aQuery, ProviderGroupSearchCriteria aProviderGroupSearchCriteria) {

		// Apply the required query parameters
		// NOTE parameters are forced to upper case to ensure equality in LIKE case
		if (aProviderGroupSearchCriteria.isProviderGroupCitySet()) {
			aQuery.setParameter(1, "%" + aProviderGroupSearchCriteria.getProviderGroupCity().toUpperCase() + "%");
		}
		if (aProviderGroupSearchCriteria.isProviderGroupIdSet()) {
			aQuery.setParameter(2, "%" + aProviderGroupSearchCriteria.getProviderGroupId() + "%");
		}
		if (aProviderGroupSearchCriteria.isProviderGroupNameSet()) {
			aQuery.setParameter(3, "%" + aProviderGroupSearchCriteria.getProviderGroupName().toUpperCase() + "%");
		}
		if (aProviderGroupSearchCriteria.isProviderGroupTypeDescSet()) {
			aQuery.setParameter(4, "%" + aProviderGroupSearchCriteria.getProviderGroupTypeDesc() + "%");
		}
	}
}
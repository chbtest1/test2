package ca.medavie.nspp.audit.service.dao;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.data.AuditCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.DataUtilities;
import ca.medavie.nspp.audit.service.data.DinSearchCriteria;
import ca.medavie.nspp.audit.service.data.DssDin;
import ca.medavie.nspp.audit.service.data.DssPharmAuditCriteria;
import ca.medavie.nspp.audit.service.data.DssPharmAuditDinExcl;
import ca.medavie.nspp.audit.service.data.DssPharmClaimAudit;
import ca.medavie.nspp.audit.service.data.DssPharmClaimAuditPK;
import ca.medavie.nspp.audit.service.data.DssRxAudIndivExcl;
import ca.medavie.nspp.audit.service.data.DssRxAudIndivExclPK;
import ca.medavie.nspp.audit.service.data.PharmAuditDinExcl;
import ca.medavie.nspp.audit.service.data.PharmacareAuditLetterResponseSearchCriteria;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssIndividualExclusion;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssPharmAuditCriteriaMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssPharmacareALRMappedEntity;

public class PharmacareDAOImpl implements PharmacareDAO {

	/** Class logger */
	private static final Logger logger = LoggerFactory.getLogger(PharmacareDAOImpl.class);

	/** Handles all DB connections and transactions */
	public EntityManager entityManager;

	public PharmacareDAOImpl(EntityManager em) {

		entityManager = em;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.PharmacareDAO#getPharmacareALRRecords(ca.medavie.nspp.audit.service.data.
	 * PharmacareAuditLetterResponseSearchCriteria, int, int)
	 */
	@Override
	public List<DssPharmacareALRMappedEntity> getPharmacareALRRecords(
			PharmacareAuditLetterResponseSearchCriteria pharmCareALRSearchCriteria, int startRow, int maxRows) {
		logger.debug("PharmacareDAOImpl--getPharmacareALRRecords(): Begin");

		StringBuilder queryString = new StringBuilder(
				"SELECT dpca.claim_ref_num, dpca.audit_run_number, dpca.provider_number, dpca.provider_type, dpca.client_id, dpca.se_selection_date, dpca.audit_letter_creation_date, "
						+ "dpca.response_status_code, dpca.number_of_times_sent, dpca.chargeback_amount, dpca.number_of_services_affected, dpca.audit_note, dpca.modified_by, "
						+ "dpca.last_modified, cc.code_table_alpha_entry_desc as audit_type, ce.din, ce.claim_date, ce.total_amt_paid as full_prescription_cost, cp.original_rx_numb, "
						+ "cp.mqty_paid, dd.name as prod_e, ' ' as client_name, dp2.organization_name || dp2.last_name || ' ' || dp2.first_name || ' ' || substr(dp2.middle_name, 1, 1) as provider_name ");

		queryString.append(constructMedicareALRSqlStatement(pharmCareALRSearchCriteria));

		// Append order by clause
		if (pharmCareALRSearchCriteria.isSortingSet()) {

			// Append required ORDER BY SQL
			DataUtilities.appendSQLOrderByStatements(queryString, pharmCareALRSearchCriteria);

		} else {
			// Use default sorting
			queryString
					.append("ORDER BY CASE WHEN dpca.audit_letter_creation_date IS NULL THEN 1 ELSE 0 END, dpca.audit_letter_creation_date DESC");
		}

		Query query = entityManager.createNativeQuery(queryString.toString(), "DssPharmacareALRQueryMapping");

		// Apply required arguments
		setALRSearchQueryParameters(query, pharmCareALRSearchCriteria);

		// Set pagination values
		query.setFirstResult(startRow);
		query.setMaxResults(maxRows);

		@SuppressWarnings("unchecked")
		List<DssPharmacareALRMappedEntity> results = query.getResultList();
		logger.debug("PharmacareDAOImpl--getPharmacareALRRecords(): End");

		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.PharmacareDAO#getSearchedPharmacareALRCount(ca.medavie.nspp.audit.service.data
	 * .PharmacareAuditLetterResponseSearchCriteria)
	 */
	@Override
	public Integer getSearchedPharmacareALRCount(PharmacareAuditLetterResponseSearchCriteria pharmCareALRSearchCriteria) {

		StringBuilder queryString = new StringBuilder("SELECT COUNT(1) ");
		queryString.append(constructMedicareALRSqlStatement(pharmCareALRSearchCriteria));

		// Create query and set any required arguments
		Query query = entityManager.createNativeQuery(queryString.toString());

		// Apply arguments
		setALRSearchQueryParameters(query, pharmCareALRSearchCriteria);

		// Load count result and convert to Integer (Null safe)
		BigDecimal count = (BigDecimal) query.getSingleResult();
		Integer result = new Integer(0);
		if (count != null) {
			result = count.intValue();
		}
		return result;
	}

	/**
	 * Utility method to generate the common FROM/WHERE portion of the ALR search SQL
	 * 
	 * @param pharmCareALRSearchCriteria
	 * @return construct SQL based on arguments and FROM clause
	 */
	private StringBuilder constructMedicareALRSqlStatement(
			PharmacareAuditLetterResponseSearchCriteria pharmCareALRSearchCriteria) {

		StringBuilder queryString = new StringBuilder(
				"FROM dss_pharm_claim_audit dpca, dss_complete_audit dca, cexp ce, cpend cp, dss_din dd, dss_provider_2 dp2, dss_code_table_alpha_entry cc "
						+ "WHERE dpca.audit_run_number = dca.audit_run_number and dpca.provider_number = dp2.provider_number and dpca.provider_type = dp2.provider_type and dpca.claim_ref_num = ce.claim_ref_num "
						+ "and ce.claim_ref_num = cp.claim_ref_num and ce.din = dd.din and cc.code_table_alpha_number = 123 and cc.code_table_alpha_entry = dca.audit_type ");

		if (pharmCareALRSearchCriteria.isProviderNumberSet()) {
			queryString.append("AND dpca.provider_number =?1 ");
		}
		if (pharmCareALRSearchCriteria.isProviderNameSet()) {
			queryString.append("AND (UPPER (dp2.organization_name) LIKE CONCAT('%', CONCAT(?2,'%'))) ");
		}

		if (pharmCareALRSearchCriteria.isHCNClientIDSet()) {
			queryString.append("AND (UPPER (dpca.client_id) LIKE CONCAT('%', CONCAT(?3,'%'))) ");
		}
		if (pharmCareALRSearchCriteria.isAuditTypeSet()) {
			queryString.append("AND (UPPER (dca.audit_type) LIKE CONCAT('%', CONCAT(?4,'%'))) ");

		}
		if (pharmCareALRSearchCriteria.isResponseStatusSet()) {
			queryString.append("AND (UPPER (dpca.response_status_code) LIKE CONCAT('%', CONCAT(?5,'%'))) ");
		}

		if (pharmCareALRSearchCriteria.isAuditFromDateSet()) {
			queryString.append("AND dpca.audit_letter_creation_date >= to_date(?6,'yyyy-mm-dd') ");
		}

		if (pharmCareALRSearchCriteria.isAuditToDateSet()) {
			queryString.append("AND dpca.audit_letter_creation_date <= to_date(?7,'yyyy-mm-dd') ");
		}

		if (pharmCareALRSearchCriteria.isAuditRunNumberSet()) {
			queryString.append("AND dpca.audit_run_number=?8 ");

		}
		// make it as a "like" instead of "equal" search to make user life easier
		if (pharmCareALRSearchCriteria.isClaimRefNumberSet()) {
			queryString.append("AND (UPPER (dpca.claim_ref_num) LIKE CONCAT('%', CONCAT(?9,'%'))) ");
		}

		return queryString;
	}

	/**
	 * Applies query arguments for ALR search/count
	 * 
	 * @param aQuery
	 * @param pharmCareALRSearchCriteria
	 */
	private void setALRSearchQueryParameters(Query aQuery,
			PharmacareAuditLetterResponseSearchCriteria pharmCareALRSearchCriteria) {

		// Used to format date parameters
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");

		if (pharmCareALRSearchCriteria.isProviderNumberSet()) {

			aQuery.setParameter(1, pharmCareALRSearchCriteria.getPractitionerNumber());
		}

		if (pharmCareALRSearchCriteria.isProviderNameSet()) {

			aQuery.setParameter(2, pharmCareALRSearchCriteria.getPractitionerName().toUpperCase());
		}

		if (pharmCareALRSearchCriteria.isHCNClientIDSet()) {

			aQuery.setParameter(3, pharmCareALRSearchCriteria.getHcnClientID().replaceAll("\\s+", "").toUpperCase());
		}

		if (pharmCareALRSearchCriteria.isAuditTypeSet()) {

			aQuery.setParameter(4, pharmCareALRSearchCriteria.getAuditType().replaceAll("\\s+", "").toUpperCase());
		}

		if (pharmCareALRSearchCriteria.isResponseStatusSet()) {

			aQuery.setParameter(5, pharmCareALRSearchCriteria.getResponseStatus().replaceAll("\\s+", "").toUpperCase());
		}

		if (pharmCareALRSearchCriteria.isAuditFromDateSet()) {

			aQuery.setParameter(6, dateFormater.format(pharmCareALRSearchCriteria.getAuditFromDate()));
		}
		if (pharmCareALRSearchCriteria.isAuditToDateSet()) {

			aQuery.setParameter(7, dateFormater.format(pharmCareALRSearchCriteria.getAuditToDate()));
		}

		if (pharmCareALRSearchCriteria.isAuditRunNumberSet()) {

			aQuery.setParameter(8, pharmCareALRSearchCriteria.getAuditRunNumber());
		}
		if (pharmCareALRSearchCriteria.isClaimRefNumberSet()) {

			aQuery.setParameter(9, pharmCareALRSearchCriteria.getClaimRefNum().replaceAll("\\s+", "").toUpperCase());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.PharmacareDAO#getPharmacareALRByID(ca.medavie.nspp.audit.service.data.
	 * DssPharmClaimAuditPK)
	 */
	@Override
	public DssPharmClaimAudit getPharmacareALRByID(DssPharmClaimAuditPK aPK) {
		logger.debug("Start getPharmCaerALRByID in PeerGroupDAOImpl");

		DssPharmClaimAudit aDssPharmClaimAudit = entityManager.find(DssPharmClaimAudit.class, aPK);

		logger.debug("PharmacareDAOImpl--getPharmCaerALRByID(): " + aDssPharmClaimAudit.toString());

		return aDssPharmClaimAudit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.PharmacareDAO#savePharmacareALR(ca.medavie.nspp.audit.service.data.
	 * DssPharmClaimAudit)
	 */
	@Override
	public void savePharmacareALR(DssPharmClaimAudit aDssPharmClaimAudit) {
		logger.debug("PharmacareDAOImpl -- savePharmCareALR(): Start persisting aDssPharmClaimAudit");
		entityManager.merge(aDssPharmClaimAudit);

		logger.debug("PharmacareDAOImpl--savePharmCareALR(): " + " aDssPharmClaimAudit successfully saved! ");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.PharmacareDAO#getSearchPractitionerAuditCriterias(ca.medavie.nspp.audit.service
	 * .data.AuditCriteriaSearchCriteria, int, int)
	 */
	@Override
	public List<DssPharmAuditCriteriaMappedEntity> getSearchPractitionerAuditCriterias(
			AuditCriteriaSearchCriteria aSearchCriteria, int startRow, int maxRows) {

		logger.debug("PharmacareDAO getSearchPractitionerAuditCriterias() : Begin");

		// Create the query String
		StringBuilder queryString = new StringBuilder(
				"SELECT dpac.audit_criteria_id, dpac.audit_type, dpac.audit_from_date, dpac.audit_to_date, dpac.provider_number, dpac.provider_type, "
						+ "dp2.organization_name || dp2.last_name || ' ' || dp2.first_name || ' ' || substr(dp2.middle_name, 1, 1) as provider_name ");

		// Append remaining query
		queryString.append(constructAuditCriteriaSqlStatement(aSearchCriteria));

		// Append order by clause
		if (aSearchCriteria.isSortingSet()) {
			
			// Append required ORDER BY SQL
			DataUtilities.appendSQLOrderByStatements(queryString, aSearchCriteria);
		}

		// Create query, set required parameters and and execute
		Query query = entityManager
				.createNativeQuery(queryString.toString(), "DssPharmAuditCriteriaNativeQueryMapping");

		// Set pagination values
		query.setFirstResult(startRow);
		query.setMaxResults(maxRows);

		// Append required arguments
		setAuditCriteriaSearchQueryParameters(query, aSearchCriteria);

		// Execute
		@SuppressWarnings("unchecked")
		List<DssPharmAuditCriteriaMappedEntity> result = query.getResultList();

		logger.debug("PharmacareDAO getSearchPractitionerAuditCriterias() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.PharmacareDAO#getSearchedAuditCriteriaCount(ca.medavie.nspp.audit.service.data
	 * .AuditCriteriaSearchCriteria)
	 */
	@Override
	public Integer getSearchedAuditCriteriaCount(AuditCriteriaSearchCriteria anAuditCriteriaSearchCriteria) {

		StringBuilder queryString = new StringBuilder("SELECT COUNT(1) ");
		queryString.append(constructAuditCriteriaSqlStatement(anAuditCriteriaSearchCriteria));

		// Create query and set any required arguments
		Query query = entityManager.createNativeQuery(queryString.toString());

		// Apply arguments
		setAuditCriteriaSearchQueryParameters(query, anAuditCriteriaSearchCriteria);

		// Load count result and convert to Integer (Null safe)
		BigDecimal count = (BigDecimal) query.getSingleResult();
		Integer result = new Integer(0);
		if (count != null) {
			result = count.intValue();
		}
		return result;
	}

	/**
	 * Utility method to generate the common FROM/WHERE portion of the Audit Criteria search SQL
	 * 
	 * @param aSearchCriteria
	 * @return construct SQL based on arguments and FROM clause
	 */
	private StringBuilder constructAuditCriteriaSqlStatement(AuditCriteriaSearchCriteria aSearchCriteria) {

		StringBuilder queryString = new StringBuilder(
				"FROM dss_pharm_audit_criteria dpac, dss_provider_2 dp2 WHERE dpac.provider_number = dp2.provider_number and dpac.provider_type = dp2.provider_type ");

		if (aSearchCriteria.isAuditTypeSet()) {
			queryString.append("and dpac.audit_type = ?1 ");
		}
		if (aSearchCriteria.isAuditCriteriaIdSet()) {
			queryString.append("and dpac.audit_criteria_id = ?2 ");
		}
		if (aSearchCriteria.isPractitionerIdSet()) {
			queryString.append("and dpac.provider_number = ?3 ");
		}
		if (aSearchCriteria.isSubcategorySet()) {
			queryString.append("and dpac.provider_type = ?4 ");
		}
		if (aSearchCriteria.isPractitionerNameSet()) {
			queryString.append("and (UPPER(dp2.first_name) LIKE ?5 or UPPER(dp2.middle_name) LIKE ?5 ");
			queryString.append(" or UPPER(dp2.last_name) LIKE ?5 or UPPER(dp2.organization_name) LIKE ?5 ");

			// parse string into individual words
			String[] arr = aSearchCriteria.getPractitionerName().split(" ");

			// The clause comes out like this for multiple names entered
			if (arr.length < 2) {
				// end the statement if there aren't multiple names
				queryString.append(") ");
			} else {
				queryString.append("or (");
				// add condition for each word
				for (int i = 0; i < arr.length; i++) {
					if (i > 0) {
						queryString.append(" and ");
					}

					queryString.append("(upper(dp2.last_name) LIKE ?5" + i + " or upper(dp2.middle_name) LIKE ?5" + i
							+ " or upper(dp2.first_name) LIKE ?5" + i + ") ");
				}

				queryString.append("))");
			}
		}

		return queryString;
	}

	/**
	 * Applies query arguments for Audit Criteria search/count
	 * 
	 * @param aQuery
	 * @param aSearchCriteria
	 */
	private void setAuditCriteriaSearchQueryParameters(Query aQuery, AuditCriteriaSearchCriteria aSearchCriteria) {

		if (aSearchCriteria.isAuditTypeSet()) {
			aQuery.setParameter(1, aSearchCriteria.getAuditType());
		}
		if (aSearchCriteria.isAuditCriteriaIdSet()) {
			aQuery.setParameter(2, aSearchCriteria.getAuditCriteriaId());
		}
		if (aSearchCriteria.isPractitionerIdSet()) {
			aQuery.setParameter(3, aSearchCriteria.getPractitionerId());
		}
		if (aSearchCriteria.isSubcategorySet()) {
			aQuery.setParameter(4, aSearchCriteria.getSubcategory());
		}
		if (aSearchCriteria.isPractitionerNameSet()) {
			aQuery.setParameter(5, "%" + aSearchCriteria.getPractitionerName().toUpperCase() + "%");

			// parse string into individual words
			String[] arr = aSearchCriteria.getPractitionerName().split(" ");

			// add in individual word arguments if there are more then one
			if (arr.length > 1) {
				for (int i = 0; i < arr.length; i++) {
					int position = new Integer("5" + i);
					// remove any commas
					aQuery.setParameter(position, "%" + arr[i].trim().toUpperCase().replaceAll(",", "") + "%");
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.PharmacareDAO#getDefaultAuditCriteriaByType(java.lang.String)
	 */
	@Override
	public DssPharmAuditCriteria getDefaultAuditCriteriaByType(String anAuditType) {
		logger.debug("PharmacareDAO getDefaultAuditCriteriaByType() : Begin");

		Query query = entityManager
				.createQuery("select dpac from DssPharmAuditCriteria dpac where dpac.auditType = :auditType and "
						+ "dpac.providerNumber is null and dpac.providerType is null");

		// Apply parameter values
		query.setParameter("auditType", anAuditType);

		// Execute query
		@SuppressWarnings("unchecked")
		List<DssPharmAuditCriteria> results = query.getResultList();

		// Return nothing if there are no results
		if (results.isEmpty()) {
			return null;
		}
		// Grab the result and return
		DssPharmAuditCriteria result = results.get(0);

		logger.debug("PharmacareDAO getDefaultAuditCriteriaByType() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.PharmacareDAO#getDssPharmAuditCriteriaByPk(java.lang.Long)
	 */
	@Override
	public DssPharmAuditCriteria getDssPharmAuditCriteriaByPk(Long aPk) {
		logger.debug("PharmacareDAO getDssMedAudCriteriaByPk() : Begin");

		DssPharmAuditCriteria audCriteria = entityManager.find(DssPharmAuditCriteria.class, aPk);

		logger.debug("PharmacareDAO getDssMedAudCriteriaByPk() : End");
		return audCriteria;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.PharmacareDAO#getNextPharmAuditCriteriaSequenceValue()
	 */
	@Override
	public Long getNextPharmAuditCriteriaSequenceValue() {
		logger.debug("PharmacareDAO getNextPharmAuditCriteriaSequenceValue() : Begin");

		// Simple select of next val from a sequence
		Query query = entityManager.createNativeQuery("select dss_pharm_audit_criteria_seq.nextval from dual");

		// Execute query and load result
		BigDecimal result = (BigDecimal) query.getSingleResult();

		logger.debug("PharmacareDAO getNextPharmAuditCriteriaSequenceValue() : End");
		return result.longValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.PharmacareDAO#savePharmacareAuditCriteria(ca.medavie.nspp.audit.service.data
	 * .DssPharmAuditCriteria)
	 */
	@Override
	public void savePharmacareAuditCriteria(DssPharmAuditCriteria anAuditCriteria) {
		logger.debug("PharmacareDAO savePharmacareAuditCriteria() : Begin");

		// Submit changes to DB
		entityManager.merge(anAuditCriteria);

		logger.debug("PharmacareDAO savePharmacareAuditCriteria() : End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.PharmacareDAO#getSearchDins(ca.medavie.nspp.audit.service.data.DinSearchCriteria
	 * )
	 */
	@Override
	public List<DssDin> getSearchDins(DinSearchCriteria aSearchCriteria) {
		logger.debug("PharmacareDAO getSearchDins() : Begin");

		// Create select query
		StringBuilder queryString = new StringBuilder("select dd from DssDin dd ");

		if (aSearchCriteria.isDinSet() || aSearchCriteria.isDrugNameSet()) {
			// Append WHERE
			queryString.append("WHERE ");
		}

		// Append required AND statements if required
		if (aSearchCriteria.isDinSet()) {
			queryString.append("dd.din = :din ");
			if (aSearchCriteria.isDrugNameSet()) {
				// Append AND
				queryString.append("AND ");
			}
		}
		if (aSearchCriteria.isDrugNameSet()) {
			queryString.append("dd.name LIKE :drugName");
		}

		Query query = entityManager.createQuery(queryString.toString());

		// Apply search parameters if provided
		if (aSearchCriteria.isDinSet()) {
			query.setParameter("din", aSearchCriteria.getDin());
		}
		if (aSearchCriteria.isDrugNameSet()) {
			query.setParameter("drugName", "%" + aSearchCriteria.getDrugName().toUpperCase() + "%");
		}

		// Cap results to 500 rows
		query.setMaxResults(500);

		// Execute query
		@SuppressWarnings("unchecked")
		List<DssDin> results = query.getResultList();

		logger.debug("PharmacareDAO getSearchDins() : End");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.PharmacareDAO#getDssRxAudIndivExclByPK(ca.medavie.nspp.audit.service.data.
	 * DssRxAudIndivExclPK)
	 */
	@Override
	public DssRxAudIndivExcl getDssRxAudIndivExclByPK(DssRxAudIndivExclPK aPk) {
		logger.debug("PharmacareDAO getDssRxAudIndivExclByPK() : Begin");

		DssRxAudIndivExcl dssRxExcl = entityManager.find(DssRxAudIndivExcl.class, aPk);

		logger.debug("PharmacareDAO getDssRxAudIndivExclByPK() : End");
		return dssRxExcl;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.PharmacareDAO#deleteIndividualExclusions(java.util.List)
	 */
	@Override
	public void deleteIndividualExclusions(List<DssRxAudIndivExcl> aListOfIndExclToDelete) {
		logger.debug("PharmacareDAO deleteIndividualExclusions() : Begin");

		for (DssRxAudIndivExcl excl : aListOfIndExclToDelete) {
			entityManager.remove(excl);
		}

		// Apply delete request immediately
		entityManager.flush();
		entityManager.clear();

		logger.debug("PharmacareDAO deleteIndividualExclusions() : End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.PharmacareDAO#saveIndividualExclusions(java.util.List)
	 */
	@Override
	public void saveIndividualExclusions(List<DssRxAudIndivExcl> aListOfIndExclToSave) {
		logger.debug("PharmacareDAO saveIndividualExclusions() : Begin");

		for (DssRxAudIndivExcl excl : aListOfIndExclToSave) {
			entityManager.merge(excl);
		}

		logger.debug("PharmacareDAO saveIndividualExclusions() : End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.PharmacareDAO#getIndividualExclusions()
	 */
	@Override
	public List<DssIndividualExclusion> getIndividualExclusions() {
		logger.debug("PharmacareDAO getIndividualExclusions() : Begin");

		// Create the query String
		StringBuilder queryString = new StringBuilder(
				"SELECT rxe.health_card_number, ind.surname || ', ' || ind.first_name as name, rxe.effective_from_date, rxe.effective_to_date, "
						+ "rxe.last_modified, rxe.modified_by FROM DSS_RX_AUD_INDIV_EXCL rxe, dss_individual_2 ind WHERE rxe.health_card_number = ind.health_card_number "
						+ "ORDER BY 2");

		// Create query and execute
		Query query = entityManager.createNativeQuery(queryString.toString(),
				"DssIndividualExclusionNativeQueryMapping");
		@SuppressWarnings("unchecked")
		List<DssIndividualExclusion> result = query.getResultList();

		logger.debug("PharmacareDAO getIndividualExclusions() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.PharmacareDAO#getLastAuditDate(java.lang.Long)
	 */
	@Override
	public Date getLastAuditDate(Long anAuditCriteriaId) {

		logger.debug("PharmacareDAO getLastAuditDate() : Begin");

		Query query = entityManager
				.createNativeQuery("select max(s.audit_letter_creation_date) from dss_pharm_claim_audit s "
						+ "where s.audit_run_number = (select m.last_audit_run_number from DSS_PHARM_AUDIT_CRITERIA m "
						+ "where m.audit_criteria_id = ?1)");

		// Apply query argument
		query.setParameter(1, anAuditCriteriaId);

		// Execute the query
		Date result = (Date) query.getSingleResult();

		logger.debug("PharmacareDAO getLastAuditDate() : End");
		return result;
	}

	@Override
	public List<DssPharmAuditDinExcl> getDinExclusions() {

		logger.debug("PharmacareDAO getDinExclusions() : Begin");

		StringBuilder queryString = new StringBuilder("SELECT pa.din, d.name, pa.age_restriction, pa.gender_restriction, pa.modified_by, pa.last_modified" +
								   "  FROM dss_pharm_audit_din_excl pa, dss_din d" +
								   " WHERE pa.din = d.din");
		
		// Create query and execute
		Query query = entityManager.createNativeQuery(queryString.toString(), "DssDinExclusionNativeQueryMapping");
		@SuppressWarnings("unchecked")
		List<DssPharmAuditDinExcl> result = query.getResultList();
		
		logger.debug("PharmacareDAO getDinExclusions() : End");
		return result;
	}

	@Override
	public void saveDinExclusions(List<DssPharmAuditDinExcl> exclusionsToSave) {
		logger.debug("PharmacareDAO saveDinExclusions() : Begin");

		for (DssPharmAuditDinExcl excl : exclusionsToSave) {
			entityManager.merge(excl);
		}

		logger.debug("PharmacareDAO saveDinExclusions() : End");		
	}

	@Override
	public void deleteDinExclusions(PharmAuditDinExcl pharmAuditDinExcl) {
		logger.debug("PharmacareDAO deleteDinExclusions() : Begin");
		DssPharmAuditDinExcl aDssPharmAuditDinExcl = getDinExclusion(pharmAuditDinExcl.getDin());		
		entityManager.remove(aDssPharmAuditDinExcl);
		logger.debug("PharmacareDAO deleteDinExclusions() : End");
	}
	
	private DssPharmAuditDinExcl getDinExclusion(String din) {
		logger.debug("PharmacareDAO getDinExclusion() : Begin");
		StringBuilder queryString = new StringBuilder("SELECT pa.din, d.name, pa.age_restriction, pa.gender_restriction, pa.modified_by, pa.last_modified" +
								   "  FROM dss_pharm_audit_din_excl pa, dss_din d" +
								   " WHERE pa.din = d.din AND d.din = ?1");
		// Create query and execute
		Query query = entityManager.createNativeQuery(queryString.toString(), "DssDinExclusionNativeQueryMapping");
		query.setParameter(1, din);
		DssPharmAuditDinExcl result = (DssPharmAuditDinExcl) query.getSingleResult();
		logger.debug("PharmacareDAO getDinExclusion() : End");
		return result;
	}
	
}

package ca.medavie.nspp.audit.service.dao;

import java.util.List;

import ca.medavie.nspp.audit.service.data.DssMsiConstant;

public interface DssConstantsDAO {

	/**
	 * List of all DSS Constants
	 * @return 
	 */
	public List<DssMsiConstant> getDssConstants();
	
	/**
	 * Update DSS Constant
	 * @param aDssMsiConstant
	 */
	public void updateDssConstant(DssMsiConstant aDssMsiConstant);

}

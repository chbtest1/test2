package ca.medavie.nspp.audit.service.dao;

import java.util.List;

import ca.medavie.nspp.audit.service.data.Locum;
import ca.medavie.nspp.audit.service.data.LocumSearchCriteria;

/**
 * Interface of the Locum Hosts DAO layer
 */
public interface LocumDAO {

	/**
	 * Search for matching Locums based on the user defined search criteria
	 * 
	 * Data loaded from: dss_provider_2, dss_provider_specialty, dss_provider_locum
	 * 
	 * Query:
	 * 
	 * SELECT DISTINCT p.provider_number, p.provider_type, pa.city, p.birth_date, p.organization_name || p.last_name ||
	 * ' ' || p.first_name || ' ' || substr(p.middle_name, 1, 1), nvl(locum_a.host_provider_number /
	 * locum_a.host_provider_number, 0), nvl(locum_b.visiting_provider_number / locum_b.visiting_provider_number, 0)
	 * FROM dss_provider_2 p LEFT OUTER JOIN dss_provider_specialty ps ON p.provider_type = ps.provider_type AND
	 * p.provider_number = ps.provider_number LEFT OUTER JOIN dss_provider_locum locum_a ON p.provider_number =
	 * locum_a.host_provider_number AND p.provider_type = locum_a.host_provider_type LEFT OUTER JOIN dss_provider_locum
	 * locum_b ON p.provider_number = locum_b.visiting_provider_number AND p.provider_type =
	 * locum_b.visiting_provider_type, dss_provider_address pa WHERE (p.provider_number = pa.provider_number) and
	 * (p.provider_type = pa.provider_type) and ((pa.address_type = 1) and (pa.effective_from_date <= Trunc(SYSDATE) +
	 * .99999) and (pa.effective_to_date >= Trunc(SYSDATE) or (pa.effective_to_date is NULL)))
	 * 
	 * 
	 * Data returned: (0)Practitioner id, (1)Practitioner Type, (2)City, (3)Birth Date, (4)Practitioner Name, (5)Host
	 * Practitioner Count, (6)Visitor Practitioner Count
	 * 
	 * @param aLocumSearchCriteria
	 * @param startRow
	 * @param maxRows
	 * @return list of object[] containing data that makes up a Locum record
	 */
	public List<Object[]> getSearchedLocums(LocumSearchCriteria aLocumSearchCriteria, int startRow, int maxRows);

	/**
	 * Counts all rows that return by an Locum search
	 * 
	 * @param aLocumSearchCriteria
	 * @return - total row count for entire search dataset
	 */
	public Integer getSearchedLocumsCount(LocumSearchCriteria aLocumSearchCriteria);

	/**
	 * Data loaded from: dss_provider_2
	 * 
	 * Query:
	 * 
	 * SELECT dp2.provider_number, dp2.provider_type, dp2.organization_name || dp2.last_name || ' ' || dp2.first_name ||
	 * ' ' || substr(dp2.middle_name, 1, 1) AS provider_name, dp2.city FROM dss_provider_2 dp2
	 * 
	 * Data returned: (0)Practitioner id, (1)Practitioner Type, (2)Practitioner Name, (3)City
	 * 
	 * @param aLocumSearchCriteria
	 * @return list of Object[] containing data that makes up a Locum record
	 */
	public List<Object[]> getSearchedLocumPractitioners(LocumSearchCriteria aLocumSearchCriteria);

	/**
	 * Loads the details of the specified Locum when selected from the search results
	 * 
	 * Data loaded from dss_provider_locum, dss_provider_2
	 * 
	 * Query:
	 * 
	 * Host query
	 * 
	 * SELECT locum.host_provider_number, locum.host_provider_type, locum.visiting_provider_number,
	 * locum.visiting_provider_type, locum.effective_from_date, locum.effective_to_date, provider.organization_name ||
	 * provider.last_name || ' ' || provider.first_name || ' ' || substr(provider.middle_name, 1, 1) as provider_name,
	 * locum.modified_by, locum.last_modified FROM dss_provider_locum locum, dss_provider_2 provider WHERE
	 * (locum.visiting_provider_number = provider.provider_number) and (locum.visiting_provider_type =
	 * provider.provider_type) and ((locum.host_provider_number = ?) and (locum.host_provider_type = ?)) ORDER BY
	 * locum.effective_from_date DESC
	 * 
	 * Visitor query
	 * 
	 * SELECT locum.host_provider_number, locum.host_provider_type, locum.visiting_provider_number,
	 * locum.visiting_provider_type, locum.effective_from_date, locum.effective_to_date, provider.organization_name ||
	 * provider.last_name || ' ' || provider.first_name || ' ' || substr(provider.middle_name, 1, 1) as provider_name,
	 * locum.modified_by, locum.last_modified FROM dss_provider_locum locum, dss_provider_2 provider WHERE
	 * (locum.host_provider_number = provider.provider_number) and (locum.host_provider_type = provider.provider_type)
	 * and ((locum.visiting_provider_number = ?) and (locum.visiting_provider_type = ?)) ORDER BY
	 * locum.effective_from_date DESC
	 * 
	 * Data returned: (0) Host Practitioner id, (1) Host Practitioner Type, (2) Visiting Practitioner id, (3)Visiting
	 * Practitioner Type, (4)Effective From, (5)Effective To, (6)Practitioner Name, (7)Modified By, (8)Last Modified
	 * 
	 * @param aLocum
	 *            - Locum record to load more details of
	 * @return list of Locums contains within the provided Locum
	 */
	public List<Object[]> getLocumDetails(Locum aLocum);

	/**
	 * Saves all changes made to the Locum record
	 * 
	 * @param aLocum
	 */
	public void saveLocum(Locum aLocum);

	/**
	 * Deletes the provided locums from the parent locum
	 * 
	 * @param aListOfLocumsForDelete
	 *            - Locums to delete from the parent
	 */
	public void deleteLocums(List<Locum> aListOfLocumsForDelete);
}

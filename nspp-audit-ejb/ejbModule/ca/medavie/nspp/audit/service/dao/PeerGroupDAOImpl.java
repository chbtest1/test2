package ca.medavie.nspp.audit.service.dao;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.data.DataUtilities;
import ca.medavie.nspp.audit.service.data.DssCodeTableAlphaEntry;
import ca.medavie.nspp.audit.service.data.DssCodeTableAlphaEntryPK;
import ca.medavie.nspp.audit.service.data.DssProvider2;
import ca.medavie.nspp.audit.service.data.DssProviderPeerGroup;
import ca.medavie.nspp.audit.service.data.DssProviderPeerGroupPK;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.PeerGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.QueryConstants;
import ca.medavie.nspp.audit.service.data.TownCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssHealthServiceMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssPeerGroupMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssPostalCodeMappedEntity;

/**
 * Implementation of the PeerGroup DAO layer
 * 
 * 
 */

public class PeerGroupDAOImpl extends QueryConstants implements PeerGroupDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(PeerGroupDAOImpl.class);

	public EntityManager entityManager;

	/**
	 * Constructor for OutlierCriteria with EntityManager provided
	 * 
	 * @param em
	 */
	public PeerGroupDAOImpl(EntityManager em) {

		entityManager = em;
	}

	/**
	 * @param EntityManager
	 *            the entityManager to set
	 */
	public void setEntityManager(EntityManager aEntityManager) {
		entityManager = aEntityManager;
	}

	/**
	 * @param DssProviderPeerGroupPK
	 *            given DssProviderPeerGroupPK
	 * @return A list of DssProviderPeerGroup Object based on the search DssProviderPeerGroupPK
	 */
	@Override
	public DssProviderPeerGroup getDssProviderPeerGroupByPK(DssProviderPeerGroupPK aPK) {

		LOGGER.debug("Start getDssProviderPeerGroupByPK in PeerGroupDAOImpl");

		DssProviderPeerGroup aDssProviderPeerGroup = entityManager.find(DssProviderPeerGroup.class, aPK);

		LOGGER.debug("PeerGroupDAOImpl--getLatestDssProviderPeerGroup(): " + aDssProviderPeerGroup.toString());

		return aDssProviderPeerGroup;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.PeerGroupDAO#getBasePeerGroups(ca.medavie.nspp.audit.service.data.
	 * PeerGroupSearchCriteria, int, int)
	 */
	@Override
	public List<DssPeerGroupMappedEntity> getBasePeerGroups(PeerGroupSearchCriteria peerGroupSearchCriteria,
			int startRow, int maxRows) {

		LOGGER.debug("PeerGroupDAOImpl--getBasePeerGroups(): finding qualifier records from DssHealthService ");

		StringBuilder queryString = constructPeerGroupSqlStatement(peerGroupSearchCriteria);

		// Append the order by
		if (peerGroupSearchCriteria.isSortingSet()) {

			// Append required ORDER BY SQL
			DataUtilities.appendSQLOrderByStatements(queryString, peerGroupSearchCriteria);
		} else {
			// Use default sorting
			queryString.append(" ORDER BY dss_provider_peer_group.provider_peer_group_id asc, "
					+ "dss_provider_peer_group.year_end_date desc");
		}

		Query query = entityManager.createNativeQuery(queryString.toString(), "DssPeerGroupBaseInfoQueryMapping");

		// Apply required arguments.
		setPeerGroupQueryParameters(query, peerGroupSearchCriteria);

		// Apply pagination parameters
		query.setFirstResult(startRow);
		query.setMaxResults(maxRows);

		@SuppressWarnings("unchecked")
		List<DssPeerGroupMappedEntity> results = query.getResultList();
		LOGGER.debug("PeerGroupDAOImpl--getBasePeerGroups(): End");

		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.PeerGroupDAO#getSearchedPeerGroupsCount(ca.medavie.nspp.audit.service.data.
	 * PeerGroupSearchCriteria)
	 */
	@Override
	public Integer getSearchedPeerGroupsCount(PeerGroupSearchCriteria peerGroupSearchCriteria) {

		StringBuilder queryString = new StringBuilder("SELECT COUNT(*) FROM (");

		// Append remaining SQL
		queryString.append(constructPeerGroupSqlStatement(peerGroupSearchCriteria));
		queryString.append(")");

		// Create query and set any required arguments
		Query query = entityManager.createNativeQuery(queryString.toString());

		// Apply arguments
		setPeerGroupQueryParameters(query, peerGroupSearchCriteria);

		// Load count result and convert to Integer (Null safe)
		BigDecimal count = (BigDecimal) query.getSingleResult();
		Integer result = new Integer(0);
		if (count != null) {
			result = count.intValue();
		}
		return result;
	}

	/**
	 * Utility method to generate the common FROM/WHERE portion of the PeerGroup search SQL
	 * 
	 * @param peerGroupSearchCriteria
	 * @return construct SQL based on arguments and FROM clause
	 */
	private StringBuilder constructPeerGroupSqlStatement(PeerGroupSearchCriteria peerGroupSearchCriteria) {

		StringBuilder queryString = new StringBuilder(
				"SELECT DISTINCT dss_provider_peer_group.provider_peer_group_id,"
						+ " dss_provider_peer_group.year_end_date,dss_provider_peer_group.provider_peer_group_name,"
						+ " dss_provider_peer_group.last_modified,dss_provider_peer_group.modified_by "
						+ "FROM dss_provider_peer_group LEFT OUTER JOIN dss_provider_peer_group_xref ON dss_provider_peer_group.provider_peer_group_id = "
						+ " dss_provider_peer_group_xref.provider_peer_group_id AND dss_provider_peer_group.year_end_date = dss_provider_peer_group_xref.year_end_date"
						+ " LEFT OUTER JOIN dss_provider_2 ON dss_provider_peer_group_xref.provider_number = dss_provider_2.provider_number AND dss_provider_peer_group_xref.provider_type ="
						+ " dss_provider_2.provider_type WHERE (dss_provider_peer_group.provider_peer_group_type = 'PROVI')");

		if (peerGroupSearchCriteria.isYearEndDateSet()) {
			queryString.append(" AND dss_provider_peer_group.YEAR_END_DATE = to_date(?1,'yyyy/mm/dd')");
		}

		// if 0 entered, it is considered nothing
		if (peerGroupSearchCriteria.isPeerGroupIDSet()) {
			queryString.append(" AND dss_provider_peer_group.PROVIDER_PEER_GROUP_ID=?2 ");
		}

		if (peerGroupSearchCriteria.isPeerGroupNameSet()) {
			queryString
					.append(" AND UPPER(dss_provider_peer_group.PROVIDER_PEER_GROUP_NAME) like CONCAT('%', CONCAT(?3,'%'))");
		}

		// if 0 entered, it is considered nothing
		if (peerGroupSearchCriteria.isPractitionerNumberSet()) {
			queryString.append(" AND dss_provider_2.PROVIDER_NUMBER = ?4");

		}

		if (peerGroupSearchCriteria.isSubcategorySet()) {
			queryString.append(" AND UPPER(dss_provider_2.PROVIDER_TYPE) LIKE CONCAT('%', CONCAT(?5,'%'))");
		}

		if (peerGroupSearchCriteria.isPractitionerNameSet()) {
			queryString.append("AND (UPPER(dss_provider_2.FIRST_NAME) LIKE ?6 OR ");
			queryString.append(" UPPER(dss_provider_2.LAST_NAME) LIKE ?6 ");

			// parse string into individual words
			String[] arr = peerGroupSearchCriteria.getPractitionerName().split(" ");

			// The clause comes out like this for multiple names entered
			if (arr.length < 2) {
				// end the statement if there aren't multiple names
				queryString.append(") ");
			} else {
				queryString.append("or (");
				// add condition for each word
				for (int i = 0; i < arr.length; i++) {
					if (i > 0) {
						queryString.append(" and ");
					}

					queryString.append("(upper(dss_provider_2.last_name) LIKE ?6" + i
							+ " or upper(dss_provider_2.first_name) LIKE ?6" + i + ") ");
				}

				queryString.append("))");
			}

		}

		return queryString;
	}

	/**
	 * Applies query arguments for PeerGroup search/count
	 * 
	 * @param aQuery
	 * @param peerGroupSearchCriteria
	 */
	private void setPeerGroupQueryParameters(Query aQuery, PeerGroupSearchCriteria peerGroupSearchCriteria) {

		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy/MM/dd");

		if (peerGroupSearchCriteria.isYearEndDateSet()) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(peerGroupSearchCriteria.getYearEndDate());
			aQuery.setParameter(1, dateFormater.format(peerGroupSearchCriteria.getYearEndDate()));
		}

		if (peerGroupSearchCriteria.isPeerGroupIDSet()) {
			aQuery.setParameter(2, peerGroupSearchCriteria.getPeerGroupId());
		}

		if (peerGroupSearchCriteria.isPeerGroupNameSet()) {
			aQuery.setParameter(3, peerGroupSearchCriteria.getPeerGroupName().toUpperCase());
		}

		if (peerGroupSearchCriteria.isPractitionerNumberSet()) {
			aQuery.setParameter(4, peerGroupSearchCriteria.getPractitionerNumber());
		}

		if (peerGroupSearchCriteria.isSubcategorySet()) {
			aQuery.setParameter(5, peerGroupSearchCriteria.getSubcategory().toUpperCase());
		}

		if (peerGroupSearchCriteria.isPractitionerNameSet()) {
			aQuery.setParameter(6, "%" + peerGroupSearchCriteria.getPractitionerName().toUpperCase() + "%");

			// parse string into individual words
			String[] arr = peerGroupSearchCriteria.getPractitionerName().split(" ");

			// add in individual word arguments if there are more then one
			if (arr.length > 1) {
				for (int i = 0; i < arr.length; i++) {
					int position = new Integer("6" + i);
					// remove any commas
					aQuery.setParameter(position, "%" + arr[i].trim().toUpperCase().replaceAll(",", "") + "%");
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.PeerGroupDAO#getSearchedProviders(ca.medavie.nspp.audit.service.data.
	 * PractitionerSearchCriteria)
	 */
	@Override
	public List<DssProvider2> getSearchedDssProvider2(PractitionerSearchCriteria aPractitionerSearchCriteria) {

		StringBuilder queryString = new StringBuilder("SELECT DISTINCT p2 FROM DssProvider2 p2 ");

		// Append joins if the user specified Group Name and/or ID
		if (aPractitionerSearchCriteria.isPractitionerGroupIdSet()
				|| aPractitionerSearchCriteria.isPractitionerGroupNameSet()) {
			queryString
					.append("LEFT JOIN p2.dpgx dpgx LEFT JOIN dpgx.dpg dpg WHERE p2.id.providerType <> 'RX' AND p2.id.providerNumber = dpgx.id.providerNumber ");
			queryString
					.append("AND p2.id.providerType = dpgx.id.providerType AND dpgx.id.providerGroupId = dpg.providerGroupId ");
		} else {

			// No joins required..
			queryString.append("WHERE p2.id.providerType <> 'RX' ");
		}

		// Determine if the user specified any search criteria and append to query appropriately
		if (aPractitionerSearchCriteria.isPractitionerNumberSet()) {
			queryString.append("AND p2.id.providerNumber LIKE :practitionerNumber ");
		}
		if (aPractitionerSearchCriteria.isSubcategorySet()) {
			queryString.append("AND p2.id.providerType = :practitionerType ");
		}
		if (aPractitionerSearchCriteria.isPractitionerNameSet()) {
			queryString.append("AND (p2.firstName LIKE :practitionerName OR ");
			queryString.append("p2.middleName LIKE :practitionerName OR ");
			queryString.append("p2.lastName LIKE :practitionerName) ");
		}
		if (aPractitionerSearchCriteria.isLiscenseNumberSet()) {
			queryString.append("AND p2.providerLicenseNumber = :practitionerLicenseNumber ");
		}
		if (aPractitionerSearchCriteria.isPractitionerGroupIdSet()) {
			queryString.append("AND dpg.providerGroupId LIKE :practitionerGroupId ");
		}
		if (aPractitionerSearchCriteria.isPractitionerGroupNameSet()) {
			queryString.append("AND dpg.providerGroupName LIKE :practitionerGroupName ");
		}

		// Append the proper order by
		queryString.append("ORDER BY p2.id.providerNumber ASC");

		// Query to execute to load results
		Query query = entityManager.createQuery(queryString.toString());

		// Apply the required query parameters
		// NOTE parameters are forced to upper case to ensure equality in LIKE case
		if (aPractitionerSearchCriteria.isPractitionerNumberSet()) {
			query.setParameter("practitionerNumber", "%" + aPractitionerSearchCriteria.getPractitionerNumber() + "%");
		}
		if (aPractitionerSearchCriteria.isSubcategorySet()) {
			query.setParameter("practitionerType", aPractitionerSearchCriteria.getSubcategory().toUpperCase());
		}
		if (aPractitionerSearchCriteria.isPractitionerNameSet()) {
			query.setParameter("practitionerName", "%"
					+ aPractitionerSearchCriteria.getPractitionerName().toUpperCase() + "%");
		}
		if (aPractitionerSearchCriteria.isLiscenseNumberSet()) {
			query.setParameter("practitionerLicenseNumber", aPractitionerSearchCriteria.getLiscenseNumber());
		}
		if (aPractitionerSearchCriteria.isPractitionerGroupIdSet()) {
			query.setParameter("practitionerGroupId", "%" + aPractitionerSearchCriteria.getPractitionerGroupId() + "%");
		}
		if (aPractitionerSearchCriteria.isPractitionerGroupNameSet()) {
			query.setParameter("practitionerGroupName", "%"
					+ aPractitionerSearchCriteria.getPractitionerGroupName().toUpperCase() + "%");
		}

		// Cap the query to 500 rows
		query.setMaxResults(500);

		// Execute the Query and get results
		@SuppressWarnings("unchecked")
		List<DssProvider2> dp2s = query.getResultList();

		return dp2s;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.PeerGroupDAO#getLatestDssProviderPeerGroup()
	 */
	@Override
	public List<DssPeerGroupMappedEntity> getLatestDssProviderPeerGroup() {
		LOGGER.debug("Start getLatestDssProviderPeerGroup in PeerGroupDAOImpl");

		String queryString = "select a1.provider_peer_group_id, " + "a1.provider_peer_group_name,a1.year_end_date,"
				+ "a1.modified_by,a1.last_modified from dss_provider_peer_group a1 "
				+ "where a1.year_end_date =(select max(b1.year_end_date) from "
				+ "dss_provider_peer_group b1 where b1.provider_peer_group_id = a1.provider_peer_group_id)"
				+ "and a1.provider_peer_group_type = 'PROVI'";

		Query query = entityManager.createNativeQuery(queryString.toString(), "DssPeerGroupBaseInfoQueryMapping");
		@SuppressWarnings("unchecked")
		List<DssPeerGroupMappedEntity> results = query.getResultList();

		LOGGER.debug("PeerGroupDAOImpl--getLatestDssProviderPeerGroup(): " + results.size()
				+ " results found from {getLatestDssProviderPeerGroup}");

		return results;
	}

	/**
	 * @return the entityManager
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.PeerGroupDAO#savePeergroup(ca.medavie.nspp.audit.service.data.PeerGroup)
	 */
	@Override
	public DssProviderPeerGroup saveOrUpdateDssProviderPeerGroup(DssProviderPeerGroup aDssProviderPeerGroup) {

		LOGGER.debug("PeerGroupRepositoryImpl -- saveOrUpdateDssProviderPeerGroup(): Start persisting aDssProviderPeerGroup");
		entityManager.merge(aDssProviderPeerGroup);

		return aDssProviderPeerGroup;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.PeerGroupDAO#saveOrUpdateBulkDssProviderPeerGroup(java.util.List)
	 */
	@Override
	public void copyDssProviderPeerGroups(List<DssProviderPeerGroup> aListOfDssProviderPeerGroupsToSave) {
		LOGGER.debug("PeerGroupRepositoryImpl -- saveOrUpdateBulkDssProviderPeerGroup():"
				+ " Start persisting for each DssProviderPeerGroup record");
		// Save each record
		for (DssProviderPeerGroup dspp : aListOfDssProviderPeerGroupsToSave) {
			// Persist the DSS_PROVIDER_PEER_GROUP object and it's children
			entityManager.merge(dspp);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.PeerGroupDAO#getDssProviderPeerGroupByID(ca.medavie.nspp.audit.service.data
	 * .DssProviderPeerGroupPK)
	 */
	@Override
	public DssProviderPeerGroup getDssProviderPeerGroupByID(DssProviderPeerGroupPK aPK) {
		DssProviderPeerGroup aDssProviderPeerGroup = entityManager.find(DssProviderPeerGroup.class, aPK);
		return aDssProviderPeerGroup;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.PeerGroupDAO#getSpeciatyDesc(ca.medavie.nspp.audit.service.data.
	 * DssCodeTableAlphaEntryPK)
	 */
	@Override
	public DssCodeTableAlphaEntry getSpeciatyDesc(DssCodeTableAlphaEntryPK aPK) {
		// 102 is a magic number

		LOGGER.debug("PeerGroupDAOImpl--getDssAlphaEntries(): finding DssCodeTableAlphaEntry by primary key : ery is [ "
				+ aPK.getCodeTableAlphaEntry() + "] and number is[" + aPK.getCodeTableAlphaNumber() + "]");
		DssCodeTableAlphaEntry aDssCodeTableAlphaEntry = entityManager.find(DssCodeTableAlphaEntry.class, aPK);

		return aDssCodeTableAlphaEntry;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.PeerGroupDAO#getNextValueFromDssProviderPeerGroupSeq()
	 */
	@Override
	public Long getNextValueFromDssProviderPeerGroupSeq() {
		Query query = entityManager.createNativeQuery("SELECT DSS_PROVIDER_PEER_GROUP_ID_SEQ.nextVal FROM DUAL");
		BigDecimal nextval = (BigDecimal) query.getSingleResult();
		// Safe to call longValue() since the sequence will never have any decimal places
		return nextval.longValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.PeerGroupDAO#getAllUniqueDssPostalCodes()
	 */
	public List<DssPostalCodeMappedEntity> getAllUniqueDssPostalCodes() {
		LOGGER.debug("Start getAllUniqueDssPostalCodes in PeerGroupDAOImpl");

		String queryString = "select distinct dptc.town_code,dptc.town_name,dptc.county_code,dptc.county_name,dptc.municipality_code,dptc.municipality_name,dptc.health_region_code,dptc.health_region_desc FROM dss_postal_code dptc ";

		Query query = entityManager.createNativeQuery(queryString.toString(), "DssPostalCodeEntityMapping");
		@SuppressWarnings("unchecked")
		List<DssPostalCodeMappedEntity> results = query.getResultList();

		LOGGER.debug("PeerGroupDAOImpl--getAllUniqueDssPostalCodes(): there are " + results.size() + " items found ");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.PeerGroupDAO#getSearcheDssPostalCodeBySC(ca.medavie.nspp.audit.service.data
	 * .TownCriteriaSearchCriteria)
	 */
	@Override
	public List<Object[]> getSearcheDssPostalCodeBySC(TownCriteriaSearchCriteria townSearchCriteria) {

		LOGGER.debug("Start getSearcheDssPostalCodeBySC in PeerGroupDAOImpl townSearchCriteria is {"
				+ townSearchCriteria + "}");

		StringBuilder queryString = new StringBuilder();

		boolean isTownCodeNameSet = false;
		boolean isCountyCodeNameSet = false;
		boolean isMunicipalityCodeNameSet = false;
		boolean isHealthRegionCodeNameSet = false;

		queryString
				.append("select distinct dpc.townCode,dpc.townName,dpc.countyCode,dpc.countyName"
						+ ",dpc.municipalityCode,dpc.municipalityName,dpc.healthRegionCode,dpc.healthRegionDesc FROM  DssPostalCode dpc where ");

		// if town code being set
		if (townSearchCriteria.getTownCodeName() != null && !townSearchCriteria.getTownCodeName().isEmpty()) {

			queryString.append(" dpc.townCode = : townCode and ");
			queryString.append(" dpc.townName LIKE : townName and ");
			isTownCodeNameSet = true;
		}

		// if county code being set
		if (townSearchCriteria.getCountyCodeName() != null && !townSearchCriteria.getCountyCodeName().isEmpty()) {

			queryString.append(" dpc.countyCode = : countyCode and ");
			queryString.append(" dpc.countyName LIKE : countyName and ");
			isCountyCodeNameSet = true;
		}

		// if Municipality Code being set
		if (townSearchCriteria.getMunicipalityCodeName() != null
				&& !townSearchCriteria.getMunicipalityCodeName().isEmpty()) {

			queryString.append(" dpc.municipalityCode = : municipalityCode and ");
			queryString.append(" dpc.municipalityName LIKE : municipalityName and ");
			isMunicipalityCodeNameSet = true;
		}

		// if Health Region code being set (shown as name)
		if (townSearchCriteria.getHealthRegionCodeName() != null
				&& !townSearchCriteria.getHealthRegionCodeName().isEmpty()) {

			queryString.append(" dpc.healthRegionCode = : healthRegionCode and ");
			queryString.append(" dpc.healthRegionDesc LIKE : healthRegionDesc and ");
			isHealthRegionCodeNameSet = true;
		}

		String query_end_with_and = queryString.substring(queryString.length() - 4, queryString.length() - 1);
		String query_end_with_where = queryString.substring(queryString.length() - 6, queryString.length() - 1);
		String stringQuery = null;

		// if query string end with "and" remove it
		if (query_end_with_and.trim().equals("and")) {

			stringQuery = queryString.substring(0, queryString.length() - 4);
		}

		// if query string end with "where" remove it
		if (query_end_with_where.trim().equals("where")) {

			stringQuery = queryString.substring(0, queryString.length() - 6);
		}

		LOGGER.debug("PeerGroupDAOImpl--getSearcheDssPostalCodeBySC(), JPQL query is " + stringQuery);

		Query query = entityManager.createQuery(stringQuery);

		if (isTownCodeNameSet) {
			query.setParameter("townCode", townSearchCriteria.getTownCode());
			query.setParameter("townName", "%" + townSearchCriteria.getTownName().toUpperCase() + "%");
		}

		if (isCountyCodeNameSet) {

			query.setParameter("countyCode", townSearchCriteria.getCountyCode());
			query.setParameter("countyName", "%" + townSearchCriteria.getCountyName().toUpperCase() + "%");
		}

		if (isMunicipalityCodeNameSet) {

			query.setParameter("municipalityCode", townSearchCriteria.getMunicipalityCode());
			query.setParameter("municipalityName", "%" + townSearchCriteria.getMunicipalityName().toUpperCase() + "%");
		}

		if (isHealthRegionCodeNameSet) {

			query.setParameter("healthRegionCode", townSearchCriteria.getHealthRegionCode());
			query.setParameter("healthRegionDesc", "%" + townSearchCriteria.getHealthRegionDesc().toUpperCase() + "%");
		}

		// Execute the Query
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.getResultList();

		LOGGER.debug("PeerGroupDAOImpl--getSearcheDssPostalCodeBySC(): " + results.size()
				+ " results found from {getSearcheDssPostalCodeBySC}");

		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.PeerGroupDAO#getHealthServiceEFCodes()
	 */
	@Override
	public List<Date> getHealthServiceEFCodes() {

		LOGGER.debug("PeerGroupDAOImpl--getHealthServiceEFCodes(): finding  records from DssHealthService ");
		Query query = entityManager
				.createQuery(" select distinct dhs.effectiveFromDate FROM DssHealthService dhs order by  dhs.effectiveFromDate desc");
		// Execute the Query
		@SuppressWarnings("unchecked")
		List<Date> results = query.getResultList();

		LOGGER.debug("PeerGroupDAOImpl--getHealthServiceEFCodes(): there" + " are " + results.size() + "  items found ");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.PeerGroupDAO#getSearchedProviders(ca.medavie.nspp.audit.service.data.
	 * PractitionerSearchCriteria)
	 */
	@Override
	/**
	 * SQL query pulled from powerbuilder as follows:
	 * 
	 * 		SELECT DISTINCT dss_health_service.health_service_id,
	 * 			dss_health_service.program_code,
	 * 			dss_health_service.health_service_code,
	 * 			dss_health_service.qualifier_code,
	 * 			dss_health_service.effective_from_date,
	 * 			dss_health_service.effective_to_date,
	 * 			dss_health_service_code.description,
	 * 			'N' as selected_indicator,
	 * 			dss_health_service.modifiers,
	 *          dss_health_service.implicit_modifiers
	 *		FROM {oj dss_health_service
	 *		LEFT OUTER JOIN dss_health_service_group_xref
	 *		ON dss_health_service.health_service_id = dss_health_service_group_xref.health_service_id},
	 *			dss_health_service_code
	 *		WHERE (dss_health_service.health_service_code = dss_health_service_code.health_service_code)
	 *		AND (dss_health_service.qualifier_code = dss_health_service_code.qualifier_code)
	 *		AND ((dss_health_service.program_code = :program_code) 
	 *		AND (dss_health_service.effective_to_date >= trunc(dss_health_service_code.effective_from_date)) 
	 *		AND (dss_health_service.effective_from_date <= trunc(dss_health_service_code.effective_to_date) + .99999)) 
	 **/
	public List<DssHealthServiceMappedEntity> getSearchedHealthServices(
			HealthServiceCriteriaSearchCriteria aHealthServiceCriteriaSearchCriteria) {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		StringBuilder queryString = new StringBuilder(
				"SELECT DISTINCT dss_health_service.health_service_id, dss_health_service.program_code, dss_health_service.health_service_code, "
						+ "dss_health_service.qualifier_code, dss_health_service.effective_from_date, dss_health_service.effective_to_date, dss_health_service_code.description, "
						+ "'N' as selected_indicator, dss_health_service.modifiers, dss_health_service.implicit_modifiers FROM dss_health_service "
						+ "LEFT OUTER JOIN dss_health_service_group_xref ON dss_health_service.health_service_id = dss_health_service_group_xref.health_service_id, "
						+ "dss_health_service_code ");

		// Append an additional ON join table if modifiers were specified in search
		if (aHealthServiceCriteriaSearchCriteria.isModifierSet()
				|| aHealthServiceCriteriaSearchCriteria.isImplicitModifierSet()) {
			queryString.append(", dss_health_service_modifiers ");
		}

		// Append remaining base query
		queryString
				.append("WHERE (dss_health_service.health_service_code = dss_health_service_code.health_service_code) "
						+ "AND (dss_health_service.qualifier_code = dss_health_service_code.qualifier_code) AND (dss_health_service.effective_to_date >= trunc(dss_health_service_code.effective_from_date)) "
						+ "AND (dss_health_service.effective_from_date <= trunc(dss_health_service_code.effective_to_date) + .99999) ");

		// Append required AND statements
		if (aHealthServiceCriteriaSearchCriteria.isModifierSet()
				|| aHealthServiceCriteriaSearchCriteria.isImplicitModifierSet()) {
			queryString
					.append("AND dss_health_service.HEALTH_SERVICE_ID= DSS_HEALTH_SERVICE_MODIFIERS.HEALTH_SERVICE_ID ");
		}
		if (aHealthServiceCriteriaSearchCriteria.isModifierSet()) {
			queryString.append(" AND UPPER(dss_health_service.modifiers) like CONCAT('%', CONCAT(UPPER(?1),'%')) ");
		}
		if (aHealthServiceCriteriaSearchCriteria.isImplicitModifierSet()) {
			queryString.append(" AND UPPER(dss_health_service.implicit_modifiers) like CONCAT('%', CONCAT(UPPER(?2),'%')) ");
		}
		if (aHealthServiceCriteriaSearchCriteria.isProgramSet()) {
			queryString.append(" AND dss_health_service.program_code = ?3 ");
		}
		if (aHealthServiceCriteriaSearchCriteria.isEffectiveDateFromSet()) {
			queryString.append(" AND dss_health_service.effective_from_date = to_date(?4,'dd/mm/yyyy') ");
			queryString.append(" AND (dss_health_service.effective_to_date >= to_date(?4,'dd/mm/yyyy') ");
			queryString.append(" OR dss_health_service.effective_to_date is NULL) ");
		}
		if (aHealthServiceCriteriaSearchCriteria.isHealthServiceCodeSet()) {
			queryString.append(" AND dss_health_service.health_service_code like CONCAT('%', CONCAT(UPPER(?5),'%')) ");
		}
		if (aHealthServiceCriteriaSearchCriteria.isQualifierSet()) {
			queryString.append(" AND dss_health_service.qualifier_code = ?6 ");
		}
		if (aHealthServiceCriteriaSearchCriteria.isMsiFeeCodeSet()) {
			queryString.append(" AND dss_health_service.msi_fee_code = ?7 ");
		}
		if (aHealthServiceCriteriaSearchCriteria.isHealthServiceDescSet()) {
			queryString.append(" AND (dss_health_service_code.description like CONCAT('%', CONCAT(UPPER(?8),'%')) OR ");
			queryString
					.append("  dss_health_service_code.alternate_wording like CONCAT('%', CONCAT(UPPER(?8),'%')) ) ");
		}
		if (aHealthServiceCriteriaSearchCriteria.isHealthServiceGroupSet()) {
			queryString.append(" AND dss_health_service.health_service_id in "
					+ "  (select dss_health_service_group_xref.health_service_id from dss_health_service_group_xref  "
					+ "  where dss_health_service_group_xref.health_service_group_id = ?9)");
		}

		queryString
				.append(" ORDER BY dss_health_service.health_service_code ASC, dss_health_service.effective_from_date DESC");

		// Create query and append any search arguments
		Query query = entityManager.createNativeQuery(queryString.toString(), "DssHealthServiceNativeQueryMapping");
		if (aHealthServiceCriteriaSearchCriteria.isModifierSet()) {
			query.setParameter(1, aHealthServiceCriteriaSearchCriteria.getModifierString());
		}
		if (aHealthServiceCriteriaSearchCriteria.isImplicitModifierSet()) {
			query.setParameter(2, aHealthServiceCriteriaSearchCriteria.getImplicitModifierString());
		}
		if (aHealthServiceCriteriaSearchCriteria.isProgramSet()) {
			query.setParameter(3, aHealthServiceCriteriaSearchCriteria.getProgramCode());
		}
		if (aHealthServiceCriteriaSearchCriteria.isEffectiveDateFromSet()) {
			query.setParameter(4, sdf.format(aHealthServiceCriteriaSearchCriteria.getEffectiveFrom()));
		}
		if (aHealthServiceCriteriaSearchCriteria.isHealthServiceCodeSet()) {
			query.setParameter(5, aHealthServiceCriteriaSearchCriteria.getHealthServiceCode());
		}
		if (aHealthServiceCriteriaSearchCriteria.isQualifierSet()) {
			query.setParameter(6, aHealthServiceCriteriaSearchCriteria.getQualifier());
		}
		if (aHealthServiceCriteriaSearchCriteria.isMsiFeeCodeSet()) {
			query.setParameter(7, aHealthServiceCriteriaSearchCriteria.getMsiFeeCode());
		}
		if (aHealthServiceCriteriaSearchCriteria.isHealthServiceDescSet()) {
			query.setParameter(8, aHealthServiceCriteriaSearchCriteria.getHealthServiceCodeDescription());
		}
		if (aHealthServiceCriteriaSearchCriteria.isHealthServiceGroupSet()) {
			query.setParameter(9, aHealthServiceCriteriaSearchCriteria.getHealthServiceGroup());
		}

		// limit the results to 500
		query.setMaxResults(500);

		@SuppressWarnings("unchecked")
		List<DssHealthServiceMappedEntity> results = query.getResultList();

		LOGGER.debug("PeerGroupDAOImpl--getSearchedHealthServices(): there" + " are " + results.size()
				+ "  items found ");
		return results;
	}

	@Override
	public List<String> getDssHealthServiceCodeDesc(String healthServiceCode, String qualifierCode) {
		LOGGER.debug("PeerGroupDAOImpl--getDssHealthServiceCodeDesc(): finding healther service code desc");
		Query query = entityManager
				.createQuery(" select distinct dhsc.description FROM DssHealthServiceCode dhsc WHERE dhsc.id.healthServiceCode = : healthServiceCode"
						+ " AND dhsc.id.qualifierCode = : qualifierCode");

		query.setParameter("healthServiceCode", healthServiceCode);
		query.setParameter("qualifierCode", qualifierCode);
		// Execute the Query
		@SuppressWarnings("unchecked")
		List<String> results = query.getResultList();

		LOGGER.debug("PeerGroupDAOImpl--getDssHealthServiceCodeDesc(): there are " + results.size()
				+ "  health service desc  items found ");
		return results;
	}
}

package ca.medavie.nspp.audit.service.dao;

import java.util.Date;
import java.util.List;

import ca.medavie.nspp.audit.service.data.AuditCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.DinSearchCriteria;
import ca.medavie.nspp.audit.service.data.DssDin;
import ca.medavie.nspp.audit.service.data.DssPharmAuditCriteria;
import ca.medavie.nspp.audit.service.data.DssPharmAuditDinExcl;
import ca.medavie.nspp.audit.service.data.DssPharmClaimAudit;
import ca.medavie.nspp.audit.service.data.DssPharmClaimAuditPK;
import ca.medavie.nspp.audit.service.data.DssRxAudIndivExcl;
import ca.medavie.nspp.audit.service.data.DssRxAudIndivExclPK;
import ca.medavie.nspp.audit.service.data.PharmAuditDinExcl;
import ca.medavie.nspp.audit.service.data.PharmacareAuditLetterResponseSearchCriteria;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssIndividualExclusion;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssPharmAuditCriteriaMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssPharmacareALRMappedEntity;
import ca.medavie.nspp.audit.service.exception.MedicareServiceException;

public interface PharmacareDAO {

	/**
	 * @param pharmCareALRSearchCriteria
	 * @param startRow
	 * @param maxRows
	 * @return
	 */
	public List<DssPharmacareALRMappedEntity> getPharmacareALRRecords(
			PharmacareAuditLetterResponseSearchCriteria pharmCareALRSearchCriteria, int startRow, int maxRows);

	/**
	 * Counts all rows that return by an ALR search
	 * 
	 * @param pharmCareALRSearchCriteria
	 * @return - total row count for entire search dataset
	 */
	public Integer getSearchedPharmacareALRCount(PharmacareAuditLetterResponseSearchCriteria pharmCareALRSearchCriteria);

	/**
	 * Find PharmCaerALR By ID
	 * 
	 * @param pk
	 * @return a DssPharmClaimAudit
	 */
	public DssPharmClaimAudit getPharmacareALRByID(DssPharmClaimAuditPK pk);

	/**
	 * Save DssPharmClaimAudit
	 * 
	 * @param aDssPharmClaimAudit
	 */
	public void savePharmacareALR(DssPharmClaimAudit aDssPharmClaimAudit);

	/**
	 * Loads any matching DssPharmAuditCriteriaMappedEntity records using the specified Search Criteria
	 * 
	 * @param aSearchCriteria
	 * @param startRow
	 * @param maxRows
	 * @return any matching DssPharmAuditCriteriaMappedEntity records
	 */
	public List<DssPharmAuditCriteriaMappedEntity> getSearchPractitionerAuditCriterias(
			AuditCriteriaSearchCriteria aSearchCriteria, int startRow, int maxRows);

	/**
	 * Counts all rows that return by an Audit Criteria search
	 * 
	 * @param anAuditCriteriaSearchCriteria
	 * @return - total row count for entire search dataset
	 */
	public Integer getSearchedAuditCriteriaCount(AuditCriteriaSearchCriteria anAuditCriteriaSearchCriteria);

	/**
	 * Attempts to load a DssPharmAuditCriteria by the specified AuditType
	 * 
	 * @param anAuditType
	 * @return the matching DssPharmAuditCriteria record if it exists or a brand new DssPharmAuditCriteria object if
	 *         nothing is found
	 */
	public DssPharmAuditCriteria getDefaultAuditCriteriaByType(String anAuditType);

	/**
	 * Attempts to load DssPharmAuditCriteria record by PK
	 * 
	 * @param aPk
	 *            - PK of the desired DssPharmAuditCriteria
	 * @return DssPharmAuditCriteria if found, null if not.
	 */
	public DssPharmAuditCriteria getDssPharmAuditCriteriaByPk(Long aPk);

	/**
	 * @return next value from dss_pharm_audit_criteria_seq
	 */
	public Long getNextPharmAuditCriteriaSequenceValue();

	/**
	 * Saves the specified DssPharmAuditCriteria record
	 * 
	 * @param anAuditCriteria
	 * @throws MedicareServiceException
	 */
	public void savePharmacareAuditCriteria(DssPharmAuditCriteria anAuditCriteria);

	/**
	 * Loads any matching DssDin records using the search criteria
	 * 
	 * @param aSearchCriteria
	 * @return list of matching DssDin records
	 */
	public List<DssDin> getSearchDins(DinSearchCriteria aSearchCriteria);

	/**
	 * Attempts to load a DssRxAudIndivExcl record by PK
	 * 
	 * @param aPk
	 * @return matching DssRxAudIndivExcl or null
	 */
	public DssRxAudIndivExcl getDssRxAudIndivExclByPK(DssRxAudIndivExclPK aPk);

	/**
	 * Deletes the specified DssRxAudIndivExcl records from the database
	 * 
	 * @param aListOfIndExclToDelete
	 */
	public void deleteIndividualExclusions(List<DssRxAudIndivExcl> aListOfIndExclToDelete);

	/**
	 * Saves the new and updated DssRxAudIndivExcl records to the database
	 * 
	 * @param aListOfIndExclToSave
	 */
	public void saveIndividualExclusions(List<DssRxAudIndivExcl> aListOfIndExclToSave);

	/**
	 * Load the current DssIndividualExclusion records
	 * 
	 * @return current DssIndividualExclusion records
	 */
	public List<DssIndividualExclusion> getIndividualExclusions();

	/**
	 * Loads the last Audit Date for the specified Audit Criteria.
	 * 
	 * @param anAuditCriteriaId
	 * @return the lastAudit date for the Audit Criteria ID provided
	 */
	public Date getLastAuditDate(Long anAuditCriteriaId);

	/**
	 * Returns list of Din Exclusions given a partial or complete din.
	 * @param din
	 * @return
	 */
	public List<DssPharmAuditDinExcl> getDinExclusions();

	/**
	 * @param exclusionsToSave
	 */
	public void saveDinExclusions(List<DssPharmAuditDinExcl> exclusionsToSave);

	public void deleteDinExclusions(PharmAuditDinExcl pharmAuditDinExcl);
}

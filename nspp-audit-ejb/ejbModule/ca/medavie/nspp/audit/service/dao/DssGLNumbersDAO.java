package ca.medavie.nspp.audit.service.dao;

import java.util.List;

import ca.medavie.nspp.audit.service.data.DssGlNumber;

public interface DssGLNumbersDAO {

	/**
	 * Loads all GL Numbers in the systems
	 * 
	 * @return all GL Numbers currently available.
	 */
	public List<DssGlNumber> getGLNumbers();

	/**
	 * Attempt to load a GL Number record by the specified GL Number. If nothing is found return null
	 * 
	 * @param aGlNumber
	 * @return the GLNumber record that uses the specified GL Number
	 */
	public DssGlNumber getGLNumberByGL(Long aGlNumber);

	/**
	 * Loads multiple GL Numbers records by PK
	 * 
	 * @param aListOfGLNumbers
	 * @return list of matching GL Number records
	 */
	public List<DssGlNumber> getGLNumbersByGL(List<Long> aListOfGLNumbers);

	/**
	 * Saves an existing (Changes) or new GL Number to the system.
	 * 
	 * @param aDssGLNumber
	 *            - GL Number to update or save
	 */
	public void saveGLNumber(DssGlNumber aDssGLNumber);

	/**
	 * Deletes the selected GL Numbers from the system.
	 * 
	 * @param aListOfGLNumbersForDelete
	 *            - GL Numbers to delete
	 */
	public void deleteGLNumbers(List<DssGlNumber> aListOfGLNumbersForDelete);
}

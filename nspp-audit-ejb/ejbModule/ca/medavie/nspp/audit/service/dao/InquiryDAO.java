package ca.medavie.nspp.audit.service.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ca.medavie.nspp.audit.service.data.DssProviderType;
import ca.medavie.nspp.audit.service.data.HealthServiceCode;
import ca.medavie.nspp.audit.service.data.PaymentDetails;
import ca.medavie.nspp.audit.service.data.PeerGroup;
import ca.medavie.nspp.audit.service.data.PeerGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssPeerGroupHSCTotalsMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssPractitionerHSCTotalsMappedEntity;

/**
 * Interface of the Inquiry DAO layer
 */
public interface InquiryDAO {

	/**
	 * @return List of provider type data objects
	 */
	public List<DssProviderType> getDssProviderTypes();

	/**
	 * Load matching practitioner records based on the search criteria passed
	 * 
	 * Data loaded from: dss_med_prov_summary, dss_provider_peer_group, dss_provider_2
	 * 
	 * Query:
	 * 
	 * SELECT dmps.provider_peer_group_id, dppg.provider_peer_group_name, dmps.provider_number, dmps.provider_type,
	 * dp2.organization_name || dp2.last_name || ' ' || dp2.first_name as provider_name, dmps.year_end_date,
	 * dmps.period_id, dmps.shadow_billing_indicator, dp2.birth_date, dp2.gender, dp2.address_line_1,
	 * dp2.address_line_2, dp2.city, dp2.province_code, dp2.country, dp2.postal_code, dmps.number_of_patients,
	 * dmps.number_of_se, dmps.total_units, dmps.total_amount_paid, Round(decode(dmps.number_of_patients, 0, 0,
	 * dmps.number_of_se / dmps.number_of_patients), 2) as services_per_patient, Round(decode(dmps.number_of_patients,
	 * 0, 0, dmps.total_amount_paid / dmps.number_of_patients), 2) as amount_paid_per_patient FROM dss_med_prov_summary
	 * dmps, dss_provider_peer_group dppg, dss_provider_2 dp2 WHERE dmps.provider_peer_group_id =
	 * dppg.provider_peer_group_id and dmps.provider_number = dp2.provider_number and dmps.provider_type =
	 * dp2.provider_type and dppg.year_end_date = dmps.year_end_date and dppg.PROVIDER_PEER_GROUP_TYPE = 'PROVI' ORDER
	 * BY dp2.last_name ASC
	 * 
	 * 
	 * Data returned: (0)PeerGroupId, (1)PeerGroup Name, (2) Practitioner Id, (3) Practitioner Type, (4)Practitioner
	 * Name, (5)Year End Date, (6)Period ID, (7)ShadowBillingIndicator, (8)BirthDate, (9)Gender, (10)Address Line 1,
	 * (11)Address Line 2, (12)City, (13)Province Code, (14)Country, (15)Postal Code, (16)Number Of Patients, (17)Number
	 * Of Services, (18)Total Units, (19)Total Amount Paid, (20)Services Per Patient, (21)Amount Paid Per Patient
	 * 
	 * @param aPractitionerSearchCriteria
	 * @param startRow
	 * @param maxRows
	 * @return list of matching Practitioners
	 */
	public List<Object[]> getSearchedPractitioners(PractitionerSearchCriteria aPractitionerSearchCriteria,
			int startRow, int maxRows);

	/**
	 * Loads the count of all records returned by a search without pagination enabled
	 * 
	 * @param aPractitionerSearchCriteria
	 * @return count of all records without pagination
	 */
	public Integer getSearchedPractitionersCount(PractitionerSearchCriteria aPractitionerSearchCriteria);

	/**
	 * Loads the most recent Opt in/out dates for the matching practitioner Id and type.
	 * 
	 * Data loaded from: dss_provider_opt_history
	 * 
	 * Query:
	 * 
	 * SELECT max(dpoh.opt_in_date) as opt_in_date, max(dpoh.opt_out_date) as opt_out_date FROM dss_provider_opt_history
	 * dpoh WHERE dpoh.provider_number = ? and dpoh.provider_type = ? and dpoh.program_code = 'MC'
	 * 
	 * Data returned: (0)opt in date, (1)opt out date
	 * 
	 * @param aPractitionerNumber
	 * @param aPractitionerType
	 * @return loaded optIn/optOut dates
	 */
	public Object[] getOptDates(Long aPractitionerNumber, String aPractitionerType);

	/**
	 * Retrieve the Practitioner count for the provided PeerGroup ID and YearEndDate. The count will be pulled for
	 * either FeeForService or Shadow practitioners depending on the aShadowBillingIndicator provided
	 * 
	 * Data loaded from: dss_provider_peer_group_xref or dss_shad_prov_peer_group_xref
	 * 
	 * FeeForService Query:
	 * 
	 * SELECT Count(*) as number_of_providers_in_group, Sum(decode(pgx.drop_indicator, 'N', 1, 'Y', 0, 0)) as
	 * providers_used_for_average, Sum(decode(pgx.drop_indicator, 'N', 0, 'Y', 1, 1)) as providers_dropped FROM
	 * dss_provider_peer_group_xref pgx WHERE pgx.provider_peer_group_id = ? and pgx.year_end_date = ?
	 * 
	 * Shadow Query:
	 * 
	 * SELECT Count(*) as number_of_providers_in_group, Sum(decode(pgx.drop_indicator, 'N', 1, 'Y', 0, 0)) as
	 * providers_used_for_average, Sum(decode(pgx.drop_indicator, 'N', 0, 'Y', 1, 1)) as providers_dropped FROM
	 * dss_shad_prov_peer_group_xref pgx WHERE pgx.provider_peer_group_id = ? and pgx.year_end_date = ?;
	 * 
	 * Data returned: (0)Practitioners In Group, (1)Practitioners Used For Average, (2)Practitioners Dropped
	 * 
	 * @param aPeerGroupId
	 * @param aYearEndDate
	 * @param aShadowBillingIndicator
	 * @return loaded practitioner counts
	 */
	public Object[] getPractitionerCounts(Long aPeerGroupId, Date aYearEndDate, String aShadowBillingIndicator);

	/**
	 * Loads the ProfilePeriod with the provided YearEnd. NOTE: This only searches for 'PROVI' type ProfilePeriods
	 * 
	 * Data loaded from: dss_profile_period
	 * 
	 * Query:
	 * 
	 * SELECT min(dpp.period_start_date) as period_start_date, max(dpp.period_end_date) as period_end_date,
	 * min(period_id) as min_period_id, max(period_id) as max_period_id FROM dss_profile_period dpp WHERE
	 * dpp.year_end_date = ? and dpp.period_type = 'PROVI'
	 * 
	 * Data returned: (0)Period Start date, (1)Period End date, (2)Minimum Period ID, (3)Maximum Period ID
	 * 
	 * @param aYearEndDate
	 * @return matching ProfilePeriod details
	 */
	public Object[] getProfilePeriodDetailsByYearEnd(Date aYearEndDate);

	/**
	 * Loads the ProfilePeriod with the provided PeriodID. NOTE: This only searches for 'PROVI' type ProfilePeriods
	 * 
	 * Data loaded from: dss_profile_period
	 * 
	 * Query:
	 * 
	 * SELECT dpp.period_start_date, dpp.period_end_date FROM dss_profile_period dpp WHERE dpp.period_id = ? and
	 * dpp.period_type = 'PROVI'
	 * 
	 * Data returned: (0)Period Start date, (1)Period End date
	 * 
	 * @param aPeriodId
	 * @return matching ProfilePeriod details
	 */
	public Object[] getProfilePeriodDetailsByPeriodID(Long aPeriodId);

	/**
	 * Loads all the Health Service Codes performed by the specified Practitioner
	 * 
	 * Data loaded from: dss_med_prov_dtl_summary, dss_med_group_dtl_summary, dss_health_service_group
	 * 
	 * Query:
	 * 
	 * SELECT dhsg.health_service_group_name, dmpds.health_service_group_id, dmpds.number_of_patients,
	 * dmpds.number_of_se, ROUND(dmpds.total_amount_paid, 0), NVL(ROUND(dmgds.percentage_payment, 1), 0) as
	 * percentage_payment_group, round(decode(dmpds.number_of_patients, 0, 0, dmpds.number_of_se /
	 * dmpds.number_of_patients), 2) as number_of_se_per_patient, round(decode(dmpds.number_of_patients, 0, 0,
	 * dmpds.total_amount_paid / dmpds.number_of_patients), 2) as amount_paid_per_patient,
	 * NVL(dmpds.idx_amount_paid_per_100, 0) as idx_amount_paid_per_100, NVL(dmpds.idx_number_of_se_per_100, 0) as
	 * idx_number_of_se_per_100, NVL(dmpds.idx_number_of_patients, 0) as idx_number_of_patients,
	 * NVL(dmpds.idx_number_of_se, 0) as idx_number_of_se, NVL(dmpds.idx_total_amount_paid, 0) as idx_total_amount_paid,
	 * NVL(dmpds.idx_number_of_se_per_patient, 0) as idx_number_of_se_per_patient,
	 * NVL(dmpds.idx_amount_paid_per_patient, 0) as idx_amount_paid_per_patient, NVL(ROUND(dmpds.percentage_payment, 1),
	 * 0) as percentage_payment, NVL(dmpds.number_of_se_per_100, 0) as number_of_se_per_100,
	 * NVL(dmpds.amount_paid_per_100, 0) as amount_paid_per_100 FROM dss_med_prov_dtl_summary dmpds LEFT OUTER JOIN
	 * dss_med_group_dtl_summary dmgds ON dmpds.provider_peer_group_id = dmgds.provider_peer_group_id AND
	 * dmpds.health_service_group_id = dmgds.health_service_group_id AND dmpds.period_id = dmgds.period_id AND
	 * dmpds.shadow_billing_indicator = dmgds.shadow_billing_indicator AND dmpds.year_end_date = dmgds.year_end_date,
	 * dss_health_service_group dhsg WHERE dmpds.health_service_group_id = dhsg.health_service_group_id AND
	 * dmpds.provider_peer_group_id = :provider_peer_group_id AND dmpds.provider_number = :provider_number AND
	 * dmpds.provider_type = :provider_type AND dmpds.year_end_date = :year_end_date AND dmpds.period_id = :period_id
	 * AND dmpds.shadow_billing_indicator = :shadow_billing_indicator ORDER BY dhsg.health_service_group_name ASC
	 * 
	 * Data returned: (0)HealthService name, (1)HealthService id, (2)Number Of Patients, (3) Number Of Services,
	 * (4)Total Amount Paid, (5)Percentage Payment Index, (6) Num/Services per Patient, (7)Amount Paid Per Patient,
	 * (8)Amount Paid Per 100 Index, (9)Number Of Services Per 100 Index, (10)Number Of Patients Index, (11)Number Of
	 * Services Index, (12)Total Amount Paid Index, (13)Number Of Service Per Patient Index, (14)Amount Paid Per Patient
	 * Index, (15)Percentage Payment, (16)Number Of Services Per 100, (17)Amount Paid Per 100
	 * 
	 * @param aPractitioner
	 * @return list of objects(rows). Each row contains the details of a single Health Service Code
	 */
	public List<Object[]> getPractitionerHealthServiceCodeDetails(Practitioner aPractitioner);

	/**
	 * Loads the totals for the Practitioner HealthServiceCriteria performed.
	 * 
	 * Data loaded from: dss_med_prov_summary
	 * 
	 * Query: ??
	 * 
	 * 
	 * @param aPractitioner
	 * @return DssPractitionerHSCTotalsMappedEntity
	 */
	public DssPractitionerHSCTotalsMappedEntity getPractitionerHealthServiceCodeTotals(Practitioner aPractitioner);

	/**
	 * Load matching PeerGroup records based on the search criteria passed
	 * 
	 * Data load from: dss_provider_peer_group, dss_med_group_summary
	 * 
	 * Query:
	 * 
	 * SELECT dmgs.provider_peer_group_id, dppg.provider_peer_group_name, dmgs.year_end_date, dmgs.period_id,
	 * dmgs.shadow_billing_indicator, dmgs.number_of_patients, dmgs.number_of_se, dmgs.total_units,
	 * dmgs.total_amount_paid, Round(decode(dmgs.number_of_patients, 0, 0, dmgs.number_of_se / dmgs.number_of_patients),
	 * 2) as services_per_patient, Round(decode(dmgs.number_of_patients, 0, 0, dmgs.total_amount_paid /
	 * dmgs.number_of_patients), 2) as amount_paid_per_patient, dmgs.number_of_providers_in_group FROM
	 * dss_provider_peer_group dppg, dss_med_group_summary dmgs WHERE dppg.provider_peer_group_id =
	 * dmgs.provider_peer_group_id and dppg.year_end_date = dmgs.year_end_date
	 * 
	 * Data returned: (0)PeerGroup ID, (1)PeerGroup Name, (2)Year End Date, (3)Period ID, (4)Shadow Billing Indicator,
	 * (5)Number Of Patients, (6)Number Of Services, (7)Total Units, (8)Total Amount Paid, (9)Services Per Patient,
	 * (10)Amount Paid Per Patient
	 * 
	 * @param aPeerGroupSearchCriteria
	 * @param startRow
	 * @param maxRows
	 * @return list of matching PeerGroups
	 */
	public List<Object[]> getSearchedPeerGroups(PeerGroupSearchCriteria aPeerGroupSearchCriteria, int startRow,
			int maxRows);

	/**
	 * Loads the count of all records returned by a search without pagination enabled
	 * 
	 * @param aPeerGroupSearchCriteria
	 * @return count of all records without pagination
	 */
	public Integer getSearchPeerGroupsCount(PeerGroupSearchCriteria aPeerGroupSearchCriteria);

	/**
	 * Loads all the Health Service Codes performed under the specified PeerGroup
	 * 
	 * Data loaded from: dss_med_group_dtl_summary, dss_health_service_group
	 * 
	 * Query:
	 * 
	 * Step 1
	 * 
	 * SELECT a.health_service_group_id, MAX(a.health_service_group_name), MAX(b.number_of_patients),
	 * ROUND(MAX(b.avg_number_of_patients), 0), MAX(b.number_of_se) as number_of_se, ROUND(MAX(b.avg_number_of_se), 0),
	 * ROUND(MAX(b.total_amount_paid), 0), ROUND(MAX(b.avg_total_amount_paid), 0), MAX(b.percentage_payment),
	 * MAX(b.avg_number_of_se_per_patient), MAX(b.avg_amount_paid_per_patient), MAX(b.avg_number_of_se_per_100),
	 * MAX(b.avg_amount_paid_per_100) FROM dss_med_group_dtl_summary b, dss_health_service_group a WHERE
	 * b.provider_peer_group_id = ? AND b.health_service_group_id = a.health_service_group_id AND b.year_end_date = ?
	 * AND b.period_id = ? AND b.shadow_billing_indicator = ? GROUP BY a.health_service_group_id
	 * 
	 * Step 2
	 * 
	 * ??
	 * 
	 * Data returned:
	 * 
	 * Step 1: (0)HealthServiceGroup ID, (1)HealthServiceGroup Name, (2)Number Of Patients, (3)Average Number Of
	 * Patients, (4)Number Of Services, (5)Average Number Of Services, (6)Total Amount Paid, (7)Average Total Amount
	 * Paid, (8)Percentage Payment, (9)Average Number Of Services Per Patient, (10)Average Amount Paid Per Patient,
	 * (11)Average Number Of Services Per 100, (12)Average Amount Paid Per 100
	 * 
	 * Step 2: (0)Number Of Practitioners, (1)Standard Deviation Number Of Patients, (2)Standard Deviation Number Of
	 * Services, (3)Standard Deviation Total Amount Paid, (4)Standard Deviation Number Of Services Per Patient,
	 * (5)Standard Deviation Amount Paid Per Patient, (6)Standard Deviation Number Of Services Per 100, (7)Standard
	 * Deviation Amount Paid Per 100
	 * 
	 * @param aPeerGroup
	 */
	public void getPeerGroupHealthServiceCodeDetails(PeerGroup aPeerGroup);

	/**
	 * Loads the totals for the PeerGroup HealthServiceCriteria performed.
	 * 
	 * Data loaded from: dss_med_prov_summary, dss_med_group_summary
	 * 
	 * FFS uses: dss_provider_peer_group_xref
	 * 
	 * Shadow uses: dss_shad_prov_peer_group_xref
	 * 
	 * Query: ??
	 * 
	 * @param aPeerGroup
	 * @return DssPeerGroupHSCTotalsMappedEntity
	 */
	public DssPeerGroupHSCTotalsMappedEntity getPeerGroupHealthServiceCodeTotals(PeerGroup aPeerGroup);

	/**
	 * Loads all the required data for determining Payment Information for a Practitioner.
	 * 
	 * @param aPractitioner
	 * 
	 * @return payment information details
	 */
	public PaymentDetails getPractitionerPaymentDetails(Practitioner aPractitioner);

	/**
	 * Used in the event that the Practitioner has 0 value on Fee For Service. Loads the sum of TOTAL_AMOUNT_PAID form
	 * DSS_MED_PROV_ACT_SUMMARY
	 * 
	 * Data loaded from: DSS_MED_PROV_ACT_SUMMARY
	 * 
	 * Query:
	 * 
	 * SELECT SUM(dmpas.total_amount_paid) FROM dss_med_prov_act_summary dmpas WHERE dmpas.provider_number = ? AND
	 * dmpas.provider_type = ? AND dmpas.year_end_date = ?
	 * 
	 * Data returned: Total amount paid BigDecimal
	 * 
	 * @param aPractitioner
	 * @return total amount paid
	 */
	public BigDecimal getPractitionerTotalAmountPaid(Practitioner aPractitioner);

	/**
	 * Loads all the required data for determining Payment Information for a PeerGroup.
	 * 
	 * @param aPeerGroup
	 * 
	 * @return payment information details
	 */
	public PaymentDetails getPeerGroupPaymentDetails(PeerGroup aPeerGroup);

	/**
	 * Loads the details for Age Distribution for the provided Practitioner
	 * 
	 * Data loaded from: dss_med_prov_summary_age, dss_provider_peer_group and (dss_provider_peer_group_xref or
	 * dss_provider_peer_group_xref)
	 * 
	 * Query:
	 * 
	 * Group FFS query
	 * 
	 * SELECT dmpsa.age_rank, ROUND(DECODE(?1, 0, 0, (SUM(dmpsa.number_of_patients) / ?1) * 100), 1) as
	 * number_of_patients FROM dss_med_prov_summary_age dmpsa, dss_provider_peer_group dppg,
	 * dss_provider_peer_group_xref gxref WHERE gxref.provider_peer_group_id = ?2 and gxref.provider_number > 0 and
	 * gxref.provider_type > ' ' and gxref.year_end_date = ?3 and gxref.drop_indicator = 'N' and
	 * dppg.provider_peer_group_id = gxref.provider_peer_group_id and dppg.year_end_date = gxref.year_end_date and
	 * dppg.provider_peer_group_type = 'PROVI' and dmpsa.provider_number = gxref.provider_number and dmpsa.provider_type
	 * = gxref.provider_type and dmpsa.year_end_date = gxref.year_end_date and dmpsa.period_id = ?4 and
	 * dmpsa.shadow_billing_indicator = ?5 GROUP BY dmpsa.age_rank
	 * 
	 * For Shadow replace dss_provider_peer_group_xref with dss_shad_prov_peer_group_xref
	 * 
	 * Practitioner FFS query
	 * 
	 * SELECT dmpsa.age_rank, round(decode(?1, 0, 0, (sum(dmpsa.number_of_patients) / ?1) * 100), 1) as
	 * number_of_patients FROM dss_med_prov_summary_age dmpsa, dss_provider_peer_group_xref gxref WHERE
	 * dmpsa.provider_number = gxref.provider_number and dmpsa.provider_type = gxref.provider_type and
	 * dmpsa.year_end_date = gxref.year_end_date and ((dmpsa.provider_number = ?2) and (dmpsa.provider_type = ?3) and
	 * (dmpsa.year_end_date = ?4) and (dmpsa.period_id = ?5) and (dmpsa.shadow_billing_indicator = ?6) and
	 * (gxref.provider_peer_group_id = ?7)) GROUP BY dmpsa.age_rank
	 * 
	 * For Shadow replace dss_provider_peer_group_xref with dss_shad_prov_peer_group_xref
	 * 
	 * Data return: Age Distribution objects in Map<Integer, AgeDistribution>
	 * 
	 * @param aPractitioner
	 */
	public void getPractitionerAgeDistributionDetails(Practitioner aPractitioner);

	/**
	 * Load all HSC found under the provided HealthServiceCode/Group
	 * 
	 * Date loaded from: DSS_HEALTH_SERVICE_CODE, DSS_HEALTH_SERVICE, DSS_MED_PROV_ACT_DTL_SUMMARY
	 * 
	 * Query:
	 * 
	 * SELECT dmpads.NUMBER_OF_PATIENTS, dmpads.NUMBER_OF_SE, dmpads.TOTAL_UNITS, dmpads.TOTAL_AMOUNT_PAID,
	 * dmpads.NUMBER_OF_SE_PER_100, dmpads.AMOUNT_PAID_PER_100, dhsc.EFFECTIVE_FROM_DATE, dhsc.EFFECTIVE_TO_DATE,
	 * dhsc.DESCRIPTION, rtrim(dhsc.health_service_code) || rtrim(dhsc.qualifier_code) as health_service_code,
	 * dhs.MODIFIERS, dhs.IMPLICIT_MODIFIERS FROM DSS_HEALTH_SERVICE_CODE dhsc, DSS_HEALTH_SERVICE dhs,
	 * DSS_MED_PROV_ACT_DTL_SUMMARY dmpads WHERE dmpads.HEALTH_SERVICE_ID = dhs.HEALTH_SERVICE_ID and
	 * dhs.HEALTH_SERVICE_CODE = dhsc.HEALTH_SERVICE_CODE and dhs.QUALIFIER_CODE = dhsc.QUALIFIER_CODE and
	 * ((dmpads.provider_peer_group_id = &provider_peer_group_id) and (dmpads.provider_number = &provider_number) and
	 * (dmpads.provider_type = &provider_type) and (dmpads.year_end_date = &year_end_date) and (dmpads.period_id =
	 * &period_id) and (dmpads.shadow_billing_indicator = &shadow_billing_indicator) and (dmpads.health_service_group_id
	 * = &health_service_group_id) and (dhs.program_code = 'MC') and (dhsc.effective_to_date >= &period_start_date) and
	 * (dhsc.effective_from_date <= &period_end_date)) ORDER BY 10 ASC
	 * 
	 * Date return: (0)Number of Patients, (1)Number of Services, (2)Total Units, (3)Total Amount Paid, (4)Number of
	 * Service Per 100, (5)Amount Paid Per 100, (6)Effective From Date, (7)Effective To Date, (8)Description, (9)Health
	 * Service Code, (10)Modifiers, (11) Implicit Modifiers
	 * 
	 * @param aPractitioner
	 * @param aHealthServiceCode
	 * @return list of Objects that contain the data of HSC found under the provided HSC/Group
	 */
	public List<Object[]> getHealthServiceDrillDownDetails(Practitioner aPractitioner,
			HealthServiceCode aHealthServiceCode);

}

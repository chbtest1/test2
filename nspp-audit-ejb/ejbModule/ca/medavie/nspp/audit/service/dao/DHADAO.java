package ca.medavie.nspp.audit.service.dao;

import java.util.List;

import ca.medavie.nspp.audit.service.ServicePersistenceException;
import ca.medavie.nspp.audit.service.data.DistrictHealthAuthority;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssProviderDhaMappedEntity;

/**
 * Interface of the DHA(District Health Authority) DAO layer
 */
public interface DHADAO {
	/**
	 * Loads all DssProviderDhaMappedEntity records
	 * 
	 * Data loaded from: dss_provider_dha, dss_provider_2, dss_business_arrangement
	 * 
	 * Query:
	 * 
	 * SELECT dha.pdha_id, dha.provider_number, dha.provider_type, dp.first_name || ' ' || dp.last_name AS
	 * provider_name, dha.bus_arr_number, ba.bus_arr_description, dha.contract_town_name, dha.health_region_code AS
	 * zone_id, (SELECT cc.code_table_entry_description FROM dss_code_table_entry cc WHERE dha.health_region_code =
	 * cc.code_table_entry AND cc.code_table_number = 135) AS zone_name, dha.dha_code, (SELECT
	 * cc.code_table_entry_description FROM dss_code_table_entry cc WHERE dha.dha_code = cc.code_table_entry AND
	 * cc.code_table_number = 157) AS dha_desc, dha.emp_type, dha.fte_value, dha.modified_by, dha.last_modified FROM
	 * DSS_PROVIDER_DHA dha, dss_provider_2 dp, dss_business_arrangement ba WHERE dha.provider_number =
	 * dp.provider_number AND dha.provider_type = dp.provider_type AND dha.bus_arr_number = ba.bus_arr_number AND
	 * dha.provider_number = ba.provider_number AND dha.provider_type = ba.provider_type ORDER BY dha.bus_arr_number
	 * DESC
	 * 
	 * @return list of DssProviderDhaMappedEntity records
	 */
	public List<DssProviderDhaMappedEntity> getDHAs();

	/**
	 * Loads the matching Practitioner data using the user search criteria
	 * 
	 * Data loaded from: dss_provider_2, dss_business_arrangement
	 * 
	 * Query:
	 * 
	 * SELECT ba.provider_number, ba.provider_type, d.first_name || ' ' || d.last_name AS provider_name,
	 * ba.bus_arr_number, ba.bus_arr_description FROM dss_provider_2 d, dss_business_arrangement ba WHERE
	 * d.provider_number = ba.provider_number AND d.provider_type = ba.provider_type ORDER BY ba.provider_number DESC
	 * 
	 * Data returned: (0)Practitioner Id, (1)Practitioner Type, (2)Practitioner Name, (3)Business Arrangement Number,
	 * (4)Business Arrangement Description
	 * 
	 * @param aSearchCriteria
	 * @return list of object arrays that contain matching Practitioner data
	 */
	public List<Object[]> getSearchedPractitioners(PractitionerSearchCriteria aSearchCriteria);

	/**
	 * Save the changes made to a DHA record to the DB.
	 * 
	 * @param aDha
	 *            - the DHA record that was changed
	 */
	public void saveDHAChanges(DistrictHealthAuthority aDha);

	/**
	 * Deletes the provided DHA records from the database
	 * 
	 * @param aListOfDhasToDelete
	 *            - DHAs to be deleted
	 */
	public void deleteDHAs(List<DistrictHealthAuthority> aListOfDhasToDelete);

	/**
	 * Loads the Zone Name
	 * 
	 * Data loaded from: dss_code_table_entry
	 * 
	 * Query:
	 * 
	 * SELECT cc.code_table_entry_description FROM dss_code_table_entry cc WHERE cc.code_table_number = 135 AND
	 * cc.code_table_entry = ?
	 * 
	 * Data loaded: zoneName
	 * 
	 * @param aZoneId
	 *            - zoneId that matches the required ZoneName
	 * @return - found zone name
	 */
	public String getZoneName(String aZoneId);

	/**
	 * Loads the DHA Description
	 * 
	 * Data loaded from: dss_code_table_entry
	 * 
	 * Query:
	 * 
	 * SELECT cc.code_table_entry_description FROM dss_code_table_entry cc WHERE cc.code_table_number = 157 AND
	 * cc.code_table_entry = ?
	 * 
	 * Data loaded: DHA Description
	 * 
	 * @param aDhaCode
	 *            - dhaCode that matches the required DHA Description
	 * @return - found DHA description
	 */
	public String getDhaDescription(String aDhaCode);

	/**
	 * Loads the available ZoneIds along with their description for the given town
	 * 
	 * Data loaded from: dss_placename, dss_code_table_entry
	 * 
	 * Query:
	 * 
	 * SELECT DISTINCT p.health_region_code, cc.code_table_entry_description FROM dss_placename p, dss_code_table_entry
	 * cc WHERE p.placename = ? AND cc.code_table_number = 135 AND cc.code_table_entry = p.health_region_code ORDER BY
	 * p.health_region_code ASC
	 * 
	 * Data loaded: (0)Zone ID, (1)Zone Description
	 * 
	 * @param aTownName
	 * @return object array containing zoneId/description
	 */
	public List<Object[]> getZoneIds(String aTownName);

	/**
	 * Loads the available DhaCodes along with their description for the given town and zone
	 * 
	 * Data loaded from: dss_placename, dss_code_table_entry
	 * 
	 * Query:
	 * 
	 * SELECT DISTINCT p.dha_code, cc.code_table_entry_description FROM dss_placename p, dss_code_table_entry cc WHERE
	 * p.placename = ? AND p.Health_Region_Code = ? AND cc.code_table_number = 157 AND cc.code_table_entry = p.dha_code
	 * 
	 * Data loaded: (0)DHA Code, (1)DHA Description
	 * 
	 * @param aTownName
	 * @param aZoneId
	 * @return object array containing dhaCode/name
	 * @throws ServicePersistenceException
	 */
	public List<Object[]> getDhaCodes(String aTownName, String aZoneId) throws ServicePersistenceException;
}

package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the DSS_TOWN database table.
 * 
 */
@Entity
@Table(name="DSS_TOWN")
public class DssTown implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="TOWN_CODE")
	private long townCode;

	@Column(name="COUNTY_CODE")
	private BigDecimal countyCode;

	@Column(name="MUNICIPALITY_CODE")
	private BigDecimal municipalityCode;

	@Column(name="TOWN_NAME")
	private String townName;

	public DssTown() {
	}

	public long getTownCode() {
		return this.townCode;
	}

	public void setTownCode(long townCode) {
		this.townCode = townCode;
	}

	public BigDecimal getCountyCode() {
		return this.countyCode;
	}

	public void setCountyCode(BigDecimal countyCode) {
		this.countyCode = countyCode;
	}

	public BigDecimal getMunicipalityCode() {
		return this.municipalityCode;
	}

	public void setMunicipalityCode(BigDecimal municipalityCode) {
		this.municipalityCode = municipalityCode;
	}

	public String getTownName() {
		return this.townName;
	}

	public void setTownName(String townName) {
		this.townName = townName;
	}

}
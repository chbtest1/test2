package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the DSS_CODE_TABLE_ENTRY database table.
 * 
 */
@Entity
@Table(name = "DSS_CODE_TABLE_ENTRY")
@NamedQueries({
		@NamedQuery(name = QueryConstants.DSS_CODE_TABLE_ENTRY_GET_AUDIT_RX_TYPES, query = "select dcte from DssCodeTableEntry dcte where dcte.id.codeTableNumber = 203 and dcte.id.codeTableEntry in (1,2) "),
		@NamedQuery(name = QueryConstants.DSS_CODE_TABLE_ENTRY_GET_NON_AUDIT_RX_TYPES, query = "select dcte from DssCodeTableEntry dcte where dcte.id.codeTableNumber =203"),
		@NamedQuery(name = QueryConstants.DSS_HEALTH_SERVICE_CODE_HEALTH_AUDIT_SOURCE_MED_QUERY, query = "select dcte.codeTableEntryDescription from DssCodeTableEntry dcte where dcte.id.codeTableNumber =201"),
		@NamedQuery(name = QueryConstants.DSS_HEALTH_SERVICE_CODE_HEALTH_AUDIT_SOURCE_PHARM_QUERY, query = "select dcte.codeTableEntryDescription from DssCodeTableEntry dcte where dcte.id.codeTableNumber =202"),
		@NamedQuery(name = QueryConstants.DSS_HEALTH_SERVICE_CODE_HEALTH_PHARM_TYPE_QUERY, query = "select dcte.codeTableEntryDescription from DssCodeTableEntry dcte where dcte.id.codeTableNumber =205"),
		@NamedQuery(name = QueryConstants.DSS_UNIT_VALUE_LEVEL_QUERY, query = "select dcte.id.codeTableEntry from DssCodeTableEntry dcte where dcte.id.codeTableNumber =161"),
		@NamedQuery(name = QueryConstants.DSS_UNIT_VALUE_DESC_QUERY, query = "select dcte from DssCodeTableEntry dcte where dcte.id.codeTableNumber =156")

})
public class DssCodeTableEntry implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DssCodeTableEntryPK id;

	@Column(name = "CODE_TABLE_ENTRY_DESCRIPTION")
	private String codeTableEntryDescription;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	public DssCodeTableEntry() {
	}

	public DssCodeTableEntryPK getId() {
		return this.id;
	}

	public void setId(DssCodeTableEntryPK id) {
		this.id = id;
	}

	public String getCodeTableEntryDescription() {
		return this.codeTableEntryDescription;
	}

	public void setCodeTableEntryDescription(String codeTableEntryDescription) {
		this.codeTableEntryDescription = codeTableEntryDescription;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

}
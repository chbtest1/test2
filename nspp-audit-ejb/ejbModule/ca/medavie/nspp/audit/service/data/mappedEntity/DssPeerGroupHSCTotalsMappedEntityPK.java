package ca.medavie.nspp.audit.service.data.mappedEntity;

import java.math.BigDecimal;

import javax.persistence.Embeddable;

@Embeddable
public class DssPeerGroupHSCTotalsMappedEntityPK {

	private BigDecimal number_of_providers;
	private BigDecimal number_of_patients;
	private BigDecimal avg_number_of_patients;
	private BigDecimal std_dev_number_of_patients;
	private BigDecimal number_of_se;
	private BigDecimal avg_number_of_se;
	private BigDecimal std_dev_number_of_se;
	private BigDecimal total_amount_paid;
	private BigDecimal avg_total_amount_paid;
	private BigDecimal std_dev_total_amount_paid;
	private BigDecimal percentage_payment;
	private BigDecimal avg_number_of_se_per_patient;
	private BigDecimal std_dev_number_of_se_per_pat;
	private BigDecimal avg_amount_paid_per_patient;
	private BigDecimal std_dev_amount_paid_per_pat;
	private BigDecimal avg_number_of_se_per_100;
	private BigDecimal std_dev_number_of_se_per_100;
	private BigDecimal avg_amount_paid_per_100;
	private BigDecimal std_dev_amount_paid_per_100;

	/**
	 * @return the number_of_providers
	 */
	public BigDecimal getNumber_of_providers() {
		return number_of_providers;
	}

	/**
	 * @param number_of_providers
	 *            the number_of_providers to set
	 */
	public void setNumber_of_providers(BigDecimal number_of_providers) {
		this.number_of_providers = number_of_providers;
	}

	/**
	 * @return the number_of_patients
	 */
	public BigDecimal getNumber_of_patients() {
		return number_of_patients;
	}

	/**
	 * @param number_of_patients
	 *            the number_of_patients to set
	 */
	public void setNumber_of_patients(BigDecimal number_of_patients) {
		this.number_of_patients = number_of_patients;
	}

	/**
	 * @return the avg_number_of_patients
	 */
	public BigDecimal getAvg_number_of_patients() {
		return avg_number_of_patients;
	}

	/**
	 * @param avg_number_of_patients
	 *            the avg_number_of_patients to set
	 */
	public void setAvg_number_of_patients(BigDecimal avg_number_of_patients) {
		this.avg_number_of_patients = avg_number_of_patients;
	}

	/**
	 * @return the std_dev_number_of_patients
	 */
	public BigDecimal getStd_dev_number_of_patients() {
		return std_dev_number_of_patients;
	}

	/**
	 * @param std_dev_number_of_patients
	 *            the std_dev_number_of_patients to set
	 */
	public void setStd_dev_number_of_patients(BigDecimal std_dev_number_of_patients) {
		this.std_dev_number_of_patients = std_dev_number_of_patients;
	}

	/**
	 * @return the number_of_se
	 */
	public BigDecimal getNumber_of_se() {
		return number_of_se;
	}

	/**
	 * @param number_of_se
	 *            the number_of_se to set
	 */
	public void setNumber_of_se(BigDecimal number_of_se) {
		this.number_of_se = number_of_se;
	}

	/**
	 * @return the avg_number_of_se
	 */
	public BigDecimal getAvg_number_of_se() {
		return avg_number_of_se;
	}

	/**
	 * @param avg_number_of_se
	 *            the avg_number_of_se to set
	 */
	public void setAvg_number_of_se(BigDecimal avg_number_of_se) {
		this.avg_number_of_se = avg_number_of_se;
	}

	/**
	 * @return the std_dev_number_of_se
	 */
	public BigDecimal getStd_dev_number_of_se() {
		return std_dev_number_of_se;
	}

	/**
	 * @param std_dev_number_of_se
	 *            the std_dev_number_of_se to set
	 */
	public void setStd_dev_number_of_se(BigDecimal std_dev_number_of_se) {
		this.std_dev_number_of_se = std_dev_number_of_se;
	}

	/**
	 * @return the total_amount_paid
	 */
	public BigDecimal getTotal_amount_paid() {
		return total_amount_paid;
	}

	/**
	 * @param total_amount_paid
	 *            the total_amount_paid to set
	 */
	public void setTotal_amount_paid(BigDecimal total_amount_paid) {
		this.total_amount_paid = total_amount_paid;
	}

	/**
	 * @return the avg_total_amount_paid
	 */
	public BigDecimal getAvg_total_amount_paid() {
		return avg_total_amount_paid;
	}

	/**
	 * @param avg_total_amount_paid
	 *            the avg_total_amount_paid to set
	 */
	public void setAvg_total_amount_paid(BigDecimal avg_total_amount_paid) {
		this.avg_total_amount_paid = avg_total_amount_paid;
	}

	/**
	 * @return the std_dev_total_amount_paid
	 */
	public BigDecimal getStd_dev_total_amount_paid() {
		return std_dev_total_amount_paid;
	}

	/**
	 * @param std_dev_total_amount_paid
	 *            the std_dev_total_amount_paid to set
	 */
	public void setStd_dev_total_amount_paid(BigDecimal std_dev_total_amount_paid) {
		this.std_dev_total_amount_paid = std_dev_total_amount_paid;
	}

	/**
	 * @return the percentage_payment
	 */
	public BigDecimal getPercentage_payment() {
		return percentage_payment;
	}

	/**
	 * @param percentage_payment
	 *            the percentage_payment to set
	 */
	public void setPercentage_payment(BigDecimal percentage_payment) {
		this.percentage_payment = percentage_payment;
	}

	/**
	 * @return the avg_number_of_se_per_patient
	 */
	public BigDecimal getAvg_number_of_se_per_patient() {
		return avg_number_of_se_per_patient;
	}

	/**
	 * @param avg_number_of_se_per_patient
	 *            the avg_number_of_se_per_patient to set
	 */
	public void setAvg_number_of_se_per_patient(BigDecimal avg_number_of_se_per_patient) {
		this.avg_number_of_se_per_patient = avg_number_of_se_per_patient;
	}

	/**
	 * @return the std_dev_number_of_se_per_pat
	 */
	public BigDecimal getStd_dev_number_of_se_per_pat() {
		return std_dev_number_of_se_per_pat;
	}

	/**
	 * @param std_dev_number_of_se_per_pat
	 *            the std_dev_number_of_se_per_pat to set
	 */
	public void setStd_dev_number_of_se_per_pat(BigDecimal std_dev_number_of_se_per_pat) {
		this.std_dev_number_of_se_per_pat = std_dev_number_of_se_per_pat;
	}

	/**
	 * @return the avg_amount_paid_per_patient
	 */
	public BigDecimal getAvg_amount_paid_per_patient() {
		return avg_amount_paid_per_patient;
	}

	/**
	 * @param avg_amount_paid_per_patient
	 *            the avg_amount_paid_per_patient to set
	 */
	public void setAvg_amount_paid_per_patient(BigDecimal avg_amount_paid_per_patient) {
		this.avg_amount_paid_per_patient = avg_amount_paid_per_patient;
	}

	/**
	 * @return the std_dev_amount_paid_per_pat
	 */
	public BigDecimal getStd_dev_amount_paid_per_pat() {
		return std_dev_amount_paid_per_pat;
	}

	/**
	 * @param std_dev_amount_paid_per_pat
	 *            the std_dev_amount_paid_per_pat to set
	 */
	public void setStd_dev_amount_paid_per_pat(BigDecimal std_dev_amount_paid_per_pat) {
		this.std_dev_amount_paid_per_pat = std_dev_amount_paid_per_pat;
	}

	/**
	 * @return the avg_number_of_se_per_100
	 */
	public BigDecimal getAvg_number_of_se_per_100() {
		return avg_number_of_se_per_100;
	}

	/**
	 * @param avg_number_of_se_per_100
	 *            the avg_number_of_se_per_100 to set
	 */
	public void setAvg_number_of_se_per_100(BigDecimal avg_number_of_se_per_100) {
		this.avg_number_of_se_per_100 = avg_number_of_se_per_100;
	}

	/**
	 * @return the std_dev_number_of_se_per_100
	 */
	public BigDecimal getStd_dev_number_of_se_per_100() {
		return std_dev_number_of_se_per_100;
	}

	/**
	 * @param std_dev_number_of_se_per_100
	 *            the std_dev_number_of_se_per_100 to set
	 */
	public void setStd_dev_number_of_se_per_100(BigDecimal std_dev_number_of_se_per_100) {
		this.std_dev_number_of_se_per_100 = std_dev_number_of_se_per_100;
	}

	/**
	 * @return the avg_amount_paid_per_100
	 */
	public BigDecimal getAvg_amount_paid_per_100() {
		return avg_amount_paid_per_100;
	}

	/**
	 * @param avg_amount_paid_per_100
	 *            the avg_amount_paid_per_100 to set
	 */
	public void setAvg_amount_paid_per_100(BigDecimal avg_amount_paid_per_100) {
		this.avg_amount_paid_per_100 = avg_amount_paid_per_100;
	}

	/**
	 * @return the std_dev_amount_paid_per_100
	 */
	public BigDecimal getStd_dev_amount_paid_per_100() {
		return std_dev_amount_paid_per_100;
	}

	/**
	 * @param std_dev_amount_paid_per_100
	 *            the std_dev_amount_paid_per_100 to set
	 */
	public void setStd_dev_amount_paid_per_100(BigDecimal std_dev_amount_paid_per_100) {
		this.std_dev_amount_paid_per_100 = std_dev_amount_paid_per_100;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((avg_amount_paid_per_100 == null) ? 0 : avg_amount_paid_per_100.hashCode());
		result = prime * result + ((avg_amount_paid_per_patient == null) ? 0 : avg_amount_paid_per_patient.hashCode());
		result = prime * result + ((avg_number_of_patients == null) ? 0 : avg_number_of_patients.hashCode());
		result = prime * result + ((avg_number_of_se == null) ? 0 : avg_number_of_se.hashCode());
		result = prime * result + ((avg_number_of_se_per_100 == null) ? 0 : avg_number_of_se_per_100.hashCode());
		result = prime * result
				+ ((avg_number_of_se_per_patient == null) ? 0 : avg_number_of_se_per_patient.hashCode());
		result = prime * result + ((avg_total_amount_paid == null) ? 0 : avg_total_amount_paid.hashCode());
		result = prime * result + ((number_of_patients == null) ? 0 : number_of_patients.hashCode());
		result = prime * result + ((number_of_providers == null) ? 0 : number_of_providers.hashCode());
		result = prime * result + ((number_of_se == null) ? 0 : number_of_se.hashCode());
		result = prime * result + ((percentage_payment == null) ? 0 : percentage_payment.hashCode());
		result = prime * result + ((std_dev_amount_paid_per_100 == null) ? 0 : std_dev_amount_paid_per_100.hashCode());
		result = prime * result + ((std_dev_amount_paid_per_pat == null) ? 0 : std_dev_amount_paid_per_pat.hashCode());
		result = prime * result + ((std_dev_number_of_patients == null) ? 0 : std_dev_number_of_patients.hashCode());
		result = prime * result + ((std_dev_number_of_se == null) ? 0 : std_dev_number_of_se.hashCode());
		result = prime * result
				+ ((std_dev_number_of_se_per_100 == null) ? 0 : std_dev_number_of_se_per_100.hashCode());
		result = prime * result
				+ ((std_dev_number_of_se_per_pat == null) ? 0 : std_dev_number_of_se_per_pat.hashCode());
		result = prime * result + ((std_dev_total_amount_paid == null) ? 0 : std_dev_total_amount_paid.hashCode());
		result = prime * result + ((total_amount_paid == null) ? 0 : total_amount_paid.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DssPeerGroupHSCTotalsMappedEntityPK other = (DssPeerGroupHSCTotalsMappedEntityPK) obj;
		if (avg_amount_paid_per_100 == null) {
			if (other.avg_amount_paid_per_100 != null)
				return false;
		} else if (!avg_amount_paid_per_100.equals(other.avg_amount_paid_per_100))
			return false;
		if (avg_amount_paid_per_patient == null) {
			if (other.avg_amount_paid_per_patient != null)
				return false;
		} else if (!avg_amount_paid_per_patient.equals(other.avg_amount_paid_per_patient))
			return false;
		if (avg_number_of_patients == null) {
			if (other.avg_number_of_patients != null)
				return false;
		} else if (!avg_number_of_patients.equals(other.avg_number_of_patients))
			return false;
		if (avg_number_of_se == null) {
			if (other.avg_number_of_se != null)
				return false;
		} else if (!avg_number_of_se.equals(other.avg_number_of_se))
			return false;
		if (avg_number_of_se_per_100 == null) {
			if (other.avg_number_of_se_per_100 != null)
				return false;
		} else if (!avg_number_of_se_per_100.equals(other.avg_number_of_se_per_100))
			return false;
		if (avg_number_of_se_per_patient == null) {
			if (other.avg_number_of_se_per_patient != null)
				return false;
		} else if (!avg_number_of_se_per_patient.equals(other.avg_number_of_se_per_patient))
			return false;
		if (avg_total_amount_paid == null) {
			if (other.avg_total_amount_paid != null)
				return false;
		} else if (!avg_total_amount_paid.equals(other.avg_total_amount_paid))
			return false;
		if (number_of_patients == null) {
			if (other.number_of_patients != null)
				return false;
		} else if (!number_of_patients.equals(other.number_of_patients))
			return false;
		if (number_of_providers == null) {
			if (other.number_of_providers != null)
				return false;
		} else if (!number_of_providers.equals(other.number_of_providers))
			return false;
		if (number_of_se == null) {
			if (other.number_of_se != null)
				return false;
		} else if (!number_of_se.equals(other.number_of_se))
			return false;
		if (percentage_payment == null) {
			if (other.percentage_payment != null)
				return false;
		} else if (!percentage_payment.equals(other.percentage_payment))
			return false;
		if (std_dev_amount_paid_per_100 == null) {
			if (other.std_dev_amount_paid_per_100 != null)
				return false;
		} else if (!std_dev_amount_paid_per_100.equals(other.std_dev_amount_paid_per_100))
			return false;
		if (std_dev_amount_paid_per_pat == null) {
			if (other.std_dev_amount_paid_per_pat != null)
				return false;
		} else if (!std_dev_amount_paid_per_pat.equals(other.std_dev_amount_paid_per_pat))
			return false;
		if (std_dev_number_of_patients == null) {
			if (other.std_dev_number_of_patients != null)
				return false;
		} else if (!std_dev_number_of_patients.equals(other.std_dev_number_of_patients))
			return false;
		if (std_dev_number_of_se == null) {
			if (other.std_dev_number_of_se != null)
				return false;
		} else if (!std_dev_number_of_se.equals(other.std_dev_number_of_se))
			return false;
		if (std_dev_number_of_se_per_100 == null) {
			if (other.std_dev_number_of_se_per_100 != null)
				return false;
		} else if (!std_dev_number_of_se_per_100.equals(other.std_dev_number_of_se_per_100))
			return false;
		if (std_dev_number_of_se_per_pat == null) {
			if (other.std_dev_number_of_se_per_pat != null)
				return false;
		} else if (!std_dev_number_of_se_per_pat.equals(other.std_dev_number_of_se_per_pat))
			return false;
		if (std_dev_total_amount_paid == null) {
			if (other.std_dev_total_amount_paid != null)
				return false;
		} else if (!std_dev_total_amount_paid.equals(other.std_dev_total_amount_paid))
			return false;
		if (total_amount_paid == null) {
			if (other.total_amount_paid != null)
				return false;
		} else if (!total_amount_paid.equals(other.total_amount_paid))
			return false;
		return true;
	}
}

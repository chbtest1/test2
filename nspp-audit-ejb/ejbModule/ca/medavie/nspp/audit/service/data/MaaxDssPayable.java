package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the MAAX_DSS_PAYABLE database table.
 * 
 */
@Entity
@Table(name="MAAX_DSS_PAYABLE")
public class MaaxDssPayable implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CREATED_DATE_TIME")
	private Timestamp createdDateTime;

	@Column(name="CREATED_USER")
	private String createdUser;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_DATE")
	private Date effectiveDate;

	@Column(name="EXT_PAYEE_CLIENT_ID")
	private BigDecimal extPayeeClientId;

	@Column(name="EXT_PMT_CHEQUE_NBR")
	private String extPmtChequeNbr;

	@Column(name="EXT_PMT_CONSOLIDATION_NUMBER")
	private BigDecimal extPmtConsolidationNumber;

	@Temporal(TemporalType.DATE)
	@Column(name="EXT_PMT_DATE")
	private Date extPmtDate;

	@Column(name="EXT_PMT_METHOD")
	private BigDecimal extPmtMethod;

	@Column(name="EXT_PMT_REQUISITION_NBR")
	private String extPmtRequisitionNbr;

	@Column(name="EXT_PMT_RUN_ID")
	private BigDecimal extPmtRunId;

	@Column(name="MODIFIED_DATE_TIME")
	private Timestamp modifiedDateTime;

	@Column(name="MODIFIED_USER")
	private String modifiedUser;

	@Column(name="PAYABLE_AMOUNT")
	private BigDecimal payableAmount;

	@Column(name="PAYABLE_AMOUNT_UNUSED")
	private BigDecimal payableAmountUnused;

	@Column(name="PAYABLE_ID")
	private BigDecimal payableId;

	@Column(name="PAYEE_BOOK_OF_BUSINESS_ID")
	private BigDecimal payeeBookOfBusinessId;

	@Column(name="PAYEE_CODE")
	private String payeeCode;

	@Column(name="PAYEE_HOSTED_CARRIER_ID")
	private BigDecimal payeeHostedCarrierId;

	@Column(name="PAYEE_TYPE")
	private String payeeType;

	@Column(name="SOURCE_TYPE")
	private BigDecimal sourceType;

	private BigDecimal status;

	@Column(name="TRANSACTION_CODE")
	private String transactionCode;

	@Column(name="TRANSACTION_TYPE")
	private BigDecimal transactionType;

	@Column(name="VERSION_NO")
	private BigDecimal versionNo;

	public MaaxDssPayable() {
	}

	public Timestamp getCreatedDateTime() {
		return this.createdDateTime;
	}

	public void setCreatedDateTime(Timestamp createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public String getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public BigDecimal getExtPayeeClientId() {
		return this.extPayeeClientId;
	}

	public void setExtPayeeClientId(BigDecimal extPayeeClientId) {
		this.extPayeeClientId = extPayeeClientId;
	}

	public String getExtPmtChequeNbr() {
		return this.extPmtChequeNbr;
	}

	public void setExtPmtChequeNbr(String extPmtChequeNbr) {
		this.extPmtChequeNbr = extPmtChequeNbr;
	}

	public BigDecimal getExtPmtConsolidationNumber() {
		return this.extPmtConsolidationNumber;
	}

	public void setExtPmtConsolidationNumber(BigDecimal extPmtConsolidationNumber) {
		this.extPmtConsolidationNumber = extPmtConsolidationNumber;
	}

	public Date getExtPmtDate() {
		return this.extPmtDate;
	}

	public void setExtPmtDate(Date extPmtDate) {
		this.extPmtDate = extPmtDate;
	}

	public BigDecimal getExtPmtMethod() {
		return this.extPmtMethod;
	}

	public void setExtPmtMethod(BigDecimal extPmtMethod) {
		this.extPmtMethod = extPmtMethod;
	}

	public String getExtPmtRequisitionNbr() {
		return this.extPmtRequisitionNbr;
	}

	public void setExtPmtRequisitionNbr(String extPmtRequisitionNbr) {
		this.extPmtRequisitionNbr = extPmtRequisitionNbr;
	}

	public BigDecimal getExtPmtRunId() {
		return this.extPmtRunId;
	}

	public void setExtPmtRunId(BigDecimal extPmtRunId) {
		this.extPmtRunId = extPmtRunId;
	}

	public Timestamp getModifiedDateTime() {
		return this.modifiedDateTime;
	}

	public void setModifiedDateTime(Timestamp modifiedDateTime) {
		this.modifiedDateTime = modifiedDateTime;
	}

	public String getModifiedUser() {
		return this.modifiedUser;
	}

	public void setModifiedUser(String modifiedUser) {
		this.modifiedUser = modifiedUser;
	}

	public BigDecimal getPayableAmount() {
		return this.payableAmount;
	}

	public void setPayableAmount(BigDecimal payableAmount) {
		this.payableAmount = payableAmount;
	}

	public BigDecimal getPayableAmountUnused() {
		return this.payableAmountUnused;
	}

	public void setPayableAmountUnused(BigDecimal payableAmountUnused) {
		this.payableAmountUnused = payableAmountUnused;
	}

	public BigDecimal getPayableId() {
		return this.payableId;
	}

	public void setPayableId(BigDecimal payableId) {
		this.payableId = payableId;
	}

	public BigDecimal getPayeeBookOfBusinessId() {
		return this.payeeBookOfBusinessId;
	}

	public void setPayeeBookOfBusinessId(BigDecimal payeeBookOfBusinessId) {
		this.payeeBookOfBusinessId = payeeBookOfBusinessId;
	}

	public String getPayeeCode() {
		return this.payeeCode;
	}

	public void setPayeeCode(String payeeCode) {
		this.payeeCode = payeeCode;
	}

	public BigDecimal getPayeeHostedCarrierId() {
		return this.payeeHostedCarrierId;
	}

	public void setPayeeHostedCarrierId(BigDecimal payeeHostedCarrierId) {
		this.payeeHostedCarrierId = payeeHostedCarrierId;
	}

	public String getPayeeType() {
		return this.payeeType;
	}

	public void setPayeeType(String payeeType) {
		this.payeeType = payeeType;
	}

	public BigDecimal getSourceType() {
		return this.sourceType;
	}

	public void setSourceType(BigDecimal sourceType) {
		this.sourceType = sourceType;
	}

	public BigDecimal getStatus() {
		return this.status;
	}

	public void setStatus(BigDecimal status) {
		this.status = status;
	}

	public String getTransactionCode() {
		return this.transactionCode;
	}

	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

	public BigDecimal getTransactionType() {
		return this.transactionType;
	}

	public void setTransactionType(BigDecimal transactionType) {
		this.transactionType = transactionType;
	}

	public BigDecimal getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(BigDecimal versionNo) {
		this.versionNo = versionNo;
	}

}
package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the DSS_PROVIDER_AUDIT_NOTES database table.
 * 
 */
@Entity
@Table(name = "DSS_PROVIDER_AUDIT_NOTES")
public class DssProviderAuditNote implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "NOTE_ID")
	private Long noteId;

	@OneToOne(mappedBy = "dssProviderAuditNote")
	private DssProviderAuditNotesXref dssProviderAuditNotesXref;

	@Temporal(TemporalType.DATE)
	@Column(name = "AUDIT_NOTE_DATE")
	private Date auditNoteDate;

	@Column(name = "AUDIT_NOTE_DESC")
	private String auditNoteDesc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	public DssProviderAuditNote() {
	}

	public Long getNoteId() {
		return this.noteId;
	}

	public void setNoteId(Long noteId) {
		this.noteId = noteId;
	}

	public Date getAuditNoteDate() {
		return this.auditNoteDate;
	}

	public void setAuditNoteDate(Date auditNoteDate) {
		this.auditNoteDate = auditNoteDate;
	}

	public String getAuditNoteDesc() {
		return this.auditNoteDesc;
	}

	public void setAuditNoteDesc(String auditNoteDesc) {
		this.auditNoteDesc = auditNoteDesc;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public DssProviderAuditNotesXref getDssProviderAuditNotesXref() {
		return this.dssProviderAuditNotesXref;
	}

	public void setDssProviderAuditNotesXref(DssProviderAuditNotesXref dssProviderAuditNotesXref) {
		this.dssProviderAuditNotesXref = dssProviderAuditNotesXref;
	}

}
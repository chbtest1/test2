package ca.medavie.nspp.audit.service.data.mappedEntity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Models a Business Arrangement Exclusion. Loads provider/group information from DSS_PROVIDER_2 and DSS_PROVIDER_GROUP
 * tables
 */
@Entity
@SqlResultSetMapping(name = "DssBusinessArrangementExclusionQueryMapping", entities = { @EntityResult(entityClass = DssBusinessArrangementExclusionMappedEntity.class, fields = {
		@FieldResult(name = "bus_arr_number", column = "bus_arr_number"),
		@FieldResult(name = "bus_arr_description", column = "bus_arr_description"),
		@FieldResult(name = "provider_name", column = "provider_name"),
		@FieldResult(name = "provider_number", column = "provider_number"),
		@FieldResult(name = "provider_type", column = "provider_type"),
		@FieldResult(name = "provider_group_id", column = "provider_group_id"),
		@FieldResult(name = "last_modified", column = "last_modified"),
		@FieldResult(name = "modified_by", column = "modified_by") }) })
public class DssBusinessArrangementExclusionMappedEntity {
	@Id
	private Long bus_arr_number;
	@Column
	private String bus_arr_description;
	@Column
	private String provider_name;
	@Column
	private Long provider_number;
	@Column
	private String provider_type;
	@Column
	private Long provider_group_id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column
	private Date last_modified;
	@Column
	private String modified_by;

	/**
	 * @return the bus_arr_number
	 */
	public Long getBus_arr_number() {
		return bus_arr_number;
	}

	/**
	 * @param bus_arr_number
	 *            the bus_arr_number to set
	 */
	public void setBus_arr_number(Long bus_arr_number) {
		this.bus_arr_number = bus_arr_number;
	}

	/**
	 * @return the bus_arr_description
	 */
	public String getBus_arr_description() {
		return bus_arr_description;
	}

	/**
	 * @param bus_arr_description
	 *            the bus_arr_description to set
	 */
	public void setBus_arr_description(String bus_arr_description) {
		this.bus_arr_description = bus_arr_description;
	}

	/**
	 * @return the provider_name
	 */
	public String getProvider_name() {
		return provider_name;
	}

	/**
	 * @param provider_name
	 *            the provider_name to set
	 */
	public void setProvider_name(String provider_name) {
		this.provider_name = provider_name;
	}

	/**
	 * @return the provider_number
	 */
	public Long getProvider_number() {
		return provider_number;
	}

	/**
	 * @param provider_number
	 *            the provider_number to set
	 */
	public void setProvider_number(Long provider_number) {
		this.provider_number = provider_number;
	}

	/**
	 * @return the provider_type
	 */
	public String getProvider_type() {
		return provider_type;
	}

	/**
	 * @param provider_type
	 *            the provider_type to set
	 */
	public void setProvider_type(String provider_type) {
		this.provider_type = provider_type;
	}

	/**
	 * @return the provider_group_id
	 */
	public Long getProvider_group_id() {
		return provider_group_id;
	}

	/**
	 * @param provider_group_id
	 *            the provider_group_id to set
	 */
	public void setProvider_group_id(Long provider_group_id) {
		this.provider_group_id = provider_group_id;
	}

	/**
	 * @return the last_modified
	 */
	public Date getLast_modified() {
		return last_modified;
	}

	/**
	 * @param last_modified
	 *            the last_modified to set
	 */
	public void setLast_modified(Date last_modified) {
		this.last_modified = last_modified;
	}

	/**
	 * @return the modified_by
	 */
	public String getModified_by() {
		return modified_by;
	}

	/**
	 * @param modified_by
	 *            the modified_by to set
	 */
	public void setModified_by(String modified_by) {
		this.modified_by = modified_by;
	}
}

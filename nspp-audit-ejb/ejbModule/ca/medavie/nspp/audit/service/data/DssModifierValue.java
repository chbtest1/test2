package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the DSS_MODIFIER_VALUE database table.
 * 
 */
@Entity
@Table(name="DSS_MODIFIER_VALUE")
public class DssModifierValue implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DssModifierValuePK id;

	private String description;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_TO_DATE")
	private Date effectiveToDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFIED")
	private Date lastModified;

	public DssModifierValue() {
	}

	public DssModifierValuePK getId() {
		return this.id;
	}

	public void setId(DssModifierValuePK id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getEffectiveToDate() {
		return this.effectiveToDate;
	}

	public void setEffectiveToDate(Date effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

}
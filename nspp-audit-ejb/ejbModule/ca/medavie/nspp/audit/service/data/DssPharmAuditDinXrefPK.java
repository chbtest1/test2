package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_PHARM_AUDIT_DIN_XREF database table.
 * 
 */
@Embeddable
public class DssPharmAuditDinXrefPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="AUDIT_CRITERIA_ID")
	private long auditCriteriaId;

	private String din;

	public DssPharmAuditDinXrefPK() {
	}
	public long getAuditCriteriaId() {
		return this.auditCriteriaId;
	}
	public void setAuditCriteriaId(long auditCriteriaId) {
		this.auditCriteriaId = auditCriteriaId;
	}
	public String getDin() {
		return this.din;
	}
	public void setDin(String din) {
		this.din = din;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssPharmAuditDinXrefPK)) {
			return false;
		}
		DssPharmAuditDinXrefPK castOther = (DssPharmAuditDinXrefPK)other;
		return 
			(this.auditCriteriaId == castOther.auditCriteriaId)
			&& this.din.equals(castOther.din);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.auditCriteriaId ^ (this.auditCriteriaId >>> 32)));
		hash = hash * prime + this.din.hashCode();
		
		return hash;
	}
}
package ca.medavie.nspp.audit.service.data.mapping;

import java.util.ArrayList;
import java.util.List;

import org.dozer.ConfigurableCustomConverter;

import ca.medavie.nspp.audit.service.data.AuditResult;
import ca.medavie.nspp.audit.service.data.AuditResultAttachment;
import ca.medavie.nspp.audit.service.data.AuditResultNote;
import ca.medavie.nspp.audit.service.data.DssProviderAudit;
import ca.medavie.nspp.audit.service.data.DssProviderAuditAttach;
import ca.medavie.nspp.audit.service.data.DssProviderAuditNotesXref;
import ca.medavie.nspp.audit.service.repository.BaseRepository;

/**
 * 
 * Data mapper
 * 
 **/

@SuppressWarnings("rawtypes")
public class AuditResultMapper extends BaseRepository implements ConfigurableCustomConverter {

	private String param;

	@Override
	public Object convert(Object dest, Object source, Class<?> arg2, Class<?> arg3) {

		List<Object> result = new ArrayList<Object>();

		// Ensure source object is provided.
		if (source != null) {

			DssProviderAuditNotesXref dssProviderAuditNotesXrefTarget = null;
			DssProviderAuditAttach dssProviderAuditAttachTarget = null;

			// Data converter class convert DssProviderAudit TO AuditResult
			if (param.equals(AuditResults)) {

				for (Object s : (List) source) {
					// convert DssProvTypePeerXref to subcategory
					if (s instanceof DssProviderAudit) {
						AuditResult target = new AuditResult();
						BEAN_MAPPER.map(s, target);
						result.add(target);
					}

					// Mapping in opposite way, convert from subcategory type to DssProvTypePeerXref
					if (s instanceof AuditResult) {
						DssProviderAudit target = new DssProviderAudit();
						BEAN_MAPPER.map(s, target);
						result.add(target);
					}
				}
			}

			// Data converter class convert DssProviderAuditNotesXref TO AuditResultNote
			if (param.equals(AuditResultsNote)) {

				for (Object s : (List) source) {
					// convert DssProviderAuditNote to AuditResultNote
					if (s instanceof DssProviderAuditNotesXref) {
						AuditResultNote target = new AuditResultNote();
						BEAN_MAPPER.map(s, target);
						result.add(target);
					}

					// Mapping in opposite way, convert from AuditResultNote type to DssProviderAuditNotesXref
					if (s instanceof AuditResultNote) {
						dssProviderAuditNotesXrefTarget = new DssProviderAuditNotesXref();
						BEAN_MAPPER.map(s, dssProviderAuditNotesXrefTarget);
						result.add(dssProviderAuditNotesXrefTarget);
					}
				}
			}

			// Data converter class convert DssProviderAuditAttach TO AuditResultAttachment
			if (param.equals(AuditResultsAttachment)) {
				for (Object s : (List) source) {
					if (s instanceof DssProviderAuditAttach) {
						AuditResultAttachment target = new AuditResultAttachment();
						BEAN_MAPPER.map(s, target);
						result.add(target);
					}

					// Mapping in the opposite direction, convert from AuditResultAttachment TO DssProviderAuditAttach
					if (s instanceof AuditResultAttachment) {
						dssProviderAuditAttachTarget = new DssProviderAuditAttach();
						BEAN_MAPPER.map(s, dssProviderAuditAttachTarget);
						result.add(dssProviderAuditAttachTarget);
					}
				}
			}
		}
		return result;
	}

	@Override
	public void setParameter(String arg0) {

		this.param = arg0;

	}

}

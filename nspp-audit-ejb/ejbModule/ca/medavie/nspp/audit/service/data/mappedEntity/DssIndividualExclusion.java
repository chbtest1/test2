package ca.medavie.nspp.audit.service.data.mappedEntity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.SqlResultSetMapping;

/**
 * The persistent class for dss_med_aud_indiv_excl
 */
@Entity
@SqlResultSetMapping(name = "DssIndividualExclusionNativeQueryMapping", entities = { @EntityResult(entityClass = DssIndividualExclusion.class, fields = {
		@FieldResult(name = "id.health_card_number", column = "health_card_number"),
		@FieldResult(name = "id.name", column = "name"),
		@FieldResult(name = "id.effective_from_date", column = "effective_from_date"),
		@FieldResult(name = "id.effective_to_date", column = "effective_to_date"),
		@FieldResult(name = "id.last_modified", column = "last_modified"),
		@FieldResult(name = "id.modified_by", column = "modified_by") }) })
public class DssIndividualExclusion {

	/**
	 * Set all feild values in ID to avoid duplicates
	 */
	@EmbeddedId
	private DssIndividualExclusionPK id;

	/**
	 * @return the id
	 */
	public DssIndividualExclusionPK getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(DssIndividualExclusionPK id) {
		this.id = id;
	}
}

package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_MODIFIER_VALUE database table.
 * 
 */
@Embeddable
public class DssModifierValuePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="MODIFIER_TYPE")
	private String modifierType;

	@Column(name="MODIFIER_VALUE")
	private String modifierValue;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_FROM_DATE")
	private java.util.Date effectiveFromDate;

	public DssModifierValuePK() {
	}
	public String getModifierType() {
		return this.modifierType;
	}
	public void setModifierType(String modifierType) {
		this.modifierType = modifierType;
	}
	public String getModifierValue() {
		return this.modifierValue;
	}
	public void setModifierValue(String modifierValue) {
		this.modifierValue = modifierValue;
	}
	public java.util.Date getEffectiveFromDate() {
		return this.effectiveFromDate;
	}
	public void setEffectiveFromDate(java.util.Date effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssModifierValuePK)) {
			return false;
		}
		DssModifierValuePK castOther = (DssModifierValuePK)other;
		return 
			this.modifierType.equals(castOther.modifierType)
			&& this.modifierValue.equals(castOther.modifierValue)
			&& this.effectiveFromDate.equals(castOther.effectiveFromDate);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.modifierType.hashCode();
		hash = hash * prime + this.modifierValue.hashCode();
		hash = hash * prime + this.effectiveFromDate.hashCode();
		
		return hash;
	}
}
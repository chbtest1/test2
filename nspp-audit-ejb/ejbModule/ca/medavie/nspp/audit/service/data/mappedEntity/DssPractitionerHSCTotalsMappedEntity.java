package ca.medavie.nspp.audit.service.data.mappedEntity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.SqlResultSetMapping;

/**
 * Maps the HSC totals for Practitioner inquiry data
 */
@Entity
@SqlResultSetMapping(name = "DssPractitionerHSCTotalsQueryMapping", entities = { @EntityResult(entityClass = DssPractitionerHSCTotalsMappedEntity.class, fields = {
		@FieldResult(name = "id.number_of_patients", column = "number_of_patients"),
		@FieldResult(name = "id.idx_number_of_patients", column = "idx_number_of_patients"),
		@FieldResult(name = "id.number_of_se", column = "number_of_se"),
		@FieldResult(name = "id.idx_number_of_se", column = "idx_number_of_se"),
		@FieldResult(name = "id.total_amount_paid", column = "total_amount_paid"),
		@FieldResult(name = "id.idx_total_amount_paid", column = "idx_total_amount_paid"),
		@FieldResult(name = "id.number_of_se_per_patient", column = "number_of_se_per_patient"),
		@FieldResult(name = "id.idx_number_of_se_per_patient", column = "idx_number_of_se_per_patient"),
		@FieldResult(name = "id.amount_paid_per_patient", column = "amount_paid_per_patient"),
		@FieldResult(name = "id.idx_amount_paid_per_patient", column = "idx_amount_paid_per_patient") }) })
public class DssPractitionerHSCTotalsMappedEntity {

	@EmbeddedId
	private DssPractitionerHSCTotalsMappedEntityPK id;

	/**
	 * @return the id
	 */
	public DssPractitionerHSCTotalsMappedEntityPK getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(DssPractitionerHSCTotalsMappedEntityPK id) {
		this.id = id;
	}
}

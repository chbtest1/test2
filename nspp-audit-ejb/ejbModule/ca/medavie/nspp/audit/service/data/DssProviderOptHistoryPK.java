package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_PROVIDER_OPT_HISTORY database table.
 * 
 */
@Embeddable
public class DssProviderOptHistoryPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="PROVIDER_NUMBER")
	private long providerNumber;

	@Column(name="PROVIDER_TYPE")
	private String providerType;

	@Column(name="PROGRAM_CODE")
	private String programCode;

	@Temporal(TemporalType.DATE)
	@Column(name="OPT_IN_DATE")
	private java.util.Date optInDate;

	public DssProviderOptHistoryPK() {
	}
	public long getProviderNumber() {
		return this.providerNumber;
	}
	public void setProviderNumber(long providerNumber) {
		this.providerNumber = providerNumber;
	}
	public String getProviderType() {
		return this.providerType;
	}
	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}
	public String getProgramCode() {
		return this.programCode;
	}
	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}
	public java.util.Date getOptInDate() {
		return this.optInDate;
	}
	public void setOptInDate(java.util.Date optInDate) {
		this.optInDate = optInDate;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssProviderOptHistoryPK)) {
			return false;
		}
		DssProviderOptHistoryPK castOther = (DssProviderOptHistoryPK)other;
		return 
			(this.providerNumber == castOther.providerNumber)
			&& this.providerType.equals(castOther.providerType)
			&& this.programCode.equals(castOther.programCode)
			&& this.optInDate.equals(castOther.optInDate);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.providerNumber ^ (this.providerNumber >>> 32)));
		hash = hash * prime + this.providerType.hashCode();
		hash = hash * prime + this.programCode.hashCode();
		hash = hash * prime + this.optInDate.hashCode();
		
		return hash;
	}
}
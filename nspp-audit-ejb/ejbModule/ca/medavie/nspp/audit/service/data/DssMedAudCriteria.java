package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the DSS_MED_AUD_CRITERIA database table.
 * 
 */
@Entity
@Table(name = "DSS_MED_AUD_CRITERIA")
public class DssMedAudCriteria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "AUDIT_CRITERIA_ID")
	private long auditCriteriaId;

	@Temporal(TemporalType.DATE)
	@Column(name = "AUDIT_FROM_DATE")
	private Date auditFromDate;

	@Column(name = "AUDIT_FROM_LAST_INDICATOR")
	private String auditFromLastIndicator;

	@Temporal(TemporalType.DATE)
	@Column(name = "AUDIT_FROM_PAYMENT_DATE")
	private Date auditFromPaymentDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "AUDIT_TO_DATE")
	private Date auditToDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "AUDIT_TO_PAYMENT_DATE")
	private Date auditToPaymentDate;

	@Column(name = "AUDIT_TYPE")
	private String auditType;

	@Column(name = "BUS_ARR_NUMBER")
	private Long busArrNumber;

	@Column(name = "DEFAULT_HEALTH_SERVICE_DESC")
	private String defaultHealthServiceDesc;

	@Column(name = "EXCL_DIAGNOSTIC_CODE_INDICATOR")
	private String exclDiagnosticCodeIndicator;

	@Column(name = "FACILITY_INCLUSIVE")
	private String facilityInclusive;

	@Column(name = "FROM_PAYMENT_RUN_NUMBER")
	private BigDecimal fromPaymentRunNumber;

	@Column(name = "HEALTH_SERVICE_INCLUSIVE")
	private String healthServiceInclusive;

	@Column(name = "LAST_AUDIT_PAYMENT_RUN_NUMBER")
	private BigDecimal lastAuditPaymentRunNumber;

	@Column(name = "LAST_AUDIT_RUN_NUMBER")
	private BigDecimal lastAuditRunNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Temporal(TemporalType.DATE)
	@Column(name = "LAST_PAYMENT_DATE")
	private Date lastPaymentDate;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	@Column(name = "NUMBER_OF_SERVICES_TO_AUDIT")
	private BigDecimal numberOfServicesToAudit;

	@Column(name = "ONE_TIME_AUDIT_INDICATOR")
	private String oneTimeAuditIndicator;

	@Column(name = "OTHER_REMUN_METHOD_INDICATOR")
	private String otherRemunMethodIndicator;

	@Column(name = "PAY_TO_CODE_INCLUSIVE")
	private String payToCodeInclusive;

	@Column(name = "PAYMENT_RESP_INCLUSIVE")
	private String paymentRespInclusive;

	@Column(name = "PROVIDER_NUMBER")
	private BigDecimal providerNumber;

	@Column(name = "PROVIDER_TYPE")
	private String providerType;

	@Temporal(TemporalType.DATE)
	@Column(name = "SCND_LAST_PAYMENT_DATE")
	private Date scndLastPaymentDate;

	@Column(name = "SCND_LAST_PAYMENT_RUN_NUMBER")
	private BigDecimal scndLastPaymentRunNumber;

	@Column(name = "TO_PAYMENT_RUN_NUMBER")
	private BigDecimal toPaymentRunNumber;

	// bi-directional many-to-one association to DssMedAudPayRespXref
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "dssMedAudCriteria", cascade = { CascadeType.ALL, CascadeType.MERGE }, orphanRemoval = true)
	private List<DssMedAudPayRespXref> dssMedAudPayRespXrefs;

	// bi-directional many-to-one association to DssMedAudPayToCdXref
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "dssMedAudCriteria", cascade = { CascadeType.ALL, CascadeType.MERGE }, orphanRemoval = true)
	private List<DssMedAudPayToCdXref> dssMedAudPayToCdXrefs;

	// bi-directional many-to-one association to DssMedAudHlthSrvXref
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "dssMedAudCriteria", cascade = { CascadeType.ALL, CascadeType.MERGE }, orphanRemoval = true)
	private List<DssMedAudHlthSrvXref> dssMedAudHlthSrvXrefs;

	public DssMedAudCriteria() {
	}

	public long getAuditCriteriaId() {
		return this.auditCriteriaId;
	}

	public void setAuditCriteriaId(long auditCriteriaId) {
		this.auditCriteriaId = auditCriteriaId;
	}

	public Date getAuditFromDate() {
		return this.auditFromDate;
	}

	public void setAuditFromDate(Date auditFromDate) {
		this.auditFromDate = auditFromDate;
	}

	public String getAuditFromLastIndicator() {
		return this.auditFromLastIndicator;
	}

	public void setAuditFromLastIndicator(String auditFromLastIndicator) {
		this.auditFromLastIndicator = auditFromLastIndicator;
	}

	public Date getAuditFromPaymentDate() {
		return this.auditFromPaymentDate;
	}

	public void setAuditFromPaymentDate(Date auditFromPaymentDate) {
		this.auditFromPaymentDate = auditFromPaymentDate;
	}

	public Date getAuditToDate() {
		return this.auditToDate;
	}

	public void setAuditToDate(Date auditToDate) {
		this.auditToDate = auditToDate;
	}

	public Date getAuditToPaymentDate() {
		return this.auditToPaymentDate;
	}

	public void setAuditToPaymentDate(Date auditToPaymentDate) {
		this.auditToPaymentDate = auditToPaymentDate;
	}

	public String getAuditType() {
		return this.auditType;
	}

	public void setAuditType(String auditType) {
		this.auditType = auditType;
	}

	public Long getBusArrNumber() {
		return this.busArrNumber;
	}

	public void setBusArrNumber(Long busArrNumber) {
		this.busArrNumber = busArrNumber;
	}

	public String getDefaultHealthServiceDesc() {
		return this.defaultHealthServiceDesc;
	}

	public void setDefaultHealthServiceDesc(String defaultHealthServiceDesc) {
		this.defaultHealthServiceDesc = defaultHealthServiceDesc;
	}

	public String getExclDiagnosticCodeIndicator() {
		return this.exclDiagnosticCodeIndicator;
	}

	public void setExclDiagnosticCodeIndicator(String exclDiagnosticCodeIndicator) {
		this.exclDiagnosticCodeIndicator = exclDiagnosticCodeIndicator;
	}

	public String getFacilityInclusive() {
		return this.facilityInclusive;
	}

	public void setFacilityInclusive(String facilityInclusive) {
		this.facilityInclusive = facilityInclusive;
	}

	public BigDecimal getFromPaymentRunNumber() {
		return this.fromPaymentRunNumber;
	}

	public void setFromPaymentRunNumber(BigDecimal fromPaymentRunNumber) {
		this.fromPaymentRunNumber = fromPaymentRunNumber;
	}

	public String getHealthServiceInclusive() {
		return this.healthServiceInclusive;
	}

	public void setHealthServiceInclusive(String healthServiceInclusive) {
		this.healthServiceInclusive = healthServiceInclusive;
	}

	public BigDecimal getLastAuditPaymentRunNumber() {
		return this.lastAuditPaymentRunNumber;
	}

	public void setLastAuditPaymentRunNumber(BigDecimal lastAuditPaymentRunNumber) {
		this.lastAuditPaymentRunNumber = lastAuditPaymentRunNumber;
	}

	public BigDecimal getLastAuditRunNumber() {
		return this.lastAuditRunNumber;
	}

	public void setLastAuditRunNumber(BigDecimal lastAuditRunNumber) {
		this.lastAuditRunNumber = lastAuditRunNumber;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public Date getLastPaymentDate() {
		return this.lastPaymentDate;
	}

	public void setLastPaymentDate(Date lastPaymentDate) {
		this.lastPaymentDate = lastPaymentDate;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public BigDecimal getNumberOfServicesToAudit() {
		return this.numberOfServicesToAudit;
	}

	public void setNumberOfServicesToAudit(BigDecimal numberOfServicesToAudit) {
		this.numberOfServicesToAudit = numberOfServicesToAudit;
	}

	public String getOneTimeAuditIndicator() {
		return this.oneTimeAuditIndicator;
	}

	public void setOneTimeAuditIndicator(String oneTimeAuditIndicator) {
		this.oneTimeAuditIndicator = oneTimeAuditIndicator;
	}

	public String getOtherRemunMethodIndicator() {
		return this.otherRemunMethodIndicator;
	}

	public void setOtherRemunMethodIndicator(String otherRemunMethodIndicator) {
		this.otherRemunMethodIndicator = otherRemunMethodIndicator;
	}

	public String getPayToCodeInclusive() {
		return this.payToCodeInclusive;
	}

	public void setPayToCodeInclusive(String payToCodeInclusive) {
		this.payToCodeInclusive = payToCodeInclusive;
	}

	public String getPaymentRespInclusive() {
		return this.paymentRespInclusive;
	}

	public void setPaymentRespInclusive(String paymentRespInclusive) {
		this.paymentRespInclusive = paymentRespInclusive;
	}

	public BigDecimal getProviderNumber() {
		return this.providerNumber;
	}

	public void setProviderNumber(BigDecimal providerNumber) {
		this.providerNumber = providerNumber;
	}

	public String getProviderType() {
		return this.providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public Date getScndLastPaymentDate() {
		return this.scndLastPaymentDate;
	}

	public void setScndLastPaymentDate(Date scndLastPaymentDate) {
		this.scndLastPaymentDate = scndLastPaymentDate;
	}

	public BigDecimal getScndLastPaymentRunNumber() {
		return this.scndLastPaymentRunNumber;
	}

	public void setScndLastPaymentRunNumber(BigDecimal scndLastPaymentRunNumber) {
		this.scndLastPaymentRunNumber = scndLastPaymentRunNumber;
	}

	public BigDecimal getToPaymentRunNumber() {
		return this.toPaymentRunNumber;
	}

	public void setToPaymentRunNumber(BigDecimal toPaymentRunNumber) {
		this.toPaymentRunNumber = toPaymentRunNumber;
	}

	public List<DssMedAudPayRespXref> getDssMedAudPayRespXrefs() {
		return this.dssMedAudPayRespXrefs;
	}

	public void setDssMedAudPayRespXrefs(List<DssMedAudPayRespXref> dssMedAudPayRespXrefs) {
		this.dssMedAudPayRespXrefs = dssMedAudPayRespXrefs;
	}

	public DssMedAudPayRespXref addDssMedAudPayRespXref(DssMedAudPayRespXref dssMedAudPayRespXref) {
		getDssMedAudPayRespXrefs().add(dssMedAudPayRespXref);
		dssMedAudPayRespXref.setDssMedAudCriteria(this);

		return dssMedAudPayRespXref;
	}

	public DssMedAudPayRespXref removeDssMedAudPayRespXref(DssMedAudPayRespXref dssMedAudPayRespXref) {
		getDssMedAudPayRespXrefs().remove(dssMedAudPayRespXref);
		dssMedAudPayRespXref.setDssMedAudCriteria(null);

		return dssMedAudPayRespXref;
	}

	public List<DssMedAudPayToCdXref> getDssMedAudPayToCdXrefs() {
		return this.dssMedAudPayToCdXrefs;
	}

	public void setDssMedAudPayToCdXrefs(List<DssMedAudPayToCdXref> dssMedAudPayToCdXrefs) {
		this.dssMedAudPayToCdXrefs = dssMedAudPayToCdXrefs;
	}

	public DssMedAudPayToCdXref addDssMedAudPayToCdXref(DssMedAudPayToCdXref dssMedAudPayToCdXref) {
		getDssMedAudPayToCdXrefs().add(dssMedAudPayToCdXref);
		dssMedAudPayToCdXref.setDssMedAudCriteria(this);

		return dssMedAudPayToCdXref;
	}

	public DssMedAudPayToCdXref removeDssMedAudPayToCdXref(DssMedAudPayToCdXref dssMedAudPayToCdXref) {
		getDssMedAudPayToCdXrefs().remove(dssMedAudPayToCdXref);
		dssMedAudPayToCdXref.setDssMedAudCriteria(null);

		return dssMedAudPayToCdXref;
	}

	public List<DssMedAudHlthSrvXref> getDssMedAudHlthSrvXrefs() {
		return this.dssMedAudHlthSrvXrefs;
	}

	public void setDssMedAudHlthSrvXrefs(List<DssMedAudHlthSrvXref> dssMedAudHlthSrvXrefs) {
		this.dssMedAudHlthSrvXrefs = dssMedAudHlthSrvXrefs;
	}

	public DssMedAudHlthSrvXref addDssMedAudHlthSrvXref(DssMedAudHlthSrvXref dssMedAudHlthSrvXref) {
		getDssMedAudHlthSrvXrefs().add(dssMedAudHlthSrvXref);
		dssMedAudHlthSrvXref.setDssMedAudCriteria(this);

		return dssMedAudHlthSrvXref;
	}

	public DssMedAudHlthSrvXref removeDssMedAudHlthSrvXref(DssMedAudHlthSrvXref dssMedAudHlthSrvXref) {
		getDssMedAudHlthSrvXrefs().remove(dssMedAudHlthSrvXref);
		dssMedAudHlthSrvXref.setDssMedAudCriteria(null);

		return dssMedAudHlthSrvXref;
	}

}
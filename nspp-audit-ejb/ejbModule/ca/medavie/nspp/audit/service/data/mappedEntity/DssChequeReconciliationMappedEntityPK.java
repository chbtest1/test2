package ca.medavie.nspp.audit.service.data.mappedEntity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class DssChequeReconciliationMappedEntityPK implements Serializable {

	/** IDE Generated */
	private static final long serialVersionUID = -4750722865417800468L;

	@Column(name = "AGENCY_ID")
	private String agencyId;

	@Column(name = "BATCH_NUMBER")
	private String batchNumber;

	@Column(name = "CHEQUE_AMOUNT")
	private BigDecimal chequeAmount;

	@Column(name = "CHEQUE_NOTE")
	private String chequeNote;
	
	@Column(name = "CHEQUE_NOTE")
	private String originalChequeNote;

	@Column(name = "CHEQUE_NUMBER")
	private Long chequeNumber;

	@Column(name = "CLAIMED_AMOUNT")
	private BigDecimal claimedAmount;

	@Temporal(TemporalType.DATE)
	@Column(name = "DATE_CASHED")
	private Date dateCashed;

	@Column(name = "FAMILY_BENEFITS_NUMBER")
	private String familyBenefitsNumber;

	@Column(name = "GL_NUMBER")
	private Long glNumber;
	
	@Column(name = "GL_NUMBER")
	private Long originalGlNumber;

	@Column(name = "HEALTH_CARD_NUMBER")
	private String healthCardNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "OTHER_PAYEE_ID")
	private Long otherPayeeId;

	@Column(name = "PAID_STATUS")
	private String paidStatus;

	@Column(name = "PAYEE_NAME")
	private String payeeName;

	@Column(name = "PAYEE_TYPE")
	private Long payeeType;

	@Temporal(TemporalType.DATE)
	@Column(name = "PAYMENT_DATE")
	private Date paymentDate;

	@Column(name = "PAYMENT_RUN_NUMBER")
	private Long paymentRunNumber;

	@Column(name = "PROVIDER_GROUP_ID")
	private Long providerGroupId;

	@Column(name = "PROVIDER_NUMBER")
	private Long providerNumber;

	@Column(name = "PROVIDER_TYPE")
	private String providerType;

	@Column(name = "PROVINCE_CODE")
	private String provinceCode;

	@Column(name = "RECONCILIATION_STATUS")
	private String reconciliationStatus;

	@Column(name = "REGISTER_STATUS")
	private String registerStatus;

	@Column(name = "STUBS_VOIDED")
	private Long stubsVoided;

	@Column(name = "SUMMARY_NUMBER")
	private Long summaryNumber;

	/**
	 * @return the agencyId
	 */
	public String getAgencyId() {
		return agencyId;
	}

	/**
	 * @param agencyId
	 *            the agencyId to set
	 */
	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}

	/**
	 * @return the batchNumber
	 */
	public String getBatchNumber() {
		return batchNumber;
	}

	/**
	 * @param batchNumber
	 *            the batchNumber to set
	 */
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	/**
	 * @return the chequeAmount
	 */
	public BigDecimal getChequeAmount() {
		return chequeAmount;
	}

	/**
	 * @param chequeAmount
	 *            the chequeAmount to set
	 */
	public void setChequeAmount(BigDecimal chequeAmount) {
		this.chequeAmount = chequeAmount;
	}

	/**
	 * @return the chequeNote
	 */
	public String getChequeNote() {
		return chequeNote;
	}

	/**
	 * @param chequeNote
	 *            the chequeNote to set
	 */
	public void setChequeNote(String chequeNote) {
		this.chequeNote = chequeNote;
	}

	/**
	 * @return the chequeNumber
	 */
	public Long getChequeNumber() {
		return chequeNumber;
	}

	/**
	 * @param chequeNumber
	 *            the chequeNumber to set
	 */
	public void setChequeNumber(Long chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	/**
	 * @return the claimedAmount
	 */
	public BigDecimal getClaimedAmount() {
		return claimedAmount;
	}

	/**
	 * @param claimedAmount
	 *            the claimedAmount to set
	 */
	public void setClaimedAmount(BigDecimal claimedAmount) {
		this.claimedAmount = claimedAmount;
	}

	/**
	 * @return the dateCashed
	 */
	public Date getDateCashed() {
		return dateCashed;
	}

	/**
	 * @param dateCashed
	 *            the dateCashed to set
	 */
	public void setDateCashed(Date dateCashed) {
		this.dateCashed = dateCashed;
	}

	/**
	 * @return the familyBenefitsNumber
	 */
	public String getFamilyBenefitsNumber() {
		return familyBenefitsNumber;
	}

	/**
	 * @param familyBenefitsNumber
	 *            the familyBenefitsNumber to set
	 */
	public void setFamilyBenefitsNumber(String familyBenefitsNumber) {
		this.familyBenefitsNumber = familyBenefitsNumber;
	}

	/**
	 * @return the glNumber
	 */
	public Long getGlNumber() {
		return glNumber;
	}

	/**
	 * @param glNumber
	 *            the glNumber to set
	 */
	public void setGlNumber(Long glNumber) {
		this.glNumber = glNumber;
	}

	/**
	 * @return the healthCardNumber
	 */
	public String getHealthCardNumber() {
		return healthCardNumber;
	}

	/**
	 * @param healthCardNumber
	 *            the healthCardNumber to set
	 */
	public void setHealthCardNumber(String healthCardNumber) {
		this.healthCardNumber = healthCardNumber;
	}

	/**
	 * @return the lastModified
	 */
	public Date getLastModified() {
		return lastModified;
	}

	/**
	 * @param lastModified
	 *            the lastModified to set
	 */
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 * @return the otherPayeeId
	 */
	public Long getOtherPayeeId() {
		return otherPayeeId;
	}

	/**
	 * @param otherPayeeId
	 *            the otherPayeeId to set
	 */
	public void setOtherPayeeId(Long otherPayeeId) {
		this.otherPayeeId = otherPayeeId;
	}

	/**
	 * @return the paidStatus
	 */
	public String getPaidStatus() {
		return paidStatus;
	}

	/**
	 * @param paidStatus
	 *            the paidStatus to set
	 */
	public void setPaidStatus(String paidStatus) {
		this.paidStatus = paidStatus;
	}

	/**
	 * @return the payeeName
	 */
	public String getPayeeName() {
		return payeeName;
	}

	/**
	 * @param payeeName
	 *            the payeeName to set
	 */
	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}

	/**
	 * @return the payeeType
	 */
	public Long getPayeeType() {
		return payeeType;
	}

	/**
	 * @param payeeType
	 *            the payeeType to set
	 */
	public void setPayeeType(Long payeeType) {
		this.payeeType = payeeType;
	}

	/**
	 * @return the paymentDate
	 */
	public Date getPaymentDate() {
		return paymentDate;
	}

	/**
	 * @param paymentDate
	 *            the paymentDate to set
	 */
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	/**
	 * @return the paymentRunNumber
	 */
	public Long getPaymentRunNumber() {
		return paymentRunNumber;
	}

	/**
	 * @param paymentRunNumber
	 *            the paymentRunNumber to set
	 */
	public void setPaymentRunNumber(Long paymentRunNumber) {
		this.paymentRunNumber = paymentRunNumber;
	}

	/**
	 * @return the providerGroupId
	 */
	public Long getProviderGroupId() {
		return providerGroupId;
	}

	/**
	 * @param providerGroupId
	 *            the providerGroupId to set
	 */
	public void setProviderGroupId(Long providerGroupId) {
		this.providerGroupId = providerGroupId;
	}

	/**
	 * @return the providerNumber
	 */
	public Long getProviderNumber() {
		return providerNumber;
	}

	/**
	 * @param providerNumber
	 *            the providerNumber to set
	 */
	public void setProviderNumber(Long providerNumber) {
		this.providerNumber = providerNumber;
	}

	/**
	 * @return the providerType
	 */
	public String getProviderType() {
		return providerType;
	}

	/**
	 * @param providerType
	 *            the providerType to set
	 */
	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	/**
	 * @return the provinceCode
	 */
	public String getProvinceCode() {
		return provinceCode;
	}

	/**
	 * @param provinceCode
	 *            the provinceCode to set
	 */
	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	/**
	 * @return the reconciliationStatus
	 */
	public String getReconciliationStatus() {
		return reconciliationStatus;
	}

	/**
	 * @param reconciliationStatus
	 *            the reconciliationStatus to set
	 */
	public void setReconciliationStatus(String reconciliationStatus) {
		this.reconciliationStatus = reconciliationStatus;
	}

	/**
	 * @return the registerStatus
	 */
	public String getRegisterStatus() {
		return registerStatus;
	}

	/**
	 * @param registerStatus
	 *            the registerStatus to set
	 */
	public void setRegisterStatus(String registerStatus) {
		this.registerStatus = registerStatus;
	}

	/**
	 * @return the stubsVoided
	 */
	public Long getStubsVoided() {
		return stubsVoided;
	}

	/**
	 * @param stubsVoided
	 *            the stubsVoided to set
	 */
	public void setStubsVoided(Long stubsVoided) {
		this.stubsVoided = stubsVoided;
	}

	/**
	 * @return the summaryNumber
	 */
	public Long getSummaryNumber() {
		return summaryNumber;
	}

	/**
	 * @param summaryNumber
	 *            the summaryNumber to set
	 */
	public void setSummaryNumber(Long summaryNumber) {
		this.summaryNumber = summaryNumber;
	}

	/**
	 * @return the originalGlNumber
	 */
	public Long getOriginalGlNumber() {
		return originalGlNumber;
	}

	/**
	 * @param originalGlNumber the originalGlNumber to set
	 */
	public void setOriginalGlNumber(Long originalGlNumber) {
		this.originalGlNumber = originalGlNumber;
	}

	/**
	 * @return the originalChequeNote
	 */
	public String getOriginalChequeNote() {
		return originalChequeNote;
	}

	/**
	 * @param originalChequeNote the originalChequeNote to set
	 */
	public void setOriginalChequeNote(String originalChequeNote) {
		this.originalChequeNote = originalChequeNote;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((agencyId == null) ? 0 : agencyId.hashCode());
		result = prime * result + ((batchNumber == null) ? 0 : batchNumber.hashCode());
		result = prime * result + ((chequeAmount == null) ? 0 : chequeAmount.hashCode());
		result = prime * result + ((chequeNote == null) ? 0 : chequeNote.hashCode());
		result = prime * result + ((chequeNumber == null) ? 0 : chequeNumber.hashCode());
		result = prime * result + ((claimedAmount == null) ? 0 : claimedAmount.hashCode());
		result = prime * result + ((dateCashed == null) ? 0 : dateCashed.hashCode());
		result = prime * result + ((familyBenefitsNumber == null) ? 0 : familyBenefitsNumber.hashCode());
		result = prime * result + ((glNumber == null) ? 0 : glNumber.hashCode());
		result = prime * result + ((healthCardNumber == null) ? 0 : healthCardNumber.hashCode());
		result = prime * result + ((lastModified == null) ? 0 : lastModified.hashCode());
		result = prime * result + ((otherPayeeId == null) ? 0 : otherPayeeId.hashCode());
		result = prime * result + ((paidStatus == null) ? 0 : paidStatus.hashCode());
		result = prime * result + ((payeeName == null) ? 0 : payeeName.hashCode());
		result = prime * result + ((payeeType == null) ? 0 : payeeType.hashCode());
		result = prime * result + ((paymentDate == null) ? 0 : paymentDate.hashCode());
		result = prime * result + ((paymentRunNumber == null) ? 0 : paymentRunNumber.hashCode());
		result = prime * result + ((providerGroupId == null) ? 0 : providerGroupId.hashCode());
		result = prime * result + ((providerNumber == null) ? 0 : providerNumber.hashCode());
		result = prime * result + ((providerType == null) ? 0 : providerType.hashCode());
		result = prime * result + ((provinceCode == null) ? 0 : provinceCode.hashCode());
		result = prime * result + ((reconciliationStatus == null) ? 0 : reconciliationStatus.hashCode());
		result = prime * result + ((registerStatus == null) ? 0 : registerStatus.hashCode());
		result = prime * result + ((stubsVoided == null) ? 0 : stubsVoided.hashCode());
		result = prime * result + ((summaryNumber == null) ? 0 : summaryNumber.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DssChequeReconciliationMappedEntityPK other = (DssChequeReconciliationMappedEntityPK) obj;
		if (agencyId == null) {
			if (other.agencyId != null)
				return false;
		} else if (!agencyId.equals(other.agencyId))
			return false;
		if (batchNumber == null) {
			if (other.batchNumber != null)
				return false;
		} else if (!batchNumber.equals(other.batchNumber))
			return false;
		if (chequeAmount == null) {
			if (other.chequeAmount != null)
				return false;
		} else if (!chequeAmount.equals(other.chequeAmount))
			return false;
		if (chequeNote == null) {
			if (other.chequeNote != null)
				return false;
		} else if (!chequeNote.equals(other.chequeNote))
			return false;
		if (chequeNumber == null) {
			if (other.chequeNumber != null)
				return false;
		} else if (!chequeNumber.equals(other.chequeNumber))
			return false;
		if (claimedAmount == null) {
			if (other.claimedAmount != null)
				return false;
		} else if (!claimedAmount.equals(other.claimedAmount))
			return false;
		if (dateCashed == null) {
			if (other.dateCashed != null)
				return false;
		} else if (!dateCashed.equals(other.dateCashed))
			return false;
		if (familyBenefitsNumber == null) {
			if (other.familyBenefitsNumber != null)
				return false;
		} else if (!familyBenefitsNumber.equals(other.familyBenefitsNumber))
			return false;
		if (glNumber == null) {
			if (other.glNumber != null)
				return false;
		} else if (!glNumber.equals(other.glNumber))
			return false;
		if (healthCardNumber == null) {
			if (other.healthCardNumber != null)
				return false;
		} else if (!healthCardNumber.equals(other.healthCardNumber))
			return false;
		if (lastModified == null) {
			if (other.lastModified != null)
				return false;
		} else if (!lastModified.equals(other.lastModified))
			return false;
		if (otherPayeeId == null) {
			if (other.otherPayeeId != null)
				return false;
		} else if (!otherPayeeId.equals(other.otherPayeeId))
			return false;
		if (paidStatus == null) {
			if (other.paidStatus != null)
				return false;
		} else if (!paidStatus.equals(other.paidStatus))
			return false;
		if (payeeName == null) {
			if (other.payeeName != null)
				return false;
		} else if (!payeeName.equals(other.payeeName))
			return false;
		if (payeeType == null) {
			if (other.payeeType != null)
				return false;
		} else if (!payeeType.equals(other.payeeType))
			return false;
		if (paymentDate == null) {
			if (other.paymentDate != null)
				return false;
		} else if (!paymentDate.equals(other.paymentDate))
			return false;
		if (paymentRunNumber == null) {
			if (other.paymentRunNumber != null)
				return false;
		} else if (!paymentRunNumber.equals(other.paymentRunNumber))
			return false;
		if (providerGroupId == null) {
			if (other.providerGroupId != null)
				return false;
		} else if (!providerGroupId.equals(other.providerGroupId))
			return false;
		if (providerNumber == null) {
			if (other.providerNumber != null)
				return false;
		} else if (!providerNumber.equals(other.providerNumber))
			return false;
		if (providerType == null) {
			if (other.providerType != null)
				return false;
		} else if (!providerType.equals(other.providerType))
			return false;
		if (provinceCode == null) {
			if (other.provinceCode != null)
				return false;
		} else if (!provinceCode.equals(other.provinceCode))
			return false;
		if (reconciliationStatus == null) {
			if (other.reconciliationStatus != null)
				return false;
		} else if (!reconciliationStatus.equals(other.reconciliationStatus))
			return false;
		if (registerStatus == null) {
			if (other.registerStatus != null)
				return false;
		} else if (!registerStatus.equals(other.registerStatus))
			return false;
		if (stubsVoided == null) {
			if (other.stubsVoided != null)
				return false;
		} else if (!stubsVoided.equals(other.stubsVoided))
			return false;
		if (summaryNumber == null) {
			if (other.summaryNumber != null)
				return false;
		} else if (!summaryNumber.equals(other.summaryNumber))
			return false;
		return true;
	}
}

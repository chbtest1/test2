package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the DSS_PROVIDER_PEER_GROUP database table.
 * 
 */
@Entity
@Table(name = "DSS_PROVIDER_PEER_GROUP")
@SqlResultSetMapping(name = "DSSPeerGroupNativeQueryMapping", entities = { @EntityResult(entityClass = DssProviderPeerGroup.class, fields = {
		@FieldResult(name = "id.providerPeerGroupId", column = "provider_peer_group_id"),
		@FieldResult(name = "id.yearEndDate", column = "year_end_date"),
		@FieldResult(name = "providerPeerGroupName", column = "provider_peer_group_name") }) })
public class DssProviderPeerGroup implements Serializable {
	private static final long serialVersionUID = 1L;

	// association with subcategories through DssProvTypePeerXref
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL, CascadeType.MERGE }, orphanRemoval = true, mappedBy = "dssProviderPeerGroup1")
	private List<DssProvTypePeerXref> dssProvTypePeerXrefs;

	// association with Specialties through DssSpecialtyPeerGroupXref
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL, CascadeType.MERGE }, orphanRemoval = true, mappedBy = "dssProviderPeerGroup2")
	private List<DssSpecialtyPeerGroupXref> dssSpecialtyPeerGroupXrefs;

	// association with DssProvider2 through DssProviderPeerGroupXref (Fee for Service Practitioner)
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL, CascadeType.MERGE }, orphanRemoval = true, mappedBy = "dssProviderPeerGroup3")
	private List<DssProviderPeerGroupXref> dssProviderPeerGroupXrefs;

	// association with DssProvider2 through DssShadProvPeerGroupXref (Shadow Practitioner)
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL, CascadeType.MERGE }, orphanRemoval = true, mappedBy = "dssProviderPeerGroup4")
	private List<DssShadProvPeerGroupXref> dssShadProvPeerGroupXrefs;

	// association with Towncriteria through DssTownPeerGroupXref
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL, CascadeType.MERGE }, orphanRemoval = true, mappedBy = "dssProviderPeerGroup5")
	private List<DssTownPeerGroupXref> dssTownPeerGroupXrefs;

	// association with HealthService through DssHealthServicePeerXref
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL, CascadeType.MERGE }, orphanRemoval = true, mappedBy = "dssProviderPeerGroup6")
	private List<DssHealthServicePeerXref> dssHealthServicePeerXrefs;

	@EmbeddedId
	private DssProviderPeerGroupPK id;

	@Column(name = "ACT_MINIMUM_EARNINGS")
	private BigDecimal actMinimumEarnings;

	@Column(name = "ACT_MINIMUM_NUMBER_OF_RX")
	private BigDecimal actMinimumNumberOfRx;

	@Column(name = "ACT_MINIMUM_SHADOW_EARNINGS")
	private BigDecimal actMinimumShadowEarnings;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "MAX_EARNINGS")
	private BigDecimal maxEarnings;

	@Column(name = "MAX_TOWN_POPULATION")
	private BigDecimal maxTownPopulation;

	@Column(name = "MIN_EARNINGS")
	private BigDecimal minEarnings;

	@Column(name = "MIN_TOWN_POPULATION")
	private BigDecimal minTownPopulation;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	@Column(name = "PERCENTAGE_SE_EARNINGS")
	private BigDecimal percentageSeEarnings;

	@Column(name = "PROVIDER_PEER_GROUP_NAME")
	private String providerPeerGroupName;

	@Column(name = "PROVIDER_PEER_GROUP_TYPE")
	private String providerPeerGroupType;

	@Column(name = "TOWN_CODE_INCLUSIVE")
	private String townCodeInclusive;

	public DssProviderPeerGroup() {
	}

	public DssProviderPeerGroupPK getId() {
		return this.id;
	}

	public void setId(DssProviderPeerGroupPK id) {
		this.id = id;
	}

	public BigDecimal getActMinimumEarnings() {
		return this.actMinimumEarnings;
	}

	public void setActMinimumEarnings(BigDecimal actMinimumEarnings) {
		this.actMinimumEarnings = actMinimumEarnings;
	}

	public BigDecimal getActMinimumNumberOfRx() {
		return this.actMinimumNumberOfRx;
	}

	public void setActMinimumNumberOfRx(BigDecimal actMinimumNumberOfRx) {
		this.actMinimumNumberOfRx = actMinimumNumberOfRx;
	}

	public BigDecimal getActMinimumShadowEarnings() {
		return this.actMinimumShadowEarnings;
	}

	public void setActMinimumShadowEarnings(BigDecimal actMinimumShadowEarnings) {
		this.actMinimumShadowEarnings = actMinimumShadowEarnings;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public BigDecimal getMaxEarnings() {
		return this.maxEarnings;
	}

	public void setMaxEarnings(BigDecimal maxEarnings) {
		this.maxEarnings = maxEarnings;
	}

	public BigDecimal getMaxTownPopulation() {
		return this.maxTownPopulation;
	}

	public void setMaxTownPopulation(BigDecimal maxTownPopulation) {
		this.maxTownPopulation = maxTownPopulation;
	}

	public BigDecimal getMinEarnings() {
		return this.minEarnings;
	}

	public void setMinEarnings(BigDecimal minEarnings) {
		this.minEarnings = minEarnings;
	}

	public BigDecimal getMinTownPopulation() {
		return this.minTownPopulation;
	}

	public void setMinTownPopulation(BigDecimal minTownPopulation) {
		this.minTownPopulation = minTownPopulation;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public BigDecimal getPercentageSeEarnings() {
		return this.percentageSeEarnings;
	}

	public void setPercentageSeEarnings(BigDecimal percentageSeEarnings) {
		this.percentageSeEarnings = percentageSeEarnings;
	}

	public String getProviderPeerGroupName() {
		return this.providerPeerGroupName;
	}

	public void setProviderPeerGroupName(String providerPeerGroupName) {
		this.providerPeerGroupName = providerPeerGroupName;
	}

	public String getProviderPeerGroupType() {
		return this.providerPeerGroupType;
	}

	public void setProviderPeerGroupType(String providerPeerGroupType) {
		this.providerPeerGroupType = providerPeerGroupType;
	}

	public String getTownCodeInclusive() {
		return this.townCodeInclusive;
	}

	public void setTownCodeInclusive(String townCodeInclusive) {
		this.townCodeInclusive = townCodeInclusive;
	}

	public List<DssProviderPeerGroupXref> getDssProviderPeerGroupXrefs() {
		return dssProviderPeerGroupXrefs;
	}

	public void setDssProviderPeerGroupXrefs(List<DssProviderPeerGroupXref> dppgxs) {
		this.dssProviderPeerGroupXrefs = dppgxs;
	}

	/**
	 * @return the dssShadProvPeerGroupXrefs
	 */
	public List<DssShadProvPeerGroupXref> getDssShadProvPeerGroupXrefs() {
		return dssShadProvPeerGroupXrefs;
	}

	/**
	 * @param dssShadProvPeerGroupXrefs
	 *            the dssShadProvPeerGroupXrefs to set
	 */
	public void setDssShadProvPeerGroupXrefs(List<DssShadProvPeerGroupXref> dsppgx) {
		this.dssShadProvPeerGroupXrefs = dsppgx;
	}

	public List<DssProvTypePeerXref> getDssProvTypePeerXrefs() {
		return dssProvTypePeerXrefs;
	}

	public void setDssProvTypePeerXrefs(List<DssProvTypePeerXref> dptpxr) {
		this.dssProvTypePeerXrefs = dptpxr;
	}

	/**
	 * @return the dssSpecialtyPeerGroupXrefs
	 */
	public List<DssSpecialtyPeerGroupXref> getDssSpecialtyPeerGroupXrefs() {
		return dssSpecialtyPeerGroupXrefs;
	}

	/**
	 * @param dssSpecialtyPeerGroupXrefs
	 *            the dssSpecialtyPeerGroupXrefs to set
	 */
	public void setDssSpecialtyPeerGroupXrefs(List<DssSpecialtyPeerGroupXref> dspgxr) {
		this.dssSpecialtyPeerGroupXrefs = dspgxr;
	}

	public List<DssTownPeerGroupXref> getDssTownPeerGroupXrefs() {
		return dssTownPeerGroupXrefs;
	}

	public void setDssTownPeerGroupXrefs(List<DssTownPeerGroupXref> dtpgx) {
		this.dssTownPeerGroupXrefs = dtpgx;
	}

	public List<DssHealthServicePeerXref> getDssHealthServicePeerXrefs() {
		return dssHealthServicePeerXrefs;
	}

	public void setDssHealthServicePeerXrefs(List<DssHealthServicePeerXref> dssHealthServicePeerXrefs) {
		this.dssHealthServicePeerXrefs = dssHealthServicePeerXrefs;
	}

	@Override
	public String toString() {
		return "DssProviderPeerGroup [dssProvTypePeerXrefs=" + dssProvTypePeerXrefs + ", dssSpecialtyPeerGroupXrefs="
				+ dssSpecialtyPeerGroupXrefs + ", dssProviderPeerGroupXrefs=" + dssProviderPeerGroupXrefs
				+ ", dssShadProvPeerGroupXrefs=" + dssShadProvPeerGroupXrefs + ", dssTownPeerGroupXrefs="
				+ dssTownPeerGroupXrefs + ", dssHealthServicePeerXrefs=" + dssHealthServicePeerXrefs + ", id=" + id
				+ ", actMinimumEarnings=" + actMinimumEarnings + ", actMinimumNumberOfRx=" + actMinimumNumberOfRx
				+ ", actMinimumShadowEarnings=" + actMinimumShadowEarnings + ", lastModified=" + lastModified
				+ ", maxEarnings=" + maxEarnings + ", maxTownPopulation=" + maxTownPopulation + ", minEarnings="
				+ minEarnings + ", minTownPopulation=" + minTownPopulation + ", modifiedBy=" + modifiedBy
				+ ", percentageSeEarnings=" + percentageSeEarnings + ", providerPeerGroupName=" + providerPeerGroupName
				+ ", providerPeerGroupType=" + providerPeerGroupType + ", townCodeInclusive=" + townCodeInclusive + "]";
	}

}
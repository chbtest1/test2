package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the DSS_PROVIDER_PEER_GROUP_XREF database table.
 * AKA Fee For Service Practitioners
 */
@Entity
@Table(name = "DSS_PROVIDER_PEER_GROUP_XREF")
public class DssProviderPeerGroupXref implements Serializable {
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "PROVIDER_PEER_GROUP_ID", referencedColumnName = "PROVIDER_PEER_GROUP_ID"),
			@JoinColumn(name = "YEAR_END_DATE", referencedColumnName = "YEAR_END_DATE") })
	private DssProviderPeerGroup dssProviderPeerGroup3;

	@OneToOne(mappedBy = "dppgx")
	private DssProvider2 dp2;

	@EmbeddedId
	private DssProviderPeerGroupXrefPK id;

	@Column(name = "DROP_INDICATOR")
	private String dropIndicator;

	@Column(name = "FORCED_INDICATOR")
	private String forcedIndicator;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	public DssProviderPeerGroupXref() {
	}

	public DssProviderPeerGroup getDssProviderPeerGroup3() {
		return dssProviderPeerGroup3;
	}

	public void setDssProviderPeerGroup3(DssProviderPeerGroup dppg3) {
		this.dssProviderPeerGroup3 = dppg3;
	}

	public DssProviderPeerGroupXrefPK getId() {
		return this.id;
	}

	public void setId(DssProviderPeerGroupXrefPK id) {
		this.id = id;
	}

	public String getDropIndicator() {
		return this.dropIndicator;
	}

	public void setDropIndicator(String dropIndicator) {
		this.dropIndicator = dropIndicator;
	}

	public String getForcedIndicator() {
		return this.forcedIndicator;
	}

	public void setForcedIndicator(String forcedIndicator) {
		this.forcedIndicator = forcedIndicator;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public DssProvider2 getDp2() {
		return dp2;
	}

	public void setDp2(DssProvider2 dp2) {
		this.dp2 = dp2;
	}

//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see java.lang.Object#toString()
//	 */
//	@Override
//	public String toString() {
//		return "DssProviderPeerGroupXref [dp2=" + dp2 + ", id=" + id + ", dropIndicator=" + dropIndicator
//				+ ", forcedIndicator=" + forcedIndicator + ", lastModified=" + lastModified + ", modifiedBy="
//				+ modifiedBy + "]";
//	}

}
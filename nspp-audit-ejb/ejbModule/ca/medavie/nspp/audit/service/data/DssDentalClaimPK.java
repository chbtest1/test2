package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_DENTAL_CLAIMS database table.
 * 
 */
@Embeddable
public class DssDentalClaimPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="CLAIM_NUMBER")
	private long claimNumber;

	@Column(name="ITEM_CODE")
	private long itemCode;

	public DssDentalClaimPK() {
	}
	public long getClaimNumber() {
		return this.claimNumber;
	}
	public void setClaimNumber(long claimNumber) {
		this.claimNumber = claimNumber;
	}
	public long getItemCode() {
		return this.itemCode;
	}
	public void setItemCode(long itemCode) {
		this.itemCode = itemCode;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssDentalClaimPK)) {
			return false;
		}
		DssDentalClaimPK castOther = (DssDentalClaimPK)other;
		return 
			(this.claimNumber == castOther.claimNumber)
			&& (this.itemCode == castOther.itemCode);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.claimNumber ^ (this.claimNumber >>> 32)));
		hash = hash * prime + ((int) (this.itemCode ^ (this.itemCode >>> 32)));
		
		return hash;
	}
}
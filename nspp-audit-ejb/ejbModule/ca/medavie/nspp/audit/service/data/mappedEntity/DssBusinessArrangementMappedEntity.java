package ca.medavie.nspp.audit.service.data.mappedEntity;

import javax.persistence.*;

/**
 * The persistent class for the DSS_HEALTH_SERVICE database table.
 * 
 */
@Entity
@SqlResultSetMapping(name = "DssBusinessArrangementQueryMapping", entities = { @EntityResult(entityClass = DssBusinessArrangementMappedEntity.class, fields = {
	@FieldResult(name = "bus_arr_number", column = "bus_arr_number"),
	@FieldResult(name = "bus_arr_description", column = "bus_arr_description")	}) })
public class DssBusinessArrangementMappedEntity {
	@Id
	private Long bus_arr_number; 

	@Column
	private String bus_arr_description;

	public Long getBus_arr_number() {
		return bus_arr_number;
	}

	public void setBus_arr_number(Long bus_arr_number) {
		this.bus_arr_number = bus_arr_number;
	}

	public String getBus_arr_description() {
		return bus_arr_description;
	}

	public void setBus_arr_description(String bus_arr_description) {
		this.bus_arr_description = bus_arr_description;
	}
}
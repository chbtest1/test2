package ca.medavie.nspp.audit.service.data;

import java.math.BigDecimal;

/**
 * Holds raw details of Payment Information for either Practitioner or PeerGroup. The details held here are used to
 * calculate the final Payment Information
 */
public class PaymentDetails {

	private BigDecimal totalNumberOfReimbursements;
	private BigDecimal totalNumberOfBottomLineAdjustments;
	private BigDecimal sessionalPay;
	private BigDecimal psychiatricPay;
	private BigDecimal salaryPerPay;
	private BigDecimal diagImaging;
	private BigDecimal diagImagingAverage;
	private BigDecimal bottomLineAdj;
	private BigDecimal manualDeposits;
	private BigDecimal cheqReconciliation;
	private Long numberOfPatients;
	private BigDecimal totalAmountPaid;
	private BigDecimal totalAmountPaidAverage;

	/**
	 * @return the sessionalPay
	 */
	public BigDecimal getSessionalPay() {
		return sessionalPay;
	}

	/**
	 * @param sessionalPay
	 *            the sessionalPay to set
	 */
	public void setSessionalPay(BigDecimal sessionalPay) {
		this.sessionalPay = sessionalPay;
	}

	/**
	 * @return the psychiatricPay
	 */
	public BigDecimal getPsychiatricPay() {
		return psychiatricPay;
	}

	/**
	 * @param psychiatricPay
	 *            the psychiatricPay to set
	 */
	public void setPsychiatricPay(BigDecimal psychiatricPay) {
		this.psychiatricPay = psychiatricPay;
	}

	/**
	 * @return the salaryPerPay
	 */
	public BigDecimal getSalaryPerPay() {
		return salaryPerPay;
	}

	/**
	 * @param salaryPerPay
	 *            the salaryPerPay to set
	 */
	public void setSalaryPerPay(BigDecimal salaryPerPay) {
		this.salaryPerPay = salaryPerPay;
	}

	/**
	 * @return the diagImaging
	 */
	public BigDecimal getDiagImaging() {
		return diagImaging;
	}

	/**
	 * @param diagImaging
	 *            the diagImaging to set
	 */
	public void setDiagImaging(BigDecimal diagImaging) {
		this.diagImaging = diagImaging;
	}

	/**
	 * @return the diagImagingAverage
	 */
	public BigDecimal getDiagImagingAverage() {
		return diagImagingAverage;
	}

	/**
	 * @param diagImagingAverage
	 *            the diagImagingAverage to set
	 */
	public void setDiagImagingAverage(BigDecimal diagImagingAverage) {
		this.diagImagingAverage = diagImagingAverage;
	}

	/**
	 * @return the bottomLineAdj
	 */
	public BigDecimal getBottomLineAdj() {
		return bottomLineAdj;
	}

	/**
	 * @param bottomLineAdj
	 *            the bottomLineAdj to set
	 */
	public void setBottomLineAdj(BigDecimal bottomLineAdj) {
		this.bottomLineAdj = bottomLineAdj;
	}

	/**
	 * @return the manualDeposits
	 */
	public BigDecimal getManualDeposits() {
		return manualDeposits;
	}

	/**
	 * @param manualDeposits
	 *            the manualDeposits to set
	 */
	public void setManualDeposits(BigDecimal manualDeposits) {
		this.manualDeposits = manualDeposits;
	}

	/**
	 * @return the cheqReconciliation
	 */
	public BigDecimal getCheqReconciliation() {
		return cheqReconciliation;
	}

	/**
	 * @param cheqReconciliation
	 *            the cheqReconciliation to set
	 */
	public void setCheqReconciliation(BigDecimal cheqReconciliation) {
		this.cheqReconciliation = cheqReconciliation;
	}

	/**
	 * @return the totalNumberOfReimbursements
	 */
	public BigDecimal getTotalNumberOfReimbursements() {
		return totalNumberOfReimbursements;
	}

	/**
	 * @param totalNumberOfReimbursements
	 *            the totalNumberOfReimbursements to set
	 */
	public void setTotalNumberOfReimbursements(BigDecimal totalNumberOfReimbursements) {
		this.totalNumberOfReimbursements = totalNumberOfReimbursements;
	}

	/**
	 * @return the totalNumberOfBottomLineAdjustments
	 */
	public BigDecimal getTotalNumberOfBottomLineAdjustments() {
		return totalNumberOfBottomLineAdjustments;
	}

	/**
	 * @param totalNumberOfBottomLineAdjustments
	 *            the totalNumberOfBottomLineAdjustments to set
	 */
	public void setTotalNumberOfBottomLineAdjustments(BigDecimal totalNumberOfBottomLineAdjustments) {
		this.totalNumberOfBottomLineAdjustments = totalNumberOfBottomLineAdjustments;
	}

	/**
	 * @return the numberOfPatients
	 */
	public Long getNumberOfPatients() {
		return numberOfPatients;
	}

	/**
	 * @param numberOfPatients
	 *            the numberOfPatients to set
	 */
	public void setNumberOfPatients(Long numberOfPatients) {
		this.numberOfPatients = numberOfPatients;
	}

	/**
	 * @return the totalAmountPaid
	 */
	public BigDecimal getTotalAmountPaid() {
		return totalAmountPaid;
	}

	/**
	 * @param totalAmountPaid
	 *            the totalAmountPaid to set
	 */
	public void setTotalAmountPaid(BigDecimal totalAmountPaid) {
		this.totalAmountPaid = totalAmountPaid;
	}

	/**
	 * @return the totalAmountPaidAverage
	 */
	public BigDecimal getTotalAmountPaidAverage() {
		return totalAmountPaidAverage;
	}

	/**
	 * @param totalAmountPaidAverage
	 *            the totalAmountPaidAverage to set
	 */
	public void setTotalAmountPaidAverage(BigDecimal totalAmountPaidAverage) {
		this.totalAmountPaidAverage = totalAmountPaidAverage;
	}

}

package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the DSS_PROVIDER_AUDIT database table.
 * 
 */
@Entity
@Table(name = "DSS_PROVIDER_AUDIT")
public class DssProviderAudit implements Serializable {
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "PROVIDER_NUMBER", referencedColumnName = "PROVIDER_NUMBER"),
			@JoinColumn(name = "PROVIDER_TYPE", referencedColumnName = "PROVIDER_TYPE") })
	private DssProvider2 aDSssProvider;

	// association with DssProviderAuditNote through DssProviderAuditNotesXref
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL, CascadeType.MERGE }, mappedBy = "dssProviderAudit")
	private List<DssProviderAuditNotesXref> dssProviderAuditNotesXrefs;

	// many-to-one association to DssProviderAuditAttachments
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL, CascadeType.MERGE }, orphanRemoval = true, mappedBy = "dssProviderAudit")
	private List<DssProviderAuditAttach> dssProviderAuditAttachments;

	@Id
	@Column(name = "AUDIT_ID")
	private Long auditId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "AUDIT_DATE")
	private Date auditDate;

	@Column(name = "PROVIDER_NUMBER")
	private Long providerNumber;

	@Column(name = "PROVIDER_TYPE")
	private String providerType;

	@Temporal(TemporalType.DATE)
	@Column(name = "AUDIT_END_DATE")
	private Date auditEndDate;

	@Column(name = "AUDIT_INDICATOR")
	private String auditIndicator;

	@Column(name = "AUDIT_PERSON")
	private String auditPerson;

	@Column(name = "AUDIT_REASON")
	private String auditReason;

	@Column(name = "AUDIT_RECOVERY_AMOUNT")
	private BigDecimal auditRecoveryAmount;

	@Column(name = "AUDIT_SOURCE")
	private String auditSource;

	@Temporal(TemporalType.DATE)
	@Column(name = "AUDIT_START_DATE")
	private Date auditStartDate;

	@Column(name = "AUDIT_TYPE")
	private String auditType;

	@Temporal(TemporalType.DATE)
	@Column(name = "FINAL_CONTACT_DATE")
	private Date finalContactDate;

	@Column(name = "HEALTH_SERVICE_CODE")
	private String healthServiceCode;

	@Column(name = "INAPPROPRIATLY_BILLED_SERVICES")
	private BigDecimal inappropriatlyBilledServices;

	@Temporal(TemporalType.DATE)
	@Column(name = "INTERIM_DATE")
	private Date interimDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	@Column(name = "NUMBER_OF_SERVICES_AUDITED")
	private BigDecimal numberOfServicesAudited;

	@Column(name = "PHARM_SOURCE")
	private String pharmSource;

	@Column(name = "PHARM_TYPE")
	private String pharmType;

	@Column(name = "QUALIFIER")
	private String qualifier;

	public DssProviderAudit() {
	}

	/**
	 * @return the aDSssProvider
	 */
	public DssProvider2 getaDSssProvider() {
		return aDSssProvider;
	}

	/**
	 * @param aDSssProvider
	 *            the aDSssProvider to set
	 */
	public void setaDSssProvider(DssProvider2 aDSssProvider) {
		this.aDSssProvider = aDSssProvider;
	}

	public Long getAuditId() {
		return this.auditId;
	}

	public void setAuditId(Long auditId) {
		this.auditId = auditId;
	}

	public Date getAuditDate() {
		return this.auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public Date getAuditEndDate() {
		return this.auditEndDate;
	}

	public void setAuditEndDate(Date auditEndDate) {
		this.auditEndDate = auditEndDate;
	}

	public String getAuditIndicator() {
		return this.auditIndicator;
	}

	public void setAuditIndicator(String auditIndicator) {
		this.auditIndicator = auditIndicator;
	}

	public String getAuditPerson() {
		return this.auditPerson;
	}

	public void setAuditPerson(String auditPerson) {
		this.auditPerson = auditPerson;
	}

	public String getAuditReason() {
		return this.auditReason;
	}

	public void setAuditReason(String auditReason) {
		this.auditReason = auditReason;
	}

	public BigDecimal getAuditRecoveryAmount() {
		return this.auditRecoveryAmount;
	}

	public void setAuditRecoveryAmount(BigDecimal auditRecoveryAmount) {
		this.auditRecoveryAmount = auditRecoveryAmount;
	}

	public String getAuditSource() {
		return this.auditSource;
	}

	public void setAuditSource(String auditSource) {
		this.auditSource = auditSource;
	}

	public Date getAuditStartDate() {
		return this.auditStartDate;
	}

	public void setAuditStartDate(Date auditStartDate) {
		this.auditStartDate = auditStartDate;
	}

	public String getAuditType() {
		return this.auditType;
	}

	public void setAuditType(String auditType) {
		this.auditType = auditType;
	}

	public Date getFinalContactDate() {
		return this.finalContactDate;
	}

	public void setFinalContactDate(Date finalContactDate) {
		this.finalContactDate = finalContactDate;
	}

	public String getHealthServiceCode() {
		return this.healthServiceCode;
	}

	public void setHealthServiceCode(String healthServiceCode) {
		this.healthServiceCode = healthServiceCode;
	}

	public BigDecimal getInappropriatlyBilledServices() {
		return this.inappropriatlyBilledServices;
	}

	public void setInappropriatlyBilledServices(BigDecimal inappropriatlyBilledServices) {
		this.inappropriatlyBilledServices = inappropriatlyBilledServices;
	}

	public Date getInterimDate() {
		return this.interimDate;
	}

	public void setInterimDate(Date interimDate) {
		this.interimDate = interimDate;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public BigDecimal getNumberOfServicesAudited() {
		return this.numberOfServicesAudited;
	}

	public void setNumberOfServicesAudited(BigDecimal numberOfServicesAudited) {
		this.numberOfServicesAudited = numberOfServicesAudited;
	}

	public String getPharmSource() {
		return this.pharmSource;
	}

	public void setPharmSource(String pharmSource) {
		this.pharmSource = pharmSource;
	}

	public String getPharmType() {
		return this.pharmType;
	}

	public void setPharmType(String pharmType) {
		this.pharmType = pharmType;
	}

	public Long getProviderNumber() {
		return this.providerNumber;
	}

	public void setProviderNumber(Long providerNumber) {
		this.providerNumber = providerNumber;
	}

	public String getProviderType() {
		return this.providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public String getQualifier() {
		return this.qualifier;
	}

	public void setQualifier(String qualifier) {
		this.qualifier = qualifier;
	}

	public List<DssProviderAuditNotesXref> getDssProviderAuditNotesXrefs() {
		return this.dssProviderAuditNotesXrefs;
	}

	public void setDssProviderAuditNotesXrefs(List<DssProviderAuditNotesXref> dssProviderAuditNotesXrefs) {
		this.dssProviderAuditNotesXrefs = dssProviderAuditNotesXrefs;
	}

	public DssProviderAuditNotesXref addDssProviderAuditNotesXref(DssProviderAuditNotesXref dssProviderAuditNotesXref) {
		getDssProviderAuditNotesXrefs().add(dssProviderAuditNotesXref);
		dssProviderAuditNotesXref.setDssProviderAudit(this);

		return dssProviderAuditNotesXref;
	}

	public DssProviderAuditNotesXref removeDssProviderAuditNotesXref(DssProviderAuditNotesXref dssProviderAuditNotesXref) {
		getDssProviderAuditNotesXrefs().remove(dssProviderAuditNotesXref);
		dssProviderAuditNotesXref.setDssProviderAudit(null);

		return dssProviderAuditNotesXref;
	}

	public List<DssProviderAuditAttach> getDssProviderAuditAttachments() {
		return this.dssProviderAuditAttachments;
	}

	public void setDssProviderAuditAttachments(List<DssProviderAuditAttach> dssProviderAuditAttachments) {
		this.dssProviderAuditAttachments = dssProviderAuditAttachments;
	}

	public DssProviderAuditAttach addDssProviderAuditAttachment(DssProviderAuditAttach dssProviderAuditAttachment) {
		getDssProviderAuditAttachments().add(dssProviderAuditAttachment);
		dssProviderAuditAttachment.setDssProviderAudit(this);

		return dssProviderAuditAttachment;
	}

	public DssProviderAuditAttach removeDssProviderAuditAttachment(DssProviderAuditAttach dssProviderAuditAttachment) {
		getDssProviderAuditAttachments().remove(dssProviderAuditAttachment);
		dssProviderAuditAttachment.setDssProviderAudit(null);

		return dssProviderAuditAttachment;
	}

}
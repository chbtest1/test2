package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;

/**
 * The persistent class for the DSS_PROVIDER_GROUP database table.
 * 
 */
@Entity
@Table(name = "DSS_PROVIDER_GROUP")
@NamedQueries({ @NamedQuery(name = QueryConstants.DSS_PROVIDER_GROUP_TYPE_QUERY, query = "SELECT DISTINCT dpg.providerGroupTypeDesc FROM DssProviderGroup dpg ORDER BY dpg.providerGroupTypeDesc ASC") })
public class DssProviderGroup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PROVIDER_GROUP_ID")
	private long providerGroupId;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PROVIDER_GROUP_ID", referencedColumnName = "PROVIDER_GROUP_ID", insertable = false, updatable = false)
	private DssProviderGroupXref dpgx;

	@Column(name = "OLD_PROVIDER_GROUP_ID")
	private BigDecimal oldProviderGroupId;

	@Column(name = "OLD_PROVIDER_GROUP_TYPE")
	private String oldProviderGroupType;

	@Column(name = "PROVIDER_CONTACT_NAME")
	private String providerContactName;

	@Column(name = "PROVIDER_GROUP_NAME")
	private String providerGroupName;

	@Column(name = "PROVIDER_GROUP_TYPE")
	private BigDecimal providerGroupType;

	@Column(name = "PROVIDER_GROUP_TYPE_DESC")
	private String providerGroupTypeDesc;

	public DssProviderGroup() {
	}

	public long getProviderGroupId() {
		return this.providerGroupId;
	}

	public void setProviderGroupId(long providerGroupId) {
		this.providerGroupId = providerGroupId;
	}

	/**
	 * @return the dpgx
	 */
	public DssProviderGroupXref getDpgx() {
		return dpgx;
	}

	/**
	 * @param dpgx
	 *            the dpgx to set
	 */
	public void setDpgx(DssProviderGroupXref dpgx) {
		this.dpgx = dpgx;
	}

	public BigDecimal getOldProviderGroupId() {
		return this.oldProviderGroupId;
	}

	public void setOldProviderGroupId(BigDecimal oldProviderGroupId) {
		this.oldProviderGroupId = oldProviderGroupId;
	}

	public String getOldProviderGroupType() {
		return this.oldProviderGroupType;
	}

	public void setOldProviderGroupType(String oldProviderGroupType) {
		this.oldProviderGroupType = oldProviderGroupType;
	}

	public String getProviderContactName() {
		return this.providerContactName;
	}

	public void setProviderContactName(String providerContactName) {
		this.providerContactName = providerContactName;
	}

	public String getProviderGroupName() {
		return this.providerGroupName;
	}

	public void setProviderGroupName(String providerGroupName) {
		this.providerGroupName = providerGroupName;
	}

	public BigDecimal getProviderGroupType() {
		return this.providerGroupType;
	}

	public void setProviderGroupType(BigDecimal providerGroupType) {
		this.providerGroupType = providerGroupType;
	}

	public String getProviderGroupTypeDesc() {
		return this.providerGroupTypeDesc;
	}

	public void setProviderGroupTypeDesc(String providerGroupTypeDesc) {
		this.providerGroupTypeDesc = providerGroupTypeDesc;
	}

}
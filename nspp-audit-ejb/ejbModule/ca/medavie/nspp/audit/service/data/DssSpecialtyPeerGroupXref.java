package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the DSS_SPECIALTY_PEER_GROUP_XREF database table.
 * 
 */
@Entity
@Table(name = "DSS_SPECIALTY_PEER_GROUP_XREF")
public class DssSpecialtyPeerGroupXref implements Serializable {
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "PROVIDER_PEER_GROUP_ID", referencedColumnName = "PROVIDER_PEER_GROUP_ID"),
			@JoinColumn(name = "YEAR_END_DATE", referencedColumnName = "YEAR_END_DATE") })
	private DssProviderPeerGroup dssProviderPeerGroup2;

	@OneToMany
	// it very important to set "updatable=false" otherwise will get : Caused by:
	// org.apache.openjpa.lib.jdbc.ReportingSQLException: ORA-01407: cannot update
	// ("ACP_CONV"."DSS_CODE_TABLE_ALPHA_ENTRY"."CODE_TABLE_ALPHA_ENTRY") to NULL
	// {prepstmnt -1442963419 UPDATE DSS_CODE_TABLE_ALPHA_ENTRY SET CODE_TABLE_ALPHA_ENTRY = ? WHERE
	// CODE_TABLE_ALPHA_ENTRY = ? [params=(null) null, (String) MDON ]} [code=1407, state=72000]
	@JoinColumn(name = "CODE_TABLE_ALPHA_ENTRY", referencedColumnName = "SPECIALTY_CODE", updatable = false)
	private List<DssCodeTableAlphaEntry> dctae;
	
	// convenient property for mapping purpose
	@Transient
	private DssCodeTableAlphaEntry mappedDssCodeTableAlphaEntry;

	@EmbeddedId
	private DssSpecialtyPeerGroupXrefPK id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "MINIMUM_EARNINGS")
	private BigDecimal minimumEarnings;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	public DssSpecialtyPeerGroupXref() {
	}

	public DssSpecialtyPeerGroupXrefPK getId() {
		return this.id;
	}

	public void setId(DssSpecialtyPeerGroupXrefPK id) {
		this.id = id;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public BigDecimal getMinimumEarnings() {
		return this.minimumEarnings;
	}

	public void setMinimumEarnings(BigDecimal minimumEarnings) {
		this.minimumEarnings = minimumEarnings;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the dssProviderPeerGroup
	 */
	public DssProviderPeerGroup getDssProviderPeerGroup2() {
		return dssProviderPeerGroup2;
	}

	/**
	 * @param dssProviderPeerGroup
	 *            the dssProviderPeerGroup to set
	 */
	public void setDssProviderPeerGroup2(DssProviderPeerGroup dppg2) {
		this.dssProviderPeerGroup2 = dppg2;
	}

	public List<DssCodeTableAlphaEntry> getDctae() {
		return dctae;
	}

	public void setDctae(List<DssCodeTableAlphaEntry> dctae) {
		this.dctae = dctae;
	}

	public DssCodeTableAlphaEntry getMappedDssCodeTableAlphaEntry() {

		if (dctae != null && !dctae.isEmpty()) {

			for (DssCodeTableAlphaEntry entry : dctae) {
				// magic number 102!!
				if (entry.getId().getCodeTableAlphaNumber() == 102) {

					mappedDssCodeTableAlphaEntry = entry;
					return mappedDssCodeTableAlphaEntry;
				}
			}
		}

		return mappedDssCodeTableAlphaEntry;
	}

	public void setmappedDssCodeTableAlphaEntry(DssCodeTableAlphaEntry wantedDssCodeTableAlphaEntry) {
		this.mappedDssCodeTableAlphaEntry = wantedDssCodeTableAlphaEntry;
	}

}
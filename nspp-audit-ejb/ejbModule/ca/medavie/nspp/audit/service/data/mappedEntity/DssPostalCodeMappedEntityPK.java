package ca.medavie.nspp.audit.service.data.mappedEntity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DssPostalCodeMappedEntityPK {

	@Column
	private BigDecimal town_code;
	@Column
	private BigDecimal county_code;
	@Column
	private BigDecimal municipality_code;
	@Column
	private BigDecimal health_region_code;

	public BigDecimal getTown_code() {
		return town_code;
	}

	public void setTown_code(BigDecimal town_code) {
		this.town_code = town_code;
	}

	public BigDecimal getCounty_code() {
		return county_code;
	}

	public void setCounty_code(BigDecimal county_code) {
		this.county_code = county_code;
	}

	public BigDecimal getMunicipality_code() {
		return municipality_code;
	}

	public void setMunicipality_code(BigDecimal municipality_code) {
		this.municipality_code = municipality_code;
	}

	public BigDecimal getHealth_region_code() {
		return health_region_code;
	}

	public void setHealth_region_code(BigDecimal health_region_code) {
		this.health_region_code = health_region_code;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((county_code == null) ? 0 : county_code.hashCode());
		result = prime * result + ((health_region_code == null) ? 0 : health_region_code.hashCode());
		result = prime * result + ((municipality_code == null) ? 0 : municipality_code.hashCode());
		result = prime * result + ((town_code == null) ? 0 : town_code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DssPostalCodeMappedEntityPK other = (DssPostalCodeMappedEntityPK) obj;
		if (county_code == null) {
			if (other.county_code != null)
				return false;
		} else if (!county_code.equals(other.county_code))
			return false;
		if (health_region_code == null) {
			if (other.health_region_code != null)
				return false;
		} else if (!health_region_code.equals(other.health_region_code))
			return false;
		if (municipality_code == null) {
			if (other.municipality_code != null)
				return false;
		} else if (!municipality_code.equals(other.municipality_code))
			return false;
		if (town_code == null) {
			if (other.town_code != null)
				return false;
		} else if (!town_code.equals(other.town_code))
			return false;
		return true;
	}

}

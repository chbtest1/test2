package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_MED_AUD_PAY_TO_CD_XREF database table.
 * 
 */
@Embeddable
public class DssMedAudPayToCdXrefPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="AUDIT_CRITERIA_ID")
	private long auditCriteriaId;

	@Column(name="PAY_TO_CODE")
	private String payToCode;

	public DssMedAudPayToCdXrefPK() {
	}
	public long getAuditCriteriaId() {
		return this.auditCriteriaId;
	}
	public void setAuditCriteriaId(long auditCriteriaId) {
		this.auditCriteriaId = auditCriteriaId;
	}
	public String getPayToCode() {
		return this.payToCode;
	}
	public void setPayToCode(String payToCode) {
		this.payToCode = payToCode;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssMedAudPayToCdXrefPK)) {
			return false;
		}
		DssMedAudPayToCdXrefPK castOther = (DssMedAudPayToCdXrefPK)other;
		return 
			(this.auditCriteriaId == castOther.auditCriteriaId)
			&& this.payToCode.equals(castOther.payToCode);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.auditCriteriaId ^ (this.auditCriteriaId >>> 32)));
		hash = hash * prime + this.payToCode.hashCode();
		
		return hash;
	}
}
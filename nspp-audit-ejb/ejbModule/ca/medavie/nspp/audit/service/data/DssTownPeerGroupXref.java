package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;
import java.util.List;

/**
 * The persistent class for the DSS_TOWN_PEER_GROUP_XREF database table.
 * 
 */
@Entity
@Table(name = "DSS_TOWN_PEER_GROUP_XREF")
public class DssTownPeerGroupXref implements Serializable {
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "PROVIDER_PEER_GROUP_ID", referencedColumnName = "PROVIDER_PEER_GROUP_ID"),
			@JoinColumn(name = "YEAR_END_DATE", referencedColumnName = "YEAR_END_DATE") })
	private DssProviderPeerGroup dssProviderPeerGroup5;

	@OneToMany
	// it very important to set "updatable=false" otherwise will get : Caused by:
	// org.apache.openjpa.lib.jdbc.ReportingSQLException: ORA-01407: cannot update
	// ("ACP_CONV"."DSS_CODE_TABLE_ALPHA_ENTRY"."CODE_TABLE_ALPHA_ENTRY") to NULL
	// {prepstmnt -1442963419 UPDATE DSS_CODE_TABLE_ALPHA_ENTRY SET CODE_TABLE_ALPHA_ENTRY = ? WHERE
	// CODE_TABLE_ALPHA_ENTRY = ? [params=(null) null, (String) MDON ]} [code=1407, state=72000]
	@JoinColumn(name = "TOWN_CODE", referencedColumnName = "TOWN_CODE", updatable = false)
	private List<DssPostalCode> aDssPostalCodeList;

	// convenient property for mapping purpose
	@Transient
	private DssPostalCode aDssPostalCode;

	@EmbeddedId
	private DssTownPeerGroupXrefPK id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	public DssTownPeerGroupXref() {
	}

	public DssTownPeerGroupXrefPK getId() {
		return this.id;
	}

	public void setId(DssTownPeerGroupXrefPK id) {
		this.id = id;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}


	public List<DssPostalCode> getaDssPostalCodeList() {
		return aDssPostalCodeList;
	}

	public void setaDssPostalCodeList(List<DssPostalCode> aDssPostalCode) {
		this.aDssPostalCodeList = aDssPostalCode;
	}

	public DssPostalCode getaDssPostalCode() {

		// we know they are all the same in the list, so We only need get the first one.
		if (aDssPostalCodeList != null && !aDssPostalCodeList.isEmpty()) {
			return aDssPostalCodeList.get(0);
		}

		return aDssPostalCode;
	}

	public void setaDssPostalCode(DssPostalCode aDssPostalCode) {
		this.aDssPostalCode = aDssPostalCode;
	}

	public DssProviderPeerGroup getDssProviderPeerGroup5() {
		return dssProviderPeerGroup5;
	}

	public void setDssProviderPeerGroup5(DssProviderPeerGroup dssProviderPeerGroup) {
		this.dssProviderPeerGroup5 = dssProviderPeerGroup;
	}

}
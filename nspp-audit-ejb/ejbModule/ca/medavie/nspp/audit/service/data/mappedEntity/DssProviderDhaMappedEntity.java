package ca.medavie.nspp.audit.service.data.mappedEntity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.SqlResultSetMapping;

/**
 * The persistent class for the DSS_PROVIDER_DHA database table.
 */
@Entity
@SqlResultSetMapping(name = "DssProviderDhaQueryMapping", entities = { @EntityResult(entityClass = DssProviderDhaMappedEntity.class, fields = {
		@FieldResult(name = "id.pdha_id", column = "pdha_id"),
		@FieldResult(name = "id.provider_number", column = "provider_number"),
		@FieldResult(name = "id.provider_type", column = "provider_type"),
		@FieldResult(name = "id.provider_name", column = "provider_name"),
		@FieldResult(name = "id.contract_town_name", column = "contract_town_name"),
		@FieldResult(name = "id.zone_id", column = "zone_id"),
		@FieldResult(name = "id.zone_name", column = "zone_name"),
		@FieldResult(name = "id.dha_code", column = "dha_code"),
		@FieldResult(name = "id.dha_desc", column = "dha_desc"),
		@FieldResult(name = "id.emp_type", column = "emp_type"),
		@FieldResult(name = "id.fte_value", column = "fte_value"),
		@FieldResult(name = "id.bus_arr_number", column = "bus_arr_number"),
		@FieldResult(name = "id.modified_by", column = "modified_by"),
		@FieldResult(name = "id.last_modified", column = "last_modified") ,
		@FieldResult(name = "id.provider_group_id", column = "provider_group_id"),
		@FieldResult(name = "id.provider_group_description", column = "provider_group_description") }) })
public class DssProviderDhaMappedEntity {

	@EmbeddedId
	private DssProviderDhaMappedEntityPK id;

	/**
	 * @return the id
	 */
	public DssProviderDhaMappedEntityPK getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(DssProviderDhaMappedEntityPK id) {
		this.id = id;
	}
}

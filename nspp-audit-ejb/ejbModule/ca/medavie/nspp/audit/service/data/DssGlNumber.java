package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the DSS_GL_NUMBER database table.
 * 
 */
@Entity
@Table(name = "DSS_GL_NUMBER")
public class DssGlNumber implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "GL_NUMBER")
	private long glNumber;

	@Column(name = "CAPPED_INDICATOR")
	private String cappedIndicator;

	@Temporal(TemporalType.DATE)
	@Column(name = "FISCAL_YEAR_END_DATE")
	private Date fiscalYearEndDate;

	@Column(name = "GL_NUMBER_DESCRIPTION")
	private String glNumberDescription;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	public DssGlNumber() {
	}

	public long getGlNumber() {
		return this.glNumber;
	}

	public void setGlNumber(long glNumber) {
		this.glNumber = glNumber;
	}

	public String getCappedIndicator() {
		return this.cappedIndicator;
	}

	public void setCappedIndicator(String cappedIndicator) {
		this.cappedIndicator = cappedIndicator;
	}

	public Date getFiscalYearEndDate() {
		return this.fiscalYearEndDate;
	}

	public void setFiscalYearEndDate(Date fiscalYearEndDate) {
		this.fiscalYearEndDate = fiscalYearEndDate;
	}

	public String getGlNumberDescription() {
		return this.glNumberDescription;
	}

	public void setGlNumberDescription(String glNumberDescription) {
		this.glNumberDescription = glNumberDescription;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

}
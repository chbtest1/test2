package ca.medavie.nspp.audit.service.data.mappedEntity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

/**
 * The persistent class for the DSS_MANUAL_DEPOSITS database table.
 * 
 */
@Entity
@SqlResultSetMapping(name = "DssManualDepositsBaseInfoQueryMapping", entities = { @EntityResult(entityClass = DssManualDepositsMappedEntity.class, fields = {
	@FieldResult(name = "deposit_number", column = "deposit_number"),
	@FieldResult(name = "provider_number", column = "provider_number"),
	@FieldResult(name = "provider_group_name", column = "provider_group_name"),
	@FieldResult(name = "provider_type", column = "provider_type"),
	@FieldResult(name = "organization_name", column = "organization_name"),
	@FieldResult(name = "first_name", column = "first_name"),
	@FieldResult(name = "last_name", column = "last_name"),
	@FieldResult(name = "middle_name", column = "middle_name"),
	@FieldResult(name = "bus_arr_number", column = "bus_arr_number"),
	@FieldResult(name = "fiscal_year_end_date", column = "fiscal_year_end_date"),
	@FieldResult(name = "gl_number", column = "gl_number"),
	@FieldResult(name = "deposit_amount", column = "deposit_amount"),
	@FieldResult(name = "deposit_note", column = "deposit_note"),
	@FieldResult(name = "date_received", column = "date_received"),
	@FieldResult(name = "last_modified", column = "last_modified"),
	@FieldResult(name = "modified_by", column = "modified_by"),
	@FieldResult(name = "bus_arr_description", column = "bus_arr_description") 	}) })
public class DssManualDepositsMappedEntity {
	@Id
	private long deposit_number;
	
	@Column
	private BigDecimal provider_number;
	@Column
	private BigDecimal provider_group_id;
	@Column
	private String provider_group_name;
	@Column
	private String provider_type;
	@Column
	private String organization_name;
	@Column
	private String first_name;
	@Column
	private String last_name;
	@Column
	private String middle_name;
	@Column
	private BigDecimal bus_arr_number;
	@Column
	private Date fiscal_year_end_date;
	@Column
	private BigDecimal gl_number;
	@Column
	private BigDecimal deposit_amount;
	@Column
	private String deposit_note;
	@Column
	private Date date_received;
	@Column
	private Date last_modified;
	@Column
	private String modified_by;
	@Column
	private String bus_arr_description;
	
	public long getDeposit_number() {
		return deposit_number;
	}
	public void setDeposit_number(long deposit_number) {
		this.deposit_number = deposit_number;
	}
	public BigDecimal getProvider_number() {
		return provider_number;
	}
	public void setProvider_number(BigDecimal provider_number) {
		this.provider_number = provider_number;
	}
	public BigDecimal getProvider_group_id() {
		return provider_group_id;
	}
	public void setProvider_group_id(BigDecimal provider_group_id) {
		this.provider_group_id = provider_group_id;
	}
	public String getProvider_group_name() {
		return provider_group_name;
	}
	public void setProvider_group_name(String provider_group_name) {
		this.provider_group_name = provider_group_name;
	}
	public String getProvider_type() {
		return provider_type;
	}
	public void setProvider_type(String provider_type) {
		this.provider_type = provider_type;
	}
	public String getOrganization_name() {
		return organization_name;
	}
	public void setOrganization_name(String organization_name) {
		this.organization_name = organization_name;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getMiddle_name() {
		return middle_name;
	}
	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}
	public BigDecimal getBus_arr_number() {
		return bus_arr_number;
	}
	public void setBus_arr_number(BigDecimal bus_arr_number) {
		this.bus_arr_number = bus_arr_number;
	}
	public Date getFiscal_year_end_date() {
		return fiscal_year_end_date;
	}
	public void setFiscal_year_end_date(Date fiscal_year_end_date) {
		this.fiscal_year_end_date = fiscal_year_end_date;
	}
	public BigDecimal getGl_number() {
		return gl_number;
	}
	public void setGl_number(BigDecimal gl_number) {
		this.gl_number = gl_number;
	}
	public BigDecimal getDeposit_amount() {
		return deposit_amount;
	}
	public void setDeposit_amount(BigDecimal deposit_amount) {
		this.deposit_amount = deposit_amount;
	}
	public String getDeposit_note() {
		return deposit_note;
	}
	public void setDeposit_note(String deposit_note) {
		this.deposit_note = deposit_note;
	}
	public Date getDate_received() {
		return date_received;
	}
	public void setDate_received(Date date_received) {
		this.date_received = date_received;
	}
	public Date getLast_modified() {
		return last_modified;
	}
	public void setLast_modified(Date last_modified) {
		this.last_modified = last_modified;
	}
	public String getModified_by() {
		return modified_by;
	}
	public void setModified_by(String modified_by) {
		this.modified_by = modified_by;
	}
	public String getBus_arr_description() {
		return bus_arr_description;
	}
	public void setBus_arr_description(String bus_arr_description) {
		this.bus_arr_description = bus_arr_description;
	}
}

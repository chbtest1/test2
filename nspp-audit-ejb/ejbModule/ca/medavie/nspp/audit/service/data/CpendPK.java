package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the CPEND database table.
 * 
 */
@Embeddable
public class CpendPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="DATE_PROCESSED")
	private long dateProcessed;

	@Column(name="CARRIER_ID")
	private long carrierId;

	@Column(name="CLAIM_REF_NUM")
	private String claimRefNum;

	@Column(name="TRACE_NUM")
	private String traceNum;

	public CpendPK() {
	}
	public long getDateProcessed() {
		return this.dateProcessed;
	}
	public void setDateProcessed(long dateProcessed) {
		this.dateProcessed = dateProcessed;
	}
	public long getCarrierId() {
		return this.carrierId;
	}
	public void setCarrierId(long carrierId) {
		this.carrierId = carrierId;
	}
	public String getClaimRefNum() {
		return this.claimRefNum;
	}
	public void setClaimRefNum(String claimRefNum) {
		this.claimRefNum = claimRefNum;
	}
	public String getTraceNum() {
		return this.traceNum;
	}
	public void setTraceNum(String traceNum) {
		this.traceNum = traceNum;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CpendPK)) {
			return false;
		}
		CpendPK castOther = (CpendPK)other;
		return 
			(this.dateProcessed == castOther.dateProcessed)
			&& (this.carrierId == castOther.carrierId)
			&& this.claimRefNum.equals(castOther.claimRefNum)
			&& this.traceNum.equals(castOther.traceNum);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.dateProcessed ^ (this.dateProcessed >>> 32)));
		hash = hash * prime + ((int) (this.carrierId ^ (this.carrierId >>> 32)));
		hash = hash * prime + this.claimRefNum.hashCode();
		hash = hash * prime + this.traceNum.hashCode();
		
		return hash;
	}
}
package ca.medavie.nspp.audit.service.data.mappedEntity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DssAuditHistoryTransferMappedEntityPK implements Serializable {

	/** IDE generated */
	private static final long serialVersionUID = 339795081215818554L;

	@Column
	private String se_number;
	@Column
	private String se_sequence_number;
	@Column
	private String response_tag_number;
	@Column
	private String from_health_card_number;
	@Column
	private String to_health_card_number;
	@Column
	private String comments;
	@Column
	private Date transfer_date;
	@Column
	private String modified_by;
	@Column
	private String med;
	@Column
	private String pharm;
	@Column
	private String dent;
	@Column
	private String mf;
	@Column
	private String pf;
	@Column
	private String source_name;
	@Column
	private String target_name;

	/**
	 * @return the se_number
	 */
	public String getSe_number() {
		return se_number;
	}

	/**
	 * @param se_number
	 *            the se_number to set
	 */
	public void setSe_number(String se_number) {
		this.se_number = se_number;
	}

	/**
	 * @return the se_sequence_number
	 */
	public String getSe_sequence_number() {
		return se_sequence_number;
	}

	/**
	 * @param se_sequence_number
	 *            the se_sequence_number to set
	 */
	public void setSe_sequence_number(String se_sequence_number) {
		this.se_sequence_number = se_sequence_number;
	}

	/**
	 * @return the response_tag_number
	 */
	public String getResponse_tag_number() {
		return response_tag_number;
	}

	/**
	 * @param response_tag_number
	 *            the response_tag_number to set
	 */
	public void setResponse_tag_number(String response_tag_number) {
		this.response_tag_number = response_tag_number;
	}

	/**
	 * @return the from_health_card_number
	 */
	public String getFrom_health_card_number() {
		return from_health_card_number;
	}

	/**
	 * @param from_health_card_number
	 *            the from_health_card_number to set
	 */
	public void setFrom_health_card_number(String from_health_card_number) {
		this.from_health_card_number = from_health_card_number;
	}

	/**
	 * @return the to_health_card_number
	 */
	public String getTo_health_card_number() {
		return to_health_card_number;
	}

	/**
	 * @param to_health_card_number
	 *            the to_health_card_number to set
	 */
	public void setTo_health_card_number(String to_health_card_number) {
		this.to_health_card_number = to_health_card_number;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments
	 *            the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the transfer_date
	 */
	public Date getTransfer_date() {
		return transfer_date;
	}

	/**
	 * @param transfer_date
	 *            the transfer_date to set
	 */
	public void setTransfer_date(Date transfer_date) {
		this.transfer_date = transfer_date;
	}

	/**
	 * @return the modified_by
	 */
	public String getModified_by() {
		return modified_by;
	}

	/**
	 * @param modified_by
	 *            the modified_by to set
	 */
	public void setModified_by(String modified_by) {
		this.modified_by = modified_by;
	}

	/**
	 * @return the source_name
	 */
	public String getSource_name() {
		return source_name;
	}

	/**
	 * @param source_name
	 *            the source_name to set
	 */
	public void setSource_name(String source_name) {
		this.source_name = source_name;
	}

	/**
	 * @return the target_name
	 */
	public String getTarget_name() {
		return target_name;
	}

	/**
	 * @param target_name
	 *            the target_name to set
	 */
	public void setTarget_name(String target_name) {
		this.target_name = target_name;
	}

	/**
	 * @return the med
	 */
	public String getMed() {
		return med;
	}

	/**
	 * @param med
	 *            the med to set
	 */
	public void setMed(String med) {
		this.med = med;
	}

	/**
	 * @return the pharm
	 */
	public String getPharm() {
		return pharm;
	}

	/**
	 * @param pharm
	 *            the pharm to set
	 */
	public void setPharm(String pharm) {
		this.pharm = pharm;
	}

	/**
	 * @return the dent
	 */
	public String getDent() {
		return dent;
	}

	/**
	 * @param dent
	 *            the dent to set
	 */
	public void setDent(String dent) {
		this.dent = dent;
	}

	/**
	 * @return the mf
	 */
	public String getMf() {
		return mf;
	}

	/**
	 * @param mf
	 *            the mf to set
	 */
	public void setMf(String mf) {
		this.mf = mf;
	}

	/**
	 * @return the pf
	 */
	public String getPf() {
		return pf;
	}

	/**
	 * @param pf
	 *            the pf to set
	 */
	public void setPf(String pf) {
		this.pf = pf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((comments == null) ? 0 : comments.hashCode());
		result = prime * result + ((dent == null) ? 0 : dent.hashCode());
		result = prime * result + ((from_health_card_number == null) ? 0 : from_health_card_number.hashCode());
		result = prime * result + ((med == null) ? 0 : med.hashCode());
		result = prime * result + ((mf == null) ? 0 : mf.hashCode());
		result = prime * result + ((modified_by == null) ? 0 : modified_by.hashCode());
		result = prime * result + ((pf == null) ? 0 : pf.hashCode());
		result = prime * result + ((pharm == null) ? 0 : pharm.hashCode());
		result = prime * result + ((response_tag_number == null) ? 0 : response_tag_number.hashCode());
		result = prime * result + ((se_number == null) ? 0 : se_number.hashCode());
		result = prime * result + ((se_sequence_number == null) ? 0 : se_sequence_number.hashCode());
		result = prime * result + ((source_name == null) ? 0 : source_name.hashCode());
		result = prime * result + ((target_name == null) ? 0 : target_name.hashCode());
		result = prime * result + ((to_health_card_number == null) ? 0 : to_health_card_number.hashCode());
		result = prime * result + ((transfer_date == null) ? 0 : transfer_date.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DssAuditHistoryTransferMappedEntityPK other = (DssAuditHistoryTransferMappedEntityPK) obj;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (dent == null) {
			if (other.dent != null)
				return false;
		} else if (!dent.equals(other.dent))
			return false;
		if (from_health_card_number == null) {
			if (other.from_health_card_number != null)
				return false;
		} else if (!from_health_card_number.equals(other.from_health_card_number))
			return false;
		if (med == null) {
			if (other.med != null)
				return false;
		} else if (!med.equals(other.med))
			return false;
		if (mf == null) {
			if (other.mf != null)
				return false;
		} else if (!mf.equals(other.mf))
			return false;
		if (modified_by == null) {
			if (other.modified_by != null)
				return false;
		} else if (!modified_by.equals(other.modified_by))
			return false;
		if (pf == null) {
			if (other.pf != null)
				return false;
		} else if (!pf.equals(other.pf))
			return false;
		if (pharm == null) {
			if (other.pharm != null)
				return false;
		} else if (!pharm.equals(other.pharm))
			return false;
		if (response_tag_number == null) {
			if (other.response_tag_number != null)
				return false;
		} else if (!response_tag_number.equals(other.response_tag_number))
			return false;
		if (se_number == null) {
			if (other.se_number != null)
				return false;
		} else if (!se_number.equals(other.se_number))
			return false;
		if (se_sequence_number == null) {
			if (other.se_sequence_number != null)
				return false;
		} else if (!se_sequence_number.equals(other.se_sequence_number))
			return false;
		if (source_name == null) {
			if (other.source_name != null)
				return false;
		} else if (!source_name.equals(other.source_name))
			return false;
		if (target_name == null) {
			if (other.target_name != null)
				return false;
		} else if (!target_name.equals(other.target_name))
			return false;
		if (to_health_card_number == null) {
			if (other.to_health_card_number != null)
				return false;
		} else if (!to_health_card_number.equals(other.to_health_card_number))
			return false;
		if (transfer_date == null) {
			if (other.transfer_date != null)
				return false;
		} else if (!transfer_date.equals(other.transfer_date))
			return false;
		return true;
	}
}

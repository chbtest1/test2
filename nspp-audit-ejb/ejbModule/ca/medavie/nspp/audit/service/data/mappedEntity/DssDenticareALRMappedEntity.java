package ca.medavie.nspp.audit.service.data.mappedEntity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

/**
 * The persistent class for the DSS_HEALTH_SERVICE database table.
 * 
 */
@Entity
@SqlResultSetMapping(name = "DssDenticareALRQueryMapping", entities = { @EntityResult(entityClass = DssDenticareALRMappedEntity.class, fields = {
		@FieldResult(name = "id.claim_num", column = "claim_num"), 
		@FieldResult(name = "id.item_code", column = "item_code"),
		@FieldResult(name = "id.audit_run_number", column = "audit_run_number"),
		@FieldResult(name = "provider_number", column = "provider_number"),
		@FieldResult(name = "provider_type", column = "provider_type"),
		@FieldResult(name = "health_card_number", column = "health_card_number"),
		@FieldResult(name = "claim_selection_date", column = "claim_selection_date"),
		@FieldResult(name = "audit_letter_creation_date", column = "audit_letter_creation_date"),
		@FieldResult(name = "fee_code_description", column = "fee_code_description"),
		@FieldResult(name = "response_status_code", column = "response_status_code"),
		@FieldResult(name = "number_of_times_sent", column = "number_of_times_sent"),
		@FieldResult(name = "chargeback_amount", column = "chargeback_amount"),
		@FieldResult(name = "number_of_claims_affected", column = "number_of_claims_affected"),
		@FieldResult(name = "modified_by", column = "modified_by"),
		@FieldResult(name = "last_modified", column = "last_modified"),
		@FieldResult(name = "audit_type", column = "audit_type"),
		@FieldResult(name = "client_name", column = "client_name"),
		@FieldResult(name = "audit_note", column = "audit_note"),
		@FieldResult(name = "provider_name", column = "provider_name"),
		@FieldResult(name = "service_date", column = "service_date"),
		@FieldResult(name = "paid_amount", column = "paid_amount"),
		@FieldResult(name = "individual_name", column = "individual_name") }) })
public class DssDenticareALRMappedEntity {

	@EmbeddedId
	private DssDenticareALRMappedEntityPK id;
	
	@Column
	private Long provider_number;
	@Column
	private String provider_type;
	@Column
	private String health_card_number;
	@Column
	private Date claim_selection_date;
	@Column
	private Date audit_letter_creation_date;
	@Column
	private String fee_code_description;
	@Column
	private String response_status_code;
	@Column
	private Integer number_of_times_sent;
	@Column
	private BigDecimal chargeback_amount;
	@Column
	private Integer number_of_claims_affected;
	@Column
	private String modified_by;
	@Column
	private Date last_modified;
	@Column
	private String audit_type;
	@Column
	private String client_name;
	@Column
	private String audit_note;
	@Column
	private Date service_date;
	@Column
	private BigDecimal paid_amount;
	@Column
	private String individual_name;
	public DssDenticareALRMappedEntityPK getId() {
		return id;
	}

	public void setId(DssDenticareALRMappedEntityPK id) {
		this.id = id;
	}

	@Column
	private String provider_name;

	public Long getProvider_number() {
		return provider_number;
	}

	public void setProvider_number(Long provider_number) {
		this.provider_number = provider_number;
	}

	public String getProvider_type() {
		return provider_type;
	}

	public void setProvider_type(String provider_type) {
		this.provider_type = provider_type;
	}

	public String getHealth_card_number() {
		return health_card_number;
	}

	public void setHealth_card_number(String health_card_number) {
		this.health_card_number = health_card_number;
	}

	public Date getClaim_selection_date() {
		return claim_selection_date;
	}

	public void setClaim_selection_date(Date claim_selection_date) {
		this.claim_selection_date = claim_selection_date;
	}

	public Date getAudit_letter_creation_date() {
		return audit_letter_creation_date;
	}

	public void setAudit_letter_creation_date(Date audit_letter_creation_date) {
		this.audit_letter_creation_date = audit_letter_creation_date;
	}

	public String getFee_code_description() {
		return fee_code_description;
	}

	public void setFee_code_description(String fee_code_description) {
		this.fee_code_description = fee_code_description;
	}

	public String getResponse_status_code() {
		// remove the white space for status code
		return response_status_code.replaceAll("\\s+", "");
	}

	public void setResponse_status_code(String response_status_code) {
		this.response_status_code = response_status_code;
	}

	public Integer getNumber_of_times_sent() {
		return number_of_times_sent;
	}

	public void setNumber_of_times_sent(Integer number_of_times_sent) {
		this.number_of_times_sent = number_of_times_sent;
	}

	public BigDecimal getChargeback_amount() {
		return chargeback_amount;
	}

	public void setChargeback_amount(BigDecimal chargeback_amount) {
		this.chargeback_amount = chargeback_amount;
	}

	public Integer getNumber_of_claims_affected() {
		return number_of_claims_affected;
	}

	public void setNumber_of_claims_affected(Integer number_of_claims_affected) {
		this.number_of_claims_affected = number_of_claims_affected;
	}

	public String getModified_by() {
		return modified_by;
	}

	public void setModified_by(String modified_by) {
		this.modified_by = modified_by;
	}

	public String getAudit_type() {
		return audit_type;
	}

	public void setAudit_type(String audit_type) {
		this.audit_type = audit_type;
	}

	public Date getLast_modified() {
		return last_modified;
	}

	public void setLast_modified(Date last_modified) {
		this.last_modified = last_modified;
	}

	public String getClient_name() {
		return client_name;
	}

	public void setClient_name(String client_name) {
		this.client_name = client_name;
	}

	public String getAudit_note() {
		return audit_note;
	}

	public void setAudit_note(String audit_note) {
		this.audit_note = audit_note;
	}

	public String getProvider_name() {
		return provider_name;
	}

	public void setProvider_name(String provider_name) {
		this.provider_name = provider_name;
	}

	/**
	 * @return the service_date
	 */
	public Date getService_date() {
		return service_date;
	}

	/**
	 * @param service_date the service_date to set
	 */
	public void setService_date(Date service_date) {
		this.service_date = service_date;
	}

	/**
	 * @return the paid_amount
	 */
	public BigDecimal getPaid_amount() {
		return paid_amount;
	}

	/**
	 * @param paid_amount the paid_amount to set
	 */
	public void setPaid_amount(BigDecimal paid_amount) {
		this.paid_amount = paid_amount;
	}

	/**
	 * @return the individual_name
	 */
	public String getIndividual_name() {
		return individual_name;
	}

	/**
	 * @param individual_name the individual_name to set
	 */
	public void setIndividual_name(String individual_name) {
		this.individual_name = individual_name;
	}

}
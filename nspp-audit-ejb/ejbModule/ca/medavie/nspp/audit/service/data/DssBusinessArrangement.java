package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DSS_BUSINESS_ARRANGEMENT database table.
 * 
 */
@Entity
@Table(name="DSS_BUSINESS_ARRANGEMENT")
public class DssBusinessArrangement implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="BUS_ARR_NUMBER")
	private Long busArrNumber;

	@Column(name="BUS_ARR_DESCRIPTION")
	private String busArrDescription;

	@Column(name="CAPPING_INDICATOR")
	private String cappingIndicator;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_FROM_DATE")
	private Date effectiveFromDate;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_TO_DATE")
	private Date effectiveToDate;

	@Column(name="FACILITY_INCLUSIVE")
	private String facilityInclusive;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFIED")
	private Date lastModified;

	@Column(name="PAYMENT_METHOD")
	private BigDecimal paymentMethod;

	@Column(name="PROVIDER_GROUP_ID")
	private Long providerGroupId;

	@Column(name="PROVIDER_NUMBER")
	private Long providerNumber;

	@Column(name="PROVIDER_TYPE")
	private String providerType;

	@Column(name="REMUNERATION_METHOD")
	private Long remunerationMethod;

	@Column(name="SPECIALTY_INCLUSIVE")
	private String specialtyInclusive;

	@Column(name="SUBMITTER_ID")
	private String submitterId;

	public DssBusinessArrangement() {
	}

	public Long getBusArrNumber() {
		return this.busArrNumber;
	}

	public void setBusArrNumber(Long busArrNumber) {
		this.busArrNumber = busArrNumber;
	}

	public String getBusArrDescription() {
		return this.busArrDescription;
	}

	public void setBusArrDescription(String busArrDescription) {
		this.busArrDescription = busArrDescription;
	}

	public String getCappingIndicator() {
		return this.cappingIndicator;
	}

	public void setCappingIndicator(String cappingIndicator) {
		this.cappingIndicator = cappingIndicator;
	}

	public Date getEffectiveFromDate() {
		return this.effectiveFromDate;
	}

	public void setEffectiveFromDate(Date effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}

	public Date getEffectiveToDate() {
		return this.effectiveToDate;
	}

	public void setEffectiveToDate(Date effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}

	public String getFacilityInclusive() {
		return this.facilityInclusive;
	}

	public void setFacilityInclusive(String facilityInclusive) {
		this.facilityInclusive = facilityInclusive;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public BigDecimal getPaymentMethod() {
		return this.paymentMethod;
	}

	public void setPaymentMethod(BigDecimal paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public Long getProviderGroupId() {
		return this.providerGroupId;
	}

	public void setProviderGroupId(Long providerGroupId) {
		this.providerGroupId = providerGroupId;
	}

	public Long getProviderNumber() {
		return this.providerNumber;
	}

	public void setProviderNumber(Long providerNumber) {
		this.providerNumber = providerNumber;
	}

	public String getProviderType() {
		return this.providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public Long getRemunerationMethod() {
		return this.remunerationMethod;
	}

	public void setRemunerationMethod(Long remunerationMethod) {
		this.remunerationMethod = remunerationMethod;
	}

	public String getSpecialtyInclusive() {
		return this.specialtyInclusive;
	}

	public void setSpecialtyInclusive(String specialtyInclusive) {
		this.specialtyInclusive = specialtyInclusive;
	}

	public String getSubmitterId() {
		return this.submitterId;
	}

	public void setSubmitterId(String submitterId) {
		this.submitterId = submitterId;
	}

}
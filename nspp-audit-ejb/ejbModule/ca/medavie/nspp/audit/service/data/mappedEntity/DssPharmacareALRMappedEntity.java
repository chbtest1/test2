package ca.medavie.nspp.audit.service.data.mappedEntity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

/**
 * The persistent class for the DSS_HEALTH_SERVICE database table.
 * 
 */
@Entity
@SqlResultSetMapping(name = "DssPharmacareALRQueryMapping", entities = { @EntityResult(entityClass = DssPharmacareALRMappedEntity.class, fields = {
		@FieldResult(name = "id.claim_ref_num", column = "claim_ref_num"),
		@FieldResult(name = "id.audit_run_number", column = "audit_run_number"),
		@FieldResult(name = "provider_number", column = "provider_number"),
		@FieldResult(name = "provider_type", column = "provider_type"),
		@FieldResult(name = "client_id", column = "client_id"),
		@FieldResult(name = "se_selection_date", column = "se_selection_date"),
		@FieldResult(name = "audit_letter_creation_date", column = "audit_letter_creation_date"),
		@FieldResult(name = "response_status_code", column = "response_status_code"),
		@FieldResult(name = "number_of_times_sent", column = "number_of_times_sent"),
		@FieldResult(name = "chargeback_amount", column = "chargeback_amount"),
		@FieldResult(name = "number_of_services_affected", column = "number_of_services_affected"),
		@FieldResult(name = "audit_note", column = "audit_note"),
		@FieldResult(name = "modified_by", column = "modified_by"),
		@FieldResult(name = "last_modified", column = "last_modified"),
		@FieldResult(name = "audit_type", column = "audit_type"), 
		@FieldResult(name = "din", column = "din"),
		@FieldResult(name = "claim_date", column = "claim_date"),
		@FieldResult(name = "full_prescription_cost", column = "full_prescription_cost"),
		@FieldResult(name = "original_rx_numb", column = "original_rx_numb"),
		@FieldResult(name = "mqty_paid", column = "mqty_paid"), 
		@FieldResult(name = "prod_e", column = "prod_e"),
		@FieldResult(name = "client_name", column = "client_name"),
		@FieldResult(name = "provider_name", column = "provider_name") }) })
public class DssPharmacareALRMappedEntity {

	@EmbeddedId
	private DssPharmaCareALRMappedEntityPK id;
	@Column
	private Long provider_number;
	@Column
	private String provider_type;
	@Column
	private String client_id;
	@Column
	private Date se_selection_date;
	@Column
	private Date audit_letter_creation_date;
	@Column
	private String response_status_code;
	@Column
	private Integer number_of_times_sent;
	@Column
	private BigDecimal chargeback_amount;
	@Column
	private Integer number_of_services_affected;
	@Column
	private String audit_note;
	@Column
	private String modified_by;
	@Column
	private Date last_modified;
	@Column
	private String audit_type;
	@Column
	private String din;
	@Column
	private String claim_date;
	@Column
	private String full_prescription_cost;
	@Column
	private String original_rx_numb;
	@Column
	private String mqty_paid;
	@Column
	private String prod_e;
	@Column
	private String client_name;
	@Column
	private String provider_name;

	public DssPharmaCareALRMappedEntityPK getId() {
		return id;
	}

	public void setId(DssPharmaCareALRMappedEntityPK id) {
		this.id = id;
	}

	public Long getProvider_number() {
		return provider_number;
	}

	public void setProvider_number(Long provider_number) {
		this.provider_number = provider_number;
	}

	public String getProvider_type() {
		return provider_type;
	}

	public void setProvider_type(String provider_type) {
		this.provider_type = provider_type;
	}

	public Date getAudit_letter_creation_date() {
		return audit_letter_creation_date;
	}

	public void setAudit_letter_creation_date(Date audit_letter_creation_date) {
		this.audit_letter_creation_date = audit_letter_creation_date;
	}

	public String getResponse_status_code() {
		// remove the white space for status code
		return response_status_code.replaceAll("\\s+", "");
	}

	public void setResponse_status_code(String response_status_code) {
		this.response_status_code = response_status_code;
	}

	public Integer getNumber_of_times_sent() {
		return number_of_times_sent;
	}

	public void setNumber_of_times_sent(Integer number_of_times_sent) {
		this.number_of_times_sent = number_of_times_sent;
	}

	public BigDecimal getChargeback_amount() {
		return chargeback_amount;
	}

	public void setChargeback_amount(BigDecimal chargeback_amount) {
		this.chargeback_amount = chargeback_amount;
	}

	public Integer getNumber_of_services_affected() {
		return number_of_services_affected;
	}

	public void setNumber_of_services_affected(Integer number_of_services_affected) {
		this.number_of_services_affected = number_of_services_affected;
	}

	public String getModified_by() {
		return modified_by;
	}

	public void setModified_by(String modified_by) {
		this.modified_by = modified_by;
	}

	public String getAudit_type() {
		return audit_type;
	}

	public void setAudit_type(String audit_type) {
		this.audit_type = audit_type;
	}

	public Date getLast_modified() {
		return last_modified;
	}

	public void setLast_modified(Date last_modified) {
		this.last_modified = last_modified;
	}

	public String getClient_name() {
		return client_name;
	}

	public void setClient_name(String client_name) {
		this.client_name = client_name;
	}

	public String getAudit_note() {
		return audit_note;
	}

	public void setAudit_note(String audit_note) {
		this.audit_note = audit_note;
	}

	public String getProvider_name() {
		return provider_name;
	}

	public void setProvider_name(String provider_name) {
		this.provider_name = provider_name;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public Date getSe_selection_date() {
		return se_selection_date;
	}

	public void setSe_selection_date(Date se_selection_date) {
		this.se_selection_date = se_selection_date;
	}

	public String getDin() {
		return din;
	}

	public void setDin(String din) {
		this.din = din;
	}

	public String getClaim_date() {
		return claim_date;
	}

	public void setClaim_date(String claim_date) {
		this.claim_date = claim_date;
	}

	public String getFull_prescription_cost() {
		return full_prescription_cost;
	}

	public void setFull_prescription_cost(String full_prescription_cost) {
		this.full_prescription_cost = full_prescription_cost;
	}

	public String getOriginal_rx_numb() {
		return original_rx_numb;
	}

	public void setOriginal_rx_numb(String original_rx_numb) {
		this.original_rx_numb = original_rx_numb;
	}

	public String getMqty_paid() {
		return mqty_paid;
	}

	public void setMqty_paid(String mqty_paid) {
		this.mqty_paid = mqty_paid;
	}

	public String getProd_e() {
		return prod_e;
	}

	public void setProd_e(String prod_e) {
		this.prod_e = prod_e;
	}
}
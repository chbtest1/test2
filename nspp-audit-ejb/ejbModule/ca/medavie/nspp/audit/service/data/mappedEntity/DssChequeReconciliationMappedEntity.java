package ca.medavie.nspp.audit.service.data.mappedEntity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.SqlResultSetMapping;

@Entity
@SqlResultSetMapping(name = "DssChequeReconciliationNativeQueryMapping", entities = { @EntityResult(entityClass = DssChequeReconciliationMappedEntity.class, fields = {
		@FieldResult(name = "id.agencyId", column = "AGENCY_ID"),
		@FieldResult(name = "id.batchNumber", column = "BATCH_NUMBER"),
		@FieldResult(name = "id.chequeAmount", column = "CHEQUE_AMOUNT"),
		@FieldResult(name = "id.chequeNote", column = "CHEQUE_NOTE"),
		@FieldResult(name = "id.originalChequeNote", column = "CHEQUE_NOTE"),
		@FieldResult(name = "id.chequeNumber", column = "CHEQUE_NUMBER"),
		@FieldResult(name = "id.claimedAmount", column = "CLAIMED_AMOUNT"),
		@FieldResult(name = "id.dateCashed", column = "DATE_CASHED"),
		@FieldResult(name = "id.familyBenefitsNumber", column = "FAMILY_BENEFITS_NUMBER"),
		@FieldResult(name = "id.glNumber", column = "GL_NUMBER"),
		@FieldResult(name = "id.originalGlNumber", column = "GL_NUMBER"),
		@FieldResult(name = "id.healthCardNumber", column = "HEALTH_CARD_NUMBER"),
		@FieldResult(name = "id.lastModified", column = "LAST_MODIFIED"),
		@FieldResult(name = "id.otherPayeeId", column = "OTHER_PAYEE_ID"),
		@FieldResult(name = "id.paidStatus", column = "PAID_STATUS"),
		@FieldResult(name = "id.payeeName", column = "PAYEE_NAME"),
		@FieldResult(name = "id.payeeType", column = "PAYEE_TYPE"),
		@FieldResult(name = "id.paymentDate", column = "PAYMENT_DATE"),
		@FieldResult(name = "id.paymentRunNumber", column = "PAYMENT_RUN_NUMBER"),
		@FieldResult(name = "id.providerGroupId", column = "PROVIDER_GROUP_ID"),
		@FieldResult(name = "id.providerNumber", column = "PROVIDER_NUMBER"),
		@FieldResult(name = "id.providerType", column = "PROVIDER_TYPE"),
		@FieldResult(name = "id.provinceCode", column = "PROVINCE_CODE"),
		@FieldResult(name = "id.reconciliationStatus", column = "RECONCILIATION_STATUS"),
		@FieldResult(name = "id.registerStatus", column = "REGISTER_STATUS"),
		@FieldResult(name = "id.stubsVoided", column = "STUBS_VOIDED"),
		@FieldResult(name = "id.summaryNumber", column = "SUMMARY_NUMBER") }) })
public class DssChequeReconciliationMappedEntity {

	@EmbeddedId
	private DssChequeReconciliationMappedEntityPK id;

	/**
	 * @return the id
	 */
	public DssChequeReconciliationMappedEntityPK getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(DssChequeReconciliationMappedEntityPK id) {
		this.id = id;
	}
}

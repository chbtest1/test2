package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DSS_DENTAL_CLAIMS database table.
 * 
 */
@Entity
@Table(name="DSS_DENTAL_CLAIMS")
public class DssDentalClaim implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DssDentalClaimPK id;

	@Column(name="APPROVE_REVIEW")
	private BigDecimal approveReview;

	@Temporal(TemporalType.DATE)
	@Column(name="BIRTH_DATE")
	private Date birthDate;

	@Column(name="BUS_ARR_NUMBER")
	private BigDecimal busArrNumber;

	@Column(name="BUS_CLIENT_NAME")
	private String busClientName;

	@Column(name="CLAIM_CODE")
	private BigDecimal claimCode;

	@Column(name="COVERAGE_CODE")
	private BigDecimal coverageCode;

	private BigDecimal deductible;

	@Column(name="DENTIST_POSTAL_CODE")
	private String dentistPostalCode;

	@Column(name="ELIGIBLE_AMOUNT")
	private BigDecimal eligibleAmount;

	private String facility;

	@Column(name="FEE_ALLOWED")
	private BigDecimal feeAllowed;

	@Column(name="FEE_CLAIMED")
	private BigDecimal feeClaimed;

	@Column(name="FEE_PAID")
	private BigDecimal feePaid;

	private String gender;

	@Column(name="HEALTH_CARD_NUMBER")
	private String healthCardNumber;

	@Column(name="HISTORY_CODE")
	private BigDecimal historyCode;

	@Column(name="ITEM_STATUS")
	private String itemStatus;

	@Column(name="LAB_ALLOWED")
	private BigDecimal labAllowed;

	@Column(name="LAB_CLAIM")
	private BigDecimal labClaim;

	@Column(name="LAB_PAID")
	private BigDecimal labPaid;

	private BigDecimal multiplier;

	@Column(name="ORIGINAL_ITEM_CODE")
	private BigDecimal originalItemCode;

	@Column(name="OVERRIDE_TYPE")
	private String overrideType;

	@Column(name="PAID_AMOUNT")
	private BigDecimal paidAmount;

	@Column(name="PAID_CARRIER")
	private BigDecimal paidCarrier;

	@Temporal(TemporalType.DATE)
	@Column(name="PAYMENT_DATE")
	private Date paymentDate;

	@Column(name="PRE_AUTH_CODE")
	private BigDecimal preAuthCode;

	private BigDecimal primetime;

	@Column(name="PROCEDURE_CODE")
	private BigDecimal procedureCode;

	@Column(name="PROGRAM_CODE")
	private String programCode;

	@Column(name="PROVIDER_LICENSE_NUMBER")
	private BigDecimal providerLicenseNumber;

	@Column(name="PROVIDER_NAME")
	private String providerName;

	@Column(name="PROVIDER_NUMBER")
	private BigDecimal providerNumber;

	@Column(name="PROVIDER_TYPE")
	private String providerType;

	@Column(name="REVERSED_ITEM_CODE")
	private BigDecimal reversedItemCode;

	@Temporal(TemporalType.DATE)
	@Column(name="SERVICE_DATE")
	private Date serviceDate;

	@Column(name="SKIP_FEE_LOOKUP")
	private BigDecimal skipFeeLookup;

	@Column(name="SPECIAL_CONDITION")
	private BigDecimal specialCondition;

	private String specialty;

	@Column(name="TOOTH_CODE")
	private BigDecimal toothCode;

	@Column(name="TOOTH_SURFACE")
	private String toothSurface;

	@Column(name="UNIT_COUNT")
	private BigDecimal unitCount;

	public DssDentalClaim() {
	}

	public DssDentalClaimPK getId() {
		return this.id;
	}

	public void setId(DssDentalClaimPK id) {
		this.id = id;
	}

	public BigDecimal getApproveReview() {
		return this.approveReview;
	}

	public void setApproveReview(BigDecimal approveReview) {
		this.approveReview = approveReview;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public BigDecimal getBusArrNumber() {
		return this.busArrNumber;
	}

	public void setBusArrNumber(BigDecimal busArrNumber) {
		this.busArrNumber = busArrNumber;
	}

	public String getBusClientName() {
		return this.busClientName;
	}

	public void setBusClientName(String busClientName) {
		this.busClientName = busClientName;
	}

	public BigDecimal getClaimCode() {
		return this.claimCode;
	}

	public void setClaimCode(BigDecimal claimCode) {
		this.claimCode = claimCode;
	}

	public BigDecimal getCoverageCode() {
		return this.coverageCode;
	}

	public void setCoverageCode(BigDecimal coverageCode) {
		this.coverageCode = coverageCode;
	}

	public BigDecimal getDeductible() {
		return this.deductible;
	}

	public void setDeductible(BigDecimal deductible) {
		this.deductible = deductible;
	}

	public String getDentistPostalCode() {
		return this.dentistPostalCode;
	}

	public void setDentistPostalCode(String dentistPostalCode) {
		this.dentistPostalCode = dentistPostalCode;
	}

	public BigDecimal getEligibleAmount() {
		return this.eligibleAmount;
	}

	public void setEligibleAmount(BigDecimal eligibleAmount) {
		this.eligibleAmount = eligibleAmount;
	}

	public String getFacility() {
		return this.facility;
	}

	public void setFacility(String facility) {
		this.facility = facility;
	}

	public BigDecimal getFeeAllowed() {
		return this.feeAllowed;
	}

	public void setFeeAllowed(BigDecimal feeAllowed) {
		this.feeAllowed = feeAllowed;
	}

	public BigDecimal getFeeClaimed() {
		return this.feeClaimed;
	}

	public void setFeeClaimed(BigDecimal feeClaimed) {
		this.feeClaimed = feeClaimed;
	}

	public BigDecimal getFeePaid() {
		return this.feePaid;
	}

	public void setFeePaid(BigDecimal feePaid) {
		this.feePaid = feePaid;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getHealthCardNumber() {
		return this.healthCardNumber;
	}

	public void setHealthCardNumber(String healthCardNumber) {
		this.healthCardNumber = healthCardNumber;
	}

	public BigDecimal getHistoryCode() {
		return this.historyCode;
	}

	public void setHistoryCode(BigDecimal historyCode) {
		this.historyCode = historyCode;
	}

	public String getItemStatus() {
		return this.itemStatus;
	}

	public void setItemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
	}

	public BigDecimal getLabAllowed() {
		return this.labAllowed;
	}

	public void setLabAllowed(BigDecimal labAllowed) {
		this.labAllowed = labAllowed;
	}

	public BigDecimal getLabClaim() {
		return this.labClaim;
	}

	public void setLabClaim(BigDecimal labClaim) {
		this.labClaim = labClaim;
	}

	public BigDecimal getLabPaid() {
		return this.labPaid;
	}

	public void setLabPaid(BigDecimal labPaid) {
		this.labPaid = labPaid;
	}

	public BigDecimal getMultiplier() {
		return this.multiplier;
	}

	public void setMultiplier(BigDecimal multiplier) {
		this.multiplier = multiplier;
	}

	public BigDecimal getOriginalItemCode() {
		return this.originalItemCode;
	}

	public void setOriginalItemCode(BigDecimal originalItemCode) {
		this.originalItemCode = originalItemCode;
	}

	public String getOverrideType() {
		return this.overrideType;
	}

	public void setOverrideType(String overrideType) {
		this.overrideType = overrideType;
	}

	public BigDecimal getPaidAmount() {
		return this.paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public BigDecimal getPaidCarrier() {
		return this.paidCarrier;
	}

	public void setPaidCarrier(BigDecimal paidCarrier) {
		this.paidCarrier = paidCarrier;
	}

	public Date getPaymentDate() {
		return this.paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public BigDecimal getPreAuthCode() {
		return this.preAuthCode;
	}

	public void setPreAuthCode(BigDecimal preAuthCode) {
		this.preAuthCode = preAuthCode;
	}

	public BigDecimal getPrimetime() {
		return this.primetime;
	}

	public void setPrimetime(BigDecimal primetime) {
		this.primetime = primetime;
	}

	public BigDecimal getProcedureCode() {
		return this.procedureCode;
	}

	public void setProcedureCode(BigDecimal procedureCode) {
		this.procedureCode = procedureCode;
	}

	public String getProgramCode() {
		return this.programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public BigDecimal getProviderLicenseNumber() {
		return this.providerLicenseNumber;
	}

	public void setProviderLicenseNumber(BigDecimal providerLicenseNumber) {
		this.providerLicenseNumber = providerLicenseNumber;
	}

	public String getProviderName() {
		return this.providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public BigDecimal getProviderNumber() {
		return this.providerNumber;
	}

	public void setProviderNumber(BigDecimal providerNumber) {
		this.providerNumber = providerNumber;
	}

	public String getProviderType() {
		return this.providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public BigDecimal getReversedItemCode() {
		return this.reversedItemCode;
	}

	public void setReversedItemCode(BigDecimal reversedItemCode) {
		this.reversedItemCode = reversedItemCode;
	}

	public Date getServiceDate() {
		return this.serviceDate;
	}

	public void setServiceDate(Date serviceDate) {
		this.serviceDate = serviceDate;
	}

	public BigDecimal getSkipFeeLookup() {
		return this.skipFeeLookup;
	}

	public void setSkipFeeLookup(BigDecimal skipFeeLookup) {
		this.skipFeeLookup = skipFeeLookup;
	}

	public BigDecimal getSpecialCondition() {
		return this.specialCondition;
	}

	public void setSpecialCondition(BigDecimal specialCondition) {
		this.specialCondition = specialCondition;
	}

	public String getSpecialty() {
		return this.specialty;
	}

	public void setSpecialty(String specialty) {
		this.specialty = specialty;
	}

	public BigDecimal getToothCode() {
		return this.toothCode;
	}

	public void setToothCode(BigDecimal toothCode) {
		this.toothCode = toothCode;
	}

	public String getToothSurface() {
		return this.toothSurface;
	}

	public void setToothSurface(String toothSurface) {
		this.toothSurface = toothSurface;
	}

	public BigDecimal getUnitCount() {
		return this.unitCount;
	}

	public void setUnitCount(BigDecimal unitCount) {
		this.unitCount = unitCount;
	}

}
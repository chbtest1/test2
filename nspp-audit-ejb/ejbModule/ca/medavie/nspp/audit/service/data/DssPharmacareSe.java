package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DSS_PHARMACARE_SE database table.
 * 
 */
@Entity
@Table(name="DSS_PHARMACARE_SE")
public class DssPharmacareSe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SE_NUMBER")
	private String seNumber;

	@Column(name="AMOUNT_APPROVED")
	private BigDecimal amountApproved;

	@Column(name="AMOUNT_CLAIMED")
	private BigDecimal amountClaimed;

	@Column(name="AMOUNT_PAID")
	private BigDecimal amountPaid;

	@Column(name="ATC_CODE")
	private String atcCode;

	@Column(name="AUTHORIZATION_CODE")
	private BigDecimal authorizationCode;

	@Column(name="BENEFIT_CODE")
	private BigDecimal benefitCode;

	@Column(name="COINSURANCE_PAID")
	private BigDecimal coinsurancePaid;

	@Column(name="COMPOUND_FEE_CLAIMED")
	private BigDecimal compoundFeeClaimed;

	@Column(name="COMPOUND_FEE_PAID")
	private BigDecimal compoundFeePaid;

	@Column(name="COMPOUND_INDICATOR")
	private String compoundIndicator;

	@Column(name="COPAY_PAID")
	private BigDecimal copayPaid;

	@Temporal(TemporalType.DATE)
	@Column(name="DATE_OF_SERVICE")
	private Date dateOfService;

	@Temporal(TemporalType.DATE)
	@Column(name="DATE_PROCESSED")
	private Date dateProcessed;

	@Column(name="DAYS_SUPPLY_CLAIMED")
	private BigDecimal daysSupplyClaimed;

	@Column(name="DAYS_SUPPLY_PAID")
	private BigDecimal daysSupplyPaid;

	@Column(name="DAYS_SUPPLY_REPORTED_INDICATOR")
	private String daysSupplyReportedIndicator;

	@Column(name="DEDUCTIBLE_PAID")
	private BigDecimal deductiblePaid;

	private String din;

	@Column(name="DRUG_COST_CLAIMED")
	private BigDecimal drugCostClaimed;

	@Column(name="DRUG_COST_PAID")
	private BigDecimal drugCostPaid;

	@Column(name="GENERIC_INDICATOR")
	private String genericIndicator;

	@Column(name="GREEN_AMBER_RED_INDICATOR")
	private String greenAmberRedIndicator;

	@Column(name="INTERV_EXC_CODE")
	private String intervExcCode;

	@Column(name="MAC_DRUG_INDICATOR")
	private String macDrugIndicator;

	@Column(name="ORIGINAL_REFILL_INDICATOR")
	private String originalRefillIndicator;

	@Column(name="PAYEE_TYPE")
	private String payeeType;

	@Temporal(TemporalType.DATE)
	@Column(name="PAYMENT_DATE")
	private Date paymentDate;

	@Column(name="PAYMENT_METHOD")
	private BigDecimal paymentMethod;

	@Column(name="PLAN_EXCEPTION_INDICATOR")
	private String planExceptionIndicator;

	@Column(name="POS_COINS_PAID")
	private BigDecimal posCoinsPaid;

	@Column(name="PRESCRIBER_LICENSE_NUMBER")
	private String prescriberLicenseNumber;

	@Column(name="PRESCRIBER_PROVIDER_NUMBER")
	private BigDecimal prescriberProviderNumber;

	@Column(name="PRESCRIBER_PROVIDER_TYPE")
	private String prescriberProviderType;

	@Column(name="PRESCRIPTION_NUMBER")
	private String prescriptionNumber;

	@Column(name="PREV_PAID")
	private BigDecimal prevPaid;

	@Column(name="PRICING_AGREEMENT_NUMBER")
	private String pricingAgreementNumber;

	@Column(name="PROF_FEE_CLAIMED")
	private BigDecimal profFeeClaimed;

	@Column(name="PROF_FEE_PAID")
	private BigDecimal profFeePaid;

	@Column(name="PROGRAM_CODE")
	private String programCode;

	@Column(name="PROVIDER_NUMBER")
	private BigDecimal providerNumber;

	@Column(name="PROVIDER_TYPE")
	private BigDecimal providerType;

	@Column(name="QTY_CLAIMED")
	private BigDecimal qtyClaimed;

	@Column(name="QTY_PAID")
	private BigDecimal qtyPaid;

	@Column(name="RECIPIENT_AGE")
	private BigDecimal recipientAge;

	@Column(name="RECIPIENT_HEALTH_CARD_NUMBER")
	private String recipientHealthCardNumber;

	@Column(name="RECIPIENT_POSTAL_CODE")
	private String recipientPostalCode;

	@Column(name="REFILLS_ALLOWED")
	private BigDecimal refillsAllowed;

	@Column(name="REFILLS_REMAINING")
	private BigDecimal refillsRemaining;

	@Column(name="RESPONSE_CODES")
	private String responseCodes;

	@Column(name="SE_CROSS_REFERENCE_NUMBER")
	private String seCrossReferenceNumber;

	@Column(name="SE_TYPE")
	private String seType;

	@Column(name="SPEC_AUTH_NUM")
	private String specAuthNum;

	@Column(name="SS_FEE")
	private BigDecimal ssFee;

	@Column(name="SS_FEE_PAID")
	private BigDecimal ssFeePaid;

	@Column(name="SUBMIT_METHOD")
	private String submitMethod;

	@Column(name="TRIAL_DIN_INDICATOR")
	private String trialDinIndicator;

	@Column(name="UPCHARGE_CLAIMED")
	private BigDecimal upchargeClaimed;

	@Column(name="UPCHARGE_PAID")
	private BigDecimal upchargePaid;

	@Column(name="XREF_TO_ORIGINAL_RX_NUMBER")
	private String xrefToOriginalRxNumber;

	public DssPharmacareSe() {
	}

	public String getSeNumber() {
		return this.seNumber;
	}

	public void setSeNumber(String seNumber) {
		this.seNumber = seNumber;
	}

	public BigDecimal getAmountApproved() {
		return this.amountApproved;
	}

	public void setAmountApproved(BigDecimal amountApproved) {
		this.amountApproved = amountApproved;
	}

	public BigDecimal getAmountClaimed() {
		return this.amountClaimed;
	}

	public void setAmountClaimed(BigDecimal amountClaimed) {
		this.amountClaimed = amountClaimed;
	}

	public BigDecimal getAmountPaid() {
		return this.amountPaid;
	}

	public void setAmountPaid(BigDecimal amountPaid) {
		this.amountPaid = amountPaid;
	}

	public String getAtcCode() {
		return this.atcCode;
	}

	public void setAtcCode(String atcCode) {
		this.atcCode = atcCode;
	}

	public BigDecimal getAuthorizationCode() {
		return this.authorizationCode;
	}

	public void setAuthorizationCode(BigDecimal authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	public BigDecimal getBenefitCode() {
		return this.benefitCode;
	}

	public void setBenefitCode(BigDecimal benefitCode) {
		this.benefitCode = benefitCode;
	}

	public BigDecimal getCoinsurancePaid() {
		return this.coinsurancePaid;
	}

	public void setCoinsurancePaid(BigDecimal coinsurancePaid) {
		this.coinsurancePaid = coinsurancePaid;
	}

	public BigDecimal getCompoundFeeClaimed() {
		return this.compoundFeeClaimed;
	}

	public void setCompoundFeeClaimed(BigDecimal compoundFeeClaimed) {
		this.compoundFeeClaimed = compoundFeeClaimed;
	}

	public BigDecimal getCompoundFeePaid() {
		return this.compoundFeePaid;
	}

	public void setCompoundFeePaid(BigDecimal compoundFeePaid) {
		this.compoundFeePaid = compoundFeePaid;
	}

	public String getCompoundIndicator() {
		return this.compoundIndicator;
	}

	public void setCompoundIndicator(String compoundIndicator) {
		this.compoundIndicator = compoundIndicator;
	}

	public BigDecimal getCopayPaid() {
		return this.copayPaid;
	}

	public void setCopayPaid(BigDecimal copayPaid) {
		this.copayPaid = copayPaid;
	}

	public Date getDateOfService() {
		return this.dateOfService;
	}

	public void setDateOfService(Date dateOfService) {
		this.dateOfService = dateOfService;
	}

	public Date getDateProcessed() {
		return this.dateProcessed;
	}

	public void setDateProcessed(Date dateProcessed) {
		this.dateProcessed = dateProcessed;
	}

	public BigDecimal getDaysSupplyClaimed() {
		return this.daysSupplyClaimed;
	}

	public void setDaysSupplyClaimed(BigDecimal daysSupplyClaimed) {
		this.daysSupplyClaimed = daysSupplyClaimed;
	}

	public BigDecimal getDaysSupplyPaid() {
		return this.daysSupplyPaid;
	}

	public void setDaysSupplyPaid(BigDecimal daysSupplyPaid) {
		this.daysSupplyPaid = daysSupplyPaid;
	}

	public String getDaysSupplyReportedIndicator() {
		return this.daysSupplyReportedIndicator;
	}

	public void setDaysSupplyReportedIndicator(String daysSupplyReportedIndicator) {
		this.daysSupplyReportedIndicator = daysSupplyReportedIndicator;
	}

	public BigDecimal getDeductiblePaid() {
		return this.deductiblePaid;
	}

	public void setDeductiblePaid(BigDecimal deductiblePaid) {
		this.deductiblePaid = deductiblePaid;
	}

	public String getDin() {
		return this.din;
	}

	public void setDin(String din) {
		this.din = din;
	}

	public BigDecimal getDrugCostClaimed() {
		return this.drugCostClaimed;
	}

	public void setDrugCostClaimed(BigDecimal drugCostClaimed) {
		this.drugCostClaimed = drugCostClaimed;
	}

	public BigDecimal getDrugCostPaid() {
		return this.drugCostPaid;
	}

	public void setDrugCostPaid(BigDecimal drugCostPaid) {
		this.drugCostPaid = drugCostPaid;
	}

	public String getGenericIndicator() {
		return this.genericIndicator;
	}

	public void setGenericIndicator(String genericIndicator) {
		this.genericIndicator = genericIndicator;
	}

	public String getGreenAmberRedIndicator() {
		return this.greenAmberRedIndicator;
	}

	public void setGreenAmberRedIndicator(String greenAmberRedIndicator) {
		this.greenAmberRedIndicator = greenAmberRedIndicator;
	}

	public String getIntervExcCode() {
		return this.intervExcCode;
	}

	public void setIntervExcCode(String intervExcCode) {
		this.intervExcCode = intervExcCode;
	}

	public String getMacDrugIndicator() {
		return this.macDrugIndicator;
	}

	public void setMacDrugIndicator(String macDrugIndicator) {
		this.macDrugIndicator = macDrugIndicator;
	}

	public String getOriginalRefillIndicator() {
		return this.originalRefillIndicator;
	}

	public void setOriginalRefillIndicator(String originalRefillIndicator) {
		this.originalRefillIndicator = originalRefillIndicator;
	}

	public String getPayeeType() {
		return this.payeeType;
	}

	public void setPayeeType(String payeeType) {
		this.payeeType = payeeType;
	}

	public Date getPaymentDate() {
		return this.paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public BigDecimal getPaymentMethod() {
		return this.paymentMethod;
	}

	public void setPaymentMethod(BigDecimal paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getPlanExceptionIndicator() {
		return this.planExceptionIndicator;
	}

	public void setPlanExceptionIndicator(String planExceptionIndicator) {
		this.planExceptionIndicator = planExceptionIndicator;
	}

	public BigDecimal getPosCoinsPaid() {
		return this.posCoinsPaid;
	}

	public void setPosCoinsPaid(BigDecimal posCoinsPaid) {
		this.posCoinsPaid = posCoinsPaid;
	}

	public String getPrescriberLicenseNumber() {
		return this.prescriberLicenseNumber;
	}

	public void setPrescriberLicenseNumber(String prescriberLicenseNumber) {
		this.prescriberLicenseNumber = prescriberLicenseNumber;
	}

	public BigDecimal getPrescriberProviderNumber() {
		return this.prescriberProviderNumber;
	}

	public void setPrescriberProviderNumber(BigDecimal prescriberProviderNumber) {
		this.prescriberProviderNumber = prescriberProviderNumber;
	}

	public String getPrescriberProviderType() {
		return this.prescriberProviderType;
	}

	public void setPrescriberProviderType(String prescriberProviderType) {
		this.prescriberProviderType = prescriberProviderType;
	}

	public String getPrescriptionNumber() {
		return this.prescriptionNumber;
	}

	public void setPrescriptionNumber(String prescriptionNumber) {
		this.prescriptionNumber = prescriptionNumber;
	}

	public BigDecimal getPrevPaid() {
		return this.prevPaid;
	}

	public void setPrevPaid(BigDecimal prevPaid) {
		this.prevPaid = prevPaid;
	}

	public String getPricingAgreementNumber() {
		return this.pricingAgreementNumber;
	}

	public void setPricingAgreementNumber(String pricingAgreementNumber) {
		this.pricingAgreementNumber = pricingAgreementNumber;
	}

	public BigDecimal getProfFeeClaimed() {
		return this.profFeeClaimed;
	}

	public void setProfFeeClaimed(BigDecimal profFeeClaimed) {
		this.profFeeClaimed = profFeeClaimed;
	}

	public BigDecimal getProfFeePaid() {
		return this.profFeePaid;
	}

	public void setProfFeePaid(BigDecimal profFeePaid) {
		this.profFeePaid = profFeePaid;
	}

	public String getProgramCode() {
		return this.programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public BigDecimal getProviderNumber() {
		return this.providerNumber;
	}

	public void setProviderNumber(BigDecimal providerNumber) {
		this.providerNumber = providerNumber;
	}

	public BigDecimal getProviderType() {
		return this.providerType;
	}

	public void setProviderType(BigDecimal providerType) {
		this.providerType = providerType;
	}

	public BigDecimal getQtyClaimed() {
		return this.qtyClaimed;
	}

	public void setQtyClaimed(BigDecimal qtyClaimed) {
		this.qtyClaimed = qtyClaimed;
	}

	public BigDecimal getQtyPaid() {
		return this.qtyPaid;
	}

	public void setQtyPaid(BigDecimal qtyPaid) {
		this.qtyPaid = qtyPaid;
	}

	public BigDecimal getRecipientAge() {
		return this.recipientAge;
	}

	public void setRecipientAge(BigDecimal recipientAge) {
		this.recipientAge = recipientAge;
	}

	public String getRecipientHealthCardNumber() {
		return this.recipientHealthCardNumber;
	}

	public void setRecipientHealthCardNumber(String recipientHealthCardNumber) {
		this.recipientHealthCardNumber = recipientHealthCardNumber;
	}

	public String getRecipientPostalCode() {
		return this.recipientPostalCode;
	}

	public void setRecipientPostalCode(String recipientPostalCode) {
		this.recipientPostalCode = recipientPostalCode;
	}

	public BigDecimal getRefillsAllowed() {
		return this.refillsAllowed;
	}

	public void setRefillsAllowed(BigDecimal refillsAllowed) {
		this.refillsAllowed = refillsAllowed;
	}

	public BigDecimal getRefillsRemaining() {
		return this.refillsRemaining;
	}

	public void setRefillsRemaining(BigDecimal refillsRemaining) {
		this.refillsRemaining = refillsRemaining;
	}

	public String getResponseCodes() {
		return this.responseCodes;
	}

	public void setResponseCodes(String responseCodes) {
		this.responseCodes = responseCodes;
	}

	public String getSeCrossReferenceNumber() {
		return this.seCrossReferenceNumber;
	}

	public void setSeCrossReferenceNumber(String seCrossReferenceNumber) {
		this.seCrossReferenceNumber = seCrossReferenceNumber;
	}

	public String getSeType() {
		return this.seType;
	}

	public void setSeType(String seType) {
		this.seType = seType;
	}

	public String getSpecAuthNum() {
		return this.specAuthNum;
	}

	public void setSpecAuthNum(String specAuthNum) {
		this.specAuthNum = specAuthNum;
	}

	public BigDecimal getSsFee() {
		return this.ssFee;
	}

	public void setSsFee(BigDecimal ssFee) {
		this.ssFee = ssFee;
	}

	public BigDecimal getSsFeePaid() {
		return this.ssFeePaid;
	}

	public void setSsFeePaid(BigDecimal ssFeePaid) {
		this.ssFeePaid = ssFeePaid;
	}

	public String getSubmitMethod() {
		return this.submitMethod;
	}

	public void setSubmitMethod(String submitMethod) {
		this.submitMethod = submitMethod;
	}

	public String getTrialDinIndicator() {
		return this.trialDinIndicator;
	}

	public void setTrialDinIndicator(String trialDinIndicator) {
		this.trialDinIndicator = trialDinIndicator;
	}

	public BigDecimal getUpchargeClaimed() {
		return this.upchargeClaimed;
	}

	public void setUpchargeClaimed(BigDecimal upchargeClaimed) {
		this.upchargeClaimed = upchargeClaimed;
	}

	public BigDecimal getUpchargePaid() {
		return this.upchargePaid;
	}

	public void setUpchargePaid(BigDecimal upchargePaid) {
		this.upchargePaid = upchargePaid;
	}

	public String getXrefToOriginalRxNumber() {
		return this.xrefToOriginalRxNumber;
	}

	public void setXrefToOriginalRxNumber(String xrefToOriginalRxNumber) {
		this.xrefToOriginalRxNumber = xrefToOriginalRxNumber;
	}

}
package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the DSS_MED_AUD_HLTH_SRV_XREF database table.
 * 
 */
@Entity
@Table(name = "DSS_MED_AUD_HLTH_SRV_XREF")
public class DssMedAudHlthSrvXref implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DssMedAudHlthSrvXrefPK id;

	@Column(name = "AGE_RESTRICTION")
	private BigDecimal ageRestriction;

	@Column(name = "GENDER_RESTRICTION")
	private String genderRestriction;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	// bi-directional many-to-one association to DssMedAudCriteria
	@ManyToOne
	@JoinColumn(name = "AUDIT_CRITERIA_ID")
	private DssMedAudCriteria dssMedAudCriteria;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
	@JoinColumn(name = "UNIQUE_NUMBER", referencedColumnName = "HEALTH_SERVICE_ID")
	private DssHealthService dssHealthService;

	public DssMedAudHlthSrvXref() {
	}

	public DssMedAudHlthSrvXrefPK getId() {
		return this.id;
	}

	public void setId(DssMedAudHlthSrvXrefPK id) {
		this.id = id;
	}

	public BigDecimal getAgeRestriction() {
		return this.ageRestriction;
	}

	public void setAgeRestriction(BigDecimal ageRestriction) {
		this.ageRestriction = ageRestriction;
	}

	public String getGenderRestriction() {
		return this.genderRestriction;
	}

	public void setGenderRestriction(String genderRestriction) {
		this.genderRestriction = genderRestriction;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public DssMedAudCriteria getDssMedAudCriteria() {
		return this.dssMedAudCriteria;
	}

	public void setDssMedAudCriteria(DssMedAudCriteria dssMedAudCriteria) {
		this.dssMedAudCriteria = dssMedAudCriteria;
	}

	/**
	 * @return the dssHealthService
	 */
	public DssHealthService getDssHealthService() {
		return dssHealthService;
	}

	/**
	 * @param dssHealthService
	 *            the dssHealthService to set
	 */
	public void setDssHealthService(DssHealthService dssHealthService) {
		this.dssHealthService = dssHealthService;
	}

}
package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the DSS_DIN database table.
 * 
 */
@Entity
@Table(name = "DSS_DIN")
public class DssDin implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "DIN")
	private String din;

	@Column(name = "ATC_CODE")
	private String atcCode;

	@Column(name = "DOSAGE")
	private String dosage;

	@Column(name = "DRUG_CLASS")
	private BigDecimal drugClass;

	@Column(name = "DRUG_GROUP")
	private String drugGroup;

	@Temporal(TemporalType.DATE)
	@Column(name = "EFFECTIVE_FROM_DATE")
	private Date effectiveFromDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "EFFECTIVE_TO_DATE")
	private Date effectiveToDate;

	@Column(name = "FORMULARY_DIN")
	private String formularyDin;

	@Column(name = "GENERIC_INDICATOR")
	private String genericIndicator;

	@Column(name = "GENERIC_NAME")
	private String genericName;

	@Column(name = "GP_INDICATOR")
	private String gpIndicator;

	@Column(name = "GREEN_AMBER_RED_INDICATOR")
	private String greenAmberRedIndicator;

	@Column(name = "MANUFACTURER_CODE")
	private String manufacturerCode;

	@Column(name = "NAME")
	private String name;

	@Column(name = "OVERRIDE_ATC_CODE")
	private String overrideAtcCode;

	@Column(name = "PRODUCT_ID")
	private String productId;

	@Column(name = "PRODUCT_TYPE_CODE")
	private String productTypeCode;

	@Column(name = "ROUTE_CODE")
	private String routeCode;

	@Column(name = "SCHED_CODE")
	private String schedCode;

	@Column(name = "SHORT_NAME")
	private String shortName;

	@Temporal(TemporalType.DATE)
	@Column(name = "SP_APP_EFF_DATE")
	private Date spAppEffDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "SP_APP_EXP_DATE")
	private Date spAppExpDate;

	@Column(name = "SP_DIN_STATUS")
	private BigDecimal spDinStatus;

	@Column(name = "SP_TERM_STATUS")
	private BigDecimal spTermStatus;

	private String strength;

	@Column(name = "STRENGTH_UNIT")
	private String strengthUnit;

	// bi-directional one-to-one association to DssPharmAuditDinXref
	@OneToOne(mappedBy = "dssDin")
	private DssPharmAuditDinXref dssPharmAuditDinXref;

	public DssDin() {
	}

	public String getDin() {
		return this.din;
	}

	public void setDin(String din) {
		this.din = din;
	}

	public String getAtcCode() {
		return this.atcCode;
	}

	public void setAtcCode(String atcCode) {
		this.atcCode = atcCode;
	}

	public String getDosage() {
		return this.dosage;
	}

	public void setDosage(String dosage) {
		this.dosage = dosage;
	}

	public BigDecimal getDrugClass() {
		return this.drugClass;
	}

	public void setDrugClass(BigDecimal drugClass) {
		this.drugClass = drugClass;
	}

	public String getDrugGroup() {
		return this.drugGroup;
	}

	public void setDrugGroup(String drugGroup) {
		this.drugGroup = drugGroup;
	}

	public Date getEffectiveFromDate() {
		return this.effectiveFromDate;
	}

	public void setEffectiveFromDate(Date effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}

	public Date getEffectiveToDate() {
		return this.effectiveToDate;
	}

	public void setEffectiveToDate(Date effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}

	public String getFormularyDin() {
		return this.formularyDin;
	}

	public void setFormularyDin(String formularyDin) {
		this.formularyDin = formularyDin;
	}

	public String getGenericIndicator() {
		return this.genericIndicator;
	}

	public void setGenericIndicator(String genericIndicator) {
		this.genericIndicator = genericIndicator;
	}

	public String getGenericName() {
		return this.genericName;
	}

	public void setGenericName(String genericName) {
		this.genericName = genericName;
	}

	public String getGpIndicator() {
		return this.gpIndicator;
	}

	public void setGpIndicator(String gpIndicator) {
		this.gpIndicator = gpIndicator;
	}

	public String getGreenAmberRedIndicator() {
		return this.greenAmberRedIndicator;
	}

	public void setGreenAmberRedIndicator(String greenAmberRedIndicator) {
		this.greenAmberRedIndicator = greenAmberRedIndicator;
	}

	public String getManufacturerCode() {
		return this.manufacturerCode;
	}

	public void setManufacturerCode(String manufacturerCode) {
		this.manufacturerCode = manufacturerCode;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOverrideAtcCode() {
		return this.overrideAtcCode;
	}

	public void setOverrideAtcCode(String overrideAtcCode) {
		this.overrideAtcCode = overrideAtcCode;
	}

	public String getProductId() {
		return this.productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductTypeCode() {
		return this.productTypeCode;
	}

	public void setProductTypeCode(String productTypeCode) {
		this.productTypeCode = productTypeCode;
	}

	public String getRouteCode() {
		return this.routeCode;
	}

	public void setRouteCode(String routeCode) {
		this.routeCode = routeCode;
	}

	public String getSchedCode() {
		return this.schedCode;
	}

	public void setSchedCode(String schedCode) {
		this.schedCode = schedCode;
	}

	public String getShortName() {
		return this.shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public Date getSpAppEffDate() {
		return this.spAppEffDate;
	}

	public void setSpAppEffDate(Date spAppEffDate) {
		this.spAppEffDate = spAppEffDate;
	}

	public Date getSpAppExpDate() {
		return this.spAppExpDate;
	}

	public void setSpAppExpDate(Date spAppExpDate) {
		this.spAppExpDate = spAppExpDate;
	}

	public BigDecimal getSpDinStatus() {
		return this.spDinStatus;
	}

	public void setSpDinStatus(BigDecimal spDinStatus) {
		this.spDinStatus = spDinStatus;
	}

	public BigDecimal getSpTermStatus() {
		return this.spTermStatus;
	}

	public void setSpTermStatus(BigDecimal spTermStatus) {
		this.spTermStatus = spTermStatus;
	}

	public String getStrength() {
		return this.strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getStrengthUnit() {
		return this.strengthUnit;
	}

	public void setStrengthUnit(String strengthUnit) {
		this.strengthUnit = strengthUnit;
	}

	public DssPharmAuditDinXref getDssPharmAuditDinXref() {
		return this.dssPharmAuditDinXref;
	}

	public void setDssPharmAuditDinXref(DssPharmAuditDinXref dssPharmAuditDinXref) {
		this.dssPharmAuditDinXref = dssPharmAuditDinXref;
	}

}
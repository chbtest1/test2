package ca.medavie.nspp.audit.service.data.mappedEntity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DssPharmaCareALRMappedEntityPK {

	@Column
	private String claim_ref_num;
	@Column
	private Long audit_run_number;

	public String getClaim_ref_num() {
		return claim_ref_num;
	}

	public void setClaim_ref_num(String claim_ref_num) {
		this.claim_ref_num = claim_ref_num;
	}

	public Long getAudit_run_number() {
		return audit_run_number;
	}

	public void setAudit_run_number(Long audit_run_number) {
		this.audit_run_number = audit_run_number;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((audit_run_number == null) ? 0 : audit_run_number.hashCode());
		result = prime * result + ((claim_ref_num == null) ? 0 : claim_ref_num.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DssPharmaCareALRMappedEntityPK other = (DssPharmaCareALRMappedEntityPK) obj;
		if (audit_run_number == null) {
			if (other.audit_run_number != null)
				return false;
		} else if (!audit_run_number.equals(other.audit_run_number))
			return false;
		if (claim_ref_num == null) {
			if (other.claim_ref_num != null)
				return false;
		} else if (!claim_ref_num.equals(other.claim_ref_num))
			return false;
		return true;
	}

}

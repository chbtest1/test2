package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the DSS_PROGRAM database table.
 * 
 */
@Entity
@Table(name = "DSS_PROGRAM")
public class DssProgram implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PROGRAM_CODE")
	private String programCode;

	@Temporal(TemporalType.DATE)
	@Column(name = "EFFECTIVE_FROM_DATE")
	private Date effectiveFromDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "EFFECTIVE_TO_DATE")
	private Date effectiveToDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "MASTER_PROGRAM_CODE")
	private String masterProgramCode;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	@Column(name = "PHARM_BENEFIT_CODE")
	private BigDecimal pharmBenefitCode;

	@Column(name = "PHARM_SAS")
	private String pharmSas;

	@Column(name = "PROGRAM_NAME")
	private String programName;

	public DssProgram() {
	}

	public Date getEffectiveFromDate() {
		return this.effectiveFromDate;
	}

	public void setEffectiveFromDate(Date effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}

	public Date getEffectiveToDate() {
		return this.effectiveToDate;
	}

	public void setEffectiveToDate(Date effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getMasterProgramCode() {
		return this.masterProgramCode;
	}

	public void setMasterProgramCode(String masterProgramCode) {
		this.masterProgramCode = masterProgramCode;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public BigDecimal getPharmBenefitCode() {
		return this.pharmBenefitCode;
	}

	public void setPharmBenefitCode(BigDecimal pharmBenefitCode) {
		this.pharmBenefitCode = pharmBenefitCode;
	}

	public String getPharmSas() {
		return this.pharmSas;
	}

	public void setPharmSas(String pharmSas) {
		this.pharmSas = pharmSas;
	}

	public String getProgramCode() {
		return this.programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public String getProgramName() {
		return this.programName;
	}

	@Override
	public String toString() {
		return "DssProgram [programCode=" + programCode + ", effectiveFromDate=" + effectiveFromDate
				+ ", effectiveToDate=" + effectiveToDate + ", lastModified=" + lastModified + ", masterProgramCode="
				+ masterProgramCode + ", modifiedBy=" + modifiedBy + ", pharmBenefitCode=" + pharmBenefitCode
				+ ", pharmSas=" + pharmSas + ", programName=" + programName + "]";
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}

}
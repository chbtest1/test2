package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the DSS_HEALTH_SERVICE_GROUP_XREF database table.
 * 
 */
@Entity
@Table(name="DSS_HEALTH_SERVICE_GROUP_XREF")
public class DssHealthServiceGroupXref implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
//	@OneToMany(fetch = FetchType.LAZY)
//	@JoinColumns({ @JoinColumn(name = "HEALTH_SERVICE_ID", referencedColumnName = "HEALTH_SERVICE_ID")})
//	private List<DssHealthService> dhs;
	

	@EmbeddedId
	private DssHealthServiceGroupXrefPK id;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_TO_DATE")
	private Date effectiveToDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFIED")
	private Date lastModified;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	public DssHealthServiceGroupXref() {
	}

	public DssHealthServiceGroupXrefPK getId() {
		return this.id;
	}

	public void setId(DssHealthServiceGroupXrefPK id) {
		this.id = id;
	}

	public Date getEffectiveToDate() {
		return this.effectiveToDate;
	}

	public void setEffectiveToDate(Date effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

}
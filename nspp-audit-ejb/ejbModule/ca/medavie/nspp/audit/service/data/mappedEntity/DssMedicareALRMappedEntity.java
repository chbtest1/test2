package ca.medavie.nspp.audit.service.data.mappedEntity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

/**
 * The persistent class for the DSS_HEALTH_SERVICE database table.
 * 
 */
@Entity
@SqlResultSetMapping(name = "DssMedicareALRQueryMapping", entities = { @EntityResult(entityClass = DssMedicareALRMappedEntity.class, fields = {
		@FieldResult(name = "id.se_number", column = "se_number"),
		@FieldResult(name = "id.se_sequence_number", column = "se_sequence_number"),
		@FieldResult(name = "id.response_tag_number", column = "response_tag_number"),
		@FieldResult(name = "id.audit_run_number", column = "audit_run_number"),
		@FieldResult(name = "provider_number", column = "provider_number"),
		@FieldResult(name = "provider_type", column = "provider_type"),
		@FieldResult(name = "health_card_number", column = "health_card_number"),
		@FieldResult(name = "se_selection_date", column = "se_selection_date"),
		@FieldResult(name = "audit_letter_creation_date", column = "audit_letter_creation_date"),
		@FieldResult(name = "response_status_code", column = "response_status_code"),
		@FieldResult(name = "number_of_times_sent", column = "number_of_times_sent"),
		@FieldResult(name = "chargeback_amount", column = "chargeback_amount"),
		@FieldResult(name = "number_of_services_affected", column = "number_of_services_affected"),
		@FieldResult(name = "audit_note", column = "audit_note"),
		@FieldResult(name = "modified_by", column = "modified_by"),
		@FieldResult(name = "last_modified", column = "last_modified"),
		@FieldResult(name = "audit_type", column = "audit_type"),
		@FieldResult(name = "health_service_code", column = "health_service_code"),
		@FieldResult(name = "service_start_date", column = "service_start_date"),
		@FieldResult(name = "modifiers", column = "modifiers"),
		@FieldResult(name = "implicit_modifiers", column = "implicit_modifiers"),
		@FieldResult(name = "payable_amount", column = "amount_paid"),
		@FieldResult(name = "provider_name", column = "provider_name"),
		@FieldResult(name = "individual_name", column = "individual_name")}) })
public class DssMedicareALRMappedEntity {

	@EmbeddedId
	private DssMedicareALRMappedEntityPK id;
	@Column
	private Long provider_number;
	@Column
	private String provider_type;
	@Column
	private String health_card_number;
	@Column
	private Date se_selection_date;
	@Column
	private Date audit_letter_creation_date;
	@Column
	private String response_status_code;
	@Column
	private Integer number_of_times_sent;
	@Column
	private BigDecimal chargeback_amount;
	@Column
	private Integer number_of_services_affected;
	@Column
	private String audit_note;
	@Column
	private String modified_by;
	@Column
	private Date last_modified;
	@Column
	private String audit_type;
	@Column
	private String health_service_code;
	@Column
	private Date service_start_date;
	@Column
	private String modifiers;
	@Column
	private String implicit_modifiers;
	@Column
	private String payable_amount;
	@Column
	private String provider_name;
	@Column
	private String individual_name;

	/**
	 * @return the id
	 */
	public DssMedicareALRMappedEntityPK getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(DssMedicareALRMappedEntityPK id) {
		this.id = id;
	}

	/**
	 * @return the provider_number
	 */
	public Long getProvider_number() {
		return provider_number;
	}

	/**
	 * @param provider_number
	 *            the provider_number to set
	 */
	public void setProvider_number(Long provider_number) {
		this.provider_number = provider_number;
	}

	/**
	 * @return the provider_type
	 */
	public String getProvider_type() {
		return provider_type;
	}

	/**
	 * @param provider_type
	 *            the provider_type to set
	 */
	public void setProvider_type(String provider_type) {
		this.provider_type = provider_type;
	}

	/**
	 * @return the health_card_number
	 */
	public String getHealth_card_number() {
		return health_card_number;
	}

	/**
	 * @param health_card_number
	 *            the health_card_number to set
	 */
	public void setHealth_card_number(String health_card_number) {
		this.health_card_number = health_card_number;
	}

	/**
	 * @return the se_selection_date
	 */
	public Date getSe_selection_date() {
		return se_selection_date;
	}

	/**
	 * @param se_selection_date
	 *            the se_selection_date to set
	 */
	public void setSe_selection_date(Date se_selection_date) {
		this.se_selection_date = se_selection_date;
	}

	/**
	 * @return the audit_letter_creation_date
	 */
	public Date getAudit_letter_creation_date() {
		return audit_letter_creation_date;
	}

	/**
	 * @param audit_letter_creation_date
	 *            the audit_letter_creation_date to set
	 */
	public void setAudit_letter_creation_date(Date audit_letter_creation_date) {
		this.audit_letter_creation_date = audit_letter_creation_date;
	}

	/**
	 * @return the response_status_code
	 */
	public String getResponse_status_code() {
		return response_status_code;
	}

	/**
	 * @param response_status_code
	 *            the response_status_code to set
	 */
	public void setResponse_status_code(String response_status_code) {
		this.response_status_code = response_status_code;
	}

	/**
	 * @return the number_of_times_sent
	 */
	public Integer getNumber_of_times_sent() {
		return number_of_times_sent;
	}

	/**
	 * @param number_of_times_sent
	 *            the number_of_times_sent to set
	 */
	public void setNumber_of_times_sent(Integer number_of_times_sent) {
		this.number_of_times_sent = number_of_times_sent;
	}

	/**
	 * @return the chargeback_amount
	 */
	public BigDecimal getChargeback_amount() {
		return chargeback_amount;
	}

	/**
	 * @param chargeback_amount
	 *            the chargeback_amount to set
	 */
	public void setChargeback_amount(BigDecimal chargeback_amount) {
		this.chargeback_amount = chargeback_amount;
	}

	/**
	 * @return the number_of_services_affected
	 */
	public Integer getNumber_of_services_affected() {
		return number_of_services_affected;
	}

	/**
	 * @param number_of_services_affected
	 *            the number_of_services_affected to set
	 */
	public void setNumber_of_services_affected(Integer number_of_services_affected) {
		this.number_of_services_affected = number_of_services_affected;
	}

	/**
	 * @return the audit_note
	 */
	public String getAudit_note() {
		return audit_note;
	}

	/**
	 * @param audit_note
	 *            the audit_note to set
	 */
	public void setAudit_note(String audit_note) {
		this.audit_note = audit_note;
	}

	/**
	 * @return the modified_by
	 */
	public String getModified_by() {
		return modified_by;
	}

	/**
	 * @param modified_by
	 *            the modified_by to set
	 */
	public void setModified_by(String modified_by) {
		this.modified_by = modified_by;
	}

	/**
	 * @return the last_modified
	 */
	public Date getLast_modified() {
		return last_modified;
	}

	/**
	 * @param last_modified
	 *            the last_modified to set
	 */
	public void setLast_modified(Date last_modified) {
		this.last_modified = last_modified;
	}

	/**
	 * @return the audit_type
	 */
	public String getAudit_type() {
		return audit_type;
	}

	/**
	 * @param audit_type
	 *            the audit_type to set
	 */
	public void setAudit_type(String audit_type) {
		this.audit_type = audit_type;
	}

	/**
	 * @return the health_service_code
	 */
	public String getHealth_service_code() {
		return health_service_code;
	}

	/**
	 * @param health_service_code
	 *            the health_service_code to set
	 */
	public void setHealth_service_code(String health_service_code) {
		this.health_service_code = health_service_code;
	}

	/**
	 * @return the service_start_date
	 */
	public Date getService_start_date() {
		return service_start_date;
	}

	/**
	 * @param service_start_date
	 *            the service_start_date to set
	 */
	public void setService_start_date(Date service_start_date) {
		this.service_start_date = service_start_date;
	}

	/**
	 * @return the modifiers
	 */
	public String getModifiers() {
		return modifiers;
	}

	/**
	 * @param modifiers
	 *            the modifiers to set
	 */
	public void setModifiers(String modifiers) {
		this.modifiers = modifiers;
	}

	/**
	 * @return the implicit_modifiers
	 */
	public String getImplicit_modifiers() {
		return implicit_modifiers;
	}

	/**
	 * @param implicit_modifiers
	 *            the implicit_modifiers to set
	 */
	public void setImplicit_modifiers(String implicit_modifiers) {
		this.implicit_modifiers = implicit_modifiers;
	}

	/**
	 * @return the payable_amount
	 */
	public String getPayable_amount() {
		return payable_amount;
	}

	/**
	 * @param payable_amount
	 *            the payable_amount to set
	 */
	public void setPayable_amount(String payable_amount) {
		this.payable_amount = payable_amount;
	}

	/**
	 * @return the provider_name
	 */
	public String getProvider_name() {
		return provider_name;
	}

	/**
	 * @param provider_name
	 *            the provider_name to set
	 */
	public void setProvider_name(String provider_name) {
		this.provider_name = provider_name;
	}

	/**
	 * @return the individual_name
	 */
	public String getIndividual_name() {
		return individual_name;
	}

	/**
	 * @param individual_name the individual_name to set
	 */
	public void setIndividual_name(String individual_name) {
		this.individual_name = individual_name;
	}

}
package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;

/**
 * The primary key class for the DSS_TOWN_PEER_GROUP_XREF database table.
 * 
 */
@Embeddable
public class DssTownPeerGroupXrefPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="PROVIDER_PEER_GROUP_ID")
	private long providerPeerGroupId;

	@Temporal(TemporalType.DATE)
	@Column(name="YEAR_END_DATE")
	private java.util.Date yearEndDate;

	@Column(name="TOWN_CODE")
	private BigDecimal townCode;

	public DssTownPeerGroupXrefPK() {
	}
	public long getProviderPeerGroupId() {
		return this.providerPeerGroupId;
	}
	public void setProviderPeerGroupId(long providerPeerGroupId) {
		this.providerPeerGroupId = providerPeerGroupId;
	}
	public java.util.Date getYearEndDate() {
		return this.yearEndDate;
	}
	public void setYearEndDate(java.util.Date yearEndDate) {
		this.yearEndDate = yearEndDate;
	}
	public BigDecimal getTownCode() {
		return this.townCode;
	}
	public void setTownCode(BigDecimal townCode) {
		this.townCode = townCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (providerPeerGroupId ^ (providerPeerGroupId >>> 32));
		result = prime * result + ((townCode == null) ? 0 : townCode.hashCode());
		result = prime * result + ((yearEndDate == null) ? 0 : yearEndDate.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DssTownPeerGroupXrefPK other = (DssTownPeerGroupXrefPK) obj;
		if (providerPeerGroupId != other.providerPeerGroupId)
			return false;
		if (townCode == null) {
			if (other.townCode != null)
				return false;
		} else if (!townCode.equals(other.townCode))
			return false;
		if (yearEndDate == null) {
			if (other.yearEndDate != null)
				return false;
		} else if (!yearEndDate.equals(other.yearEndDate))
			return false;
		return true;
	}

}
package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class DssProvTypePeerXrefPK implements Serializable {

	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "PROVIDER_PEER_GROUP_ID")
	private long providerPeerGroupId;

	@Temporal(TemporalType.DATE)
	@Column(name = "YEAR_END_DATE")
	private java.util.Date yearEndDate;

	@Column(name = "PROVIDER_TYPE")
	private String providerType;

	public DssProvTypePeerXrefPK() {
	}

	public long getProviderPeerGroupId() {
		return this.providerPeerGroupId;
	}

	public void setProviderPeerGroupId(long providerPeerGroupId) {
		this.providerPeerGroupId = providerPeerGroupId;
	}

	public java.util.Date getYearEndDate() {
		return this.yearEndDate;
	}

	public void setYearEndDate(java.util.Date yearEndDate) {
		this.yearEndDate = yearEndDate;
	}

	public String getProviderType() {
		return this.providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssProvTypePeerXrefPK)) {
			return false;
		}
		DssProvTypePeerXrefPK castOther = (DssProvTypePeerXrefPK) other;
		return (this.providerPeerGroupId == castOther.providerPeerGroupId)
				&& this.yearEndDate.equals(castOther.yearEndDate) && this.providerType.equals(castOther.providerType);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.providerPeerGroupId ^ (this.providerPeerGroupId >>> 32)));
		hash = hash * prime + this.yearEndDate.hashCode();
		hash = hash * prime + this.providerType.hashCode();

		return hash;
	}

}

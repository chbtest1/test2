package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the CEXP database table.
 * 
 */
@Entity
public class Cexp implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CexpPK id;

	@Column(name="ACCUM_ID")
	private String accumId;

	@Column(name="ACCUM_XREF")
	private BigDecimal accumXref;

	@Column(name="ALLOWABLE_COST")
	private BigDecimal allowableCost;

	@Column(name="ANN_DED_FAM_SAT")
	private BigDecimal annDedFamSat;

	@Column(name="ANN_DED_FAM_TWD")
	private BigDecimal annDedFamTwd;

	@Column(name="ANN_DED_IND_SAT")
	private BigDecimal annDedIndSat;

	@Column(name="ANN_DED_IND_TWD")
	private BigDecimal annDedIndTwd;

	@Column(name="ANN_DED_SINGLE_SAT")
	private BigDecimal annDedSingleSat;

	@Column(name="ANN_DED_SINGLE_TWD")
	private BigDecimal annDedSingleTwd;

	@Column(name="AUTHORIZATION_CODE")
	private BigDecimal authorizationCode;

	@Column(name="AUTHORIZATION_NUM")
	private BigDecimal authorizationNum;

	@Column(name="CEXP_REC_ID")
	private BigDecimal cexpRecId;

	@Column(name="CLAIM_DATE")
	private BigDecimal claimDate;

	@Column(name="CLAIM_STATUS")
	private BigDecimal claimStatus;

	@Column(name="CLAIM_XREF_NUM")
	private String claimXrefNum;

	@Column(name="CLIENT_ID")
	private String clientId;

	@Column(name="CLIENT_LOC")
	private String clientLoc;

	@Column(name="COINS_PAID")
	private BigDecimal coinsPaid;

	@Column(name="COMM_FLAG")
	private BigDecimal commFlag;

	@Column(name="COMP_FEE")
	private BigDecimal compFee;

	@Column(name="COMP_FEE_PAID")
	private BigDecimal compFeePaid;

	@Column(name="COPAY_CLAIMED")
	private BigDecimal copayClaimed;

	@Column(name="COPAY_PAID")
	private BigDecimal copayPaid;

	@Column(name="COST_BASIS")
	private String costBasis;

	@Column(name="COST_DIFF")
	private BigDecimal costDiff;

	@Column(name="COST_PLUS")
	private BigDecimal costPlus;

	@Column(name="COST_UPCHARGE")
	private BigDecimal costUpcharge;

	@Column(name="COST_UPCHARGE_PAID")
	private BigDecimal costUpchargePaid;

	@Column(name="CURRENT_RX_NUM")
	private String currentRxNum;

	@Column(name="DATE_PROVIDED")
	private BigDecimal dateProvided;

	@Column(name="DATE_RECEIVED")
	private BigDecimal dateReceived;

	@Column(name="DATE_XREF_NUM")
	private BigDecimal dateXrefNum;

	@Column(name="DAYS_SUPPLY")
	private BigDecimal daysSupply;

	@Column(name="DEDUCT_PAID")
	private BigDecimal deductPaid;

	@Column(name="DEDUCT_SATISFIED")
	private String deductSatisfied;

	@Column(name="DEPENDENT_ID")
	private BigDecimal dependentId;

	private String din;

	@Column(name="DOSAGE_FORM")
	private String dosageForm;

	@Column(name="DRUG_ACCUM_PAID")
	private BigDecimal drugAccumPaid;

	@Column(name="DRUG_COST")
	private BigDecimal drugCost;

	@Column(name="ECL_CODE")
	private String eclCode;

	@Column(name="EFT_REF_NUM")
	private String eftRefNum;

	@Column(name="ERROR_CODES")
	private String errorCodes;

	@Column(name="FED_SCHED")
	private String fedSched;

	@Column(name="FEDERAL_GST")
	private BigDecimal federalGst;

	@Column(name="FEDERAL_GST_PAID")
	private BigDecimal federalGstPaid;

	@Column(name="GENERIC_INCEN")
	private BigDecimal genericIncen;

	@Column(name="GENERIC_INCEN_PAID")
	private BigDecimal genericIncenPaid;

	private String gsas;

	@Column(name="INGRED_COST_PAID")
	private BigDecimal ingredCostPaid;

	@Column(name="LANG_CODE")
	private String langCode;

	@Column(name="MAJOR_INGRED")
	private BigDecimal majorIngred;

	@Column(name="NEW_PATIENT")
	private BigDecimal newPatient;

	@Column(name="NEXT_ROLL_DATE")
	private BigDecimal nextRollDate;

	@Column(name="NUM_ERRORS")
	private BigDecimal numErrors;

	@Column(name="PATIENT_CODE")
	private String patientCode;

	@Column(name="PATIENT_DOB")
	private BigDecimal patientDob;

	@Column(name="PATIENT_FNAME")
	private String patientFname;

	@Column(name="PATIENT_LNAME")
	private String patientLname;

	@Column(name="PATIENT_SEX")
	private String patientSex;

	@Column(name="PAYEE_CODE")
	private BigDecimal payeeCode;

	@Column(name="PAYMENT_METHOD")
	private BigDecimal paymentMethod;

	@Column(name="PHARM_NUM")
	private String pharmNum;

	@Column(name="PHARM_PROV")
	private String pharmProv;

	@Column(name="PLAN_NUM")
	private String planNum;

	@Column(name="POS_DEVICE_ID")
	private String posDeviceId;

	@Column(name="PROD_SELECTION")
	private String prodSelection;

	@Column(name="PROF_FEE")
	private BigDecimal profFee;

	@Column(name="PROF_FEE_PAID")
	private BigDecimal profFeePaid;

	@Column(name="PROV_SALES_TAX")
	private BigDecimal provSalesTax;

	@Column(name="PROV_SALES_TAX_PAID")
	private BigDecimal provSalesTaxPaid;

	@Column(name="PROV_SCHED")
	private String provSched;

	@Column(name="PROV_SOFT_ID")
	private String provSoftId;

	@Column(name="QTY_CLAIMED")
	private BigDecimal qtyClaimed;

	@Column(name="QTY_PAID")
	private BigDecimal qtyPaid;

	@Column(name="REC_TYPE")
	private String recType;

	@Column(name="REFILL_CODE")
	private BigDecimal refillCode;

	@Column(name="REJECT_CODES")
	private String rejectCodes;

	private BigDecimal relationship;

	@Column(name="ROUTE_OF_ADMIN")
	private String routeOfAdmin;

	@Column(name="RX_NUM")
	private String rxNum;

	@Column(name="SS_FEE")
	private BigDecimal ssFee;

	@Column(name="SS_FEE_PAID")
	private BigDecimal ssFeePaid;

	@Column(name="SUBMIT_METHOD")
	private BigDecimal submitMethod;

	@Column(name="TEST_INDICATOR")
	private String testIndicator;

	@Column(name="TH_CLASS")
	private BigDecimal thClass;

	@Column(name="TOTAL_AMT_CLAIMED")
	private BigDecimal totalAmtClaimed;

	@Column(name="TOTAL_AMT_PAID")
	private BigDecimal totalAmtPaid;

	@Column(name="TRACE_XREF_NUM")
	private String traceXrefNum;

	@Column(name="UNIT_PRICE")
	private BigDecimal unitPrice;

	public Cexp() {
	}

	public CexpPK getId() {
		return this.id;
	}

	public void setId(CexpPK id) {
		this.id = id;
	}

	public String getAccumId() {
		return this.accumId;
	}

	public void setAccumId(String accumId) {
		this.accumId = accumId;
	}

	public BigDecimal getAccumXref() {
		return this.accumXref;
	}

	public void setAccumXref(BigDecimal accumXref) {
		this.accumXref = accumXref;
	}

	public BigDecimal getAllowableCost() {
		return this.allowableCost;
	}

	public void setAllowableCost(BigDecimal allowableCost) {
		this.allowableCost = allowableCost;
	}

	public BigDecimal getAnnDedFamSat() {
		return this.annDedFamSat;
	}

	public void setAnnDedFamSat(BigDecimal annDedFamSat) {
		this.annDedFamSat = annDedFamSat;
	}

	public BigDecimal getAnnDedFamTwd() {
		return this.annDedFamTwd;
	}

	public void setAnnDedFamTwd(BigDecimal annDedFamTwd) {
		this.annDedFamTwd = annDedFamTwd;
	}

	public BigDecimal getAnnDedIndSat() {
		return this.annDedIndSat;
	}

	public void setAnnDedIndSat(BigDecimal annDedIndSat) {
		this.annDedIndSat = annDedIndSat;
	}

	public BigDecimal getAnnDedIndTwd() {
		return this.annDedIndTwd;
	}

	public void setAnnDedIndTwd(BigDecimal annDedIndTwd) {
		this.annDedIndTwd = annDedIndTwd;
	}

	public BigDecimal getAnnDedSingleSat() {
		return this.annDedSingleSat;
	}

	public void setAnnDedSingleSat(BigDecimal annDedSingleSat) {
		this.annDedSingleSat = annDedSingleSat;
	}

	public BigDecimal getAnnDedSingleTwd() {
		return this.annDedSingleTwd;
	}

	public void setAnnDedSingleTwd(BigDecimal annDedSingleTwd) {
		this.annDedSingleTwd = annDedSingleTwd;
	}

	public BigDecimal getAuthorizationCode() {
		return this.authorizationCode;
	}

	public void setAuthorizationCode(BigDecimal authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	public BigDecimal getAuthorizationNum() {
		return this.authorizationNum;
	}

	public void setAuthorizationNum(BigDecimal authorizationNum) {
		this.authorizationNum = authorizationNum;
	}

	public BigDecimal getCexpRecId() {
		return this.cexpRecId;
	}

	public void setCexpRecId(BigDecimal cexpRecId) {
		this.cexpRecId = cexpRecId;
	}

	public BigDecimal getClaimDate() {
		return this.claimDate;
	}

	public void setClaimDate(BigDecimal claimDate) {
		this.claimDate = claimDate;
	}

	public BigDecimal getClaimStatus() {
		return this.claimStatus;
	}

	public void setClaimStatus(BigDecimal claimStatus) {
		this.claimStatus = claimStatus;
	}

	public String getClaimXrefNum() {
		return this.claimXrefNum;
	}

	public void setClaimXrefNum(String claimXrefNum) {
		this.claimXrefNum = claimXrefNum;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientLoc() {
		return this.clientLoc;
	}

	public void setClientLoc(String clientLoc) {
		this.clientLoc = clientLoc;
	}

	public BigDecimal getCoinsPaid() {
		return this.coinsPaid;
	}

	public void setCoinsPaid(BigDecimal coinsPaid) {
		this.coinsPaid = coinsPaid;
	}

	public BigDecimal getCommFlag() {
		return this.commFlag;
	}

	public void setCommFlag(BigDecimal commFlag) {
		this.commFlag = commFlag;
	}

	public BigDecimal getCompFee() {
		return this.compFee;
	}

	public void setCompFee(BigDecimal compFee) {
		this.compFee = compFee;
	}

	public BigDecimal getCompFeePaid() {
		return this.compFeePaid;
	}

	public void setCompFeePaid(BigDecimal compFeePaid) {
		this.compFeePaid = compFeePaid;
	}

	public BigDecimal getCopayClaimed() {
		return this.copayClaimed;
	}

	public void setCopayClaimed(BigDecimal copayClaimed) {
		this.copayClaimed = copayClaimed;
	}

	public BigDecimal getCopayPaid() {
		return this.copayPaid;
	}

	public void setCopayPaid(BigDecimal copayPaid) {
		this.copayPaid = copayPaid;
	}

	public String getCostBasis() {
		return this.costBasis;
	}

	public void setCostBasis(String costBasis) {
		this.costBasis = costBasis;
	}

	public BigDecimal getCostDiff() {
		return this.costDiff;
	}

	public void setCostDiff(BigDecimal costDiff) {
		this.costDiff = costDiff;
	}

	public BigDecimal getCostPlus() {
		return this.costPlus;
	}

	public void setCostPlus(BigDecimal costPlus) {
		this.costPlus = costPlus;
	}

	public BigDecimal getCostUpcharge() {
		return this.costUpcharge;
	}

	public void setCostUpcharge(BigDecimal costUpcharge) {
		this.costUpcharge = costUpcharge;
	}

	public BigDecimal getCostUpchargePaid() {
		return this.costUpchargePaid;
	}

	public void setCostUpchargePaid(BigDecimal costUpchargePaid) {
		this.costUpchargePaid = costUpchargePaid;
	}

	public String getCurrentRxNum() {
		return this.currentRxNum;
	}

	public void setCurrentRxNum(String currentRxNum) {
		this.currentRxNum = currentRxNum;
	}

	public BigDecimal getDateProvided() {
		return this.dateProvided;
	}

	public void setDateProvided(BigDecimal dateProvided) {
		this.dateProvided = dateProvided;
	}

	public BigDecimal getDateReceived() {
		return this.dateReceived;
	}

	public void setDateReceived(BigDecimal dateReceived) {
		this.dateReceived = dateReceived;
	}

	public BigDecimal getDateXrefNum() {
		return this.dateXrefNum;
	}

	public void setDateXrefNum(BigDecimal dateXrefNum) {
		this.dateXrefNum = dateXrefNum;
	}

	public BigDecimal getDaysSupply() {
		return this.daysSupply;
	}

	public void setDaysSupply(BigDecimal daysSupply) {
		this.daysSupply = daysSupply;
	}

	public BigDecimal getDeductPaid() {
		return this.deductPaid;
	}

	public void setDeductPaid(BigDecimal deductPaid) {
		this.deductPaid = deductPaid;
	}

	public String getDeductSatisfied() {
		return this.deductSatisfied;
	}

	public void setDeductSatisfied(String deductSatisfied) {
		this.deductSatisfied = deductSatisfied;
	}

	public BigDecimal getDependentId() {
		return this.dependentId;
	}

	public void setDependentId(BigDecimal dependentId) {
		this.dependentId = dependentId;
	}

	public String getDin() {
		return this.din;
	}

	public void setDin(String din) {
		this.din = din;
	}

	public String getDosageForm() {
		return this.dosageForm;
	}

	public void setDosageForm(String dosageForm) {
		this.dosageForm = dosageForm;
	}

	public BigDecimal getDrugAccumPaid() {
		return this.drugAccumPaid;
	}

	public void setDrugAccumPaid(BigDecimal drugAccumPaid) {
		this.drugAccumPaid = drugAccumPaid;
	}

	public BigDecimal getDrugCost() {
		return this.drugCost;
	}

	public void setDrugCost(BigDecimal drugCost) {
		this.drugCost = drugCost;
	}

	public String getEclCode() {
		return this.eclCode;
	}

	public void setEclCode(String eclCode) {
		this.eclCode = eclCode;
	}

	public String getEftRefNum() {
		return this.eftRefNum;
	}

	public void setEftRefNum(String eftRefNum) {
		this.eftRefNum = eftRefNum;
	}

	public String getErrorCodes() {
		return this.errorCodes;
	}

	public void setErrorCodes(String errorCodes) {
		this.errorCodes = errorCodes;
	}

	public String getFedSched() {
		return this.fedSched;
	}

	public void setFedSched(String fedSched) {
		this.fedSched = fedSched;
	}

	public BigDecimal getFederalGst() {
		return this.federalGst;
	}

	public void setFederalGst(BigDecimal federalGst) {
		this.federalGst = federalGst;
	}

	public BigDecimal getFederalGstPaid() {
		return this.federalGstPaid;
	}

	public void setFederalGstPaid(BigDecimal federalGstPaid) {
		this.federalGstPaid = federalGstPaid;
	}

	public BigDecimal getGenericIncen() {
		return this.genericIncen;
	}

	public void setGenericIncen(BigDecimal genericIncen) {
		this.genericIncen = genericIncen;
	}

	public BigDecimal getGenericIncenPaid() {
		return this.genericIncenPaid;
	}

	public void setGenericIncenPaid(BigDecimal genericIncenPaid) {
		this.genericIncenPaid = genericIncenPaid;
	}

	public String getGsas() {
		return this.gsas;
	}

	public void setGsas(String gsas) {
		this.gsas = gsas;
	}

	public BigDecimal getIngredCostPaid() {
		return this.ingredCostPaid;
	}

	public void setIngredCostPaid(BigDecimal ingredCostPaid) {
		this.ingredCostPaid = ingredCostPaid;
	}

	public String getLangCode() {
		return this.langCode;
	}

	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}

	public BigDecimal getMajorIngred() {
		return this.majorIngred;
	}

	public void setMajorIngred(BigDecimal majorIngred) {
		this.majorIngred = majorIngred;
	}

	public BigDecimal getNewPatient() {
		return this.newPatient;
	}

	public void setNewPatient(BigDecimal newPatient) {
		this.newPatient = newPatient;
	}

	public BigDecimal getNextRollDate() {
		return this.nextRollDate;
	}

	public void setNextRollDate(BigDecimal nextRollDate) {
		this.nextRollDate = nextRollDate;
	}

	public BigDecimal getNumErrors() {
		return this.numErrors;
	}

	public void setNumErrors(BigDecimal numErrors) {
		this.numErrors = numErrors;
	}

	public String getPatientCode() {
		return this.patientCode;
	}

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	public BigDecimal getPatientDob() {
		return this.patientDob;
	}

	public void setPatientDob(BigDecimal patientDob) {
		this.patientDob = patientDob;
	}

	public String getPatientFname() {
		return this.patientFname;
	}

	public void setPatientFname(String patientFname) {
		this.patientFname = patientFname;
	}

	public String getPatientLname() {
		return this.patientLname;
	}

	public void setPatientLname(String patientLname) {
		this.patientLname = patientLname;
	}

	public String getPatientSex() {
		return this.patientSex;
	}

	public void setPatientSex(String patientSex) {
		this.patientSex = patientSex;
	}

	public BigDecimal getPayeeCode() {
		return this.payeeCode;
	}

	public void setPayeeCode(BigDecimal payeeCode) {
		this.payeeCode = payeeCode;
	}

	public BigDecimal getPaymentMethod() {
		return this.paymentMethod;
	}

	public void setPaymentMethod(BigDecimal paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getPharmNum() {
		return this.pharmNum;
	}

	public void setPharmNum(String pharmNum) {
		this.pharmNum = pharmNum;
	}

	public String getPharmProv() {
		return this.pharmProv;
	}

	public void setPharmProv(String pharmProv) {
		this.pharmProv = pharmProv;
	}

	public String getPlanNum() {
		return this.planNum;
	}

	public void setPlanNum(String planNum) {
		this.planNum = planNum;
	}

	public String getPosDeviceId() {
		return this.posDeviceId;
	}

	public void setPosDeviceId(String posDeviceId) {
		this.posDeviceId = posDeviceId;
	}

	public String getProdSelection() {
		return this.prodSelection;
	}

	public void setProdSelection(String prodSelection) {
		this.prodSelection = prodSelection;
	}

	public BigDecimal getProfFee() {
		return this.profFee;
	}

	public void setProfFee(BigDecimal profFee) {
		this.profFee = profFee;
	}

	public BigDecimal getProfFeePaid() {
		return this.profFeePaid;
	}

	public void setProfFeePaid(BigDecimal profFeePaid) {
		this.profFeePaid = profFeePaid;
	}

	public BigDecimal getProvSalesTax() {
		return this.provSalesTax;
	}

	public void setProvSalesTax(BigDecimal provSalesTax) {
		this.provSalesTax = provSalesTax;
	}

	public BigDecimal getProvSalesTaxPaid() {
		return this.provSalesTaxPaid;
	}

	public void setProvSalesTaxPaid(BigDecimal provSalesTaxPaid) {
		this.provSalesTaxPaid = provSalesTaxPaid;
	}

	public String getProvSched() {
		return this.provSched;
	}

	public void setProvSched(String provSched) {
		this.provSched = provSched;
	}

	public String getProvSoftId() {
		return this.provSoftId;
	}

	public void setProvSoftId(String provSoftId) {
		this.provSoftId = provSoftId;
	}

	public BigDecimal getQtyClaimed() {
		return this.qtyClaimed;
	}

	public void setQtyClaimed(BigDecimal qtyClaimed) {
		this.qtyClaimed = qtyClaimed;
	}

	public BigDecimal getQtyPaid() {
		return this.qtyPaid;
	}

	public void setQtyPaid(BigDecimal qtyPaid) {
		this.qtyPaid = qtyPaid;
	}

	public String getRecType() {
		return this.recType;
	}

	public void setRecType(String recType) {
		this.recType = recType;
	}

	public BigDecimal getRefillCode() {
		return this.refillCode;
	}

	public void setRefillCode(BigDecimal refillCode) {
		this.refillCode = refillCode;
	}

	public String getRejectCodes() {
		return this.rejectCodes;
	}

	public void setRejectCodes(String rejectCodes) {
		this.rejectCodes = rejectCodes;
	}

	public BigDecimal getRelationship() {
		return this.relationship;
	}

	public void setRelationship(BigDecimal relationship) {
		this.relationship = relationship;
	}

	public String getRouteOfAdmin() {
		return this.routeOfAdmin;
	}

	public void setRouteOfAdmin(String routeOfAdmin) {
		this.routeOfAdmin = routeOfAdmin;
	}

	public String getRxNum() {
		return this.rxNum;
	}

	public void setRxNum(String rxNum) {
		this.rxNum = rxNum;
	}

	public BigDecimal getSsFee() {
		return this.ssFee;
	}

	public void setSsFee(BigDecimal ssFee) {
		this.ssFee = ssFee;
	}

	public BigDecimal getSsFeePaid() {
		return this.ssFeePaid;
	}

	public void setSsFeePaid(BigDecimal ssFeePaid) {
		this.ssFeePaid = ssFeePaid;
	}

	public BigDecimal getSubmitMethod() {
		return this.submitMethod;
	}

	public void setSubmitMethod(BigDecimal submitMethod) {
		this.submitMethod = submitMethod;
	}

	public String getTestIndicator() {
		return this.testIndicator;
	}

	public void setTestIndicator(String testIndicator) {
		this.testIndicator = testIndicator;
	}

	public BigDecimal getThClass() {
		return this.thClass;
	}

	public void setThClass(BigDecimal thClass) {
		this.thClass = thClass;
	}

	public BigDecimal getTotalAmtClaimed() {
		return this.totalAmtClaimed;
	}

	public void setTotalAmtClaimed(BigDecimal totalAmtClaimed) {
		this.totalAmtClaimed = totalAmtClaimed;
	}

	public BigDecimal getTotalAmtPaid() {
		return this.totalAmtPaid;
	}

	public void setTotalAmtPaid(BigDecimal totalAmtPaid) {
		this.totalAmtPaid = totalAmtPaid;
	}

	public String getTraceXrefNum() {
		return this.traceXrefNum;
	}

	public void setTraceXrefNum(String traceXrefNum) {
		this.traceXrefNum = traceXrefNum;
	}

	public BigDecimal getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

}
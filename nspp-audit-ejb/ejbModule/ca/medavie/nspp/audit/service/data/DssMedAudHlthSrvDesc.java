package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the DSS_MED_AUD_HLTH_SRV_DESC database table.
 * 
 */
@Entity
@Table(name="DSS_MED_AUD_HLTH_SRV_DESC")
public class DssMedAudHlthSrvDesc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="UNIQUE_NUMBER")
	private long uniqueNumber;

	private String description;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFIED")
	private Date lastModified;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	public DssMedAudHlthSrvDesc() {
	}

	public long getUniqueNumber() {
		return this.uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

}
package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_COMPLETE_AUDIT database table.
 * 
 */
@Embeddable
public class DssCompleteAuditPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="AUDIT_RUN_NUMBER")
	private long auditRunNumber;

	@Column(name="AUDIT_TYPE")
	private String auditType;

	public DssCompleteAuditPK() {
	}
	public long getAuditRunNumber() {
		return this.auditRunNumber;
	}
	public void setAuditRunNumber(long auditRunNumber) {
		this.auditRunNumber = auditRunNumber;
	}
	public String getAuditType() {
		return this.auditType;
	}
	public void setAuditType(String auditType) {
		this.auditType = auditType;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssCompleteAuditPK)) {
			return false;
		}
		DssCompleteAuditPK castOther = (DssCompleteAuditPK)other;
		return 
			(this.auditRunNumber == castOther.auditRunNumber)
			&& this.auditType.equals(castOther.auditType);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.auditRunNumber ^ (this.auditRunNumber >>> 32)));
		hash = hash * prime + this.auditType.hashCode();
		
		return hash;
	}
}
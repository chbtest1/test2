package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_PROVIDER_GROUP_XREF database table.
 * 
 */
@Embeddable
public class DssProviderGroupXrefPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="PROVIDER_GROUP_ID")
	private long providerGroupId;

	@Column(name="PROVIDER_NUMBER")
	private long providerNumber;

	@Column(name="PROVIDER_TYPE")
	private String providerType;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_FROM_DATE")
	private java.util.Date effectiveFromDate;

	public DssProviderGroupXrefPK() {
	}
	public long getProviderGroupId() {
		return this.providerGroupId;
	}
	public void setProviderGroupId(long providerGroupId) {
		this.providerGroupId = providerGroupId;
	}
	public long getProviderNumber() {
		return this.providerNumber;
	}
	public void setProviderNumber(long providerNumber) {
		this.providerNumber = providerNumber;
	}
	public String getProviderType() {
		return this.providerType;
	}
	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}
	public java.util.Date getEffectiveFromDate() {
		return this.effectiveFromDate;
	}
	public void setEffectiveFromDate(java.util.Date effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssProviderGroupXrefPK)) {
			return false;
		}
		DssProviderGroupXrefPK castOther = (DssProviderGroupXrefPK)other;
		return 
			(this.providerGroupId == castOther.providerGroupId)
			&& (this.providerNumber == castOther.providerNumber)
			&& this.providerType.equals(castOther.providerType)
			&& this.effectiveFromDate.equals(castOther.effectiveFromDate);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.providerGroupId ^ (this.providerGroupId >>> 32)));
		hash = hash * prime + ((int) (this.providerNumber ^ (this.providerNumber >>> 32)));
		hash = hash * prime + this.providerType.hashCode();
		hash = hash * prime + this.effectiveFromDate.hashCode();
		
		return hash;
	}
}
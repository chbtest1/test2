package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the DSS_HEALTH_SERVICE_MODIFIERS database table.
 * 
 */
@Entity
@Table(name="DSS_HEALTH_SERVICE_MODIFIERS")
public class DssHealthServiceModifier implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DssHealthServiceModifierPK id;

	@Column(name="IMPLICIT_MODIFIER_INDICATOR")
	private String implicitModifierIndicator;

	public DssHealthServiceModifier() {
	}

	public DssHealthServiceModifierPK getId() {
		return this.id;
	}

	public void setId(DssHealthServiceModifierPK id) {
		this.id = id;
	}

	public String getImplicitModifierIndicator() {
		return this.implicitModifierIndicator;
	}

	public void setImplicitModifierIndicator(String implicitModifierIndicator) {
		this.implicitModifierIndicator = implicitModifierIndicator;
	}

}
package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DSS_PHARM_CLAIM_AUDIT database table.
 * 
 */
@Entity
@Table(name="DSS_PHARM_CLAIM_AUDIT")
public class DssPharmClaimAudit implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DssPharmClaimAuditPK id;

	@Temporal(TemporalType.DATE)
	@Column(name="AUDIT_LETTER_CREATION_DATE")
	private Date auditLetterCreationDate;

	@Column(name="AUDIT_NOTE")
	private String auditNote;

	@Column(name="CHARGEBACK_AMOUNT")
	private BigDecimal chargebackAmount;

	@Column(name="CLIENT_ID")
	private String clientId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFIED")
	private Date lastModified;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	@Column(name="NUMBER_OF_SERVICES_AFFECTED")
	private BigDecimal numberOfServicesAffected;

	@Column(name="NUMBER_OF_TIMES_SENT")
	private BigDecimal numberOfTimesSent;

	@Column(name="PROVIDER_NUMBER")
	private BigDecimal providerNumber;

	@Column(name="PROVIDER_TYPE")
	private String providerType;

	@Column(name="RESPONSE_STATUS_CODE")
	private String responseStatusCode;

	@Temporal(TemporalType.DATE)
	@Column(name="SE_SELECTION_DATE")
	private Date seSelectionDate;

	public DssPharmClaimAudit() {
	}

	public DssPharmClaimAuditPK getId() {
		return this.id;
	}

	public void setId(DssPharmClaimAuditPK id) {
		this.id = id;
	}

	public Date getAuditLetterCreationDate() {
		return this.auditLetterCreationDate;
	}

	public void setAuditLetterCreationDate(Date auditLetterCreationDate) {
		this.auditLetterCreationDate = auditLetterCreationDate;
	}

	public String getAuditNote() {
		return this.auditNote;
	}

	public void setAuditNote(String auditNote) {
		this.auditNote = auditNote;
	}

	public BigDecimal getChargebackAmount() {
		return this.chargebackAmount;
	}

	public void setChargebackAmount(BigDecimal chargebackAmount) {
		this.chargebackAmount = chargebackAmount;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public BigDecimal getNumberOfServicesAffected() {
		return this.numberOfServicesAffected;
	}

	public void setNumberOfServicesAffected(BigDecimal numberOfServicesAffected) {
		this.numberOfServicesAffected = numberOfServicesAffected;
	}

	public BigDecimal getNumberOfTimesSent() {
		return this.numberOfTimesSent;
	}

	public void setNumberOfTimesSent(BigDecimal numberOfTimesSent) {
		this.numberOfTimesSent = numberOfTimesSent;
	}

	public BigDecimal getProviderNumber() {
		return this.providerNumber;
	}

	public void setProviderNumber(BigDecimal providerNumber) {
		this.providerNumber = providerNumber;
	}

	public String getProviderType() {
		return this.providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public String getResponseStatusCode() {
		return this.responseStatusCode;
	}

	public void setResponseStatusCode(String responseStatusCode) {
		this.responseStatusCode = responseStatusCode;
	}

	public Date getSeSelectionDate() {
		return this.seSelectionDate;
	}

	public void setSeSelectionDate(Date seSelectionDate) {
		this.seSelectionDate = seSelectionDate;
	}

}
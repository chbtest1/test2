package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the DSS_PROVIDER_TYPE database table. This table does not have a primary key, do we need
 * one?
 * 
 */
@Entity
@Table(name = "DSS_PROVIDER_TYPE")
@NamedQueries({ @NamedQuery(name = QueryConstants.DSS_CODE_TABLE_SUBCATEGORY_QUERY, query = "select distinct dpt FROM  DssProviderType dpt ORDER BY dpt.providerType, dpt.providerTypeDescription ASC ") })
public class DssProviderType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "PROVIDER_TYPE")
	@Id
	private String providerType;

	@Column(name = "BILLING_INDICATOR")
	private String billingIndicator;

	@Column(name = "FOLDER_FORMAT")
	private String folderFormat;

	@Column(name = "HEALTH_WELFARE_REQUIRED")
	private String healthWelfareRequired;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	@Column(name = "PRESCRIBER_INDICATOR")
	private String prescriberIndicator;

	@Column(name = "PROVIDER_TYPE_DESCRIPTION")
	private String providerTypeDescription;

	@Column(name = "REFERRING_INDICATOR")
	private String referringIndicator;

	@Column(name = "SCREEN_FORMAT")
	private String screenFormat;

	@Column(name = "SPECIALTY_REQUIRED")
	private String specialtyRequired;

	public DssProviderType() {
	}

	public String getBillingIndicator() {
		return this.billingIndicator;
	}

	public void setBillingIndicator(String billingIndicator) {
		this.billingIndicator = billingIndicator;
	}

	public String getFolderFormat() {
		return this.folderFormat;
	}

	public void setFolderFormat(String folderFormat) {
		this.folderFormat = folderFormat;
	}

	public String getHealthWelfareRequired() {
		return this.healthWelfareRequired;
	}

	public void setHealthWelfareRequired(String healthWelfareRequired) {
		this.healthWelfareRequired = healthWelfareRequired;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getPrescriberIndicator() {
		return this.prescriberIndicator;
	}

	public void setPrescriberIndicator(String prescriberIndicator) {
		this.prescriberIndicator = prescriberIndicator;
	}

	public String getProviderType() {
		return this.providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public String getProviderTypeDescription() {
		return this.providerTypeDescription;
	}

	public void setProviderTypeDescription(String providerTypeDescription) {
		this.providerTypeDescription = providerTypeDescription;
	}

	public String getReferringIndicator() {
		return this.referringIndicator;
	}

	public void setReferringIndicator(String referringIndicator) {
		this.referringIndicator = referringIndicator;
	}

	public String getScreenFormat() {
		return this.screenFormat;
	}

	public void setScreenFormat(String screenFormat) {
		this.screenFormat = screenFormat;
	}

	public String getSpecialtyRequired() {
		return this.specialtyRequired;
	}

	public void setSpecialtyRequired(String specialtyRequired) {
		this.specialtyRequired = specialtyRequired;
	}

}
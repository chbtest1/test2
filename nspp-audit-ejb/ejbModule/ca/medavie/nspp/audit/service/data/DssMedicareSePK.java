package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_MEDICARE_SE database table.
 * 
 */
@Embeddable
public class DssMedicareSePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="SE_NUMBER")
	private String seNumber;

	@Column(name="SE_SEQUENCE_NUMBER")
	private long seSequenceNumber;

	@Column(name="RESPONSE_TAG_NUMBER")
	private long responseTagNumber;

	public DssMedicareSePK() {
	}
	public String getSeNumber() {
		return this.seNumber;
	}
	public void setSeNumber(String seNumber) {
		this.seNumber = seNumber;
	}
	public long getSeSequenceNumber() {
		return this.seSequenceNumber;
	}
	public void setSeSequenceNumber(long seSequenceNumber) {
		this.seSequenceNumber = seSequenceNumber;
	}
	public long getResponseTagNumber() {
		return this.responseTagNumber;
	}
	public void setResponseTagNumber(long responseTagNumber) {
		this.responseTagNumber = responseTagNumber;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssMedicareSePK)) {
			return false;
		}
		DssMedicareSePK castOther = (DssMedicareSePK)other;
		return 
			this.seNumber.equals(castOther.seNumber)
			&& (this.seSequenceNumber == castOther.seSequenceNumber)
			&& (this.responseTagNumber == castOther.responseTagNumber);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.seNumber.hashCode();
		hash = hash * prime + ((int) (this.seSequenceNumber ^ (this.seSequenceNumber >>> 32)));
		hash = hash * prime + ((int) (this.responseTagNumber ^ (this.responseTagNumber >>> 32)));
		
		return hash;
	}
}
package ca.medavie.nspp.audit.service.data.mappedEntity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DssDenticareALRMappedEntityPK {

	@Column
	private Long claim_num;
	@Column
	private Long item_code;
	@Column
	private Long audit_run_number;

	public Long getClaim_num() {
		return claim_num;
	}

	public void setClaim_num(Long claim_num) {
		this.claim_num = claim_num;
	}

	public Long getItem_code() {
		return item_code;
	}

	public void setItem_code(Long item_code) {
		this.item_code = item_code;
	}

	public Long getAudit_run_number() {
		return audit_run_number;
	}

	public void setAudit_run_number(Long audit_run_number) {
		this.audit_run_number = audit_run_number;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((audit_run_number == null) ? 0 : audit_run_number.hashCode());
		result = prime * result + ((claim_num == null) ? 0 : claim_num.hashCode());
		result = prime * result + ((item_code == null) ? 0 : item_code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DssDenticareALRMappedEntityPK other = (DssDenticareALRMappedEntityPK) obj;
		if (audit_run_number == null) {
			if (other.audit_run_number != null)
				return false;
		} else if (!audit_run_number.equals(other.audit_run_number))
			return false;
		if (claim_num == null) {
			if (other.claim_num != null)
				return false;
		} else if (!claim_num.equals(other.claim_num))
			return false;
		if (item_code == null) {
			if (other.item_code != null)
				return false;
		} else if (!item_code.equals(other.item_code))
			return false;
		return true;
	}

}

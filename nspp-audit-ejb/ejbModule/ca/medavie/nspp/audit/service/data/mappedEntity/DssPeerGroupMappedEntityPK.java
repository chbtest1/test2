package ca.medavie.nspp.audit.service.data.mappedEntity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DssPeerGroupMappedEntityPK implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column
	private String provider_peer_group_id;
	@Column
	private Date year_end_date;

	public String getProvider_peer_group_id() {
		return provider_peer_group_id;
	}

	public void setProvider_peer_group_id(String providerPeerGroupId) {
		this.provider_peer_group_id = providerPeerGroupId;
	}

	public Date getYear_end_date() {
		return year_end_date;
	}

	public void setYear_end_date(Date yearEndDate) {
		this.year_end_date = yearEndDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((provider_peer_group_id == null) ? 0 : provider_peer_group_id.hashCode());
		result = prime * result + ((year_end_date == null) ? 0 : year_end_date.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DssPeerGroupMappedEntityPK other = (DssPeerGroupMappedEntityPK) obj;
		if (provider_peer_group_id == null) {
			if (other.provider_peer_group_id != null)
				return false;
		} else if (!provider_peer_group_id.equals(other.provider_peer_group_id))
			return false;
		if (year_end_date == null) {
			if (other.year_end_date != null)
				return false;
		} else if (!year_end_date.equals(other.year_end_date))
			return false;
		return true;
	}

}

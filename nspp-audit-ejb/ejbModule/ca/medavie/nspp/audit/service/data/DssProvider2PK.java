package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_PROVIDER_2 database table.
 * 
 */
@Embeddable
public class DssProvider2PK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="PROVIDER_NUMBER")
	private long providerNumber;

	@Column(name="PROVIDER_TYPE")
	private String providerType;

	public DssProvider2PK() {
	}
	public long getProviderNumber() {
		return this.providerNumber;
	}
	public void setProviderNumber(long providerNumber) {
		this.providerNumber = providerNumber;
	}
	public String getProviderType() {
		return this.providerType;
	}
	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssProvider2PK)) {
			return false;
		}
		DssProvider2PK castOther = (DssProvider2PK)other;
		return 
			(this.providerNumber == castOther.providerNumber)
			&& this.providerType.equals(castOther.providerType);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.providerNumber ^ (this.providerNumber >>> 32)));
		hash = hash * prime + this.providerType.hashCode();
		
		return hash;
	}
}
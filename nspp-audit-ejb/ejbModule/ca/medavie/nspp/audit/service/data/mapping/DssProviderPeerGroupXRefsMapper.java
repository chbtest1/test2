package ca.medavie.nspp.audit.service.data.mapping;

import java.util.ArrayList;
import java.util.List;

import org.dozer.ConfigurableCustomConverter;

import ca.medavie.nspp.audit.service.data.DssHealthServicePeerXref;
import ca.medavie.nspp.audit.service.data.DssProvTypePeerXref;
import ca.medavie.nspp.audit.service.data.DssProviderPeerGroupXref;
import ca.medavie.nspp.audit.service.data.DssShadProvPeerGroupXref;
import ca.medavie.nspp.audit.service.data.DssSpecialtyPeerGroupXref;
import ca.medavie.nspp.audit.service.data.DssTownPeerGroupXref;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteria;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.SpecialtyCriteria;
import ca.medavie.nspp.audit.service.data.SubcategoryType;
import ca.medavie.nspp.audit.service.data.TownCriteria;
import ca.medavie.nspp.audit.service.repository.BaseRepository;

/**
 * 
 * Data mapper for mapping DssProviderPeerGroup to its corresponding business class of PeerGroup
 * 
 **/

@SuppressWarnings("rawtypes")
public class DssProviderPeerGroupXRefsMapper extends BaseRepository implements ConfigurableCustomConverter {

	private String param;

	@Override
	public Object convert(Object dest, Object source, Class<?> arg2, Class<?> arg3) {

		List<Object> result = new ArrayList<Object>();

		// Data converter class convert DssProvTypePeerXref TO SubcategoryType
		if (param.equals(DssProvTypePeerXref_To_SubcategoryType)) {

			for (Object s : (List) source) {
				// convert DssProvTypePeerXref to subcategory
				if (s instanceof DssProvTypePeerXref) {
					SubcategoryType target = new SubcategoryType();
					BEAN_MAPPER.map(s, target);
					result.add(target);
				}

				// Mapping in opposite way, convert from subcategory type to DssProvTypePeerXref
				if (s instanceof SubcategoryType) {
					DssProvTypePeerXref target = new DssProvTypePeerXref();
					BEAN_MAPPER.map(s, target);
					result.add(target);
				}
			}
		}

		// Data converter class convert DssSpecialtyPeerGroupXref TO SpecialtyCriteria
		if (param.equals(DssSpecialtyPeerGroupXref_To_SpecialtyCriteria)) {

			for (Object s : (List) source) {

				// convert DssSpecialtyPeerGroupXref to SpecialtyCriteria
				if (s instanceof DssSpecialtyPeerGroupXref) {
					SpecialtyCriteria target = new SpecialtyCriteria();
					BEAN_MAPPER.map(s, target);
					result.add(target);
				}

				// Mapping in opposite way, convert from SpecialtyCriteria type to DssSpecialtyPeerGroupXref
				if (s instanceof SpecialtyCriteria) {
					DssSpecialtyPeerGroupXref target = new DssSpecialtyPeerGroupXref();
					BEAN_MAPPER.map(s, target);
					result.add(target);
				}

			}
		}

		// Data converter class convert DssProviderPeerGroupXref TO Practitioner
		if (param.equals(DssProviderPeerGroupXref_To_Practitioner)) {

			for (Object s : (List) source) {

				// convert DssProviderPeerGroupXref to Practitioner
				if (s instanceof DssProviderPeerGroupXref) {
					Practitioner target = new Practitioner();
					BEAN_MAPPER.map(s, target);
					result.add(target);
				}

				// Mapping in opposite way, convert from Practitioner type to DssProviderPeerGroupXref
				if (s instanceof Practitioner) {
					DssProviderPeerGroupXref target = new DssProviderPeerGroupXref();
					BEAN_MAPPER.map(s, target);
					result.add(target);
				}
			}
		}

		// Data converter class convert (Shadow) DssShadProvPeerGroupXref TO Practitioner
		if (param.equals(DssShadProvPeerGroupXref_To_Practitioner)) {
			for (Object s : (List) source) {

				// convert DssShadProvPeerGroupXref to Practitioner
				if (s instanceof DssShadProvPeerGroupXref) {
					Practitioner target = new Practitioner();
					BEAN_MAPPER.map(s, target);
					result.add(target);
				}

				// Mapping in opposite way, convert from Practitioner to DssShadProvPeerGroupXref
				if (s instanceof Practitioner) {
					DssShadProvPeerGroupXref target = new DssShadProvPeerGroupXref();
					BEAN_MAPPER.map(s, target);
					result.add(target);
				}
			}
		}

		// Data converter DssTownPeerGroupXref TO TownCriterias
		if (param.equals(DssTownPeerGroupXref_To_TownCriterias)) {
			for (Object s : (List) source) {
				// convert DssTownPeerGroupXref to TownCriterias
				if (s instanceof DssTownPeerGroupXref) {
					TownCriteria target = new TownCriteria();
					BEAN_MAPPER.map(s, target);
					result.add(target);
				}

				// Mapping in opposite way, convert from Practitioner to DssShadProvPeerGroupXref
				if (s instanceof TownCriteria) {
					DssTownPeerGroupXref target = new DssTownPeerGroupXref();
					BEAN_MAPPER.map(s, target);
					result.add(target);
				}
			}
		}

		// Data converter DssHealthServicePeerXref_To_HealthServiceCriteria
		if (param.equals(DssHealthServicePeerXref_To_HealthServiceCriteria)) {
			for (Object s : (List) source) {
				// convert DssTownPeerGroupXref to TownCriterias
				if (s instanceof DssHealthServicePeerXref) {
					HealthServiceCriteria target = new HealthServiceCriteria();
					BEAN_MAPPER.map(s, target);
					result.add(target);
				}

				// Mapping in opposite way, convert from HealthServiceCriteria to DssHealthServicePeerXref
				if (s instanceof HealthServiceCriteria) {
					DssHealthServicePeerXref target = new DssHealthServicePeerXref();
					BEAN_MAPPER.map(s, target);
					result.add(target);
				}
			}
		}

		return result;
	}

	@Override
	public void setParameter(String arg0) {

		this.param = arg0;

	}

}

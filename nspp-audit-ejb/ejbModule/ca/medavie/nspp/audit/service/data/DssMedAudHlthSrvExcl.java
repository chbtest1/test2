package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the DSS_MED_AUD_HLTH_SRV_EXCL database table.
 * 
 */
@Entity
@Table(name = "DSS_MED_AUD_HLTH_SRV_EXCL")
public class DssMedAudHlthSrvExcl implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "UNIQUE_NUMBER")
	private long uniqueNumber;

	@Column(name = "AGE_RESTRICTION")
	private BigDecimal ageRestriction;

	@Column(name = "GENDER_RESTRICTION")
	private String genderRestriction;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	public DssMedAudHlthSrvExcl() {
	}

	public long getUniqueNumber() {
		return this.uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public BigDecimal getAgeRestriction() {
		return this.ageRestriction;
	}

	public void setAgeRestriction(BigDecimal ageRestriction) {
		this.ageRestriction = ageRestriction;
	}

	public String getGenderRestriction() {
		return this.genderRestriction;
	}

	public void setGenderRestriction(String genderRestriction) {
		this.genderRestriction = genderRestriction;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

}
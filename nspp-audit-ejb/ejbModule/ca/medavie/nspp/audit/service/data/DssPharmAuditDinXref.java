package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the DSS_PHARM_AUDIT_DIN_XREF database table.
 * 
 */
@Entity
@Table(name = "DSS_PHARM_AUDIT_DIN_XREF")
public class DssPharmAuditDinXref implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DssPharmAuditDinXrefPK id;

	@Column(name = "AGE_RESTRICTION")
	private BigDecimal ageRestriction;

	@Column(name = "GENDER_RESTRICTION")
	private String genderRestriction;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	// bi-directional many-to-one association to DssPharmAuditCriteria
	@ManyToOne
	@JoinColumn(name = "AUDIT_CRITERIA_ID")
	private DssPharmAuditCriteria dssPharmAuditCriteria;

	// bi-directional one-to-one association to DssDin
	@OneToOne
	@JoinColumn(name = "DIN")
	private DssDin dssDin;

	public DssPharmAuditDinXref() {
	}

	public DssPharmAuditDinXrefPK getId() {
		return this.id;
	}

	public void setId(DssPharmAuditDinXrefPK id) {
		this.id = id;
	}

	public BigDecimal getAgeRestriction() {
		return this.ageRestriction;
	}

	public void setAgeRestriction(BigDecimal ageRestriction) {
		this.ageRestriction = ageRestriction;
	}

	public String getGenderRestriction() {
		return this.genderRestriction;
	}

	public void setGenderRestriction(String genderRestriction) {
		this.genderRestriction = genderRestriction;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public DssPharmAuditCriteria getDssPharmAuditCriteria() {
		return this.dssPharmAuditCriteria;
	}

	public void setDssPharmAuditCriteria(DssPharmAuditCriteria dssPharmAuditCriteria) {
		this.dssPharmAuditCriteria = dssPharmAuditCriteria;
	}

	public DssDin getDssDin() {
		return this.dssDin;
	}

	public void setDssDin(DssDin dssDin) {
		this.dssDin = dssDin;
	}

}
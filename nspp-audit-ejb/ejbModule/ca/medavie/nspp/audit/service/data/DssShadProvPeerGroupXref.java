package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the DSS_SHAD_PROV_PEER_GROUP_XREF database table.
 * AKA Shadow Practitioners
 */
@Entity
@Table(name = "DSS_SHAD_PROV_PEER_GROUP_XREF")
public class DssShadProvPeerGroupXref implements Serializable {
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "PROVIDER_PEER_GROUP_ID", referencedColumnName = "PROVIDER_PEER_GROUP_ID"),
			@JoinColumn(name = "YEAR_END_DATE", referencedColumnName = "YEAR_END_DATE") })
	private DssProviderPeerGroup dssProviderPeerGroup4;

	@OneToOne(mappedBy = "dsppgx")
	private DssProvider2 dp2;

	@EmbeddedId
	private DssShadProvPeerGroupXrefPK id;

	@Column(name = "DROP_INDICATOR")
	private String dropIndicator;

	@Column(name = "FORCED_INDICATOR")
	private String forcedIndicator;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	public DssShadProvPeerGroupXref() {
	}

	public DssProviderPeerGroup getDssProviderPeerGroup4() {
		return dssProviderPeerGroup4;
	}

	public void setDssProviderPeerGroup4(DssProviderPeerGroup dppg4) {
		this.dssProviderPeerGroup4 = dppg4;
	}

	public DssProvider2 getDp2() {
		return dp2;
	}

	public void setDp2(DssProvider2 dp2) {
		this.dp2 = dp2;
	}

	public DssShadProvPeerGroupXrefPK getId() {
		return this.id;
	}

	public void setId(DssShadProvPeerGroupXrefPK id) {
		this.id = id;
	}

	public String getDropIndicator() {
		return this.dropIndicator;
	}

	public void setDropIndicator(String dropIndicator) {
		this.dropIndicator = dropIndicator;
	}

	public String getForcedIndicator() {
		return this.forcedIndicator;
	}

	public void setForcedIndicator(String forcedIndicator) {
		this.forcedIndicator = forcedIndicator;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

}
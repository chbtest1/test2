package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the DSS_PROFILE_PERIOD database table.
 * 
 */
@Entity
@Table(name="DSS_PROFILE_PERIOD")
public class DssProfilePeriod implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="DSS_PROFILE_PERIOD_ID_SEQ",sequenceName="DSS_PROFILE_PERIOD_ID_SEQ" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DSS_PROFILE_PERIOD_ID_SEQ")
	@Column(name="PERIOD_ID")
	private long periodId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFIED")
	private Date lastModified;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="PERIOD_END_DATE")
	private Date periodEndDate;

	@Temporal(TemporalType.DATE)
	@Column(name="PERIOD_START_DATE")
	private Date periodStartDate;

	@Column(name="PERIOD_TYPE")
	private String periodType;

	@Temporal(TemporalType.DATE)
	@Column(name="YEAR_END_DATE")
	private Date yearEndDate;

	public DssProfilePeriod() {
	}

	public long getPeriodId() {
		return this.periodId;
	}

	public void setPeriodId(long periodId) {
		this.periodId = periodId;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getPeriodEndDate() {
		return this.periodEndDate;
	}

	public void setPeriodEndDate(Date periodEndDate) {
		this.periodEndDate = periodEndDate;
	}

	public Date getPeriodStartDate() {
		return this.periodStartDate;
	}

	public void setPeriodStartDate(Date periodStartDate) {
		this.periodStartDate = periodStartDate;
	}

	public String getPeriodType() {
		return this.periodType;
	}

	public void setPeriodType(String periodType) {
		this.periodType = periodType;
	}

	public Date getYearEndDate() {
		return this.yearEndDate;
	}

	public void setYearEndDate(Date yearEndDate) {
		this.yearEndDate = yearEndDate;
	}

}
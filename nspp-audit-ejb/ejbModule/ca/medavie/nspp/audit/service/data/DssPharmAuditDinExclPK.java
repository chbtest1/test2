package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_PHARM_AUDIT_DIN_Excl database table.
 * 
 */
@Embeddable
public class DssPharmAuditDinExclPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "DIN")
	private String din;

	public DssPharmAuditDinExclPK() {
	}
	public String getDin() {
		return this.din;
	}
	public void setDin(String din) {
		this.din = din;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssPharmAuditDinExclPK)) {
			return false;
		}
		DssPharmAuditDinExclPK castOther = (DssPharmAuditDinExclPK)other;
		return this.din.equals(castOther.din);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.din.hashCode();
		return hash;
	}
}
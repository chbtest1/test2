package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ca.medavie.nspp.audit.service.data.ChequeReconciliation;
import ca.medavie.nspp.audit.service.data.ChequeReconciliationSearchCriteria;
import ca.medavie.nspp.audit.service.exception.DssChequeReconciliationException;
import ca.medavie.nspp.audit.service.repository.DssChequeReconciliationRepository;
import ca.medavie.nspp.audit.service.repository.DssChequeReconciliationRepositoryImpl;

@Stateless
@Local(DssChequeReconciliationService.class)
@LocalBean
public class DssChequeReconciliationServiceImpl implements DssChequeReconciliationService {

	@PersistenceContext(name = "nspp-audit-ejb")
	private EntityManager em;

	private DssChequeReconciliationRepository dssChequeReconciliationRepository;

	@PostConstruct
	public void doInitRepo() {
		dssChequeReconciliationRepository = new DssChequeReconciliationRepositoryImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.DssChequeReconciliationService#getChequeReconciliationSearchResults(ca.medavie.
	 * nspp.audit.service.data.ChequeReconciliationSearchCriteria, int, int)
	 */
	@Override
	public List<ChequeReconciliation> getChequeReconciliationSearchResults(
			ChequeReconciliationSearchCriteria aChequeReconciliationSearchCriteria, int startRow, int maxRows)
			throws DssChequeReconciliationException {
		try {
			return dssChequeReconciliationRepository.getChequeReconciliationSearchResults(
					aChequeReconciliationSearchCriteria, startRow, maxRows);
		} catch (Exception e) {
			throw new DssChequeReconciliationException(e.getMessage(), e);
		}
	}

	@Override
	public Integer getChequeReconciliationSearchResultsCount(
			ChequeReconciliationSearchCriteria aChequeReconciliationSearchCriteria)
			throws DssChequeReconciliationException {
		try {
			return dssChequeReconciliationRepository
					.getChequeReconciliationSearchResultsCount(aChequeReconciliationSearchCriteria);
		} catch (Exception e) {
			throw new DssChequeReconciliationException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.DssChequeReconciliationService#saveChequeReconciliation(ca.medavie.nspp.audit.service
	 * .data.ChequeReconciliation)
	 */
	@Override
	public void saveChequeReconciliation(ChequeReconciliation aChequeReconciliation)
			throws DssChequeReconciliationException {
		try {
			dssChequeReconciliationRepository.saveChequeReconciliation(aChequeReconciliation);
		} catch (Exception e) {
			throw new DssChequeReconciliationException(e.getMessage(), e);
		}
	}
}

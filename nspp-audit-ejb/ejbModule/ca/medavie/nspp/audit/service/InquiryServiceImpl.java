package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ca.medavie.nspp.audit.service.data.HealthServiceCode;
import ca.medavie.nspp.audit.service.data.PeerGroup;
import ca.medavie.nspp.audit.service.data.PeerGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.exception.InquiryServiceException;
import ca.medavie.nspp.audit.service.repository.InquiryRepository;
import ca.medavie.nspp.audit.service.repository.InquiryRepositoryImpl;

/**
 * Session Bean implementation class InquiryServiceImpl
 */
@Stateless
@Local(InquiryService.class)
@LocalBean
public class InquiryServiceImpl implements InquiryService {

	@PersistenceContext(name = "nspp-audit-ejb")
	private EntityManager em;

	private InquiryRepository inquiryRepository;

	@PostConstruct
	public void doInitRepo() {
		inquiryRepository = new InquiryRepositoryImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.InquiryService#getSearchedPractitioners(ca.medavie.nspp.audit.service.data.
	 * PractitionerSearchCriteria, int, int)
	 */
	@Override
	public List<Practitioner> getSearchedPractitioners(PractitionerSearchCriteria aPractitionerSearchCriteria,
			int startRow, int maxRows) throws InquiryServiceException {
		try {
			return inquiryRepository.getSearchedPractitioners(aPractitionerSearchCriteria, startRow, maxRows);
		} catch (Exception e) {
			throw new InquiryServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.InquiryService#getSearchedPractitionersCount(ca.medavie.nspp.audit.service.data
	 * .PractitionerSearchCriteria)
	 */
	@Override
	public Integer getSearchedPractitionersCount(PractitionerSearchCriteria aPractitionerSearchCriteria)
			throws InquiryServiceException {
		try {
			return inquiryRepository.getSearchedPractitionersCount(aPractitionerSearchCriteria);
		} catch (Exception e) {
			throw new InquiryServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.InquiryService#getPractitionerDetails(ca.medavie.nspp.audit.service.data.Practitioner
	 * )
	 */
	@Override
	public Practitioner getPractitionerDetails(Practitioner aPractitioner) throws InquiryServiceException {
		try {
			return inquiryRepository.getPractitionerDetails(aPractitioner);
		} catch (Exception e) {
			throw new InquiryServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.InquiryService#getSearchPeerGroups(ca.medavie.nspp.audit.service.data.
	 * PeerGroupSearchCriteria, int, int)
	 */
	@Override
	public List<PeerGroup> getSearchPeerGroups(PeerGroupSearchCriteria aPeerGroupSearchCriteria, int startRow,
			int maxRows) throws InquiryServiceException {
		try {
			return inquiryRepository.getSearchPeerGroups(aPeerGroupSearchCriteria, startRow, maxRows);
		} catch (Exception e) {
			throw new InquiryServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.InquiryService#getSearchPeerGroupsCount(ca.medavie.nspp.audit.service.data.
	 * PeerGroupSearchCriteria)
	 */
	@Override
	public Integer getSearchPeerGroupsCount(PeerGroupSearchCriteria aPeerGroupSearchCriteria)
			throws InquiryServiceException {
		try {
			return inquiryRepository.getSearchPeerGroupsCount(aPeerGroupSearchCriteria);
		} catch (Exception e) {
			throw new InquiryServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.InquiryService#getPeerGroupDetails(ca.medavie.nspp.audit.service.data.PeerGroup)
	 */
	@Override
	public PeerGroup getPeerGroupDetails(PeerGroup aPeerGroup) throws InquiryServiceException {
		try {
			return inquiryRepository.getPeerGroupDetails(aPeerGroup);
		} catch (Exception e) {
			throw new InquiryServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.InquiryService#getHealthServiceDrillDownDetails(ca.medavie.nspp.audit.service.data
	 * .Practitioner, ca.medavie.nspp.audit.service.data.HealthServiceCode)
	 */
	@Override
	public List<HealthServiceCode> getHealthServiceDrillDownDetails(Practitioner aPractitioner,
			HealthServiceCode aHealthServiceCode) throws InquiryServiceException {
		try {
			return inquiryRepository.getHealthServiceDrillDownDetails(aPractitioner, aHealthServiceCode);
		} catch (Exception e) {
			throw new InquiryServiceException(e.getMessage(), e);
		}
	}
}

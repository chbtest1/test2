package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ca.medavie.nspp.audit.service.data.Locum;
import ca.medavie.nspp.audit.service.data.LocumSearchCriteria;
import ca.medavie.nspp.audit.service.exception.LocumServiceException;
import ca.medavie.nspp.audit.service.repository.LocumRepository;
import ca.medavie.nspp.audit.service.repository.LocumRepositoryImpl;

/**
 * Session Bean implementation class LocumServiceImpl
 */
@Stateless
@Local(LocumService.class)
@LocalBean
public class LocumServiceImpl implements LocumService {

	@PersistenceContext(name = "nspp-audit-ejb")
	private EntityManager em;

	private LocumRepository locumRepository;

	@PostConstruct
	public void doInitRepo() {
		locumRepository = new LocumRepositoryImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.LocumService#getSearchedLocums(ca.medavie.nspp.audit.service.data.LocumSearchCriteria
	 * , int, int)
	 */
	@Override
	public List<Locum> getSearchedLocums(LocumSearchCriteria aLocumSearchCriteria, int firstRow, int maxRows)
			throws LocumServiceException {
		try {
			return locumRepository.getSearchedLocums(aLocumSearchCriteria, firstRow, maxRows);
		} catch (Exception e) {
			throw new LocumServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.LocumService#getSearchedLocumsCount(ca.medavie.nspp.audit.service.data.
	 * LocumSearchCriteria)
	 */
	@Override
	public Integer getSearchedLocumsCount(LocumSearchCriteria aLocumSearchCriteria) throws LocumServiceException {
		try {
			return locumRepository.getSearchedLocumsCount(aLocumSearchCriteria);
		} catch (Exception e) {
			throw new LocumServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.LocumService#getSearchedLocumPractitioners(ca.medavie.nspp.audit.service.data.
	 * LocumSearchCriteria)
	 */
	@Override
	public List<Locum> getSearchedLocumPractitioners(LocumSearchCriteria aLocumSearchCriteria)
			throws LocumServiceException {

		try {
			return locumRepository.getSearchedLocumPractitioners(aLocumSearchCriteria);
		} catch (Exception e) {
			throw new LocumServiceException(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.LocumService#getLocumDetails(ca.medavie.nspp.audit.service.data.Locum)
	 */
	@Override
	public Locum getLocumDetails(Locum aLocum) throws LocumServiceException {

		try {
			return locumRepository.getLocumDetails(aLocum);
		} catch (Exception e) {
			throw new LocumServiceException(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.LocumService#saveLocum(ca.medavie.nspp.audit.service.data.Locum)
	 */
	@Override
	public void saveLocum(Locum aLocum) throws LocumServiceException {

		try {
			locumRepository.saveLocum(aLocum);
		} catch (Exception e) {
			throw new LocumServiceException(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.LocumService#deleteLocums(java.util.List)
	 */
	@Override
	public void deleteLocums(List<Locum> aListOfLocumsForDelete) throws LocumServiceException {
		try {
			locumRepository.deleteLocums(aListOfLocumsForDelete);
		} catch (Exception e) {
			throw new LocumServiceException(e.getLocalizedMessage(), e);
		}
	}
}

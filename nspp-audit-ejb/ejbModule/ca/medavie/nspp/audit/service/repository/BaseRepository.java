package ca.medavie.nspp.audit.service.repository;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;

public abstract class BaseRepository {

	protected static final String DssProvTypePeerXref_To_SubcategoryType = "DssProvTypePeerXref_To_SubcategoryType";
	protected static final String DssSpecialtyPeerGroupXref_To_SpecialtyCriteria = "DssSpecialtyPeerGroupXref_To_SpecialtyCriteria";
	protected static final String DssProviderPeerGroupXref_To_Practitioner = "DssProviderPeerGroupXref_To_Practitioner";
	protected static final String DssShadProvPeerGroupXref_To_Practitioner = "DssShadProvPeerGroupXref_To_Practitioner";
	protected static final String DssTownPeerGroupXref_To_TownCriterias = "DssTownPeerGroupXref_To_TownCriterias";
	protected static final String DssHealthServicePeerXref_To_HealthServiceCriteria = "DssHealthServicePeerXref_To_HealthServiceCriteria";

	protected static final String AuditResults = "AuditResults";
	protected static final String AuditResultsNote = "AuditResultsNote";
	protected static final String AuditResultsNoteID = "AuditResultsNoteID";
	protected static final String AuditResultsAttachment = "AuditResultsAttachment";

	protected static final DozerBeanMapper BEAN_MAPPER = new DozerBeanMapper();
	private static final String MAPPING_FILE_1 = "/ca/medavie/nspp/audit/service/data/mapping/PeerGroup-Mapping.xml";
	private static final String MAPPING_FILE_2 = "/ca/medavie/nspp/audit/service/data/mapping/AuditResult-Mapping.xml";
	private static final String MAPPING_FILE_3 = "/ca/medavie/nspp/audit/service/data/mapping/ManualDeposit-Mapping.xml";
	private static final String MAPPING_FILE_4 = "/ca/medavie/nspp/audit/service/data/mapping/ProviderGroup-Mapping.xml";
	private static final String MAPPING_FILE_5 = "/ca/medavie/nspp/audit/service/data/mapping/DssUnitValue-Mapping.xml";
	private static final String MAPPING_FILE_6 = "/ca/medavie/nspp/audit/service/data/mapping/Medicare-Mapping.xml";
	private static final String MAPPING_FILE_7 = "/ca/medavie/nspp/audit/service/data/mapping/Denticare-Mapping.xml";
	private static final String MAPPING_FILE_8 = "/ca/medavie/nspp/audit/service/data/mapping/Pharmacare-Mapping.xml";
	private static final String MAPPING_FILE_9 = "/ca/medavie/nspp/audit/service/data/mapping/DssClaimsHistoryTransfer-Mapping.xml";
	private static final String MAPPING_FILE_10 = "/ca/medavie/nspp/audit/service/data/mapping/DistrictHealthAuthority-Mapping.xml";
	private static final String MAPPING_FILE_11 = "/ca/medavie/nspp/audit/service/data/mapping/Inquiry-Mapping.xml";
	private static final String MAPPING_FILE_12 = "/ca/medavie/nspp/audit/service/data/mapping/DssGLNumbers-Mapping.xml";
	private static final String MAPPING_FILE_13 = "/ca/medavie/nspp/audit/service/data/mapping/DssChequeReconciliation-Mapping.xml";

	// FOR JUNITS --- Un-comment the following directories.. updated path if needed based on your PC drives */
//	private static final String MAPPING_FILE_1 = "file:E:/Development/NSPP/Maint/trax-test/src/ca/medavie/nspp/audit/test/mappings/PeerGroup-Mapping.xml";
//	private static final String MAPPING_FILE_2 = "file:E:/Development/NSPP/Maint/trax-test/src/ca/medavie/nspp/audit/test/mappings/AuditResult-Mapping.xml";
//	private static final String MAPPING_FILE_3 = "file:E:/Development/NSPP/Maint/trax-test/src/ca/medavie/nspp/audit/test/mappings/ManualDeposit-Mapping.xml";
//	private static final String MAPPING_FILE_4 = "file:E:/Development/NSPP/Maint/trax-test/src/ca/medavie/nspp/audit/test/mappings/ProviderGroup-Mapping.xml";
//	private static final String MAPPING_FILE_5 = "file:E:/Development/NSPP/Maint/trax-test/src/ca/medavie/nspp/audit/test/mappings/DssUnitValue-Mapping.xml";
//	private static final String MAPPING_FILE_6 = "file:E:/Development/NSPP/Maint/trax-test/src/ca/medavie/nspp/audit/test/mappings/Medicare-Mapping.xml";
//	private static final String MAPPING_FILE_7 = "file:E:/Development/NSPP/Maint/trax-test/src/ca/medavie/nspp/audit/test/mappings/Denticare-Mapping.xml";
//	private static final String MAPPING_FILE_8 = "file:E:/Development/NSPP/Maint/trax-test/src/ca/medavie/nspp/audit/test/mappings/Pharmacare-Mapping.xml";
//	private static final String MAPPING_FILE_9 = "file:E:/Development/NSPP/Maint/trax-test/src/ca/medavie/nspp/audit/test/mappings/DssClaimsHistoryTransfer-Mapping.xml";
//	private static final String MAPPING_FILE_10 = "file:E:/Development/NSPP/Maint/trax-test/src/ca/medavie/nspp/audit/test/mappings/DistrictHealthAuthority-Mapping.xml";
//	private static final String MAPPING_FILE_11 = "file:E:/Development/NSPP/Maint/trax-test/src/ca/medavie/nspp/audit/test/mappings/Inquiry-Mapping.xml";
//	private static final String MAPPING_FILE_12 = "file:E:/Development/NSPP/Maint/trax-test/src/ca/medavie/nspp/audit/test/mappings/DssGLNumbers-Mapping.xml";
//	private static final String MAPPING_FILE_13 = "file:E:/Development/NSPP/Maint/trax-test/src/ca/medavie/nspp/audit/test/mappings/DssChequeReconciliation-Mapping.xml";

	static {
		List<String> mappingFiles = new ArrayList<String>();
		mappingFiles.add(MAPPING_FILE_1);
		mappingFiles.add(MAPPING_FILE_2);
		mappingFiles.add(MAPPING_FILE_3);
		mappingFiles.add(MAPPING_FILE_4);
		mappingFiles.add(MAPPING_FILE_5);
		mappingFiles.add(MAPPING_FILE_6);
		mappingFiles.add(MAPPING_FILE_7);
		mappingFiles.add(MAPPING_FILE_8);
		mappingFiles.add(MAPPING_FILE_9);
		mappingFiles.add(MAPPING_FILE_10);
		mappingFiles.add(MAPPING_FILE_11);
		mappingFiles.add(MAPPING_FILE_12);
		mappingFiles.add(MAPPING_FILE_13);
		BEAN_MAPPER.setMappingFiles(mappingFiles);
	}
}

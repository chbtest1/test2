package ca.medavie.nspp.audit.service.repository;

import java.util.List;

import ca.medavie.nspp.audit.service.data.DssMsiConstant;
import ca.medavie.nspp.audit.service.data.OutlierCriteria;
import ca.medavie.nspp.audit.service.data.PractitionerProfilePeriod;

/**
 * Interface of the OutlierCriteria repository layer
 */
public interface OutlierCriteriaRepository {

	/** Constant used for when no specific subsystem is required */
	public static final String NO_SUBSYSTEM = "";
	/** Subsystem constant for PractitionerProfileCriteria */
	public static final String PRACTITIONER_CRITERIA_SUBSYSTEM = "DSSPPROF";
	/** Subsystem constant for OutlierCriteria */
	public static final String OUTLIER_CRITERIA_SUBSYSTEM = "DSSOUTLIER";

	/**
	 * 
	 * get the list of OutLierCriteriaBO
	 * 
	 * @return list of OutLierCriteriaBO
	 */
	public List<OutlierCriteria> getOutlierCriterias();

	/**
	 * 
	 * get the list of PractitionerPeriod
	 * 
	 * @return list of PractitionerPeriod
	 */
	public List<PractitionerProfilePeriod> getPractitionerPeriods();

	/**
	 * Gets the details for the PractitionerProfileCriteria
	 * 
	 * @return up to date PracitionerProfileCriteria object
	 */
	public List<DssMsiConstant> getPractitionerProfileCriteria();

	/**
	 * Saves changes made to the PractitionerProfileCriteria record
	 * 
	 * @param aProfileCriteria
	 */
	public void savePractitionerProfileCriteria(DssMsiConstant aProfileCriteriaConstant);

	/**
	 * Save changes made to an OutlierCriteria record
	 * 
	 * @param anOutlierCriteria
	 *            - OutlierCriteria to be saved
	 */
	public void saveOutlierCriteria(OutlierCriteria anOutlierCriteria);

	/**
	 * 
	 * save PracitionerProfilePeriod to the data base
	 * 
	 * @param practitionerPeriod
	 *            a PracitionerProfilePeriod to save
	 * @return An updated PractitionerProfilePeriod
	 */
	public PractitionerProfilePeriod addNewPracitionerProfilePeriod(PractitionerProfilePeriod practitionerPeriod);

}

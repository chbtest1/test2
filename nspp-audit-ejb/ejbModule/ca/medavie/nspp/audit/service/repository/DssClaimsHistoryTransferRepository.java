package ca.medavie.nspp.audit.service.repository;

import java.util.List;

import ca.medavie.nspp.audit.service.data.DenticareClaim;
import ca.medavie.nspp.audit.service.data.DssClaimsHistoryTransferRequest;
import ca.medavie.nspp.audit.service.data.DssClaimsHistoryTransferSummary;
import ca.medavie.nspp.audit.service.data.HealthCard;
import ca.medavie.nspp.audit.service.data.MedicareClaim;
import ca.medavie.nspp.audit.service.data.PharmacareClaim;

/**
 * Interface of the DssClaimsHistoryTransfer repository layer
 */
public interface DssClaimsHistoryTransferRepository {
	/**
	 * Attempts to load the matching HealthCard record using the user specified search criteria
	 * 
	 * @param aHealthCardNumber
	 * @return matching HealthCard
	 */
	public HealthCard getSearchedHealthCard(String aHealthCardNumber);

	/**
	 * @param aTransferRequest
	 * @return list of Medicare SE Claims found under the specified HCN and dates if provided in initial search
	 */
	public List<MedicareClaim> getMedicareSeClaims(DssClaimsHistoryTransferRequest aTransferRequest);

	/**
	 * @param aTransferRequest
	 * @return list of Medicare Mainframe Claims found under the specified HCN and dates if provided in initial search
	 */
	public List<MedicareClaim> getMedicareMainframeClaims(DssClaimsHistoryTransferRequest aTransferRequest);

	/**
	 * @param aTransferRequest
	 * @return list of Pharmacare SE Claims found under the specified HCN and dates if provided in initial search
	 */
	public List<PharmacareClaim> getPharmacareSeClaims(DssClaimsHistoryTransferRequest aTransferRequest);

	/**
	 * @param aTransferRequest
	 * @return list of Pharmacare Mainframe Claims found under the specified HCN and dates if provided in initial search
	 */
	public List<PharmacareClaim> getPharmacareMainframeClaims(DssClaimsHistoryTransferRequest aTransferRequest);

	/**
	 * @param aTransferRequest
	 * @return list of Dental Claims found under the specified HCN and dates if provided in initial search
	 */
	public List<DenticareClaim> getDentalClaims(DssClaimsHistoryTransferRequest aTransferRequest);

	/**
	 * Loads the required data for the History Transfer to the staging table
	 * 
	 * @param aTransferRequest
	 */
	public void saveClaimsHistoryTransferStagingData(DssClaimsHistoryTransferRequest aTransferRequest);

	/**
	 * Triggers the stored procedure 'P_EXE_DSS_HISTORY_TRANSFER' to copy the claims data
	 * 
	 * @param aSourceHcn
	 * @param aTargetHcn
	 */
	public void executeClaimsHistoryTransferProcedure(String aSourceHcn, String aTargetHcn);

	/**
	 * Loads the summary details for the transfer made between the 2 specified Health Card Numbers
	 * 
	 * @param aSourceHcn
	 * @param aTargetHcn
	 * @return transfer summary
	 */
	public DssClaimsHistoryTransferSummary getClaimsHistoryTransferSummary(String aSourceHcn, String aTargetHcn);
}

package ca.medavie.nspp.audit.service.repository;

import java.util.List;

import ca.medavie.nspp.audit.service.data.DssUnitValueBO;

public interface DssUnitValueRepository {

	/**
	 * @return list of DssUnitValueBO
	 */
	public List<DssUnitValueBO> getDssUnitValues();

	/**
	 * @param aNewDssUnitValue
	 * @return
	 */
	public DssUnitValueBO saveOrUpdateDssUnitValue(DssUnitValueBO aNewDssUnitValue);

	/**
	 * Deletes the specified Dss Unit Values from the system.
	 * 
	 * @param aListOfDssUnitValuesForDelete
	 */
	public void deleteDssUnitValues(List<DssUnitValueBO> aListOfDssUnitValuesForDelete);

	/**
	 * @param aDssUnitValue
	 * @return
	 */
	public Boolean overlaps(DssUnitValueBO aDssUnitValue);

}

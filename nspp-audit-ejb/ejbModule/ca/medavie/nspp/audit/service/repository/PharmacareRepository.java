package ca.medavie.nspp.audit.service.repository;

import java.util.List;

import ca.medavie.nspp.audit.service.data.AuditCriteria;
import ca.medavie.nspp.audit.service.data.AuditCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.DIN;
import ca.medavie.nspp.audit.service.data.DinSearchCriteria;
import ca.medavie.nspp.audit.service.data.IndividualExclusion;
import ca.medavie.nspp.audit.service.data.PharmAuditDinExcl;
import ca.medavie.nspp.audit.service.data.PharmacareAuditLetterResponse;
import ca.medavie.nspp.audit.service.data.PharmacareAuditLetterResponseSearchCriteria;

public interface PharmacareRepository {

	/**
	 * @param dentalCareALRSearchCriteria
	 * @param startRow
	 * @param maxRows
	 * @return list of PharmCareAuditLetterRespondse
	 */
	public List<PharmacareAuditLetterResponse> getSearchedPharmacareALR(
			PharmacareAuditLetterResponseSearchCriteria dentalCareALRSearchCriteria, int startRow, int maxRows);

	/**
	 * Counts all rows that return by an ALR search
	 * 
	 * @param pharmCareALRSearchCriteria
	 * @return - total row count for entire search dataset
	 */
	public Integer getSearchedPharmacareALRCount(PharmacareAuditLetterResponseSearchCriteria pharmCareALRSearchCriteria);

	/**
	 * save dental care audit letter responds
	 * 
	 * @param selectedDCALR
	 * 
	 */
	public void savePharmacareALR(PharmacareAuditLetterResponse selectedDCALR);

	/**
	 * Loads any matching AuditCriteria records using the specified Search Criteria
	 * 
	 * @param aSearchCriteria
	 * @param startRow
	 * @param maxRows
	 * @return any matching AuditCriteria records
	 */
	public List<AuditCriteria> getSearchPractitionerAuditCriterias(AuditCriteriaSearchCriteria aSearchCriteria,
			int startRow, int maxRows);

	/**
	 * Counts all rows that return by an Audit Criteria search
	 * 
	 * @param anAuditCriteriaSearchCriteria
	 * @return - total row count for entire search dataset
	 */
	public Integer getSearchedAuditCriteriaCount(AuditCriteriaSearchCriteria anAuditCriteriaSearchCriteria);

	/**
	 * Attempts to load a Default AuditCriteria by the specified AuditType
	 * 
	 * @param anAuditType
	 * @return the matching audit if it exists or a brand new AuditCriteria object if nothing is found
	 */
	public AuditCriteria getDefaultAuditCriteriaByType(String anAuditType);

	/**
	 * Attempts to load AuditCriteria record by ID
	 * 
	 * @param anId
	 *            - ID of the desired AuditCriteria
	 * @return AuditCriteria with the specified ID
	 */
	public AuditCriteria getPractitionerAuditCriteriaById(Long anId);

	/**
	 * Saves the specified AuditCriteria record
	 * 
	 * @param anAuditCriteria
	 */
	public void savePharmacareAuditCriteria(AuditCriteria anAuditCriteria);

	/**
	 * Loads any matching DINs using the search criteria
	 * 
	 * @param aSearchCriteria
	 * @return list of DIN records that matched the search criteria
	 */
	public List<DIN> getSearchDins(DinSearchCriteria aSearchCriteria);

	/**
	 * Saves the new and updated Individual Exclusions to the database
	 * 
	 * @param aListOfIndExclToSave
	 */
	public void saveIndividualExclusions(List<IndividualExclusion> aListOfIndExclToSave);

	/**
	 * Load the current Individual Exclusions
	 * 
	 * @return current Individual Exclusions
	 */
	public List<IndividualExclusion> getIndividualExclusions();

	/**
	 * Load the current Din Inclusions
	 * @return
	 */
	public List<PharmAuditDinExcl> getDinExclusions();

	/**
	 * @param pharmAuditDinExclResults
	 */
	public void saveDinExclusions(
			List<PharmAuditDinExcl> pharmAuditDinExclResults);

	public void deleteDinExcl(PharmAuditDinExcl pharmAuditDinExcl);
}

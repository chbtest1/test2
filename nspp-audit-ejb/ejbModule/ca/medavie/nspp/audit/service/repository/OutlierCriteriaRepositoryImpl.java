package ca.medavie.nspp.audit.service.repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import ca.medavie.nspp.audit.service.dao.OutlierCriteriaDAO;
import ca.medavie.nspp.audit.service.dao.OutlierCriteriaDAOImpl;
import ca.medavie.nspp.audit.service.data.DssMsiConstant;
import ca.medavie.nspp.audit.service.data.DssProfilePeriod;
import ca.medavie.nspp.audit.service.data.OutlierCriteria;
import ca.medavie.nspp.audit.service.data.OutlierCriteria.Types;
import ca.medavie.nspp.audit.service.data.PractitionerProfilePeriod;

/**
 * Implementation of the OutlierCriteria repository layer
 */
public class OutlierCriteriaRepositoryImpl implements OutlierCriteriaRepository {

	private OutlierCriteriaDAO outlierCriteriaDAO;

	/**
	 * Constructor taking an EntityManager
	 * 
	 * @param em
	 */
	public OutlierCriteriaRepositoryImpl(EntityManager em) {

		outlierCriteriaDAO = new OutlierCriteriaDAOImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.OutlierCriteriaRepository#getOutlierCriterias()
	 */
	@Override
	public List<OutlierCriteria> getOutlierCriterias() {

		List<OutlierCriteria> outlierCriterias = new ArrayList<OutlierCriteria>();

		List<DssMsiConstant> bos = outlierCriteriaDAO.getDssMsiConstants(OUTLIER_CRITERIA_SUBSYSTEM);

		for (DssMsiConstant dmc : bos) {

			OutlierCriteria oc = new OutlierCriteria();
			oc.setSubsystem(dmc.getId().getSubsystem());
			oc.setCode(dmc.getId().getConstantCode());
			oc.setType(getConstantType(dmc.getConstantType()));
			oc.setDescription(dmc.getConstantDesc());
			oc.setModifiedBy(dmc.getModifiedBy());
			oc.setLastModified(dmc.getLastModified());

			oc.setValue(getTypeValue(dmc.getConstantType(), dmc));

			outlierCriterias.add(oc);
		}

		return outlierCriterias;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.OutlierCriteriaRepository#getPractitionerPeriods(javax.persistence.
	 * EntityManager )
	 */
	@Override
	public List<PractitionerProfilePeriod> getPractitionerPeriods() {

		List<PractitionerProfilePeriod> ppps = new ArrayList<PractitionerProfilePeriod>();
		List<DssProfilePeriod> dpps = outlierCriteriaDAO.getDssProfilePeriods();

		for (DssProfilePeriod dpp : dpps) {

			PractitionerProfilePeriod ppp = new PractitionerProfilePeriod();

			ppp.setPeriodId(dpp.getPeriodId());
			ppp.setPeriodStart(dpp.getPeriodStartDate());
			ppp.setPeriodEnd(dpp.getPeriodEndDate());
			ppp.setYearEnd(dpp.getYearEndDate());
			ppp.setLastModified(dpp.getLastModified());
			ppp.setModifiedBy(dpp.getModifiedBy());

			ppps.add(ppp);
		}
		return ppps;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.OutlierCriteriaRepository#getPractitionerProfileCriteria(javax.persistence
	 * .EntityManager)
	 */
	@Override
	public List<DssMsiConstant> getPractitionerProfileCriteria() {

		return outlierCriteriaDAO.getDssMsiConstants(PRACTITIONER_CRITERIA_SUBSYSTEM);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.OutlierCriteriaRepository#savePractitionerProfileCriteria(ca.medavie
	 * .nspp .audit.business.criteria.PractitionerProfileCriteria)
	 */
	@Override
	public void savePractitionerProfileCriteria(DssMsiConstant aProfileCriteriaConstant) {

		// Save the updates to the database
		outlierCriteriaDAO.saveDssMsiConstant(aProfileCriteriaConstant);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.OutlierCriteriaRepository#saveOutlierCriteria(ca.medavie.nspp.audit.
	 * ejbclient .criteria.OutlierCriteria)
	 */
	@Override
	public void saveOutlierCriteria(OutlierCriteria anOutlierCriteria) {

		// Get the DB rows to be updated
		List<DssMsiConstant> constantsToUpdate = outlierCriteriaDAO.getDssMsiConstants(OUTLIER_CRITERIA_SUBSYSTEM,
				anOutlierCriteria.getCode());

		for (DssMsiConstant dmc : constantsToUpdate) {
			// Apply any changes to description
			dmc.setConstantDesc(anOutlierCriteria.getDescription());
			// Set the correct Constant value
			if (anOutlierCriteria.getType().equals(OutlierCriteria.Types.CHAR)) {
				dmc.setConstantChar((String) anOutlierCriteria.getValue());
			}
			if (anOutlierCriteria.getType().equals(OutlierCriteria.Types.INTEGER)) {
				dmc.setConstantNumeric(Integer.parseInt(String.valueOf(anOutlierCriteria.getValue()).trim()));
			}
			if (anOutlierCriteria.getType().equals(OutlierCriteria.Types.CURRENCY)) {
				dmc.setConstantCurrency(new BigDecimal(String.valueOf(anOutlierCriteria.getValue()).trim()));
			}

			// Set Modified meta data
			dmc.setModifiedBy(anOutlierCriteria.getModifiedBy());
			dmc.setLastModified(anOutlierCriteria.getLastModified());
		}

		// Save updates to the DB
		outlierCriteriaDAO.saveDssMsiConstants(constantsToUpdate);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.OutlierCriteriaRepository#addNewPracitionerProfilePeriod(ca.medavie.nspp
	 * .audit.business.criteria.PractitionerProfilePeriod)
	 */
	@Override
	public PractitionerProfilePeriod addNewPracitionerProfilePeriod(PractitionerProfilePeriod practitionerPeriod) {

		DssProfilePeriod dpp = new DssProfilePeriod();

		dpp.setPeriodStartDate(practitionerPeriod.getPeriodStart());
		dpp.setPeriodEndDate(practitionerPeriod.getPeriodEnd());
		dpp.setYearEndDate(practitionerPeriod.getYearEnd());
		dpp.setLastModified(practitionerPeriod.getLastModified());
		dpp.setModifiedBy(practitionerPeriod.getModifiedBy());
		dpp.setPeriodType("PROVI");
		dpp = outlierCriteriaDAO.saveDssProfilePeriod(dpp);

		// Get the PeriodID that was generated
		practitionerPeriod.setPeriodId(dpp.getPeriodId());

		return practitionerPeriod;
	}

	/**
	 * 
	 * get the corresponding values based on the type
	 * 
	 * @param constantType
	 * @param dmc
	 * @return a value
	 */
	private Object getTypeValue(String constantType, DssMsiConstant dmc) {

		if (constantType.equals("C")) {

			return dmc.getConstantChar();
		}

		if (constantType.equals("D")) {

			return dmc.getConstantDate();
		}

		if (constantType.equals("N")) {

			return dmc.getConstantNumeric();
		}

		if (constantType.equals("$")) {

			return dmc.getConstantCurrency();
		}

		return null;
	}

	/**
	 * 
	 * get the type
	 * 
	 * @param constantType
	 * @return
	 */
	private Types getConstantType(String constantType) {

		if (constantType.equals("C")) {

			return OutlierCriteria.Types.CHAR;
		}

		if (constantType.equals("D")) {

			return OutlierCriteria.Types.DATE;
		}

		if (constantType.equals("N")) {

			return OutlierCriteria.Types.INTEGER;
		}

		if (constantType.equals("$")) {

			return OutlierCriteria.Types.CURRENCY;
		}

		return null;
	}

	/**
	 * @param outlierCriteriaDAO
	 *            the outlierCriteriaDAO to set
	 */
	public void setOutlierCriteriaEJBDAO(OutlierCriteriaDAO outlierCriteriaDAO) {
		this.outlierCriteriaDAO = outlierCriteriaDAO;
	}
}

package ca.medavie.nspp.audit.service.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import ca.medavie.nspp.audit.service.dao.DssGLNumbersDAO;
import ca.medavie.nspp.audit.service.dao.DssGLNumbersDAOImpl;
import ca.medavie.nspp.audit.service.data.DssGLNumberBO;
import ca.medavie.nspp.audit.service.data.DssGlNumber;

public class DssGLNumbersRepositoryImpl extends BaseRepository implements DssGLNumbersRepository {

	private DssGLNumbersDAO dssGLNumbersDAO;

	public DssGLNumbersRepositoryImpl(EntityManager em) {
		dssGLNumbersDAO = new DssGLNumbersDAOImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DssGLNumbersRepository#getGLNumbers()
	 */
	@Override
	public List<DssGLNumberBO> getGLNumbers() {

		List<DssGlNumber> sources = dssGLNumbersDAO.getGLNumbers();
		List<DssGLNumberBO> result = new ArrayList<DssGLNumberBO>();

		for (DssGlNumber source : sources) {
			DssGLNumberBO target = new DssGLNumberBO();
			BEAN_MAPPER.map(source, target);

			result.add(target);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DssGLNumbersRepository#getGLNumberByGL(java.lang.Long)
	 */
	@Override
	public DssGLNumberBO getGLNumberByGL(Long aGlNumber) {

		DssGlNumber source = dssGLNumbersDAO.getGLNumberByGL(aGlNumber);
		DssGLNumberBO result = new DssGLNumberBO();

		if (source != null) {
			BEAN_MAPPER.map(source, result);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.DssGLNumbersRepository#saveGLNumber(ca.medavie.nspp.audit.service.data
	 * .DssGLNumber)
	 */
	@Override
	public void saveGLNumber(DssGLNumberBO aDssGLNumber) {
		// Get the existing record from the DB
		DssGlNumber target = dssGLNumbersDAO.getGLNumberByGL(aDssGLNumber.getGlNumber());

		// Map changes to the entity and persist
		if (target == null) {
			// New record being saved
			target = new DssGlNumber();
		}

		BEAN_MAPPER.map(aDssGLNumber, target);

		// Save the updated entity
		dssGLNumbersDAO.saveGLNumber(target);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DssGLNumbersRepository#deleteGLNumbers(java.util.List)
	 */
	@Override
	public void deleteGLNumbers(List<DssGLNumberBO> aListOfGLNumbersForDelete) {

		// Get the IDs for every record being deleted
		List<Long> glNumbers = new ArrayList<Long>();
		for (DssGLNumberBO gl : aListOfGLNumbersForDelete) {
			glNumbers.add(gl.getGlNumber());
		}

		// Loads all entities that will be deleted
		List<DssGlNumber> recordsToDelete = dssGLNumbersDAO.getGLNumbersByGL(glNumbers);
		// Perform delete
		dssGLNumbersDAO.deleteGLNumbers(recordsToDelete);
	}
}

package ca.medavie.nspp.audit.service.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.dao.MedicareDAO;
import ca.medavie.nspp.audit.service.dao.MedicareDAOImpl;
import ca.medavie.nspp.audit.service.data.AuditCriteria;
import ca.medavie.nspp.audit.service.data.AuditCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.BusinessArrangement;
import ca.medavie.nspp.audit.service.data.DssBusinessArrangement;
import ca.medavie.nspp.audit.service.data.DssMedAudBusArrExcl;
import ca.medavie.nspp.audit.service.data.DssMedAudCriteria;
import ca.medavie.nspp.audit.service.data.DssMedAudHlthSrvDesc;
import ca.medavie.nspp.audit.service.data.DssMedAudHlthSrvExcl;
import ca.medavie.nspp.audit.service.data.DssMedAudIndivExcl;
import ca.medavie.nspp.audit.service.data.DssMedAudIndivExclPK;
import ca.medavie.nspp.audit.service.data.DssMedSeAudit;
import ca.medavie.nspp.audit.service.data.DssMedSeAuditPK;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.IndividualExclusion;
import ca.medavie.nspp.audit.service.data.MedicareAuditLetterResponse;
import ca.medavie.nspp.audit.service.data.MedicareAuditLetterResponseSearchCriteria;
import ca.medavie.nspp.audit.service.data.PayeeType;
import ca.medavie.nspp.audit.service.data.PaymentResponsibility;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssBusinessArrangementExclusionMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssHealthServiceDescriptionMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssHealthServiceExclusionMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssIndividualExclusion;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssMedAuditCriteriaMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssMedicareALRMappedEntity;

/**
 * Implementation of the Medicare repository layer
 */
public class MedicareRepositoryImpl extends BaseRepository implements MedicareRepository {

	private MedicareDAO medicareDAO;

	private static final Logger logger = LoggerFactory.getLogger(MedicareRepositoryImpl.class);

	/**
	 * Constructor for Medicare Repository
	 * 
	 * @param em
	 */
	public MedicareRepositoryImpl(EntityManager em) {
		medicareDAO = new MedicareDAOImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.MedicareRepository#getSearchedHealthServiceCriteriaDescriptions(ca.medavie
	 * .nspp.audit.service.data.HealthServiceCriteriaSearchCriteria)
	 */
	@Override
	public List<HealthServiceCriteria> getSearchedHealthServiceCriteriaDescriptions(
			HealthServiceCriteriaSearchCriteria aSearchCriteria) {
		logger.debug("MedicareRepositoryImpl -- getSearchedHealthServiceCriteriaDescriptions(): Begin");

		List<HealthServiceCriteria> result = new ArrayList<HealthServiceCriteria>();
		List<DssHealthServiceDescriptionMappedEntity> dataObjects = medicareDAO
				.getSearchedHealthServiceCriteriaDescriptions(aSearchCriteria);

		for (DssHealthServiceDescriptionMappedEntity source : dataObjects) {

			HealthServiceCriteria hsc = new HealthServiceCriteria();
			BEAN_MAPPER.map(source, hsc);
			result.add(hsc);
		}

		logger.debug("MedicareRepositoryImpl -- getSearchedHealthServiceCriteriaDescriptions(): End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.MedicareRepository#saveHealthServiceCriteriaDescriptions(java.util.List)
	 */
	@Override
	public void saveHealthServiceCriteriaDescriptions(List<HealthServiceCriteria> aListOfHsc) {

		logger.debug("MedicareRepositoryImpl -- saveHealthServiceCriteriaDescriptions(): Begin");
		List<DssMedAudHlthSrvDesc> entitesToSave = new ArrayList<DssMedAudHlthSrvDesc>();

		for (HealthServiceCriteria hsc : aListOfHsc) {
			/*
			 * First determine if a description exists in dss_med_aud_hlth_srv_desc. If not we need to create a new
			 * record for it. If it does exist we are updating the current value
			 */
			// Convert ID on HSC to Long
			Long pkID = Long.valueOf(hsc.getHealthServiceID().trim());
			DssMedAudHlthSrvDesc dssMedDesc = medicareDAO.getDssMedAudHlthSrvDeskByPK(pkID);

			if (dssMedDesc == null) {
				// No description exists. Creating new entity.
				dssMedDesc = new DssMedAudHlthSrvDesc();
				BEAN_MAPPER.map(hsc, dssMedDesc);
			} else {
				// Description exists updating existing entity.
				BEAN_MAPPER.map(hsc, dssMedDesc);
			}

			// Add to list of DssMedAudHlthSrvDesc to save
			entitesToSave.add(dssMedDesc);
		}
		// Submit changes to DB
		medicareDAO.saveHealthServiceCriteriaDescriptions(entitesToSave);
		logger.debug("MedicareRepositoryImpl -- saveHealthServiceCriteriaDescriptions(): End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.MedicareRepository#getHealthServiceCriteriaExclusions()
	 */
	@Override
	public List<HealthServiceCriteria> getHealthServiceCriteriaExclusions() {

		logger.debug("MedicareRepositoryImpl -- getHealthServiceCriteriaExclusions(): Begin");
		List<HealthServiceCriteria> result = new ArrayList<HealthServiceCriteria>();
		List<DssHealthServiceExclusionMappedEntity> dataObjects = medicareDAO.getHealthServiceCriteriaExclusions();

		for (DssHealthServiceExclusionMappedEntity source : dataObjects) {

			HealthServiceCriteria hsc = new HealthServiceCriteria();
			BEAN_MAPPER.map(source, hsc);
			result.add(hsc);
		}
		logger.debug("MedicareRepositoryImpl -- getHealthServiceCriteriaExclusions(): End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.MedicareRepository#saveHealthServiceCriteriaExclusions(java.util.List)
	 */
	@Override
	public void saveHealthServiceCriteriaExclusions(List<HealthServiceCriteria> aListOfHscForSave) {

		logger.debug("MedicareRepositoryImpl -- saveHealthServiceCriteriaExclusions(): Begin");
		List<DssMedAudHlthSrvExcl> exclusionsToSave = new ArrayList<DssMedAudHlthSrvExcl>();

		for (HealthServiceCriteria hsc : aListOfHscForSave) {
			/*
			 * First determine if a exclusion exists in dss_med_aud_hlth_srv_excl. If not we need to create a new record
			 * for it. If it does exist we are updating the current value
			 */
			// Convert ID on HSC to Long
			Long pkID = Long.valueOf(hsc.getHealthServiceID().trim());
			DssMedAudHlthSrvExcl dssMedExcl = medicareDAO.getDssMedAudHlthSrvExclByPK(pkID);
			if (dssMedExcl == null) {
				// Exclusion does not exist in system. Create new one
				dssMedExcl = new DssMedAudHlthSrvExcl();
				BEAN_MAPPER.map(hsc, dssMedExcl);
			} else {
				// Exclusion already exists. Update existing record
				BEAN_MAPPER.map(hsc, dssMedExcl);
			}

			// Add entity to list for save
			exclusionsToSave.add(dssMedExcl);
		}
		// Submit changes to DB
		medicareDAO.saveHealthServiceCriteriaExclusions(exclusionsToSave);
		logger.debug("MedicareRepositoryImpl -- saveHealthServiceCriteriaExclusions(): End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.MedicareRepository#deleteHealthServiceCriteriaExclusions(java.util.List)
	 */
	@Override
	public void deleteHealthServiceCriteriaExclusions(List<HealthServiceCriteria> aListOfHscForDelete) {

		logger.debug("MedicareRepositoryImpl -- deleteHealthServiceCriteriaExclusions(): Begin");
		List<DssMedAudHlthSrvExcl> exclusionsToDelete = new ArrayList<DssMedAudHlthSrvExcl>();

		for (HealthServiceCriteria hsc : aListOfHscForDelete) {
			/*
			 * First determine if a description exists in dss_med_aud_hlth_srv_excl. If not the delete will not be
			 * performed on this record
			 */
			// Convert ID on HSC to Long
			Long pkID = Long.valueOf(hsc.getHealthServiceID().trim());
			DssMedAudHlthSrvExcl dssMedExcl = medicareDAO.getDssMedAudHlthSrvExclByPK(pkID);
			if (dssMedExcl != null) {
				// Exclusion found. Add to delete list
				exclusionsToDelete.add(dssMedExcl);
			}
		}
		// Submit delete request to DB
		medicareDAO.deleteHealthServiceCriteriaExclusions(exclusionsToDelete);
		logger.debug("MedicareRepositoryImpl -- deleteHealthServiceCriteriaExclusions(): End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.MedicareRepository#getIndividualExclusions()
	 */
	@Override
	public List<IndividualExclusion> getIndividualExclusions() {
		logger.debug("MedicareRepositoryImpl -- getIndividualExclusions(): Begin");

		List<IndividualExclusion> result = new ArrayList<IndividualExclusion>();
		List<DssIndividualExclusion> dataObjects = medicareDAO.getIndividualExclusions();

		for (DssIndividualExclusion source : dataObjects) {

			IndividualExclusion target = new IndividualExclusion();
			BEAN_MAPPER.map(source, target);
			result.add(target);
		}

		logger.debug("MedicareRepositoryImpl -- getIndividualExclusions(): End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.MedicareRepository#saveIndividualExclusions(java.util.List)
	 */
	@Override
	public void saveIndividualExclusions(List<IndividualExclusion> aListOfIndExclToSave) {
		logger.debug("MedicareRepositoryImpl -- saveIndividualExclusions(): Begin");

		// Stage 1 of save.. Delete the records being updated
		List<DssMedAudIndivExcl> exclusionsForRemovalForUpdate = new ArrayList<DssMedAudIndivExcl>();
		for (IndividualExclusion excl : aListOfIndExclToSave) {

			/*
			 * Only attempt to delete existing records.. This is simply determined by the existence of the original
			 * Effective From value
			 */
			if (excl.getOriginalEffectiveFrom() != null) {
				/*
				 * Determine the records being saved are updates of existing, we must delete the record and re add it as
				 * a new row.. The reason for this is due to the Primary Key. The user is able to change part of the
				 * primary (Effective From) and if this is changed we are unable to complete the update successfully
				 * without an Optimistic lock exception.
				 */
				DssMedAudIndivExclPK pk = new DssMedAudIndivExclPK();
				pk.setEffectiveFromDate(excl.getOriginalEffectiveFrom());
				pk.setHealthCardNumber(excl.getHealthCardNumber());

				DssMedAudIndivExcl dssMedExcl = medicareDAO.getDssMedAudIndivExclByPK(pk);
				if (dssMedExcl != null) {
					exclusionsForRemovalForUpdate.add(dssMedExcl);
				}
			}
		}

		// Submit delete request
		medicareDAO.deleteIndividualExclusions(exclusionsForRemovalForUpdate);

		// Stage 2 of save. Create new entities for each updated/new record
		List<DssMedAudIndivExcl> exclusionsToSave = new ArrayList<DssMedAudIndivExcl>();
		for (IndividualExclusion excl : aListOfIndExclToSave) {

			DssMedAudIndivExcl newEntity = new DssMedAudIndivExcl();
			BEAN_MAPPER.map(excl, newEntity);

			// Add to list for save
			exclusionsToSave.add(newEntity);
		}

		// Save changes to database
		medicareDAO.saveIndividualExclusions(exclusionsToSave);

		logger.debug("MedicareRepositoryImpl -- saveIndividualExclusions(): End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.MedicareRepository#getHealthCardNumberOwnerName(java.lang.String)
	 */
	@Override
	public String getHealthCardNumberOwnerName(String aHealthCardNumber) {
		logger.debug("MedicareRepositoryImpl -- getHealthCardNumberOwnerName(): Begin");
		logger.debug("MedicareRepositoryImpl -- getHealthCardNumberOwnerName(): End");

		return medicareDAO.getHealthCardNumberOwnerName(aHealthCardNumber);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.MedicareRepository#saveMedicareAuditCriteria(ca.medavie.nspp.audit.service
	 * .data.AuditCriteria)
	 */
	@Override
	public void saveMedicareAuditCriteria(AuditCriteria anAuditCriteria) {
		logger.debug("MedicareRepositoryImpl -- saveMedicareAuditCriteria(): Begin");
		/*
		 * If there is a Criteria ID set we are editing an existing record. Grab that record for update. If there is no
		 * ID we create a new entity to persist
		 */
		DssMedAudCriteria dssMedAuditCriteria;

		if (anAuditCriteria.getAuditCriteriaId() == null) {
			// Call Sequence to generate a unique ID for this entity
			Long uniqueId = medicareDAO.getNextMedAudCriteriaSequenceValue();
			anAuditCriteria.setAuditCriteriaId(uniqueId);

			// Need to update all children to have the correct parent ID
			if (anAuditCriteria.getHealthServiceCriterias() != null) {
				for (HealthServiceCriteria hsc : anAuditCriteria.getHealthServiceCriterias()) {
					hsc.setAuditCriteriaId(uniqueId);
				}
			}
			if (anAuditCriteria.getPaymentResponsibilities() != null) {
				for (PaymentResponsibility pr : anAuditCriteria.getPaymentResponsibilities()) {
					pr.setAuditCriteriaId(uniqueId);
				}
			}
			if (anAuditCriteria.getPayeeTypes() != null) {
				for (PayeeType pt : anAuditCriteria.getPayeeTypes()) {
					pt.setAuditCriteriaId(uniqueId);
				}
			}

			// Create new Entity
			dssMedAuditCriteria = new DssMedAudCriteria();
			BEAN_MAPPER.map(anAuditCriteria, dssMedAuditCriteria);
		} else {
			// Load the existing record by PK
			dssMedAuditCriteria = medicareDAO.getDssMedAudCriteriaByPk(anAuditCriteria.getAuditCriteriaId());

			// Clear children lists
			dssMedAuditCriteria.getDssMedAudPayToCdXrefs().clear();
			dssMedAuditCriteria.getDssMedAudHlthSrvXrefs().clear();
			dssMedAuditCriteria.getDssMedAudPayRespXrefs().clear();

			BEAN_MAPPER.map(anAuditCriteria, dssMedAuditCriteria);
		}
		// Submit changes to DAO
		medicareDAO.saveMedicareAuditCriteria(dssMedAuditCriteria);

		logger.debug("MedicareRepositoryImpl -- saveMedicareAuditCriteria(): End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.MedicareRepository#getPractitionerName(java.lang.Long,
	 * java.lang.String)
	 */
	@Override
	public String getPractitionerName(Long aPractitionerId, String aSubcategory) {
		logger.debug("MedicareRepositoryImpl -- getPractitionerName(): Begin");
		logger.debug("MedicareRepositoryImpl -- getPractitionerName(): End");
		return medicareDAO.getPractitionerName(aPractitionerId, aSubcategory);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.MedicareRepository#getMedicarePractitionerAuditCriteria(java.lang.Long,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public AuditCriteria getMedicarePractitionerAuditCriteria(Long aPractitionerId, String aSubcategory,
			String anAuditType) {
		logger.debug("MedicareRepositoryImpl -- getMedicarePractitionerAuditCriteria(): Begin");

		AuditCriteria result = null;
		DssMedAudCriteria dataEntity = medicareDAO.getMedicarePractitionerAuditCriteria(aPractitionerId, aSubcategory,
				anAuditType);

		// Map if result found
		if (dataEntity != null) {
			result = new AuditCriteria();
			BEAN_MAPPER.map(dataEntity, result);
		}

		logger.debug("MedicareRepositoryImpl -- getMedicarePractitionerAuditCriteria(): End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.MedicareRepository#getSearchPractitionerAuditCriterias(ca.medavie.nspp
	 * .audit.service.data.AuditCriteriaSearchCriteria, int, int)
	 */
	@Override
	public List<AuditCriteria> getSearchPractitionerAuditCriterias(AuditCriteriaSearchCriteria aSearchCriteria,
			int startRow, int maxRows) {
		logger.debug("MedicareRepositoryImpl -- getSearchPractitionerAuditCriterias(): Begin");

		List<AuditCriteria> results = new ArrayList<AuditCriteria>();
		List<DssMedAuditCriteriaMappedEntity> dataEntities = medicareDAO.getSearchPractitionerAuditCriterias(
				aSearchCriteria, startRow, maxRows);

		for (DssMedAuditCriteriaMappedEntity entity : dataEntities) {

			AuditCriteria targetBo = new AuditCriteria();
			BEAN_MAPPER.map(entity, targetBo);
			results.add(targetBo);
		}

		logger.debug("MedicareRepositoryImpl -- getSearchPractitionerAuditCriterias(): End");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.MedicareRepository#getSearchedAuditCriteriaCount(ca.medavie.nspp.audit
	 * .service.data.AuditCriteriaSearchCriteria)
	 */
	@Override
	public Integer getSearchedAuditCriteriaCount(AuditCriteriaSearchCriteria anAuditCriteriaSearchCriteria) {
		logger.debug("MedicareRepositoryImpl -- getSearchedAuditCriteriaCount(): Begin");
		Integer count = medicareDAO.getSearchedAuditCriteriaCount(anAuditCriteriaSearchCriteria);
		logger.debug("MedicareRepositoryImpl -- getSearchedAuditCriteriaCount(): Begin");
		return count;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.MedicareRepository#getPractitionerAuditCriteriaById(java.lang.Long)
	 */
	@Override
	public AuditCriteria getPractitionerAuditCriteriaById(Long anId) {
		logger.debug("MedicareRepositoryImpl -- getPractitionerAuditCriteriaById(): Begin");

		AuditCriteria result = new AuditCriteria();
		DssMedAudCriteria dataEntity = medicareDAO.getDssMedAudCriteriaByPk(anId);

		// Perform map
		BEAN_MAPPER.map(dataEntity, result);

		// Get the lastAuditDate if available
		result.setLastAuditDate(getLastAuditDate(result.getAuditCriteriaId()));

		logger.debug("MedicareRepositoryImpl -- getPractitionerAuditCriteriaById(): End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.MedicareRepository#getDefaultAuditCriteriaByType(java.lang.String)
	 */
	@Override
	public AuditCriteria getDefaultAuditCriteriaByType(String anAuditType) {
		logger.debug("MedicareRepositoryImpl -- getDefaultAuditCriteriaByType(): Begin");

		AuditCriteria result = new AuditCriteria();
		DssMedAudCriteria dataEntity = medicareDAO.getDefaultAuditCriteriaByType(anAuditType);

		if (dataEntity != null) {
			// Audit record found, map to BO
			BEAN_MAPPER.map(dataEntity, result);

			// Get the lastAuditDate if available
			result.setLastAuditDate(getLastAuditDate(result.getAuditCriteriaId()));

		} else {
			// No audit record exists for this audit type. Set the AuditType on the new object
			result.setAuditType(anAuditType);
		}

		logger.debug("MedicareRepositoryImpl -- getDefaultAuditCriteriaByType(): End");

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.MedicareRepository#getSearchedMedicareALR(ca.medavie.nspp.audit.service
	 * .data.MedicareAuditLetterResponseSearchCriteria)
	 */
	@Override
	public List<MedicareAuditLetterResponse> getSearchedMedicareALR(
			MedicareAuditLetterResponseSearchCriteria medicareALRSearchCriteria, int startRow, int maxRows) {

		logger.debug("MedicareRepositoryImpl -- getSearchedMedicareALR(): Begin");

		List<DssMedicareALRMappedEntity> sources = medicareDAO.getMedicareALRRecords(medicareALRSearchCriteria,
				startRow, maxRows);

		List<MedicareAuditLetterResponse> result = new ArrayList<MedicareAuditLetterResponse>();

		for (DssMedicareALRMappedEntity source : sources) {

			MedicareAuditLetterResponse target = new MedicareAuditLetterResponse();

			// transfer data object to business object
			BEAN_MAPPER.map(source, target);

			result.add(target);
		}

		logger.debug("MedicareRepositoryImpl -- getSearchedMedicareALR(): End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.MedicareRepository#getSearchedMedicareALRCount(ca.medavie.nspp.audit
	 * .service.data.MedicareAuditLetterResponseSearchCriteria)
	 */
	@Override
	public Integer getSearchedMedicareALRCount(MedicareAuditLetterResponseSearchCriteria medicareALRSearchCriteria) {
		logger.debug("MedicareRepositoryImpl -- getSearchedMedicareALRCount(): Begin");
		Integer count = medicareDAO.getSearchedMedicareALRCount(medicareALRSearchCriteria);
		logger.debug("MedicareRepositoryImpl -- getSearchedMedicareALRCount(): End");
		return count;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.MedicareRepository#saveMedicareALR(ca.medavie.nspp.audit.service.data
	 * .MedicareAuditLetterResponse)
	 */
	@Override
	public void saveMedicareALR(MedicareAuditLetterResponse selectedMCALR) {

		logger.debug("MedicareRepositoryImpl -- saveMedicareALR(): Begin");
		// if it is an existing one
		DssMedSeAuditPK pk = new DssMedSeAuditPK();
		pk.setAudit_run_number(selectedMCALR.getAudit_run_number());
		pk.setResponse_tag_number(selectedMCALR.getResponse_tag_number());
		pk.setSe_number(selectedMCALR.getSe_number());
		pk.setSe_sequence_number(selectedMCALR.getSe_sequence_number());

		DssMedSeAudit target = medicareDAO.getMedicareALRByID(pk);
		// transfer business t data object
		BEAN_MAPPER.map(selectedMCALR, target);

		medicareDAO.saveMedicareALR(target);
		logger.debug("MedicareRepositoryImpl -- saveMedicareALR(): End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.MedicareRepository#getNumberOfLettersCreated(java.lang.Long,
	 * java.lang.String)
	 */
	@Override
	public Long getNumberOfLettersCreated(Long anAuditRunNumber, String anAuditType) {
		logger.debug("MedicareRepositoryImpl -- getNumberOfLettersCreated(): Begin");
		logger.debug("MedicareRepositoryImpl -- getNumberOfLettersCreated(): End");
		return medicareDAO.getNumberOfLettersCreated(anAuditRunNumber, anAuditType);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.MedicareRepository#getBusinessArrangement(java.lang.Long)
	 */
	@Override
	public BusinessArrangement getBusinessArrangement(Long aBusinessArrangementNumber) {

		logger.debug("MedicareRepositoryImpl -- getBusinessArrangement(): Begin");

		DssBusinessArrangement source = medicareDAO.getBusinessArrangement(aBusinessArrangementNumber);
		BusinessArrangement target = null;

		// Ensure BA was found.. if not we simply return null
		if (source != null) {
			target = new BusinessArrangement();
			BEAN_MAPPER.map(source, target);
		}

		logger.debug("MedicareRepositoryImpl -- getBusinessArrangement(): End");
		return target;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.MedicareRepository#getBusinessArrangementExclusions()
	 */
	@Override
	public List<BusinessArrangement> getBusinessArrangementExclusions() {

		logger.debug("MedicareRepositoryImpl -- getBusinessArrangementExclusions(): Begin");

		// Load exclusions from db
		List<DssBusinessArrangementExclusionMappedEntity> sources = medicareDAO.getBusinessArrangementExclusions();
		List<BusinessArrangement> results = new ArrayList<BusinessArrangement>();

		for (DssBusinessArrangementExclusionMappedEntity source : sources) {

			BusinessArrangement target = new BusinessArrangement();
			// Map entity data to Business object
			BEAN_MAPPER.map(source, target);
			results.add(target);
		}

		logger.debug("MedicareRepositoryImpl -- getBusinessArrangementExclusions(): End");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.MedicareRepository#saveBusinessArrangementExclusion(ca.medavie.nspp.
	 * audit.service.data.BusinessArrangement)
	 */
	@Override
	public void saveBusinessArrangementExclusion(BusinessArrangement aBusinessArrangementExclusion) {

		logger.debug("MedicareRepositoryImpl -- saveBusinessArrangementExclusion(): Begin");

		// Map business object to entity
		DssMedAudBusArrExcl target = new DssMedAudBusArrExcl();
		BEAN_MAPPER.map(aBusinessArrangementExclusion, target);

		// Save the new Exclusion
		medicareDAO.saveBusinessArrangementExclusion(target);

		logger.debug("MedicareRepositoryImpl -- saveBusinessArrangementExclusion(): End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.MedicareRepository#deleteBusinessArrangementExclusions(java.util.List)
	 */
	@Override
	public void deleteBusinessArrangementExclusions(List<BusinessArrangement> aListOfBAExclusionsForDelete) {

		logger.debug("MedicareRepositoryImpl -- deleteBusinessArrangementExclusions(): Begin");
		List<DssMedAudBusArrExcl> exclusionsToDelete = new ArrayList<DssMedAudBusArrExcl>();

		for (BusinessArrangement ba : aListOfBAExclusionsForDelete) {
			DssMedAudBusArrExcl entity = medicareDAO.getDssMedAudBusArrExclByPK(ba.getBusArrNumber());
			if (entity != null) {
				exclusionsToDelete.add(entity);
			}
		}

		// Delete the records
		medicareDAO.deleteBusinessArrangementExclusions(exclusionsToDelete);

		logger.debug("MedicareRepositoryImpl -- deleteBusinessArrangementExclusions(): End");
	}

	/**
	 * Simple utility method used by the Repository to load the Last Audit Date value for an Audit Criteria
	 * 
	 * @param anAuditCriteriaId
	 * @return the lastAuditDate for the specified Audit Criteria. Null is returned if no date is found.
	 */
	private Date getLastAuditDate(Long anAuditCriteriaId) {
		return medicareDAO.getLastAuditDate(anAuditCriteriaId);
	}
}

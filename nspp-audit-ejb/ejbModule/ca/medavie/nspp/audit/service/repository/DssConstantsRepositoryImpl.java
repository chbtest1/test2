package ca.medavie.nspp.audit.service.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import ca.medavie.nspp.audit.service.dao.DssConstantsDAO;
import ca.medavie.nspp.audit.service.dao.DssConstantsDAOImpl;
import ca.medavie.nspp.audit.service.data.DssConstantBO;
import ca.medavie.nspp.audit.service.data.DssMsiConstant;
import ca.medavie.nspp.audit.service.exception.DssConstantsServiceException;

public class DssConstantsRepositoryImpl extends BaseRepository implements DssConstantsRepository {

	private DssConstantsDAO dssConstantsDAO;

	public DssConstantsRepositoryImpl(EntityManager em) {
		dssConstantsDAO = new DssConstantsDAOImpl(em);
	}

	/* (non-Javadoc)
	 * @see ca.medavie.nspp.audit.service.repository.DssConstantsRepository#getDssConstants()
	 */
	@Override
	public List<DssConstantBO> getDssConstants() {

		List<DssMsiConstant> constants = dssConstantsDAO.getDssConstants();
		List<DssConstantBO> result = new ArrayList<DssConstantBO>();

		for (DssMsiConstant constant : constants) {
			result.add(tableMapper(constant));
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see ca.medavie.nspp.audit.service.repository.DssConstantsRepository#updateDssConstant(ca.medavie.nspp.audit.service.data.DssConstantBO)
	 */
	@Override
	public void updateDssConstant(DssConstantBO dssConstant) throws DssConstantsServiceException {
		List<DssMsiConstant> constants = dssConstantsDAO.getDssConstants();
		for (DssMsiConstant constant : constants) {
			if (constant.getId().getConstantCode().equals(dssConstant.getConstantCode())) {
				constant.setConstantDesc(dssConstant.getConstantDesc());
				constant.setConstantDate(dssConstant.getConstantDate());
				dssConstantsDAO.updateDssConstant(constant);
				return;
			}
		}
		throw new DssConstantsServiceException("Constant not found.");
	}
	
	private DssConstantBO tableMapper(DssMsiConstant constant) {
		DssConstantBO constantBO = new DssConstantBO();
		constantBO.setSubsystem(constant.getId().getSubsystem());
		constantBO.setConstantCode(constant.getId().getConstantCode());
		constantBO.setConstantType(constant.getConstantType());
		constantBO.setConstantDesc(constant.getConstantDesc());
		constantBO.setConstantDate(constant.getConstantDate());
		constantBO.setModifiedBy(constant.getModifiedBy());
		constantBO.setLastModified(constant.getLastModified());
		return constantBO;
	}
}

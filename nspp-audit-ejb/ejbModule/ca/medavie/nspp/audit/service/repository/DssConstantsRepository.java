package ca.medavie.nspp.audit.service.repository;

import java.util.List;

import ca.medavie.nspp.audit.service.data.DssConstantBO;
import ca.medavie.nspp.audit.service.exception.DssConstantsServiceException;

public interface DssConstantsRepository {
	/**
	 * @return list of DssConstantBO
	 */
	public List<DssConstantBO> getDssConstants();

	/**
	 * @param dssConstant
	 * @return
	 * @throws DssConstantsServiceException 
	 */	
	public void updateDssConstant(DssConstantBO dssConstant) throws DssConstantsServiceException;

}
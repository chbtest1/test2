package ca.medavie.nspp.audit.service.repository;

import java.util.List;

import ca.medavie.nspp.audit.service.data.DssGLNumberBO;

public interface DssGLNumbersRepository {
	/**
	 * Loads all GL Numbers in the systems
	 * 
	 * @return all GL Numbers currently available.
	 */
	public List<DssGLNumberBO> getGLNumbers();

	/**
	 * Attempt to load a GL Number record by the specified GL Number.
	 * 
	 * @param aGlNumber
	 * @return the GLNumber record that uses the specified GL Number
	 */
	public DssGLNumberBO getGLNumberByGL(Long aGlNumber);

	/**
	 * Saves an existing (Changes) or new GL Number to the system.
	 * 
	 * @param aDssGLNumber
	 *            - GL Number to update or save
	 */
	public void saveGLNumber(DssGLNumberBO aDssGLNumber);

	/**
	 * Deletes the selected GL Numbers from the system.
	 * 
	 * @param aListOfGLNumbersForDelete
	 *            - GL Numbers to delete
	 */
	public void deleteGLNumbers(List<DssGLNumberBO> aListOfGLNumbersForDelete);
}

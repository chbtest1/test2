package ca.medavie.nspp.audit.service.repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import ca.medavie.nspp.audit.service.dao.LocumDAO;
import ca.medavie.nspp.audit.service.dao.LocumDAOImpl;
import ca.medavie.nspp.audit.service.data.Locum;
import ca.medavie.nspp.audit.service.data.LocumSearchCriteria;

/**
 * Implementation of the Locum repository layer
 */
public class LocumRepositoryImpl implements LocumRepository {

	private LocumDAO locumDAO;

	/**
	 * Constructor for Locum Repository
	 * 
	 * @param em
	 */
	public LocumRepositoryImpl(EntityManager em) {
		locumDAO = new LocumDAOImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.LocumRepository#getSearchedLocums(ca.medavie.nspp.audit.service.data
	 * .LocumSearchCriteria, int, int)
	 */
	@Override
	public List<Locum> getSearchedLocums(LocumSearchCriteria aLocumHostSearchCriteria, int startRow, int maxRows) {

		// Holds matching Locum objects
		List<Locum> locums = new ArrayList<Locum>();

		// Perform the search
		List<Object[]> searchResult = locumDAO.getSearchedLocums(aLocumHostSearchCriteria, startRow, maxRows);

		// Go over the results
		for (Object[] row : searchResult) {

			Locum locum = new Locum();

			locum.setPractitionerNumber(((BigDecimal) row[0]).longValue());
			locum.setPractitionerType((String) row[1]);
			locum.setCity((String) row[2]);
			locum.setBirthDate((Date) row[3]);
			locum.setPractitionerName((String) row[4]);

			// Determine if record is HOST and/or VISITOR
			Long hostCount = ((BigDecimal) row[5]).longValue();
			Long visitorCount = ((BigDecimal) row[6]).longValue();

			// If count is not zero then set true as boolean
			locum.setLocumHost(!hostCount.equals(new Long(0)));
			locum.setLocumVisitor(!visitorCount.equals(new Long(0)));

			locums.add(locum);
		}
		return locums;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.LocumRepository#getSearchedLocumsCount(ca.medavie.nspp.audit.service
	 * .data.LocumSearchCriteria)
	 */
	@Override
	public Integer getSearchedLocumsCount(LocumSearchCriteria aLocumSearchCriteria) {
		return locumDAO.getSearchedLocumsCount(aLocumSearchCriteria);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.LocumRepository#getSearchedLocumPractitioners(ca.medavie.nspp.audit.
	 * service.data.LocumSearchCriteria)
	 */
	@Override
	public List<Locum> getSearchedLocumPractitioners(LocumSearchCriteria aLocumSearchCriteria) {

		// Holds the matching LocumPractitioners
		List<Locum> locums = new ArrayList<Locum>();

		// Execute search
		List<Object[]> result = locumDAO.getSearchedLocumPractitioners(aLocumSearchCriteria);

		// Create Locums from loaded data
		for (Object[] row : result) {

			Locum locum = new Locum();
			locum.setPractitionerNumber(((BigDecimal) row[0]).longValue());
			locum.setPractitionerType((String) row[1]);
			locum.setCity((String) row[2]);
			locum.setPractitionerName((String) row[3]);

			locums.add(locum);
		}
		return locums;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.LocumRepository#getLocumDetails(ca.medavie.nspp.audit.service.data
	 * .Locum)
	 */
	@Override
	public Locum getLocumDetails(Locum aLocum) {

		// Load the locum records for the specified Locum type(Host and/or Visitor)
		List<Object[]> locumDetailResult = locumDAO.getLocumDetails(aLocum);

		// Holds the poupulated Locum objects that will be added to the provided Locum record
		List<Locum> locums = new ArrayList<Locum>();

		// Construct either Host or Parent Locums to add to the selected Locum record
		for (Object[] result : locumDetailResult) {
			Locum l = new Locum();

			l.setHostPractitionerNumber(((BigDecimal) result[0]).longValue());
			l.setHostPractitionerType((String) result[1]);
			l.setVisitingPractitionerNumber(((BigDecimal) result[2]).longValue());
			l.setVisitingPractitionerType((String) result[3]);

			// Set remaining details that are not Locum type sensitive
			l.setOriginalEffectiveFrom((Date) result[4]);
			l.setEffectiveFrom((Date) result[4]);
			l.setEffectiveTo((Date) result[5]);
			l.setPractitionerName((String) result[6]);
			l.setModifiedBy((String) result[7]);
			l.setLastModified((Date) result[8]);

			// Add completed Locum object to list
			locums.add(l);
		}

		// Add the locums to the provided Locum record
		aLocum.setLocums(locums);
		return aLocum;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.LocumRepository#saveLocum(ca.medavie.nspp.audit.service.data.Locum)
	 */
	@Override
	public void saveLocum(Locum aLocum) {
		locumDAO.saveLocum(aLocum);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.LocumRepository#deleteLocums(java.util.List)
	 */
	@Override
	public void deleteLocums(List<Locum> aListOfLocumsForDelete) {
		locumDAO.deleteLocums(aListOfLocumsForDelete);
	}
}

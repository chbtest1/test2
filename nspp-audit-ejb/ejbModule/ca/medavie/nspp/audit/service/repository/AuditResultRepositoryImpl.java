package ca.medavie.nspp.audit.service.repository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.dao.AuditResultDAO;
import ca.medavie.nspp.audit.service.dao.AuditResultDAOImpl;
import ca.medavie.nspp.audit.service.data.AuditResult;
import ca.medavie.nspp.audit.service.data.AuditResultAttachment;
import ca.medavie.nspp.audit.service.data.AuditResultNote;
import ca.medavie.nspp.audit.service.data.DssProvider2;
import ca.medavie.nspp.audit.service.data.DssProviderAudit;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;

/**
 * Implementation of the PeerGroup repository layer
 */
public class AuditResultRepositoryImpl extends BaseRepository implements AuditResultRepository {

	private static final Logger logger = LoggerFactory.getLogger(AuditResultRepositoryImpl.class);

	private AuditResultDAO auditResultDAO;

	/**
	 * Constructor taking an EntityManager
	 * 
	 * @param em
	 */
	public AuditResultRepositoryImpl(EntityManager em) {
		auditResultDAO = new AuditResultDAOImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.AuditResultRepository#getPractitionerSearchResult(ca.medavie.nspp.audit
	 * .service.data.PractitionerSearchCriteria)
	 */
	@Override
	public List<Practitioner> getPractitionerSearchResult(PractitionerSearchCriteria aPractitionerSearchCriteria) {

		List<DssProvider2> searchResult = auditResultDAO.findPractitioners(aPractitionerSearchCriteria);
		List<Practitioner> result = new ArrayList<Practitioner>();

		for (DssProvider2 source : searchResult) {

			Practitioner target = new Practitioner();
			BEAN_MAPPER.map(source, target);
			result.add(target);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.AuditResultRepository#setAuditResult(ca.medavie.nspp.audit.service.data
	 * .AuditResult)
	 */
	@Override
	public void setAuditResult(AuditResult anAuditedResultToSave) {
		logger.debug("AuditResultRepositoryImpl -- setAuditResult(): save or update a Audit Result.");

		// Load the Audit Result if it exists
		DssProviderAudit target = auditResultDAO.findAuditResultByID(anAuditedResultToSave.getAuditId());

		// If nothing is found we are saving a new AuditResult so create a new target object
		if (target == null) {
			target = new DssProviderAudit();

			// Since no target was found that would mean our BO has no ID
			anAuditedResultToSave.setAuditId(auditResultDAO.getNextAuditResultSequenceVal());
		}

		// Prior to mapping the AuditResult to a data entity acquire the IDs for notes if needed
		for (AuditResultNote note : anAuditedResultToSave.getNotes()) {
			if (note.getId() == null) {
				// Get the next Sequence value from 'dss_provider_audit_notes_seq'
				Long nextSeqVal = auditResultDAO.getNextAuditNoteSequenceVal();
				note.setId(nextSeqVal);

				// Set the Audit ID on the note
				note.setAuditId(anAuditedResultToSave.getAuditId());
			}
		}

		// Prior to mapping the AuditResult to a data entity acquire the IDs for attachments if needed
		for (AuditResultAttachment attachment : anAuditedResultToSave.getAttachments()) {
			if (attachment.getAttachmentId() == null) {
				// Get the next sequence value from 'dss_provider_audit_attach_seq'
				Long nextSeqVal = auditResultDAO.getNextAuditAttachmentSequenceVal();
				attachment.setAttachmentId(nextSeqVal);

				attachment.setAuditId(anAuditedResultToSave.getAuditId());
			}
		}

		BEAN_MAPPER.map(anAuditedResultToSave, target);
		auditResultDAO.saveOrUpdateDssProviderAudit(target);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.AuditResultRepository#getSelectedPractitionerAuditResultDetails(ca.medavie
	 * .nspp.audit.service.data.Practitioner)
	 */
	@Override
	public List<AuditResult> getSelectedPractitionerAuditResultDetails(Practitioner aSelectedPractitioner) {

		List<DssProviderAudit> sources = auditResultDAO.findAuditResults(aSelectedPractitioner);
		List<AuditResult> result = new ArrayList<AuditResult>();

		for (DssProviderAudit source : sources) {

			AuditResult target = new AuditResult();
			BEAN_MAPPER.map(source, target);
			result.add(target);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.AuditResultRepository#getHealthServiceCodes()
	 */
	@Override
	public List<String> getHealthServiceCodes() {

		return auditResultDAO.getHealthServiceCodes();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.AuditResultRepository#getAttachmentBinary(java.lang.Long)
	 */
	@Override
	public byte[] getAttachmentBinary(Long anAttachmentId) throws SQLException {
		return auditResultDAO.findAttachmentBinary(anAttachmentId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.AuditResultRepository#saveAttachmentBinary(java.lang.Long, byte[])
	 */
	@Override
	public void setAttachmentBinary(Long anAttachmentId, byte[] anAttachmentBinary) {
		auditResultDAO.saveAttachmentBinary(anAttachmentId, anAttachmentBinary);
	}

	@Override
	public String getReason(String healthServiceCode, String qualifer) {
		return auditResultDAO.getReason(healthServiceCode, qualifer);
	}

	@Override
	public void deleteAuditResult(AuditResult aSelectedAuditResult) {
		auditResultDAO.deleteAuditResult(aSelectedAuditResult);
	}

}

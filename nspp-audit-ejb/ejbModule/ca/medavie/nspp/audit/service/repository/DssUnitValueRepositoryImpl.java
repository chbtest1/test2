package ca.medavie.nspp.audit.service.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import ca.medavie.nspp.audit.service.dao.DssUnitValueDAO;
import ca.medavie.nspp.audit.service.dao.DssUnitValueDAOImpl;
import ca.medavie.nspp.audit.service.data.DssUnitValue;
import ca.medavie.nspp.audit.service.data.DssUnitValueBO;

public class DssUnitValueRepositoryImpl extends BaseRepository implements DssUnitValueRepository {

	DssUnitValueDAO dssUnitValueDAO;

	public DssUnitValueRepositoryImpl(EntityManager em) {
		dssUnitValueDAO = new DssUnitValueDAOImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DssUnitValueRepository#getDssUnitValues()
	 */
	@Override
	public List<DssUnitValueBO> getDssUnitValues() {

		List<DssUnitValue> sources = dssUnitValueDAO.getDssUnitValues();
		List<DssUnitValueBO> result = new ArrayList<DssUnitValueBO>();

		for (DssUnitValue source : sources) {

			DssUnitValueBO target = new DssUnitValueBO();
			BEAN_MAPPER.map(source, target);
			result.add(target);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.DssUnitValueRepository#saveOrUpdateDssUnitValue(ca.medavie.nspp.audit
	 * .service.data.DssUnitValueBO)
	 */
	@Override
	public DssUnitValueBO saveOrUpdateDssUnitValue(DssUnitValueBO aDssUnitValueBO) {
		DssUnitValue aDssUnitValue = null;

		// if it is an existing one
		if (aDssUnitValueBO.getUnitValueID() != null) {

			aDssUnitValue = dssUnitValueDAO.findDssUnitValueById(aDssUnitValueBO.getUnitValueID());
			BEAN_MAPPER.map(aDssUnitValueBO, aDssUnitValue);
		} else {
			aDssUnitValue = new DssUnitValue();
			BEAN_MAPPER.map(aDssUnitValueBO, aDssUnitValue);
		}

		aDssUnitValue = dssUnitValueDAO.saveOrUPdateDssUnitValue(aDssUnitValue);
		BEAN_MAPPER.map(aDssUnitValue, aDssUnitValueBO);
		return aDssUnitValueBO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DssUnitValueRepository#deleteDssUnitValues(java.util.List)
	 */
	@Override
	public void deleteDssUnitValues(List<DssUnitValueBO> aListOfDssUnitValuesForDelete) {

		// Holds the DSS Unit Value entities for delete
		List<DssUnitValue> dssUnitValues = new ArrayList<DssUnitValue>();

		// Create List of data Entities so JPA can handle the delete action
		for (DssUnitValueBO source : aListOfDssUnitValuesForDelete) {
			DssUnitValue target = new DssUnitValue();
			BEAN_MAPPER.map(source, target);

			// Add to list
			dssUnitValues.add(target);
		}
		// Perform delete action
		dssUnitValueDAO.deleteDssUnitValues(dssUnitValues);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DssUnitValueRepository#overlaps(ca.medavie.nspp.audit.service.data.
	 * DssUnitValueBO)
	 */
	@Override
	public Boolean overlaps(DssUnitValueBO aDssUnitValue) {
		return dssUnitValueDAO.overlaps(aDssUnitValue);
	}
}

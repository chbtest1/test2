package ca.medavie.nspp.audit.service.repository;

import java.util.List;
import java.util.Map;

import ca.medavie.nspp.audit.service.data.HealthServiceCode;
import ca.medavie.nspp.audit.service.data.PeerGroup;
import ca.medavie.nspp.audit.service.data.PeerGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.SubcategoryType;

/**
 * Interface of the Inquiry repository layer
 */
public interface InquiryRepository {

	/**
	 * @return Map of subcategory types, key is label as well as type code, value is subcategoryType Object
	 */
	public Map<String, SubcategoryType> getSubCategoryTypeDropDown();

	/**
	 * Loads Practitioners that match the provided search criteria (Searches only Practitioners currently in PeerGroups)
	 * 
	 * @param aPractitionerSearchCriteria
	 *            - User specified search Criteria
	 * @startRow
	 * @maxRows
	 * @return list of matching Practitioners
	 */
	public List<Practitioner> getSearchedPractitioners(PractitionerSearchCriteria aPractitionerSearchCriteria,
			int startRow, int maxRows);

	/**
	 * Loads the count of all records returned by a search without pagination enabled
	 * 
	 * @param aPractitionerSearchCriteria
	 * @return count of all records without pagination
	 */
	public Integer getSearchedPractitionersCount(PractitionerSearchCriteria aPractitionerSearchCriteria);

	/**
	 * Load in all details of the specified Practitioner
	 * 
	 * @param aPractitioner
	 *            - Shell of practitioner to use for search criteria.
	 * @return the specified practitioner with in-depth details
	 */
	public Practitioner getPractitionerDetails(Practitioner aPractitioner);

	/**
	 * Loads PeerGroups that match the provider search criteria
	 * 
	 * @param aPeerGroupSearchCriteria
	 *            - User specified search Criteria
	 * @startRow
	 * @maxRows
	 * @return list of matching PeerGroups
	 */
	public List<PeerGroup> getSearchPeerGroups(PeerGroupSearchCriteria aPeerGroupSearchCriteria, int startRow,
			int maxRows);

	/**
	 * Loads the count of all records returned by a search without pagination enabled
	 * 
	 * @param aPeerGroupSearchCriteria
	 * @return count of all records without pagination
	 */
	public Integer getSearchPeerGroupsCount(PeerGroupSearchCriteria aPeerGroupSearchCriteria);

	/**
	 * Load in all details of the specified PeerGroup
	 * 
	 * @param aPeerGroup
	 *            - Shell of a PeerGroup that will be populated. Also provides basic criteria for loading data
	 * @return the specified PeerGroup with in-depth details
	 */
	public PeerGroup getPeerGroupDetails(PeerGroup aPeerGroup);

	/**
	 * Load all HSC found under the provided HealthServiceCode/Group
	 * 
	 * @param aPractitioner
	 * @param aHealthServiceCode
	 * @return list of HealthServiceCodes found during drill down search for the provided HealthServiceCode/Group
	 */
	public List<HealthServiceCode> getHealthServiceDrillDownDetails(Practitioner aPractitioner,
			HealthServiceCode aHealthServiceCode);
}

package ca.medavie.nspp.audit.ui.navmodel;

import java.io.Serializable;

import javax.faces.bean.CustomScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean(name = PractitionerNavigationModel.BEAN_NAME)
@CustomScoped(value = "#{window}")
public class PractitionerNavigationModel implements Serializable {

	/** JRE generated */
	private static final long serialVersionUID = 462697031417863001L;
	/** Name of bean. */
	public static final String BEAN_NAME = "practitionerNavigationModel";

	private String currentPage = "index";

	/**
	 * Locum Menu links
	 */
	private static String[][] locumMenuLinks = { { "practitioner/Locum/LocumIndex.html",
			"menu.practitioner.locum.hosts" } };

	/**
	 * Practitioner DHA Menu links
	 */
	private static String[][] maintainProviderDHAMenuLinks = { { "practitioner/DHA/DHAIndex.html",
			"menu.practitioner.provider.dha" } };

	/**
	 * @return the locumMenuLinks
	 */
	public String[][] getLocumMenuLinks() {
		return locumMenuLinks;
	}

	/**
	 * @return the maintainProviderDHAMenuLinks
	 */
	public String[][] getMaintainProviderDHAMenuLinks() {
		return maintainProviderDHAMenuLinks;
	}

	/**
	 * @return the currentPage
	 */
	public String getCurrentPage() {
		return currentPage;
	}

	/**
	 * @param currentPage
	 *            the currentPage to set
	 */
	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}
}

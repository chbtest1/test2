package ca.medavie.nspp.audit.ui.navmodel;

import java.io.Serializable;

import javax.faces.bean.CustomScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean(name = AccountingNavigationModel.BEAN_NAME)
@CustomScoped(value = "#{window}")
public class AccountingNavigationModel implements Serializable {

	/** JRE generated */
	private static final long serialVersionUID = -7816355282527220055L;
	/** Name of bean */
	public static final String BEAN_NAME = "accountingNavigationModel";

	private String currentPage = "index";

	/** Links found under the Manual Deposits under the Accounting menu */
	private static String[][] manualDepositsMenuLinks = { 
		{ "accounting/ManualDeposits/AccountingIndex.html", "menu.accounting.deposits" } };

	/**
	 * @return the manualDepositsMenuLinks
	 */
	public String[][] getManualDepositsMenuLinks() {
		return manualDepositsMenuLinks;
	}

	public String getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}
}

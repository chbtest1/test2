package ca.medavie.nspp.audit.ui.navmodel;

import java.io.Serializable;

import javax.faces.bean.CustomScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean(name = DSSNavigationModel.BEAN_NAME)
@CustomScoped(value = "#{window}")
public class DSSNavigationModel implements Serializable {

	/** JRE generated */
	private static final long serialVersionUID = -3181049822398638528L;
	/** Name of bean */
	public static final String BEAN_NAME = "dssNavigationModel";

	private String currentPage = "index";

	private static String[][] dssMenuLinks = {
			{ "dss/DssClaimsHistoryTransfer/DssClaimsHistoryTransferIndex.html", "menu.dss.claim.history" },
			{ "dss/DssUnitValue/DssUnitValueIndex.html", "menu.dss.unit.value" },
			{ "dss/DssConstants/DssConstantsIndex.html", "menu.dss.constants" },
			{ "dss/DssChequeReconciliations/DssChequeReconciliationIndex.html", "menu.dss.cheque.reconciliation" },
			{ "dss/DssGLNumbers/DssGLNumberIndex.html", "menu.dss.gl.numbers" } };

	/**
	 * @return the currentPage
	 */
	public String getCurrentPage() {
		return currentPage;
	}

	/**
	 * @param currentPage
	 *            the currentPage to set
	 */
	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}

	/**
	 * @return the dssClaimsHistoryMenuLinks
	 */
	public String[][] getDssMenuLinks() {
		return dssMenuLinks;
	}
}

/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 2015 Medavie BlueCross. All rights reserved.
 * 
 * The contents of this file are subject to the Medavie Blue Cross
 * Application Code terms of use policy, for more information contact
 * Medavie Blue Cross.
 */
package ca.medavie.nspp.audit.ui.bean;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.icefaces.ace.component.datatable.DataTable;
import org.icefaces.ace.model.table.RowState;
import org.icefaces.ace.model.table.RowStateMap;
import org.icefaces.util.JavaScriptRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.exception.NSPPWebException;
import ca.medavie.nspp.audit.security.RoleBasedSecurityUtil;

/**
 * Root Bean for all Controllers.
 */
public abstract class BaseBackingBean {

	/** Class logger */
	private static final Logger logger = LoggerFactory.getLogger(BaseBackingBean.class);

	/** Property file path and help classes */
	private static final String file = "ca/medavie/nspp/audit/ui/ejb_lookup.properties";
	private static InputStream input;
	protected static Properties prop;

	/**
	 * Load the properties file that contains EJB service paths and configuration
	 */
	static {

		prop = new Properties();
		try {
			input = BaseBackingBean.class.getClassLoader().getResourceAsStream(file);
			if (input == null) {
				logger.error("Sorry, unable to find " + file);
			}

			// load a properties file from class path, inside static method
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		logger.debug("EJB Look Up propertie file loaded in BaseBackingBean");
	}

	/**
	 * 
	 */
	public BaseBackingBean() {
	}

	public Map<String, SelectItem[]> getDropDown() {
		// todo make this good
		Map<String, SelectItem[]> toRet = new HashMap<String, SelectItem[]>();

		SelectItem[] providerTypes = new SelectItem[2];
		providerTypes[0] = new SelectItem();
		providerTypes[0].setDescription("RX");
		providerTypes[0].setLabel("RX");
		providerTypes[0].setValue(1);

		providerTypes[1] = new SelectItem();
		providerTypes[1].setDescription("AB");
		providerTypes[1].setLabel("AB");
		providerTypes[1].setValue(2);

		toRet.put("PROVIDERTYPE", providerTypes);

		return toRet;
	}

	/**
	 * Determines if the currently logged in user can perform a particular action. This returns a Map based on the
	 * current user's roles and the contents of the access-config.properties file found on the classpath.
	 * 
	 * The Map has: Key: an action name that can be done on the screen Value: True or False. This defaults to 'False'
	 * for any entitlements not found in access-config.properties (in other words, it defaults "closed")
	 * 
	 * An example of how to use this... Let's say your access-config.properties file contains these entries:
	 * PractitionerControllerBean.search=DSSAuditRole PractitionerControllerBean.abc=XYZ,DSSAuditRole
	 * PractitionerControllerBean.xyz=XYZ
	 * 
	 * ...and your user only has the DSSAuditRole
	 * 
	 * Inspecting the above, it's logical that your user should be able to 'search' and 'abc' on the
	 * PractitionerControllerBean.
	 * 
	 * This method will actually return a Map that looks like this: search ==> TRUE abc ==> TRUE anything else ==> FALSE
	 * 
	 * Given this, you can lock-down elements on the UI as follows:
	 * 
	 * SHOULD DISPLAY: <mbs:commandButton value="Search" action="#{providerController.search}"
	 * rendered="#{providerController.canDo['search']}"/>
	 * 
	 * SHOULD NOT DISPLAY: <mbs:commandButton value="DoNotDisplay" rendered="#{providerController.canDo['xyz']}"/>
	 * 
	 * SHOULD DISPLAY: <mbs:commandButton value="ShouldDisplay" rendered="#{providerController.canDo['abc']}"/>
	 * 
	 * @return A Map, as described above.
	 * @throws NSPPWebException
	 */
	public Map<String, Boolean> getCanDo() throws NSPPWebException {
		Map<String, Boolean> toRet = RoleBasedSecurityUtil.generateEntitlementsMap(this.getClass());
		return toRet;
	}

	/**
	 * Closes modal pop ups via JavaScript. Used in conjunction with setting display boolean to false in controller bean
	 */
	public void closePopupWindowJS() {
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "hidePopupModal()");
	}

	public void clearForm() {
		UIViewRoot root = FacesContext.getCurrentInstance().getViewRoot();
		if (root != null) {
			resetForm(root);
		}
	}

	/**
	 * Should reset all form values
	 * 
	 * @param obj
	 *            Parent component, a form for example
	 */
	private void resetForm(UIComponent obj) {
		if (obj == null || !(obj instanceof UIComponent)) {
			return;
		}
		Iterator<UIComponent> chld = ((UIComponent) obj).getFacetsAndChildren();
		while (chld.hasNext()) {
			resetForm(chld.next());
		}
		if (obj instanceof UIInput) {
			((UIInput) obj).setSubmittedValue(null);
			((UIInput) obj).setValue(null);
			((UIInput) obj).setLocalValueSet(false);
			((UIInput) obj).resetValue();
		}
	}

	/**
	 * Generic method used to select all rows in the provided table
	 * 
	 * @param aTableId
	 *            - ID of the table the select all action was requested on
	 * 
	 * @param aRowStateMap
	 *            - The stateMap that represents the table the select all action is being requested for
	 * 
	 * @param aListOfObjectsInTable
	 *            - This is the list of objects the table is displaying. For example: list of search results
	 */
	@SuppressWarnings("unchecked")
	public void selectAllTableRows(String aTableId, RowStateMap aRowStateMap, List<?> aListOfObjectsInTable) {

		// Get a reference to the table
		UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
		DataTable table = (DataTable) viewRoot.findComponent(aTableId);

		// Continue long as table is found.. avoids app crashing error if a typo in the ID was made
		if (table != null) {

			// Determine if the user set any data filters
			boolean filtersSet = table.getFilteredData() == null ? false : true;
			if (filtersSet) {

				// Type will never been known..
				List<Object> objects = table.getFilteredData();

				for (Object o : objects) {
					RowState row = aRowStateMap.get(o);
					row.setSelected(true);
				}
			} else {
				// No filters set.. Select all rows
				for (Object o : aListOfObjectsInTable) {
					aRowStateMap.get(o).setSelected(true);
				}
			}
		}
	}

	/**
	 * Generic method used to un-select all rows in the provided table
	 * 
	 * @param aTableId
	 *            - ID of the table the un-select all action was requested on
	 * 
	 * @param aRowStateMap
	 *            - The stateMap that represents the table the un-select all action is being requested for
	 * 
	 * @param aListOfObjectsInTable
	 *            - This is the list of objects the table is displaying. For example: list of search results
	 */
	@SuppressWarnings("unchecked")
	public void unselectAllTableRows(String aTableId, RowStateMap aRowStateMap, List<?> aListOfObjectsInTable) {
		// Get a reference to the table
		UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
		DataTable table = (DataTable) viewRoot.findComponent(aTableId);

		// Continue long as table is found.. avoids app crashing error if a typo in the ID was made
		if (table != null) {

			// Determine if the user set any data filters
			boolean filtersSet = table.getFilteredData() == null ? false : true;
			if (filtersSet) {

				// Type will never been known..
				List<Object> objects = table.getFilteredData();

				for (Object o : objects) {
					RowState row = aRowStateMap.get(o);
					row.setSelected(false);
				}
			} else {
				// No filters set.. De-Select all rows
				for (Object o : aListOfObjectsInTable) {
					aRowStateMap.get(o).setSelected(false);
				}
			}
		}
	}

	/**
	 * Generic method used to reset the state of the specified DataTable. Clears filter values, page number active,
	 * sorting
	 * 
	 * @param aDataTableId
	 */
	public void resetDataTableState(String aDataTableId) {

		if (aDataTableId != null && !aDataTableId.isEmpty()) {
			UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
			DataTable table = (DataTable) viewRoot.findComponent(aDataTableId);

			if (table != null) {
				table.resetFilters();
				table.resetPagination();
				table.resetSorting();
			}
		}
	}
}

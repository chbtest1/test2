package ca.medavie.nspp.audit.ui.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.model.SelectItem;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.DropDownMenuItemService;
import ca.medavie.nspp.audit.service.data.CodeEntry;
import ca.medavie.nspp.audit.service.data.PractitionerProfilePeriod;
import ca.medavie.nspp.audit.service.data.SpecialtyCriteria;
import ca.medavie.nspp.audit.service.data.SubcategoryType;
import ca.medavie.nspp.audit.service.exception.DropDownMenuItemServiceException;
import ca.medavie.nspp.audit.ui.controller.validator.Validator;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorMessage;

/**
 * Responsible for loading all data for dropdowns
 */
public class DropdownConstantsBean extends BaseBackingBean {

	@EJB
	protected DropDownMenuItemService dropDownMenuItemService;

	private static final Logger logger = LoggerFactory.getLogger(DropdownConstantsBean.class);
	/** constant SelectItem list for SubcategoryType dropdown */
	private Map<String, SubcategoryType> subcategoryDropdownItems;
	/** constant SelectItem list for SpecialtyCriteria dropdown */
	private Map<String, SpecialtyCriteria> specialtyDropdownItems;
	/** constant SelectItem list for Audit type dropdown */
	private Map<String, CodeEntry> auditRXTypes;
	/** constant SelectItem list for Non Audit type dropdown */
	private Map<String, CodeEntry> auditNonRXTypes;
	/** constant SelectItem list for ContractTown dropdown */
	private List<String> contractTownNames;
	/** constant SelectItem list Qualifiers dropdown */
	private Map<String, String> qualifiers;
	/** constant SelectItem list for auditSourceMed type dropdown */
	private Map<String, String> auditSourceMed;
	/** constant SelectItem list for auditSourcePharm type dropdown */
	private Map<String, String> auditSourcePharm;
	/** constant SelectItem list for auditSourcePharm type dropdown */
	private Map<String, String> auditPharmTypeCodes;
	/** constant SelectItem list for provider group type dropdown */
	private Map<String, String> groupTypeCodes;
	/** constant SelectItem list for ProfilePeriod YearEndDate dropdown */
	private Map<String, String> profilePeriodYearEndDates;
	/** constant SelectItem list for ProfilerPeriods dropdown */
	private Map<Long, PractitionerProfilePeriod> profilePeriods;
	/** constant SelectItem list for muvTypes dropdown */
	private Map<String, String> muvTypes;
	/** constant SelectItem list for muvLevels dropdown */
	private Map<String, String> muvLevels;
	/** constant SelectItem list for HealthService Modifiers */
	private Map<String, String> hsModifiers;
	/** constant SelectItem list for HealthService Implicit Modifiers */
	private Map<String, String> hsImplicitModifiers;
	/** constant SelectItem list for Program dropdown */
	private Map<String, String> programs;
	/** constant SelectItem list for HealthServiceGroup dropdown */
	private Map<Long, String> hsGroups;
	/** constant SelectItem list for HealthServiceQualifiers */
	private Map<String, String> hsQualifiers;
	/** constant SelectItem list for PayeeTypes */
	private Map<String, String> payeeTypes;
	/** constant SelectItem list for PaymentResponsibilities */
	private Map<String, String> paymentResponsibilities;
	/** constant SelectItem list for dss unit value level dropdown */
	private Map<Long, Long> dssUnitValueLevels;
	/** constant SelectItem list for dss unit descriptions(value) and codes(key) dropdown */
	private Map<String, Long> dssUnitValueDescs;
	/** constant SelectItem list for gl Numbers */
	private Map<String, String> glNumbers;
	/** constant SelectItem list for Denticare Audit type dropdown */
	private Map<String, String> denticareAuditTypes;
	/** constant SelectItem list for Medicare Audit type dropdown */
	private Map<String, String> medicareAuditTypes;
	/** constant SelectItem list for Pharmacare Audit type dropdown */
	private Map<String, String> pharmacareAuditTypes;
	/** constant SelectItem list for responds status dropdown */
	private Map<String, String> responseStatus;
	/** constant SelectItem list for Audit Criteria Types (Medicare and Denticare) */
	private Map<String, String> generalAuditCriteriaTypes;
	/** constant SelectItem list for Pharmacare Audit Criteria Types */
	private Map<String, String> pharmacareAuditCriteriaTypes;
	/** Simple selectItem list containing boolean values */
	private List<SelectItem> booleanAnswerTypes;
	/** Simple selectItem list containing 'Y' for yes, and 'N' for no */
	private List<SelectItem> charBooleanAnswerTypes;
	/** constant SelectItem list for health service effective from date dropdown */
	private Map<String, String> hsEffectiveDateDropDown;

	/**
	 * Bean constructor
	 */
	public DropdownConstantsBean() {
		if (this.dropDownMenuItemService == null) {
			try {
				this.dropDownMenuItemService = InitialContext.doLookup(prop.getProperty(DropDownMenuItemService.class
						.getName()));
				logger.debug("dropDownMenuItemService is loaded in ConstantDataSetBean");

			} catch (NamingException e) {
				e.printStackTrace();
			}
		}

		// Load Subcategories
		if (subcategoryDropdownItems == null) {
			try {
				subcategoryDropdownItems = dropDownMenuItemService.getSubCategoryTypesDropDown();
				logger.debug("subcategoryDropdownItems is loaded in ConstantDataSetBean");
			} // catch service exceptions
			catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			}
			// catch all other system errors
			catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}

		// Load Specialties
		if (specialtyDropdownItems == null) {
			try {
				specialtyDropdownItems = dropDownMenuItemService.getSpecialtyDropDown();
				logger.debug("specialtyDropdownItems is loaded in ConstantDataSetBean");
			}
			// catch service exceptions
			catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			}
			// catch all other system errors
			catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
	}

	/**
	 * @return Map<String, SubcategoryType> of SubcategoryDropdownItems
	 */
	public Map<String, SubcategoryType> getSubcategoryDropdownItems() {
		return subcategoryDropdownItems;
	}

	/**
	 * @param subcategoryDropdownItems
	 */
	public void setSubcategoryDropdownItems(Map<String, SubcategoryType> subcategoryDropdownItems) {
		this.subcategoryDropdownItems = subcategoryDropdownItems;
	}

	/**
	 * @return Map<String, SpecialtyCriteria> of SpecialtyDropdownItems
	 */
	public Map<String, SpecialtyCriteria> getSpecialtyDropdownItems() {
		return specialtyDropdownItems;
	}

	/**
	 * @param specialtyDropdownItems
	 */
	public void setSpecialtyDropdownItems(Map<String, SpecialtyCriteria> specialtyDropdownItems) {
		this.specialtyDropdownItems = specialtyDropdownItems;
	}

	/**
	 * @return DropDownMenuItemService
	 */
	public DropDownMenuItemService getDropDownMenuItemService() {
		return dropDownMenuItemService;
	}

	/**
	 * @param dropDownMenuItemService
	 */
	public void setDropDownMenuItemService(DropDownMenuItemService dropDownMenuItemService) {
		this.dropDownMenuItemService = dropDownMenuItemService;
	}

	/**
	 * @return Map<String, CodeEntry> of AuditRXTypes
	 */
	public Map<String, CodeEntry> getAuditRXTypes() {

		// Initial auditRXTypes , only load once from service
		if (auditRXTypes == null) {
			try {
				auditRXTypes = dropDownMenuItemService.getAuditRXTypeDropDown();
				logger.debug("auditRXTypes is loaded in ConstantDataSetBean");
			} // catch service exceptions
			catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
			// catch all other system errors
			catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
		}
		return auditRXTypes;
	}

	/**
	 * @param auditRXTypes
	 */
	public void setAuditRXTypes(Map<String, CodeEntry> auditRXTypes) {
		this.auditRXTypes = auditRXTypes;
	}

	/**
	 * @return Map<String, CodeEntry> of AuditNonRXTypes
	 */
	public Map<String, CodeEntry> getAuditNonRXTypes() {

		// Initial Non auditRXTypes , only load once from service
		if (auditNonRXTypes == null) {
			try {
				auditNonRXTypes = dropDownMenuItemService.getAuditNonRXTypeDropDown();
				logger.debug("auditNonRXTypes is loaded in ConstantDataSetBean");
			} // catch service exceptions
			catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
			// catch all other system errors
			catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
		}
		return auditNonRXTypes;
	}

	/**
	 * @param auditNonRXTypes
	 */
	public void setAuditNonRXTypes(Map<String, CodeEntry> auditNonRXTypes) {
		this.auditNonRXTypes = auditNonRXTypes;
	}

	/**
	 * @return Map<String, String> Qualifiers
	 */
	public Map<String, String> getQualifiers() {
		// Initial qulifiers , only load once from service
		if (qualifiers == null) {
			try {
				qualifiers = dropDownMenuItemService.getQualifierDropDown();
				logger.debug("auditRXTypes is loaded in ConstantDataSetBean");
			} // catch service exceptions
			catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
			// catch all other system errors
			catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
		}
		return qualifiers;
	}

	/**
	 * @param qualifiers
	 */
	public void setQualifiers(Map<String, String> qualifiers) {
		this.qualifiers = qualifiers;
	}

	/**
	 * @return Map<String, String> of auditSourceMed
	 */
	public Map<String, String> getAuditSourceMed() {

		// Initial auditSourceMed , only load once from service
		if (auditSourceMed == null) {
			try {
				auditSourceMed = dropDownMenuItemService.getAuditSourceMedCodeDropDown();
				logger.debug("auditSourceMed is loaded in ConstantDataSetBean");
			} // catch service exceptions
			catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
			// catch all other system errors
			catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
		}
		return auditSourceMed;
	}

	/**
	 * @param auditSourceMed
	 */
	public void setAuditSourceMed(Map<String, String> auditSourceMed) {
		this.auditSourceMed = auditSourceMed;
	}

	/**
	 * @return Map<String, String> of AuditSourcePharm
	 */
	public Map<String, String> getAuditSourcePharm() {

		// Initial auditSourceMed , only load once from service
		if (auditSourcePharm == null) {
			try {
				auditSourcePharm = dropDownMenuItemService.getAuditSourcePharmCodeDropDown();
				logger.debug("auditSourcePharm is loaded in ConstantDataSetBean");
			} // catch service exceptions
			catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
			// catch all other system errors
			catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
		}
		return auditSourcePharm;
	}

	/**
	 * @param auditSourcePharm
	 */
	public void setAuditSourcePharm(Map<String, String> auditSourcePharm) {
		this.auditSourcePharm = auditSourcePharm;
	}

	/**
	 * @return Map<String, String> of AuditPharmTypeCodes
	 */
	public Map<String, String> getAuditPharmTypeCodes() {

		// Initial auditSourceMed , only load once from service
		if (auditPharmTypeCodes == null) {
			try {
				auditPharmTypeCodes = dropDownMenuItemService.getAuditPharmTypeCodeDropDown();
				logger.debug("auditSourcePharm is loaded in ConstantDataSetBean");
			} // catch service exceptions
			catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
			// catch all other system errors
			catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
		}
		return auditPharmTypeCodes;
	}

	/**
	 * @param auditPharmTypeCodes
	 */
	public void setAuditPharmTypeCodes(Map<String, String> auditPharmTypeCodes) {
		this.auditPharmTypeCodes = auditPharmTypeCodes;
	}

	public Map<String, String> getGroupTypeCodes() {
		// Initial groupTypeCodes, only load once for service
		if (groupTypeCodes == null) {
			try {
				groupTypeCodes = dropDownMenuItemService.getGroupTypeCodesDropDown();
			} // catch service exceptions
			catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
			// catch all other system errors
			catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
		}
		return groupTypeCodes;
	}

	public void setGroupTypeCodes(Map<String, String> groupTypeCodes) {
		this.groupTypeCodes = groupTypeCodes;
	}

	/**
	 * @return the profilePeriodYearEndDates
	 */
	public Map<String, String> getProfilePeriodYearEndDates() {

		// Initialize profilePeriodYearEndDates, only load once from service
		if (profilePeriodYearEndDates == null) {
			try {
				profilePeriodYearEndDates = dropDownMenuItemService.getPeriodYearEndDateDropdown();
				logger.debug("profilePeriodYearEndDates is loaded in ConstantDataSetBean");
			} // catch service exceptions
			catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
			// catch all other system errors
			catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
		}
		return profilePeriodYearEndDates;
	}

	/**
	 * @param profilePeriodYearEndDates
	 *            the profilePeriodYearEndDates to set
	 */
	public void setProfilePeriodYearEndDates(Map<String, String> profilePeriodYearEndDates) {
		this.profilePeriodYearEndDates = profilePeriodYearEndDates;
	}

	public Map<String, String> getMuvTypes() {
		return muvTypes;
	}

	public void setMuvTypes(Map<String, String> muvTypes) {
		this.muvTypes = muvTypes;
	}

	public Map<String, String> getMuvLevels() {
		return muvLevels;
	}

	public void setMuvLevels(Map<String, String> muvLevels) {
		this.muvLevels = muvLevels;
	}

	public Map<Long, Long> getDssUnitValueLevels() {

		// Initialize profilePeriodYearEndDates, only load once from service
		if (dssUnitValueLevels == null) {
			try {
				dssUnitValueLevels = dropDownMenuItemService.getDssUnitValueLevelDropdown();
				logger.debug("dssUnitValueLevels is loaded in ConstantDataSetBean");
			} // catch service exceptions
			catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
			// catch all other system errors
			catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
		}
		return dssUnitValueLevels;
	}

	public void setDssUnitValueLevels(Map<Long, Long> dssUnitValueLevels) {
		this.dssUnitValueLevels = dssUnitValueLevels;
	}

	public Map<String, Long> getDssUnitValueDescs() {

		// Initialize profilePeriodYearEndDates, only load once from service
		if (dssUnitValueDescs == null) {
			try {
				dssUnitValueDescs = dropDownMenuItemService.getDssUnitValueDescsDropdown();
				logger.debug("dssUnitValueDescs is loaded in ConstantDataSetBean");
			} // catch service exceptions
			catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
			// catch all other system errors
			catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
		}

		return dssUnitValueDescs;
	}

	public void setDssUnitValueDescs(Map<String, Long> dssUnitValueDescs) {
		this.dssUnitValueDescs = dssUnitValueDescs;
	}

	/**
	 * @return the contractTownNames
	 */
	public List<String> getContractTownNames() {
		// Initialize contractTowns if null
		if (contractTownNames == null) {
			try {
				contractTownNames = dropDownMenuItemService.getContractTownNames();
				logger.debug("contractTownNames is loaded in ConstantDataSetBean");
			} // catch service exceptions
			catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
			// catch all other system errors
			catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
		}
		return contractTownNames;
	}

	/**
	 * @param contractTownNames
	 *            the contractTownNames to set
	 */
	public void setContractTownNames(List<String> contractTownNames) {
		this.contractTownNames = contractTownNames;
	}

	/**
	 * @return the profilePeriods
	 */
	public Map<Long, PractitionerProfilePeriod> getProfilePeriods() {

		// Initialize profilePeriods, only load once from service
		if (profilePeriods == null) {
			try {
				profilePeriods = dropDownMenuItemService.getPeriodIdDropdown();
				logger.debug("profilePeriods is loaded in ConstantDataSetBean");
			} // catch service exceptions
			catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			}
			// catch all other system errors
			catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
		return profilePeriods;
	}

	/**
	 * @param profilePeriods
	 *            the profilePeriods to set
	 */
	public void setProfilePeriods(Map<Long, PractitionerProfilePeriod> profilePeriods) {
		this.profilePeriods = profilePeriods;
	}

	/**
	 * @return the hsModifiers
	 */
	public Map<String, String> getHsModifiers() {

		// Initialize hsModifiers, only load once from service
		if (hsModifiers == null) {
			try {
				hsModifiers = dropDownMenuItemService.getHsModifierDropDown();
			} catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			} catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
		return hsModifiers;
	}

	/**
	 * @param hsModifiers
	 *            the hsModifiers to set
	 */
	public void setHsModifiers(Map<String, String> hsModifiers) {
		this.hsModifiers = hsModifiers;
	}

	/**
	 * @return the hsImplicitModifiers
	 */
	public Map<String, String> getHsImplicitModifiers() {

		// Initialize hsImplicitModifiers, only load once from service
		if (hsImplicitModifiers == null) {
			try {
				hsImplicitModifiers = dropDownMenuItemService.getHsImplicitModifiersDropDown();
			} catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			} catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
		return hsImplicitModifiers;
	}

	/**
	 * @param hsImplicitModifiers
	 *            the hsImplicitModifiers to set
	 */
	public void setHsImplicitModifiers(Map<String, String> hsImplicitModifiers) {
		this.hsImplicitModifiers = hsImplicitModifiers;
	}

	/**
	 * @return the programs
	 */
	public Map<String, String> getPrograms() {
		// Initialize programs, only load once from service
		if (programs == null) {
			try {
				programs = dropDownMenuItemService.getHsProgramDropDown();
			} catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			} catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
		return programs;
	}

	/**
	 * @param programs
	 *            the programs to set
	 */
	public void setPrograms(Map<String, String> programs) {
		this.programs = programs;
	}

	/**
	 * @return the hsGroups
	 */
	public Map<Long, String> getHsGroups() {
		// Initialize hsGroups, only load once from service
		if (hsGroups == null) {
			try {
				hsGroups = dropDownMenuItemService.getHsGroupDropDown();
			} catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			} catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
		return hsGroups;
	}

	/**
	 * @param hsGroups
	 *            the hsGroups to set
	 */
	public void setHsGroups(Map<Long, String> hsGroups) {
		this.hsGroups = hsGroups;
	}

	/**
	 * @return the hsQualifiers
	 */
	public Map<String, String> getHsQualifiers() {
		// Initialize hsQualifiers, only load once from service
		if (hsQualifiers == null) {
			try {
				hsQualifiers = dropDownMenuItemService.getHsQualifierDropDown();
			} catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			} catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
		return hsQualifiers;
	}

	/**
	 * @param hsQualifiers
	 *            the hsQualifiers to set
	 */
	public void setHsQualifiers(Map<String, String> hsQualifiers) {
		this.hsQualifiers = hsQualifiers;
	}

	/**
	 * @return the payeeTypes
	 */
	public Map<String, String> getPayeeTypes() {
		// Initialize payeeTypes, only load once from service
		if (payeeTypes == null) {
			try {
				payeeTypes = dropDownMenuItemService.getPayeeTypeDropDown();
			} catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			} catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
		return payeeTypes;
	}

	/**
	 * @param payeeTypes
	 *            the payeeTypes to set
	 */
	public void setPayeeTypes(Map<String, String> payeeTypes) {
		this.payeeTypes = payeeTypes;
	}

	/**
	 * @return the paymentResponsibilities
	 */
	public Map<String, String> getPaymentResponsibilities() {
		// Initialize paymentResponsibilities, only load once from service
		if (paymentResponsibilities == null) {
			try {
				paymentResponsibilities = dropDownMenuItemService.getPaymentResponsibilityDropDown();
			} catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			} catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
		return paymentResponsibilities;
	}

	/**
	 * @param paymentResponsibilities
	 *            the paymentResponsibilities to set
	 */
	public void setPaymentResponsibilities(Map<String, String> paymentResponsibilities) {
		this.paymentResponsibilities = paymentResponsibilities;
	}

	public Map<String, String> getGlNumbers() {
		if (glNumbers == null) {
			try {
				glNumbers = dropDownMenuItemService.getGlNumbers();
			} catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			} catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
		return glNumbers;
	}

	public void setGlNumbers(Map<String, String> glNumbers) {
		this.glNumbers = glNumbers;
	}

	public Map<String, String> getDenticareAuditTypes() {
		// Initialize Denticare Audit Types, only load once from service
		if (denticareAuditTypes == null) {
			try {
				denticareAuditTypes = dropDownMenuItemService.getDenticareAuditTypeDropDown();
			} catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			} catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
		return denticareAuditTypes;
	}

	public void setDenticareAuditTypes(Map<String, String> denticareAuditTypes) {
		this.denticareAuditTypes = denticareAuditTypes;
	}

	public Map<String, String> getResponseStatus() {
		// Initialize Response Status, only load once from service
		if (responseStatus == null) {
			try {
				responseStatus = dropDownMenuItemService.getResponseStatusDropDown();
			} catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			} catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
		return responseStatus;
	}

	public void setResponseStatus(Map<String, String> responseStatus) {
		this.responseStatus = responseStatus;
	}

	/**
	 * @return the medicareAuditTypes
	 */
	public Map<String, String> getMedicareAuditTypes() {
		// Initialize Medicare Audit Types, only load once from service
		if (medicareAuditTypes == null) {
			try {
				medicareAuditTypes = dropDownMenuItemService.getMedicareAuditTypeDropDown();
			} catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			} catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
		return medicareAuditTypes;
	}

	/**
	 * @param medicareAuditTypes
	 *            the medicareAuditTypes to set
	 */
	public void setMedicareAuditTypes(Map<String, String> medicareAuditTypes) {
		this.medicareAuditTypes = medicareAuditTypes;
	}

	/**
	 * @return the pharmacareAuditTypes
	 */
	public Map<String, String> getPharmacareAuditTypes() {
		// Initialize Pharmacare Audit Types, only load once from service
		if (pharmacareAuditTypes == null) {
			try {
				pharmacareAuditTypes = dropDownMenuItemService.getPharmacareAuditTypeDropDown();
			} catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			} catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
		return pharmacareAuditTypes;
	}

	/**
	 * @param pharmacareAuditTypes
	 *            the pharmacareAuditTypes to set
	 */
	public void setPharmacareAuditTypes(Map<String, String> pharmacareAuditTypes) {
		this.pharmacareAuditTypes = pharmacareAuditTypes;
	}

	/**
	 * Inner Enum class that populates the Gender Restriction dropdowns
	 */
	public enum GenderRestrictions {
		MALE("M", "common.label.male"), FEMALE("F", "common.label.female");

		private String genderValue;
		private String genderLabel;

		private GenderRestrictions(String aGenderVal, String aLabel) {
			genderValue = aGenderVal;
			genderLabel = aLabel;
		}

		public String getGenderValue() {
			return genderValue;
		}

		public String getGenderLabel() {
			return genderLabel;
		}
	}

	/**
	 * @return specified Enum values for GenderRestriction
	 */
	public GenderRestrictions[] getGenderRestrictions() {
		return GenderRestrictions.values();
	}

	/**
	 * Inner Enum class that populates the Employee Type dropdowns
	 */
	public enum EmployeeTypes {
		FULL_TIME("F", "common.label.full.time.emp"), PART_TIME("P", "common.label.part.time.emp");

		private String employeeTypeValue;
		private String employeeTypeLabel;

		private EmployeeTypes(String aVal, String aLabel) {
			this.employeeTypeValue = aVal;
			this.employeeTypeLabel = aLabel;
		}

		public String getEmployeeTypeValue() {
			return employeeTypeValue;
		}

		public String getEmployeeTypeLabel() {
			return employeeTypeLabel;
		}
	}

	/**
	 * @return specified Enum values for EmployeeTypes
	 */
	public EmployeeTypes[] getEmployeeTypes() {
		return EmployeeTypes.values();
	}

	/**
	 * Inner Enum class that populates the Manual Deposits search Type dropdown
	 */
	public enum ManualDepositSearchTypes {
		PRACTITIONER_SEARCH("Practitioner", "label.manualDeposits.providerSearch"), GROUP_SEARCH("Group",
				"label.manualDeposits.groupSearch");

		private String manualDepositSearchTypeValue;
		private String manualDepositSearchTypeLabel;

		private ManualDepositSearchTypes(String aVal, String aLabel) {
			this.manualDepositSearchTypeLabel = aLabel;
			this.manualDepositSearchTypeValue = aVal;
		}

		public String getManualDepositSearchTypeValue() {
			return manualDepositSearchTypeValue;
		}

		public String getManualDepositSearchTypeLabel() {
			return manualDepositSearchTypeLabel;
		}
	}

	/**
	 * @return specified Enum values for ManualDepositSearchTypes
	 */
	public ManualDepositSearchTypes[] getManualDepositSearchTypes() {
		return ManualDepositSearchTypes.values();
	}

	public enum ReconciliationStatusTypes {

		OUTSTANDING("O", "label.dss.ck.status.outstanding"), RECONCILED("R", "label.dss.ck.status.reconciled"), STALE(
				"S", "label.dss.ck.status.stale"), VOID("V", "label.dss.ck.status.void"), CANCELLED("C",
				"label.dss.ck.status.cancelled");

		private String statusCode;
		private String statusLabel;

		private ReconciliationStatusTypes(String aStatusCode, String aStatusLabel) {
			statusCode = aStatusCode;
			statusLabel = aStatusLabel;
		}

		/**
		 * @return the statusCode
		 */
		public String getStatusCode() {
			return statusCode;
		}

		/**
		 * @return the statusLabel
		 */
		public String getStatusLabel() {
			return statusLabel;
		}
	}

	/**
	 * @return specified Enum values for ReconciliationStatusTypes
	 */
	public ReconciliationStatusTypes[] getReconciliationStatusTypes() {
		return ReconciliationStatusTypes.values();
	}

	/**
	 * @return the generalAuditCriteriaTypes
	 */
	public Map<String, String> getGeneralAuditCriteriaTypes() {

		// Init GeneralAuditCriteriaType values if null. Only do this once per session
		if (generalAuditCriteriaTypes == null) {
			try {
				generalAuditCriteriaTypes = dropDownMenuItemService.getGeneralAuditCriteriaTypeDropDown();
			} catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			} catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
		return generalAuditCriteriaTypes;
	}

	/**
	 * @param generalAuditCriteriaTypes
	 *            the generalAuditCriteriaTypes to set
	 */
	public void setGeneralAuditCriteriaTypes(Map<String, String> generalAuditCriteriaTypes) {
		this.generalAuditCriteriaTypes = generalAuditCriteriaTypes;
	}

	/**
	 * @return the pharmacareAuditCriteriaTypes
	 */
	public Map<String, String> getPharmacareAuditCriteriaTypes() {

		// Init PharmacareAuditCriteriaType values if null. Only do this once per session.
		if (pharmacareAuditCriteriaTypes == null) {
			try {
				pharmacareAuditCriteriaTypes = dropDownMenuItemService.getPharmacareAuditCriteriaTypeDropDown();
			} catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			} catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
		return pharmacareAuditCriteriaTypes;
	}

	/**
	 * @param pharmacareAuditCriteriaTypes
	 *            the pharmacareAuditCriteriaTypes to set
	 */
	public void setPharmacareAuditCriteriaTypes(Map<String, String> pharmacareAuditCriteriaTypes) {
		this.pharmacareAuditCriteriaTypes = pharmacareAuditCriteriaTypes;
	}

	/**
	 * @return the booleanAnswerTypes
	 */
	public List<SelectItem> getBooleanAnswerTypes() {
		if (booleanAnswerTypes == null || booleanAnswerTypes.isEmpty()) {

			booleanAnswerTypes = new ArrayList<SelectItem>();
			booleanAnswerTypes.add(new SelectItem(false, "No"));
			booleanAnswerTypes.add(new SelectItem(true, "Yes"));
		}
		return booleanAnswerTypes;
	}

	public List<SelectItem> getCharBooleanAnswerTypes() {
		if (charBooleanAnswerTypes == null || charBooleanAnswerTypes.isEmpty()) {
			charBooleanAnswerTypes = new ArrayList<SelectItem>();
			charBooleanAnswerTypes.add(new SelectItem("Y", "Yes"));
			charBooleanAnswerTypes.add(new SelectItem("N", "No"));
		}
		return charBooleanAnswerTypes;
	}

	/**
	 * @return the profilePeriodYearEndDates
	 */
	public Map<String, String> getHsEffectiveDateDropDown() {

		// Initialize profilePeriodYearEndDates, only load once from service
		if (hsEffectiveDateDropDown == null) {
			try {
				hsEffectiveDateDropDown = dropDownMenuItemService.getHSEffectiveDateDropDown();
				logger.debug("hsEffectiveDateDropDown is loaded in ConstantDataSetBean");
			} // catch service exceptions
			catch (DropDownMenuItemServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
			// catch all other system errors
			catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
				return null;
			}
		}
		return hsEffectiveDateDropDown;
	}

	/**
	 * @param booleanAnswerTypes
	 *            the booleanAnswerTypes to set
	 */
	public void setBooleanAnswerTypes(List<SelectItem> booleanAnswerTypes) {
		this.booleanAnswerTypes = booleanAnswerTypes;
	}

	/**
	 * @param charBooleanAnswerTypes
	 *            the charBooleanAnswerTypes to set
	 */
	public void setCharBooleanAnswerTypes(List<SelectItem> charBooleanAnswerTypes) {
		this.charBooleanAnswerTypes = charBooleanAnswerTypes;
	}

}

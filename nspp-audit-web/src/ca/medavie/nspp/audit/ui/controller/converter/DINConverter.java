package ca.medavie.nspp.audit.ui.controller.converter;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 * Clean Strings that will be converted to Integers.
 * 
 */
@FacesConverter("dinConverter")
public class DINConverter implements Converter {
	private final String msgBundleLocation = "ca.medavie.nspp.audit.ui.messages";
	private final static String DINPATTERN = "\\d{1,2}[.]{1}\\d{1,2}";

	public DINConverter() {
		super();
	}

	/**
	 * Clean String so that it can be converted to an Integer.
	 * 
	 * @param aContext
	 *            The FacesContext
	 * @param aComponent
	 *            The UIComponent
	 * @param aString
	 *            The String to convert
	 * 
	 * @return Cleaned String
	 */
	public Object getAsObject(FacesContext aContext, UIComponent aComponent, String aString) {

		// Load the currently used locale
		Locale currentLocale = FacesContext.getCurrentInstance().getViewRoot().getLocale();

		if (aString == null || aString.trim().equals("")) {
			return null;
		}

		if (Pattern.matches(DINPATTERN, aString)) {
			return aString;
		} else {
			ResourceBundle messageBundle = ResourceBundle.getBundle(msgBundleLocation, currentLocale);
			String errorMsg = messageBundle.getString("ERROR.DSS.DIN.INVALID");
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMsg, errorMsg);
			throw new ConverterException(fm);
		}
	}

	/**
	 * Convert an Integer to a string
	 * 
	 * @param aContext
	 *            The FacesContext
	 * @param aComponent
	 *            The UIComponent
	 * @param anObj
	 *            The Integer to convert
	 * 
	 * @return The String, converted from the Integer.
	 */
	public String getAsString(FacesContext aContext, UIComponent aComponent, Object anObj) {
		// Load the currently used locale
		Locale currentLocale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		String aValueStr = anObj.toString();
		if (Pattern.matches(DINPATTERN, aValueStr)) {
			return aValueStr;
		} else {
			ResourceBundle messageBundle = ResourceBundle.getBundle(msgBundleLocation, currentLocale);
			String errorMsg = messageBundle.getString("ERROR.DSS.DIN.INVALID");
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMsg, errorMsg);
			throw new ConverterException(fm);
		}
	}
}
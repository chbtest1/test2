package ca.medavie.nspp.audit.ui.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.icefaces.ace.model.table.LazyDataModel;
import org.icefaces.ace.model.table.RowStateMap;
import org.icefaces.ace.model.table.SortCriteria;
import org.icefaces.util.JavaScriptRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.HealthServiceGroupService;
import ca.medavie.nspp.audit.service.data.DataUtilities;
import ca.medavie.nspp.audit.service.data.HealthServiceCode;
import ca.medavie.nspp.audit.service.data.HealthServiceCodeSearchCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceCodeSearchCriteria.HealthServiceCodeGroupFilters;
import ca.medavie.nspp.audit.service.data.HealthServiceGroup;
import ca.medavie.nspp.audit.service.data.HealthServiceGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceGroupSearchCriteria.HealthServiceGroupSearchTypes;
import ca.medavie.nspp.audit.service.data.HealthServiceProgram;
import ca.medavie.nspp.audit.service.data.Modifier;
import ca.medavie.nspp.audit.service.exception.HealthServiceGroupServiceException;
import ca.medavie.nspp.audit.ui.FacesUtils;
import ca.medavie.nspp.audit.ui.bean.DropdownConstantsBean;
import ca.medavie.nspp.audit.ui.controller.validator.HealthServiceGroupValidator;
import ca.medavie.nspp.audit.ui.controller.validator.Validator;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorException;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorMessage;

@ManagedBean(name = "healthServiceGroupsController")
@ViewScoped
public class HealthServiceGroupsControllerBean extends DropdownConstantsBean implements Serializable {

	/** IDE generated */
	private static final long serialVersionUID = 4981436592673399582L;

	/** Class logger */
	private static final Logger logger = LoggerFactory.getLogger(HealthServiceGroupsControllerBean.class);

	// Holds reference to the EJB Client service class
	@EJB
	protected HealthServiceGroupService healthServiceGroupsLocalService;

	/** ********************************************** */
	/** Edit/Add Health Service Group screen variables */
	/** ********************************************** */
	/** Reference the currently selected HealthServiceGroup */
	private HealthServiceGroup selectedHealthServiceGroup;
	/** Reference to a new HealthServiceGroup record.. This is simply used to keep track of values for a new record */
	private HealthServiceGroup newHealthServiceGroup;

	/** Holds all user specified inputs for searching HealthServiceGroups */
	private HealthServiceGroupSearchCriteria healthServiceGroupSearchCriteira;

	/** Holds all user specified inputs for searching HealthServices */
	private HealthServiceCodeSearchCriteria healthServiceCodeSearchCriteria;

	/** Holds the search results from the Health Service Group search */
	private LazyDataModel<HealthServiceGroup> healthServiceGroupSearchResults;
	private Integer totalHealthServiceGroups;
	/** Holds the search results from the Health Service Code search */
	private List<HealthServiceCode> healthServiceCodeSearchResults;

	/** Keeps track of the selected HealthServiceCodes for delete */
	private RowStateMap healthServiceCodesInGroupStateMap;
	/** Keeps track of the selected HealthServiceCodes to be added to the Group */
	private RowStateMap healthServiceCodeResultsStateMap;

	/** Booleans that keep track of pop up displays */
	private boolean displayHSGSearchResults;
	private boolean displayHSCodeSearchResults;
	private boolean displayDeleteDHSCConfirmation;

	/** List containing the HealthServiceCodes selected for add */
	private List<HealthServiceCode> hscForAdd;

	/** Holds objects for select drop down menus */
	private List<HealthServiceProgram> programDropdownItems;
	private List<Modifier> modifierDropdownItems;
	private List<Modifier> implicitModifierDropdownItems;

	/** Validators */
	private HealthServiceGroupValidator healthServiceGroupValidator;

	/** ******************************************* */
	/** View Services not in group screen variables */
	/** ******************************************* */
	/**
	 * Used for holding state during the transfer to enable download of summary
	 */
	@ManagedProperty(value = "#{fileDownloadController}")
	private FileDownloadControllerBean fileDownloadControllerBean;

	/** Holds all the HealthServiceCodes that are currently not in a group */
	private List<HealthServiceCode> healthServiceCodesNotInGroup;

	/** Holds the data selected for viewing Health Service Codes not in group */
	private String selectedYearEndDateForHSCView;

	/** Boolean that controls the display of the table showing Services not in group */
	private boolean displayServicesNotInGroup;

	/** Filter values **/
	private String filterProgramCode;
	private String filterHealthServiceCode;
	private String filerModifiers;
	private String filterImplicitModifiers;
	private String filterQualifiers;
	private String filterCategoryCode;
	private String filterUnitFormula;

	/**
	 * Constructor Sets up all bean reference with EJB client layer.
	 */
	public HealthServiceGroupsControllerBean() {

		if (this.healthServiceGroupsLocalService == null) {
			try {
				this.healthServiceGroupsLocalService = InitialContext.doLookup(prop
						.getProperty(HealthServiceGroupService.class.getName()));
				logger.debug("healthServiceGroupsLocalService is loaded in the constructor");
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}

		// Populate program list if needed
		if (programDropdownItems == null || programDropdownItems.isEmpty()) {
			try {
				programDropdownItems = healthServiceGroupsLocalService.getHealthServicePrograms();
			}
			// catch service exceptions
			catch (HealthServiceGroupServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
				return;
			}
			// catch all other system errors
			catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
				return;
			}
		}

		// Populate the modifier dropdowns if needed
		if (modifierDropdownItems == null || modifierDropdownItems.isEmpty()) {
			try {
				modifierDropdownItems = healthServiceGroupsLocalService.getModifiers();
			} // catch service exceptions
			catch (HealthServiceGroupServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
				return;
			}
			// catch all other system errors
			catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
				return;
			}
		}
		if (implicitModifierDropdownItems == null || implicitModifierDropdownItems.isEmpty()) {
			try {
				implicitModifierDropdownItems = healthServiceGroupsLocalService.getImplicitModifiers();
			}
			// catch service exceptions
			catch (HealthServiceGroupServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			}
			// catch all other system errors
			catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}

		// Instantiate validator
		if (healthServiceGroupValidator == null) {
			healthServiceGroupValidator = new HealthServiceGroupValidator();
		}

		// Trigger Search Edit HS init since tab is displayed on load
		initiateSearchEditHSGroupsPanel();

		// Sync HSG search results data model with bean
		healthServiceGroupSearchResults = performHSGSearchLoad();
	}

	/**
	 * Clears save search type, clear entity bean, set search type back to original, clear Faces components.
	 */
	public void executeResetSearchHealthServiceGroupSearch() {
		HealthServiceGroupSearchTypes searchType = healthServiceGroupSearchCriteira.getSearchTypeSelection();
		healthServiceGroupSearchCriteira = new HealthServiceGroupSearchCriteria();
		healthServiceGroupSearchCriteira.setSearchTypeSelection(searchType);
		displayHSGSearchResults = false;
		healthServiceCodeSearchCriteria.resetSearchCriteria();
		// Set Program back to MC by default
		healthServiceCodeSearchCriteria.setProgram("MC");
		clearForm();

		// Hide search results
		displayHSGSearchResults = false;
	}

	public void healthServiceTabChangeListener(ValueChangeEvent anEvent) {
		// Get the index of the requested tab
		Integer requestedTabIndex = (Integer) anEvent.getNewValue();

		// Prepare backing bean for the requested tab
		if (requestedTabIndex.equals(Integer.valueOf("0"))) {
			initiateSearchEditHSGroupsPanel();
		} else if (requestedTabIndex.equals(Integer.valueOf("1"))) {
			initiateCreateNewHSGroupPanel();
		} else if (requestedTabIndex.equals(Integer.valueOf("2"))) {
			initiateViewServicesNotInGroupPanel();
		}
	}

	/**
	 * Takes the user to the Search/Edit Health Service Groups panel.
	 */
	private void initiateSearchEditHSGroupsPanel() {

		if (healthServiceCodesNotInGroup != null) {
			healthServiceCodesNotInGroup.clear();
		}

		// Ensure brand new search Criteria is set and ready
		healthServiceGroupSearchCriteira = new HealthServiceGroupSearchCriteria();
		healthServiceCodeSearchCriteria = new HealthServiceCodeSearchCriteria();
		selectedHealthServiceGroup = null;
		
		// Ensure search results are in fresh state
		displayHSGSearchResults = false;
	}

	/**
	 * Takes the user to the Create New Health Service Group panel.
	 */
	private void initiateCreateNewHSGroupPanel() {
		// Ensure brand new HealthServiceGroup is set and ready
		newHealthServiceGroup = new HealthServiceGroup();
	}

	/**
	 * Takes the user to the View Services not in group panel. This is a simple table display so data is loaded on
	 * click.
	 */
	private void initiateViewServicesNotInGroupPanel() {
		// Clear out previously selected YearEndDate
		selectedYearEndDateForHSCView = null;
		// Hide the services table
		displayServicesNotInGroup = false;
		// Ensure the services table is in default state
		resetDataTableState("viewServicesNotInGroupPanel:viewServicesNotInGroupForm:viewServicesNotInGroupTable");
	}

	/**
	 * Search for HealthServiceGroups using the provided search criteria
	 */
	public void executeHealthServiceGroupSearch() {
		try {
			// Perform search results count
			totalHealthServiceGroups = healthServiceGroupsLocalService
					.getSearchHealthServiceGroupsCount(healthServiceGroupSearchCriteira);
			healthServiceGroupSearchResults.setRowCount(totalHealthServiceGroups);

			// Reset search results table state
			resetDataTableState("editHSGroupsView:searchHSGroupPanel:healthServiceSearchForm:hsgSearchResultsTable");

			// Display search results, data model refreshed automatically
			displayHSGSearchResults = true;

		} catch (HealthServiceGroupServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Loads the matching Health Service Group records based on the search criteria
	 * 
	 * @return Health Service Group records matching the user search criteria
	 */
	private LazyDataModel<HealthServiceGroup> performHSGSearchLoad() {
		healthServiceGroupSearchResults = new LazyDataModel<HealthServiceGroup>() {

			/** IDE Generated */
			private static final long serialVersionUID = -5568320985046577897L;

			@Override
			public List<HealthServiceGroup> load(int first, int pageSize, final SortCriteria[] criteria,
					final Map<String, String> filters) {

				List<HealthServiceGroup> results = new ArrayList<HealthServiceGroup>();

				try {
					if (displayHSGSearchResults) {

						// Apply any sorting to search criteria
						if (criteria.length > 0) {

							for (int x = 0; x < criteria.length; x++) {
								SortCriteria sc = criteria[x];
								healthServiceGroupSearchCriteira.addSortArgument(HealthServiceGroup.class,
										sc.getPropertyName(), sc.isAscending());
							}
						}

						// Load record matching search results
						results = healthServiceGroupsLocalService.getSearchHealthServiceGroups(
								healthServiceGroupSearchCriteira, first, pageSize);
					}
				} catch (HealthServiceGroupServiceException e) {
					Validator.displayServiceError(e);
					e.printStackTrace();
					logger.error(e.getMessage());

				} catch (Exception e) {
					Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.APPLICATION.FAILURE"));
					e.printStackTrace();
					logger.error(e.getMessage());
				}
				// Return results. If nothing found an empty list is returned
				return results;
			}
		};
		return healthServiceGroupSearchResults;
	}

	/**
	 * Search for HealthServiceCodes using the provided search criteria.
	 * 
	 * Used for adding Health Services
	 */
	public void executeHealthServiceCodeSearch() {

		try {
			// Validate search criteria
			healthServiceGroupValidator.validateHealthServiceCodeSearch(healthServiceCodeSearchCriteria);

			// Begin the search call if criteria is valid
			healthServiceCodeSearchResults = healthServiceGroupsLocalService
					.getSearchHealthServiceCodes(healthServiceCodeSearchCriteria);
			// Display results
			displayHSCodeSearchResults = true;

			// Remove any HSC records that already exist in the Group
			for (HealthServiceCode hsc : selectedHealthServiceGroup.getHealthServiceCodes()) {
				if (healthServiceCodeSearchResults.contains(hsc)) {
					healthServiceCodeSearchResults.remove(hsc);
				}
			}

		} catch (ValidatorException ve) {
			Validator.displayValidationErrors(ve);
		} catch (HealthServiceGroupServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Saves the changes made to the HealthServiceGroup.
	 * 
	 */
	public void executeSaveHealthServiceGroup() {

		try {
			// Validate the HealthServiceGroup
			healthServiceGroupValidator.validate(selectedHealthServiceGroup);

			// Set the edit meta data
			selectedHealthServiceGroup.setLastModified(new Date());
			selectedHealthServiceGroup.setModifiedBy(FacesUtils.getUserId());

			// Attempt saving changes
			healthServiceGroupsLocalService.saveHealthServiceGroup(selectedHealthServiceGroup, hscForAdd);
			// Display success message
			Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.HS.GROUPS.SAVE.SUCCESSFUL");
			// Clear out list holding codes to add
			hscForAdd.clear();

		} catch (ValidatorException ve) {
			Validator.displayValidationErrors(ve);
		} catch (HealthServiceGroupServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Saves a new Health Service Group.
	 */
	public void executeSaveNewHealthServiceGroup() {

		try {
			// Validate the HealthServiceGroup
			healthServiceGroupValidator.validate(newHealthServiceGroup);

			// Perform a quick duplicate check with DB
			if (healthServiceGroupsLocalService.isHealthServiceGroupDuplicate(newHealthServiceGroup
					.getHealthServiceGroupId())) {

				// Duplicate record detected.. Display error to user
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.HS.GROUPS.DUPLICATE.GROUP"));
			} else {
				// Apply edit meta user and date
				newHealthServiceGroup.setModifiedBy(FacesUtils.getUserId());
				newHealthServiceGroup.setLastModified(new Date());

				healthServiceGroupsLocalService.saveNewHealthServiceGroup(newHealthServiceGroup);

				// Display success message
				Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.HS.GROUPS.SAVE.SUCCESSFUL");

				// Reset the HealthServiceGroup object
				newHealthServiceGroup = new HealthServiceGroup();
			}
		} catch (ValidatorException ve) {
			Validator.displayValidationErrors(ve);
		} catch (HealthServiceGroupServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Using the specified Year End Date load all the Services not in a Group
	 */
	public void executeViewServiceNotInGroup() {
		if (selectedYearEndDateForHSCView != null) {
			// Attempt to load the ;data
			try {
				Date selectedYearEndDate = DataUtilities.SDF.parse(selectedYearEndDateForHSCView);

				healthServiceCodesNotInGroup = healthServiceGroupsLocalService
						.getServicesNotInGroup(selectedYearEndDate);

				// Reset the fileDownloadController bean list so we don't end up with duplicates in excel
				if (fileDownloadControllerBean.getFilteredHSCs() != null) {
					fileDownloadControllerBean.getFilteredHSCs().clear();
				}

				// add data to the export object
				fileDownloadControllerBean.getFilteredHSCs().addAll(healthServiceCodesNotInGroup);

				// Display the data table
				displayServicesNotInGroup = true;

			} // catch service exceptions
			catch (HealthServiceGroupServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			}
			// catch all other system errors
			catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		} else {
			// Display error stating YearEndDate is required before continuing
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.YEAR.END.DATE.REQUIRED"));
		}
	}

	/**
	 * Returns the user to the search form for Health Service Groups
	 */
	public void executeReturnToHealthServiceSearch() {
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "returnToHealthServiceSearch()");
	}

	/**
	 * Clears out any user input values on the Create New HealthServiceGroup
	 */
	public void executeResetAddHealthServiceGroupForm() {
		newHealthServiceGroup.setHealthServiceGroupName("");
		newHealthServiceGroup.setHealthServiceGroupId(null);
	}

	/**
	 * Loads all details of the specified HealthServiceGroup and takes user to the HealthServiceGroup details page
	 * 
	 * @param aHealthServiceGroup
	 *            - the selected HealthServiceGroup
	 */
	public void initiateEditHealthServiceGroup(HealthServiceGroup aHealthServiceGroup) {

		try {
			selectedHealthServiceGroup = aHealthServiceGroup;

			// Load the groups HSC
			Long hsGroupId = selectedHealthServiceGroup.getHealthServiceGroupId();
			selectedHealthServiceGroup.setHealthServiceCodes(healthServiceGroupsLocalService
					.getHealthServiceCodes(hsGroupId));

			// Ensure search criteria for HealthServiceCodes is empty
			healthServiceCodeSearchCriteria.resetSearchCriteria();

			// Clear HSC record lists
			hscForAdd = new ArrayList<HealthServiceCode>();

			// Clear state map. (Known ICEfaces bug)
			healthServiceCodesInGroupStateMap.clear();
			healthServiceCodeResultsStateMap.clear();

			// Display the HealthService Group details and minimize the search panel
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayHealthServiceDetails()");

			// Ensure Health Service Codes table is in default state
			resetDataTableState("searchHSGroupPanel:healthServiceCodeForm:hsGroupCodesTable");

		} // catch service exceptions
		catch (HealthServiceGroupServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Prompts the user to confirm the HSC delete action
	 */
	public void deleteSelectedHSCFromGroup() {

		// Only display confirmation if rows are selected
		if (!healthServiceCodesInGroupStateMap.getSelected().isEmpty()) {
			displayDeleteDHSCConfirmation = true;
		} else {
			// Display error to user
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.HS.GROUPS.NONE.SELECTED.DELETE"));
		}
	}

	/**
	 * Deletes the selected HeathServiceCodes from the active Group following confirmation
	 */
	public void confirmDeleteSelectedHSCFromGroup() {

		// Delete the selected records from the system
		try {
			@SuppressWarnings("unchecked")
			List<HealthServiceCode> hscForDelete = healthServiceCodesInGroupStateMap.getSelected();

			healthServiceGroupsLocalService.deleteHealthServiceCodesFromGroup(
					selectedHealthServiceGroup.getHealthServiceGroupId(), hscForDelete);

			// Upon successful delete remove the objects from UI
			for (HealthServiceCode hsc : hscForDelete) {
				// Remove the HSC from the groups list
				selectedHealthServiceGroup.getHealthServiceCodes().remove(hsc);
				// Remove HSC from the add list
				hscForAdd.remove(hsc);
			}
			// Clear the state map and close the confirmation window
			healthServiceCodesInGroupStateMap.clear();
			displayDeleteDHSCConfirmation = false;
			closePopupWindowJS();

			// Display success message
			Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.HS.GROUPS.DELETE.SUCCESSFUL");

		} catch (HealthServiceGroupServiceException e) {
			e.printStackTrace();
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.HS.GROUPS.DELETE.FAILED"));
		}// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Adds the selected HealthServcieCodes (Selected in search results popup) to the active group.
	 */
	public void addSelectedHSCToGroup() {

		// Only continue if there is selected records... Display warning if none.
		if (!healthServiceCodeResultsStateMap.getSelected().isEmpty()) {

			@SuppressWarnings("unchecked")
			List<HealthServiceCode> codesToAdd = healthServiceCodeResultsStateMap.getSelected();

			// Used to determine if there is an error on add.. prevent the pop up from closing
			boolean duplicateErrorDetected = false;

			// Add new HealthServiceCodes to the HealthServiceGroup
			for (HealthServiceCode code : codesToAdd) {

				// Need to determine if this is a duplicate
				if (healthServiceCodeDuplicateCheck(code)) {
					// Only add the duplicate error to UI once
					if (!duplicateErrorDetected) {
						// Display error to user
						Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
								"ERROR.HS.GROUPS.DUPLICATE.CODE"));
					}
					duplicateErrorDetected = true;
				} else {
					// Set the modified by and last values
					code.setLastModified(new Date());
					code.setModifiedBy(FacesUtils.getUserId());

					selectedHealthServiceGroup.getHealthServiceCodes().add(0, code);
					// Add to the New HSC list
					hscForAdd.add(code);
				}
			}
			// Only close pop up when no errors occurred
			if (!duplicateErrorDetected) {
				// Clear state map
				healthServiceCodeResultsStateMap.clear();
				// Close the search results
				closeHSCodeSearchPopup();
			}
		} else {
			// Display warning
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.HS.GROUPS.HSC.NONE.SELECTED"));
		}
	}

	/**
	 * Takes the user to step 1 of Health Service Groups search
	 */
	public void displayHsSearchSelection() {
		healthServiceGroupSearchCriteira = new HealthServiceGroupSearchCriteria();
		displayHSGSearchResults = false;
		healthServiceGroupSearchCriteira.setSearchTypeSelection(null);

		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayHsSearchSelection()");
	}

	/**
	 * Takes the user to step 2 of Health Service Groups search.
	 * 
	 * Based on dropdown selection the appropriate search form will be displayed
	 */
	public void displayHsSearchForm() {
		if (healthServiceGroupSearchCriteira.getSearchTypeSelection().equals(
				HealthServiceGroupSearchTypes.HEALTH_SERVICE_GROUP_SEARCH)) {
			// Searching by Health Service Groups
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayHealthServiceGroupsSearch()");
		} else {
			// Searching by Health Services
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayHealthServicesSearch()");
		}
	}

	/**
	 * Action to filter Total Services Not In Group data to be exported.
	 */
	public void executeProgramFilter() {

		fileDownloadControllerBean.getFilteredHSCs().clear();
		fileDownloadControllerBean.getFilteredHSCs().addAll(healthServiceCodesNotInGroup);

		CollectionUtils.filter(fileDownloadControllerBean.getFilteredHSCs(), new Predicate<HealthServiceCode>() {
			@Override
			public boolean evaluate(HealthServiceCode hsc) {

				boolean programFiltered = (StringUtils.isBlank(filterProgramCode) || hsc.getProgram().toUpperCase()
						.contains(filterProgramCode.toUpperCase())) ? true : false;
				boolean hscFiltered = (StringUtils.isBlank(filterHealthServiceCode) || hsc.getHealthServiceCode()
						.toUpperCase().contains(filterHealthServiceCode.toUpperCase())) ? true : false;
				boolean modifierFiltered = (StringUtils.isBlank(filerModifiers) || hsc.getModifiers().toUpperCase()
						.contains(filerModifiers.toUpperCase())) ? true : false;
				boolean qualifierFiltered = (StringUtils.isBlank(filterQualifiers) || hsc.getQualifier().toUpperCase()
						.contains(filterQualifiers.toUpperCase())) ? true : false;
				boolean categoryFiltered = (StringUtils.isBlank(filterCategoryCode) || hsc.getCategory().toUpperCase()
						.contains(filterCategoryCode.toUpperCase())) ? true : false;
				boolean unitFormulaFiltered = (StringUtils.isBlank(filterUnitFormula) || hsc.getUnitFormula()
						.toUpperCase().contains(filterUnitFormula.toUpperCase())) ? true : false;

				if (programFiltered && hscFiltered && modifierFiltered && qualifierFiltered && categoryFiltered
						&& unitFormulaFiltered) {
					return true;
				}
				return false;
			}
		});
	}

	/**
	 * Checks for duplicate codes
	 * 
	 * @param aHsc
	 *            - Code being added. Checks if this duplicates any codes already in the group
	 * @return true if duplicate
	 */
	private boolean healthServiceCodeDuplicateCheck(HealthServiceCode aHsc) {
		// Scan for duplicate HSC
		for (HealthServiceCode code : selectedHealthServiceGroup.getHealthServiceCodes()) {

			// Duplicate is: Service ID and Effective From date
			if (aHsc.getHealthServiceId().equals(code.getHealthServiceId())
					&& aHsc.getEffectiveFrom().equals(code.getEffectiveFrom())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Closes the Health Service Code search result popup
	 */
	public void closeHSCodeSearchPopup() {

		// Ensure the search results datatable is reset to default state
		resetDataTableState("hsCodeSearchResultsForm:hsCodeSearchResultsPopup:healthServiceCodeResultsDatatable");

		// Set the Health Service Code popup to be hidden
		displayHSCodeSearchResults = false;
		closePopupWindowJS();
	}

	/**
	 * clearHSSearchForm
	 */
	public void clearHSSearchForm() {

		clearForm();
		healthServiceCodeSearchCriteria.resetSearchCriteria();
	}

	/**
	 * Closes the HSC delete confirmation pop up
	 */
	public void closeDeleteHSCConfirmationPopup() {
		displayDeleteDHSCConfirmation = false;
		closePopupWindowJS();
	}

	public HealthServiceGroupSearchTypes[] getHealthServiceGroupSearchTypes() {
		return HealthServiceGroupSearchTypes.values();
	}

	public HealthServiceCodeGroupFilters[] getHealthServiceCodeGroupFilters() {
		return HealthServiceCodeGroupFilters.values();
	}

	/**
	 * Selects all rows in the HSC table. If the user has applied filters on the table the rows removed from view will
	 * not be selected
	 */
	public void executeSelectAllHscs() {
		String tableId = "editHSGroupsView:hsCodeSearchResultsForm:hsCodeSearchResultsPopup:healthServiceCodeResultsDatatable";
		super.selectAllTableRows(tableId, healthServiceCodeResultsStateMap, healthServiceCodeSearchResults);
	}

	/**
	 * De-Selects all rows in the HSC table. If the user has applied filters on the table the rows removed from view
	 * will not be selected
	 */
	public void executeDeselectAllHscs() {
		String tableId = "editHSGroupsView:hsCodeSearchResultsForm:hsCodeSearchResultsPopup:healthServiceCodeResultsDatatable";
		super.unselectAllTableRows(tableId, healthServiceCodeResultsStateMap, healthServiceCodeSearchResults);
	}

	/**
	 * @return the healthServiceGroupSearchCriteira
	 */
	public HealthServiceGroupSearchCriteria getHealthServiceGroupSearchCriteira() {
		return healthServiceGroupSearchCriteira;
	}

	/**
	 * @param healthServiceGroupSearchCriteira
	 *            the healthServiceGroupSearchCriteira to set
	 */
	public void setHealthServiceGroupSearchCriteira(HealthServiceGroupSearchCriteria healthServiceGroupSearchCriteira) {
		this.healthServiceGroupSearchCriteira = healthServiceGroupSearchCriteira;
	}

	/**
	 * @return the programDropdownItems
	 */
	public List<HealthServiceProgram> getProgramDropdownItems() {
		return programDropdownItems;
	}

	/**
	 * @param programDropdownItems
	 *            the programDropdownItems to set
	 */
	public void setProgramDropdownItems(List<HealthServiceProgram> programDropdownItems) {
		this.programDropdownItems = programDropdownItems;
	}

	/**
	 * @return the selectedHealthServiceGroup
	 */
	public HealthServiceGroup getSelectedHealthServiceGroup() {
		return selectedHealthServiceGroup;
	}

	/**
	 * @param selectedHealthServiceGroup
	 *            the selectedHealthServiceGroup to set
	 */
	public void setSelectedHealthServiceGroup(HealthServiceGroup selectedHealthServiceGroup) {
		this.selectedHealthServiceGroup = selectedHealthServiceGroup;
	}

	/**
	 * @return the healthServiceCodeSearchCriteria
	 */
	public HealthServiceCodeSearchCriteria getHealthServiceCodeSearchCriteria() {
		return healthServiceCodeSearchCriteria;
	}

	/**
	 * @param healthServiceCodeSearchCriteria
	 *            the healthServiceCodeSearchCriteria to set
	 */
	public void setHealthServiceCodeSearchCriteria(HealthServiceCodeSearchCriteria healthServiceCodeSearchCriteria) {
		this.healthServiceCodeSearchCriteria = healthServiceCodeSearchCriteria;
	}

	/**
	 * @return the healthServiceCodeSearchResults
	 */
	public List<HealthServiceCode> getHealthServiceCodeSearchResults() {
		return healthServiceCodeSearchResults;
	}

	/**
	 * @param healthServiceCodeSearchResults
	 *            the healthServiceCodeSearchResults to set
	 */
	public void setHealthServiceCodeSearchResults(List<HealthServiceCode> healthServiceCodeSearchResults) {
		this.healthServiceCodeSearchResults = healthServiceCodeSearchResults;
	}

	/**
	 * @return the displayHSCodeSearchResults
	 */
	public boolean isDisplayHSCodeSearchResults() {
		return displayHSCodeSearchResults;
	}

	/**
	 * @param displayHSCodeSearchResults
	 *            the displayHSCodeSearchResults to set
	 */
	public void setDisplayHSCodeSearchResults(boolean displayHSCodeSearchResults) {
		this.displayHSCodeSearchResults = displayHSCodeSearchResults;
	}

	/**
	 * @return the healthServiceCodesInGroupStateMap
	 */
	public RowStateMap getHealthServiceCodesInGroupStateMap() {
		return healthServiceCodesInGroupStateMap;
	}

	/**
	 * @param healthServiceCodesInGroupStateMap
	 *            the healthServiceCodesInGroupStateMap to set
	 */
	public void setHealthServiceCodesInGroupStateMap(RowStateMap healthServiceCodesInGroupStateMap) {
		this.healthServiceCodesInGroupStateMap = healthServiceCodesInGroupStateMap;
	}

	/**
	 * @return the healthServiceCodeResultsStateMap
	 */
	public RowStateMap getHealthServiceCodeResultsStateMap() {
		return healthServiceCodeResultsStateMap;
	}

	/**
	 * @param healthServiceCodeResultsStateMap
	 *            the healthServiceCodeResultsStateMap to set
	 */
	public void setHealthServiceCodeResultsStateMap(RowStateMap healthServiceCodeResultsStateMap) {
		this.healthServiceCodeResultsStateMap = healthServiceCodeResultsStateMap;
	}

	/**
	 * @return the modifierDropdownItems
	 */
	public List<Modifier> getModifierDropdownItems() {
		return modifierDropdownItems;
	}

	/**
	 * @param modifierDropdownItems
	 *            the modifierDropdownItems to set
	 */
	public void setModifierDropdownItems(List<Modifier> modifierDropdownItems) {
		this.modifierDropdownItems = modifierDropdownItems;
	}

	/**
	 * @return the implicitModifierDropdownItems
	 */
	public List<Modifier> getImplicitModifierDropdownItems() {
		return implicitModifierDropdownItems;
	}

	/**
	 * @param implicitModifierDropdownItems
	 *            the implicitModifierDropdownItems to set
	 */
	public void setImplicitModifierDropdownItems(List<Modifier> implicitModifierDropdownItems) {
		this.implicitModifierDropdownItems = implicitModifierDropdownItems;
	}

	/**
	 * @return the newHealthServiceGroup
	 */
	public HealthServiceGroup getNewHealthServiceGroup() {
		return newHealthServiceGroup;
	}

	/**
	 * @param newHealthServiceGroup
	 *            the newHealthServiceGroup to set
	 */
	public void setNewHealthServiceGroup(HealthServiceGroup newHealthServiceGroup) {
		this.newHealthServiceGroup = newHealthServiceGroup;
	}

	/**
	 * @return the healthServiceCodesNotInGroup
	 */
	public List<HealthServiceCode> getHealthServiceCodesNotInGroup() {

		return healthServiceCodesNotInGroup;
	}

	/**
	 * @param healthServiceCodesNotInGroup
	 *            the healthServiceCodesNotInGroup to set
	 */
	public void setHealthServiceCodesNotInGroup(List<HealthServiceCode> healthServiceCodesNotInGroup) {
		this.healthServiceCodesNotInGroup = healthServiceCodesNotInGroup;
	}

	/**
	 * @return the selectedYearEndDateForHSCView
	 */
	public String getSelectedYearEndDateForHSCView() {
		return selectedYearEndDateForHSCView;
	}

	/**
	 * @param selectedYearEndDateForHSCView
	 *            the selectedYearEndDateForHSCView to set
	 */
	public void setSelectedYearEndDateForHSCView(String selectedYearEndDateForHSCView) {
		this.selectedYearEndDateForHSCView = selectedYearEndDateForHSCView;
	}

	/**
	 * @return the displayServicesNotInGroup
	 */
	public boolean isDisplayServicesNotInGroup() {
		return displayServicesNotInGroup;
	}

	/**
	 * @param displayServicesNotInGroup
	 *            the displayServicesNotInGroup to set
	 */
	public void setDisplayServicesNotInGroup(boolean displayServicesNotInGroup) {
		this.displayServicesNotInGroup = displayServicesNotInGroup;
	}

	public String getFilterCategoryCode() {
		return filterCategoryCode;
	}

	public void setFilterCategoryCode(String filterCategoryCode) {
		this.filterCategoryCode = filterCategoryCode;
	}

	public String getFilterUnitFormula() {
		return filterUnitFormula;
	}

	public void setFilterUnitFormula(String filterUnitFormula) {
		this.filterUnitFormula = filterUnitFormula;
	}

	public String getFilerModifiers() {
		return filerModifiers;
	}

	public void setFilerModifiers(String filerModifiers) {
		this.filerModifiers = filerModifiers;
	}

	public String getFilterQualifiers() {
		return filterQualifiers;
	}

	public void setFilterQualifiers(String filerQualifiers) {
		this.filterQualifiers = filerQualifiers;
	}

	public String getFilterImplicitModifiers() {
		return filterImplicitModifiers;
	}

	public void setFilterImplicitModifiers(String filerImplicitModifiers) {
		this.filterImplicitModifiers = filerImplicitModifiers;
	}

	public String getFilterHealthServiceCode() {
		return filterHealthServiceCode;
	}

	public void setFilterHealthServiceCode(String filerHealthServiceCode) {
		this.filterHealthServiceCode = filerHealthServiceCode;
	}

	public String getFilterProgramCode() {
		return filterProgramCode;
	}

	public void setFilterProgramCode(String filerProgramCode) {
		this.filterProgramCode = filerProgramCode;
	}

	/**
	 * @return the displayDeleteDHSCConfirmation
	 */
	public boolean isDisplayDeleteDHSCConfirmation() {
		return displayDeleteDHSCConfirmation;
	}

	/**
	 * @param displayDeleteDHSCConfirmation
	 *            the displayDeleteDHSCConfirmation to set
	 */
	public void setDisplayDeleteDHSCConfirmation(boolean displayDeleteDHSCConfirmation) {
		this.displayDeleteDHSCConfirmation = displayDeleteDHSCConfirmation;
	}

	/**
	 * @return the fileDownloadControllerBean
	 */
	public FileDownloadControllerBean getFileDownloadControllerBean() {
		return fileDownloadControllerBean;
	}

	/**
	 * @param fileDownloadControllerBean
	 *            the fileDownloadControllerBean to set
	 */
	public void setFileDownloadControllerBean(FileDownloadControllerBean fileDownloadControllerBean) {
		this.fileDownloadControllerBean = fileDownloadControllerBean;
	}

	/**
	 * @return the displayHSGSearchResults
	 */
	public boolean isDisplayHSGSearchResults() {
		return displayHSGSearchResults;
	}

	/**
	 * @param displayHSGSearchResults
	 *            the displayHSGSearchResults to set
	 */
	public void setDisplayHSGSearchResults(boolean displayHSGSearchResults) {
		this.displayHSGSearchResults = displayHSGSearchResults;
	}

	/**
	 * @return the healthServiceGroupSearchResults
	 */
	public LazyDataModel<HealthServiceGroup> getHealthServiceGroupSearchResults() {
		return healthServiceGroupSearchResults;
	}

	/**
	 * @param healthServiceGroupSearchResults
	 *            the healthServiceGroupSearchResults to set
	 */
	public void setHealthServiceGroupSearchResults(LazyDataModel<HealthServiceGroup> healthServiceGroupSearchResults) {
		this.healthServiceGroupSearchResults = healthServiceGroupSearchResults;
	}

	/**
	 * @return the totalHealthServiceGroups
	 */
	public Integer getTotalHealthServiceGroups() {
		return totalHealthServiceGroups;
	}

	/**
	 * @param totalHealthServiceGroups
	 *            the totalHealthServiceGroups to set
	 */
	public void setTotalHealthServiceGroups(Integer totalHealthServiceGroups) {
		this.totalHealthServiceGroups = totalHealthServiceGroups;
	}
}

package ca.medavie.nspp.audit.ui.controller.validator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;

import ca.medavie.nspp.audit.service.data.IndividualExclusion;

/**
 * Used for validating Individual Exclusions
 */
public class IndividualExclusionValidator extends Validator<IndividualExclusion> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.ui.controller.validator.Validator#validate(java.lang.Object)
	 */
	@Override
	public void validate(IndividualExclusion t) throws ValidatorException {

		List<ValidatorMessage> validatorMsgs = new ArrayList<ValidatorMessage>();

		// 1) Check that Effective From date is provided
		if (t.getEffectiveFrom() == null) {
			validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.MEDICARE.IND.EXCL.EFFECTIVE.FROM.REQUIRED"));
		} else {
			// 2) If an Effective To date is provided ensure it is after the From date
			if (t.getEffectiveTo() != null) {

				/*
				 * Only perform check if From and To are not equal to each other. It is valid to have the same From/To
				 * values
				 */
				if (!t.getEffectiveFrom().equals(t.getEffectiveTo())) {
					// Ensure Effective From is before To
					if (t.getEffectiveFrom().after(t.getEffectiveTo())
							|| t.getEffectiveTo().before(t.getEffectiveFrom())) {

						validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
								"ERROR.MEDICARE.IND.EXCL.INVALID.DATE.RANGE"));
					}
				}
			}
		}
		if (!validatorMsgs.isEmpty()) {
			throw new ValidatorException(validatorMsgs);
		}
	}

	/**
	 * Checks there is no duplicate Exclusions. This includes overlap detection
	 * 
	 * @param aListOfExclusions
	 * @throws ValidatorException
	 */
	public void checkForDuplicateExclusions(List<IndividualExclusion> aListOfExclusions) throws ValidatorException {

		List<ValidatorMessage> validatorMsgs = new ArrayList<ValidatorMessage>();

		boolean duplicateDetected = false;
		boolean overlapDetected = false;

		// Check that there is no duplicates of any records
		for (IndividualExclusion exclusionA : aListOfExclusions) {
			String hlthCardNumber = exclusionA.getHealthCardNumber();

			// Grab any records with the same HealthCardNumber
			List<IndividualExclusion> similarExclusions = new ArrayList<IndividualExclusion>();

			for (IndividualExclusion exclusionB : aListOfExclusions) {
				if (exclusionB.getHealthCardNumber().equals(hlthCardNumber)) {
					// Ensure we are not adding the current record to the list.
					if (exclusionA != exclusionB) {
						similarExclusions.add(exclusionB);
					}
				}
			}

			/*
			 * Check that any similar Exclusions are not exact duplicates (only performed until a previous duplication
			 * is detected)
			 */
			if (!duplicateDetected) {
				for (IndividualExclusion exclusionB : similarExclusions) {

					// If from dates are the exact same that is a duplicate
					if (exclusionA.getEffectiveFrom().equals(exclusionB.getEffectiveFrom())) {
						duplicateDetected = true;
					}

					/*
					 * If no To dates are set they are duplicates since they never term. So their from dates eventually
					 * cross
					 */
					if (exclusionA.getEffectiveTo() == null && exclusionB.getEffectiveTo() == null) {
						duplicateDetected = true;
					}
				}

				// Add duplicate message if needed
				if (duplicateDetected) {
					validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.MEDICARE.IND.EXCL.DUPLICATE"));
				}
			}

			/*
			 * Check to see if any similar Exclusions date range overlaps with others. (Only performed until a previous
			 * overlap is detected)
			 */
			if (!overlapDetected) {
				for (IndividualExclusion exclusionB : similarExclusions) {

					// Grab date values (If the TO date is not provided we treat this as 31/12/4444)
					Date exclusionAFrom = exclusionA.getEffectiveFrom();
					Date exclusionATo = exclusionA.getEffectiveTo();

					Date exclusionBFrom = exclusionB.getEffectiveFrom();
					Date exclusionBTo = exclusionB.getEffectiveTo();

					// Check if TO values are null. IF so set to 31/12/4444
					Calendar c = Calendar.getInstance();
					c.set(4444, 12, 31);

					if (exclusionATo == null) {
						exclusionATo = c.getTime();
					}
					if (exclusionBTo == null) {
						exclusionBTo = c.getTime();
					}

					if ((!exclusionBFrom.before(exclusionAFrom) && !exclusionBFrom.after(exclusionATo))
							|| (!exclusionBTo.before(exclusionAFrom) && !exclusionBTo.after(exclusionATo))) {
						overlapDetected = true;
					}

				}

				// Add overlap message if needed
				if (overlapDetected) {
					validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.MEDICARE.IND.EXCL.OVERLAP"));
				}
			}
		}

		if (!validatorMsgs.isEmpty()) {
			throw new ValidatorException(validatorMsgs);
		}
	}
}

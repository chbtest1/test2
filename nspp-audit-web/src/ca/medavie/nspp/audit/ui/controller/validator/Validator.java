/**
 * 
 */
package ca.medavie.nspp.audit.ui.controller.validator;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

import ca.medavie.nspp.audit.service.exception.ServiceException;

/**
 * Validation implementation. Implementations of this Abstract class allow custom validation for the specified object
 */
public abstract class Validator<T> {

	/**
	 * Retrieves a message from the message bundle by Key
	 * 
	 * @param aMsgKey
	 * @return message from message bundle with matching key.
	 */
	private static String getMessageByKey(String aMsgKey) {
		// Get the current locale
		Locale currentLocale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		String bundleLocation = "ca.medavie.nspp.audit.ui.messages";
		ResourceBundle messageBundle = ResourceBundle.getBundle(bundleLocation, currentLocale);
		// Return the matching message
		return messageBundle.getString(aMsgKey);
	}
	
	/**
	 * Retrieves a message from the message bundle by Key
	 * @param aMsgKey
	 * @param params Option error msg parameters. e.g. BAD_ERROR = User {0} caused {1}.
	 * @return
	private static String getMessageByKey(String aMsgKey, Object... params) {
		// Get the current locale
		Locale currentLocale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		String bundleLocation = "ca.medavie.nspp.audit.ui.messages";
		ResourceBundle messageBundle = ResourceBundle.getBundle(bundleLocation, currentLocale);
		return MessageFormat.format(messageBundle.getString(aMsgKey), params);
	}	
	 */
	

	/**
	 * Displays a single error on the UI
	 * 
	 * @param anError
	 *            error message to display
	 */
	public static void displayValidationError(ValidatorMessage anError) {
		String localizedMessage = getMessageByKey(anError.getCode());
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(anError.getSeverity(), localizedMessage, null));
	}

	/**
	 * Should display list of Errors on UI
	 * 
	 * @param aServityLevel
	 *            - Specifies the severity of the messages
	 * @param errors
	 *            - list of errors
	 */
	public static void displayValidationErrors(ValidatorException exception) {
		for (ValidatorMessage error : exception.getMessages()) {
			String localizedMessage = getMessageByKey(error.getCode());
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(error.getSeverity(), localizedMessage, null));
		}
	}

	/**
	 * Should display error on UI
	 * 
	 * @param exception
	 *            - java.lang.Exception
	 */
	public static void displaySystemError(Exception exception) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, exception.getMessage(), null));
	}

	/**
	 * Should display error on UI
	 * 
	 * @param severity
	 * @param msgCode
	 */
	public static void displaySystemMessage(Severity severity, String msgCode) {
		String localizedMessage = getMessageByKey(msgCode);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(severity, localizedMessage, null));
	}

	/**
	 * 
	 * Display service exceptions
	 * 
	 * @param e
	 */
	public static void displayServiceError(ServiceException e) {

		String localizedMessage = getMessageByKey(e.getMessageCode());
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, localizedMessage, null));
	}

	/**
	 * Validates the provided class using the defined business logic within the implementation
	 * 
	 * @param t
	 *            - Object/Class that will be validated
	 * @throws validatorException
	 */
	public abstract void validate(T t) throws ValidatorException;

}

package ca.medavie.nspp.audit.ui.controller;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.ViewHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.DssClaimsHistoryTransferService;
import ca.medavie.nspp.audit.service.HealthServiceGroupService;
import ca.medavie.nspp.audit.service.data.DssClaimsHistoryTransferRequest;
import ca.medavie.nspp.audit.service.data.DssClaimsHistoryTransferSummary;
import ca.medavie.nspp.audit.service.data.HealthServiceCode;
import ca.medavie.nspp.audit.service.exception.DssClaimsHistoryTransferException;
import ca.medavie.nspp.audit.ui.AsposeLicenseUtility;
import ca.medavie.nspp.audit.ui.bean.BaseBackingBean;
import ca.medavie.nspp.audit.ui.controller.validator.Validator;

import com.aspose.cells.SaveFormat;
import com.aspose.cells.Workbook;
import com.aspose.cells.WorkbookDesigner;

/**
 * This session scope beans sole purpose is to hold state used in File downloads as well as constructing the Files for
 * download
 */
@ManagedBean(name = "fileDownloadController")
@SessionScoped
public class FileDownloadControllerBean extends BaseBackingBean implements Serializable {

	/** IDE Generated */
	private static final long serialVersionUID = 8756278077582468177L;

	/** Class logger */
	private static final Logger logger = LoggerFactory.getLogger(FileDownloadControllerBean.class);

	/** Template file path for Claims History Transfer excel export */
	private static final String CLAIMS_EXPORT_TEMPLATE_LOCATION = "/ca/medavie/nspp/audit/template/DSS_CLAIMS_HISTORY_TRANSFER.xlsx";
	private static final String HEALTH_SERVICE_TEMPLATE_LOCATION = "/ca/medavie/nspp/audit/template/Health_Services.xlsx";

	/** Holds the HSC filtered */
	private List<HealthServiceCode> filteredHSCs = new ArrayList<HealthServiceCode>();

	/**
	 * EJB beans
	 */
	@EJB
	protected DssClaimsHistoryTransferService dssClaimsHistoryTransferService;
	@EJB
	protected HealthServiceGroupService healthServiceGroupsLocalService;

	/** Holds the transfer request details */
	protected DssClaimsHistoryTransferRequest transferRequest;

	public FileDownloadControllerBean() {
		// Connect to EJB service
		if (dssClaimsHistoryTransferService == null) {
			try {
				this.dssClaimsHistoryTransferService = InitialContext.doLookup(prop
						.getProperty(DssClaimsHistoryTransferService.class.getName()));

				logger.debug("dssClaimsHistoryTransferService is loaded in the constructor");
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}
		if (this.healthServiceGroupsLocalService == null) {
			try {
				this.healthServiceGroupsLocalService = InitialContext.doLookup(prop
						.getProperty(HealthServiceGroupService.class.getName()));
				logger.debug("healthServiceGroupsLocalService is loaded in the constructor");
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}

		// Instantiate variables
		transferRequest = new DssClaimsHistoryTransferRequest();
		filteredHSCs = new ArrayList<HealthServiceCode>();
	}

	/**
	 * Loads the data for the previous claims transfer and provides and Excel file to the user
	 */
	public void exportClaimsTransferSummary() {

		// Load template
		InputStream xlsStream = getClass().getResourceAsStream(CLAIMS_EXPORT_TEMPLATE_LOCATION);

		try {
			// Load the transfer summary data
			DssClaimsHistoryTransferSummary summary = dssClaimsHistoryTransferService.getClaimsHistoryTransferSummary(
					transferRequest.getSourceHealthCardNumber(), transferRequest.getTargetHealthCardNumber());

			// Set up Aspose Liscense
			AsposeLicenseUtility.setUpAsposeLicense();

			// Create the Excel workbook
			Workbook workbook = new Workbook(xlsStream);
			WorkbookDesigner designer = new WorkbookDesigner();
			designer.setWorkbook(workbook);

			designer.setDataSource("Summary", summary);
			designer.setDataSource("medSe", summary.getMedicareSeClaimSummaries());
			designer.setDataSource("medMain", summary.getMedicareMainClaimsSummaries());
			designer.setDataSource("pharmSe", summary.getPharmSeClaimSummaries());
			designer.setDataSource("pharmMain", summary.getPharmMainClaimSummaries());
			designer.setDataSource("denticare", summary.getDenticareClaimSummaries());
			designer.process(false);

			// Create file name based on transfer details.
			String fileName = "ClaimsTransfer-" + transferRequest.getSourceHealthCardNumber() + "_"
					+ transferRequest.getTargetHealthCardNumber() + ".xlsx";
			FacesContext fc = FacesContext.getCurrentInstance();
			ExternalContext ec = fc.getExternalContext();
			// Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want
			// to get rid of them, else it may collide.
			ec.responseReset();
			ec.setResponseContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			// will work if it's omitted, but the download progress will be unknown.
			ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

			// Send file to user
			OutputStream output = ec.getResponseOutputStream();
			designer.getWorkbook().save(output, SaveFormat.XLSX);

			String viewId = fc.getViewRoot().getViewId();
			ViewHandler handler = fc.getApplication().getViewHandler();
			UIViewRoot root = handler.createView(fc, viewId);
			root.setViewId(viewId);
			fc.setViewRoot(root);

			fc.responseComplete();

		} catch (DssClaimsHistoryTransferException te) {
			Validator.displayServiceError(te);
			te.printStackTrace();
			logger.error(te.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				xlsStream.close();
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
	}

	/**
	 * Action to export total services not in group data to an excel file
	 */
	public void exportServicesNotInGroupSummary() {

		InputStream xlsStream = getClass().getResourceAsStream(HEALTH_SERVICE_TEMPLATE_LOCATION);

		try {
			// Set up Aspose Liscense
			AsposeLicenseUtility.setUpAsposeLicense();

			Workbook workbook = new Workbook(xlsStream);
			WorkbookDesigner designer = new WorkbookDesigner();
			designer.setWorkbook(workbook);

			filteredHSCs.removeAll(Collections.singleton(null));
			designer.setDataSource("HealthServices", filteredHSCs);
			designer.process(true);

			String fileName = "Health_Services_Not_In_Group.xlsx";

			FacesContext fc = FacesContext.getCurrentInstance();
			ExternalContext ec = fc.getExternalContext();
			// Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want
			// to get rid of them, else it may collide.
			ec.responseReset();
			ec.setResponseContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			// will work if it's omitted, but the download progress will be unknown.
			ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

			OutputStream output = ec.getResponseOutputStream();
			designer.getWorkbook().save(output, SaveFormat.XLSX);
			
			String viewId = fc.getViewRoot().getViewId();
			ViewHandler handler = fc.getApplication().getViewHandler();
			UIViewRoot root = handler.createView(fc, viewId);
			root.setViewId(viewId);
			fc.setViewRoot(root);

			fc.responseComplete();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				xlsStream.close();
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
	}

	/**
	 * @return the transferRequest
	 */
	public DssClaimsHistoryTransferRequest getTransferRequest() {
		return transferRequest;
	}

	/**
	 * @param transferRequest
	 *            the transferRequest to set
	 */
	public void setTransferRequest(DssClaimsHistoryTransferRequest transferRequest) {
		this.transferRequest = transferRequest;
	}

	/**
	 * @return the filteredHSCs
	 */
	public List<HealthServiceCode> getFilteredHSCs() {
		return filteredHSCs;
	}

	/**
	 * @param filteredHSCs
	 *            the filteredHSCs to set
	 */
	public void setFilteredHSCs(List<HealthServiceCode> filteredHSCs) {
		this.filteredHSCs = filteredHSCs;
	}
}

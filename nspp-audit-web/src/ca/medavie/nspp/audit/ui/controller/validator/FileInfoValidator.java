package ca.medavie.nspp.audit.ui.controller.validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.application.FacesMessage;

import org.icefaces.ace.component.fileentry.FileEntryStatuses;
import org.icefaces.ace.component.fileentry.FileEntryResults.FileInfo;

public class FileInfoValidator extends Validator<FileInfo> {

	private final List<String> VALID_MIME_TYPES = Arrays.asList("image/jpeg", "image/png", "application/pdf",
			"image/tiff", "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
			"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/msword", "text/plain",
			"application/vnd.ms-excel", "application/vnd.ms-excel.sheet.macroenabled.12",
			"application/vnd.ms-xpsdocument");

	@Override
	public void validate(FileInfo aFileInfo) throws ValidatorException {
		List<ValidatorMessage> errorMessages = new ArrayList<ValidatorMessage>();

		// Checks to see if a file was selected
		if (aFileInfo == null) {
			errorMessages.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.AUDIT.RESULT.ATTACHMENT.UPLOAD.NO.FILE.SELECTED"));
		} else {

			// Checks if the file is empty
			if (aFileInfo.getSize() == 0) {
				aFileInfo.updateStatus(FileEntryStatuses.INVALID, false, true);
				errorMessages.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.RESULT.ATTACHMENT.UPLOAD.FILE.EMPTY"));
			}

			// Check if the file size is too big
			if (aFileInfo.getSize() > 26214400) {
				aFileInfo.updateStatus(FileEntryStatuses.MAX_FILE_SIZE_EXCEEDED, false, true);
				errorMessages.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.RESULT.ATTACHMENT.UPLOAD.MAX.FILE.SIZE"));
			}

			// Checks that it's one of the MIME types allowed
			if (!VALID_MIME_TYPES.contains(aFileInfo.getContentType().toLowerCase())) {
				aFileInfo.updateStatus(FileEntryStatuses.INVALID_CONTENT_TYPE, false, true);
				errorMessages.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.RESULT.ATTACHMENT.UPLOAD.INVALID.FILE.TYPE"));
			}
		}

		if (!errorMessages.isEmpty()) {
			throw new ValidatorException(errorMessages);
		}
	}
}

package ca.medavie.nspp.audit.ui.controller.validator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;

import ca.medavie.nspp.audit.service.data.DenticareAuditLetterResponse;

/**
 * Used for validating Denticare Audit Letter Responses
 */
public class DenticareALRValidator extends Validator<DenticareAuditLetterResponse> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.ui.controller.validator.Validator#validate(java.lang.Object)
	 */
	@Override
	public void validate(DenticareAuditLetterResponse aDenticareALR) throws ValidatorException {
		List<ValidatorMessage> errorMsgs = new ArrayList<ValidatorMessage>();

		// Validate the value length of Charge Back amount (10,2)
		if (aDenticareALR.getChargeBackAmount() != null) {
			if (aDenticareALR.getChargeBackAmount().compareTo(BigDecimal.valueOf(99999999.99)) == 1) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.CHARGE.BACK.INVALID.MAX"));
			}
		}

		// Validate the value length of Number of Claims Affected (7)
		if (aDenticareALR.getNumberOfClaimAffected() != null) {
			if (aDenticareALR.getNumberOfClaimAffected().compareTo(Integer.valueOf(9999999)) == 1) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.CLAIMS.AFFECTED.INVALID.MAX"));
			}
		}

		if (!errorMsgs.isEmpty()) {
			throw new ValidatorException(errorMsgs);
		}
	}
}

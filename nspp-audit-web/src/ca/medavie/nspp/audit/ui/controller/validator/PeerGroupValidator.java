package ca.medavie.nspp.audit.ui.controller.validator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;

import ca.medavie.nspp.audit.service.data.PeerGroup;

/**
 * Used for validating PeerGroups
 */
public class PeerGroupValidator extends Validator<PeerGroup> {
	private static final BigDecimal maxValue = new BigDecimal("99999999.99");

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.ui.controller.validator.Validator#validate(java.lang.Object)
	 */
	@Override
	public void validate(PeerGroup aPeerGroupToValidate) throws ValidatorException {
		List<ValidatorMessage> errorMsgs = new ArrayList<ValidatorMessage>();

		// Check that all the required values are provided.
		if (aPeerGroupToValidate.getPeerGroupName() == null || aPeerGroupToValidate.getPeerGroupName().equals("")) {
			errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.PEER.GROUP.NAME.REQUIRED"));
		}
		if (aPeerGroupToValidate.getYearEndDate() == null) {
			errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.YEAR.END.DATE.REQUIRED"));
		}
		if (aPeerGroupToValidate.getTownCodeInclusive() == null) {
			errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.PEER.GROUP.TOWN.CODE.REQUIRED"));
		}

		// Check that the Min/Max Town population values are greater than 0 if provided
		if (aPeerGroupToValidate.getMinimumTownPopulation() != null) {
			if (aPeerGroupToValidate.getMinimumTownPopulation().compareTo(BigDecimal.ZERO) < 0
					|| aPeerGroupToValidate.getMinimumTownPopulation().compareTo(BigDecimal.ZERO) == 0) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.MIN.TOWN.POP.INVALID"));
			}
			// Verify Minimum population is not above maximum allowed value (7)
			if (aPeerGroupToValidate.getMinimumTownPopulation().compareTo(BigDecimal.valueOf(9999999)) == 1) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.MIN.TOWN.POP.INVALID.MAX"));
			}
		}
		if (aPeerGroupToValidate.getMaximumTownPopulation() != null) {
			if (aPeerGroupToValidate.getMaximumTownPopulation().compareTo(BigDecimal.ZERO) < 0
					|| aPeerGroupToValidate.getMaximumTownPopulation().compareTo(BigDecimal.ZERO) == 0) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.MAX.TOWN.POP.INVALID"));
			}
			// Verify Maximum population is not above maximum allowed value (7)
			if (aPeerGroupToValidate.getMaximumTownPopulation().compareTo(BigDecimal.valueOf(9999999)) == 1) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.MAX.TOWN.POP.INVALID.MAX")); // TODO
			}
		}

		// Check that the Min/Max Earnings values are greater than 0 if provided
		if (aPeerGroupToValidate.getMinimumEarnings() != null) {
			if (aPeerGroupToValidate.getMinimumEarnings().compareTo(BigDecimal.ZERO) < 0
					|| aPeerGroupToValidate.getMinimumEarnings().compareTo(BigDecimal.ZERO) == 0) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.MIN.EARNINGS.INVALID"));
			}
			if (aPeerGroupToValidate.getMinimumEarnings().compareTo(maxValue) == 1) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.MIN.EARNINGS.INVALID.MAX"));
			}
		}
		if (aPeerGroupToValidate.getMaximumEarnings() != null) {
			if (aPeerGroupToValidate.getMaximumEarnings().compareTo(BigDecimal.ZERO) < 0
					|| aPeerGroupToValidate.getMaximumEarnings().compareTo(BigDecimal.ZERO) == 0) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.MAX.EARNINGS.INVALID"));
			}
			if (aPeerGroupToValidate.getMaximumEarnings().compareTo(maxValue) == 1) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.MAX.EARNINGS.INVALID.MAX"));
			}
		}

		// Check that a Town Criteria record exists for the Peer Group if Town Code Inclusive is set to True
		if (aPeerGroupToValidate.getTownCodeInclusive()) {
			// Check if any Town Criteria records were added to the Peer Group
			if (aPeerGroupToValidate.getTownCriterias() == null || aPeerGroupToValidate.getTownCriterias().isEmpty()) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.NO.TOWN.CRITERIAS"));
			}
		}

		// Check that SE Percentage is greater than 0 and less than or equal to 100
		if (aPeerGroupToValidate.getPercentageSeEarnings() != null) {
			// Check that SE Percentage is greater than 0 and less than or equal to 100
			if (aPeerGroupToValidate.getPercentageSeEarnings().compareTo(BigDecimal.ZERO) < 0
					|| aPeerGroupToValidate.getPercentageSeEarnings().compareTo(BigDecimal.valueOf(100)) > 0) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.SE.PERCENTAGE.INVALID"));
			}
		}

		// Check that a Percentage SE is provided if HS records exist.
		if (!aPeerGroupToValidate.getHealthServiceCriterias().isEmpty()) {
			if (aPeerGroupToValidate.getPercentageSeEarnings() == null) {
				// User failed to provide Percentage SE earnings when HS records are in the PeerGroup
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.SE.PERCENTAGE.REQUIRED"));
			}
		}

		// Check Activity Min Earnings is valid if provided.
		if (aPeerGroupToValidate.getActMinimumEarnings() != null) {
			if (aPeerGroupToValidate.getActMinimumEarnings().compareTo(BigDecimal.ZERO) < 0) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.ACT.MIN.EARNINGS.INVALID"));
			}
			if (aPeerGroupToValidate.getActMinimumEarnings().compareTo(maxValue) == 1) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.ACT.MIN.EARNINGS.INVALID.MAX"));
			}
		}

		// Check Activity Min Shadow Earnings is valid if provided
		if (aPeerGroupToValidate.getMinimumShadowEarnings() != null) {
			if (aPeerGroupToValidate.getMinimumShadowEarnings().compareTo(BigDecimal.ZERO) < 0) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.SHADOW.MIN.EARNINGS.INVALID"));
			}
			if (aPeerGroupToValidate.getMinimumShadowEarnings().compareTo(maxValue) == 1) {
				errorMsgs
						.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.SHADOW.MIN.EARNINGS.INVALID.MAX"));
			}
		}

		if (!errorMsgs.isEmpty()) {
			throw new ValidatorException(errorMsgs);
		}
	}
}

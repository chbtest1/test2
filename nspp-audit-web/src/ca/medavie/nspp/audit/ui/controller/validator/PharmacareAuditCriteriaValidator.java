package ca.medavie.nspp.audit.ui.controller.validator;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;

import ca.medavie.nspp.audit.service.MedicareService;
import ca.medavie.nspp.audit.service.data.AuditCriteria;
import ca.medavie.nspp.audit.service.exception.MedicareServiceException;

public class PharmacareAuditCriteriaValidator extends Validator<AuditCriteria> {

	@EJB
	protected MedicareService medicareService;

	/**
	 * Since we need the EJB service for parts of the validation we pass it on construction of Validator class
	 * 
	 * @param aMedicareService
	 */
	public PharmacareAuditCriteriaValidator(MedicareService aMedicareService) {
		this.medicareService = aMedicareService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.ui.controller.validator.Validator#validate(java.lang.Object)
	 */
	@Override
	public void validate(AuditCriteria t) throws ValidatorException {
		List<ValidatorMessage> validatorMsgs = new ArrayList<ValidatorMessage>();

		/**
		 * ----------------------------------------------------
		 * 
		 * Common Audit rules (Applies to both Audit types)
		 * 
		 * ----------------------------------------------------
		 * 
		 * 1) Number of Services to Audit is a required value
		 * 
		 * 2) If Audit From date is provided it must be less than or equal to the Audit To date if provided.
		 * 
		 * 3) If Cexp From code is provided is must be less than or equal to the Cexp To code if provided.
		 * 
		 * 4) If Audit Payment To Date or Cexp To code are provided the associated From date/code is required.
		 * 
		 * 5) Both Payment Dates and Cexp Codes cannot be provided at the same time. Only one or the other can be
		 * entered
		 * 
		 * 6) If 'Audit Latest Payment Date' indicator is set to N, either the Payment dates or the Cexp codes are
		 * required.
		 * 
		 * 7) If 'Audit Latest Payment Date' indicator is set to Y, neither the Payment dates or the Cexp codes can be
		 * provided.
		 */

		// Check if Services To Audit is provided
		if (t.getNumberOfServicesToAudit() == null) {
			validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.AUDIT.CRIT.SERVICES.TO.AUDIT.REQUIRED"));
		} else {
			// Verify Services To Audit value length (7)
			if (t.getNumberOfServicesToAudit().compareTo(Long.valueOf(9999999)) == 1) {
				validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.CRIT.NUMBER.AUDITS.INVALID.MAX"));
			}
		}

		// Check that Audit From and To are valid values.
		if (t.getAuditTo() != null) {
			if (t.getAuditFrom() == null) {
				// No Audit From date provided.. Error
				validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.CRIT.AUDIT.FROM.REQUIRED"));
			} else {
				// Check if Audit From is before Audit To (From and To can be the same value)
				if (!t.getAuditFrom().equals(t.getAuditTo()) && t.getAuditFrom().after(t.getAuditTo())) {
					// Audit From is after Audit To. Error
					validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.AUDIT.CRIT.AUDIT.FROM.INVALID"));
				}
			}
		}

		// Check that Cexp From and To are valid values.
		if (t.getToCexpCode() != null) {
			if (t.getFromCexpCode() == null) {
				// No Cexp From code provided.. Error
				validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.CRIT.AUDIT.CEXP.FROM.REQUIRED"));
			} else {
				// Check if Cexp From is before Cexp To (From and To can be the same value)
				if (!t.getFromCexpCode().equals(t.getToCexpCode()) && t.getFromCexpCode() > t.getToCexpCode()) {
					// Cexp From code is bigger than the To code
					validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.AUDIT.CRIT.AUDIT.CEXP.INVALID"));
				}
			}
		}

		// Check that only Cexp Codes or Audit dates can be provided. Both cannot exist
		if (t.getAuditFrom() != null && t.getFromCexpCode() != null) {
			validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.AUDIT.CRIT.BOTH.DATE.CEXP.PROVIDED"));
		}

		if (!t.isAuditFromLastIndicator()) {
			// Either Cexp Codes or Audit dates are required. (Not both, only one or the other)
			if (t.getAuditFrom() == null && t.getFromCexpCode() == null) {
				validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.CRIT.AUDIT.DATE.CEXP.REQUIRED"));
			}
		} else {
			// Neither Cexp Codes or Audit dates can be provided
			if (t.getAuditFrom() != null || t.getFromCexpCode() != null) {
				validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.CRIT.AUDIT.LAST.AND.DATE.CEXP.PROVIDED"));
			}
		}

		if (!validatorMsgs.isEmpty()) {
			throw new ValidatorException(validatorMsgs);
		}
	}

	/**
	 * Validate that the provide Practitioner ID/Type exists (Also loads the name if valid into the Audit Criteria)
	 * 
	 * If Practitioner is valid we then validate that an audit of the same type does not already exist for this
	 * practitioner
	 * 
	 * @param t
	 * @throws ValidatorException
	 */
	public void validateNewCriteriaParameters(AuditCriteria t) throws ValidatorException {
		List<ValidatorMessage> validatorMsgs = new ArrayList<ValidatorMessage>();

		try {
			// Check that an Audit Type was provided. If no type is provided do nothing and display error
			if (t.getAuditType() == null || t.getAuditType().trim().isEmpty()) {
				// Audit Type not provided. Add error
				validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.MEDICARE.AUDIT.TYPE.REQUIRED"));
			} else {
				// Perform a lookup of the provider. Gets the name if the provider is valid
				String practitionerName = medicareService.getPractitionerName(t.getProviderNumber(),
						t.getProviderType());

				if (practitionerName == null || practitionerName.isEmpty()) {
					// Add error message stating the Practitioner does not exist.
					validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.AUDIT.CRITERIA.INVALID.PRACTITIONER"));
				} else {
					// Set the Practitioner name to the Audit Criteria object
					t.setProviderName(practitionerName);

					// Now check that this Audit does not already exist in the system
					AuditCriteria duplicateAuditCriteria = medicareService.getMedicarePractitionerAuditCriteria(
							t.getProviderNumber(), t.getProviderType(), t.getAuditType());

					if (duplicateAuditCriteria != null) {
						// Audit already exists. Add error
						validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
								"ERROR.AUDIT.CRITERIA.PRACTITIONER.DUPLICATE"));
					}
				}
			}
		} catch (MedicareServiceException e) {
			ValidatorMessage message = new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.PHARM.CARE.SERVICE.EXCEPTION");
			validatorMsgs.add(message);
			e.printStackTrace();
		}

		if (!validatorMsgs.isEmpty()) {
			throw new ValidatorException(validatorMsgs);
		}
	}
}

package ca.medavie.nspp.audit.ui.controller.validator;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;

import ca.medavie.nspp.audit.service.data.HealthServiceCodeSearchCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceGroup;

/**
 * Used for validating HealthServiceGroups
 */
public class HealthServiceGroupValidator extends Validator<HealthServiceGroup> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.ui.controller.validator.Validator#validate(java.lang.Object)
	 */
	@Override
	public void validate(HealthServiceGroup aHealthServiceGroup) throws ValidatorException {

		List<ValidatorMessage> validatorMsgs = new ArrayList<ValidatorMessage>();

		if (aHealthServiceGroup.getHealthServiceGroupId() == null) {
			validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.HS.GROUPS.ID.REQUIRED"));
		}
		if (aHealthServiceGroup.getHealthServiceGroupName() == null
				|| aHealthServiceGroup.getHealthServiceGroupName().equals("")) {
			validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.HS.GROUPS.NAME.REQUIRED"));
		}

		if (!validatorMsgs.isEmpty()) {
			throw new ValidatorException(validatorMsgs);
		}
	}

	/**
	 * Validates the provided HealthServiceCode search criteria
	 * 
	 * @param aSearchCriteria
	 * @throws ValidatorException
	 */
	public void validateHealthServiceCodeSearch(HealthServiceCodeSearchCriteria aSearchCriteria)
			throws ValidatorException {

		List<ValidatorMessage> validatorMsgs = new ArrayList<ValidatorMessage>();

		/**
		 * Rules:
		 * 
		 * 1) Program is a required criteria
		 * 
		 * 2) If using group filter on services not in group then Effective From is required
		 * 
		 */

		// Rule 1) Program is a required criteria
		if (aSearchCriteria.getProgram() == null || aSearchCriteria.getProgram().isEmpty()) {

			validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.HS.GROUPS.HSC.SEARCH.PROGRAM.REQUIRED"));
		}

		// Rule 2) If using group filter on services not in group then Effective From is required
		if (!aSearchCriteria.isServiceInGroup()) {
			if (aSearchCriteria.getGroupFilterFromDate() == null) {

				validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.HS.GROUPS.HSC.SEARCH.EFFECTIVE.FROM.REQUIRED"));
			}
		}		

		if (!validatorMsgs.isEmpty()) {
			throw new ValidatorException(validatorMsgs);
		}
	}
}

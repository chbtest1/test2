package ca.medavie.nspp.audit.ui.controller;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.AjaxBehaviorEvent;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.PeerGroupService;
import ca.medavie.nspp.audit.service.data.DataUtilities;
import ca.medavie.nspp.audit.service.data.PeerGroup;
import ca.medavie.nspp.audit.service.exception.PeerGroupServiceException;
import ca.medavie.nspp.audit.ui.FacesUtils;
import ca.medavie.nspp.audit.ui.bean.DropdownConstantsBean;
import ca.medavie.nspp.audit.ui.controller.validator.Validator;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorMessage;

/**
 * Backing bean for the PeerGroup Bulk Copy page/panel
 */
@ManagedBean(name = "bulkCopyController")
@ViewScoped
public class BulkCopyControllerBean extends DropdownConstantsBean implements Serializable {

	// Holds reference to the EJB Client service class
	@EJB
	protected PeerGroupService peerGroupService;

	/** IDE generated */
	private static final long serialVersionUID = -847535101009263763L;

	/** Year End Date that all the selected Peer Groups will be copied to */
	private String selectedBulkCopyDate;
	private Date bulkCopyTargetDate;

	/** List of PeerGroups that can be copied to the specified date */
	private List<PeerGroup> peerGroupsAvailableForCopy;
	private static final Logger logger = LoggerFactory.getLogger(BulkCopyControllerBean.class);

	/**
	 * Bean constructor
	 */
	public BulkCopyControllerBean() {
		if (this.peerGroupService == null) {
			try {
				this.peerGroupService = InitialContext.doLookup(prop.getProperty(PeerGroupService.class.getName()));
				logger.debug("peerGroupService is loaded in the constructor");

			} catch (NamingException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Takes the user to the Bulk Copy panel
	 */
	public void initiateBulkCopyPanel() {
		// Reset year end field
		selectedBulkCopyDate = null;

		// Load in all peerGroups by their latest YearEndDate
		try {
			peerGroupsAvailableForCopy = peerGroupService.getLatestPeerGroups();
			Collections.sort(peerGroupsAvailableForCopy);
		} catch (PeerGroupServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Triggered via Ajax to perform a scan of all PeerGroups to determine what groups are eligible to be copied to the
	 * specified date
	 */
	public void targetYearEndDateChangeScan(AjaxBehaviorEvent anEvent) {

		if (selectedBulkCopyDate == null) {
			// Always clear all selections following date value change
			deselectAllPeerGroups();

			// With no date selected. No PeerGroup can be selected
			for (PeerGroup pg : peerGroupsAvailableForCopy) {
				pg.setCopyDisabled(true);
			}
		} else {
			try {
				bulkCopyTargetDate = DataUtilities.SDF.parse(selectedBulkCopyDate);

				if (bulkCopyTargetDate != null) {
					/**
					 * Iterate over each PeerGroup and compare peerGroup current year end to new year end (Determine if
					 * it can be copied)
					 */
					for (PeerGroup pg : peerGroupsAvailableForCopy) {
						Date currentYearEnd = pg.getYearEndDate();

						/**
						 * Remove selected state on any rows. This ensures previous selections are not copied with a new
						 * target date
						 */
						pg.setSelectedForCopy(false);

						if (!bulkCopyTargetDate.equals(currentYearEnd) && !bulkCopyTargetDate.before(currentYearEnd)
								&& bulkCopyTargetDate.after(currentYearEnd)) {
							// Copy allowed for this group with the current target date
							pg.setCopyDisabled(false);
						} else {
							// Copy not allowed for this group with the current target date
							pg.setCopyDisabled(true);
						}
					}
				} else {
					// Ensure check-boxes are disabled if the date field is cleared out
					for (PeerGroup pg : peerGroupsAvailableForCopy) {
						pg.setCopyDisabled(true);
					}
				}
			} catch (ParseException pe) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
			}
		}
	}

	/**
	 * Used for selecting all PeerGroups that are able to be copied to the target YearEndDate
	 */
	public void selectAllPeerGroups() {
		for (PeerGroup pg : peerGroupsAvailableForCopy) {
			// If PeerGroup is allowed to copy to target date select it..
			if (!pg.isCopyDisabled()) {
				pg.setSelectedForCopy(true);
			}
		}
	}

	/**
	 * Used for deselected all the selected PeerGroups
	 */
	public void deselectAllPeerGroups() {
		for (PeerGroup pg : peerGroupsAvailableForCopy) {
			pg.setSelectedForCopy(false);
		}
	}

	/**
	 * Copies the selected PeerGroups. Once completed the table is refreshed with the records new Year End Dates
	 */
	public void executePeerGroupCopy() {
		// Holds all PeerGroups that will be copied to the new Year End Date
		List<PeerGroup> peerGroupsToCopy = new ArrayList<PeerGroup>();

		for (PeerGroup pg : peerGroupsAvailableForCopy) {
			if (pg.isSelectedForCopy()) {

				pg.setNewYearEndDate(bulkCopyTargetDate);
				peerGroupsToCopy.add(pg);
			}
		}

		// if there is nothing selected display the error message
		if (peerGroupsToCopy.isEmpty()) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.PEER.GROUP.NO.GROUPS.SELECTED"));
		}

		else {
			// Perform the copy of records in the DB if any peerGroups were selected
			try {

				peerGroupService.copyPeerGroups(peerGroupsToCopy, FacesUtils.getUserId());

				// Reload the PeerGroup list for the Bulk Copy screen to show changes
				peerGroupsAvailableForCopy.clear();
				peerGroupsAvailableForCopy = peerGroupService.getLatestPeerGroups();
				Collections.sort(peerGroupsAvailableForCopy);

				// copy complete.. Clear out the new YearEnd Date
				// Disable all checkboxes once again
				for (PeerGroup pg : peerGroupsAvailableForCopy) {
					pg.setCopyDisabled(true);
				}
				bulkCopyTargetDate = null;
				// Set success message
				Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.PEERGROUP.BULK.COPY.COMPLETE");

			} catch (PeerGroupServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());

			}
			// catch all other system errors
			catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());

			}
		}
	}

	/**
	 * @return the peerGroupsAvailableForCopy
	 */
	public List<PeerGroup> getPeerGroupsAvailableForCopy() {
		return peerGroupsAvailableForCopy;
	}

	/**
	 * @param peerGroupsAvailableForCopy
	 *            the peerGroupsAvailableForCopy to set
	 */
	public void setPeerGroupsAvailableForCopy(List<PeerGroup> peerGroupsAvailableForCopy) {
		this.peerGroupsAvailableForCopy = peerGroupsAvailableForCopy;
	}

	/**
	 * @return the selectedBulkCopyDate
	 */
	public String getSelectedBulkCopyDate() {
		return selectedBulkCopyDate;
	}

	/**
	 * @param selectedBulkCopyDate
	 *            the selectedBulkCopyDate to set
	 */
	public void setSelectedBulkCopyDate(String selectedBulkCopyDate) {
		this.selectedBulkCopyDate = selectedBulkCopyDate;
	}
}

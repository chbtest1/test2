package ca.medavie.nspp.audit.ui.controller.validator;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;

import ca.medavie.nspp.audit.service.MedicareService;
import ca.medavie.nspp.audit.service.data.AuditCriteria;
import ca.medavie.nspp.audit.service.exception.MedicareServiceException;

/**
 * Used for validating a Medicare Audit Criteria record
 */
public class MedicareAuditCriteriaValidator extends Validator<AuditCriteria> {

	@EJB
	protected MedicareService medicareService;

	/**
	 * Since we need the EJB service for parts of the validation we pass it on construction of Validator class
	 * 
	 * @param aMedicareService
	 */
	public MedicareAuditCriteriaValidator(MedicareService aMedicareService) {
		medicareService = aMedicareService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.ui.controller.validator.Validator#validate(java.lang.Object)
	 */
	@Override
	public void validate(AuditCriteria t) throws ValidatorException {

		List<ValidatorMessage> validatorMsgs = new ArrayList<ValidatorMessage>();

		/**
		 * 
		 * Default Audit specific rules:
		 * 
		 * 1) Payment Date is required if Audit From Last Payment Date is not selected
		 * 
		 * Practitioner Audit specific rules:
		 * 
		 * 1) If Audit From date is provided it must be less than or equal to the Audit To date if provided.
		 * 
		 * 2) If Payment From date is provided it must be less than or equal to Payment To date if provided
		 * 
		 * 3) If Audit Payment To or Audit To dates are provided the associated From date is required.
		 * 
		 * 4) Both Payment and Audit Dates cannot be provided at the same time. Only one or the other can be entered
		 * 
		 * ----------------------------------------------------
		 * 
		 * Common Audit rules (Applies to both Audit types)
		 * 
		 * ----------------------------------------------------
		 * 
		 * 5) If 'Audit Latest Payment Date' indicator is set to N, either the Payment dates or the Audit dates are
		 * required.
		 * 
		 * 6) If 'Audit Latest Payment Date' indicator is set to Y, neither the Payment dates or the Audit dates can be
		 * provided.
		 * 
		 * 7) Number of Services to Audit and HS Description are both required values
		 */
		// Check that Audit From and To are valid values.
		if (t.getAuditTo() != null) {
			if (t.getAuditFrom() == null) {
				// No Audit From date provided.. Error
				validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.CRIT.AUDIT.FROM.REQUIRED"));
			} else {
				// Check if Audit From is before Audit To (From and To can be the same value)
				if (!t.getAuditFrom().equals(t.getAuditTo()) && t.getAuditFrom().after(t.getAuditTo())) {
					// Audit From is after Audit To. Error
					validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.AUDIT.CRIT.AUDIT.FROM.INVALID"));
				}
			}
		}

		// Check that Audit Payment From and To are valid values.
		if (t.getAuditToPaymentDate() != null) {
			if (t.getAuditFromPaymentDate() == null) {
				// No Audit Payment From date provided.. Error
				validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.CRIT.AUDIT.PAY.FROM.REQUIRED"));
			} else {
				// Check if Audit Payment From date is before Audit Payment To (From and To can be the same value)
				if (!t.getAuditFromPaymentDate().equals(t.getAuditToPaymentDate())
						&& t.getAuditFromPaymentDate().after(t.getAuditToPaymentDate())) {
					// Audit Payment From is after Audit Payment To.. Error
					validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.AUDIT.CRIT.AUDIT.PAY.FROM.INVALID"));
				}
			}
		}

		// Check that only Payment or Audit dates are provided. Both cannot exist
		if (t.getAuditFrom() != null && t.getAuditFromPaymentDate() != null) {
			validatorMsgs
					.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.AUDIT.CRIT.BOTH.DATES.PROVIDED"));
		}

		// Validation for Audit Last Indicator varies based on type. Practitioner or default
		if (t.getProviderNumber() == null) {

			// Default audit validation flow
			if (!t.isAuditFromLastIndicator()) {
				// Payment date is required
				if (t.getAuditFromPaymentDate() == null) {
					validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.AUDIT.CRIT.AUDIT.PAYMENT.NOT.PROVIDED"));
				}
			} else {
				// Payment date cannot be provided
				if (t.getAuditFromPaymentDate() != null) {
					validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.AUDIT.CRIT.AUDIT.PAYMENT.PROVIDED"));
				}
			}
		} else {

			// Practitioner audit validation flow
			if (!t.isAuditFromLastIndicator()) {
				// Either Payment or Audit dates are required. (Not both, only one or the other)
				if (t.getAuditFrom() == null && t.getAuditFromPaymentDate() == null) {
					validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.AUDIT.CRIT.AUDIT.DATES.REQUIRED"));
				}
			} else {
				// Neither Payment or Audit dates can be provided
				if (t.getAuditFrom() != null || t.getAuditFromPaymentDate() != null) {
					validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.AUDIT.CRIT.AUDIT.LAST.AND.DATE.PROVIDED"));
				}
			}
		}

		// Check if Services To Audit is provided
		if (t.getNumberOfServicesToAudit() == null) {
			validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.AUDIT.CRIT.SERVICES.TO.AUDIT.REQUIRED"));
		} else {
			// Check Services to Audit value length (7)
			if (t.getNumberOfServicesToAudit().compareTo(Long.valueOf(9999999)) == 1) {
				validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.CRIT.NUMBER.AUDITS.INVALID.MAX"));
			}
		}

		// Check if HS Description is provided
		if (t.getDefaultHealthServiceDesc() == null || t.getDefaultHealthServiceDesc().isEmpty()) {
			validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.AUDIT.CRIT.HS.DESCRIPTION.REQUIRED"));
		}

		if (!validatorMsgs.isEmpty()) {
			throw new ValidatorException(validatorMsgs);
		}
	}

	/**
	 * Validate that the provide Practitioner ID/Type exists (Also loads the name if valid into the Audit Criteria)
	 * 
	 * If Practitioner is valid we then validate that an audit of the same type does not already exist for this
	 * practitioner
	 * 
	 * @param t
	 * @throws ValidatorException
	 */
	public void validateNewCriteriaParameters(AuditCriteria t) throws ValidatorException {
		List<ValidatorMessage> validatorMsgs = new ArrayList<ValidatorMessage>();

		try {

			// Check that an Audit Type was provided. If not do not do an existing audit check
			if (t.getAuditType() == null || t.getAuditType().trim().isEmpty()) {
				// Audit Type not provided. Add error
				validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.MEDICARE.AUDIT.TYPE.REQUIRED"));
			} else {
				// Perform a lookup of the provider. Gets the name if the provider is valid
				String practitionerName = medicareService.getPractitionerName(t.getProviderNumber(),
						t.getProviderType());

				if (practitionerName == null || practitionerName.isEmpty()) {
					// Add error message stating the Practitioner does not exist.
					validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.AUDIT.CRITERIA.INVALID.PRACTITIONER"));
				} else {

					// Set the Practitioner name to the Audit Criteria object
					t.setProviderName(practitionerName);

					// Now check that this Audit does not already exist in the system
					AuditCriteria duplicateAuditCriteria = medicareService.getMedicarePractitionerAuditCriteria(
							t.getProviderNumber(), t.getProviderType(), t.getAuditType());

					if (duplicateAuditCriteria != null) {
						// Audit already exists. Add error
						validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
								"ERROR.AUDIT.CRITERIA.PRACTITIONER.DUPLICATE"));
					}
				}
			}

		} catch (MedicareServiceException e) {
			ValidatorMessage message = new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.MEDICARE.SERVICE");
			validatorMsgs.add(message);
			e.printStackTrace();
		}

		if (!validatorMsgs.isEmpty()) {
			throw new ValidatorException(validatorMsgs);
		}
	}
}

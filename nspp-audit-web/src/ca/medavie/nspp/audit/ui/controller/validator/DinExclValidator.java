package ca.medavie.nspp.audit.ui.controller.validator;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;

import ca.medavie.nspp.audit.service.PharmacareService;
import ca.medavie.nspp.audit.service.data.PharmAuditDinExcl;

public class DinExclValidator extends Validator<PharmAuditDinExcl> {
	
	@EJB
	protected PharmacareService pharmacareService;

	/**
	 * Since we need the EJB service for parts of the validation we pass it on construction of Validator class
	 * 
	 * @param pharmacareService
	 */
	public DinExclValidator(PharmacareService pharmacareService) {
		this.pharmacareService = pharmacareService;
	}
	
	@Override
	public void validate(PharmAuditDinExcl pharmAuditDinExcl) throws ValidatorException {
		List<ValidatorMessage> errorMsgs = new ArrayList<ValidatorMessage>();
		
		if (pharmAuditDinExcl.getName().isEmpty()) {
			errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.DSS.DIN.INVALID"));						
		}
		
		if (pharmAuditDinExcl.getAgeRestriction() != null) {
			if (pharmAuditDinExcl.getAgeRestriction().longValue() < 1) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.MEDICARE.HS.EXCL.INVALID.AGE.RESTRICTION"));						
			}
		}
		
		// Throw Validator exception if errors were found
		if (!errorMsgs.isEmpty()) {
			throw new ValidatorException(errorMsgs);
		}		
	}
}


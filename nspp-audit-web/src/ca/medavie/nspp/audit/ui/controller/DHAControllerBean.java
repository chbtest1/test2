package ca.medavie.nspp.audit.ui.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.icefaces.ace.model.table.RowStateMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.DHAService;
import ca.medavie.nspp.audit.service.data.DistrictHealthAuthority;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.exception.DHAServiceException;
import ca.medavie.nspp.audit.ui.FacesUtils;
import ca.medavie.nspp.audit.ui.bean.DropdownConstantsBean;
import ca.medavie.nspp.audit.ui.controller.validator.DistrictHealthAuthorityValidator;
import ca.medavie.nspp.audit.ui.controller.validator.Validator;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorException;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorMessage;

/**
 * Backing bean for the DHA pages
 */
@ManagedBean(name = "dhaController")
@ViewScoped
public class DHAControllerBean extends DropdownConstantsBean implements Serializable {

	/** JRE generated */
	private static final long serialVersionUID = 1592233197695448160L;

	/**
	 * DHA EJB service
	 */
	@EJB
	protected DHAService dhaLocalService;

	/** Logger for class */
	private static final Logger logger = LoggerFactory.getLogger(DHAControllerBean.class);

	/** Used for Searching practitioners */
	private PractitionerSearchCriteria practitionerSearchCriteria;

	/** Holds the Practitioner search results */
	private List<Practitioner> practitionerSearchResults;

	/** Holds the list of existing DHA records */
	private List<DistrictHealthAuthority> districtHealthAuthorities;

	/** The state map of the DHA table */
	private RowStateMap dhaStateMap;

	/** Maps that hold ZoneIds and DHACodes */
	private Map<String, String> zoneIds;
	private Map<String, String> dhaCodes;

	/** DHA validator */
	private DistrictHealthAuthorityValidator dhaValidator;

	/** Holds reference to the DHA record being edited or added */
	private DistrictHealthAuthority selectedDHA;
	private String oldTown;
	private String oldZone;
	private String oldDHACode;

	/** Booleans that control pop-up display */
	private boolean displayPractitionerSearchResults;
	private boolean displayEditDHA;
	private boolean displayDeleteConfirmation;

	/**
	 * Constructor. Sets up all bean reference with EJB client layer for use in the DHA portion of the application
	 */
	public DHAControllerBean() {

		// Set up EJB service
		if (this.dhaLocalService == null) {
			try {
				this.dhaLocalService = InitialContext.doLookup(prop.getProperty(DHAService.class.getName()));
				logger.debug("dhaLocalService is loaded in the constructor");
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}

		// Load all the current DHA records
		try {
			districtHealthAuthorities = dhaLocalService.getDHAs();
			// Init the Practitioner search criteria
			practitionerSearchCriteria = new PractitionerSearchCriteria();

			// Reset the state map
			if (dhaStateMap != null) {
				dhaStateMap.clear();
			}
		}
		// catch service exceptions
		catch (DHAServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		// Init validator
		if (dhaValidator == null) {
			dhaValidator = new DistrictHealthAuthorityValidator();
		}
	}

	/**
	 * Executes a search for Practitioners based on the users criteria
	 */
	public void executePractitionerSearch() {
		try {
			// Execute search
			practitionerSearchResults = dhaLocalService.getSearchedPractitioners(practitionerSearchCriteria);
			// Display results
			displayPractitionerSearchResults = true;

		} // catch service exceptions
		catch (DHAServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Adds the specified Practitioner as a new DHA record
	 * 
	 * @param aPractitioner
	 *            - practitioner record to be added to the DHA table
	 */
	public void addPractitionerDHA(Practitioner aPractitioner) {

		// Populate the DHA for edit
		selectedDHA = new DistrictHealthAuthority();
		selectedDHA.setPractitionerNumber(aPractitioner.getPractitionerNumber());
		selectedDHA.setPractitionerType(aPractitioner.getPractitionerType());
		selectedDHA.setPractitionerName(aPractitioner.getPractitionerName());
		selectedDHA.setPractitionerGroupNumber(aPractitioner.getPractitionerGroupNumber());
		selectedDHA.setPractitionerGroupDescription(aPractitioner.getPractitionerGroupDescription());

		// Set defaults for changable values..
		selectedDHA.setOriginalFte(null);
		selectedDHA.setOriginalEmpType("");
		selectedDHA.setOriginalContractTownName("");
		selectedDHA.setOriginalDhaCode("");
		selectedDHA.setOriginalDhaDescription("");
		selectedDHA.setOriginalZoneId("");
		selectedDHA.setOriginalZoneName("");
		
		// Hide search results pop-up
		displayPractitionerSearchResults = false;
		// Take user to the next step of adding a new DHa
		editDha(selectedDHA);
	}

	/**
	 * Saves the DHA record.
	 */
	public void saveDHARecord() {
		logger.debug("saveDHARecord() : Begin");

		boolean isNewRecord = selectedDHA.getDhaId() == null;
		try {
			// Also check that our record is not a duplicate of another
			if (duplicateDhaCheck(selectedDHA)) {
				// Duplicate found.. display error
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.DHA.DUPLICATE"));

				// Verify that the FTE value is the correct format.
			} else if (!selectedDHA.getFteString().matches("^([1-9]\\d?\\d?|0)(\\.\\d{1,2})?$")) {
				// Value is not in a valid format
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.DHA.FTE.INVALID"));
			} else {

				// Prior to validation convert the FTE string to the bigDecimal
				if (selectedDHA.getFteString() != null) {
					selectedDHA.setFte(new BigDecimal(selectedDHA.getFteString()));
				}

				// Validate the DHA record
				dhaValidator.validate(selectedDHA);

				// Update edit meta data
				selectedDHA.setModifiedBy(FacesUtils.getUserId());
				selectedDHA.setLastModified(new Date());

				// Persist changes to the database
				dhaLocalService.saveDHAChanges(selectedDHA);

				// Close edit pop-up
				displayEditDHA = false;
				closePopupWindowJS();

				// If record was new add it to list so user does not need to refresh the page
				if (isNewRecord) {
					districtHealthAuthorities.add(0, selectedDHA);
				}

				// Display save success message
				Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.DHA.SAVE.SUCCESSFUL");
			}
		}
		// catch validation exceptions
		catch (ValidatorException e) {
			Validator.displayValidationErrors(e);
		}
		// catch service exceptions
		catch (DHAServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		logger.debug("saveDHARecord() : End");
	}

	/**
	 * Begins the edit process for the selected DHA record
	 * 
	 * @param aDha
	 *            - DHA to be edited
	 */
	public void editDha(DistrictHealthAuthority aDha) {
		selectedDHA = aDha;
		oldTown = null;
		oldZone = null;
		oldDHACode = null;
		
		// Populate dropdowns
		try {
			// RLY!? Isn't this overkill? 
			//if (!getContractTownNames().contains(selectedDHA.getContractTownName())) {
				//selectedDHA.setZoneName("");
				//selectedDHA.setDhaDescription("");
			//}
			zoneIds = dhaLocalService.getZoneIds(selectedDHA.getContractTownName());				
			if (zoneIds.size()==0) {
				zoneIds.put("0", "UNKNOWN");
			}
			dhaCodes = dhaLocalService.getDhaCodes(selectedDHA.getContractTownName(), selectedDHA.getZoneId());
			if (dhaCodes.size()==0) {
				dhaCodes.put("0", "UNKNOWN");
			}
		} catch (DHAServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		displayEditDHA = true;
	}

	/**
	 * Triggered when the user clicks delete selected.. This triggers a confirmation popup for the user to confirm the
	 * delete
	 */
	public void deleteSelectedDhas() {

		// Only display confirmation if rows are selected
		if (!dhaStateMap.getSelected().isEmpty()) {
			displayDeleteConfirmation = true;
		} else {
			// Display error to user
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.DHA.NONE.SELECTED.DELETE"));
		}
	}

	/**
	 * Deletes the selected DistrictHealthAuthorities from the database
	 */
	public void confirmDeleteSelectedDhas() {
		try {
			// Get the selected DHA records
			@SuppressWarnings("unchecked")
			List<DistrictHealthAuthority> selectedDhasForDelete = dhaStateMap.getSelected();
			// Pass the list of objects for delete and remove them from the DB
			dhaLocalService.deleteDHAs(selectedDhasForDelete);

			// Once the delete completed successfully remove the deleted records from the page table
			for (DistrictHealthAuthority dha : selectedDhasForDelete) {
				districtHealthAuthorities.remove(dha);
			}

			// Reset table state map and close the confirmation pop up
			dhaStateMap.clear();
			displayDeleteConfirmation = false;
			closePopupWindowJS();

			// Display delete success message
			Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.DHA.DELETE.SUCCESSFUL");
		}

		// catch service exceptions
		catch (DHAServiceException e) {

			Validator.displayServiceError(e);
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.DHA.DELETE.FAILED"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}

	}

	/**
	 * Triggered when the user picks a town. This will in turn load data for Zone ID and DHA Code dropdowns
	 */
	public void contractTownChangeListener() {
		if (selectedDHA.getContractTownName() != null) {
			if (selectedDHA.getContractTownName().equals(oldTown)) {
				return;
			} else {
				oldTown = selectedDHA.getContractTownName();
				oldZone = null;
				selectedDHA.setZoneId(null);
				selectedDHA.setZoneName(null);
				oldDHACode = null;
				selectedDHA.setDhaCode(null);
				selectedDHA.setDhaDescription(null);
			}
			try {
				// Load the zoneIDs available to the selected town
				zoneIds = dhaLocalService.getZoneIds(selectedDHA.getContractTownName());				
				if (zoneIds.size()==0) {
					zoneIds.put("0", "UNKNOWN");
				}
				zoneIdChangeListener();
				dhaCodeChangeListener();
			}
			// catch service exceptions
			catch (DHAServiceException e) {

				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			}
			// catch all other system errors
			catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
	}

	/**
	 * Triggered when the user picks a zoneId. This will load the Zone Name
	 */
	public void zoneIdChangeListener() {
		if (selectedDHA.getZoneId() != null) {
			if (selectedDHA.getZoneId().equals(oldZone)) {
				return;
			} else {
				oldZone = selectedDHA.getZoneId();
				oldDHACode = null;
				selectedDHA.setDhaCode(null);
				selectedDHA.setDhaDescription(null);
			}
			try {
				// Load the DHACodes now that the user has selected a zone
				dhaCodes = dhaLocalService.getDhaCodes(selectedDHA.getContractTownName(), selectedDHA.getZoneId());
				if (dhaCodes.size()==0) {
					dhaCodes.put("0", "UNKNOWN");
				}
				// Update the ZoneName
				selectedDHA.setZoneName(zoneIds.get(selectedDHA.getZoneId()));
				dhaCodeChangeListener();
			} // catch service exceptions
			catch (DHAServiceException e) {

				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			}
			// catch all other system errors
			catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
	}

	/**
	 * Triggered when the user picks a dhaCode. This will load the DHA Name
	 */
	public void dhaCodeChangeListener() {
		if (selectedDHA.getDhaCode() != null) {
			if (selectedDHA.getDhaCode().equals(oldDHACode)) {
				return;
			} else {
				oldDHACode = selectedDHA.getDhaCode();
			}
			// Update the DHA Description
			selectedDHA.setDhaDescription(dhaCodes.get(selectedDHA.getDhaCode()));
		}
	}

	/**
	 * Closes the Practitioner search results pop-up window
	 */
	public void closePractitionerSearchResultsPopup() {
		displayPractitionerSearchResults = false;
	}

	/**
	 * Closes the edit DHA pop-up window
	 */
	public void closeEditDHAPopup() {
		displayEditDHA = false;
		closePopupWindowJS();

		// Revert any possible changes made to the DHA record (Only on editing existing records)
		if (selectedDHA.getDhaId() != null) {
			selectedDHA.setEmpType(selectedDHA.getOriginalEmpType());
			selectedDHA.setFte(selectedDHA.getOriginalFte());
			selectedDHA.setFteString(selectedDHA.getFte().toString());
			selectedDHA.setContractTownName(selectedDHA.getOriginalContractTownName());
			selectedDHA.setZoneId(selectedDHA.getOriginalZoneId());
			selectedDHA.setZoneName(selectedDHA.getOriginalZoneName());
			selectedDHA.setDhaCode(selectedDHA.getOriginalDhaCode());
			selectedDHA.setDhaDescription(selectedDHA.getOriginalDhaDescription());
		}
		
		clearForm();
	}

	/**
	 * Closes the DHA delete confirmation pop-up window
	 */
	public void closeDeleteDhaConfirmationPopup() {
		displayDeleteConfirmation = false;
		closePopupWindowJS();
	}

	// JTRAX-66 22-MAR-2018 BCAINNE
	/**
	 * Checks to make sure the provided DHA record is not a duplicate. If record is new then one instance makes it a
	 * duplicate. If editing an existing record we check for a count of 2 or more
	 * 
	 * @param aDha
	 *            - DHA record to check if it is a duplicate
	 * @return true if duplicate, false otherwise
	 */
	private boolean duplicateDhaCheck(DistrictHealthAuthority aDha) {
		int count = 0;
		// Iterate over the list to check for duplicates
		for (DistrictHealthAuthority d : districtHealthAuthorities) {
			if (aDha.equals(d)) {
				count++;
			}
		}
		
		// Determine if there is a duplicate violation
		if (aDha.getDhaId() == null) {
			// Dealing with a new DHA record (Should be 0 count)
			return count > 0;
		} else {
			// Dealing with an existing DHA record (Should be 1 count)
			return count > 1;
		}
	}
	
	

	   
	/**
	 * @return the practitionerSearchCriteria
	 */
	public PractitionerSearchCriteria getPractitionerSearchCriteria() {
		return practitionerSearchCriteria;
	}

	/**
	 * @param practitionerSearchCriteria
	 *            the practitionerSearchCriteria to set
	 */
	public void setPractitionerSearchCriteria(PractitionerSearchCriteria practitionerSearchCriteria) {
		this.practitionerSearchCriteria = practitionerSearchCriteria;
	}

	/**
	 * @return the districtHealthAuthorities
	 */
	public List<DistrictHealthAuthority> getDistrictHealthAuthorities() {
		return districtHealthAuthorities;
	}

	/**
	 * @param districtHealthAuthorities
	 *            the districtHealthAuthorities to set
	 */
	public void setDistrictHealthAuthorities(List<DistrictHealthAuthority> districtHealthAuthorities) {
		this.districtHealthAuthorities = districtHealthAuthorities;
	}

	/**
	 * @return the zoneIds
	 */
	public Map<String, String> getZoneIds() {
		return zoneIds;
	}

	/**
	 * @param zoneIds
	 *            the zoneIds to set
	 */
	public void setZoneIds(Map<String, String> zoneIds) {
		this.zoneIds = zoneIds;
	}

	/**
	 * @return the dhaCodes
	 */
	public Map<String, String> getDhaCodes() {
		return dhaCodes;
	}

	/**
	 * @param dhaCodes
	 *            the dhaCodes to set
	 */
	public void setDhaCodes(Map<String, String> dhaCodes) {
		this.dhaCodes = dhaCodes;
	}

	/**
	 * @return the dhaValidator
	 */
	public DistrictHealthAuthorityValidator getDhaValidator() {
		return dhaValidator;
	}

	/**
	 * @param dhaValidator
	 *            the dhaValidator to set
	 */
	public void setDhaValidator(DistrictHealthAuthorityValidator dhaValidator) {
		this.dhaValidator = dhaValidator;
	}

	/**
	 * @return the practitionerSearchResults
	 */
	public List<Practitioner> getPractitionerSearchResults() {
		return practitionerSearchResults;
	}

	/**
	 * @param practitionerSearchResults
	 *            the practitionerSearchResults to set
	 */
	public void setPractitionerSearchResults(List<Practitioner> practitionerSearchResults) {
		this.practitionerSearchResults = practitionerSearchResults;
	}

	/**
	 * @return the displayPractitionerSearchResults
	 */
	public boolean isDisplayPractitionerSearchResults() {
		return displayPractitionerSearchResults;
	}

	/**
	 * @param displayPractitionerSearchResults
	 *            the displayPractitionerSearchResults to set
	 */
	public void setDisplayPractitionerSearchResults(boolean displayPractitionerSearchResults) {
		this.displayPractitionerSearchResults = displayPractitionerSearchResults;
	}

	/**
	 * @return the displayEditDHA
	 */
	public boolean isDisplayEditDHA() {
		return displayEditDHA;
	}

	/**
	 * @param displayEditDHA
	 *            the displayEditDHA to set
	 */
	public void setDisplayEditDHA(boolean displayEditDHA) {
		this.displayEditDHA = displayEditDHA;
	}

	/**
	 * @return the selectedDHA
	 */
	public DistrictHealthAuthority getSelectedDHA() {
		return selectedDHA;
	}

	/**
	 * @param selectedDHA
	 *            the selectedDHA to set
	 */
	public void setSelectedDHA(DistrictHealthAuthority selectedDHA) {
		this.selectedDHA = selectedDHA;
	}

	/**
	 * @return the dhaStateMap
	 */
	public RowStateMap getDhaStateMap() {
		return dhaStateMap;
	}

	/**
	 * @param dhaStateMap
	 *            the dhaStateMap to set
	 */
	public void setDhaStateMap(RowStateMap dhaStateMap) {
		this.dhaStateMap = dhaStateMap;
	}

	/**
	 * @return the displayDeleteConfirmation
	 */
	public boolean isDisplayDeleteConfirmation() {
		return displayDeleteConfirmation;
	}

	/**
	 * @param displayDeleteConfirmation
	 *            the displayDeleteConfirmation to set
	 */
	public void setDisplayDeleteConfirmation(boolean displayDeleteConfirmation) {
		this.displayDeleteConfirmation = displayDeleteConfirmation;
	}
}

package ca.medavie.nspp.audit.ui.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.icefaces.ace.model.table.LazyDataModel;
import org.icefaces.ace.model.table.RowStateMap;
import org.icefaces.ace.model.table.SortCriteria;
import org.icefaces.util.JavaScriptRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.PeerGroupService;
import ca.medavie.nspp.audit.service.data.DataUtilities;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.PeerGroup;
import ca.medavie.nspp.audit.service.data.PeerGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.SpecialtyCriteria;
import ca.medavie.nspp.audit.service.data.SubcategoryType;
import ca.medavie.nspp.audit.service.data.TownCriteria;
import ca.medavie.nspp.audit.service.data.TownCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.exception.PeerGroupServiceException;
import ca.medavie.nspp.audit.ui.FacesUtils;
import ca.medavie.nspp.audit.ui.bean.DropdownConstantsBean;
import ca.medavie.nspp.audit.ui.controller.validator.PeerGroupValidator;
import ca.medavie.nspp.audit.ui.controller.validator.Validator;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorException;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorMessage;

/**
 * Handles all UI functionality for the PeerGroup pages.
 */
@ManagedBean(name = "peerGroupController")
@ViewScoped
public class PeerGroupControllerBean extends DropdownConstantsBean implements Serializable {

	private static final Logger logger = LoggerFactory.getLogger(PeerGroupControllerBean.class);

	// Reference to other PeerGroup managed beans
	@ManagedProperty(value = "#{bulkCopyController}")
	private BulkCopyControllerBean bulkCopyControllerBean;
	@ManagedProperty(value = "#{managePractitionersController}")
	private ManagePractitionersControllerBean managePractitionerControllerBean;

	// Holds reference to the EJB Client service class
	@EJB
	protected PeerGroupService peerGroupService;

	/** JRE generated */
	private static final long serialVersionUID = 5259831190609639454L;

	/** Object references to either a new PeerGroup or an exist record. */
	private PeerGroup selectedPeerGroup;
	/** Search criteria objects */
	private PeerGroupSearchCriteria peerGroupSearchCriteria;

	/** True if an existing PeerGroup Criteria is being updated, false if new record is being created. */
	private boolean editingRecord;

	/** Booleans that keep track of pop up display */
	private boolean displayTownSearchResults;
	private boolean displayHSCResults;
	private boolean displayPeerGroupSaveResults;

	/** Search criteria objects */
	private TownCriteriaSearchCriteria townSearchCriteria;
	private HealthServiceCriteriaSearchCriteria hscSearchCriteria;

	/** Holds references to search results */
	private LazyDataModel<PeerGroup> peerGroupSearchResults;
	private Integer totalPeerGroups;
	private List<TownCriteria> townSearchResults;
	private List<HealthServiceCriteria> healthServiceSearchResults;

	/** Controls search results display */
	private boolean displayPeerGroupSearchResults;

	/**
	 * Holds reference to either a new HealthServiceCriteria that will be added to a PeerGroup or editing an existing
	 * one.
	 */
	private HealthServiceCriteria healthServiceCriteria;

	/** Holds reference to the selected rows in the TownSearchResults table */
	private RowStateMap townsTableStateMap;
	/** Holds reference to the selected rows in the HealthServiceCriteria table */
	private RowStateMap hscTableStateMap;
	/** SelectItem list for SubcategoryType dropdown using map instead of list of selectItem */
	private Map<String, SubcategoryType> subCategorySelectItems;
	/** item selected in SubcategoryType dropdown, so far it can't take object other than"String" */
	private String selectedSubCategoryType;
	private Map<String, SpecialtyCriteria> specialtySelectItems;
	/** item selected in selectedSpecialty dropdown, so far it can't take object other than"String" */
	private String selectedSpecialty;
	/** map that holds the following four drop downs for town search criteria */
	private Map<String, BigDecimal>[] allTownSearchDropDowns;
	/** Town code name SelectItem list for drop down menu */
	private Map<String, BigDecimal> townCriteriaTCSelectItems;
	/** county code name SelectItem list for drop down menu */
	private Map<String, BigDecimal> townCriteriaCCSelectItems;
	/** municipality code name SelectItem list for drop down menu */
	private Map<String, BigDecimal> municipalityCriteriaSelectItems;
	/** Health region code name SelectItem list for drop down menu */
	private Map<String, BigDecimal> healthRegionSelectItems;

	/**
	 * All healthServiceEFDateDropDownSelectItems name for drop down menu this is set to a set of constant and only load
	 * once
	 */
	private Map<String, String> healthServiceEFDateDropDownSelectItems;
	/** healthServiceEFDateSelectItems name SelectItem list for drop down menu if there is more than one name found */
	private Map<String, String> healthServiceEFDateSelectItems;

	/** PeerGroup Validator */
	private PeerGroupValidator peerGroupValidator;

	/**
	 * PeerGroup controller constructor
	 */
	public PeerGroupControllerBean() {

		logger.info("PeerGroupControllerBean's constructor begin");

		if (this.peerGroupService == null) {
			try {
				this.peerGroupService = InitialContext.doLookup(prop.getProperty(PeerGroupService.class.getName()));
				logger.debug("peerGroupService is loaded in the constructor");

			} catch (NamingException e) {
				e.printStackTrace();
			}
		}

		// Instantiate any required variables
		peerGroupValidator = new PeerGroupValidator();
		selectedPeerGroup = new PeerGroup();
		townSearchCriteria = new TownCriteriaSearchCriteria();
		peerGroupSearchCriteria = new PeerGroupSearchCriteria();
		hscSearchCriteria = new HealthServiceCriteriaSearchCriteria();
		subCategorySelectItems = new TreeMap<String, SubcategoryType>();
		specialtySelectItems = new LinkedHashMap<String, SpecialtyCriteria>();
		townCriteriaTCSelectItems = new TreeMap<String, BigDecimal>();
		townCriteriaCCSelectItems = new TreeMap<String, BigDecimal>();
		municipalityCriteriaSelectItems = new TreeMap<String, BigDecimal>();
		healthRegionSelectItems = new TreeMap<String, BigDecimal>();
		healthServiceEFDateSelectItems = new TreeMap<String, String>(Collections.reverseOrder());

		// Populate Subcategory/Specialty maps
		subCategorySelectItems.putAll(getSubcategoryDropdownItems());
		specialtySelectItems.putAll(getSpecialtyDropdownItems());

		// Init the first panel state
		initiateSearchPeerGroupPanel();

		// Connects the LazyDataModel to the bean state for ALR search
		peerGroupSearchResults = performPeerGroupSearchLoad();
	}

	/************************************
	 ***** Navigation functionality *****
	 ************************************/
	/**
	 * 0 - Search/Edit PeerGroup, 1- Add New PeerGroup, 2 - Bulk Copy, 3 - Manage Practitioners
	 * 
	 * @param anEvent
	 *            contains the tab index requested
	 */
	public void peerGroupTabChangeListener(ValueChangeEvent anEvent) {
		// Get the index of the requested tab
		Integer requestedTabIndex = (Integer) anEvent.getNewValue();

		// Prepare backing bean for the requested tab
		if (requestedTabIndex.equals(Integer.valueOf("0"))) {
			initiateSearchPeerGroupPanel();
		} else if (requestedTabIndex.equals(Integer.valueOf("1"))) {
			initiateAddPeerGroupPanel();
		} else if (requestedTabIndex.equals(Integer.valueOf("2"))) {
			// Call BulkCopy controller bean to prepare for BulkCopy
			bulkCopyControllerBean.initiateBulkCopyPanel();
		} else if (requestedTabIndex.equals(Integer.valueOf("3"))) {
			// Call ManagePractitioner controller bean to prepare for Manage
			managePractitionerControllerBean.initiatePractitionsInGroupsPanel();
		}
	}

	/**
	 * Takes the user to the Search PeerGroup panel
	 */
	private void initiateSearchPeerGroupPanel() {
		editingRecord = false;
		selectedPeerGroup = new PeerGroup();
		townSearchCriteria = new TownCriteriaSearchCriteria();
		hscSearchCriteria = new HealthServiceCriteriaSearchCriteria();
		selectedSpecialty = null;
		selectedSubCategoryType = null;
		peerGroupSearchCriteria = new PeerGroupSearchCriteria();
		displayPeerGroupSearchResults = false;
	}

	/**
	 * Takes the user to the new PeerGroup panel
	 */
	private void initiateAddPeerGroupPanel() {
		// Loads all dropdown and other UI data
		loadPeerGroupEditUIData();
		// Turn edit mode off
		editingRecord = false;
		// Create a new PeerGroup
		selectedPeerGroup = new PeerGroup();
		// reset subCategorySelectItems types in the drop down menu, again when adding a new peer group
		subCategorySelectItems.putAll(getSubcategoryDropdownItems());
		// reset specialtySelectItems types in the drop down menu, again when adding a new peer group
		specialtySelectItems.putAll(getSpecialtyDropdownItems());
		// Reset the edit page state
		resetPeerGroupEditPageState();
	}

	/**
	 * Takes the user to the edit screen with selected records details displayed
	 * 
	 * @param aPeerGroup
	 *            - The peerGroup object to be changed.
	 */
	public void initiateEditPeerGroupPanel(PeerGroup aPeerGroup) {

		try {
			// Loads all dropdown and other UI data
			loadPeerGroupEditUIData();
			selectedPeerGroup = peerGroupService.getPeerGroupDetails(aPeerGroup);
			// Reset the edit page state
			resetPeerGroupEditPageState();

			// Display edit tab
			editingRecord = true;

			// Display the Edit form to the user
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayPeerGroupEdit()");

		} catch (PeerGroupServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Takes user back to the PeerGroup search screen from the edit screen.
	 */
	public void returnToPeerGroupSearch() {
		// Disable edit mode and clear out the selected PeerGroup
		editingRecord = false;
		selectedPeerGroup = new PeerGroup();
		// Toggle display via JS
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "returnToPeerGroupSearch()");
	}

	/****************************************
	 ***** Execute Search functionality *****
	 ****************************************/
	/**
	 * Search for TownCriterias using the provider search criteria
	 */
	public void executeTownSearch() {

		logger.debug("PeerGroupControllerBean--executeTownSearch(): townSearchCriteria is "
				+ townSearchCriteria.toString());
		// Would pass the 'townSearchCriteria' object to filter results
		// Call service and load in matching townCriteris
		townSearchResults = new ArrayList<TownCriteria>();
		try {
			townSearchResults = peerGroupService.getTownCriteriaSearchResults(townSearchCriteria);
			Collections.sort(townSearchResults);
			// Set the pop-up containing the results to display
			displayTownSearchResults = true;
			
			// Reset search criteria upon successful search
			executeTownSearchCriteriaClear();

		} // catch service exceptions
		catch (PeerGroupServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Searches for PeerGroups using the provided search criteria
	 */
	public void executePeerGroupSearch() {
		try {

			// Clear out any previously selected date. It will be set if a selection was made
			peerGroupSearchCriteria.setYearEndDate(null);

			// Apply the YearEndDate is specified
			if (peerGroupSearchCriteria.getYearEndDateString() != null) {
				Date yearEndDate = DataUtilities.SDF.parse(peerGroupSearchCriteria.getYearEndDateString());
				peerGroupSearchCriteria.setYearEndDate(yearEndDate);
			}

			// Perform search result count
			totalPeerGroups = peerGroupService.getSearchedPeerGroupsCount(peerGroupSearchCriteria);
			peerGroupSearchResults.setRowCount(totalPeerGroups);

			// Reset data table
			resetDataTableState("peerGroupEditView:searchEditPeerGroupPanel:searchForm:peerGroupSearchResultsTable");

			// Display search results
			displayPeerGroupSearchResults = true;

		} catch (PeerGroupServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Loads the search results data for Peer Group search
	 * 
	 * @return populated PeerGroup search results data model
	 */
	private LazyDataModel<PeerGroup> performPeerGroupSearchLoad() {
		peerGroupSearchResults = new LazyDataModel<PeerGroup>() {

			/** Serial Number */
			private static final long serialVersionUID = -321591594841978036L;

			@Override
			public List<PeerGroup> load(int first, int pageSize, final SortCriteria[] criteria,
					final Map<String, String> filters) {

				List<PeerGroup> resultList = new ArrayList<PeerGroup>();
				try {
					if (displayPeerGroupSearchResults) {

						// Apply any sorting to search criteria
						if (criteria.length > 0) {

							for (int x = 0; x < criteria.length; x++) {
								SortCriteria sc = criteria[x];
								peerGroupSearchCriteria.addSortArgument(PeerGroup.class, sc.getPropertyName(),
										sc.isAscending());
							}
						}

						// Load PeerGroup search results
						resultList = peerGroupService.getSearchedPeerGroups(peerGroupSearchCriteria, first, pageSize);
					}
				} catch (PeerGroupServiceException e) {
					Validator.displayServiceError(e);
					e.printStackTrace();
					logger.error(e.getMessage());

				} catch (Exception e) {
					Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.APPLICATION.FAILURE"));
					e.printStackTrace();
					logger.error(e.getMessage());
				}
				// Return results. If nothing found an empty list is returned
				return resultList;
			}
		};
		return peerGroupSearchResults;
	}

	/**
	 * Clears the PeerGroup search UI to fresh state
	 */
	public void executeClearPeerGroupSearch() {
		clearForm();

		peerGroupSearchCriteria.resetSearchCriteria();
		displayPeerGroupSearchResults = false;
		// Reset data table
		resetDataTableState("peerGroupEditView:searchEditPeerGroupPanel:searchForm:peerGroupSearchResultsTable");
	}

	/**
	 * Action to save peer group when save button clicked.
	 */
	public void executeSaveOrUpdatePeerGroup() {

		logger.debug("PeerGroupControllerBean--executeSaveOrUpdatePeerGroup(): selectedPeerGroup set in frontend is "
				+ selectedPeerGroup.toString());
		try {
			// If adding new convert the selected YearEndDate to Date object
			if (!editingRecord && selectedPeerGroup.getYearEndDateStr() != null) {
				Date yearEndDate = DataUtilities.SDF.parse(selectedPeerGroup.getYearEndDateStr());
				// Apply date to selected PeerGroup
				selectedPeerGroup.setYearEndDate(yearEndDate);
			}

			// Validate the PeerGroup object
			peerGroupValidator.validate(selectedPeerGroup);
			// Set edit meta data
			selectedPeerGroup.setModifiedBy(FacesUtils.getUserId());
			// Set the Type.. Always PROVI
			selectedPeerGroup.setProviderPeerGroupType("PROVI");
			// Save the PeerGroup
			peerGroupService.saveOrUpdatePeerGroup(selectedPeerGroup);

			if (!editingRecord) {
				// Display pop up stating success and provide user with details such as ID
				displayPeerGroupSaveResults = true;
				// Clear form and collapse panels
				this.displayHSCResults = false;
				this.displayTownSearchResults = false;
			} else {
				// Editing a record. Display save success message on page.
				Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.PEER.GROUP.SAVE.SUCCESS");
			}
			townSearchCriteria = new TownCriteriaSearchCriteria();
			peerGroupSearchCriteria = new PeerGroupSearchCriteria();
			hscSearchCriteria = new HealthServiceCriteriaSearchCriteria();
			selectedSpecialty = null;
			selectedSubCategoryType = null;

		} // catch validation exceptions
		catch (ValidatorException e) {
			Validator.displayValidationErrors(e);
		}
		// catch service exceptions
		catch (PeerGroupServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Selects all rows in the TownCriteria table. If the user has applied filters on the table the rows removed from
	 * view will not be selected
	 */
	public void executeSelectAllTownCriteria() {

		String tableId;
		if (editingRecord) {
			tableId = "peerGroupEditView:searchEditPeerGroupPanel:townSearchResultPopupForm:townCriteriaSearchResultPopup:tsTable";
		} else {
			tableId = "editPeerGroupCriteriaPanel:peerGroupAddView:townSearchResultPopupForm:townCriteriaSearchResultPopup:tsTable";
		}

		super.selectAllTableRows(tableId, townsTableStateMap, townSearchResults);
	}

	/**
	 * De-Selects all rows in the TownCriteria table. If the user has applied filters on the table the rows removed from
	 * view will not be selected
	 */
	public void executeDeselectAllTownCriteria() {

		String tableId;
		if (editingRecord) {
			tableId = "peerGroupEditView:searchEditPeerGroupPanel:townSearchResultPopupForm:townCriteriaSearchResultPopup:tsTable";
		} else {
			tableId = "editPeerGroupCriteriaPanel:peerGroupAddView:townSearchResultPopupForm:townCriteriaSearchResultPopup:tsTable";
		}

		super.unselectAllTableRows(tableId, townsTableStateMap, townSearchResults);
	}

	/**
	 * Searches for HealthServiceCriterias using the provided search criteria
	 */
	public void executeHealthServiceCriteriaSearch() {

		// Call service and load in matching healthServiceCriterias
		try {
			healthServiceSearchResults = peerGroupService.getHealthServiceCriteriaSearchResults(hscSearchCriteria);
			// Set the pop-up containing the results to display
			displayHSCResults = true;

		} // catch service exceptions
		catch (PeerGroupServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Selects all rows in the HSC table. If the user has applied filters on the table the rows removed from view will
	 * not be selected
	 */
	public void executeSelectAllHsc() {

		String tableId;
		if (editingRecord) {
			tableId = "peerGroupEditView:searchEditPeerGroupPanel:hscResultPopupForm:hscSearchResultPopup:hscDataTable";
		} else {
			tableId = "editPeerGroupCriteriaPanel:peerGroupAddView:hscResultPopupForm:hscSearchResultPopup:hscDataTable";
		}
		super.selectAllTableRows(tableId, hscTableStateMap, healthServiceSearchResults);
	}

	/**
	 * De-Selects all rows in the HSC table. If the user has applied filters on the table the rows removed from view
	 * will not be selected
	 */
	public void executeDeselectAllHsc() {

		String tableId;
		if (editingRecord) {
			tableId = "peerGroupEditView:searchEditPeerGroupPanel:hscResultPopupForm:hscSearchResultPopup:hscDataTable";
		} else {
			tableId = "editPeerGroupCriteriaPanel:peerGroupAddView:hscResultPopupForm:hscSearchResultPopup:hscDataTable";
		}

		super.unselectAllTableRows(tableId, hscTableStateMap, healthServiceSearchResults);
	}

	/********************************************
	 ***** Add/Remove objects functionality *****
	 ********************************************/
	/**
	 * Passes all selected Town rows to the active PeerGroup
	 */
	public void addSelectedTowns() {

		for (Object o : townsTableStateMap.getSelected()) {
			TownCriteria selectedTc = (TownCriteria) o;
			// need apply security context here
			selectedTc.setModifiedBy(FacesUtils.getUserId());
			// don't set it until hit save at the end.
			selectedTc.setLastModified(null);
			// don't add duplicates
			if (!selectedPeerGroup.getTownCriterias().contains(selectedTc)) {
				selectedPeerGroup.addTownCriteria(selectedTc);
			}
		}
		// Clear out the state map
		townsTableStateMap.clear();

		// Close edit pop-up
		displayTownSearchResults = false;
		closePopupWindowJS();

		// reset the pagination and table selection state
		resetDataTableState("peerGroupEditView:searchEditPeerGroupPanel:townSearchResultPopupForm:townCriteriaSearchResultPopup:tsTable");
		resetDataTableState("editPeerGroupCriteriaPanel:peerGroupAddView:townSearchResultPopupForm:townCriteriaSearchResultPopup:tsTable");
		hscTableStateMap.clear();
	}

	/**
	 * Passes all selected HSC rows to the active PeerGroup
	 */
	public void addSelectedHSC() {
		for (Object o : hscTableStateMap.getSelected()) {
			HealthServiceCriteria selectedHsc = (HealthServiceCriteria) o;
			// Set edit meta data
			selectedHsc.setModifiedBy(FacesUtils.getUserId());
			// don't set it until hit save at the end.
			selectedHsc.setLastModified(null);
			// Why is contains not working? The hash codes match!
			if (!selectedPeerGroup.getHealthServiceCriterias().contains(selectedHsc)) {
				selectedPeerGroup.addHealthServiceCriteria(selectedHsc);
			}

		}
		// Close edit pop-up
		displayHSCResults = false;
		closePopupWindowJS();

		// reset the pagination and table selection state
		resetDataTableState("peerGroupEditView:searchEditPeerGroupPanel:hscResultPopupForm:hscSearchResultPopup:hscDataTable");
		resetDataTableState("editPeerGroupCriteriaPanel:peerGroupAddView:hscResultPopupForm:hscSearchResultPopup:hscDataTable");
		hscTableStateMap.clear();
	}

	/**
	 * Removes the specified TownCriteria from the active PeerGroup record
	 * 
	 * @param aTownCriteria
	 */
	public void removeTown(TownCriteria aTownCriteria) {
		selectedPeerGroup.getTownCriterias().remove(aTownCriteria);
	}

	/**
	 * Removes the specified HealthServiceCriteria from the active PeerGroup record
	 * 
	 * @param anHsc
	 */
	public void removeHsc(HealthServiceCriteria anHsc) {
		selectedPeerGroup.getHealthServiceCriterias().remove(anHsc);
	}

	/**
	 * Remove the specified SpecialtyCriteria object from the PeerGroup
	 * 
	 * @param aSpecialtyCriteria
	 *            - The SpecialtyCriteria to be removed.
	 */
	public void removeSpecialtyCriteria(SpecialtyCriteria aSpecialtyCriteria) {
		selectedPeerGroup.getSpecialtyCriterias().remove(aSpecialtyCriteria);
	}

	/**
	 * 
	 * add a row into subcategory table when drop down selected
	 * 
	 * @param aSubcategoryType
	 */
	public void selectSubCategoryDropDown(String aSubcategoryType) {

		if ((aSubcategoryType == null) || (aSubcategoryType.equals(""))) {
			return;
		}

		// Do nothing if nothing was selected
		if (aSubcategoryType != null) {
			logger.debug("PeerGroupControllerBean -- selectSubCategoryDropDown: aSubcategoryType code is"
					+ aSubcategoryType.toString());

			SubcategoryType aSelectedSubCategoryType = getSubcategoryDropdownItems().get(aSubcategoryType);

			aSelectedSubCategoryType.setModifiedBy(FacesUtils.getUserId());
			// don't set it until hit save at the end.
			aSelectedSubCategoryType.setLastModified(null);

			if (!selectedPeerGroup.getSubcategoryTypes().contains(aSelectedSubCategoryType)) {
				selectedPeerGroup.addSubCategoryType(aSelectedSubCategoryType);
			}
		}
	}

	/**
	 * Remove the specified SubcategoryType object from the PeerGroup
	 * 
	 * @param aSubcategoryType
	 *            - The SubcategoryType to be removed.
	 */
	public void removeSubcategoryTypes(SubcategoryType aSubcategoryType) {

		logger.debug("removeSubcategoryTypes -- removeSubcategoryTypes: aSubcategoryType to be removed is"
				+ aSubcategoryType.toString());
		// remove from the table
		selectedPeerGroup.removeSubCategoryType(aSubcategoryType);
	}

	/**
	 * 
	 * add a row into SpecialtyCriteria table when drop down selected
	 * 
	 * @param aSpecialtyCriteria
	 */
	public void selectSpecialtyDropDown(String aSpecialtyCriteria) {

		if ((aSpecialtyCriteria == null) || (aSpecialtyCriteria.equals(""))) {
			return;
		}
		// Do nothing if nothing is selected
		if (aSpecialtyCriteria != null) {
			logger.debug("PeerGroupControllerBean -- selectSpecialtyDropDown: aSpecialtyCriteria code is"
					+ aSpecialtyCriteria.toString());

			// add this selected subcategory type to its parent peer group if any
			SpecialtyCriteria aSelectedSpecialtyCriteria = getSpecialtyDropdownItems().get(aSpecialtyCriteria);

			aSelectedSpecialtyCriteria.setModifiedBy(FacesUtils.getUserId());
			// don't set it until hit save at the end.
			aSelectedSpecialtyCriteria.setLastModified(null);

			// don't add to the table if there already is one
			if (!selectedPeerGroup.getSpecialtyCriterias().contains(aSelectedSpecialtyCriteria)) {
				selectedPeerGroup.addSpecialtyCriteria(aSelectedSpecialtyCriteria);
			}
		}
	}

	/********************************
	 ***** Pop-up functionality *****
	 ********************************/
	/**
	 * Hides the pop-up that displays the search results for TownCriteria
	 */
	public void hideTownCriteriaSearchResultsPopup() {

		// Close edit pop-up
		displayTownSearchResults = false;
		closePopupWindowJS();

		// Reset the Datatable state
		if (editingRecord) {
			resetDataTableState("peerGroupEditView:searchEditPeerGroupPanel:townSearchResultPopupForm:townCriteriaSearchResultPopup:tsTable");
		} else {
			resetDataTableState("editPeerGroupCriteriaPanel:peerGroupAddView:townSearchResultPopupForm:townCriteriaSearchResultPopup:tsTable");
		}
		townsTableStateMap.clear();
	}

	/**
	 * Hides the pop-up that displays teh search results for HSC
	 */
	public void hideHSCSearchResultsPoup() {
		// Hides the healthServiceCriteria search results pop-up window
		// Close edit pop-up
		displayHSCResults = false;
		closePopupWindowJS();

		// Reset the Datatable state
		if (editingRecord) {
			resetDataTableState("peerGroupEditView:searchEditPeerGroupPanel:hscResultPopupForm:hscSearchResultPopup:hscDataTable");
		} else {
			resetDataTableState("editPeerGroupCriteriaPanel:peerGroupAddView:hscResultPopupForm:hscSearchResultPopup:hscDataTable");
		}
		hscTableStateMap.clear();
	}

	/**
	 * Hides the save PeerGroup save results pop up and clears out the selected PeerGroup to allow addition of another
	 */
	public void hidePeerGroupSaveResultsPopup() {
		selectedPeerGroup = new PeerGroup();
		displayPeerGroupSaveResults = false;
		closePopupWindowJS();
		initiateAddPeerGroupPanel();
	}

	/**
	 * Loads all the required data from the service to allow the page to function. Only required on Add/Edit
	 */
	private void loadPeerGroupEditUIData() {

		try {
			townSearchCriteria.resetSearchCriteria();
			allTownSearchDropDowns = peerGroupService.getFilteredTownSCDropDowns(townSearchCriteria);
		} // catch service exceptions
		catch (PeerGroupServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
			return;
		}

		townCriteriaTCSelectItems.clear();
		townCriteriaTCSelectItems.putAll(allTownSearchDropDowns[0]);
		townCriteriaCCSelectItems.clear();
		townCriteriaCCSelectItems.putAll(allTownSearchDropDowns[1]);
		municipalityCriteriaSelectItems.clear();
		municipalityCriteriaSelectItems.putAll(allTownSearchDropDowns[2]);
		healthRegionSelectItems.clear();
		healthRegionSelectItems.putAll(allTownSearchDropDowns[3]);
	}

	/**
	 * Resets all the Dropdowns on the EditPeerGroup page. Used when editing or creating new.
	 */
	public void resetPeerGroupEditPageState() {

		// Reset dropdowns
		selectedSpecialty = "";
		selectedSubCategoryType = "";

		// Reset Data
		hscSearchCriteria.resetSearchCriteria();
		townSearchCriteria.resetSearchCriteria();
	}

	/**
	 * @return the displayHSCResults
	 */
	public boolean isDisplayHSCResults() {
		return displayHSCResults;
	}

	/**
	 * @return the displayTownSearchResults
	 */
	public boolean isDisplayTownSearchResults() {
		return displayTownSearchResults;
	}

	/**
	 * Returns the select items for the Subcategory dropdown. Options are loaded if none are loaded.
	 * 
	 * @return Map<String, SubcategoryType> subCategorySelectItems
	 */
	public Map<String, SubcategoryType> getSubCategorySelectItems() {

		return subCategorySelectItems;

	}

	public String getSelectedSubCategoryType() {
		return selectedSubCategoryType;
	}

	public void setSelectedSubCategoryType(String selectedSubCategoryType) {
		this.selectedSubCategoryType = selectedSubCategoryType;
	}

	/**
	 * @return the specialtySelectItems
	 */
	public Map<String, SpecialtyCriteria> getSpecialtySelectItems() {

		return specialtySelectItems;
	}

	/*****************************
	 ***** Getters & Setters *****
	 *****************************/
	/**
	 * @return the selectedPeerGroup
	 */
	public PeerGroup getSelectedPeerGroup() {
		return selectedPeerGroup;
	}

	/**
	 * @param selectedPeerGroup
	 *            the selectedPeerGroup to set
	 */
	public void setSelectedPeerGroup(PeerGroup selectedPeerGroup) {
		this.selectedPeerGroup = selectedPeerGroup;
	}

	/**
	 * @return the editingRecord
	 */
	public boolean isEditingRecord() {
		return editingRecord;
	}

	/**
	 * @return the healthServiceSearchResults
	 */
	public List<HealthServiceCriteria> getHealthServiceSearchResults() {
		return healthServiceSearchResults;
	}

	/**
	 * @param displayTownSearchResults
	 *            the displayTownSearchResults to set
	 */
	public void setDisplayTownSearchResults(boolean displayTownSearchResults) {
		this.displayTownSearchResults = displayTownSearchResults;
	}

	/**
	 * @return the townSearchResults
	 */
	public List<TownCriteria> getTownSearchResults() {
		return townSearchResults;
	}

	/**
	 * @return the healthServiceCriteria
	 */
	public HealthServiceCriteria getHealthServiceCriteria() {
		return healthServiceCriteria;
	}

	/**
	 * @param healthServiceCriteria
	 *            the healthServiceCriteria to set
	 */
	public void setHealthServiceCriteria(HealthServiceCriteria healthServiceCriteria) {
		this.healthServiceCriteria = healthServiceCriteria;
	}

	/**
	 * @return the townSearchCriteria
	 */
	public TownCriteriaSearchCriteria getTownSearchCriteria() {
		return townSearchCriteria;
	}

	/**
	 * @return the townsTableStateMap
	 */
	public RowStateMap getTownsTableStateMap() {
		return townsTableStateMap;
	}

	/**
	 * @param townsTableStateMap
	 *            the townsTableStateMap to set
	 */
	public void setTownsTableStateMap(RowStateMap townsTableStateMap) {
		this.townsTableStateMap = townsTableStateMap;
	}

	/**
	 * @return the hscTableStateMap
	 */
	public RowStateMap getHscTableStateMap() {
		return hscTableStateMap;
	}

	/**
	 * @param hscTableStateMap
	 *            the hscTableStateMap to set
	 */
	public void setHscTableStateMap(RowStateMap hscTableStateMap) {
		this.hscTableStateMap = hscTableStateMap;
	}

	/**
	 * @return the peerGroupSearchCriteria
	 */
	public PeerGroupSearchCriteria getPeerGroupSearchCriteria() {
		return peerGroupSearchCriteria;
	}

	/**
	 * @param peerGroupSearchCriteria
	 *            the peerGroupSearchCriteria to set
	 */
	public void setPeerGroupSearchCriteria(PeerGroupSearchCriteria peerGroupSearchCriteria) {
		this.peerGroupSearchCriteria = peerGroupSearchCriteria;
	}

	/**
	 * @return the hscSearchCriteria
	 */
	public HealthServiceCriteriaSearchCriteria getHscSearchCriteria() {
		return hscSearchCriteria;
	}

	public String getSelectedSpecialty() {
		return selectedSpecialty;
	}

	public void setSelectedSpecialty(String selectedSpecialty) {
		this.selectedSpecialty = selectedSpecialty;
	}

	/**
	 * @return the townCriteriaTCSelectItems
	 */
	public Map<String, BigDecimal> getTownCriteriaTCSelectItems() {
		return townCriteriaTCSelectItems;
	}

	/**
	 * @param townCriteriaTCSelectItems
	 *            the townCriteriaTCSelectItems to set
	 */
	public void setTownCriteriaTCSelectItems(Map<String, BigDecimal> townCriteriaTCSelectItems) {
		this.townCriteriaTCSelectItems = townCriteriaTCSelectItems;
	}

	/**
	 * @return the townCriteriaCCSelectItems
	 */
	public Map<String, BigDecimal> getTownCriteriaCCSelectItems() {
		return townCriteriaCCSelectItems;
	}

	/**
	 * @param townCriteriaCCSelectItems
	 *            the townCriteriaCCSelectItems to set
	 */
	public void setTownCriteriaCCSelectItems(Map<String, BigDecimal> townCriteriaCCSelectItems) {
		this.townCriteriaCCSelectItems = townCriteriaCCSelectItems;
	}

	public Map<String, BigDecimal> getMunicipalityCriteriaSelectItems() {
		return municipalityCriteriaSelectItems;
	}

	public void setMunicipalityCriteriaSelectItems(Map<String, BigDecimal> municipalityCriteriaSelectItems) {
		this.municipalityCriteriaSelectItems = municipalityCriteriaSelectItems;
	}

	public Map<String, BigDecimal> getHealthRegionSelectItems() {
		return healthRegionSelectItems;
	}

	public void setHealthRegionSelectItems(Map<String, BigDecimal> healthRegionSelectItems) {
		this.healthRegionSelectItems = healthRegionSelectItems;
	}

	public Map<String, BigDecimal>[] getAllTownSearchDropDowns() {
		return allTownSearchDropDowns;
	}

	public void setAllTownSearchDropDowns(Map<String, BigDecimal>[] allTownSearchDropDowns) {
		this.allTownSearchDropDowns = allTownSearchDropDowns;
	}

	public Map<String, String> getHealthServiceEFDateDropDownSelectItems() {
		return healthServiceEFDateDropDownSelectItems;
	}

	public void setHealthServiceEFDateDropDownSelectItems(Map<String, String> healthServiceEFDateDropDownSelectItems) {
		this.healthServiceEFDateDropDownSelectItems = healthServiceEFDateDropDownSelectItems;
	}

	public Map<String, String> getHealthServiceEFDateSelectItems() {
		return healthServiceEFDateSelectItems;
	}

	public void setHealthServiceEFDateSelectItems(Map<String, String> healthServiceEFDateSelectItems) {
		this.healthServiceEFDateSelectItems = healthServiceEFDateSelectItems;
	}

	/**
	 * Action event when select a town code name
	 * 
	 * @param aTownCodeName
	 *            - town code name being selcted
	 */
	public void selectTownCodeDropDownMenu(String aTownCodeName) {

		townSearchCriteria.setTownCodeName(aTownCodeName);
		Map<String, BigDecimal>[] filteredTCDropDowns;

		try {

			filteredTCDropDowns = peerGroupService.getFilteredTownSCDropDowns(townSearchCriteria);
		} // catch service exceptions
		catch (PeerGroupServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
			return;
		}

		townCriteriaTCSelectItems.clear();
		townCriteriaTCSelectItems.putAll(filteredTCDropDowns[0]);
		townCriteriaCCSelectItems.clear();
		townCriteriaCCSelectItems.putAll(filteredTCDropDowns[1]);
		municipalityCriteriaSelectItems.clear();
		municipalityCriteriaSelectItems.putAll(filteredTCDropDowns[2]);
		healthRegionSelectItems.clear();
		healthRegionSelectItems.putAll(filteredTCDropDowns[3]);

	}

	/**
	 * Action event when select a county code name
	 * 
	 * @param aCountyCodeName
	 *            - county code name being selected
	 */
	public void selectCountyCodeDropDownMenu(String aCountyCodeName) {

		townSearchCriteria.setCountyCodeName(aCountyCodeName);
		Map<String, BigDecimal>[] filteredTCDropDowns;

		try {
			filteredTCDropDowns = peerGroupService.getFilteredTownSCDropDowns(townSearchCriteria);
		} // catch service exceptions
		catch (PeerGroupServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
			return;
		}

		townCriteriaTCSelectItems.clear();
		townCriteriaTCSelectItems.putAll(filteredTCDropDowns[0]);
		townCriteriaCCSelectItems.clear();
		townCriteriaCCSelectItems.putAll(filteredTCDropDowns[1]);
		municipalityCriteriaSelectItems.clear();
		municipalityCriteriaSelectItems.putAll(filteredTCDropDowns[2]);
		healthRegionSelectItems.clear();
		healthRegionSelectItems.putAll(filteredTCDropDowns[3]);

	}

	/**
	 * Action event when select a Municipality code name
	 * 
	 * @param aMunicpalityCodeName
	 *            - Municipality code name being selected
	 */
	public void selectMunicipalityCodeDropDown(String aMunicpalityCodeName) {

		townSearchCriteria.setMunicipalityCodeName(aMunicpalityCodeName);
		Map<String, BigDecimal>[] filteredTCDropDowns;

		try {
			filteredTCDropDowns = peerGroupService.getFilteredTownSCDropDowns(townSearchCriteria);
		} // catch service exceptions
		catch (PeerGroupServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
			return;
		}

		townCriteriaTCSelectItems.clear();
		townCriteriaTCSelectItems.putAll(filteredTCDropDowns[0]);
		townCriteriaCCSelectItems.clear();
		townCriteriaCCSelectItems.putAll(filteredTCDropDowns[1]);
		municipalityCriteriaSelectItems.clear();
		municipalityCriteriaSelectItems.putAll(filteredTCDropDowns[2]);
		healthRegionSelectItems.clear();
		healthRegionSelectItems.putAll(filteredTCDropDowns[3]);

	}

	/**
	 * Action event when select a health region code name
	 * 
	 * @param aHealthRegionCodeName
	 *            - health region code name being selected
	 */
	public void selectHealthRegionCodeDropDown(String aHealthRegionCodeName) {

		townSearchCriteria.setHealthRegionCodeName(aHealthRegionCodeName);
		Map<String, BigDecimal>[] filteredTCDropDowns;

		try {
			filteredTCDropDowns = peerGroupService.getFilteredTownSCDropDowns(townSearchCriteria);
		} // catch service exceptions
		catch (PeerGroupServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
			return;
		}

		townCriteriaTCSelectItems.clear();
		townCriteriaTCSelectItems.putAll(filteredTCDropDowns[0]);
		townCriteriaCCSelectItems.clear();
		townCriteriaCCSelectItems.putAll(filteredTCDropDowns[1]);
		municipalityCriteriaSelectItems.clear();
		municipalityCriteriaSelectItems.putAll(filteredTCDropDowns[2]);
		healthRegionSelectItems.clear();
		healthRegionSelectItems.putAll(filteredTCDropDowns[3]);
	}

	/**
	 * Clear up the town criteria search form
	 * **/
	public void executeTownSearchCriteriaClear() {

		townSearchCriteria.resetSearchCriteria();

		townCriteriaTCSelectItems.clear();
		townCriteriaTCSelectItems.putAll(allTownSearchDropDowns[0]);
		townCriteriaCCSelectItems.clear();
		townCriteriaCCSelectItems.putAll(allTownSearchDropDowns[1]);
		municipalityCriteriaSelectItems.clear();
		municipalityCriteriaSelectItems.putAll(allTownSearchDropDowns[2]);
		healthRegionSelectItems.clear();
		healthRegionSelectItems.putAll(allTownSearchDropDowns[3]);

	}

	/**
	 * @return the bulkCopyControllerBean
	 */
	public BulkCopyControllerBean getBulkCopyControllerBean() {
		return bulkCopyControllerBean;
	}

	/**
	 * @param bulkCopyControllerBean
	 *            the bulkCopyControllerBean to set
	 */
	public void setBulkCopyControllerBean(BulkCopyControllerBean bulkCopyControllerBean) {
		this.bulkCopyControllerBean = bulkCopyControllerBean;
	}

	/**
	 * @return the managePractitionerControllerBean
	 */
	public ManagePractitionersControllerBean getManagePractitionerControllerBean() {
		return managePractitionerControllerBean;
	}

	/**
	 * @param managePractitionerControllerBean
	 *            the managePractitionerControllerBean to set
	 */
	public void setManagePractitionerControllerBean(ManagePractitionersControllerBean managePractitionerControllerBean) {
		this.managePractitionerControllerBean = managePractitionerControllerBean;
	}

	/**
	 * @return the displayPeerGroupSaveResults
	 */
	public boolean isDisplayPeerGroupSaveResults() {
		return displayPeerGroupSaveResults;
	}

	/**
	 * @param displayPeerGroupSaveResults
	 *            the displayPeerGroupSaveResults to set
	 */
	public void setDisplayPeerGroupSaveResults(boolean displayPeerGroupSaveResults) {
		this.displayPeerGroupSaveResults = displayPeerGroupSaveResults;
	}

	/**
	 * @return the peerGroupSearchResults
	 */
	public LazyDataModel<PeerGroup> getPeerGroupSearchResults() {
		return peerGroupSearchResults;
	}

	/**
	 * @param peerGroupSearchResults
	 *            the peerGroupSearchResults to set
	 */
	public void setPeerGroupSearchResults(LazyDataModel<PeerGroup> peerGroupSearchResults) {
		this.peerGroupSearchResults = peerGroupSearchResults;
	}

	/**
	 * @return the displayPeerGroupSearchResults
	 */
	public boolean isDisplayPeerGroupSearchResults() {
		return displayPeerGroupSearchResults;
	}

	/**
	 * @param displayPeerGroupSearchResults
	 *            the displayPeerGroupSearchResults to set
	 */
	public void setDisplayPeerGroupSearchResults(boolean displayPeerGroupSearchResults) {
		this.displayPeerGroupSearchResults = displayPeerGroupSearchResults;
	}

	/**
	 * @return the totalPeerGroups
	 */
	public Integer getTotalPeerGroups() {
		return totalPeerGroups;
	}

	/**
	 * @param totalPeerGroups
	 *            the totalPeerGroups to set
	 */
	public void setTotalPeerGroups(Integer totalPeerGroups) {
		this.totalPeerGroups = totalPeerGroups;
	}
}
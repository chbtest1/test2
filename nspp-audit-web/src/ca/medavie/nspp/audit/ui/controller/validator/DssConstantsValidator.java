package ca.medavie.nspp.audit.ui.controller.validator;

import java.util.ArrayList;
import java.util.List;

import ca.medavie.nspp.audit.service.data.DssConstantBO;

/**
 * Used for validating PractitionerProfilePeriods
 */
public class DssConstantsValidator extends Validator<DssConstantBO> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.ui.controller.validator.Validator#validate(java.lang.Object)
	 */
	@Override
	public void validate(DssConstantBO aDssConstantBO) throws ValidatorException {
		List<ValidatorMessage> errorMsgs = new ArrayList<ValidatorMessage>();

		// Throw Validator exception if errors were found
		if (!errorMsgs.isEmpty()) {
			throw new ValidatorException(errorMsgs);
		}
	}
}

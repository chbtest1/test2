package ca.medavie.nspp.audit.ui.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.icefaces.ace.model.table.LazyDataModel;
import org.icefaces.ace.model.table.RowStateMap;
import org.icefaces.ace.model.table.SortCriteria;
import org.icefaces.util.JavaScriptRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.MedicareService;
import ca.medavie.nspp.audit.service.PharmacareService;
import ca.medavie.nspp.audit.service.data.AuditCriteria;
import ca.medavie.nspp.audit.service.data.AuditCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.DIN;
import ca.medavie.nspp.audit.service.data.DinSearchCriteria;
import ca.medavie.nspp.audit.service.data.IndividualExclusion;
import ca.medavie.nspp.audit.service.data.PharmacareAuditLetterResponse;
import ca.medavie.nspp.audit.service.data.PharmacareAuditLetterResponseSearchCriteria;
import ca.medavie.nspp.audit.service.exception.MedicareServiceException;
import ca.medavie.nspp.audit.service.exception.PharmcareServiceException;
import ca.medavie.nspp.audit.ui.FacesUtils;
import ca.medavie.nspp.audit.ui.bean.DropdownConstantsBean;
import ca.medavie.nspp.audit.ui.controller.validator.IndividualExclusionValidator;
import ca.medavie.nspp.audit.ui.controller.validator.PharmacareALRValidator;
import ca.medavie.nspp.audit.ui.controller.validator.PharmacareAuditCriteriaValidator;
import ca.medavie.nspp.audit.ui.controller.validator.Validator;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorException;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorMessage;

/**
 * Backing bean for Pharmacare pages
 */
@ManagedBean(name = "pharmacareController")
@ViewScoped
public class PharmacareControllerBean extends DropdownConstantsBean implements Serializable {

	/** IDE Generated */
	private static final long serialVersionUID = -8353249088079920951L;
	private static final Logger logger = LoggerFactory.getLogger(PharmacareControllerBean.class);

	/** EJB services */
	@EJB
	protected PharmacareService pharmacareService;
	@EJB
	protected MedicareService medicareService;

	/** Only allowed Subcategory for Pharmacare Audit Criterias */
	private static final String RX_SUBCATEGORY = "RX";

	/** ******************************* */
	/** Audit Criteria screen variables */
	/** ******************************* */
	private PharmacareAuditCriteriaValidator auditCriteriaValidator;
	/**
	 * Controls display of certain fields that should be visible only when editing an existing Audit
	 */
	private boolean editMode;
	private LazyDataModel<AuditCriteria> auditCriteriaSearchResults;
	private boolean displayAuditCriteriaSearchResults;
	private Integer totalAuditCriterias;
	private AuditCriteria selectedAuditCriteria;
	private AuditCriteriaSearchCriteria auditCriteriaSearchCriteria;
	private DinSearchCriteria dinSearchCriteria;
	private boolean displayDinSearchResults;
	private List<DIN> dinSearchResults;
	private RowStateMap dinSearchResultsStateMap;

	/** ************************************** */
	/** Audit Letter Response screen variables */
	/** ************************************** */
	private PharmacareALRValidator pharmacareALRValidator;
	private LazyDataModel<PharmacareAuditLetterResponse> pharmacareAuditLetterResponses;
	private boolean displayAuditLetterResponsesSearchResults;
	private Integer totalAuditLetterResponses;
	private PharmacareAuditLetterResponse selectedFCALR;
	/** Holds the number of letters created if user provided Audit Run number for ALR search */
	private Long numberOfLettersCreated;
	private boolean displayNumberOfLettersCreated;
	private PharmacareAuditLetterResponseSearchCriteria pharmacareALRSearchCriteria;

	/** ************************************** */
	/** Individual Exclusions screen variables */
	/** ************************************** */
	private IndividualExclusionValidator individualExclusionValidator;
	/** Individual Exclusion object used for adding new records to the system. */
	private IndividualExclusion newIndividualExclusion;
	/** List that holds all of the current Individual Exclusions */
	private List<IndividualExclusion> individualExclusions;

	/**
	 * Controller constructor
	 */
	public PharmacareControllerBean() {

		// Connect to EJB service
		if (this.pharmacareService == null) {
			try {
				this.pharmacareService = InitialContext.doLookup(prop.getProperty(PharmacareService.class.getName()));
				logger.debug("pharmacareService is loaded in the constructor");
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}
		if (this.medicareService == null) {
			try {
				this.medicareService = InitialContext.doLookup(prop.getProperty(MedicareService.class.getName()));
				logger.debug("medicareService is loaded in the constructor");
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}

		// Instantiate variables
		auditCriteriaSearchCriteria = new AuditCriteriaSearchCriteria();
		dinSearchCriteria = new DinSearchCriteria();
		pharmacareALRSearchCriteria = new PharmacareAuditLetterResponseSearchCriteria();

		// Instantiate validators
		auditCriteriaValidator = new PharmacareAuditCriteriaValidator(medicareService);
		individualExclusionValidator = new IndividualExclusionValidator();
		pharmacareALRValidator = new PharmacareALRValidator();

		// Ensure state maps are cleared and reset
		dinSearchResultsStateMap = new RowStateMap();

		// Init Search edit panel since it is displayed on load
		initiateSearchEditAuditCriteriaPanel();

		// Sync the AuditCriteria search model with bean
		auditCriteriaSearchResults = performAuditCriteriaSearchLoad();

		// Sync the ALR search model with bean
		pharmacareAuditLetterResponses = performALRSearchLoad();
	}

	/**
	 * 0 - Search/Edit Audit Criteria, 1 - Add New Audit Criteria, 2 - Responses
	 * 
	 * @param anEvent
	 */
	public void pharmacareTabChangeListener(ValueChangeEvent anEvent) {
		// Get the index of the requested tab
		Integer requestedTabIndex = (Integer) anEvent.getNewValue();

		// Prepare backing bean for the requested tab
		if (requestedTabIndex.equals(Integer.valueOf("0"))) {
			initiateSearchEditAuditCriteriaPanel();
		} else if (requestedTabIndex.equals(Integer.valueOf("1"))) {
			initiateNewAuditCriteriaPanel();
		} else if (requestedTabIndex.equals(Integer.valueOf("2"))) {
			initiateSearchEditpharmacareALRPanel();
		} else if (requestedTabIndex.equals(Integer.valueOf("3"))) {
			initiateSearchEditIndExclusionsPanel();
		}
	}

	/***********************************/
	/****** Audit Criteria methods *****/
	/***********************************/

	/**
	 * Navigates the user to the Search/Edit Audit Criteria panel
	 */
	private void initiateSearchEditAuditCriteriaPanel() {

		// Instantiate variables
		auditCriteriaSearchCriteria = new AuditCriteriaSearchCriteria();
		dinSearchCriteria = new DinSearchCriteria();
		pharmacareALRSearchCriteria = new PharmacareAuditLetterResponseSearchCriteria();
		selectedAuditCriteria = new AuditCriteria();
		displayAuditCriteriaSearchResults = false;

		// User can only interact with RX type
		auditCriteriaSearchCriteria.setSubcategory(RX_SUBCATEGORY);

		// Ensure state maps are cleared and reset
		dinSearchResultsStateMap = new RowStateMap();

		// Disable edit mode
		editMode = false;
	}

	/**
	 * Navigates the user to the New Audit Criteria panel
	 */
	private void initiateNewAuditCriteriaPanel() {
		selectedAuditCriteria = new AuditCriteria();
		// User can only interact with RX type
		selectedAuditCriteria.setProviderType(RX_SUBCATEGORY);
		editMode = false;
	}

	/**
	 * Takes the user to step 2 of Audit Criteria Search
	 * 
	 * Based on the dropdown selection the appropriate search form will be displayed
	 */
	public void displayAuditCriteriaSearchForm() {

		// Do nothing if they select the option select....
		if (auditCriteriaSearchCriteria.getAuditCriteriaType() != null) {
			if (auditCriteriaSearchCriteria.getAuditCriteriaType().equals(AuditCriteriaSearchCriteria.PHARM_AUDIT_CRIT)) {
				auditCriteriaSearchCriteria.setPractitionerSearch(true);
			} else {
				auditCriteriaSearchCriteria.setPractitionerSearch(false);
			}
			// Render the search form
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayAuditCriteriaSearch()");
		}
	}

	/**
	 * Returns the user to step 1 of Audit Criteria Search
	 */
	public void returnToAuditCriteriaTypeSelection() {
		// Resets the search criteria
		auditCriteriaSearchCriteria.resetSearchCriteria();
		displayAuditCriteriaSearchResults = false;

		// User can only interact with RX type
		auditCriteriaSearchCriteria.setSubcategory(RX_SUBCATEGORY);
		// Render the Audit Criteria Type selection
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayAuditCriteriaTypeSelection()");
	}

	/**
	 * Searches for matching Practitioner AuditCriteria records using the users search criteria
	 */
	public void executeAuditCriteriaSearch() {
		try {

			// Perform search results count
			totalAuditCriterias = pharmacareService.getSearchedAuditCriteriaCount(auditCriteriaSearchCriteria);
			auditCriteriaSearchResults.setRowCount(totalAuditCriterias);

			// Display results
			displayAuditCriteriaSearchResults = true;
			// Reset table state
			resetDataTableState("searchEditAuditCriteriaView:searchAuditCriteriaPanel:auditCriteriaSearchForm:auditCriteriaSearchResultsTable");

		} catch (PharmcareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Loads the matching Audit Criteria records based on the search criteria
	 * 
	 * @return AuditCriteria records matching the user search criteria
	 */
	private LazyDataModel<AuditCriteria> performAuditCriteriaSearchLoad() {
		auditCriteriaSearchResults = new LazyDataModel<AuditCriteria>() {

			/** IDE generated */
			private static final long serialVersionUID = -767345513608096664L;

			@Override
			public List<AuditCriteria> load(int first, int pageSize, final SortCriteria[] criteria,
					final Map<String, String> filters) {

				List<AuditCriteria> results = new ArrayList<AuditCriteria>();

				try {
					if (displayAuditCriteriaSearchResults) {

						// Apply any sorting to search criteria
						if (criteria.length > 0) {

							for (int x = 0; x < criteria.length; x++) {
								SortCriteria sc = criteria[x];
								auditCriteriaSearchCriteria.addSortArgument(AuditCriteria.class, sc.getPropertyName(),
										sc.isAscending());
							}
						}

						// Load record matching search results
						results = pharmacareService.getSearchPractitionerAuditCriterias(auditCriteriaSearchCriteria,
								first, pageSize);
					}
				} catch (PharmcareServiceException e) {
					Validator.displayServiceError(e);
					e.printStackTrace();
					logger.error(e.getMessage());

				} catch (Exception e) {
					Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.APPLICATION.FAILURE"));
					e.printStackTrace();
					logger.error(e.getMessage());
				}
				// Return results. If nothing found an empty list is returned
				return results;
			}
		};
		return auditCriteriaSearchResults;
	}

	/**
	 * Assists in reseting the Audit search form. Typically we would use the super clear method but we must retain
	 * certain values through the reset due to page flow.
	 */
	public void executeClearAuditSearch() {
		clearForm();

		// Grab the state we need to carry over
		String selectedType = auditCriteriaSearchCriteria.getAuditCriteriaType();
		boolean practitionerSearch = auditCriteriaSearchCriteria.isPractitionerSearch();

		auditCriteriaSearchCriteria.resetSearchCriteria();
		displayAuditCriteriaSearchResults = false;
		// Reset table state
		resetDataTableState("searchEditAuditCriteriaView:searchAuditCriteriaPanel:auditCriteriaSearchForm:auditCriteriaSearchResultsTable");

		// User can only interact with RX type
		auditCriteriaSearchCriteria.setSubcategory(RX_SUBCATEGORY);

		// Restore state
		auditCriteriaSearchCriteria.setAuditCriteriaType(selectedType);
		auditCriteriaSearchCriteria.setPractitionerSearch(practitionerSearch);
	}

	/**
	 * Loads the data for the selected Default Audit and displays the edit screen.
	 */
	public void executeEditDefaultAuditCriteria() {

		if (auditCriteriaSearchCriteria.getAuditType() != null) {
			try {
				// Attempt to load the Default AuditCriteria. Creates new record if nothing is found
				selectedAuditCriteria = pharmacareService.getDefaultAuditCriteriaByType(auditCriteriaSearchCriteria
						.getAuditType());

				// Toggle edit flags to correct state
				editMode = true;

				// Display edit screen
				JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayEditMedAuditCriteria()");

			} catch (PharmcareServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			} catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
	}

	/**
	 * Sets the selected AuditCriteria for edit and displays the edit screen
	 * 
	 * @param anAuditCriteria
	 */
	public void editSelectedPractitionerAuditCriteria(AuditCriteria anAuditCriteria) {
		// Grab all the details of the selected record and display the edit page to the user
		try {
			selectedAuditCriteria = pharmacareService.getPractitionerAuditCriteriaById(anAuditCriteria
					.getAuditCriteriaId());

			// Set practitioner name here. Saves us from an additional SQL query
			selectedAuditCriteria.setProviderName(anAuditCriteria.getProviderName());

			// Upon successful load. Display edit screen to the user
			editMode = true;
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayEditMedAuditCriteria()");

		} catch (PharmcareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Saves changes made to the selected Audit Criteria
	 */
	public void executeAuditCriteriaSave() {
		try {
			// Validate the user provided values. This includes Practitioner existence and duplicate audit check
			auditCriteriaValidator.validate(selectedAuditCriteria);

			// Apply edit meta data
			selectedAuditCriteria.setModifiedBy(FacesUtils.getUserId());
			selectedAuditCriteria.setLastModified(new Date());
			// Perform save
			pharmacareService.savePharmicareAuditCriteria(selectedAuditCriteria);

			// Display success message
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_INFO,
					"INFO.VERIFICATION.LETTERS.MED.AUD.CRITERIA.SAVE.SUCCESSFUL"));

		} catch (ValidatorException ve) {
			Validator.displayValidationErrors(ve);
		} catch (PharmcareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Returns the user to Audit Criteria Search screen
	 */
	public void executeReturnToAuditCriteriaSearch() {
		// Reset boolean flags
		editMode = false;
		selectedAuditCriteria = new AuditCriteria();

		// Display search results if Provider search was performed
		if (auditCriteriaSearchCriteria.isPractitionerSearch()) {
			displayAuditCriteriaSearchResults = true;
		}
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "returnToEditMedAuditCriteriaSearch()");
	}

	/**
	 * Selects all rows in the DIN table. If the user has applied filters on the table the rows removed from view will
	 * not be selected
	 */
	public void executeSelectAllDins() {
		String tableId = "searchEditAuditCriteriaView:searchAuditCriteriaPanel:dinResultPopupForm:dinSearchResultPopup:dinResultsTable";
		super.selectAllTableRows(tableId, dinSearchResultsStateMap, dinSearchResults);
	}

	/**
	 * De-Selects all rows in the DIN table. If the user has applied filters on the table the rows removed from view
	 * will not be selected
	 */
	public void executeDeselectAllDins() {
		String tableId = "searchEditAuditCriteriaView:searchAuditCriteriaPanel:dinResultPopupForm:dinSearchResultPopup:dinResultsTable";
		super.unselectAllTableRows(tableId, dinSearchResultsStateMap, dinSearchResults);
	}

	/**
	 * Returns the user to Type selection for Add new Med Audit Criteria
	 */
	public void returnToAddNewAuditTypeSelection() {
		selectedAuditCriteria = new AuditCriteria();
		selectedAuditCriteria.setProviderType(RX_SUBCATEGORY);
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "returnToAddNewMedAuditCriteriaTypeSelection()");
	}

	/**
	 * Triggered when the user selects an Audit Type for Add Audit Criteria
	 */
	public void displayAddNewAuditCriteriaForm() {
		try {
			// Validate the users inputs
			auditCriteriaValidator.validateNewCriteriaParameters(selectedAuditCriteria);

			// Take user to edit screen if validation successful
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayAddNewMedAuditCriteria()");

		} catch (ValidatorException ve) {
			Validator.displayValidationErrors(ve);
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Removes the DIN from the selected Audit Criteria
	 * 
	 * @param aDin
	 */
	public void removeDinFromAuditCriteria(DIN aDin) {
		selectedAuditCriteria.getDins().remove(aDin);
	}

	/**
	 * Searches for DINs using the user provided search criteria
	 */
	public void executeDinSearch() {
		try {
			dinSearchResults = pharmacareService.getSearchDins(dinSearchCriteria);

			// Display search results
			displayDinSearchResults = true;

		} catch (PharmcareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Adds the selected DINs to the Audit Criteria
	 */
	public void addSelectedDinsToAuditCriteria() {
		@SuppressWarnings("unchecked")
		List<DIN> selectedDins = dinSearchResultsStateMap.getSelected();

		for (DIN d : selectedDins) {
			if (selectedAuditCriteria.getDins() == null) {
				selectedAuditCriteria.setDins(new ArrayList<DIN>());
			}

			// Ensure we are not adding a DIN that is already in the Audit Criteria
			boolean dupDetected = false;
			for (DIN din : selectedAuditCriteria.getDins()) {
				if (din.getDrugIdentificationNumber().equals(d.getDrugIdentificationNumber())) {
					dupDetected = true;
					break;
				}
			}

			if (!dupDetected) {
				d.setAuditCriteriaId(selectedAuditCriteria.getAuditCriteriaId());
				d.setModifiedBy(FacesUtils.getUserId());
				d.setLastModified(new Date());
				selectedAuditCriteria.getDins().add(0, d);
			}
		}
		// Close the search results pop up
		closeDinSearchResults();
	}

	/*****************************/
	/****** Response methods *****/
	/*****************************/

	/**
	 * Sets up the Search/Edit panel
	 */
	private void initiateSearchEditpharmacareALRPanel() {
		// Clear out search forms
		pharmacareALRSearchCriteria.resetSearchCriteria();
		displayAuditLetterResponsesSearchResults = false;
		// User can only interact with RX type
		pharmacareALRSearchCriteria.setPractitionerType(RX_SUBCATEGORY);
	}

	/**
	 * Search using the provided search criteria
	 */
	public void executePharmCareALRSearch() {
		logger.debug("executepharmCareALRSearch() : Begin");

		// Execute search
		try {
			// Perform search results count
			totalAuditLetterResponses = pharmacareService.getSearchedPharmacareALRCount(pharmacareALRSearchCriteria);
			pharmacareAuditLetterResponses.setRowCount(totalAuditLetterResponses);

			// Reset the count display
			displayNumberOfLettersCreated = false;
			displayAuditLetterResponsesSearchResults = true;
			// Reset table state
			resetDataTableState("editAuditLetterRespView:searchEditPharmacareALRPanel:pharmacareALRSearchForm:auditResponseResultTable");

			// load number of letters if needed
			if (pharmacareALRSearchCriteria.getAuditRunNumber() != null
					&& (pharmacareALRSearchCriteria.getAuditType() != null && !pharmacareALRSearchCriteria
							.getAuditType().isEmpty())) {

				numberOfLettersCreated = medicareService.getNumberOfLettersCreated(
						pharmacareALRSearchCriteria.getAuditRunNumber(), pharmacareALRSearchCriteria.getAuditType());

				// Set the count display to true
				displayNumberOfLettersCreated = true;
			}
		}
		// catch service exceptions
		catch (PharmcareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		logger.debug("executepharmCareALRSearch() : End");
	}

	/**
	 * Loads the matching ALR records based on the search criteria
	 * 
	 * @return ALR records matching the user search criteria
	 */
	private LazyDataModel<PharmacareAuditLetterResponse> performALRSearchLoad() {
		pharmacareAuditLetterResponses = new LazyDataModel<PharmacareAuditLetterResponse>() {

			/** IDE generated */
			private static final long serialVersionUID = -767345513608096664L;

			@Override
			public List<PharmacareAuditLetterResponse> load(int first, int pageSize, final SortCriteria[] criteria,
					final Map<String, String> filters) {

				List<PharmacareAuditLetterResponse> results = new ArrayList<PharmacareAuditLetterResponse>();

				try {
					if (displayAuditLetterResponsesSearchResults) {

						// Apply any sorting to search criteria
						if (criteria.length > 0) {

							for (int x = 0; x < criteria.length; x++) {
								SortCriteria sc = criteria[x];
								pharmacareALRSearchCriteria.addSortArgument(PharmacareAuditLetterResponse.class,
										sc.getPropertyName(), sc.isAscending());
							}
						}

						// Load record matching search results
						results = pharmacareService.getSearchedPharmacareALR(pharmacareALRSearchCriteria, first,
								pageSize);
					}
				} catch (PharmcareServiceException e) {
					Validator.displayServiceError(e);
					e.printStackTrace();
					logger.error(e.getMessage());

				} catch (Exception e) {
					Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.APPLICATION.FAILURE"));
					e.printStackTrace();
					logger.error(e.getMessage());
				}
				// Return results. If nothing found an empty list is returned
				return results;
			}
		};
		return pharmacareAuditLetterResponses;
	}

	/**
	 * reset the Responses search criteria
	 */
	public void resetPharmacareALRSearchCriteria() {
		clearForm();
		pharmacareALRSearchCriteria.resetSearchCriteria();
		displayAuditLetterResponsesSearchResults = false;
		// Reset table state
		resetDataTableState("editAuditLetterRespView:searchEditPharmacareALRPanel:pharmacareALRSearchForm:auditResponseResultTable");
		// User can only interact with RX type
		pharmacareALRSearchCriteria.setPractitionerType(RX_SUBCATEGORY);
	}

	/**
	 * Action to take after a pharm care audit letter responds record selected
	 * 
	 * @param AuditLetterResponsepharmacare
	 *            - a selected pharmacareAuditLetterResponds
	 */
	public void executeEditSelectedPCALR(PharmacareAuditLetterResponse aSelectedBO) {
		selectedFCALR = aSelectedBO;
		// Display the result table
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayEditPharmacareALRResultDetails()");
	}

	/**
	 * Go back to the Responses search page
	 */
	public void executeReturnToALRSearch() {
		pharmacareALRSearchCriteria.resetSearchCriteria();
		// User can only interact with RX type
		pharmacareALRSearchCriteria.setPractitionerType(RX_SUBCATEGORY);
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "returnToPharmaALRSearch()");
	}

	/**
	 * execute save changes to pharm care audit letter responds
	 */
	public void executeSavepharmacareALR() {

		logger.debug("executeSavepharmacareALR() : Begin");

		try {

			// Validate Pharm ALR
			pharmacareALRValidator.validate(selectedFCALR);

			// update the last modifed date and person
			selectedFCALR.setLast_modified(new Date());
			selectedFCALR.setModified_by(FacesUtils.getUserId());

			pharmacareService.savePharmacareALR(selectedFCALR);

			// Display save success message
			Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.AUDIT.RESULT.VALUE.SAVE.SUCCESSFUL");

		} catch (ValidatorException e) {
			Validator.displayValidationErrors(e);

		} catch (PharmcareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());

		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		logger.debug("executepharmCareALRSearch() : End");
	}

	/**********************************/
	/****** Ind Exclusion methods *****/
	/**********************************/

	/**
	 * Navigates the user to the Search/Edit Individual Exclusions panel
	 */
	private void initiateSearchEditIndExclusionsPanel() {
		try {
			// Reset the Exclusion datatable to default state
			resetDataTableState("editGlobalIndExclusionsView:editIndividualExclusionsPanel:individualExclusionsEditForm:individualExclusionsTable");

			// Reset any variables
			newIndividualExclusion = new IndividualExclusion();
			individualExclusions = pharmacareService.getIndividualExclusions();
		} catch (PharmcareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Adds a new Individual Exclusion to the UI. Records not saved to system until save is clicked.
	 */
	public void addNewIndividualExclusion() {
		try {
			// Look up name by Health Card Number.
			String healthCardOwnerName = medicareService.getHealthCardNumberOwnerName(newIndividualExclusion
					.getHealthCardNumber());

			if (healthCardOwnerName != null && !healthCardOwnerName.isEmpty()) {
				// Set the name to the new record
				newIndividualExclusion.setName(healthCardOwnerName);
				// Apply modified user and add to table
				newIndividualExclusion.setModifiedBy(FacesUtils.getUserId());
				individualExclusions.add(0, newIndividualExclusion);
				// Reset object to allow addition of more records
				newIndividualExclusion = new IndividualExclusion();

			} else {
				// Display error to user. Health Card Number not in system
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.MEDICARE.IND.EXCL.HEALTH.CARD.NOT.FOUND"));
			}
		} catch (MedicareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Saves changed or new Individual Exclusions
	 */
	public void executeIndividualExclusionsSave() {
		try {
			List<IndividualExclusion> exclusionsToSave = new ArrayList<IndividualExclusion>();

			for (IndividualExclusion indExcl : individualExclusions) {

				// Validate that exclusion meets all business rules
				individualExclusionValidator.validate(indExcl);

				// Check if any values changes from original
				if (indExcl.compareEffectiveFromValues() || indExcl.compareEffectiveToValues()) {

					// Set edit meta data
					indExcl.setModifiedBy(FacesUtils.getUserId());
					indExcl.setLastModified(new Date());

					// Add exclusion to list
					exclusionsToSave.add(indExcl);
				}
			}

			// Check for duplicate entries on all Exclusions
			individualExclusionValidator.checkForDuplicateExclusions(individualExclusions);

			// Submit values to save if any
			if (!exclusionsToSave.isEmpty()) {

				// Attempt save
				pharmacareService.saveIndividualExclusions(exclusionsToSave);

				// Update the original values in saved objects
				for (IndividualExclusion excl : exclusionsToSave) {
					excl.setOriginalEffectiveFrom(excl.getEffectiveFrom());
					excl.setOriginalEffectiveTo(excl.getEffectiveTo());
				}
			}
			// Display success message
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_INFO,
					"INFO.IND.EXCLUSIONS.SAVE.SUCCESSFUL"));

		} catch (PharmcareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (ValidatorException e) {
			Validator.displayValidationErrors(e);
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**************************************
	 ***** POP UP CLOSE METHODS BEGIN *****
	 **************************************/
	/**
	 * Closes the DIN search results pop up
	 */
	public void closeDinSearchResults() {
		// Reset search results table to default state
		resetDataTableState("searchEditAuditCriteriaView:searchAuditCriteriaPanel:dinResultPopupForm:dinSearchResultPopup:dinResultsTable");
		dinSearchResultsStateMap.clear();
		displayDinSearchResults = false;
		closePopupWindowJS();
	}

	/************************************
	 ***** POP UP CLOSE METHODS END *****
	 ************************************/

	public PharmacareAuditLetterResponse getSelectedFCALR() {
		return selectedFCALR;
	}

	public void setSelectedFCALR(PharmacareAuditLetterResponse selectedFCALR) {
		this.selectedFCALR = selectedFCALR;
	}

	public PharmacareAuditLetterResponseSearchCriteria getPharmacareALRSearchCriteria() {
		return pharmacareALRSearchCriteria;
	}

	public void setPharmacareALRSearchCriteria(PharmacareAuditLetterResponseSearchCriteria pharmCareALRSearchCriteria) {
		this.pharmacareALRSearchCriteria = pharmCareALRSearchCriteria;
	}

	/**
	 * @return the auditCriteriaSearchCriteria
	 */
	public AuditCriteriaSearchCriteria getAuditCriteriaSearchCriteria() {
		return auditCriteriaSearchCriteria;
	}

	/**
	 * @param auditCriteriaSearchCriteria
	 *            the auditCriteriaSearchCriteria to set
	 */
	public void setAuditCriteriaSearchCriteria(AuditCriteriaSearchCriteria auditCriteriaSearchCriteria) {
		this.auditCriteriaSearchCriteria = auditCriteriaSearchCriteria;
	}

	/**
	 * @return the selectedAuditCriteria
	 */
	public AuditCriteria getSelectedAuditCriteria() {
		return selectedAuditCriteria;
	}

	/**
	 * @param selectedAuditCriteria
	 *            the selectedAuditCriteria to set
	 */
	public void setSelectedAuditCriteria(AuditCriteria selectedAuditCriteria) {
		this.selectedAuditCriteria = selectedAuditCriteria;
	}

	/**
	 * @return the editMode
	 */
	public boolean isEditMode() {
		return editMode;
	}

	/**
	 * @param editMode
	 *            the editMode to set
	 */
	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}

	/**
	 * @return the dinSearchCriteria
	 */
	public DinSearchCriteria getDinSearchCriteria() {
		return dinSearchCriteria;
	}

	/**
	 * @param dinSearchCriteria
	 *            the dinSearchCriteria to set
	 */
	public void setDinSearchCriteria(DinSearchCriteria dinSearchCriteria) {
		this.dinSearchCriteria = dinSearchCriteria;
	}

	/**
	 * @return the displayDinSearchResults
	 */
	public boolean isDisplayDinSearchResults() {
		return displayDinSearchResults;
	}

	/**
	 * @param displayDinSearchResults
	 *            the displayDinSearchResults to set
	 */
	public void setDisplayDinSearchResults(boolean displayDinSearchResults) {
		this.displayDinSearchResults = displayDinSearchResults;
	}

	/**
	 * @return the dinSearchResults
	 */
	public List<DIN> getDinSearchResults() {
		return dinSearchResults;
	}

	/**
	 * @param dinSearchResults
	 *            the dinSearchResults to set
	 */
	public void setDinSearchResults(List<DIN> dinSearchResults) {
		this.dinSearchResults = dinSearchResults;
	}

	/**
	 * @return the dinSearchResultsStateMap
	 */
	public RowStateMap getDinSearchResultsStateMap() {
		return dinSearchResultsStateMap;
	}

	/**
	 * @param dinSearchResultsStateMap
	 *            the dinSearchResultsStateMap to set
	 */
	public void setDinSearchResultsStateMap(RowStateMap dinSearchResultsStateMap) {
		this.dinSearchResultsStateMap = dinSearchResultsStateMap;
	}

	/**
	 * @return the individualExclusions
	 */
	public List<IndividualExclusion> getIndividualExclusions() {
		return individualExclusions;
	}

	/**
	 * @param individualExclusions
	 *            the individualExclusions to set
	 */
	public void setIndividualExclusions(List<IndividualExclusion> individualExclusions) {
		this.individualExclusions = individualExclusions;
	}

	/**
	 * @return the newIndividualExclusion
	 */
	public IndividualExclusion getNewIndividualExclusion() {
		return newIndividualExclusion;
	}

	/**
	 * @param newIndividualExclusion
	 *            the newIndividualExclusion to set
	 */
	public void setNewIndividualExclusion(IndividualExclusion newIndividualExclusion) {
		this.newIndividualExclusion = newIndividualExclusion;
	}

	/**
	 * @return the individualExclusionValidator
	 */
	public IndividualExclusionValidator getIndividualExclusionValidator() {
		return individualExclusionValidator;
	}

	/**
	 * @param individualExclusionValidator
	 *            the individualExclusionValidator to set
	 */
	public void setIndividualExclusionValidator(IndividualExclusionValidator individualExclusionValidator) {
		this.individualExclusionValidator = individualExclusionValidator;
	}

	/**
	 * @return the numberOfLettersCreated
	 */
	public Long getNumberOfLettersCreated() {
		return numberOfLettersCreated;
	}

	/**
	 * @param numberOfLettersCreated
	 *            the numberOfLettersCreated to set
	 */
	public void setNumberOfLettersCreated(Long numberOfLettersCreated) {
		this.numberOfLettersCreated = numberOfLettersCreated;
	}

	/**
	 * @return the displayNumberOfLettersCreated
	 */
	public boolean isDisplayNumberOfLettersCreated() {
		return displayNumberOfLettersCreated;
	}

	/**
	 * @param displayNumberOfLettersCreated
	 *            the displayNumberOfLettersCreated to set
	 */
	public void setDisplayNumberOfLettersCreated(boolean displayNumberOfLettersCreated) {
		this.displayNumberOfLettersCreated = displayNumberOfLettersCreated;
	}

	/**
	 * @return the displayAuditCriteriaSearchResults
	 */
	public boolean isDisplayAuditCriteriaSearchResults() {
		return displayAuditCriteriaSearchResults;
	}

	/**
	 * @param displayAuditCriteriaSearchResults
	 *            the displayAuditCriteriaSearchResults to set
	 */
	public void setDisplayAuditCriteriaSearchResults(boolean displayAuditCriteriaSearchResults) {
		this.displayAuditCriteriaSearchResults = displayAuditCriteriaSearchResults;
	}

	/**
	 * @return the auditCriteriaSearchResults
	 */
	public LazyDataModel<AuditCriteria> getAuditCriteriaSearchResults() {
		return auditCriteriaSearchResults;
	}

	/**
	 * @param auditCriteriaSearchResults
	 *            the auditCriteriaSearchResults to set
	 */
	public void setAuditCriteriaSearchResults(LazyDataModel<AuditCriteria> auditCriteriaSearchResults) {
		this.auditCriteriaSearchResults = auditCriteriaSearchResults;
	}

	/**
	 * @return the totalAuditCriterias
	 */
	public Integer getTotalAuditCriterias() {
		return totalAuditCriterias;
	}

	/**
	 * @param totalAuditCriterias
	 *            the totalAuditCriterias to set
	 */
	public void setTotalAuditCriterias(Integer totalAuditCriterias) {
		this.totalAuditCriterias = totalAuditCriterias;
	}

	/**
	 * @return the pharmacareAuditLetterResponses
	 */
	public LazyDataModel<PharmacareAuditLetterResponse> getPharmacareAuditLetterResponses() {
		return pharmacareAuditLetterResponses;
	}

	/**
	 * @param pharmacareAuditLetterResponses
	 *            the pharmacareAuditLetterResponses to set
	 */
	public void setPharmacareAuditLetterResponses(
			LazyDataModel<PharmacareAuditLetterResponse> pharmacareAuditLetterResponses) {
		this.pharmacareAuditLetterResponses = pharmacareAuditLetterResponses;
	}

	/**
	 * @return the displayAuditLetterResponsesSearchResults
	 */
	public boolean isDisplayAuditLetterResponsesSearchResults() {
		return displayAuditLetterResponsesSearchResults;
	}

	/**
	 * @param displayAuditLetterResponsesSearchResults
	 *            the displayAuditLetterResponsesSearchResults to set
	 */
	public void setDisplayAuditLetterResponsesSearchResults(boolean displayAuditLetterResponsesSearchResults) {
		this.displayAuditLetterResponsesSearchResults = displayAuditLetterResponsesSearchResults;
	}

	/**
	 * @return the totalAuditLetterResponses
	 */
	public Integer getTotalAuditLetterResponses() {
		return totalAuditLetterResponses;
	}

	/**
	 * @param totalAuditLetterResponses
	 *            the totalAuditLetterResponses to set
	 */
	public void setTotalAuditLetterResponses(Integer totalAuditLetterResponses) {
		this.totalAuditLetterResponses = totalAuditLetterResponses;
	}
	
	private String getMessageByKey(String aMsgKey) {
		// Get the current locale
		Locale currentLocale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		String bundleLocation = "ca.medavie.nspp.audit.ui.messages";
		ResourceBundle messageBundle = ResourceBundle.getBundle(bundleLocation, currentLocale);
		// Return the matching message
		return messageBundle.getString(aMsgKey);
	}
	
	@SuppressWarnings("serial")
	public List<SelectItem> genderFilterOptions = new ArrayList<SelectItem>() {{
		add(new SelectItem(""));
		String maleStr = getMessageByKey(GenderRestrictions.MALE.getGenderLabel());
		String femaleStr = getMessageByKey(GenderRestrictions.FEMALE.getGenderLabel());
        add(new SelectItem(GenderRestrictions.MALE.getGenderValue(),maleStr));
        add(new SelectItem(GenderRestrictions.FEMALE.getGenderValue(),femaleStr));
    }};

	public List<SelectItem> getGenderFilterOptions() {
		return genderFilterOptions;
	}
}

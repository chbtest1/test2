package ca.medavie.nspp.audit.ui.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.OutlierCriteriaService;
import ca.medavie.nspp.audit.service.data.DataUtilities;
import ca.medavie.nspp.audit.service.data.DssMsiConstant;
import ca.medavie.nspp.audit.service.data.OutlierCriteria;
import ca.medavie.nspp.audit.service.data.PractitionerProfilePeriod;
import ca.medavie.nspp.audit.service.exception.OutlierCriteriaServiceException;
import ca.medavie.nspp.audit.ui.FacesUtils;
import ca.medavie.nspp.audit.ui.bean.DropdownConstantsBean;
import ca.medavie.nspp.audit.ui.controller.validator.OutlierCriteriaValidator;
import ca.medavie.nspp.audit.ui.controller.validator.PractitionerProfileCriteriaValidator;
import ca.medavie.nspp.audit.ui.controller.validator.PractitionerProfilePeriodValidator;
import ca.medavie.nspp.audit.ui.controller.validator.Validator;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorException;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorMessage;

@ManagedBean(name = "criteriaController")
@ViewScoped
public class CriteriaControllerBean extends DropdownConstantsBean implements Serializable {

	private static final Logger logger = LoggerFactory.getLogger(CriteriaControllerBean.class);

	/** JRE generated */
	private static final long serialVersionUID = 6580315377294148055L;

	/**
	 * The following objects hold state of either a brand new record or editing an existing
	 */
	/** Practitioner Profile Criteria object reference. */
	private List<DssMsiConstant> practitionerProfileCriteria;

	/** Practitioner Profile Period object reference */
	private PractitionerProfilePeriod practitionerPeriod;

	/** List of Periods in system */
	private List<PractitionerProfilePeriod> periods;

	/** Reference to the selected PeriodId */
	private PractitionerProfilePeriod selectedPeriod;

	private DssMsiConstant selectedPractitionerProfileCriteriaConstant;

	/** Custom comparator used to sort the Value column of the OutlierCriteria table */
	private Comparator<Object> criteriaValueComparator;

	/** Holds all available OutlierCriteria objects */
	private List<OutlierCriteria> outlierCriterias;

	/** Holds the reference to the OutlierCriteria being edited */
	private OutlierCriteria selectedOutlierCriteria;

	/** Booleans that control pop up display */
	private boolean displayPractitionerProfileCriteriaEditPopup;
	private boolean displayOutlierCriteriaEditPopup;
	private boolean displayOverlappingPeriodPopup;

	// Validators
	private PractitionerProfileCriteriaValidator profileCriteriaValidator;
	private PractitionerProfilePeriodValidator profilePeriodValidator;
	private OutlierCriteriaValidator outlierCriteriaValidator;

	// Holds reference to the EJB Client service class
	@EJB
	protected OutlierCriteriaService criteriaLocalService;

	/**
	 * Constructor. Sets up all bean reference with EJB client layer for use in the Criteria portion of the application
	 */
	public CriteriaControllerBean() {

		// Instantiate validator
		profileCriteriaValidator = new PractitionerProfileCriteriaValidator();
		profilePeriodValidator = new PractitionerProfilePeriodValidator();
		outlierCriteriaValidator = new OutlierCriteriaValidator();

		if (this.criteriaLocalService == null) {
			try {
				this.criteriaLocalService = InitialContext.doLookup(prop.getProperty(OutlierCriteriaService.class
						.getName()));
				logger.debug("criteriaLocalService is loaded in the constructor");
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}

		// Create criteria value comparator object if needed
		if (criteriaValueComparator == null) {
			criteriaValueComparator = new Comparator<Object>() {

				/**
				 * Custom converter will allow us to sort on multiple object types by treating them all as their string
				 * equivalent
				 * 
				 * @param o1
				 * @param o2
				 * @return o1 & o2 compare result
				 */
				@Override
				public int compare(Object o1, Object o2) {
					// Convert the value to String
					String value1 = String.valueOf(o1);
					String value2 = String.valueOf(o2);
					return value1.compareTo(value2);
				}
			};
		}

		// Trigger Profile Periods init since tab is displayed on load
		initiatePractitionerProfilePeriodsPanel();
	}

	/**
	 * 0 - Profile Periods, 1 - ProfileCriteria, 2 - Outlier Criteria
	 * 
	 * @param anEvent
	 */
	public void criteriaTabChangeListener(ValueChangeEvent anEvent) {
		// Get the index of the requested tab
		Integer requestedTabIndex = (Integer) anEvent.getNewValue();

		if (requestedTabIndex.equals(Integer.valueOf("0"))) {
			initiatePractitionerProfilePeriodsPanel();
		} else if (requestedTabIndex.equals(Integer.valueOf("1"))) {
			initiatePractitionerProfileCriteriaPanel();
		} else if (requestedTabIndex.equals(Integer.valueOf("2"))) {
			initiateOutlierCriteriaPanel();
		}
	}

	/**
	 * Loads all the required data for the PractitionerProfilePeriod panel. Then calls Jquery to change page for user
	 */
	private void initiatePractitionerProfilePeriodsPanel() {
		// Prepare object in the event the user wishes to add a new Period
		practitionerPeriod = new PractitionerProfilePeriod();

		// Ensure the table display is in default state
		resetDataTableState("profilePeriodsView:addPractitionerProfilePeriodsPanel:editExistingProfilePeriodsForm:periodHistoryTable");

		// Ensure we have loaded the latest Period data (Converting to list since datatables cannot use Maps)
		periods = new ArrayList<PractitionerProfilePeriod>(getProfilePeriods().values());
		//JTRAX-79
		if (periods.size() > 0) {
			  Collections.sort(periods, new Comparator<PractitionerProfilePeriod>() {
			      @Override
			      public int compare(final PractitionerProfilePeriod o1, final PractitionerProfilePeriod o2) {
			          return o2.getPeriodStart().compareTo(o1.getPeriodStart());
			      }
			  });
			}		
	}

	/**
	 * Loads all required data for the PractitionerProfileCriteria panel.
	 */
	private void initiatePractitionerProfileCriteriaPanel() {
		// Load the PractitionerProfileCriteria from the service.
		try {
			practitionerProfileCriteria = criteriaLocalService.getPractitionerProfileCriteria();

		} catch (OutlierCriteriaServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Loads all required data for the OutlierCriteria panel.
	 */
	private void initiateOutlierCriteriaPanel() {
		try {
			outlierCriterias = criteriaLocalService.getOutlierCriterias();

			// Ensure the table display is in default state
			resetDataTableState("outlierCriteriaView:outlierCriteriaForm:outlierCriteriaPanel:outlierCriteriasTable");

		} catch (OutlierCriteriaServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Clears out all fields in the 'Add New Profile Period' form
	 */
	public void clearNewPractitionerProfilePeriodForm() {
		practitionerPeriod = new PractitionerProfilePeriod();
	}

	/**
	 * Closes the Practitioner Profile Criteria edit pop up
	 */
	public void closePractitionerProfileEditPopup() {
		displayPractitionerProfileCriteriaEditPopup = false;
		closePopupWindowJS();
	}

	/**
	 * Closes the Outlier Criteria edit pop up
	 */
	public void closeOutlierEditPopup() {
		displayOutlierCriteriaEditPopup = false;
		closePopupWindowJS();
	}

	/**
	 * Closes the Overlapping Period warning pop up
	 */
	public void closeOverlappingPeriodPopup() {
		displayOverlappingPeriodPopup = false;
		closePopupWindowJS();
	}

	/**
	 * Triggered when the save button is clicked. Calls the appropriate save method
	 */
	public void savePractitionerProfileCriteria() {
		if (selectedPractitionerProfileCriteriaConstant.getConstantCode().equals("MPERIOD")) {
			updateYearEndDate();
		} else if (selectedPractitionerProfileCriteriaConstant.getConstantCode().equals("YRENDDT")) {
			updatePeriodId();
		} else {
			savePractitionerProfileCriteriaRecord(selectedPractitionerProfileCriteriaConstant, true);
		}
	}

	/**
	 * Updates the corresponding YearEndDate when PeriodID is updated.
	 */
	public void updateYearEndDate() {
		// JTRAX-52 06-FEB-18 BCAINNE
		Date yearEnd = null; 
		for (PractitionerProfilePeriod period : periods) {
			if (selectedPractitionerProfileCriteriaConstant.getConstantNumeric().equals(period.getPeriodId().intValue())) {
				yearEnd = period.getYearEnd();
			}
		}
		if (yearEnd == null) {
			yearEnd = getPeriods().get(getPeriods().size()-1).getYearEnd();
		}

		savePractitionerProfileCriteriaRecord(selectedPractitionerProfileCriteriaConstant, false);

		for (DssMsiConstant constant : practitionerProfileCriteria) {
			if (constant.getConstantCode().equals("YRENDDT")) {
				constant.setConstantDate(yearEnd);
				savePractitionerProfileCriteriaRecord(constant, true);
			}
		}
	}

	/**
	 * Updates the corresponding PeriodID when YearEndDate is updated.
	 */
	public void updatePeriodId() {

		Integer periodId = null;
		for (PractitionerProfilePeriod period : getPeriods()) {
			if (period.getYearEnd().equals(selectedPractitionerProfileCriteriaConstant.getConstantDate())) {
				periodId = new Integer(period.getPeriodId().intValue());
			}
		}

		savePractitionerProfileCriteriaRecord(selectedPractitionerProfileCriteriaConstant, false);

		for (DssMsiConstant constant : practitionerProfileCriteria) {
			if (constant.getConstantCode().equals("MPERIOD")) {
				constant.setConstantNumeric(periodId);
				savePractitionerProfileCriteriaRecord(constant, true);
			}
		}
	}

	/**
	 * Handles adding a new PractitionerProfilePeriod object.
	 */
	public void savePractitionerProfilePeriod() {
		logger.debug("savePractitionerProfilePeriod() : Begin");

		try {
			// Validate the ProfilePeriod record
			profilePeriodValidator.validate(practitionerPeriod);

			// Check if new date range overlaps other periods. If overlap detected show warning prompt
			if (profilePeriodValidator.validatePeriodDateRange(practitionerPeriod, getProfilePeriods())) {
				// Display overlap warning asking for confirmation
				displayOverlappingPeriodPopup = true;
			} else {
				// No overlap detected. Save record
				confirmPractitionerProfilePeriodSave();
			}
		} catch (ValidatorException e) {
			Validator.displayValidationErrors(e);
		}
		logger.debug("savePractitionerProfilePeriod() : Exiting");
	}

	/**
	 * Used when either there is no overlapping ProfilePeriods or the user confirmed to save even with overlap. (Step 2
	 * of saving new ProfilePeriods)
	 */
	public void confirmPractitionerProfilePeriodSave() {

		try {
			// Set edit meta data and save
			practitionerPeriod.setModifiedBy(FacesUtils.getUserId());
			practitionerPeriod.setLastModified(new Date());
			PractitionerProfilePeriod updatedBo = criteriaLocalService
					.addNewPracitionerProfilePeriod(practitionerPeriod);

			// Update the list of ProfilePeriods
			getProfilePeriods().put(updatedBo.getPeriodId(), updatedBo);

			// Add new YearEnd date to constants map
			getProfilePeriodYearEndDates().put(DataUtilities.SDF.format(updatedBo.getYearEnd()),
					DataUtilities.SDF.format(updatedBo.getYearEnd()));

			// Add to the top of the periods list to prevent the need for a reload
			periods.add(0, practitionerPeriod);

			// Save successful. Reset profilePeriod form to allow new additions
			practitionerPeriod = new PractitionerProfilePeriod();
			// Display success message
			Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.CRITERIA.PERIOD.SAVE.SUCCESS");

			// Close the confirmation pop up if displayed
			if (displayOverlappingPeriodPopup) {
				displayOverlappingPeriodPopup = false;
				closePopupWindowJS();
			}
		} catch (OutlierCriteriaServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Save changes made to the PractitionerProfileCriteria record
	 */
	public void savePractitionerProfileCriteriaRecord(DssMsiConstant record, boolean msg) {

		logger.debug("savePractitionerProfileCriteria() : Begin");
		try {
			// Validate the Profile Criteria record
			profileCriteriaValidator.validate(record);

			// Set edit meta data and save
			record.setLastModified(new Date());
			record.setModifiedBy(FacesUtils.getUserId());

			criteriaLocalService.savePractitionerProfileCriteria(record);

			// Close edit popup and display success message
			closePractitionerProfileEditPopup();
			if (msg) {
				Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.CRITERIA.PERIOD.PROFILE.SAVE.SUCCESS");
			}
		}
		// catch validation exceptions
		catch (ValidatorException e) {
			Validator.displayValidationErrors(e);
		}
		// catch service exceptions
		catch (OutlierCriteriaServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		logger.debug("savePractitionerProfileCriteria() : Exiting");
	}

	/**
	 * Saves the selected OutlierCriteria changes
	 */
	public void saveOutlierCriteriaRecord() {
		logger.debug("saveOutlierCriteriaRecord() : Begin");

		try {

			// Validate OutlierCriteria
			outlierCriteriaValidator.validate(selectedOutlierCriteria);

			// Apply edit meta data
			selectedOutlierCriteria.setLastModified(new Date());
			selectedOutlierCriteria.setModifiedBy(FacesUtils.getUserId());

			criteriaLocalService.saveOutlierCriteria(selectedOutlierCriteria);

			// Close edit popup and display success message
			closeOutlierEditPopup();
			Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.CRITERIA.OUTLIER.CRITERIA.SAVE.SUCCESS");

		} catch (ValidatorException e) {
			Validator.displayValidationErrors(e);
		} catch (OutlierCriteriaServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		logger.debug("saveOutlierCriteriaRecord() : Exiting");
	}

	/**
	 * Loads the edit popup so the user can modify the OutlierCriteria record
	 * 
	 * @param anOutlierCriteria
	 */
	public void editOutlierCriteria(OutlierCriteria anOutlierCriteria) {
		selectedOutlierCriteria = anOutlierCriteria;
		// Display edit pop up
		displayOutlierCriteriaEditPopup = true;
	}

	/**
	 * @return List<OutlierCriteria> - list of available OutlierCriterias
	 */
	public List<OutlierCriteria> getOutlierCriterias() {
		return outlierCriterias;
	}

	/**
	 * Loads the edit popup so the user can modify the PractitionerProfileCriteria record
	 * 
	 * @param constant
	 */
	public void editPractitionerProfileCriteria(DssMsiConstant aConstant) {
		selectedPractitionerProfileCriteriaConstant = aConstant;
		// Display edit pop up
		setDisplayPractitionerProfileCriteriaEditPopup(true);
	}

	/**
	 * @return the practitionerCriteria
	 */
	public List<DssMsiConstant> getPractitionerProfileCriteria() {
		return practitionerProfileCriteria;
	}

	/**
	 * @param practitionerProfileCriteria
	 *            the practitionerPs public void setPractitionerProfileCriteria(List<DssMsiConstant>
	 *            aPractitionerProfileCriteria) { this.practitionerProfileCriteria = aPractitionerProfileCriteria; }
	 * 
	 *            /**
	 * @return the practitionerPeriod
	 */
	public PractitionerProfilePeriod getPractitionerPeriod() {
		return practitionerPeriod;
	}

	/**
	 * @param practitionerPeriod
	 *            the practitionerPeriod to set
	 */
	public void setPractitionerPeriod(PractitionerProfilePeriod aPractitionerPeriod) {
		this.practitionerPeriod = aPractitionerPeriod;
	}

	/**
	 * @return the selectedPeriodID
	 */
	public PractitionerProfilePeriod getSelectedPeriod() {
		return selectedPeriod;
	}

	public DssMsiConstant getSelectedPractitionerProfileCriteriaConstant() {
		return selectedPractitionerProfileCriteriaConstant;
	}

	public void setSelectedPractitionerProfileCriteriaConstant(
			DssMsiConstant selectedPractitionerProfileCriteriaConstant) {
		this.selectedPractitionerProfileCriteriaConstant = selectedPractitionerProfileCriteriaConstant;
	}

	/**
	 * @param selectedPeriod
	 *            the selectedPeriod to set
	 */
	public void setSelectedPeriodID(PractitionerProfilePeriod selectedPeriod) {
		this.selectedPeriod = selectedPeriod;
	}

	public boolean isDisplayPractitionerProfileCriteriaEditPopup() {
		return displayPractitionerProfileCriteriaEditPopup;
	}

	public void setDisplayPractitionerProfileCriteriaEditPopup(boolean displayPractitionerProfileCriteriaEditPopup) {
		this.displayPractitionerProfileCriteriaEditPopup = displayPractitionerProfileCriteriaEditPopup;
	}

	/**
	 * @return the displayOutlierCriteriaEditPopup
	 */
	public boolean isDisplayOutlierCriteriaEditPopup() {
		return displayOutlierCriteriaEditPopup;
	}

	/**
	 * @param displayOutlierCriteriaEditPopup
	 *            the displayOutlierCriteriaEditPopup to set
	 */
	public void setDisplayOutlierCriteriaEditPopup(boolean displayOutlierCriteriaEditPopup) {
		this.displayOutlierCriteriaEditPopup = displayOutlierCriteriaEditPopup;
	}

	/**
	 * @return the selectedOutlierCriteria
	 */
	public OutlierCriteria getSelectedOutlierCriteria() {
		return selectedOutlierCriteria;
	}

	/**
	 * @param selectedOutlierCriteria
	 *            the selectedOutlierCriteria to set
	 */
	public void setSelectedOutlierCriteria(OutlierCriteria selectedOutlierCriteria) {
		this.selectedOutlierCriteria = selectedOutlierCriteria;
	}

	/**
	 * @return the displayOverlappingPeriodPopup
	 */
	public boolean isDisplayOverlappingPeriodPopup() {
		return displayOverlappingPeriodPopup;
	}

	/**
	 * @param displayOverlappingPeriodPopup
	 *            the displayOverlappingPeriodPopup to set
	 */
	public void setDisplayOverlappingPeriodPopup(boolean displayOverlappingPeriodPopup) {
		this.displayOverlappingPeriodPopup = displayOverlappingPeriodPopup;
	}

	/**
	 * @return the periods
	 */
	public List<PractitionerProfilePeriod> getPeriods() {
		return periods;
	}

	/**
	 * @param periods
	 *            the periods to set
	 */
	public void setPeriods(List<PractitionerProfilePeriod> periods) {
		this.periods = periods;
	}

	/**
	 * @return the criteriaValueComparator
	 */
	public Comparator<Object> getCriteriaValueComparator() {
		return criteriaValueComparator;
	}
}

package ca.medavie.nspp.audit.ui.controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * Simply used to manipulate the data provided in some text fields to uppercase.
 * 
 * Business requirement for some screens that drive report data
 * 
 * NOTE: ONLY USED FOR STRING INPUTS!! *****
 */
@FacesConverter("uppercaseConverter")
public class UpperCaseConverter implements Converter {

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext,
	 * javax.faces.component.UIComponent, java.lang.String)
	 */
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String aString) {

		if (aString == null || aString.trim().equals("")) {
			return null;
		}
		aString = aString.toUpperCase();
		return aString;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext,
	 * javax.faces.component.UIComponent, java.lang.Object)
	 */
	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object anObj) {

		if (anObj == null) {
			return null;
		}

		String value = (String) anObj;
		value = value.toUpperCase();
		return value;
	}
}

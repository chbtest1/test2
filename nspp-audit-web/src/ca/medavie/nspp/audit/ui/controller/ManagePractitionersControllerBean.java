package ca.medavie.nspp.audit.ui.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.icefaces.ace.model.table.LazyDataModel;
import org.icefaces.ace.model.table.RowStateMap;
import org.icefaces.ace.model.table.SortCriteria;
import org.icefaces.util.JavaScriptRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.PeerGroupService;
import ca.medavie.nspp.audit.service.data.DataUtilities;
import ca.medavie.nspp.audit.service.data.PeerGroup;
import ca.medavie.nspp.audit.service.data.PeerGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria.PractitionerTypes;
import ca.medavie.nspp.audit.service.exception.PeerGroupServiceException;
import ca.medavie.nspp.audit.ui.bean.DropdownConstantsBean;
import ca.medavie.nspp.audit.ui.bean.RoleBasedAccessSupportBean;
import ca.medavie.nspp.audit.ui.controller.validator.Validator;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorMessage;

/**
 * Backing bean for the Manage Practitioners In Groups page/panel
 */
@ManagedBean(name = "managePractitionersController")
@ViewScoped
public class ManagePractitionersControllerBean extends DropdownConstantsBean implements Serializable {

	// Holds reference to the EJB Client service class
	@EJB
	protected PeerGroupService peerGroupService;

	/** IDE generated */
	private static final long serialVersionUID = 1489317967574926041L;
	private static final Logger logger = LoggerFactory.getLogger(ManagePractitionersControllerBean.class);

	/** Holds reference to the selected PeerGroup */
	private PeerGroup selectedPeerGroup;

	/** Booleans that control pop-up display */
	private boolean displayPractitionerSearchResults;
	private boolean displayDeletePractitionersFromGroupConfirmation;

	/** List for holding search results */
	private LazyDataModel<PeerGroup> peerGroupSearchResults;
	private Integer totalPeerGroups;
	private List<Practitioner> practitionerSearchResults;

	/** Controls search results display */
	private boolean displayPeerGroupSearchResults;

	/** Monitors selected rows in tables. */
	private RowStateMap practitionerResultsStateMap;
	private RowStateMap feeForServicePractitionerStateMap;
	private RowStateMap shadowPractitionerStateMap;

	/** Used for searching PeerGroups */
	private PeerGroupSearchCriteria peerGroupSearchCriteria;
	/** Used for searching Practitioners */
	private PractitionerSearchCriteria practitionerSearchCriteria;

	/** Holds the selected YearEndDate for searching PeerGroups */
	private String selectedYearEndDate;

	/** Holds the selected value for Profile Type when searching for Practitioners.. helps for form reset */
	private PractitionerTypes typeSelection;

	/**
	 * Bean constructor
	 */
	public ManagePractitionersControllerBean() {

		if (this.peerGroupService == null) {
			try {
				this.peerGroupService = InitialContext.doLookup(prop.getProperty(PeerGroupService.class.getName()));
				logger.debug("peerGroupService is loaded in the constructor");

			} catch (NamingException e) {

				e.printStackTrace();
			}
		}

		// Set up variables
		peerGroupSearchCriteria = new PeerGroupSearchCriteria();
		practitionerSearchCriteria = new PractitionerSearchCriteria();
		selectedPeerGroup = new PeerGroup();

		// Sync lazy data model with bean state
		peerGroupSearchResults = performPeerGroupSearchLoad();
	}

	/**
	 * Takes the user to the Manage Practitioners in Groups panel
	 */
	public void initiatePractitionsInGroupsPanel() {
		// Ensure variables are in default state..
		peerGroupSearchCriteria = new PeerGroupSearchCriteria();
		practitionerSearchCriteria = new PractitionerSearchCriteria();
		displayPeerGroupSearchResults = false;
		selectedYearEndDate = null;
		// Remove default Type value
		practitionerSearchCriteria.setProfileType(null);
		selectedPeerGroup = new PeerGroup();
	}

	/**
	 * Loads the Practitioner lists for the selected PeerGroup
	 * 
	 * @param aPeerGroupToMaintain
	 *            - selected PeerGroup
	 */
	public void initiateMaintainPeerGroupPractitioners(PeerGroup aPeerGroupToMaintain) {

		try {
			selectedPeerGroup = peerGroupService.getPeerGroupDetails(aPeerGroupToMaintain);

			// Display the Manage Practitioner panel once a PeerGroup is selected
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayManagePractitionerPanel()");
		}
		// catch service exceptions
		catch (PeerGroupServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Search for PeerGroups matching the search criteria
	 */
	public void executePeerGroupSearch() {
		try {
			// Apply the YearEndDate is specified
			if (selectedYearEndDate != null) {
				Date yearEndDate = DataUtilities.SDF.parse(selectedYearEndDate);
				peerGroupSearchCriteria.setYearEndDate(yearEndDate);
			}

			// Perform PeerGroup count
			totalPeerGroups = peerGroupService.getSearchedPeerGroupsCount(peerGroupSearchCriteria);
			peerGroupSearchResults.setRowCount(totalPeerGroups);
			
			// Reset datatable
			resetDataTableState("managePractitionerInGroupView:providerPanel:managePractSearchForm:peerGroupSearchResultsTable");

			// Display search results
			displayPeerGroupSearchResults = true;
		}
		// catch service exceptions
		catch (PeerGroupServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
			return;
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
			return;
		}
	}

	/**
	 * Loads the search results data for Peer Group search
	 * 
	 * @return populated PeerGroup search results data model
	 */
	private LazyDataModel<PeerGroup> performPeerGroupSearchLoad() {
		peerGroupSearchResults = new LazyDataModel<PeerGroup>() {

			/** Serial Number */
			private static final long serialVersionUID = -321591594841978036L;

			@Override
			public List<PeerGroup> load(int first, int pageSize, final SortCriteria[] criteria,
					final Map<String, String> filters) {

				List<PeerGroup> resultList = new ArrayList<PeerGroup>();
				try {
					if (displayPeerGroupSearchResults) {

						// Apply any sorting to search criteria
						if (criteria.length > 0) {

							for (int x = 0; x < criteria.length; x++) {
								SortCriteria sc = criteria[x];
								peerGroupSearchCriteria.addSortArgument(PeerGroup.class, sc.getPropertyName(),
										sc.isAscending());
							}
						}

						// Load PeerGroup search results
						resultList = peerGroupService.getSearchedPeerGroups(peerGroupSearchCriteria, first, pageSize);
					}
				} catch (PeerGroupServiceException e) {
					Validator.displayServiceError(e);
					e.printStackTrace();
					logger.error(e.getMessage());

				} catch (Exception e) {
					Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.APPLICATION.FAILURE"));
					e.printStackTrace();
					logger.error(e.getMessage());
				}
				// Return results. If nothing found an empty list is returned
				return resultList;
			}
		};
		return peerGroupSearchResults;
	}

	/**
	 * Clears the PeerGroup search form
	 */
	public void executeClearPeerGroupSearch() {
		clearForm();
		peerGroupSearchCriteria.resetSearchCriteria();
		displayPeerGroupSearchResults = false;
		// Reset datatable
		resetDataTableState("managePractitionerInGroupView:providerPanel:managePractSearchForm:peerGroupSearchResultsTable");
	}

	/**
	 * Returns the user back to the PeerGroup search screen from edit.
	 */
	public void executeReturnToPeerGroupSearch() {
		// Re-render the search form
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "returnToManagePractitionerSearchPanel()");
	}

	/**
	 * Search Practitioners to add to the selected PeerGroup
	 */
	public void executePractitionerSearch() {
		try {

			// associate the selected selectedPeerGroup with this search criteria
			practitionerSearchCriteria.setPeerGroupId(selectedPeerGroup.getPeerGroupID());
			practitionerSearchCriteria.setYearEndDate(selectedPeerGroup.getYearEndDate());

			practitionerSearchResults = peerGroupService.getSearchedPractitioners(practitionerSearchCriteria);
		}
		// catch service exceptions
		catch (PeerGroupServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
			return;
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
			return;
		}
		// Reset the backing map for the search results table
		practitionerResultsStateMap.clear();
		// Set the pop-up containing the results to display
		displayPractitionerSearchResults = true;
	}

	/**
	 * Selects all rows in the Practitioners table. If the user has applied filters on the table the rows removed from
	 * view will not be selected
	 */
	public void executeSelectAllPractitioners() {

		String tableId = "managePractitionerInGroupView:practitionerSearchPopupForm:practitionerSearchResultPopup:practitionerSearchResultsDataTable";
		super.selectAllTableRows(tableId, practitionerResultsStateMap, practitionerSearchResults);
	}

	/**
	 * De-Selects all rows in the Practitioners table. If the user has applied filters on the table the rows removed
	 * from view will not be selected
	 */
	public void executeDeselectAllPractitioners() {

		String tableId = "managePractitionerInGroupView:practitionerSearchPopupForm:practitionerSearchResultPopup:practitionerSearchResultsDataTable";
		super.unselectAllTableRows(tableId, practitionerResultsStateMap, practitionerSearchResults);
	}

	/**
	 * Adds the selected Practitioners to the active PeerGroup
	 */
	public void addSelectedPractitionersToPeerGroup() {
		for (Object o : practitionerResultsStateMap.getSelected()) {
			Practitioner practitioner = (Practitioner) o;

			// Need to provide the missing details of the practitioner since this is a new record
			practitioner.setForcedIndicator("Y");

			// Determine if we are adding Shadow or Fee For Service practitioners
			if (practitionerSearchCriteria.getProfileType().equals(PractitionerTypes.SHADOW)) {
				if (!checkForDuplicate(selectedPeerGroup.getShadowPractitioners(), practitioner)) {
					selectedPeerGroup.getShadowPractitioners().add(0, practitioner);
				}
			} else if (practitionerSearchCriteria.getProfileType().equals(PractitionerTypes.FEE_FOR_SERVICE)) {
				if (!checkForDuplicate(selectedPeerGroup.getFeeForServicePractitioners(), practitioner)) {
					selectedPeerGroup.getFeeForServicePractitioners().add(0, practitioner);
				}
			}
		}

		// Clear out state map
		practitionerResultsStateMap.clear();

		// Upon completion close the Practitioner search results pop up
		closePractitionerSearchResultsPopup();
	}

	/**
	 * Triggered when the user clicks delete selected. This will prompt the user for confirmation
	 */
	public void deleteSelectedPractitioners() {
		// Only display confirmation if rows are selected
		if (shadowPractitionerStateMap.getSelected().isEmpty()
				&& feeForServicePractitionerStateMap.getSelected().isEmpty()) {
			// Display error to user (No rows selected)
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.NO.PRACTITIONERS.SELECTED.DELETE"));
		} else {
			displayDeletePractitionersFromGroupConfirmation = true;

		}
	}

	/**
	 * Performs the delete of Practitioners from the current PeerGroup upon delete confirmation
	 */
	public void confirmDeleteSelectedPractitioners() {

		try {
			// Remove all the selected Fee For Service Practitioners
			for (Object o : feeForServicePractitionerStateMap.getSelected()) {
				Practitioner feePractitioner = (Practitioner) o;
				selectedPeerGroup.getFeeForServicePractitioners().remove(feePractitioner);
			}

			// Remove all the selected Shadow Practitioners
			for (Object o : shadowPractitionerStateMap.getSelected()) {
				Practitioner shadowPractitioner = (Practitioner) o;
				selectedPeerGroup.getShadowPractitioners().remove(shadowPractitioner);
			}

			// Save the changes
			peerGroupService.saveOrUpdatePeerGroup(selectedPeerGroup);

			// Reset the table statemaps and hide confirmation window
			shadowPractitionerStateMap.clear();
			feeForServicePractitionerStateMap.clear();
			displayDeletePractitionersFromGroupConfirmation = false;
			closePopupWindowJS();

			// Once the delete completed successfully remove the Practitioner records from the Peer Group on the UI
			Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.PEER.GROUP.DELETE.SUCCESS");

		}

		// catch service exceptions
		catch (PeerGroupServiceException e) {
			Validator.displayServiceError(e);
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.PEER.GROUP.PRACTITIONER.DELETE.FAILED"));
			e.printStackTrace();
			logger.error(e.getMessage());
			return;
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
			return;
		}

	}

	/**
	 * Saves the changes made to the Practitioners in the selected PeerGroup
	 */
	public void executeSavePeerGroupPractitioners() {
		// JTRAX-51 06-FEB-18 BCAINNE
		try {
			RoleBasedAccessSupportBean access = new RoleBasedAccessSupportBean(); 			 
			selectedPeerGroup.setModifiedBy(access.getUserIn());
			peerGroupService.saveOrUpdatePeerGroup(selectedPeerGroup);

			// Display success message
			Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.PEERGROUP.MANAGE.GROUP.SAVE.COMPLETE");
		}

		// catch service exceptions
		catch (PeerGroupServiceException e) {
			Validator.displayServiceError(e);
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.PEER.GROUP.SAVE.FAILED"));
			e.printStackTrace();
			logger.error(e.getMessage());
			return;
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
			return;
		}

	}

	/**
	 * Takes user to the first step of searching for Practitioners
	 */
	public void displayPractitionerSearchStep1() {
		// Reset the Practitioner type selection
		practitionerSearchCriteria = new PractitionerSearchCriteria();
		// Remove default Type value
		practitionerSearchCriteria.setProfileType(null);
		// Take user to Search step 1.
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displaySearchPractitionerStep1()");
	}

	/**
	 * Take the user to the second step of searching for Practitioners
	 */
	public void displayPractitionerSearchStep2() {
		// Take user to Search step 2. If a valid selection was made. Do nothing if option is invalid
		if (practitionerSearchCriteria.getProfileType() != null) {
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displaySearchPractitionerStep2()");
			typeSelection = practitionerSearchCriteria.getProfileType();
		}
	}

	/**
	 * Method called when user closed the Practitioner results pop-up window
	 */
	public void closePractitionerSearchResultsPopup() {

		// Reset state
		resetDataTableState("managePractitionerInGroupView:practitionerSearchPopupForm:practitionerSearchResultPopup:practitionerSearchResultsDataTable");
		displayPractitionerSearchResults = false;
		closePopupWindowJS();
	}

	/**
	 * Closes the delete Practitioners from group confirmation pop-up window
	 */
	public void closeDeletePractitionersFromGroupPopup() {
		displayDeletePractitionersFromGroupConfirmation = false;
		closePopupWindowJS();
	}

	/**
	 * Clear the Practitioner search form used to add Practitioners to a group
	 */
	public void executeClearPractitionerSearch() {
		practitionerSearchCriteria.resetSearchCriteria();
		// Set the type back to the users choice
		practitionerSearchCriteria.setProfileType(typeSelection);
	}

	/**
	 * Determines if the provided list of Practitioners already contains the new Practitioner passed
	 * 
	 * @param aListOfPractitioners
	 * @param aPractitioner
	 * @return true if new Practitioner is a duplicate, false otherwise
	 */
	private boolean checkForDuplicate(List<Practitioner> aListOfPractitioners, Practitioner aPractitioner) {

		for (Practitioner p : aListOfPractitioners) {
			if (p.getPractitionerNumber().equals(aPractitioner.getPractitionerNumber())) {
				// Duplicate detected! Return true
				return true;
			}
		}
		return false;
	}

	/**
	 * @return declared enums in PractitionerSearchCriteria for PractitionerType dropdown options
	 */
	public PractitionerTypes[] getPractitionerTypes() {
		return PractitionerTypes.values();
	}

	/**
	 * @return the peerGroupSearchCriteria
	 */
	public PeerGroupSearchCriteria getPeerGroupSearchCriteria() {
		return peerGroupSearchCriteria;
	}

	/**
	 * @param peerGroupSearchCriteria
	 *            the peerGroupSearchCriteria to set
	 */
	public void setPeerGroupSearchCriteria(PeerGroupSearchCriteria peerGroupSearchCriteria) {
		this.peerGroupSearchCriteria = peerGroupSearchCriteria;
	}

	/**
	 * @return the practitionerSearchCriteria
	 */
	public PractitionerSearchCriteria getPractitionerSearchCriteria() {
		return practitionerSearchCriteria;
	}

	/**
	 * @param practitionerSearchCriteria
	 *            the practitionerSearchCriteria to set
	 */
	public void setPractitionerSearchCriteria(PractitionerSearchCriteria practitionerSearchCriteria) {
		this.practitionerSearchCriteria = practitionerSearchCriteria;
	}

	/**
	 * @return the displayPractitionerSearchResults
	 */
	public boolean isDisplayPractitionerSearchResults() {
		return displayPractitionerSearchResults;
	}

	/**
	 * @param displayPractitionerSearchResults
	 *            the displayPractitionerSearchResults to set
	 */
	public void setDisplayPractitionerSearchResults(boolean displayPractitionerSearchResults) {
		this.displayPractitionerSearchResults = displayPractitionerSearchResults;
	}

	/**
	 * @return the practitionerSearchResults
	 */
	public List<Practitioner> getPractitionerSearchResults() {
		return practitionerSearchResults;
	}

	/**
	 * @param practitionerSearchResults
	 *            the practitionerSearchResults to set
	 */
	public void setPractitionerSearchResults(List<Practitioner> practitionerSearchResults) {
		this.practitionerSearchResults = practitionerSearchResults;
	}

	/**
	 * @return the practitionerResultsStateMap
	 */
	public RowStateMap getPractitionerResultsStateMap() {
		return practitionerResultsStateMap;
	}

	/**
	 * @param practitionerResultsStateMap
	 *            the practitionerResultsStateMap to set
	 */
	public void setPractitionerResultsStateMap(RowStateMap practitionerResultsStateMap) {
		this.practitionerResultsStateMap = practitionerResultsStateMap;
	}

	/**
	 * @return the feeForServicePractitionerStateMap
	 */
	public RowStateMap getFeeForServicePractitionerStateMap() {
		return feeForServicePractitionerStateMap;
	}

	/**
	 * @param feeForServicePractitionerStateMap
	 *            the feeForServicePractitionerStateMap to set
	 */
	public void setFeeForServicePractitionerStateMap(RowStateMap feeForServicePractitionerStateMap) {
		this.feeForServicePractitionerStateMap = feeForServicePractitionerStateMap;
	}

	/**
	 * @return the shadowPractitionerStateMap
	 */
	public RowStateMap getShadowPractitionerStateMap() {
		return shadowPractitionerStateMap;
	}

	/**
	 * @param shadowPractitionerStateMap
	 *            the shadowPractitionerStateMap to set
	 */
	public void setShadowPractitionerStateMap(RowStateMap shadowPractitionerStateMap) {
		this.shadowPractitionerStateMap = shadowPractitionerStateMap;
	}

	/**
	 * @return the selectedPeerGroup
	 */
	public PeerGroup getSelectedPeerGroup() {
		return selectedPeerGroup;
	}

	/**
	 * @param selectedPeerGroup
	 *            the selectedPeerGroup to set
	 */
	public void setSelectedPeerGroup(PeerGroup selectedPeerGroup) {
		this.selectedPeerGroup = selectedPeerGroup;
	}

	/**
	 * @return the selectedYearEndDate
	 */
	public String getSelectedYearEndDate() {
		return selectedYearEndDate;
	}

	/**
	 * @param selectedYearEndDate
	 *            the selectedYearEndDate to set
	 */
	public void setSelectedYearEndDate(String selectedYearEndDate) {
		this.selectedYearEndDate = selectedYearEndDate;
	}

	/**
	 * @return the displayDeletePractitionersFromGroupConfirmation
	 */
	public boolean isDisplayDeletePractitionersFromGroupConfirmation() {
		return displayDeletePractitionersFromGroupConfirmation;
	}

	/**
	 * @param displayDeletePractitionersFromGroupConfirmation
	 *            the displayDeletePractitionersFromGroupConfirmation to set
	 */
	public void setDisplayDeletePractitionersFromGroupConfirmation(
			boolean displayDeletePractitionersFromGroupConfirmation) {
		this.displayDeletePractitionersFromGroupConfirmation = displayDeletePractitionersFromGroupConfirmation;
	}

	/**
	 * @return the peerGroupSearchResults
	 */
	public LazyDataModel<PeerGroup> getPeerGroupSearchResults() {
		return peerGroupSearchResults;
	}

	/**
	 * @param peerGroupSearchResults
	 *            the peerGroupSearchResults to set
	 */
	public void setPeerGroupSearchResults(LazyDataModel<PeerGroup> peerGroupSearchResults) {
		this.peerGroupSearchResults = peerGroupSearchResults;
	}

	/**
	 * @return the totalPeerGroups
	 */
	public Integer getTotalPeerGroups() {
		return totalPeerGroups;
	}

	/**
	 * @param totalPeerGroups
	 *            the totalPeerGroups to set
	 */
	public void setTotalPeerGroups(Integer totalPeerGroups) {
		this.totalPeerGroups = totalPeerGroups;
	}

	/**
	 * @return the displayPeerGroupSearchResults
	 */
	public boolean isDisplayPeerGroupSearchResults() {
		return displayPeerGroupSearchResults;
	}

	/**
	 * @param displayPeerGroupSearchResults
	 *            the displayPeerGroupSearchResults to set
	 */
	public void setDisplayPeerGroupSearchResults(boolean displayPeerGroupSearchResults) {
		this.displayPeerGroupSearchResults = displayPeerGroupSearchResults;
	}
}

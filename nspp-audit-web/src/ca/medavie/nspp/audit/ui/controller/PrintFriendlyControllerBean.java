package ca.medavie.nspp.audit.ui.controller;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import ca.medavie.nspp.audit.service.data.DataUtilities;
import ca.medavie.nspp.audit.service.data.PeerGroup;
import ca.medavie.nspp.audit.service.data.Practitioner;

/**
 * This session bean holds state of object being used on printer friendly beans. Session scope has to be used due to the
 * use of a new tab for print view.
 */
@ManagedBean(name = "printFriendlyController")
@SessionScoped
public class PrintFriendlyControllerBean implements Serializable {

	/** IDE Generated */
	private static final long serialVersionUID = 5065496003579126499L;

	// Objects that hold state for print friendly pages
	private PeerGroup selectedPeerGroup;
	private Practitioner selectedPractitioner;

	/**
	 * @return the selectedPeerGroup
	 */
	public PeerGroup getSelectedPeerGroup() {
		return selectedPeerGroup;
	}

	/**
	 * @param selectedPeerGroup
	 *            the selectedPeerGroup to set
	 */
	public void setSelectedPeerGroup(PeerGroup selectedPeerGroup) {
		this.selectedPeerGroup = selectedPeerGroup;
	}

	/**
	 * @return the selectedPractitioner
	 */
	public Practitioner getSelectedPractitioner() {
		return selectedPractitioner;
	}

	/**
	 * @param selectedPractitioner
	 *            the selectedPractitioner to set
	 */
	public void setSelectedPractitioner(Practitioner selectedPractitioner) {
		this.selectedPractitioner = selectedPractitioner;
	}

	/**
	 * Current system time & Date
	 * 
	 * @return current system time & Date
	 */
	public String getCurrentDateTime() {
		Date current = Calendar.getInstance().getTime();
		return DataUtilities.SDF_TIME.format(current);
	}
}

package ca.medavie.nspp.audit.ui.controller;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.icefaces.ace.model.table.RowStateMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.DssGLNumbersService;
import ca.medavie.nspp.audit.service.data.DssGLNumberBO;
import ca.medavie.nspp.audit.service.exception.DssGLNumbersServiceException;
import ca.medavie.nspp.audit.ui.FacesUtils;
import ca.medavie.nspp.audit.ui.bean.DropdownConstantsBean;
import ca.medavie.nspp.audit.ui.controller.validator.DssGLNumbersValidator;
import ca.medavie.nspp.audit.ui.controller.validator.Validator;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorException;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorMessage;

/**
 * Backing bean for the Edit DSS GL Numbers page.
 */
@ManagedBean(name = "dssGLNumbersController")
@ViewScoped
public class DssGLNumbersControllerBean extends DropdownConstantsBean implements Serializable {

	/** IDE Generated */
	private static final long serialVersionUID = 4415333942054019587L;
	/** Logger for class */
	private static final Logger logger = LoggerFactory.getLogger(DssGLNumbersControllerBean.class);

	/** Holds all the existing GL Numbers */
	private List<DssGLNumberBO> glNumberBOs;

	/** Holds reference to the GL Number being edited */
	private DssGLNumberBO selectedGLNumberForEdit;
	/** Holds reference to the GL Number being added */
	private DssGLNumberBO selectedGLNumberForAdd;

	/** State map for GL Numbers table */
	private RowStateMap glNumberStateMap;

	/** Controls the display of pop ups */
	private boolean displayEditPopup;
	private boolean displayDeleteConfirmPopup;

	/** DSS GL Number validator */
	private DssGLNumbersValidator glNumbersValidator;

	@EJB
	protected DssGLNumbersService dssGLNumbersService;

	public DssGLNumbersControllerBean() {

		// Load DSS GL service
		if (dssGLNumbersService == null) {
			try {
				this.dssGLNumbersService = InitialContext
						.doLookup(prop.getProperty(DssGLNumbersService.class.getName()));

				logger.debug("dssGLNumbersService is loaded in the constructor");
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}

		// Instantiate the validator
		glNumbersValidator = new DssGLNumbersValidator(dssGLNumbersService);
		
		// Create new object for adding a GL record
		selectedGLNumberForAdd = new DssGLNumberBO();

		// Load all existing GL Numbers for display on UI
		try {
			glNumberBOs = dssGLNumbersService.getGLNumbers();

		} catch (DssGLNumbersServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Displays the edit popup to the user for the selected record
	 * 
	 * @param aDssGLNumberForEdit
	 */
	public void executeEditSelectedGLNumber(DssGLNumberBO aDssGLNumberForEdit) {

		selectedGLNumberForEdit = aDssGLNumberForEdit;
		// Display the edit form
		displayEditPopup = true;
	}

	/**
	 * Saves the new GL Number record to the system
	 */
	public void executeSaveNewGLNumber() {

		// Save changes to record
		try {

			// Verify the GL number provided does not already exist.
			glNumbersValidator.validate(selectedGLNumberForAdd);

			// Update the the modified date and save
			selectedGLNumberForAdd.setLastModified(new Date());
			selectedGLNumberForAdd.setModifiedBy(FacesUtils.getUserId());
			dssGLNumbersService.saveGLNumber(selectedGLNumberForAdd);

			// Add to the top of the table
			glNumberBOs.add(0, selectedGLNumberForAdd);
			
			// Reset object to add another
			selectedGLNumberForAdd = new DssGLNumberBO();

			// Display success message
			Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.DSS.GL.NUMBER.SAVE.SUCCESSFUL");

		} catch (ValidatorException e) {
			Validator.displayValidationErrors(e);

		} catch (DssGLNumbersServiceException e) {
			Validator.displayServiceError(e);
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.DSS.GL.NUMBER.SAVE.FAILED"));
			e.printStackTrace();
			logger.error(e.getMessage());

		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Saves the GL Number the user just edited
	 */
	public void executeSaveGLNumber() {

		// Save changes to record
		try {

			// Update the the modified date and save
			selectedGLNumberForEdit.setLastModified(new Date());
			selectedGLNumberForEdit.setModifiedBy(FacesUtils.getUserId());
			dssGLNumbersService.saveGLNumber(selectedGLNumberForEdit);

			// Close the pop up
			closeEditGLNumberPopup();

			// Display success message
			Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.DSS.GL.NUMBER.SAVE.SUCCESSFUL");

		} catch (DssGLNumbersServiceException e) {
			Validator.displayServiceError(e);
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.DSS.GL.NUMBER.SAVE.FAILED"));
			e.printStackTrace();
			logger.error(e.getMessage());

		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Performs the delete action following user confirmation for GL Numbers
	 */
	public void executeDeleteSelectedGLNumbers() {

		try {
			// Get the selected records from the state map
			@SuppressWarnings("unchecked")
			List<DssGLNumberBO> glNumbersToDelete = glNumberStateMap.getSelected();

			// Perform the delete
			dssGLNumbersService.deleteGLNumbers(glNumbersToDelete);

			// Remove the deleted records from the GL table so user does not need to refresh page
			for (DssGLNumberBO gl : glNumbersToDelete) {
				glNumberBOs.remove(gl);
			}

			// Close pop up
			closeDeleteConfirmationPopup();
			// Display confirmation message
			Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.DSS.GL.NUMBER.DELETE.SUCCESSFUL");

		} catch (DssGLNumbersServiceException e) {
			Validator.displayServiceError(e);
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.DSS.GL.NUMBER.DELETE.FAILED"));
			e.printStackTrace();
			logger.error(e.getMessage());

		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Displays delete confirmation to the user.
	 */
	public void displayDeleteConfirmationPopup() {

		if (glNumberStateMap.getSelected().isEmpty()) {
			// Display error to user (No rows selected)
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.NO.DSS.GL.NUMBERS.SELECTED.DELETE"));
		} else {
			displayDeleteConfirmPopup = true;
		}
	}

	/**
	 * Closes the Add/Edit GL Number pop up window
	 */
	public void closeEditGLNumberPopup() {
		displayEditPopup = false;
		closePopupWindowJS();
	}

	/**
	 * Closes the Confirm delete action pop up window
	 */
	public void closeDeleteConfirmationPopup() {
		displayDeleteConfirmPopup = false;
		closePopupWindowJS();
	}

	/**
	 * @return the displayEditPopup
	 */
	public boolean isDisplayEditPopup() {
		return displayEditPopup;
	}

	/**
	 * @param displayEditPopup
	 *            the displayEditPopup to set
	 */
	public void setDisplayEditPopup(boolean displayEditPopup) {
		this.displayEditPopup = displayEditPopup;
	}

	/**
	 * @return the displayDeleteConfirmPopup
	 */
	public boolean isDisplayDeleteConfirmPopup() {
		return displayDeleteConfirmPopup;
	}

	/**
	 * @param displayDeleteConfirmPopup
	 *            the displayDeleteConfirmPopup to set
	 */
	public void setDisplayDeleteConfirmPopup(boolean displayDeleteConfirmPopup) {
		this.displayDeleteConfirmPopup = displayDeleteConfirmPopup;
	}

	/**
	 * @return the glNumberStateMap
	 */
	public RowStateMap getGlNumberStateMap() {
		return glNumberStateMap;
	}

	/**
	 * @param glNumberStateMap
	 *            the glNumberStateMap to set
	 */
	public void setGlNumberStateMap(RowStateMap glNumberStateMap) {
		this.glNumberStateMap = glNumberStateMap;
	}

	/**
	 * @return the glNumberBOs
	 */
	public List<DssGLNumberBO> getGlNumberBOs() {
		return glNumberBOs;
	}

	/**
	 * @param glNumberBOs
	 *            the glNumberBOs to set
	 */
	public void setGlNumberBOs(List<DssGLNumberBO> glNumberBOs) {
		this.glNumberBOs = glNumberBOs;
	}

	/**
	 * @return the selectedGLNumberForEdit
	 */
	public DssGLNumberBO getSelectedGLNumberForEdit() {
		return selectedGLNumberForEdit;
	}

	/**
	 * @param selectedGLNumberForEdit
	 *            the selectedGLNumberForEdit to set
	 */
	public void setSelectedGLNumberForEdit(DssGLNumberBO selectedGLNumberForEdit) {
		this.selectedGLNumberForEdit = selectedGLNumberForEdit;
	}

	/**
	 * @return the selectedGLNumberForAdd
	 */
	public DssGLNumberBO getSelectedGLNumberForAdd() {
		return selectedGLNumberForAdd;
	}

	/**
	 * @param selectedGLNumberForAdd
	 *            the selectedGLNumberForAdd to set
	 */
	public void setSelectedGLNumberForAdd(DssGLNumberBO selectedGLNumberForAdd) {
		this.selectedGLNumberForAdd = selectedGLNumberForAdd;
	}
}

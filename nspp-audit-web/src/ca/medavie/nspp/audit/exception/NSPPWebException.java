/**
 * 
 */
package ca.medavie.nspp.audit.exception;

/**
 * Custom exception for use in the NSPP audit web.
 */
public class NSPPWebException extends Exception {

	/** JRE generated */
	private static final long serialVersionUID = -3851133562045069032L;

	/**
	 * 
	 */
	public NSPPWebException() {
	}

	/**
	 * @param message
	 */
	public NSPPWebException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public NSPPWebException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public NSPPWebException(String message, Throwable cause) {
		super(message, cause);
	}

}

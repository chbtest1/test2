/**
 * 
 */
package ca.medavie.nspp.audit.security;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.security.auth.Subject;

import ca.medavie.nspp.audit.exception.NSPPWebException;

import com.ibm.websphere.security.auth.WSSubject;
import com.ibm.websphere.security.cred.WSCredential;

/**
 * @author Chris Matthews (bccmatt)
 * 
 */
public class RoleBasedSecurityUtil {
	private static Properties cache = null;

	/**
	 * 
	 */
	public RoleBasedSecurityUtil() {
		super();
	}

	/**
	 * Determines if the currently logged in user can perform a particular action. This returns a Map based on the
	 * current user's roles and the contents of the access-config.properties file found on the classpath.
	 * 
	 * The Map has: Key: an action name that can be done on the screen Value: True or False. This defaults to 'False'
	 * for any entitlements not found in access-config.properties (in other words, it defaults "closed")
	 * 
	 * An example of how to use this... Let's say your access-config.properties file contains these entries:
	 * PractitionerControllerBean.search=DSSAuditRole PractitionerControllerBean.abc=XYZ,DSSAuditRole
	 * PractitionerControllerBean.xyz=XYZ
	 * 
	 * ...and your user only has the DSSAuditRole
	 * 
	 * Inspecting the above, it's logical that your user should be able to 'search' and 'abc' on the
	 * PractitionerControllerBean.
	 * 
	 * This method will actually return a Map that looks like this: search ==> TRUE abc ==> TRUE anything else ==> FALSE
	 * 
	 * Given this, you can lock-down elements on the UI as follows:
	 * 
	 * SHOULD DISPLAY: <mbs:commandButton value="Search" action="#{providerController.search}"
	 * rendered="#{providerController.canDo['search']}"/>
	 * 
	 * SHOULD NOT DISPLAY: <mbs:commandButton value="DoNotDisplay" rendered="#{providerController.canDo['xyz']}"/>
	 * 
	 * SHOULD DISPLAY: <mbs:commandButton value="ShouldDisplay" rendered="#{providerController.canDo['abc']}"/>
	 * 
	 * @return A Map, as described above.
	 * @throws NSPPWebException
	 */
	public static Map<String, Boolean> generateEntitlementsMap(Class<?> forBean) throws NSPPWebException {
		Map<String, Boolean> toRet = null;

		Set<String> roles = getUserRoles();

		// read the access-config file...
		Properties accessConfig = readProperties("access-config.properties");
		toRet = parseEntitlementsForRoles(accessConfig, roles, forBean);

		return toRet;
	}

	/**
	 * Given a Properties file for access-config, parse out the entitlements for a user with a particular set of roles
	 * for a particular class
	 * 
	 * @param accessConfig
	 *            The loaded Proeprties file
	 * @param roles
	 *            The roles the user has
	 * @param forBean
	 *            The bean who is asking for entitlements.
	 * @return
	 * @throws NSPPWebException
	 */
	private static Map<String, Boolean> parseEntitlementsForRoles(Properties accessConfig, Set<String> roles,
			Class<?> forBean) throws NSPPWebException {
		Map<String, Boolean> toRet = new DefaultClosedEntitlementsMap<String, Boolean>();

		Map<String, List<String>> entitlements = parseEntitlementsForBean(accessConfig, forBean);

		// now we know which actions are controlled via role-based security on the bean.
		// which ones can our user do?
		for (String entitlement : entitlements.keySet()) {
			List<String> entitledRoles = entitlements.get(entitlement);

			// Do I have one of those roles?
			for (String role : entitledRoles) {
				if (roles.contains(role)) {
					// yup! I am entitled to perform this action...
					toRet.put(entitlement, Boolean.TRUE);
					break;
				}
			}
		}

		return toRet;
	}

	/**
	 * Obtain only the entitlements for a particular Bean
	 * 
	 * @param accessConfig
	 *            The Properties object containing the contents of access-config.properties
	 * @param forBean
	 *            The class we are asking for
	 * 
	 * @return A Map that has: Key: the entitlement name (eg, if the access-config has ABC.search, and forBean is ABC,
	 *         the key is 'search') Value: A List of roles entitled to perform the action described by the Key (eg, if
	 *         access-config has ABC.search=X,Y,Z you will get a List containing A, B and C items.
	 */
	private static Map<String, List<String>> parseEntitlementsForBean(Properties accessConfig, Class<?> forBean) {
		Map<String, List<String>> toRet = new HashMap<String, List<String>>();

		String beanName = forBean.getSimpleName();

		Set<String> keys = accessConfig.stringPropertyNames();
		for (String key : keys) {
			String keyBeanName = parseEntitlementBeanName(key);
			String keyActionName = parseEntitlementActionName(key);

			if (keyBeanName.equals(beanName)) {
				// we have a hit...
				String entitledRoles = accessConfig.getProperty(key);

				// parse the roles...
				List<String> roles = parseEntitledRoles(entitledRoles);
				toRet.put(keyActionName, roles);
			}
		}

		return toRet;

	}

	/**
	 * Parse the comma-separated roles String into a List<String>
	 * 
	 * @param roles
	 * @return
	 */
	private static List<String> parseEntitledRoles(String roles) {
		List<String> toRet = new ArrayList<String>();

		String[] parsed = roles.split(",");
		for (int i = 0; i < parsed.length; i++) {
			toRet.add(parsed[i]);
		}

		return toRet;
	}

	/**
	 * Given an entitlement key like "MyClass.action", parse out the "MyClass" part and return it.
	 * 
	 * @param key
	 * @return
	 */
	private static String parseEntitlementBeanName(String key) {
		String toRet = null;

		if (key.contains(".")) {
			// the last period should be regarded as the separator between the bean name and the action name
			int idx = key.lastIndexOf(".");
			toRet = key.substring(0, idx);
		} else {
			toRet = key;
		}

		return toRet;
	}

	/**
	 * Given an entitlement key like "MyClass.action", parse out the "action" part and return it.
	 * 
	 * @param key
	 * @return
	 */
	private static String parseEntitlementActionName(String key) {
		String toRet = null;

		if (key.contains(".")) {
			// the last period should be regarded as the separator between the bean name and the action name
			int idx = key.lastIndexOf(".");
			toRet = key.substring(idx + 1);
		} else {
			// no action???
			toRet = "";
		}

		return toRet;
	}

	/**
	 * Read a properties file off the classpath and cache it for later.
	 * 
	 * @param filename
	 * @return
	 * @throws NSPPWebException
	 */
	private static synchronized Properties readProperties(String filename) throws NSPPWebException {
		Properties toRet = RoleBasedSecurityUtil.cache;
		if (toRet == null) {
			toRet = new Properties();
			InputStream is = null;
			try {
				is = RoleBasedSecurityUtil.class.getResourceAsStream("/" + filename);
				toRet.load(is);
			} catch (FileNotFoundException e) {
				throw new NSPPWebException(e);
			} catch (IOException e) {
				throw new NSPPWebException(e);
			}

			RoleBasedSecurityUtil.cache = toRet;
		}

		return toRet;
	}

	/**
	 * Private method to list the groups for the currently authenticated user. The groups are parsed of domain
	 * information leaving only the group name.
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Set<String> getUserRoles() {
		// LOG.debug("Listing User's Groups...");
		Set<String> groupSet = new HashSet<String>();
		try {
			Subject subject = WSSubject.getCallerSubject();
			if (subject != null) {
				Set<WSCredential> credSet = subject.getPublicCredentials(WSCredential.class);
				// set should contain exactly one WSCredential
				if (credSet.size() > 1) {
					throw new RuntimeException("Expectedone WSCredential, found " + credSet.size());
				}
				if (credSet.isEmpty()) {
					throw new RuntimeException("Found no credentials");
				}
				WSCredential creds = (WSCredential) credSet.iterator().next();
				List<String> credsList = creds.getGroupIds();
				// LOG.debug("==================Start Printing Groups==========================");
				for (String aGroup : credsList) {
					// LOG.debug("Group: " + aGroup);
					int start = aGroup.indexOf("cn=") + 3;
					int end = aGroup.indexOf(",", start);
					groupSet.add(aGroup.substring(start, end));
					// LOG.debug("Parsed Group:" + aGroup.substring(start,end));

				}
				// LOG.debug("==================End Printing Groups==========================");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return groupSet;
	}
}

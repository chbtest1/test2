package ca.medavie.nspp.audit.security;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

public class CustomExceptionHandlerFactory extends ExceptionHandlerFactory {
	   private ExceptionHandlerFactory parent;
	 
	   // this injection handles jsf
	   public CustomExceptionHandlerFactory(ExceptionHandlerFactory parent) {
	    this.parent = parent;
	   }
	 
	    @Override
	    public ExceptionHandler getExceptionHandler() {
	 
	    	
	    	ExceptionHandler eh = parent.getExceptionHandler();
	        ExceptionHandler handler = new MyFullAjaxExceptionHander(eh);
	 
	        return handler;
	    }
	 
	}

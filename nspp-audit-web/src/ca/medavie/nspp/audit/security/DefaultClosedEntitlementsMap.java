/**
 * 
 */
package ca.medavie.nspp.audit.security;

import java.util.HashMap;

/**
 * @author bccmatt
 *
 */
public class DefaultClosedEntitlementsMap<K, V> extends HashMap<K, V> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1544432447969568028L;

	@SuppressWarnings("unchecked")
	@Override
	public V get(Object key) {
		V value = super.get(key);
		if (value == null) {
			//default closed...
			value = (V) Boolean.FALSE;
		}
		
		return value;
	}

	
}

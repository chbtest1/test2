package ca.medavie.nspp.audit.service;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import ca.medavie.nspp.audit.service.data.DistrictHealthAuthority;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.exception.DHAServiceException;

/**
 * Service layer interface for DHA business logic. Connects the front end controllers to the EJB layer.
 */
@Local
public interface DHAService {

	/**
	 * Loads all existing DHA(District Health Authority) records
	 * 
	 * @return list of DHAs
	 * @throws DHAServiceException
	 */
	public List<DistrictHealthAuthority> getDHAs() throws DHAServiceException;

	/**
	 * Loads the matching Practitioner using the user search criteria
	 * 
	 * @param aSearchCriteria
	 * @return list of matching Practitioners
	 * @throws DHAServiceException
	 */
	public List<Practitioner> getSearchedPractitioners(PractitionerSearchCriteria aSearchCriteria)
			throws DHAServiceException;

	/**
	 * Save the changes made to a DHA record to the DB.
	 * 
	 * @param aDha
	 *            - the DHA record that was changed
	 * @throws DHAServiceException
	 */
	public void saveDHAChanges(DistrictHealthAuthority aDha) throws DHAServiceException;

	/**
	 * Deletes the provided DHA records from the database
	 * 
	 * @param aListOfDhasToDelete
	 *            - DHAs to be deleted
	 * @throws DHAServiceException
	 */
	public void deleteDHAs(List<DistrictHealthAuthority> aListOfDhasToDelete) throws DHAServiceException;

	/**
	 * Updates the provided DHA object with the correct ContractTown details. (Additional data is required from the
	 * database: Zone Name/DHA Description)
	 * 
	 * @param aDha
	 *            - the DistrictHealthAuthority object being updated with new ContractTown data
	 * @throws DHAServiceException
	 */
	public void updateDHAContractTownDetails(DistrictHealthAuthority aDha) throws DHAServiceException;

	/**
	 * Loads the available ZoneIds along with their description for the given town
	 * 
	 * @param aTownName
	 * @return map<ZoneId, ZoneDescription>
	 * @throws DHAServiceException
	 */
	public Map<String, String> getZoneIds(String aTownName) throws DHAServiceException;

	/**
	 * 
	 * Loads the available DhaCodes along with their description for the given town and zone
	 * 
	 * @param aTownName
	 * @param aZoneId
	 * @return map<dhaCode, dhaName>
	 * @throws DHAServiceException
	 */
	public Map<String, String> getDhaCodes(String aTownName, String aZoneId) throws DHAServiceException;
}

package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.ejb.Local;

import ca.medavie.nspp.audit.service.data.DssMsiConstant;
import ca.medavie.nspp.audit.service.data.OutlierCriteria;
import ca.medavie.nspp.audit.service.data.PractitionerProfilePeriod;
import ca.medavie.nspp.audit.service.exception.OutlierCriteriaServiceException;

/**
 * Service layer interface for OutlierCriteria business logic. Connects the front end controllers to the EJB layer.
 */
@Local
public interface OutlierCriteriaService {

	/**
	 * 
	 * Get all OutlierCriteria objects
	 * 
	 * @return - List: a list of OutLierCriteriaBO
	 */
	public List<OutlierCriteria> getOutlierCriterias() throws OutlierCriteriaServiceException;

	/**
	 * Get all PractitionerPeriods
	 * 
	 * @return A List of PractitionerPeriod
	 */
	public List<PractitionerProfilePeriod> getPractitionerPeriods() throws OutlierCriteriaServiceException;

	/**
	 * Gets the details for the PractitionerProfileCriteria
	 * 
	 * @return an up to date PractitionerProfileCriteria.
	 */
	public List<DssMsiConstant> getPractitionerProfileCriteria() throws OutlierCriteriaServiceException;

	/**
	 * Save changes made to PractitionerProfileCriteria record
	 * 
	 * @param aProfileCriteriaConstant
	 *            - PractitionerProfileCriteriaConstant to be saved
	 */
	public void savePractitionerProfileCriteria(DssMsiConstant aProfileCriteriaConstant)
			throws OutlierCriteriaServiceException;

	/**
	 * Save changes made to an OutlierCriteria record
	 * 
	 * @param anOutlierCriteria
	 *            - OutlierCriteria to be saved
	 */
	public void saveOutlierCriteria(OutlierCriteria anOutlierCriteria) throws OutlierCriteriaServiceException;

	/**
	 * Save new PractitionerProfilePeriod
	 * 
	 * @param practitionerPeriod
	 *            a practitionerPeriod to save
	 * @return updated PractitionerProfilePeriod containing DB generated values
	 */
	public PractitionerProfilePeriod addNewPracitionerProfilePeriod(PractitionerProfilePeriod practitionerPeriod)
			throws OutlierCriteriaServiceException;
}

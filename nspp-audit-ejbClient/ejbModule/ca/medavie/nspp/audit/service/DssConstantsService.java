package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.ejb.Local;

import ca.medavie.nspp.audit.service.data.DssConstantBO;
import ca.medavie.nspp.audit.service.exception.DssConstantsServiceException;

/**
 * Service layer interface for Accounting business logic. Connects the front end controllers to the EJB layer.
 */
@Local
public interface DssConstantsService {

	  
	/**
	 * Get list of DssConstantBO
	 * @return
	 * @throws DssConstantsServiceException
	 */
	public List<DssConstantBO> getDssConstants() throws DssConstantsServiceException;

	/**
	 * @param dssConstant
	 * @return
	 * @throws DssConstantsServiceException
	 */
	public void updateDssConstant(DssConstantBO dssConstant) throws DssConstantsServiceException;
	
}

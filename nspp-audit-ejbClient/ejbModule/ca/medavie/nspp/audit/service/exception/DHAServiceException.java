package ca.medavie.nspp.audit.service.exception;

public class DHAServiceException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4975061648539010731L;
	/**
	 * a message key linking to this exception
	 */
	private static final String MESSAGE_KEY = "ERROR.PRACTITIONER.DHA.SERVICE.EXCEPTION";

	/**
	 * Public constructor which takes a string message to be included in the exception message
	 * 
	 * @param aMessage
	 *            text message
	 */
	public DHAServiceException(String aMessage) {
		super(aMessage);
	}

	/**
	 * Public constructor which takes a string message to be included in the exception message and a instance of
	 * Throwable. Used when chaining exceptions
	 * 
	 * @param aMessage
	 *            text message
	 * @param anException
	 *            a Throwable exeception
	 */
	public DHAServiceException(String aMessage, Throwable anException) {
		super(aMessage, anException);
	}

	@Override
	public String getMessageCode() {

		return MESSAGE_KEY;
	}

}

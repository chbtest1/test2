package ca.medavie.nspp.audit.service.exception;

public abstract class ServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6864811515185942033L;

	/**
	 * Public constructor which takes a string message to be included in the exception message
	 * 
	 * @param aMessage
	 *            text message
	 */
	public ServiceException(String aMessage) {
		super(aMessage);

	}

	/**
	 * Public constructor which takes a string message to be included in the exception message and a instance of
	 * Throwable. Used when chaining exceptions
	 * 
	 * @param aMessage
	 *            text message
	 * @param anException
	 *            a Throwable exeception
	 */
	public ServiceException(String aMessage, Throwable anException) {
		super(aMessage, anException);
	}

	public abstract String getMessageCode();

}

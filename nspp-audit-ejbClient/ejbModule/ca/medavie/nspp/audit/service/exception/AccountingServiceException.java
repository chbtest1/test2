package ca.medavie.nspp.audit.service.exception;

public class AccountingServiceException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2114924038740239041L;

	/**
	 * a message key linking to this exception
	 */
	private static final String MESSAGE_KEY = "ERROR.ACCOUNTING.SERVICE.EXCEPTION";

	/**
	 * Public constructor which takes a string message to be included in the exception message
	 * 
	 * @param aMessage
	 *            text message
	 */
	public AccountingServiceException(String aMessage) {
		super(aMessage);
	}

	/**
	 * Public constructor which takes a string message to be included in the exception message and a instance of
	 * Throwable. Used when chaining exceptions
	 * 
	 * @param aMessage
	 *            text message
	 * @param anException
	 *            a Throwable exeception
	 */
	public AccountingServiceException(String aMessage, Throwable anException) {
		super(aMessage, anException);
	}

	@Override
	public String getMessageCode() {

		return MESSAGE_KEY;
	}
}

/**
 * 
 */
package ca.medavie.nspp.audit.service.exception;

/**
 * @author bcscrip
 *
 */
public class PeerGroupServiceException extends ServiceException {

	/**
	 * IDE generated
	 */
	private static final long serialVersionUID = 2721940949279134423L;

	/**
	 * a message key linking to this exception
	 */
	private static final String MESSAGE_KEY = "ERROR.DSS.PEER.GROUP.SERVICE";

	/**
	 * Public constructor which takes a string message to be included in the exception message
	 * 
	 * @param aMessage
	 *            text message
	 */
	public PeerGroupServiceException(String aMessage) {
		super(aMessage);
	}

	/**
	 * Public constructor which takes a string message to be included in the exception message and a instance of
	 * Throwable. Used when chaining exceptions
	 * 
	 * @param aMessage
	 *            text message
	 * @param anException
	 *            a Throwable exeception
	 */
	public PeerGroupServiceException(String aMessage, Throwable anException) {
		super(aMessage, anException);
	}

	@Override
	public String getMessageCode() {

		return MESSAGE_KEY;
	}

}

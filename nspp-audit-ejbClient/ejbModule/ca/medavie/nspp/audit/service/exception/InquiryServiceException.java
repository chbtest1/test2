package ca.medavie.nspp.audit.service.exception;

public class InquiryServiceException extends ServiceException {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8287879519073628625L;
	
	/**
	 * a message key linking to this exception
	 */
	private static final String MESSAGE_KEY = "ERROR.INQUIRY.SERVICE.EXCEPTION";

	/**
	 * Public constructor which takes a string message to be included in the exception message
	 * 
	 * @param aMessage
	 *            text message
	 */
	public InquiryServiceException(String aMessage) {
		super(aMessage);
	}

	/**
	 * Public constructor which takes a string message to be included in the exception message and a instance of
	 * Throwable. Used when chaining exceptions
	 * 
	 * @param aMessage
	 *            text message
	 * @param anException
	 *            a Throwable exeception
	 */
	public InquiryServiceException(String aMessage, Throwable anException) {
		super(aMessage, anException);
	}

	@Override
	public String getMessageCode() {

		return MESSAGE_KEY;
	}

}

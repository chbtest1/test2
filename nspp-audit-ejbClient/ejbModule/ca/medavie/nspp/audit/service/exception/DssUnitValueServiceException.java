package ca.medavie.nspp.audit.service.exception;

public class DssUnitValueServiceException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6864811515185942033L;

	/**
	 * a message key linking to this exception
	 */
	private static final String MESSAGE_KEY = "ERROR.DSS.UNIT.VALUE.SERVICE";

	/**
	 * Public constructor which takes a string message to be included in the exception message
	 * 
	 * @param aMessage
	 *            text message
	 */
	public DssUnitValueServiceException(String aMessage) {
		super(aMessage);
	}

	/**
	 * Public constructor which takes a string message to be included in the exception message and a instance of
	 * Throwable. Used when chaining exceptions
	 * 
	 * @param aMessage
	 *            text message
	 * @param anException
	 *            a Throwable exeception
	 */
	public DssUnitValueServiceException(String aMessage, Throwable anException) {
		super(aMessage, anException);
	}

	@Override
	public String getMessageCode() {

		return MESSAGE_KEY;
	}

}

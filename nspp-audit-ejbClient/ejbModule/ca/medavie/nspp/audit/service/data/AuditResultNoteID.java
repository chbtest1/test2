package ca.medavie.nspp.audit.service.data;

import java.util.Date;

public class AuditResultNoteID extends AuditResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1969805715073934693L;
	/**
	 * Adding those properties(the information that connects the DssProviderAuditNotesXrefPK) for the purpose of mapping
	 * with DssProviderAuditNotesXrefPK
	 **/
	protected Long providerNumber;
	protected String providerType;
	protected Date auditDate;

	public Long getProviderNumber() {
		return providerNumber;
	}

	public void setProviderNumber(Long providerNumber) {
		this.providerNumber = providerNumber;
	}

	public String getProviderType() {
		return providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

}

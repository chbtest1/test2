package ca.medavie.nspp.audit.service.data;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Custom annotation to easily annotate a business object with its corresponding table column name
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface ColumnName {
	String name();
}

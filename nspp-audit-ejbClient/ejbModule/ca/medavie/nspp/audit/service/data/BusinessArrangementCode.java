package ca.medavie.nspp.audit.service.data;

/**
 * Models a single HealthServiceCode
 */
public class BusinessArrangementCode {
	private String busNumber;
	private String busDesc;
	
	public String getBusNumber() {
		return busNumber;
	}
	public void setBusNumber(String busNumber) {
		this.busNumber = busNumber;
	}
	public String getBusDesc() {
		return busDesc;
	}
	public void setBusDesc(String busDesc) {
		this.busDesc = busDesc;
	}
	
	public String getValue() {
		return this.busNumber;
	}
	
	public String getLabel() {
		return this.busDesc;
	}
}

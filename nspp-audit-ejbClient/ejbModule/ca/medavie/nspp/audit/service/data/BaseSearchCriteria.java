package ca.medavie.nspp.audit.service.data;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

/**
 * Superclass of all SearchCriterias
 */
public abstract class BaseSearchCriteria {

	/**
	 * Used for reseting search criteria. Enforces implementation
	 */
	public abstract void resetSearchCriteria();

	/**
	 * User defined sorting arguments. Set by request record sort on datatables binded to searches
	 */
	private Map<String, String> sortingArguments;

	/** String constants for sorting types */
	private static final String ASC_ORDER = "ASC";
	private static final String DESC_ORDER = "DESC";

	/**
	 * Resets the provided SearchCriteria
	 * 
	 * @param aCriteriaToReset
	 */
	protected void reset(Object aCriteriaToReset) {

		Field[] allFields = aCriteriaToReset.getClass().getDeclaredFields();
		for (Field field : allFields) {
			try {
				field.setAccessible(true);

				// Check if field is boolean... Cannot set to null
				if (field.getType() == boolean.class || field.getType() == Boolean.class) {
					field.set(aCriteriaToReset, false);

				} else {
					// Reset everything except variables declared as FINAL
					if (!Modifier.isFinal(field.getModifiers())) {
						field.set(aCriteriaToReset, null);
					}
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		// Reset the sortingParameters map
		sortingArguments = new HashMap<String, String>();
	}

	/**
	 * Simply utility method for adding a sort argument for search criteria
	 * 
	 * See 'https://docs.oracle.com/javase/tutorial/reflect/member/field.html' for Java Reflection docs
	 * 
	 * @param aClass
	 *            - Represents the business object class for object being searched
	 * @param aVariableName
	 * @param isAscending
	 */
	public void addSortArgument(Class<?> aClass, String aVariableName, Boolean isAscending) {

		// Fresh map every time we set a sort argument.. currently only support one column sort on UI.
		sortingArguments = new HashMap<String, String>();

		// Need to perform different logic for child variables
		if (aVariableName.contains(".")) {

			// Target object reference.. Starts out as root object (Parent)
			Class<?> targetClass = aClass;
			
			// Trim out combo variable bracket characters
			if (aVariableName.contains("}")) {

				// Get the index of the special character
				int index = aVariableName.indexOf("}");
				aVariableName = aVariableName.substring(0, index);
			}

			/*
			 * Split variable name into array by character "."
			 */
			String[] variables = aVariableName.split("\\.");
			int variableCount = variables.length;

			// Boolean controlling infinite search loop
			boolean searching = true;

			// Loop over variable names and navigate children objects
			int x = 0;
			while (searching) {

				String var = variables[x];

				Field[] fields = targetClass.getDeclaredFields();

				for (Field field : fields) {

					// Reached last variable. Field should be located in current class
					if (x == (variableCount - 1)) {

						// Get the column for sorting
						Field[] childFields = targetClass.getDeclaredFields();
						for (Field f : childFields) {

							if (f.getName().equalsIgnoreCase(var)) {

								ColumnName column = f.getAnnotation(ColumnName.class);
								if (column != null) {
									String columnName = column.name();

									// Add details to sorting map
									// If isAscending is True.. column is sorted ASC, otherwise it is sorted DESC
									sortingArguments.put(columnName, isAscending ? ASC_ORDER : DESC_ORDER);

									// Search for variable column complete... stop loop
									searching = false;

									// Exit inner loop
									break;
								}
							}
						}
					} else {

						if (field.getName().equalsIgnoreCase(var)) {
							targetClass = field.getType();
						}
					}
				}
				// Increase loop count
				x++;
			}
		} else {
			// Standard logic will work for the field
			// Get the column annotation from class to get correct column name for SQL
			Field[] allFields = aClass.getDeclaredFields();
			for (Field field : allFields) {

				// Perform substring on combined columns and use the first variableName
				if (aVariableName.contains("}")) {

					// Get the index of the special character
					int index = aVariableName.indexOf("}");
					aVariableName = aVariableName.substring(0, index);
				}

				// Variable is a basic field
				if (field.getName().equalsIgnoreCase(aVariableName)) {

					ColumnName column = field.getAnnotation(ColumnName.class);
					if (column != null) {
						String columnName = column.name();

						// Add details to sorting map
						// If isAscending is True.. column is sorted ASC, otherwise it is sorted DESC
						sortingArguments.put(columnName, isAscending ? ASC_ORDER : DESC_ORDER);
					}
				}
			}
		}
	}

	/**
	 * Simple utility method to check if the user specified any form of sorting on the search criteria
	 * 
	 * @return true if there is sorting arguments in map, false otherwise
	 */
	public boolean isSortingSet() {
		return (sortingArguments != null && !sortingArguments.isEmpty()) ? true : false;
	}

	/**
	 * @return the sortingArguments
	 */
	public Map<String, String> getSortingArguments() {
		return sortingArguments;
	}

	/**
	 * @param sortingArguments
	 *            the sortingArguments to set
	 */
	public void setSortingArguments(Map<String, String> sortingArguments) {
		this.sortingArguments = sortingArguments;
	}
}

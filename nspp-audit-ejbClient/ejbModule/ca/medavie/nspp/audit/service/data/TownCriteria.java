package ca.medavie.nspp.audit.service.data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Models the TownCriteria object
 */
public class TownCriteria implements Comparable<TownCriteria> {

	/** The Town Code */
	private BigDecimal townCode;
	/** The Town Name */
	private String townName;
	/** The Country Code */
	private BigDecimal countyCode;
	/** The Country Name */
	private String countyName;
	/** The Municipality Code */
	private BigDecimal municipalityCode;
	/** The Municipality Name */
	private String municipalityName;
	/** The Health Region Code */
	private BigDecimal healthRegionCode;
	/** The Health Region Description */
	private String healthRegionDesc;
	/** yearEndDate */
	private Date yearEndDate;
	/** providerPeerGroupId */
	private long providerPeerGroupId;
	/** The user that last modified this record */
	private String modifiedBy;
	/** The Date this record was last modified */
	private Date lastModified;

	/**
	 * @return the townCode
	 */
	public BigDecimal getTownCode() {
		return townCode;
	}

	/**
	 * @param townCode
	 *            the townCode to set
	 */
	public void setTownCode(BigDecimal townCode) {
		this.townCode = townCode;
	}

	/**
	 * @return the townName
	 */
	public String getTownName() {
		return townName;
	}

	/**
	 * @param townName
	 *            the townName to set
	 */
	public void setTownName(String townName) {
		this.townName = townName;
	}

	/**
	 * @return the countyCode
	 */
	public BigDecimal getCountyCode() {
		return countyCode;
	}

	/**
	 * @param countyCode
	 *            the countyCode to set
	 */
	public void setCountyCode(BigDecimal countryCode) {
		this.countyCode = countryCode;
	}

	/**
	 * @return the countyName
	 */
	public String getCountyName() {
		return countyName;
	}

	/**
	 * @param countyName
	 *            the countyName to set
	 */
	public void setCountyName(String countryName) {
		this.countyName = countryName;
	}

	/**
	 * @return the municipalityCode
	 */
	public BigDecimal getMunicipalityCode() {
		return municipalityCode;
	}

	/**
	 * @param municipalityCode
	 *            the municipalityCode to set
	 */
	public void setMunicipalityCode(BigDecimal municipalityCode) {
		this.municipalityCode = municipalityCode;
	}

	/**
	 * @return the municipalityName
	 */
	public String getMunicipalityName() {
		return municipalityName;
	}

	/**
	 * @param municipalityName
	 *            the municipalityName to set
	 */
	public void setMunicipalityName(String municipalityName) {
		this.municipalityName = municipalityName;
	}

	/**
	 * @return the healthRegionDesc
	 */
	public String getHealthRegion() {
		return healthRegionDesc;
	}

	public String getHealthRegionDesc() {
		return healthRegionDesc;
	}

	/**
	 * @param healthRegionDesc
	 *            the healthRegionDesc to set
	 */
	public void setHealthRegionDesc(String healthRegion) {
		this.healthRegionDesc = healthRegion;
	}

	public BigDecimal getHealthRegionCode() {
		return healthRegionCode;
	}

	public void setHealthRegionCode(BigDecimal healthRegionCode) {
		this.healthRegionCode = healthRegionCode;
	}

	public Date getYearEndDate() {
		return yearEndDate;
	}

	public void setYearEndDate(Date yearEndDate) {
		this.yearEndDate = yearEndDate;
	}

	public long getProviderPeerGroupId() {
		return providerPeerGroupId;
	}

	public void setProviderPeerGroupId(long providerPeerGroupId) {
		this.providerPeerGroupId = providerPeerGroupId;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getLastModified() {
		return lastModified;
	}

	/**
	 * @return string format of lastModified
	 */
	public String getLastModifiedString() {
		if (lastModified != null) {
			return DataUtilities.SDF_TIME.format(lastModified);
		}
		return "";
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	@Override
	public String toString() {
		return "TownCriteria [townCode=" + townCode + ", townName=" + townName + ", countyCode=" + countyCode
				+ ", countyName=" + countyName + ", municipalityCode=" + municipalityCode + ", municipalityName="
				+ municipalityName + ", healthRegionCode=" + healthRegionCode + ", healthRegionDesc="
				+ healthRegionDesc + ", yearEndDate=" + yearEndDate + ", providerPeerGroupId=" + providerPeerGroupId
				+ "]";
	}

	@Override
	public int compareTo(TownCriteria o) {

		// compared by municipality name in ascending order if they are not the same
		if (this.municipalityName.compareTo(o.municipalityName) != 0) {
			return this.municipalityName.compareTo(o.municipalityName);
		}

		else {
			// compared by county name in ascending if they are not the same
			if (this.countyName.compareTo(o.countyName) != 0) {
				return this.countyName.compareTo(o.countyName);
			}
			// compared by town name at last in ascending order they they are not the same
			else {
				return this.townName.compareTo(o.townName);
			}
		}

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((countyCode == null) ? 0 : countyCode.hashCode());
		result = prime * result + ((countyName == null) ? 0 : countyName.hashCode());
		result = prime * result + ((healthRegionCode == null) ? 0 : healthRegionCode.hashCode());
		result = prime * result + ((municipalityCode == null) ? 0 : municipalityCode.hashCode());
		result = prime * result + ((municipalityName == null) ? 0 : municipalityName.hashCode());
		result = prime * result + ((townCode == null) ? 0 : townCode.hashCode());
		result = prime * result + ((townName == null) ? 0 : townName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TownCriteria other = (TownCriteria) obj;
		if (countyCode == null) {
			if (other.countyCode != null)
				return false;
		} else if (!countyCode.equals(other.countyCode))
			return false;
		if (countyName == null) {
			if (other.countyName != null)
				return false;
		} else if (!countyName.equals(other.countyName))
			return false;
		if (healthRegionCode == null) {
			if (other.healthRegionCode != null)
				return false;
		} else if (!healthRegionCode.equals(other.healthRegionCode))
			return false;
		if (municipalityCode == null) {
			if (other.municipalityCode != null)
				return false;
		} else if (!municipalityCode.equals(other.municipalityCode))
			return false;
		if (municipalityName == null) {
			if (other.municipalityName != null)
				return false;
		} else if (!municipalityName.equals(other.municipalityName))
			return false;
		if (townCode == null) {
			if (other.townCode != null)
				return false;
		} else if (!townCode.equals(other.townCode))
			return false;
		if (townName == null) {
			if (other.townName != null)
				return false;
		} else if (!townName.equals(other.townName))
			return false;
		return true;
	}

}

package ca.medavie.nspp.audit.service.data;

import java.util.Date;

public class DssConstantBO implements Comparable<DssConstantBO> {
	
	private String subsystem;
	private String constantCode;
	private String constantType;
	private String constantDesc;
	private Date constantDate;
	private String modifiedBy;
	private Date lastModified;

	/**
	 * Constructor for clone purpose
	 * **/
	public DssConstantBO(DssConstantBO aDssUnitValueBO) {
		this.subsystem = aDssUnitValueBO.subsystem;
		this.constantCode = aDssUnitValueBO.constantCode;
		this.constantType = aDssUnitValueBO.constantType;
		this.constantDesc = aDssUnitValueBO.constantDesc;
		this.constantDate = aDssUnitValueBO.constantDate;
		this.modifiedBy = aDssUnitValueBO.modifiedBy;
		this.lastModified = aDssUnitValueBO.lastModified;
	}

	/**
	 * Default constructor
	 */
	public DssConstantBO() {
	}

	public String getSubsystem() {
		return subsystem;
	}

	public void setSubsystem(String subsystem) {
		this.subsystem = subsystem;
	}

	public String getConstantCode() {
		return constantCode;
	}

	public void setConstantCode(String constantCode) {
		this.constantCode = constantCode;
	}

	public String getConstantType() {
		return constantType;
	}

	public void setConstantType(String constantType) {
		this.constantType = constantType;
	}

	public String getConstantDesc() {
		return constantDesc;
	}

	public void setConstantDesc(String constantDesc) {
		this.constantDesc = constantDesc;
	}

	public Date getConstantDate() {
		return constantDate;
	}

	/**
	 * @return constantDate in string format
	 */
	public String getConstantDateString() {
		if (constantDate != null) {
			return DataUtilities.SDF.format(constantDate);
		}
		return "";
	}

	public void setConstantDate(Date constantDate) {
		this.constantDate = constantDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getLastModified() {
		return lastModified;
	}

	/**
	 * @return lastModified in string format
	 */
	public String getLastModifiedString() {
		if (lastModified != null) {
			return DataUtilities.SDF_TIME.format(lastModified);
		}
		return "";
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	@Override
	public int compareTo(DssConstantBO obj) {
		if (obj == null)
			return 0;
		if (this.constantCode == null)
			return 0;
		return this.constantCode.compareTo(obj.getConstantCode());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((constantCode == null) ? 0 : constantCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DssConstantBO other = (DssConstantBO) obj;
		if (constantCode == null) {
			if (other.constantCode != null)
				return false;
		} else if (!constantCode.equals(other.constantCode))
			return false;
		return true;
	}

}

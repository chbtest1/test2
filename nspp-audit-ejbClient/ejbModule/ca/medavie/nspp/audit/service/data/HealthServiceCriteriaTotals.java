package ca.medavie.nspp.audit.service.data;

import java.math.BigDecimal;

/**
 * Simple object to hold totals for Inquiry HealthServiceCriteria totals. Currently loaded via SQL from a summary table
 * 
 * Used for both Practitioner and PeerGroup
 */
public class HealthServiceCriteriaTotals {

	/***************************************************************
	 ***** Variables for PeerGroup HealthServiceCriteria table *****
	 *************************************************************** */
	private BigDecimal numberOfPractitionersTotal;

	private BigDecimal numberOfPatientsTotal;
	private BigDecimal numberOfPatientsAvgTotal;
	private BigDecimal numberOfPatientStdTotal;

	private BigDecimal numberOfServicesTotal;
	private BigDecimal numberOfServicesAvgTotal;
	private BigDecimal numberOfServicesStdTotal;

	private BigDecimal paymentsTotal;
	private BigDecimal paymentsAvgTotal;
	private BigDecimal paymentsStdTotal;

	private BigDecimal percentagePaymentsAvgTotal;

	private BigDecimal numberOfServicesPerPatientAvgTotal;
	private BigDecimal numberOfServicesPerPatientStdTotal;

	private BigDecimal paidPerPatientAvgTotal;
	private BigDecimal paidPerPatientStdTotal;

	private BigDecimal numberOfServicesPer100PatientAvgTotal;
	private BigDecimal numberOfServicesPer100PatientStdTotal;

	private BigDecimal paidPer100PatientAvgTotal;
	private BigDecimal paidPer100PatientStdTotal;

	/******************************************************************
	 ***** Variables for Practitioner HealthServiceCriteria table *****
	 ****************************************************************** */
	private BigDecimal patientPractitionerTotal;
	private BigDecimal patientIndexTotal;

	private BigDecimal servicesPractitionerTotal;
	private BigDecimal servicesIndexTotal;

	private BigDecimal paymentsPractitionerTotal;
	private BigDecimal paymentsIndexTotal;

	private BigDecimal percentPaymentsPractitionerTotal;
	private BigDecimal percentPaymentsIndexTotal;
	
	private BigDecimal servicesPerPractitionerTotal;
	private BigDecimal servicesPerIndexTotal;

	private BigDecimal paidPerPatientPractitionerTotal;
	private BigDecimal paidPerPatientIndexTotal;

	private BigDecimal servicesPer100PractitionerTotal;
	private BigDecimal servicesPer100IndexTotal;

	private BigDecimal paidPer100PractitionerTotal;
	private BigDecimal paidPer100IndexTotal;

	/** Default Constructor */
	public HealthServiceCriteriaTotals() {
	}

	/**
	 * @return the numberOfPractitionersTotal
	 */
	public BigDecimal getNumberOfPractitionersTotal() {
		return numberOfPractitionersTotal;
	}

	/**
	 * @param numberOfPractitionersTotal
	 *            the numberOfPractitionersTotal to set
	 */
	public void setNumberOfPractitionersTotal(BigDecimal numberOfPractitionersTotal) {
		this.numberOfPractitionersTotal = numberOfPractitionersTotal;
	}

	/**
	 * @return the numberOfPatientsTotal
	 */
	public BigDecimal getNumberOfPatientsTotal() {
		return numberOfPatientsTotal;
	}

	/**
	 * @param numberOfPatientsTotal
	 *            the numberOfPatientsTotal to set
	 */
	public void setNumberOfPatientsTotal(BigDecimal numberOfPatientsTotal) {
		this.numberOfPatientsTotal = numberOfPatientsTotal;
	}

	/**
	 * @return the numberOfPatientsAvgTotal
	 */
	public BigDecimal getNumberOfPatientsAvgTotal() {
		return numberOfPatientsAvgTotal;
	}

	/**
	 * @param numberOfPatientsAvgTotal
	 *            the numberOfPatientsAvgTotal to set
	 */
	public void setNumberOfPatientsAvgTotal(BigDecimal numberOfPatientsAvgTotal) {
		this.numberOfPatientsAvgTotal = numberOfPatientsAvgTotal;
	}

	/**
	 * @return the numberOfPatientStdTotal
	 */
	public BigDecimal getNumberOfPatientStdTotal() {
		return numberOfPatientStdTotal;
	}

	/**
	 * @param numberOfPatientStdTotal
	 *            the numberOfPatientStdTotal to set
	 */
	public void setNumberOfPatientStdTotal(BigDecimal numberOfPatientStdTotal) {
		this.numberOfPatientStdTotal = numberOfPatientStdTotal;
	}

	/**
	 * @return the numberOfServicesTotal
	 */
	public BigDecimal getNumberOfServicesTotal() {
		return numberOfServicesTotal;
	}

	/**
	 * @param numberOfServicesTotal
	 *            the numberOfServicesTotal to set
	 */
	public void setNumberOfServicesTotal(BigDecimal numberOfServicesTotal) {
		this.numberOfServicesTotal = numberOfServicesTotal;
	}

	/**
	 * @return the numberOfServicesAvgTotal
	 */
	public BigDecimal getNumberOfServicesAvgTotal() {
		return numberOfServicesAvgTotal;
	}

	/**
	 * @param numberOfServicesAvgTotal
	 *            the numberOfServicesAvgTotal to set
	 */
	public void setNumberOfServicesAvgTotal(BigDecimal numberOfServicesAvgTotal) {
		this.numberOfServicesAvgTotal = numberOfServicesAvgTotal;
	}

	/**
	 * @return the numberOfServicesStdTotal
	 */
	public BigDecimal getNumberOfServicesStdTotal() {
		return numberOfServicesStdTotal;
	}

	/**
	 * @param numberOfServicesStdTotal
	 *            the numberOfServicesStdTotal to set
	 */
	public void setNumberOfServicesStdTotal(BigDecimal numberOfServicesStdTotal) {
		this.numberOfServicesStdTotal = numberOfServicesStdTotal;
	}

	/**
	 * @return the paymentsTotal
	 */
	public BigDecimal getPaymentsTotal() {
		return paymentsTotal;
	}

	/**
	 * @param paymentsTotal
	 *            the paymentsTotal to set
	 */
	public void setPaymentsTotal(BigDecimal paymentsTotal) {
		this.paymentsTotal = paymentsTotal;
	}

	/**
	 * @return the paymentsAvgTotal
	 */
	public BigDecimal getPaymentsAvgTotal() {
		return paymentsAvgTotal;
	}

	/**
	 * @param paymentsAvgTotal
	 *            the paymentsAvgTotal to set
	 */
	public void setPaymentsAvgTotal(BigDecimal paymentsAvgTotal) {
		this.paymentsAvgTotal = paymentsAvgTotal;
	}

	/**
	 * @return the paymentsStdTotal
	 */
	public BigDecimal getPaymentsStdTotal() {
		return paymentsStdTotal;
	}

	/**
	 * @param paymentsStdTotal
	 *            the paymentsStdTotal to set
	 */
	public void setPaymentsStdTotal(BigDecimal paymentsStdTotal) {
		this.paymentsStdTotal = paymentsStdTotal;
	}

	/**
	 * @return the percentagePaymentsAvgTotal
	 */
	public BigDecimal getPercentagePaymentsAvgTotal() {
		return percentagePaymentsAvgTotal;
	}

	/**
	 * @param percentagePaymentsAvgTotal
	 *            the percentagePaymentsAvgTotal to set
	 */
	public void setPercentagePaymentsAvgTotal(BigDecimal percentagePaymentsAvgTotal) {
		this.percentagePaymentsAvgTotal = percentagePaymentsAvgTotal;
	}

	/**
	 * @return the numberOfServicesPerPatientAvgTotal
	 */
	public BigDecimal getNumberOfServicesPerPatientAvgTotal() {
		return numberOfServicesPerPatientAvgTotal;
	}

	/**
	 * @param numberOfServicesPerPatientAvgTotal
	 *            the numberOfServicesPerPatientAvgTotal to set
	 */
	public void setNumberOfServicesPerPatientAvgTotal(BigDecimal numberOfServicesPerPatientAvgTotal) {
		this.numberOfServicesPerPatientAvgTotal = numberOfServicesPerPatientAvgTotal;
	}

	/**
	 * @return the numberOfServicesPerPatientStdTotal
	 */
	public BigDecimal getNumberOfServicesPerPatientStdTotal() {
		return numberOfServicesPerPatientStdTotal;
	}

	/**
	 * @param numberOfServicesPerPatientStdTotal
	 *            the numberOfServicesPerPatientStdTotal to set
	 */
	public void setNumberOfServicesPerPatientStdTotal(BigDecimal numberOfServicesPerPatientStdTotal) {
		this.numberOfServicesPerPatientStdTotal = numberOfServicesPerPatientStdTotal;
	}

	/**
	 * @return the paidPerPatientAvgTotal
	 */
	public BigDecimal getPaidPerPatientAvgTotal() {
		return paidPerPatientAvgTotal;
	}

	/**
	 * @param paidPerPatientAvgTotal
	 *            the paidPerPatientAvgTotal to set
	 */
	public void setPaidPerPatientAvgTotal(BigDecimal paidPerPatientAvgTotal) {
		this.paidPerPatientAvgTotal = paidPerPatientAvgTotal;
	}

	/**
	 * @return the paidPerPatientStdTotal
	 */
	public BigDecimal getPaidPerPatientStdTotal() {
		return paidPerPatientStdTotal;
	}

	/**
	 * @param paidPerPatientStdTotal
	 *            the paidPerPatientStdTotal to set
	 */
	public void setPaidPerPatientStdTotal(BigDecimal paidPerPatientStdTotal) {
		this.paidPerPatientStdTotal = paidPerPatientStdTotal;
	}

	/**
	 * @return the numberOfServicesPer100PatientAvgTotal
	 */
	public BigDecimal getNumberOfServicesPer100PatientAvgTotal() {
		return numberOfServicesPer100PatientAvgTotal;
	}

	/**
	 * @param numberOfServicesPer100PatientAvgTotal
	 *            the numberOfServicesPer100PatientAvgTotal to set
	 */
	public void setNumberOfServicesPer100PatientAvgTotal(BigDecimal numberOfServicesPer100PatientAvgTotal) {
		this.numberOfServicesPer100PatientAvgTotal = numberOfServicesPer100PatientAvgTotal;
	}

	/**
	 * @return the numberOfServicesPer100PatientStdTotal
	 */
	public BigDecimal getNumberOfServicesPer100PatientStdTotal() {
		return numberOfServicesPer100PatientStdTotal;
	}

	/**
	 * @param numberOfServicesPer100PatientStdTotal
	 *            the numberOfServicesPer100PatientStdTotal to set
	 */
	public void setNumberOfServicesPer100PatientStdTotal(BigDecimal numberOfServicesPer100PatientStdTotal) {
		this.numberOfServicesPer100PatientStdTotal = numberOfServicesPer100PatientStdTotal;
	}

	/**
	 * @return the paidPer100PatientAvgTotal
	 */
	public BigDecimal getPaidPer100PatientAvgTotal() {
		return paidPer100PatientAvgTotal;
	}

	/**
	 * @param paidPer100PatientAvgTotal
	 *            the paidPer100PatientAvgTotal to set
	 */
	public void setPaidPer100PatientAvgTotal(BigDecimal paidPer100PatientAvgTotal) {
		this.paidPer100PatientAvgTotal = paidPer100PatientAvgTotal;
	}

	/**
	 * @return the paidPer100PatientStdTotal
	 */
	public BigDecimal getPaidPer100PatientStdTotal() {
		return paidPer100PatientStdTotal;
	}

	/**
	 * @param paidPer100PatientStdTotal
	 *            the paidPer100PatientStdTotal to set
	 */
	public void setPaidPer100PatientStdTotal(BigDecimal paidPer100PatientStdTotal) {
		this.paidPer100PatientStdTotal = paidPer100PatientStdTotal;
	}

	/**
	 * @return the patientPractitionerTotal
	 */
	public BigDecimal getPatientPractitionerTotal() {
		return patientPractitionerTotal;
	}

	/**
	 * @param patientPractitionerTotal
	 *            the patientPractitionerTotal to set
	 */
	public void setPatientPractitionerTotal(BigDecimal patientPractitionerTotal) {
		this.patientPractitionerTotal = patientPractitionerTotal;
	}

	/**
	 * @return the patientIndexTotal
	 */
	public BigDecimal getPatientIndexTotal() {
		return patientIndexTotal;
	}

	/**
	 * @param patientIndexTotal
	 *            the patientIndexTotal to set
	 */
	public void setPatientIndexTotal(BigDecimal patientIndexTotal) {
		this.patientIndexTotal = patientIndexTotal;
	}

	/**
	 * @return the servicesPractitionerTotal
	 */
	public BigDecimal getServicesPractitionerTotal() {
		return servicesPractitionerTotal;
	}

	/**
	 * @param servicesPractitionerTotal
	 *            the servicesPractitionerTotal to set
	 */
	public void setServicesPractitionerTotal(BigDecimal servicesPractitionerTotal) {
		this.servicesPractitionerTotal = servicesPractitionerTotal;
	}

	/**
	 * @return the servicesIndexTotal
	 */
	public BigDecimal getServicesIndexTotal() {
		return servicesIndexTotal;
	}

	/**
	 * @param servicesIndexTotal
	 *            the servicesIndexTotal to set
	 */
	public void setServicesIndexTotal(BigDecimal servicesIndexTotal) {
		this.servicesIndexTotal = servicesIndexTotal;
	}

	/**
	 * @return the paymentsPractitionerTotal
	 */
	public BigDecimal getPaymentsPractitionerTotal() {
		return paymentsPractitionerTotal;
	}

	/**
	 * @param paymentsPractitionerTotal
	 *            the paymentsPractitionerTotal to set
	 */
	public void setPaymentsPractitionerTotal(BigDecimal paymentsPractitionerTotal) {
		this.paymentsPractitionerTotal = paymentsPractitionerTotal;
	}

	/**
	 * @return the paymentsIndexTotal
	 */
	public BigDecimal getPaymentsIndexTotal() {
		return paymentsIndexTotal;
	}

	/**
	 * @param paymentsIndexTotal
	 *            the paymentsIndexTotal to set
	 */
	public void setPaymentsIndexTotal(BigDecimal paymentsIndexTotal) {
		this.paymentsIndexTotal = paymentsIndexTotal;
	}

	/**
	 * @return the percentPaymentsPractitionerTotal
	 */
	public BigDecimal getPercentPaymentsPractitionerTotal() {
		return percentPaymentsPractitionerTotal;
	}

	/**
	 * @param percentPaymentsPractitionerTotal
	 *            the percentPaymentsPractitionerTotal to set
	 */
	public void setPercentPaymentsPractitionerTotal(BigDecimal percentPaymentsPractitionerTotal) {
		this.percentPaymentsPractitionerTotal = percentPaymentsPractitionerTotal;
	}

	/**
	 * @return the percentPaymentsIndexTotal
	 */
	public BigDecimal getPercentPaymentsIndexTotal() {
		return percentPaymentsIndexTotal;
	}

	/**
	 * @return the servicesPerPractitionerTotal
	 */
	public BigDecimal getServicesPerPractitionerTotal() {
		return servicesPerPractitionerTotal;
	}

	/**
	 * @param servicesPerPractitionerTotal the servicesPerPractitionerTotal to set
	 */
	public void setServicesPerPractitionerTotal(BigDecimal servicesPerPractitionerTotal) {
		this.servicesPerPractitionerTotal = servicesPerPractitionerTotal;
	}

	/**
	 * @param percentPaymentsIndexTotal
	 *            the percentPaymentsIndexTotal to set
	 */
	public void setPercentPaymentsIndexTotal(BigDecimal percentPaymentsIndexTotal) {
		this.percentPaymentsIndexTotal = percentPaymentsIndexTotal;
	}

	/**
	 * @return the paidPerPatientPractitionerTotal
	 */
	public BigDecimal getPaidPerPatientPractitionerTotal() {
		return paidPerPatientPractitionerTotal;
	}

	/**
	 * @param paidPerPatientPractitionerTotal
	 *            the paidPerPatientPractitionerTotal to set
	 */
	public void setPaidPerPatientPractitionerTotal(BigDecimal paidPerPatientPractitionerTotal) {
		this.paidPerPatientPractitionerTotal = paidPerPatientPractitionerTotal;
	}

	/**
	 * @return the paidPerPatientIndexTotal
	 */
	public BigDecimal getPaidPerPatientIndexTotal() {
		return paidPerPatientIndexTotal;
	}

	/**
	 * @param paidPerPatientIndexTotal
	 *            the paidPerPatientIndexTotal to set
	 */
	public void setPaidPerPatientIndexTotal(BigDecimal paidPerPatientIndexTotal) {
		this.paidPerPatientIndexTotal = paidPerPatientIndexTotal;
	}

	/**
	 * @return the servicesPer100PractitionerTotal
	 */
	public BigDecimal getServicesPer100PractitionerTotal() {
		return servicesPer100PractitionerTotal;
	}

	/**
	 * @param servicesPer100PractitionerTotal
	 *            the servicesPer100PractitionerTotal to set
	 */
	public void setServicesPer100PractitionerTotal(BigDecimal servicesPer100PractitionerTotal) {
		this.servicesPer100PractitionerTotal = servicesPer100PractitionerTotal;
	}

	/**
	 * @return the servicesPer100IndexTotal
	 */
	public BigDecimal getServicesPer100IndexTotal() {
		return servicesPer100IndexTotal;
	}

	/**
	 * @param servicesPer100IndexTotal
	 *            the servicesPer100IndexTotal to set
	 */
	public void setServicesPer100IndexTotal(BigDecimal servicesPer100IndexTotal) {
		this.servicesPer100IndexTotal = servicesPer100IndexTotal;
	}

	/**
	 * @return the paidPer100PractitionerTotal
	 */
	public BigDecimal getPaidPer100PractitionerTotal() {
		return paidPer100PractitionerTotal;
	}

	/**
	 * @param paidPer100PractitionerTotal
	 *            the paidPer100PractitionerTotal to set
	 */
	public void setPaidPer100PractitionerTotal(BigDecimal paidPer100PractitionerTotal) {
		this.paidPer100PractitionerTotal = paidPer100PractitionerTotal;
	}

	/**
	 * @return the paidPer100IndexTotal
	 */
	public BigDecimal getPaidPer100IndexTotal() {
		return paidPer100IndexTotal;
	}

	/**
	 * @param paidPer100IndexTotal
	 *            the paidPer100IndexTotal to set
	 */
	public void setPaidPer100IndexTotal(BigDecimal paidPer100IndexTotal) {
		this.paidPer100IndexTotal = paidPer100IndexTotal;
	}

	/**
	 * @return the servicesPerIndexTotal
	 */
	public BigDecimal getServicesPerIndexTotal() {
		return servicesPerIndexTotal;
	}

	/**
	 * @param servicesPerIndexTotal the servicesPerIndexTotal to set
	 */
	public void setServicesPerIndexTotal(BigDecimal servicesPerIndexTotal) {
		this.servicesPerIndexTotal = servicesPerIndexTotal;
	}

}

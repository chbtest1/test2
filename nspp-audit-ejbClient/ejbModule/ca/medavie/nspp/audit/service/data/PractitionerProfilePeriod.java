package ca.medavie.nspp.audit.service.data;

import java.util.Date;

/**
 * Models a single PractitionerProfilePeriod object
 */
public class PractitionerProfilePeriod implements Comparable<PractitionerProfilePeriod> {

	/** Holds the unique period ID */
	private Long periodId;
	/** Holds the periodStart date */
	private Date periodStart;
	/** Hold the periodEnd date */
	private Date periodEnd;
	/** Holds the yearEnd date */
	private Date yearEnd;
	/** Holds the user who last modified the profile period */
	private String modifiedBy;
	/** Holds the date of when the profile period was modified */
	private Date lastModified;

	/**
	 * Default Constructor
	 */
	public PractitionerProfilePeriod() {
	}

	/**
	 * Constructor for when all parameters for object are available.
	 * 
	 * @param anId
	 * @param aStartDate
	 * @param anEndDate
	 * @param aYearEnd
	 * @param aModifiedBy
	 * @param aLastModified
	 */
	public PractitionerProfilePeriod(Long anId, Date aStartDate, Date anEndDate, Date aYearEnd, String aModifiedBy,
			Date aLastModified) {
		periodId = anId;
		periodStart = aStartDate;
		periodEnd = anEndDate;
		yearEnd = aYearEnd;
		modifiedBy = aModifiedBy;
		lastModified = aLastModified;
	}

	/**
	 * @return the periodId
	 */
	public Long getPeriodId() {
		return periodId;
	}

	/**
	 * @param periodId
	 *            the periodId to set
	 */
	public void setPeriodId(Long periodId) {
		this.periodId = periodId;
	}

	/**
	 * @return the periodStart
	 */
	public Date getPeriodStart() {
		return periodStart;
	}

	/**
	 * @param periodStart
	 *            the periodStart to set
	 */
	public void setPeriodStart(Date periodStart) {
		this.periodStart = periodStart;
	}

	/**
	 * @return the periodEnd
	 */
	public Date getPeriodEnd() {
		return periodEnd;
	}

	/**
	 * @param periodEnd
	 *            the periodEnd to set
	 */
	public void setPeriodEnd(Date periodEnd) {
		this.periodEnd = periodEnd;
	}

	/**
	 * @return the yearEnd
	 */
	public Date getYearEnd() {
		return yearEnd;
	}

	/**
	 * @param yearEnd
	 *            the yearEnd to set
	 */
	public void setYearEnd(Date yearEnd) {
		this.yearEnd = yearEnd;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the lastModified
	 */
	public Date getLastModified() {
		return lastModified;
	}

	/**
	 * @param lastModified
	 *            the lastModified to set
	 */
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	// Getters for date to string conversions. Enables the ability to sort & filter on dates in a datatable
	/**
	 * @return periodStart date formatted as string.
	 */
	public String getPeriodStartString() {
		if (periodStart != null) {
			return DataUtilities.SDF.format(periodStart);
		}
		return "";
	}

	/**
	 * @return periodEnd date formatted as string.
	 */
	public String getPeriodEndString() {
		if (periodEnd != null) {
			return DataUtilities.SDF.format(periodEnd);
		}
		return "";
	}

	/**
	 * @return yearEnd date formatted as string.
	 */
	public String getYearEndString() {
		if (yearEnd != null) {
			return DataUtilities.SDF.format(yearEnd);
		}
		return "";
	}

	/**
	 * @return lastModified date formatted as string.
	 */
	public String getLastModifiedString() {
		if (lastModified != null) {
			return DataUtilities.SDF_TIME.format(lastModified);
		}
		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PractitionerProfilePeriod [periodId=" + periodId + ", periodStart=" + periodStart + ", periodEnd="
				+ periodEnd + ", yearEnd=" + yearEnd + ", modifiedBy=" + modifiedBy + ", lastModified=" + lastModified
				+ "]";
	}

	@Override
	public int compareTo(PractitionerProfilePeriod o) {
		if (this.periodId == o.getPeriodId()) {

			return 0;
		}

		return this.getPeriodId() > o.getPeriodId() ? 1 : -1;
	}
}

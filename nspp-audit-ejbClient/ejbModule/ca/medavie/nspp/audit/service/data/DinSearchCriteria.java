package ca.medavie.nspp.audit.service.data;

/**
 * Models the search criteria for DINs
 */
public class DinSearchCriteria extends BaseSearchCriteria {
	private String din;
	private String drugName;

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.data.BaseSearchCriteria#resetSearchCriteria()
	 */
	@Override
	public void resetSearchCriteria() {
		super.reset(this);
	}

	/**
	 * @return the din
	 */
	public String getDin() {
		return din;
	}

	/**
	 * @param din
	 *            the din to set
	 */
	public void setDin(String din) {
		this.din = din;
	}

	/**
	 * @return the drugName
	 */
	public String getDrugName() {
		return drugName;
	}

	/**
	 * @param drugName
	 *            the drugName to set
	 */
	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public boolean isDinSet() {
		return (din != null && !din.isEmpty()) ? true : false;
	}

	public boolean isDrugNameSet() {
		return (drugName != null && !drugName.isEmpty()) ? true : false;
	}
}

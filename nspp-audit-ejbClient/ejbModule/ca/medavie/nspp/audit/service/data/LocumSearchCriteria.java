package ca.medavie.nspp.audit.service.data;

/**
 * Used to search for Locums based on user input
 */
public class LocumSearchCriteria extends BaseSearchCriteria {
	private String practitionerNumber;
	private String practitionerType;
	private String practitionerName;
	private String city;
	private String specialty;
	private String licenseNumber;

	// Holds the selected LocumType.. Default HOST
	private LocumTypes locumType = LocumTypes.HOST;

	// Define all types of Locums
	public enum LocumTypes {
		HOST("label.locum.host"), VISITOR("label.locum.visitor");

		private String label;

		private LocumTypes(String aLabel) {
			label = aLabel;
		}

		/**
		 * @return the label
		 */
		public String getLabel() {
			return label;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.data.BaseSearchCriteria#resetSearchCriteria(java.lang.Object)
	 */
	@Override
	public void resetSearchCriteria() {
		super.reset(this);

		// Custom default values
		locumType = LocumTypes.HOST;
	}

	public boolean isPractitionerNumberSet() {
		return (practitionerNumber != null && !practitionerNumber.isEmpty()) ? true : false;
	}

	public boolean isPractitionerTypeSet() {
		return (practitionerType != null && !practitionerType.isEmpty()) ? true : false;
	}

	public boolean isPractitionerNameSet() {
		return (practitionerName != null && !practitionerName.isEmpty()) ? true : false;
	}

	public boolean isCitySet() {
		return (city != null && !city.isEmpty()) ? true : false;
	}

	public boolean isSpecialtySet() {
		return (specialty != null && !specialty.isEmpty()) ? true : false;
	}

	public boolean isLicenseNumberSet() {
		return (licenseNumber != null && !licenseNumber.isEmpty()) ? true : false;
	}

	/**
	 * @return the practitionerNumber
	 */
	public String getPractitionerNumber() {
		return practitionerNumber;
	}

	/**
	 * @param practitionerNumber
	 *            the practitionerNumber to set
	 */
	public void setPractitionerNumber(String practitionerNumber) {
		this.practitionerNumber = practitionerNumber;
	}

	/**
	 * @return the practitionerType
	 */
	public String getPractitionerType() {
		return practitionerType;
	}

	/**
	 * @param practitionerType
	 *            the practitionerType to set
	 */
	public void setPractitionerType(String practitionerType) {
		this.practitionerType = practitionerType;
	}

	/**
	 * @return the practitionerName
	 */
	public String getPractitionerName() {
		return practitionerName;
	}

	/**
	 * @param practitionerName
	 *            the practitionerName to set
	 */
	public void setPractitionerName(String practitionerName) {
		this.practitionerName = practitionerName;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the specialty
	 */
	public String getSpecialty() {
		return specialty;
	}

	/**
	 * @param specialty
	 *            the specialty to set
	 */
	public void setSpecialty(String specialty) {
		this.specialty = specialty;
	}

	/**
	 * @return the licenseNumber
	 */
	public String getLicenseNumber() {
		return licenseNumber;
	}

	/**
	 * @param licenseNumber
	 *            the licenseNumber to set
	 */
	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	/**
	 * @return the locumType
	 */
	public LocumTypes getLocumType() {
		return locumType;
	}

	/**
	 * @param locumType
	 *            the locumType to set
	 */
	public void setLocumType(LocumTypes locumType) {
		this.locumType = locumType;
	}
}

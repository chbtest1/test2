package ca.medavie.nspp.audit.service.data;

import java.math.BigDecimal;
import java.util.Date;

public class DenticareAuditLetterResponse {

	@ColumnName(name = "provider_number")
	private String practitionerNumber;

	@ColumnName(name = "provider_type")
	private String practitionerType;

	@ColumnName(name = "provider_name")
	private String practitionerName;

	@ColumnName(name = "health_card_number")
	private String healthCardNumber;

	@ColumnName(name = "audit_type")
	private String auditType;

	@ColumnName(name = "response_status_code")
	private String responseStatus;

	@ColumnName(name = "audit_run_number")
	private Long auditRunNumber;

	@ColumnName(name = "chargeback_amount")
	private BigDecimal chargeBackAmount;

	@ColumnName(name = "number_of_claims_affected")
	private Integer numberOfClaimAffected;

	@ColumnName(name = "claim_selection_date")
	private Date claimSelectionDate;

	@ColumnName(name = "audit_letter_creation_date")
	private Date auditLetterCreationDate;

	@ColumnName(name = "number_of_times_sent")
	private Integer numberOfTimesSent;

	// Claim information
	@ColumnName(name = "claim_num")
	private Long claimNumber;

	@ColumnName(name = "item_code")
	private Long itemCode;

	@ColumnName(name = "fee_code_description")
	private String serviceDescription;

	@ColumnName(name = "service_date")
	private Date serviceDate;

	@ColumnName(name = "paid_amount")
	private BigDecimal paidAmount;

	@ColumnName(name = "individual_name")
	private String individualName;

	@ColumnName(name = "audit_note")
	private String auditNote;

	@ColumnName(name = "modified_by")
	private String modifiedBy;

	@ColumnName(name = "last_modified")
	private Date lastModified;

	public String getPractitionerNumber() {
		return practitionerNumber;
	}

	public void setPractitionerNumber(String practitionerNumber) {
		this.practitionerNumber = practitionerNumber;
	}

	public String getPractitionerType() {
		return practitionerType;
	}

	public void setPractitionerType(String practitionerType) {
		this.practitionerType = practitionerType;
	}

	public String getPractitionerName() {
		return practitionerName;
	}

	public void setPractitionerName(String practitionerName) {
		this.practitionerName = practitionerName;
	}

	public String getHealthCardNumber() {
		return healthCardNumber;
	}

	public void setHealthCardNumber(String healthCardNumber) {
		this.healthCardNumber = healthCardNumber;
	}

	public String getAuditType() {
		return auditType;
	}

	public void setAuditType(String auditType) {
		this.auditType = auditType;
	}

	public String getResponseStatus() {
		// we don't want space for status code
		return responseStatus.replaceAll("\\s+", "");
	}

	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}

	public Long getAuditRunNumber() {
		return auditRunNumber;
	}

	public void setAuditRunNumber(Long auditRunNumber) {
		this.auditRunNumber = auditRunNumber;
	}

	public BigDecimal getChargeBackAmount() {
		return chargeBackAmount;
	}

	public void setChargeBackAmount(BigDecimal chargeBackAmount) {
		this.chargeBackAmount = chargeBackAmount;
	}

	public Integer getNumberOfClaimAffected() {
		return numberOfClaimAffected;
	}

	public void setNumberOfClaimAffected(Integer numberOfClaimAffected) {
		this.numberOfClaimAffected = numberOfClaimAffected;
	}

	public Date getClaimSelectionDate() {
		return claimSelectionDate;
	}

	public void setClaimSelectionDate(Date claimSelectionDate) {
		this.claimSelectionDate = claimSelectionDate;
	}

	public Date getAuditLetterCreationDate() {
		return auditLetterCreationDate;
	}

	public void setAuditLetterCreationDate(Date auditLetterCreationDate) {
		this.auditLetterCreationDate = auditLetterCreationDate;
	}

	public Integer getNumberOfTimesSent() {
		return numberOfTimesSent;
	}

	public void setNumberOfTimesSent(Integer numberOfTimesSent) {
		this.numberOfTimesSent = numberOfTimesSent;
	}

	public Long getClaimNumber() {
		return claimNumber;
	}

	public void setClaimNumber(Long claimNumber) {
		this.claimNumber = claimNumber;
	}

	public Long getItemCode() {
		return itemCode;
	}

	public void setItemCode(Long itemCode) {
		this.itemCode = itemCode;
	}

	public String getServiceDescription() {
		return serviceDescription;
	}

	public void setServiceDescription(String serviceDescription) {
		this.serviceDescription = serviceDescription;
	}

	public Date getServiceDate() {
		return serviceDate;
	}

	public void setServiceDate(Date serviceDate) {
		this.serviceDate = serviceDate;
	}

	public BigDecimal getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getIndividualName() {
		return individualName;
	}

	public void setIndividualName(String individualName) {
		this.individualName = individualName;
	}

	public String getAuditNote() {
		return auditNote;
	}

	public void setAuditNote(String auditNote) {
		this.auditNote = auditNote;
	}

}

package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * The persistent class for the DSS_MSI_CONSTANT database table.
 * 
 */
@Entity
@Table(name = "DSS_MSI_CONSTANT")
@NamedQueries({ @NamedQuery(name = QueryConstants.DSS_MSI_CONSTANT_EDIT, query = "select distinct dmc FROM DssMsiConstant dmc where dmc.id.subsystem = 'DSSQUERY' and dmc.id.constantCode IN ('MNTHEND_DT','PR_SRV_QTR','YEAREND_DT')") })
public class DssMsiConstant implements Serializable {
	private static final long serialVersionUID = 1L;
	// JTRAX-52 06-FEB-18 BCAINNE
	@Transient 
	private String constantStrDate;

	@EmbeddedId
	private DssMsiConstantPK id;

	@Column(name = "CONSTANT_CHAR")
	private String constantChar;

	@Column(name = "CONSTANT_CURRENCY")
	private BigDecimal constantCurrency;

	@Temporal(TemporalType.DATE)
	@Column(name = "CONSTANT_DATE")
	private Date constantDate;

	@Column(name = "CONSTANT_DESC")
	private String constantDesc;

	@Column(name = "CONSTANT_NUMERIC")
	private Integer constantNumeric;

	@Column(name = "CONSTANT_TYPE")
	private String constantType;

	@Column(name = "CONSTANT_CODE")
	private String constantCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	public DssMsiConstant() {
	}

	public DssMsiConstantPK getId() {
		return this.id;
	}

	public void setId(DssMsiConstantPK id) {
		this.id = id;
	}

	public String getConstantChar() {
		return this.constantChar;
	}

	public void setConstantChar(String constantChar) {
		this.constantChar = constantChar;
	}

	public BigDecimal getConstantCurrency() {
		return this.constantCurrency;
	}

	public void setConstantCurrency(BigDecimal constantCurrency) {
		this.constantCurrency = constantCurrency;
	}

	public Date getConstantDate() {
		return this.constantDate;
	}

	public void setConstantDate(Date constantDate) {
		this.constantDate = constantDate;
	}

	public String getConstantDesc() {
		return this.constantDesc;
	}

	public void setConstantDesc(String constantDesc) {
		this.constantDesc = constantDesc;
	}

	public Integer getConstantNumeric() {
		return this.constantNumeric;
	}

	public void setConstantNumeric(Integer constantNumeric) {
		this.constantNumeric = constantNumeric;
	}

	public String getConstantType() {
		return this.constantType;
	}

	public void setConstantType(String constantType) {
		this.constantType = constantType;
	}

	public String getConstantCode() {
		return constantCode;
	}

	public void setConstantCode(String constantCode) {
		this.constantCode = constantCode;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getLastModifiedString() {
		if (this.lastModified != null) {
			return DataUtilities.SDF_TIME.format(this.lastModified);
		}
		return "";
	}

	/**
	 * Only used for sorting values in the UI
	 * 
	 * @return a string value of the constant
	 */
	public String getValue() {
		String value = "";

		if (this.constantType.equals("C")) {
			value = this.constantChar;
		} else if (this.constantType.equals("N")) {
			if (this.constantNumeric != null) {
				value = this.constantNumeric.toString();
			}
		} else if (this.constantType.equals("D")) {
			if (this.constantDate != null) {
				value = DataUtilities.SDF_TIME.format(this.constantDate);
			}
		} else if (this.constantType.equals("$")) {
			if (this.constantCurrency != null) {
				value = this.constantCurrency.toString();
			}
		}

		return value;
	}

	// JTRAX-52 06-FEB-18 BCAINNE
	public String getConstantStrDate() {
		return DataUtilities.SDF.format(this.constantDate);
	}

	public void setConstantStrDate(String constantStrDate) {
		try {
			this.constantDate = DataUtilities.SDF.parse(constantStrDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
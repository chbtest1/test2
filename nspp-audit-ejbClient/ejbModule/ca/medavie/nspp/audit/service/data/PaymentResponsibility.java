package ca.medavie.nspp.audit.service.data;

import java.util.Date;

/**
 * Models a Payment Responsibility object. Generally found under an Audit Criteria
 */
public class PaymentResponsibility {
	// The ID of the Audit Criteria object this belongs to
	private Long auditCriteriaId;
	private String paymentResponsibility;
	private String modifiedBy;
	private Date lastModified;

	/**
	 * @return the auditCriteriaId
	 */
	public Long getAuditCriteriaId() {
		return auditCriteriaId;
	}

	/**
	 * @param auditCriteriaId
	 *            the auditCriteriaId to set
	 */
	public void setAuditCriteriaId(Long auditCriteriaId) {
		this.auditCriteriaId = auditCriteriaId;
	}

	/**
	 * @return the paymentResponsibility
	 */
	public String getPaymentResponsibility() {
		return paymentResponsibility;
	}

	/**
	 * @param paymentResponsibility
	 *            the paymentResponsibility to set
	 */
	public void setPaymentResponsibility(String paymentResponsibility) {
		this.paymentResponsibility = paymentResponsibility;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the lastModified
	 */
	public Date getLastModified() {
		return lastModified;
	}

	/**
	 * @return string format of lastModified
	 */
	public String getLastModifiedString() {
		if (lastModified != null) {
			return DataUtilities.SDF_TIME.format(lastModified);
		}
		return "";
	}

	/**
	 * @param lastModified
	 *            the lastModified to set
	 */
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}
}

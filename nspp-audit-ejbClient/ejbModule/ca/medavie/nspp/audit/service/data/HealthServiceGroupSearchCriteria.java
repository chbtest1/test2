package ca.medavie.nspp.audit.service.data;

/**
 * Used to search for specific HealthServiceGroups based on user input
 */
public class HealthServiceGroupSearchCriteria extends BaseSearchCriteria {

	private HealthServiceGroupSearchTypes searchTypeSelection;
	private Long healthServiceGroupId;
	private String healthServiceGroupName;
	private String healthServiceProgram;
	private String healthServiceCode;
	private String healthServiceQualifier;
	private String descriptionWord;

	/**
	 * Enum of styles the user can search for Health Service Groups. Options include:
	 * 
	 * 'Search By Health Service Groups' or 'Search By Health Services'
	 */
	public enum HealthServiceGroupSearchTypes {
		HEALTH_SERVICE_GROUP_SEARCH("Health Service Group Search"), HEALTH_SERVICES_SEARCH("Health Services Search");

		private String label;

		private HealthServiceGroupSearchTypes(String aLabel) {
			label = aLabel;
		}

		public String getLabel() {
			return label;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.data.BaseSearchCriteria#resetSearchCriteria(java.lang.Object)
	 */
	@Override
	public void resetSearchCriteria() {
		super.reset(this);
	}

	/**
	 * @return true if healthServiceGroupId is set
	 */
	public boolean isHealthServiceGroupIdSet() {
		return (healthServiceGroupId != null && healthServiceGroupId.compareTo(Long.valueOf("0")) != 0) ? true : false;
	}

	/**
	 * @return true if healthServiceGroupName is set
	 */
	public boolean isHealthServiceGroupNameSet() {
		return (healthServiceGroupName != null && !healthServiceGroupName.isEmpty()) ? true : false;
	}

	/**
	 * @return true if healthServiceProgram is set
	 */
	public boolean isHealthServiceProgramSet() {
		return (healthServiceProgram != null && !healthServiceProgram.isEmpty()) ? true : false;
	}

	/**
	 * @return true if healthServiceCode is set
	 */
	public boolean isHealthServiceCodeSet() {
		return (healthServiceCode != null && !healthServiceCode.isEmpty()) ? true : false;
	}

	/**
	 * @return true if healthServiceQualifier is set
	 */
	public boolean isHealthServiceQualifierSet() {
		return (healthServiceQualifier != null && !healthServiceQualifier.isEmpty()) ? true : false;
	}

	/**
	 * @return true if descriptionWord is set
	 */
	public boolean isDescriptionWordSet() {
		return (descriptionWord != null && !descriptionWord.isEmpty()) ? true : false;
	}

	/**
	 * @return the searchTypeSelection
	 */
	public HealthServiceGroupSearchTypes getSearchTypeSelection() {
		return searchTypeSelection;
	}

	/**
	 * @param searchTypeSelection
	 *            the searchTypeSelection to set
	 */
	public void setSearchTypeSelection(HealthServiceGroupSearchTypes searchTypeSelection) {
		this.searchTypeSelection = searchTypeSelection;
	}

	/**
	 * @return the healthServiceGroupId
	 */
	public Long getHealthServiceGroupId() {
		return healthServiceGroupId;
	}

	/**
	 * @param healthServiceGroupId
	 *            the healthServiceGroupId to set
	 */
	public void setHealthServiceGroupId(Long healthServiceGroupId) {
		this.healthServiceGroupId = healthServiceGroupId;
	}

	/**
	 * @return the healthServiceGroupName
	 */
	public String getHealthServiceGroupName() {
		return healthServiceGroupName;
	}

	/**
	 * @param healthServiceGroupName
	 *            the healthServiceGroupName to set
	 */
	public void setHealthServiceGroupName(String healthServiceGroupName) {
		this.healthServiceGroupName = healthServiceGroupName;
	}

	/**
	 * @return the healthServiceProgram
	 */
	public String getHealthServiceProgram() {
		return healthServiceProgram;
	}

	/**
	 * @param healthServiceProgram
	 *            the healthServiceProgram to set
	 */
	public void setHealthServiceProgram(String healthServiceProgram) {
		this.healthServiceProgram = healthServiceProgram;
	}

	/**
	 * @return the healthServiceCode
	 */
	public String getHealthServiceCode() {
		return healthServiceCode;
	}

	/**
	 * @param healthServiceCode
	 *            the healthServiceCode to set
	 */
	public void setHealthServiceCode(String healthServiceCode) {
		this.healthServiceCode = healthServiceCode;
	}

	/**
	 * @return the healthServiceQualifier
	 */
	public String getHealthServiceQualifier() {
		return healthServiceQualifier;
	}

	/**
	 * @param healthServiceQualifier
	 *            the healthServiceQualifier to set
	 */
	public void setHealthServiceQualifier(String healthServiceQualifier) {
		this.healthServiceQualifier = healthServiceQualifier;
	}

	/**
	 * @return the descriptionWord
	 */
	public String getDescriptionWord() {
		return descriptionWord;
	}

	/**
	 * @param descriptionWord
	 *            the descriptionWord to set
	 */
	public void setDescriptionWord(String descriptionWord) {
		this.descriptionWord = descriptionWord;
	}
}

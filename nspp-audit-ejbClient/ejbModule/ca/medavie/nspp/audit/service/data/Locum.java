package ca.medavie.nspp.audit.service.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Models a Locum record.
 */
public class Locum {

	// HOST and VISITING exclusive values
	private Long hostPractitionerNumber;
	private String hostPractitionerType;
	private Long visitingPractitionerNumber;
	private String visitingPractitionerType;

	// Used for searching convienence
	@ColumnName(name="provider_number")
	private Long practitionerNumber;
	private String practitionerType;

	// Common values used by both HOST and VISITING
	@ColumnName(name="last_name")
	private String practitionerName;
	@ColumnName(name="birth_date")
	private Date birthDate;
	private boolean locumHost;
	private boolean locumVisitor;
	@ColumnName(name="city")
	private String city;

	/*
	 * Need 2 different variables for EffectiveFrom since this can be changed by the user and the orginial is needed to
	 * manipulate the original object
	 */
	private Date originalEffectiveFrom;
	private Date effectiveFrom;

	private Date effectiveTo;
	private String modifiedBy;
	private Date lastModified;

	// Boolean flag to indicate if locum is new to its parent
	private boolean markedForAdd;
	// Boolean flag to indicate if locum has been editted.
	private boolean markedForEdit;

	private List<Locum> locums;
	// List of invalid locums after validation display in the pop up window for user to fix
	private List<Locum> invalidLocums = new ArrayList<Locum>();

	/**
	 * Recursively set all MarkedFor* flags as false Used after saving a Locum to ensure they aren't incorrectly marked
	 * on a follow up action
	 */
	public void resetMarkedForFlags() {
		setMarkedForAdd(false);
		setMarkedForEdit(false);
		for (Locum l : locums) {
			l.setMarkedForAdd(false);
			l.setMarkedForEdit(false);
		}
	}

	/**
	 * @return the practitionerName
	 */
	public String getPractitionerName() {
		return practitionerName;
	}

	/**
	 * @param practitionerName
	 *            the practitionerName to set
	 */
	public void setPractitionerName(String practitionerName) {
		this.practitionerName = practitionerName;
	}

	/**
	 * @return the birthDate
	 */
	public Date getBirthDate() {
		return birthDate;
	}

	/**
	 * @return String format of birthDate
	 */
	public String getBirthDateString() {
		if (birthDate != null) {
			return DataUtilities.SDF.format(birthDate);
		}
		return "";
	}

	/**
	 * @param birthDate
	 *            the birthDate to set
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * @return the locumHost
	 */
	public boolean isLocumHost() {
		return locumHost;
	}

	/**
	 * @param locumHost
	 *            the locumHost to set
	 */
	public void setLocumHost(boolean locumHost) {
		this.locumHost = locumHost;
	}

	/**
	 * @return the locumVisitor
	 */
	public boolean isLocumVisitor() {
		return locumVisitor;
	}

	/**
	 * @param locumVisitor
	 *            the locumVisitor to set
	 */
	public void setLocumVisitor(boolean locumVisitor) {
		this.locumVisitor = locumVisitor;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the effectiveFrom
	 */
	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	/**
	 * @return effectiveFrom formatted as string
	 */
	public String getEffectiveFromString() {
		if (effectiveFrom != null) {
			return DataUtilities.SDF.format(effectiveFrom);
		}
		return "";
	}

	/**
	 * @param effectiveFrom
	 *            the effectiveFrom to set
	 */
	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	/**
	 * @return the effectiveTo
	 */
	public Date getEffectiveTo() {
		return effectiveTo;
	}

	/**
	 * @return effectiveTo formatted as string
	 */
	public String getEffectiveToString() {
		if (effectiveTo != null) {
			return DataUtilities.SDF.format(effectiveTo);
		}
		return "";
	}

	/**
	 * @param effectiveTo
	 *            the effectiveTo to set
	 */
	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the lastModified
	 */
	public Date getLastModified() {
		return lastModified;
	}

	/**
	 * @return lastModified formatted as string
	 */
	public String getLastModifiedString() {
		if (lastModified != null) {
			return DataUtilities.SDF_TIME.format(lastModified);
		}
		return "";
	}

	/**
	 * @param lastModified
	 *            the lastModified to set
	 */
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 * @return the locums
	 */
	public List<Locum> getLocums() {
		return locums;
	}

	/**
	 * @param locums
	 *            the locums to set
	 */
	public void setLocums(List<Locum> locums) {
		this.locums = locums;
	}

	public List<Locum> getInvalidLocums() {
		return invalidLocums;
	}

	public void setInvalidLocums(List<Locum> invalidLocums) {
		this.invalidLocums = invalidLocums;
	}

	/**
	 * @return the practitionerNumber
	 */
	public Long getPractitionerNumber() {
		return practitionerNumber;
	}

	/**
	 * @param practitionerNumber
	 *            the practitionerNumber to set
	 */
	public void setPractitionerNumber(Long practitionerNumber) {
		this.practitionerNumber = practitionerNumber;
	}

	/**
	 * @return the practitionerType
	 */
	public String getPractitionerType() {
		return practitionerType;
	}

	/**
	 * @param practitionerType
	 *            the practitionerType to set
	 */
	public void setPractitionerType(String practitionerType) {
		this.practitionerType = practitionerType;
	}

	/**
	 * @return the markedForAdd
	 */
	public boolean isMarkedForAdd() {
		return markedForAdd;
	}

	/**
	 * @param markedForAdd
	 *            the markedForAdd to set
	 */
	public void setMarkedForAdd(boolean markedForAdd) {
		this.markedForAdd = markedForAdd;
	}

	/**
	 * @return
	 */
	public boolean isMarkedForEdit() {
		return markedForEdit;
	}

	/**
	 * @param markedForEdit
	 */
	public void setMarkedForEdit(boolean markedForEdit) {
		this.markedForEdit = markedForEdit;
	}

	/**
	 * @return the hostPractitionerNumber
	 */
	public Long getHostPractitionerNumber() {
		return hostPractitionerNumber;
	}

	/**
	 * @param hostPractitionerNumber
	 *            the hostPractitionerNumber to set
	 */
	public void setHostPractitionerNumber(Long hostPractitionerNumber) {
		this.hostPractitionerNumber = hostPractitionerNumber;
	}

	/**
	 * @return the hostPractitionerType
	 */
	public String getHostPractitionerType() {
		return hostPractitionerType;
	}

	/**
	 * @param hostPractitionerType
	 *            the hostPractitionerType to set
	 */
	public void setHostPractitionerType(String hostPractitionerType) {
		this.hostPractitionerType = hostPractitionerType;
	}

	/**
	 * @return the visitingPractitionerNumber
	 */
	public Long getVisitingPractitionerNumber() {
		return visitingPractitionerNumber;
	}

	/**
	 * @param visitingPractitionerNumber
	 *            the visitingPractitionerNumber to set
	 */
	public void setVisitingPractitionerNumber(Long visitingPractitionerNumber) {
		this.visitingPractitionerNumber = visitingPractitionerNumber;
	}

	/**
	 * @return the visitingPractitionerType
	 */
	public String getVisitingPractitionerType() {
		return visitingPractitionerType;
	}

	/**
	 * @param visitingPractitionerType
	 *            the visitingPractitionerType to set
	 */
	public void setVisitingPractitionerType(String visitingPractitionerType) {
		this.visitingPractitionerType = visitingPractitionerType;
	}

	/**
	 * @return the originalEffectiveFrom
	 */
	public Date getOriginalEffectiveFrom() {
		return originalEffectiveFrom;
	}

	/**
	 * @param originalEffectiveFrom
	 *            the originalEffectiveFrom to set
	 */
	public void setOriginalEffectiveFrom(Date originalEffectiveFrom) {
		this.originalEffectiveFrom = originalEffectiveFrom;
	}

}

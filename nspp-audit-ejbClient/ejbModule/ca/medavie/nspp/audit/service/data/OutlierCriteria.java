package ca.medavie.nspp.audit.service.data;

import java.util.Date;

/**
 * Models a OutlierCriteriaEJB object
 */
public class OutlierCriteria {
	/** Holds the value of Subsystem */
	private String subsystem;
	/** Holds the value of Code */
	private String code;

	/** Enum of types the OutlierCriteriaEJB object can have for a value type */
	public static enum Types {
		CHAR, DATE, INTEGER, CURRENCY
	}

	/** Holds the value of the selected type */
	private Types type;
	/** Holds the value of Description */
	private String description;
	/** Value of the OutlierCriteria.. Can be a Date, String, Integer, BigInt(Currency) */
	private Object value;

	/** Edit meta data */
	private Date lastModified;
	private String modifiedBy;

	/**
	 * Default constructor
	 */
	public OutlierCriteria() {
	}

	/**
	 * Constructor for when all parameters for object are available.
	 * 
	 * @param aSubsystem
	 * @param aCode
	 * @param aType
	 * @param aDescription
	 * @param aValue
	 */
	public OutlierCriteria(String aSubsystem, String aCode, Types aType, String aDescription, Object aValue) {
		subsystem = aSubsystem;
		code = aCode;
		type = aType;
		description = aDescription;
		value = aValue;
	}

	/**
	 * @return the subsystem
	 */
	public String getSubsystem() {
		return subsystem;
	}

	/**
	 * @param subsystem
	 *            the subsystem to set
	 */
	public void setSubsystem(String subsystem) {
		this.subsystem = subsystem;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the type
	 */
	public Types getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(Types type) {
		this.type = type;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}

	/**
	 * @return the lastModified
	 */
	public Date getLastModified() {
		return lastModified;
	}

	/**
	 * @return lastModified in string format
	 */
	public String getLastModifiedString() {
		if (lastModified != null) {
			return DataUtilities.SDF_TIME.format(lastModified);
		}
		return "";
	}

	/**
	 * @param lastModified
	 *            the lastModified to set
	 */
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "OutlierCriteriaEJB [subsystem=" + subsystem + ", code=" + code + ", type=" + type + ", description="
				+ description + ", value=" + value + "]";
	}
}

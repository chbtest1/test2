package ca.medavie.nspp.audit.service.data;

import java.util.Date;

/**
 * Models a single Individual Exclusion object
 */
public class IndividualExclusion {
	/** Individuals Health Card Number */
	private String healthCardNumber;
	/** Individuals Name */
	private String name;
	/** Individuals Effective From date (Original value) */
	private Date originalEffectiveFrom;
	/** Individuals Effective From date (Updated via UI) */
	private Date effectiveFrom;
	/** Individuals Effective To date (Original value) */
	private Date originalEffectiveTo;
	/** Individuals Effective To date (Updated via UI) */
	private Date effectiveTo;
	/** Last Modified date */
	private Date lastModified;
	/** Modified By */
	private String modifiedBy;

	/**
	 * @return the healthCardNumber
	 */
	public String getHealthCardNumber() {
		return healthCardNumber;
	}

	/**
	 * @param healthCardNumber
	 *            the healthCardNumber to set
	 */
	public void setHealthCardNumber(String healthCardNumber) {
		this.healthCardNumber = healthCardNumber;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the effectiveFrom
	 */
	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public String getEffectiveFromString() {
		if (effectiveFrom != null) {
			return DataUtilities.SDF.format(effectiveFrom);
		}
		return "";
	}

	/**
	 * @param effectiveFrom
	 *            the effectiveFrom to set
	 */
	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	/**
	 * @return the effectiveTo
	 */
	public Date getEffectiveTo() {
		return effectiveTo;
	}

	public String getEffectiveToString() {
		if (effectiveTo != null) {
			return DataUtilities.SDF.format(effectiveTo);
		}
		return "";
	}

	/**
	 * @param effectiveTo
	 *            the effectiveTo to set
	 */
	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}

	/**
	 * @return the lastModified
	 */
	public Date getLastModified() {
		return lastModified;
	}

	public String getLastModifiedString() {
		if (lastModified != null) {
			return DataUtilities.SDF_TIME.format(lastModified);
		}
		return "";
	}

	/**
	 * @param lastModified
	 *            the lastModified to set
	 */
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the originalEffectiveFrom
	 */
	public Date getOriginalEffectiveFrom() {
		return originalEffectiveFrom;
	}

	/**
	 * @param originalEffectiveFrom
	 *            the originalEffectiveFrom to set
	 */
	public void setOriginalEffectiveFrom(Date originalEffectiveFrom) {
		this.originalEffectiveFrom = originalEffectiveFrom;
	}

	/**
	 * @return the originalEffectiveTo
	 */
	public Date getOriginalEffectiveTo() {
		return originalEffectiveTo;
	}

	/**
	 * @param originalEffectiveTo
	 *            the originalEffectiveTo to set
	 */
	public void setOriginalEffectiveTo(Date originalEffectiveTo) {
		this.originalEffectiveTo = originalEffectiveTo;
	}

	/**
	 * Determines if the Effective From values have been changed by the user since loading from the DB
	 * 
	 * @return true if change is detected, false otherwise.
	 */
	public boolean compareEffectiveFromValues() {

		if (effectiveFrom == null) {
			return originalEffectiveFrom != null;
		} else {
			return !effectiveFrom.equals(originalEffectiveFrom);
		}
	}

	/**
	 * Determines if the Effective To values have been changed by the user since loading from the DB
	 * 
	 * @return true if change is detected, false otherwise.
	 */
	public boolean compareEffectiveToValues() {

		if (effectiveTo == null) {
			return originalEffectiveTo != null;
		} else {
			return !effectiveTo.equals(originalEffectiveTo);
		}
	}
}

package ca.medavie.nspp.audit.service.data;


/**
 * Models a PeerGroup object This represents a provider peer group business object
 */
public class ProviderGroup {
	@ColumnName(name="provider_group_id")
	private long providerGroupId;
	@ColumnName(name="provider_group_name")
	private String providerGroupName;
	@ColumnName(name="provider_group_type_desc")
	private String providerGroupTypeDesc;
	
	public long getProviderGroupId() {
		return providerGroupId;
	}
	public void setProviderGroupId(long providerGroupId) {
		this.providerGroupId = providerGroupId;
	}
	public String getProviderGroupName() {
		return providerGroupName;
	}
	public void setProviderGroupName(String providerGroupName) {
		this.providerGroupName = providerGroupName;
	}
	public String getProviderGroupTypeDesc() {
		return providerGroupTypeDesc;
	}
	public void setProviderGroupTypeDesc(String providerGroupTypeDesc) {
		this.providerGroupTypeDesc = providerGroupTypeDesc;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (providerGroupId ^ (providerGroupId >>> 32));
		result = prime * result + ((providerGroupName == null) ? 0 : providerGroupName.hashCode());
		result = prime * result + ((providerGroupTypeDesc == null) ? 0 : providerGroupTypeDesc.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "ProviderGroup [providerGroupId=" + providerGroupId + ", providerGroupName=" + providerGroupName
				+ ", providerGroupTypeDesc=" + providerGroupTypeDesc + "]";
	}	
}

package ca.medavie.nspp.audit.service.data;

import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Models the search criteria for Health Service Criterias
 */
public class HealthServiceCriteriaSearchCriteria extends BaseSearchCriteria {

	private String programCode;
	private String programDesc;
	private String healthServiceCode;
	private String qualifier;
	private Date effectiveFrom;
	private String msiFeeCode;
	private String healthServiceGroup;
	private String healthServiceCodeDescription;
	private List<String> modifier;
	private List<String> implicitModifier;

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.data.BaseSearchCriteria#resetSearchCriteria(java.lang.Object)
	 */
	@Override
	public void resetSearchCriteria() {
		super.reset(this);
	}

	public String getProgramCode() {
		return programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	/**
	 * @return the programDesc
	 */
	public String getProgramDesc() {
		return programDesc;
	}

	/**
	 * @param programDesc
	 *            the programDesc to set
	 */
	public void setProgramDesc(String program) {
		this.programDesc = program;
	}

	/**
	 * @return the healthServiceCode
	 */
	public String getHealthServiceCode() {
		return healthServiceCode;
	}

	/**
	 * @param healthServiceCode
	 *            the healthServiceCode to set
	 */
	public void setHealthServiceCode(String healthServiceCode) {
		this.healthServiceCode = healthServiceCode;
	}

	/**
	 * @return the qualifier
	 */
	public String getQualifier() {
		return qualifier;
	}

	/**
	 * @param qualifier
	 *            the qualifier to set
	 */
	public void setQualifier(String qualifier) {
		this.qualifier = qualifier;
	}

	/**
	 * @return the effectiveFrom
	 */
	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	/**
	 * @param effectiveFrom
	 *            the effectiveFrom to set
	 */
	public void setEffectiveFrom(Date effective) {
		this.effectiveFrom = effective;
	}

	/**
	 * @return the msiFeeCode
	 */
	public String getMsiFeeCode() {
		return msiFeeCode;
	}

	/**
	 * @param msiFeeCode
	 *            the msiFeeCode to set
	 */
	public void setMsiFeeCode(String msiFeeCode) {
		this.msiFeeCode = msiFeeCode;
	}

	/**
	 * @return the healthServiceGroup
	 */
	public String getHealthServiceGroup() {
		return healthServiceGroup;
	}

	/**
	 * @param healthServiceGroup
	 *            the healthServiceGroup to set
	 */
	public void setHealthServiceGroup(String healthServiceGroup) {
		this.healthServiceGroup = healthServiceGroup;
	}

	/**
	 * @return the healthServiceCodeDescription
	 */
	public String getHealthServiceCodeDescription() {
		return healthServiceCodeDescription;
	}

	/**
	 * @param healthServiceCodeDescription
	 *            the healthServiceCodeDescription to set
	 */
	public void setHealthServiceCodeDescription(String healthServiceCodeDescription) {
		this.healthServiceCodeDescription = healthServiceCodeDescription;
	}

	/**
	 * @return the modifier
	 */
	public List<String> getModifier() {
		return modifier;
	}

	/**
	 * @param modifier
	 *            the modifier to set
	 */
	public void setModifier(List<String> modifier) {
		this.modifier = modifier;
	}

	/**
	 * @return the implicitModifier
	 */
	public List<String> getImplicitModifier() {
		return implicitModifier;
	}

	/**
	 * @param implicitModifier
	 *            the implicitModifier to set
	 */
	public void setImplicitModifier(List<String> implicitModifier) {
		this.implicitModifier = implicitModifier;
	}

	public String getModifierString() {

		StringBuilder modifierStr = new StringBuilder();
		int index = 0;
		int lastIndex = modifier.size();

		// make sure it is in ASC order. it is very important for searching in dss_health_service table for modifers
		// column as it is ordered in ASC order
		Collections.sort(modifier);

		for (String m : modifier) {

			if (index < lastIndex - 1) {
				modifierStr.append(m).append(";");
			} else {
				modifierStr.append(m);
			}

			index++;
		}

		// remove white space
		return modifierStr.toString().trim();

	}

	public String getImplicitModifierString() {

		StringBuilder implicitModifierStr = new StringBuilder();
		int index = 0;
		int lastIndex = implicitModifier.size();

		// make sure it is in ASC order. it is very important for searching in dss_health_service table for
		// implicitModifier
		// column as it is ordered in ASC order
		Collections.sort(implicitModifier);
		for (String m : implicitModifier) {

			if (index < lastIndex - 1) {
				implicitModifierStr.append(m).append(";");
			} else {
				implicitModifierStr.append(m);
			}

			index++;
		}

		// remove white space
		return implicitModifierStr.toString().trim();
	}

	public boolean isProgramSet() {
		return (programCode != null && !programCode.isEmpty()) ? true : false;
	}

	public boolean isHealthServiceGroupSet() {
		return (healthServiceGroup != null && !healthServiceGroup.isEmpty()) ? true : false;
	}

	public boolean isEffectiveDateFromSet() {
		return (effectiveFrom != null) ? true : false;
	}

	public boolean isHealthServiceCodeSet() {
		return (healthServiceCode != null && !healthServiceCode.isEmpty()) ? true : false;
	}

	public boolean isQualifierSet() {
		return (qualifier != null && !qualifier.isEmpty()) ? true : false;
	}

	public boolean isMsiFeeCodeSet() {
		return (msiFeeCode != null && !msiFeeCode.isEmpty()) ? true : false;
	}

	public boolean isHealthServiceDescSet() {
		return (healthServiceCodeDescription != null && !healthServiceCodeDescription.isEmpty()) ? true : false;
	}

	public boolean isModifierSet() {
		return (modifier != null && !modifier.isEmpty()) ? true : false;
	}

	public boolean isImplicitModifierSet() {
		return (implicitModifier != null && !implicitModifier.isEmpty()) ? true : false;
	}

	@Override
	public String toString() {
		return "HealthServiceCriteriaSearchCriteria [programCode=" + programCode + ", programDesc=" + programDesc
				+ ", healthServiceCode=" + healthServiceCode + ", qualifier=" + qualifier + ", effectiveFrom="
				+ effectiveFrom + ", msiFeeCode=" + msiFeeCode + ", healthServiceGroup=" + healthServiceGroup
				+ ", healthServiceCodeDescription=" + healthServiceCodeDescription + ", modifier=" + modifier
				+ ", implicitModifier=" + implicitModifier + "]";
	}
}

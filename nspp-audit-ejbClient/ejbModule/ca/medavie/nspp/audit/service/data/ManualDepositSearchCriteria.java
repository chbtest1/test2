package ca.medavie.nspp.audit.service.data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Models a OutlierCriteriaEJB object
 */
public class ManualDepositSearchCriteria extends BaseSearchCriteria {

	/** Holds the value of Code */
	private String practitionerNumber;
	private String practitionerName;
	private String practitionerCity;
	private String practitionerType;
	private String licenseNumber;
	private String subcategory;
	private BigDecimal depositAmount;
	private Date depositDate;

	private String groupNumber;
	private String groupName;
	private String groupType;
	private String groupCity;

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.data.BaseSearchCriteria#resetSearchCriteria(java.lang.Object)
	 */
	@Override
	public void resetSearchCriteria() {
		super.reset(this);
	}

	public String getPractitionerName() {
		return practitionerName;
	}

	public void setPractitionerName(String practitionerName) {
		this.practitionerName = practitionerName;
	}

	public String getPractitionerCity() {
		return practitionerCity;
	}

	public void setPractitionerCity(String practitionerCity) {
		this.practitionerCity = practitionerCity;
	}

	public String getPractitionerType() {
		return practitionerType;
	}

	public void setPractitionerType(String practitionerType) {
		this.practitionerType = practitionerType;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public String getGroupCity() {
		return groupCity;
	}

	public void setGroupCity(String groupCity) {
		this.groupCity = groupCity;
	}

	/**
	 * Default constructor
	 */
	public ManualDepositSearchCriteria() {
	}

	public enum BusArr {
		FEE("Fee for Service");

		private String label;

		private BusArr(String aLabel) {
			label = aLabel;
		}

		public String getLabel() {
			return label;
		}
	}

	public String getPractitionerNumber() {
		return practitionerNumber;
	}

	public void setPractitionerNumber(String practitionerNumber) {
		this.practitionerNumber = practitionerNumber;
	}

	public String getGroupNumber() {
		return groupNumber;
	}

	public void setGroupNumber(String groupNumber) {
		this.groupNumber = groupNumber;
	}

	public String getSubcategory() {
		return subcategory;
	}

	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}

	/**
	 * @return the depositAmount
	 */
	public BigDecimal getDepositAmount() {
		return depositAmount;
	}

	/**
	 * @param depositAmount
	 *            the depositAmount to set
	 */
	public void setDepositAmount(BigDecimal depositAmount) {
		this.depositAmount = depositAmount;
	}

	/**
	 * @return the depositDate
	 */
	public Date getDepositDate() {
		return depositDate;
	}

	/**
	 * @param depositDate
	 *            the depositDate to set
	 */
	public void setDepositDate(Date depositDate) {
		this.depositDate = depositDate;
	}

	public boolean isPractitionerNumberSet() {
		return (practitionerNumber != null && !practitionerNumber.isEmpty()) ? true : false;
	}

	public boolean isPractitionerNameSet() {
		return (practitionerName != null && !practitionerName.isEmpty()) ? true : false;
	}

	public boolean isPractitionerTypeSet() {
		return (practitionerType != null && !practitionerType.isEmpty()) ? true : false;
	}

	public boolean isPractitionerCitySet() {
		return (practitionerCity != null && !practitionerCity.isEmpty()) ? true : false;
	}

	public boolean isLicenseNumberSet() {
		return (licenseNumber != null && !licenseNumber.isEmpty()) ? true : false;
	}

	public boolean isSubcategorySet() {
		return (subcategory != null && !subcategory.isEmpty()) ? true : false;
	}

	public boolean isGroupNumberSet() {
		return (groupNumber != null && !groupNumber.isEmpty()) ? true : false;
	}

	public boolean isGroupNameSet() {
		return (groupName != null && !groupName.isEmpty()) ? true : false;
	}

	public boolean isGroupCitySet() {
		return (groupCity != null && !groupCity.isEmpty()) ? true : false;
	}

	public boolean isGroupTypeSet() {
		return (groupType != null && !groupType.isEmpty()) ? true : false;
	}

	public boolean isDepositAmountSet() {
		return (depositAmount != null) ? true : false;
	}

	public boolean isDepositDateSet() {
		return (depositDate != null) ? true : false;
	}
}

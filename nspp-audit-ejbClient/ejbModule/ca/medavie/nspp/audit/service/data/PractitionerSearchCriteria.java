package ca.medavie.nspp.audit.service.data;

import java.util.Date;

/**
 * Models parameters a user can send to search for Practitioner
 */
public class PractitionerSearchCriteria extends BaseSearchCriteria {

	private PractitionerTypes profileType;
	private Long practitionerNumber;
	private String subcategory;
	private String specialty;
	private String practitionerName;
	private Long liscenseNumber;
	private Long periodId;
	private String practitionerGroupId;
	private String practitionerGroupName;
	private String city;
	private String businessArrangementDescription;
	private Long businessArrangementNumber;

	// Inquiry specific search criteria
	private Date yearEndDate;
	private Long peerGroupId;
	private String peerGroupName;

	/**
	 * Enum of Practitioner types. Enables easy if statements and selection ability from UI standpoint.
	 */
	public enum PractitionerTypes {
		FEE_FOR_SERVICE("Fee For Service"), SHADOW("Shadow");

		private String label;

		private PractitionerTypes(String aLabel) {
			label = aLabel;
		}

		/**
		 * @return the label
		 */
		public String getLabel() {
			return label;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.data.BaseSearchCriteria#resetSearchCriteria(java.lang.Object)
	 */
	@Override
	public void resetSearchCriteria() {
		super.reset(this);
		this.profileType = PractitionerTypes.FEE_FOR_SERVICE;
	}

	/**
	 * @return the profileType
	 */
	public PractitionerTypes getProfileType() {
		return profileType;
	}

	/**
	 * @param profileType
	 *            the profileType to set
	 */
	public void setProfileType(PractitionerTypes profileType) {
		this.profileType = profileType;
	}

	/**
	 * @return the practitionerNumber
	 */
	public Long getPractitionerNumber() {
		return practitionerNumber;
	}

	/**
	 * @param practitionerNumber
	 *            the practitionerNumber to set
	 */
	public void setPractitionerNumber(Long practitionerNumber) {
		this.practitionerNumber = practitionerNumber;
	}

	/**
	 * @return the liscenseNumber
	 */
	public Long getLiscenseNumber() {
		return liscenseNumber;
	}

	/**
	 * @param liscenseNumber
	 *            the liscenseNumber to set
	 */
	public void setLiscenseNumber(Long liscenseNumber) {
		this.liscenseNumber = liscenseNumber;
	}

	/**
	 * @return the periodId
	 */
	public Long getPeriodId() {
		return periodId;
	}

	/**
	 * @param periodId
	 *            the periodId to set
	 */
	public void setPeriodId(Long periodId) {
		this.periodId = periodId;
	}

	/**
	 * @return the practitionerGroupId
	 */
	public String getPractitionerGroupId() {
		return practitionerGroupId;
	}

	/**
	 * @param practitionerGroupId
	 *            the practitionerGroupId to set
	 */
	public void setPractitionerGroupId(String practitionerGroupId) {
		this.practitionerGroupId = practitionerGroupId;
	}

	/**
	 * @return the practitionerGroupName
	 */
	public String getPractitionerGroupName() {
		return practitionerGroupName;
	}

	/**
	 * @param practitionerGroupName
	 *            the practitionerGroupName to set
	 */
	public void setPractitionerGroupName(String practitionerGroupName) {
		this.practitionerGroupName = practitionerGroupName;
	}

	/**
	 * @return the yearEndDate
	 */
	public Date getYearEndDate() {
		return yearEndDate;
	}

	/**
	 * @param yearEndDate
	 *            the yearEndDate to set
	 */
	public void setYearEndDate(Date yearEndDate) {
		this.yearEndDate = yearEndDate;
	}

	/**
	 * @return the peerGroupId
	 */
	public Long getPeerGroupId() {
		return peerGroupId;
	}

	/**
	 * @param peerGroupId
	 *            the peerGroupId to set
	 */
	public void setPeerGroupId(Long peerGroupId) {
		this.peerGroupId = peerGroupId;
	}

	/**
	 * @return the peerGroupName
	 */
	public String getPeerGroupName() {
		return peerGroupName;
	}

	/**
	 * @param peerGroupName
	 *            the peerGroupName to set
	 */
	public void setPeerGroupName(String peerGroupName) {
		this.peerGroupName = peerGroupName;
	}

	/**
	 * @return the subcategory
	 */
	public String getSubcategory() {
		return subcategory;
	}

	public String getSpecialty() {
		return specialty;
	}

	public void setSpecialty(String specialty) {
		this.specialty = specialty;
	}

	/**
	 * @param subcategory
	 *            the subcategory to set
	 */
	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}

	/**
	 * @return the practitionerName
	 */
	public String getPractitionerName() {
		return practitionerName;
	}

	/**
	 * @param practitionerName
	 *            the practitionerName to set
	 */
	public void setPractitionerName(String practitionerName) {
		this.practitionerName = practitionerName;
	}

	public String getCity() {
		return city;
	}

	public boolean isPractitionerNumberSet() {
		return (practitionerNumber != null) ? true : false;
	}

	public boolean isSubcategorySet() {
		return (subcategory != null && !subcategory.isEmpty()) ? true : false;
	}

	public boolean isSpecialtySet() {
		return (specialty != null && !specialty.isEmpty()) ? true : false;
	}

	public boolean isPractitionerNameSet() {
		return (practitionerName != null && !practitionerName.isEmpty()) ? true : false;
	}

	public boolean isLiscenseNumberSet() {
		return (liscenseNumber != null) ? true : false;
	}

	public boolean isPractitionerGroupIdSet() {
		return (practitionerGroupId != null && !practitionerGroupId.isEmpty()) ? true : false;
	}

	public boolean isPractitionerGroupNameSet() {
		return (practitionerGroupName != null && !practitionerGroupName.isEmpty()) ? true : false;
	}

	public boolean isCitySet() {
		return (city != null && !city.isEmpty()) ? true : false;
	}

	public boolean isBusinessArrangementNumberSet() {
		return (businessArrangementNumber != null);
	}

	public boolean isBusinessArrangementDescriptionSet() {
		return (businessArrangementDescription != null && !businessArrangementDescription.isEmpty());
	}

	public boolean isYearEndDateSet() {
		return (yearEndDate != null);
	}

	public boolean isPeriodIdSet() {
		return (periodId != null);
	}

	public boolean isPeerGroupIdSet() {
		return (peerGroupId != null);
	}

	public boolean isPeerGroupNameSet() {
		return (peerGroupName != null && !peerGroupName.isEmpty()) ? true : false;
	}

	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the businessArrangementNumber
	 */
	public Long getBusinessArrangementNumber() {
		return businessArrangementNumber;
	}

	/**
	 * @param businessArrangementNumber
	 *            the businessArrangementNumber to set
	 */
	public void setBusinessArrangementNumber(Long businessArrangementNumber) {
		this.businessArrangementNumber = businessArrangementNumber;
	}

	/**
	 * @return the businessArrangementDescription
	 */
	public String getBusinessArrangementDescription() {
		return businessArrangementDescription;
	}

	/**
	 * @param businessArrangementDescription
	 *            the businessArrangementDescription to set
	 */
	public void setBusinessArrangementDescription(String businessArrangementDescription) {
		this.businessArrangementDescription = businessArrangementDescription;
	}
}

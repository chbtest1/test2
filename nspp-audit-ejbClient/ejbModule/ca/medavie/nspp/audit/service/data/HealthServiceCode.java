package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * Models a single HealthServiceCode
 */
public class HealthServiceCode implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3499404120222360349L;
	/** The name of the Group this HealthServiceCode belongs to */
	private String healthServiceGroupName;
	private BigDecimal healthServiceGroupId;

	private String healthServiceId;
	private String healthServiceCode;
	private String program;
	private String description;
	private String modifiers;
	private String implicitModifiers;
	private String qualifier;
	private Date effectiveFrom;
	private Date effectiveTo;
	private String category;
	private String unitFormula;
	private String modifiedBy;
	private Date lastModified;
	private BigDecimal totalUnits;

	// Practitioner count for the Health Service
	private BigDecimal numberOfPractitioners;

	// Patient details
	private BigDecimal numberOfPatients;
	private BigDecimal averageNumberOfPatients;
	private BigDecimal standardNumberOfPatientDeviation;
	private BigDecimal numberOfPatientsIndex;

	// Service details
	private BigDecimal numberOfServices;
	private BigDecimal averageNumberOfServices;
	private BigDecimal standardNumberOfServicesDeviation;
	private BigDecimal numberOfServicesIndex;

	// Payment details
	private BigDecimal totalAmountPaid;
	private BigDecimal averageAmountPaid;
	private BigDecimal standardAmountPaidDeviation;
	private BigDecimal totalAmountPaidIndex;

	// % Payment details
	private BigDecimal percentagePayment;
	private BigDecimal averagePercentagePayment;
	private BigDecimal percentagePaymentIndex;

	// Service Per Patient details
	private BigDecimal servicesPerPatient;
	private BigDecimal averageServicesPerPatient;
	private BigDecimal standardServicesPerPatientDeviation;
	private BigDecimal servicesPerPatientIndex;

	// Paid Per Patient details
	private BigDecimal amountPaidPerPatient;
	private BigDecimal averageAmountPaidPerPatient;
	private BigDecimal standardAmountPaidPerPatientDeviation;
	private BigDecimal amountPaidPerPatientIndex;

	// Services Per 100 Patient details
	private BigDecimal numberOfServicesPer100;
	private BigDecimal averageNumberOfServicesPer100;
	private BigDecimal standardNumberOfServicesPer100Deviation;
	private BigDecimal numberOfServicesPer100Index;

	// Paid Per 100 Patient details
	private BigDecimal amountPaidPer100;
	private BigDecimal averageAmountPaidPer100;
	private BigDecimal standardAmountPaidPer100Deviation;
	private BigDecimal amountPaidPer100Index;

	/**
	 * @return the healthServiceGroupName
	 */
	public String getHealthServiceGroupName() {
		return healthServiceGroupName;
	}

	/**
	 * @param healthServiceGroupName
	 *            the healthServiceGroupName to set
	 */
	public void setHealthServiceGroupName(String healthServiceGroupName) {
		this.healthServiceGroupName = healthServiceGroupName;
	}

	/**
	 * @return the healthServiceGroupId
	 */
	public BigDecimal getHealthServiceGroupId() {
		return healthServiceGroupId;
	}

	/**
	 * @param healthServiceGroupId
	 *            the healthServiceGroupId to set
	 */
	public void setHealthServiceGroupId(BigDecimal healthServiceGroupId) {
		this.healthServiceGroupId = healthServiceGroupId;
	}

	/**
	 * @return the healthServiceId
	 */
	public String getHealthServiceId() {
		return healthServiceId;
	}

	/**
	 * @param healthServiceId
	 *            the healthServiceId to set
	 */
	public void setHealthServiceId(String healthServiceId) {
		this.healthServiceId = healthServiceId;
	}

	/**
	 * @return the healthServiceCode
	 */
	public String getHealthServiceCode() {
		return healthServiceCode;
	}

	/**
	 * @param healthServiceCode
	 *            the healthServiceCode to set
	 */
	public void setHealthServiceCode(String healthServiceCode) {
		this.healthServiceCode = healthServiceCode;
	}

	/**
	 * @return the program
	 */
	public String getProgram() {
		return program;
	}

	/**
	 * @param program
	 *            the program to set
	 */
	public void setProgram(String program) {
		this.program = program;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the modifiers
	 */
	public String getModifiers() {
		return modifiers;
	}

	/**
	 * @param modifiers
	 *            the modifiers to set
	 */
	public void setModifiers(String modifiers) {
		this.modifiers = modifiers;
	}

	/**
	 * @return the implicitModifiers
	 */
	public String getImplicitModifiers() {
		return implicitModifiers;
	}

	/**
	 * @param implicitModifiers
	 *            the implicitModifiers to set
	 */
	public void setImplicitModifiers(String implicitModifiers) {
		this.implicitModifiers = implicitModifiers;
	}

	/**
	 * @return the qualifier
	 */
	public String getQualifier() {
		return qualifier;
	}

	/**
	 * @param qualifier
	 *            the qualifier to set
	 */
	public void setQualifier(String qualifier) {
		this.qualifier = qualifier;
	}

	/**
	 * @return the effectiveFrom
	 */
	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	/**
	 * @return effectiveFrom formatted as a string
	 */
	public String getEffectiveFromString() {
		if (effectiveFrom != null) {
			return DataUtilities.SDF.format(effectiveFrom);
		}
		return "";
	}

	/**
	 * @param effectiveFrom
	 *            the effectiveFrom to set
	 */
	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	/**
	 * @return the effectiveTo
	 */
	public Date getEffectiveTo() {
		return effectiveTo;
	}

	/**
	 * @return effectiveTo formatted as a string
	 */
	public String getEffectiveToString() {
		if (effectiveTo != null) {
			return DataUtilities.SDF.format(effectiveTo);
		}
		return "";
	}

	/**
	 * @param effectiveTo
	 *            the effectiveTo to set
	 */
	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the unitFormula
	 */
	public String getUnitFormula() {
		return unitFormula;
	}

	/**
	 * @param unitFormula
	 *            the unitFormula to set
	 */
	public void setUnitFormula(String unitFormula) {
		this.unitFormula = unitFormula;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the lastModified
	 */
	public Date getLastModified() {
		return lastModified;
	}

	/**
	 * @return lastModified formatted as a string
	 */
	public String getLastModifiedString() {
		if (lastModified != null) {
			return DataUtilities.SDF_TIME.format(lastModified);
		}
		return "";
	}

	/**
	 * @param lastModified
	 *            the lastModified to set
	 */
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 * @return the totalUnits
	 */
	public BigDecimal getTotalUnits() {
		return totalUnits;
	}

	/**
	 * @param totalUnits
	 *            the totalUnits to set
	 */
	public void setTotalUnits(BigDecimal totalUnits) {
		this.totalUnits = totalUnits;
	}

	/**
	 * @return the totalAmountPaid
	 */
	public BigDecimal getTotalAmountPaid() {
		return totalAmountPaid;
	}

	/**
	 * @param totalAmountPaid
	 *            the totalAmountPaid to set
	 */
	public void setTotalAmountPaid(BigDecimal totalAmountPaid) {
		this.totalAmountPaid = totalAmountPaid;
	}

	/**
	 * @return the numberOfServicesPer100
	 */
	public BigDecimal getNumberOfServicesPer100() {
		return numberOfServicesPer100;
	}

	/**
	 * @param numberOfServicesPer100
	 *            the numberOfServicesPer100 to set
	 */
	public void setNumberOfServicesPer100(BigDecimal numberOfServicesPer100) {
		this.numberOfServicesPer100 = numberOfServicesPer100;
	}

	/**
	 * @return the amountPaidPer100
	 */
	public BigDecimal getAmountPaidPer100() {
		return amountPaidPer100;
	}

	/**
	 * @param amountPaidPer100
	 *            the amountPaidPer100 to set
	 */
	public void setAmountPaidPer100(BigDecimal amountPaidPer100) {
		this.amountPaidPer100 = amountPaidPer100;
	}

	/**
	 * @return the numberOfPractitioners
	 */
	public BigDecimal getNumberOfPractitioners() {
		return numberOfPractitioners;
	}

	/**
	 * @param numberOfPractitioners
	 *            the numberOfPractitioners to set
	 */
	public void setNumberOfPractitioners(BigDecimal numberOfPractitioners) {
		this.numberOfPractitioners = numberOfPractitioners;
	}

	/**
	 * @return the numberOfPatients
	 */
	public BigDecimal getNumberOfPatients() {
		return numberOfPatients;
	}

	/**
	 * @param numberOfPatients
	 *            the numberOfPatients to set
	 */
	public void setNumberOfPatients(BigDecimal numberOfPatients) {
		this.numberOfPatients = numberOfPatients;
	}

	/**
	 * @return the averageNumberOfPatients
	 */
	public BigDecimal getAverageNumberOfPatients() {
		return averageNumberOfPatients;
	}

	/**
	 * @param averageNumberOfPatients
	 *            the averageNumberOfPatients to set
	 */
	public void setAverageNumberOfPatients(BigDecimal averageNumberOfPatients) {
		this.averageNumberOfPatients = averageNumberOfPatients;
	}

	/**
	 * @return the standardNumberOfPatientDeviation
	 */
	public BigDecimal getStandardNumberOfPatientDeviation() {
		return standardNumberOfPatientDeviation;
	}

	/**
	 * @param standardNumberOfPatientDeviation
	 *            the standardNumberOfPatientDeviation to set
	 */
	public void setStandardNumberOfPatientDeviation(BigDecimal standardNumberOfPatientDeviation) {
		this.standardNumberOfPatientDeviation = standardNumberOfPatientDeviation;
	}

	/**
	 * @return the numberOfPatientsIndex
	 */
	public BigDecimal getNumberOfPatientsIndex() {
		return numberOfPatientsIndex;
	}

	/**
	 * @param numberOfPatientsIndex
	 *            the numberOfPatientsIndex to set
	 */
	public void setNumberOfPatientsIndex(BigDecimal numberOfPatientsIndex) {
		this.numberOfPatientsIndex = numberOfPatientsIndex;
	}

	/**
	 * @return the numberOfServices
	 */
	public BigDecimal getNumberOfServices() {
		return numberOfServices;
	}

	/**
	 * @param numberOfServices
	 *            the numberOfServices to set
	 */
	public void setNumberOfServices(BigDecimal numberOfServices) {
		this.numberOfServices = numberOfServices;
	}

	/**
	 * @return the averageNumberOfServices
	 */
	public BigDecimal getAverageNumberOfServices() {
		return averageNumberOfServices;
	}

	/**
	 * @param averageNumberOfServices
	 *            the averageNumberOfServices to set
	 */
	public void setAverageNumberOfServices(BigDecimal averageNumberOfServices) {
		this.averageNumberOfServices = averageNumberOfServices;
	}

	/**
	 * @return the standardNumberOfServicesDeviation
	 */
	public BigDecimal getStandardNumberOfServicesDeviation() {
		return standardNumberOfServicesDeviation;
	}

	/**
	 * @param standardNumberOfServicesDeviation
	 *            the standardNumberOfServicesDeviation to set
	 */
	public void setStandardNumberOfServicesDeviation(BigDecimal standardNumberOfServicesDeviation) {
		this.standardNumberOfServicesDeviation = standardNumberOfServicesDeviation;
	}

	/**
	 * @return the numberOfServicesIndex
	 */
	public BigDecimal getNumberOfServicesIndex() {
		return numberOfServicesIndex;
	}

	/**
	 * @param numberOfServicesIndex
	 *            the numberOfServicesIndex to set
	 */
	public void setNumberOfServicesIndex(BigDecimal numberOfServicesIndex) {
		this.numberOfServicesIndex = numberOfServicesIndex;
	}

	/**
	 * @return the averageAmountPaid
	 */
	public BigDecimal getAverageAmountPaid() {
		return averageAmountPaid;
	}

	/**
	 * @param averageAmountPaid
	 *            the averageAmountPaid to set
	 */
	public void setAverageAmountPaid(BigDecimal averageAmountPaid) {
		this.averageAmountPaid = averageAmountPaid;
	}

	/**
	 * @return the standardAmountPaidDeviation
	 */
	public BigDecimal getStandardAmountPaidDeviation() {
		return standardAmountPaidDeviation;
	}

	/**
	 * @param standardAmountPaidDeviation
	 *            the standardAmountPaidDeviation to set
	 */
	public void setStandardAmountPaidDeviation(BigDecimal standardAmountPaidDeviation) {
		this.standardAmountPaidDeviation = standardAmountPaidDeviation;
	}

	/**
	 * @return the totalAmountPaidIndex
	 */
	public BigDecimal getTotalAmountPaidIndex() {
		return totalAmountPaidIndex;
	}

	/**
	 * @param totalAmountPaidIndex
	 *            the totalAmountPaidIndex to set
	 */
	public void setTotalAmountPaidIndex(BigDecimal totalAmountPaidIndex) {
		this.totalAmountPaidIndex = totalAmountPaidIndex;
	}

	/**
	 * @return the percentagePayment
	 */
	public BigDecimal getPercentagePayment() {
		return percentagePayment;
	}

	/**
	 * @param percentagePayment
	 *            the percentagePayment to set
	 */
	public void setPercentagePayment(BigDecimal percentagePayment) {
		this.percentagePayment = percentagePayment;
	}

	/**
	 * @return the averagePercentagePayment
	 */
	public BigDecimal getAveragePercentagePayment() {
		return averagePercentagePayment;
	}

	/**
	 * @param averagePercentagePayment
	 *            the averagePercentagePayment to set
	 */
	public void setAveragePercentagePayment(BigDecimal averagePercentagePayment) {
		this.averagePercentagePayment = averagePercentagePayment;
	}

	/**
	 * @return the percentagePaymentIndex
	 */
	public BigDecimal getPercentagePaymentIndex() {
		return percentagePaymentIndex;
	}

	/**
	 * @param percentagePaymentIndex
	 *            the percentagePaymentIndex to set
	 */
	public void setPercentagePaymentIndex(BigDecimal percentagePaymentIndex) {
		this.percentagePaymentIndex = percentagePaymentIndex;
	}

	/**
	 * @return the servicesPerPatient
	 */
	public BigDecimal getServicesPerPatient() {
		return servicesPerPatient;
	}

	/**
	 * @param servicesPerPatient
	 *            the servicesPerPatient to set
	 */
	public void setServicesPerPatient(BigDecimal servicesPerPatient) {
		this.servicesPerPatient = servicesPerPatient;
	}

	/**
	 * @return the averageServicesPerPatient
	 */
	public BigDecimal getAverageServicesPerPatient() {
		return averageServicesPerPatient;
	}

	/**
	 * @param averageServicesPerPatient
	 *            the averageServicesPerPatient to set
	 */
	public void setAverageServicesPerPatient(BigDecimal averageServicesPerPatient) {
		this.averageServicesPerPatient = averageServicesPerPatient;
	}

	/**
	 * @return the standardServicesPerPatientDeviation
	 */
	public BigDecimal getStandardServicesPerPatientDeviation() {
		return standardServicesPerPatientDeviation;
	}

	/**
	 * @param standardServicesPerPatientDeviation
	 *            the standardServicesPerPatientDeviation to set
	 */
	public void setStandardServicesPerPatientDeviation(BigDecimal standardServicesPerPatientDeviation) {
		this.standardServicesPerPatientDeviation = standardServicesPerPatientDeviation;
	}

	/**
	 * @return the servicesPerPatientIndex
	 */
	public BigDecimal getServicesPerPatientIndex() {
		return servicesPerPatientIndex;
	}

	/**
	 * @param servicesPerPatientIndex
	 *            the servicesPerPatientIndex to set
	 */
	public void setServicesPerPatientIndex(BigDecimal servicesPerPatientIndex) {
		this.servicesPerPatientIndex = servicesPerPatientIndex;
	}

	/**
	 * @return the amountPaidPerPatient
	 */
	public BigDecimal getAmountPaidPerPatient() {
		return amountPaidPerPatient;
	}

	/**
	 * @param amountPaidPerPatient
	 *            the amountPaidPerPatient to set
	 */
	public void setAmountPaidPerPatient(BigDecimal amountPaidPerPatient) {
		this.amountPaidPerPatient = amountPaidPerPatient;
	}

	/**
	 * @return the averageAmountPaidPerPatient
	 */
	public BigDecimal getAverageAmountPaidPerPatient() {
		return averageAmountPaidPerPatient;
	}

	/**
	 * @param averageAmountPaidPerPatient
	 *            the averageAmountPaidPerPatient to set
	 */
	public void setAverageAmountPaidPerPatient(BigDecimal averageAmountPaidPerPatient) {
		this.averageAmountPaidPerPatient = averageAmountPaidPerPatient;
	}

	/**
	 * @return the standardAmountPaidPerPatientDeviation
	 */
	public BigDecimal getStandardAmountPaidPerPatientDeviation() {
		return standardAmountPaidPerPatientDeviation;
	}

	/**
	 * @param standardAmountPaidPerPatientDeviation
	 *            the standardAmountPaidPerPatientDeviation to set
	 */
	public void setStandardAmountPaidPerPatientDeviation(BigDecimal standardAmountPaidPerPatientDeviation) {
		this.standardAmountPaidPerPatientDeviation = standardAmountPaidPerPatientDeviation;
	}

	/**
	 * @return the amountPaidPerPatientIndex
	 */
	public BigDecimal getAmountPaidPerPatientIndex() {
		return amountPaidPerPatientIndex;
	}

	/**
	 * @param amountPaidPerPatientIndex
	 *            the amountPaidPerPatientIndex to set
	 */
	public void setAmountPaidPerPatientIndex(BigDecimal amountPaidPerPatientIndex) {
		this.amountPaidPerPatientIndex = amountPaidPerPatientIndex;
	}

	/**
	 * @return the averageNumberOfServicesPer100
	 */
	public BigDecimal getAverageNumberOfServicesPer100() {
		return averageNumberOfServicesPer100;
	}

	/**
	 * @param averageNumberOfServicesPer100
	 *            the averageNumberOfServicesPer100 to set
	 */
	public void setAverageNumberOfServicesPer100(BigDecimal averageNumberOfServicesPer100) {
		this.averageNumberOfServicesPer100 = averageNumberOfServicesPer100;
	}

	/**
	 * @return the standardNumberOfServicesPer100Deviation
	 */
	public BigDecimal getStandardNumberOfServicesPer100Deviation() {
		return standardNumberOfServicesPer100Deviation;
	}

	/**
	 * @param standardNumberOfServicesPer100Deviation
	 *            the standardNumberOfServicesPer100Deviation to set
	 */
	public void setStandardNumberOfServicesPer100Deviation(BigDecimal standardNumberOfServicesPer100Deviation) {
		this.standardNumberOfServicesPer100Deviation = standardNumberOfServicesPer100Deviation;
	}

	/**
	 * @return the numberOfServicesPer100Index
	 */
	public BigDecimal getNumberOfServicesPer100Index() {
		return numberOfServicesPer100Index;
	}

	/**
	 * @param numberOfServicesPer100Index
	 *            the numberOfServicesPer100Index to set
	 */
	public void setNumberOfServicesPer100Index(BigDecimal numberOfServicesPer100Index) {
		this.numberOfServicesPer100Index = numberOfServicesPer100Index;
	}

	/**
	 * @return the averageAmountPaidPer100
	 */
	public BigDecimal getAverageAmountPaidPer100() {
		return averageAmountPaidPer100;
	}

	/**
	 * @param averageAmountPaidPer100
	 *            the averageAmountPaidPer100 to set
	 */
	public void setAverageAmountPaidPer100(BigDecimal averageAmountPaidPer100) {
		this.averageAmountPaidPer100 = averageAmountPaidPer100;
	}

	/**
	 * @return the standardAmountPaidPer100Deviation
	 */
	public BigDecimal getStandardAmountPaidPer100Deviation() {
		return standardAmountPaidPer100Deviation;
	}

	/**
	 * @param standardAmountPaidPer100Deviation
	 *            the standardAmountPaidPer100Deviation to set
	 */
	public void setStandardAmountPaidPer100Deviation(BigDecimal standardAmountPaidPer100Deviation) {
		this.standardAmountPaidPer100Deviation = standardAmountPaidPer100Deviation;
	}

	/**
	 * @return the amountPaidPer100Index
	 */
	public BigDecimal getAmountPaidPer100Index() {
		return amountPaidPer100Index;
	}

	/**
	 * @param amountPaidPer100Index
	 *            the amountPaidPer100Index to set
	 */
	public void setAmountPaidPer100Index(BigDecimal amountPaidPer100Index) {
		this.amountPaidPer100Index = amountPaidPer100Index;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "HealthServiceCode [healthServiceGroupName=" + healthServiceGroupName + ", healthServiceGroupId="
				+ healthServiceGroupId + ", healthServiceId=" + healthServiceId + ", healthServiceCode="
				+ healthServiceCode + ", program=" + program + ", description=" + description + ", modifiers="
				+ modifiers + ", implicitModifiers=" + implicitModifiers + ", qualifier=" + qualifier
				+ ", effectiveFrom=" + effectiveFrom + ", effectiveTo=" + effectiveTo + ", category=" + category
				+ ", unitFormula=" + unitFormula + ", modifiedBy=" + modifiedBy + ", lastModified=" + lastModified
				+ ", totalUnits=" + totalUnits + ", numberOfPractitioners=" + numberOfPractitioners
				+ ", numberOfPatients=" + numberOfPatients + ", averageNumberOfPatients=" + averageNumberOfPatients
				+ ", standardNumberOfPatientDeviation=" + standardNumberOfPatientDeviation + ", numberOfPatientsIndex="
				+ numberOfPatientsIndex + ", numberOfServices=" + numberOfServices + ", averageNumberOfServices="
				+ averageNumberOfServices + ", standardNumberOfServicesDeviation=" + standardNumberOfServicesDeviation
				+ ", numberOfServicesIndex=" + numberOfServicesIndex + ", totalAmountPaid=" + totalAmountPaid
				+ ", averageAmountPaid=" + averageAmountPaid + ", standardAmountPaidDeviation="
				+ standardAmountPaidDeviation + ", totalAmountPaidIndex=" + totalAmountPaidIndex
				+ ", percentagePayment=" + percentagePayment + ", averagePercentagePayment=" + averagePercentagePayment
				+ ", percentagePaymentIndex=" + percentagePaymentIndex + ", servicesPerPatient=" + servicesPerPatient
				+ ", averageServicesPerPatient=" + averageServicesPerPatient + ", standardServicesPerPatientDeviation="
				+ standardServicesPerPatientDeviation + ", servicesPerPatientIndex=" + servicesPerPatientIndex
				+ ", amountPaidPerPatient=" + amountPaidPerPatient + ", averageAmountPaidPerPatient="
				+ averageAmountPaidPerPatient + ", standardAmountPaidPerPatientDeviation="
				+ standardAmountPaidPerPatientDeviation + ", amountPaidPerPatientIndex=" + amountPaidPerPatientIndex
				+ ", numberOfServicesPer100=" + numberOfServicesPer100 + ", averageNumberOfServicesPer100="
				+ averageNumberOfServicesPer100 + ", standardNumberOfServicesPer100Deviation="
				+ standardNumberOfServicesPer100Deviation + ", numberOfServicesPer100Index="
				+ numberOfServicesPer100Index + ", amountPaidPer100=" + amountPaidPer100 + ", averageAmountPaidPer100="
				+ averageAmountPaidPer100 + ", standardAmountPaidPer100Deviation=" + standardAmountPaidPer100Deviation
				+ ", amountPaidPer100Index=" + amountPaidPer100Index + "]";
	}
}

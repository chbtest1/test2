package ca.medavie.nspp.audit.service.data;

/**
 * Used by Health Service Codes. One or many can exist for a Code. Separated by ;
 */
public class Modifier {
	// Human readable representation of the modifier
	private String modifier;
	private String modifierValue;

	/**
	 * @return the modifier
	 */
	public String getModifier() {
		return modifier;
	}

	/**
	 * @param modifier
	 *            the modifier to set
	 */
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	/**
	 * @return the modifierValue
	 */
	public String getModifierValue() {
		return modifierValue;
	}

	/**
	 * @param modifierValue
	 *            the modifierValue to set
	 */
	public void setModifierValue(String modifierValue) {
		this.modifierValue = modifierValue;
	}
}

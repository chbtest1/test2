package ca.medavie.nspp.audit.service.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Models a PeerGroup object This represents a provider peer group business object
 */
public class PeerGroup implements Comparable<PeerGroup> {

	/** Provider Peer Group Information variables */
	@ColumnName(name="provider_peer_group_id")
	private Long peerGroupID;
	
	@ColumnName(name="year_end_date")
	private Date yearEndDate;
	private String yearEndDateStr;
	
	@ColumnName(name="provider_peer_group_name")
	private String peerGroupName;
	
	@ColumnName(name="modified_by")
	private String modifiedBy;
	
	@ColumnName(name="last_modified")
	private Date lastModified;
	
	private String ProviderPeerGroupType;
	private Long numberOfPatientsInGroup;
	private Long numberOfServices;
	private BigDecimal totalUnits;
	private BigDecimal totalAmountPaid;
	private BigDecimal servicesPerPatient;
	private BigDecimal amountPaidPerPatient;

	/** Peer Group Criteria variables */
	private BigDecimal minimumTownPopulation;
	private BigDecimal maximumTownPopulation;
	private BigDecimal minimumEarnings;
	private BigDecimal maximumEarnings;
	private BigDecimal percentageSeEarnings;
	private Boolean townCodeInclusive;

	/** List of Practitioners in the PeerGroup */
	private List<Practitioner> practitioners;

	/** Profile Period details */
	private ProfilePeriod profilePeriod;
	
	private Long numberOfPractitionersInGroup;
	private Long numberOfDroppedPractitionersInGroup;
	private Long numberOfPractitionersUsedForAverage;
	private Long numberOfPractitionersUsedForAvgCalc;

	/** Drop From Average Criteria variables */
	private BigDecimal actMinimumEarnings;
	private BigDecimal minimumShadowEarnings;

	/** Boolean that indicates this peerGroup is selected for Bulk Copy */
	private boolean selectedForCopy;
	/** Based on the target copy date, controls if this peer group can be copied (True by default) */
	private boolean copyDisabled = true;

	/** List of SpecialtyCriteria objects */
	private List<SpecialtyCriteria> specialtyCriterias;

	/** List of TownCriteria objects */
	private List<TownCriteria> townCriterias;

	/** List of HealthService Criteria objects */
	private List<HealthServiceCriteria> healthServiceCriterias;

	/** Holds the totals for the HealthServiceCriteria performed */
	private HealthServiceCriteriaTotals healthServiceCriteriaTotals;

	/** List of SubcategoryType objects */
	private List<SubcategoryType> subcategoryTypes;

	/** List of Fee For Service Practitioners & Shadow Practitioners that belong to this PeerGroup */
	private List<Practitioner> feeForServicePractitioners;
	private List<Practitioner> shadowPractitioners;

	/** Shadow billing indicator */
	private String shadowBillingIndicator;

	/** Holds Payment Information for PeerGroup */
	private PaymentInformation paymentInformation;

	/** Holds the Health Service Codes */
	private List<HealthServiceCode> healthServiceCodes;

	/** for bulk copy only */
	private Date newYearEndDate;

	/**
	 * Default constructor. Instantiates all required object
	 */
	public PeerGroup() {

		// Instantiate lists
		specialtyCriterias = new ArrayList<SpecialtyCriteria>();
		townCriterias = new ArrayList<TownCriteria>();
		healthServiceCriterias = new ArrayList<HealthServiceCriteria>();
		subcategoryTypes = new ArrayList<SubcategoryType>();
		feeForServicePractitioners = new ArrayList<Practitioner>();
		shadowPractitioners = new ArrayList<Practitioner>();
	}

	/**
	 * @param aSubcategoryType
	 */
	public void addSubCategoryType(SubcategoryType aSubcategoryType) {

		subcategoryTypes.add(aSubcategoryType);

	}

	/**
	 * @param aSubcategoryType
	 */
	public void removeSubCategoryType(SubcategoryType aSubcategoryType) {

		subcategoryTypes.remove(aSubcategoryType);

	}

	/**
	 * @param aSpecialtyCriteria
	 */
	public void addSpecialtyCriteria(SpecialtyCriteria aSpecialtyCriteria) {

		specialtyCriterias.add(aSpecialtyCriteria);

	}

	/**
	 * @param aSpecialtyCriteria
	 */
	public void removeSpecialtyCriteria(SpecialtyCriteria aSpecialtyCriteria) {

		specialtyCriterias.remove(aSpecialtyCriteria);

	}

	/**
	 * @param aSubcategoryType
	 */
	public void addTownCriteria(TownCriteria aTownCriteria) {

		townCriterias.add(aTownCriteria);

	}

	/**
	 * @param a
	 *            HealthServiceCriteria
	 */
	public void addHealthServiceCriteria(HealthServiceCriteria aHealthServiceCriteria) {

		healthServiceCriterias.add(aHealthServiceCriteria);

	}

	/**
	 * @return the yearEndDate
	 */
	public Date getYearEndDate() {
		return yearEndDate;
	}

	/**
	 * @return yearEndDate formatted a String
	 */
	public String getYearEndDateString() {
		if (yearEndDate != null) {
			return DataUtilities.SDF.format(yearEndDate);
		}
		return "";
	}

	/**
	 * @param yearEndDate
	 *            the yearEndDate to set
	 */
	public void setYearEndDate(Date yearEndDate) {
		this.yearEndDate = yearEndDate;
	}

	/**
	 * @return the peerGroupName
	 */
	public String getPeerGroupName() {
		return peerGroupName;
	}

	/**
	 * @param peerGroupName
	 *            the peerGroupName to set
	 */
	public void setPeerGroupName(String peerGroupName) {
		this.peerGroupName = peerGroupName;
	}

	/**
	 * @return the minimumTownPopulation
	 */
	public BigDecimal getMinimumTownPopulation() {
		return minimumTownPopulation;
	}

	/**
	 * @param minimumTownPopulation
	 *            the minimumTownPopulation to set
	 */
	public void setMinimumTownPopulation(BigDecimal minimumTownPopulation) {
		this.minimumTownPopulation = minimumTownPopulation;
	}

	/**
	 * @return the maximumTownPopulation
	 */
	public BigDecimal getMaximumTownPopulation() {

		return maximumTownPopulation;
	}

	/**
	 * @param maximumTownPopulation
	 *            the maximumTownPopulation to set
	 */
	public void setMaximumTownPopulation(BigDecimal maximumTownPopulation) {
		this.maximumTownPopulation = maximumTownPopulation;
	}

	/**
	 * @return the minimumEarnings
	 */
	public BigDecimal getMinimumEarnings() {
		return minimumEarnings;
	}

	/**
	 * @param minimumEarnings
	 *            the minimumEarnings to set
	 */
	public void setMinimumEarnings(BigDecimal minimumEarnings) {
		this.minimumEarnings = minimumEarnings;
	}

	/**
	 * @return the maximumEarnings
	 */
	public BigDecimal getMaximumEarnings() {
		return maximumEarnings;
	}

	/**
	 * @param maximumEarnings
	 *            the maximumEarnings to set
	 */
	public void setMaximumEarnings(BigDecimal maximumEarnings) {
		this.maximumEarnings = maximumEarnings;
	}

	/**
	 * @return the percentageSeEarnings
	 */
	public BigDecimal getPercentageSeEarnings() {
		return percentageSeEarnings;
	}

	/**
	 * @param percentageSeEarnings
	 *            the percentageSeEarnings to set
	 */
	public void setPercentageSeEarnings(BigDecimal percentageSeEarnings) {
		this.percentageSeEarnings = percentageSeEarnings;
	}

	/**
	 * @return the townCodeInclusive
	 */
	public Boolean getTownCodeInclusive() {
		return townCodeInclusive;
	}

	/**
	 * @param isTownCodeInclusive
	 *            the townCodeInclusive to set
	 */
	public void setTownCodeInclusive(Boolean isTownCodeInclusive) {
		this.townCodeInclusive = isTownCodeInclusive;
	}

	/**
	 * @return the actMinimumEarnings
	 */
	public BigDecimal getActMinimumEarnings() {
		return actMinimumEarnings;
	}

	/**
	 * @param actMinimumEarnings
	 *            the actMinimumEarnings to set
	 */
	public void setActMinimumEarnings(BigDecimal dropMinimumEarnings) {
		this.actMinimumEarnings = dropMinimumEarnings;
	}

	/**
	 * @return the minimumShadowEarnings
	 */
	public BigDecimal getMinimumShadowEarnings() {
		return minimumShadowEarnings;
	}

	/**
	 * @param minimumShadowEarnings
	 *            the minimumShadowEarnings to set
	 */
	public void setMinimumShadowEarnings(BigDecimal minimumShadowEarnings) {
		this.minimumShadowEarnings = minimumShadowEarnings;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the lastModified
	 */
	public Date getLastModified() {
		return lastModified;
	}

	/**
	 * @return lastModified formatted as a string
	 */
	public String getLastModifiedString() {
		if (lastModified != null) {
			return DataUtilities.SDF_TIME.format(lastModified);
		}
		return "";
	}

	/**
	 * @param lastModified
	 *            the lastModified to set
	 */
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 * @return the specialtyCriterias
	 */
	public List<SpecialtyCriteria> getSpecialtyCriterias() {
		return specialtyCriterias;
	}

	/**
	 * @param specialtyCriterias
	 *            the specialtyCriterias to set
	 */
	public void setSpecialtyCriterias(List<SpecialtyCriteria> specialtyCriterias) {
		this.specialtyCriterias = specialtyCriterias;
	}

	/**
	 * @return the townCriterias
	 */
	public List<TownCriteria> getTownCriterias() {
		return townCriterias;
	}

	/**
	 * @param townCriterias
	 *            the townCriterias to set
	 */
	public void setTownCriterias(List<TownCriteria> townCriterias) {
		this.townCriterias = townCriterias;
	}

	/**
	 * @return the healthServiceCriterias
	 */
	public List<HealthServiceCriteria> getHealthServiceCriterias() {
		return healthServiceCriterias;
	}

	/**
	 * @param healthServiceCriterias
	 *            the healthServiceCriterias to set
	 */
	public void setHealthServiceCriterias(List<HealthServiceCriteria> healthServiceCriterias) {
		this.healthServiceCriterias = healthServiceCriterias;
	}

	/**
	 * @return the subcategoryTypes
	 */
	public List<SubcategoryType> getSubcategoryTypes() {
		return subcategoryTypes;
	}

	/**
	 * @param subcategoryTypes
	 *            the subcategoryTypes to set
	 */
	public void setSubcategoryTypes(List<SubcategoryType> subcategoryTypes) {
		this.subcategoryTypes = subcategoryTypes;
	}

	public Long getPeerGroupID() {
		return peerGroupID;
	}

	public void setPeerGroupID(Long peerGroupID) {
		this.peerGroupID = peerGroupID;
	}

	/**
	 * @return the paymentInformation
	 */
	public PaymentInformation getPaymentInformation() {
		return paymentInformation;
	}

	/**
	 * @param paymentInformation
	 *            the paymentInformation to set
	 */
	public void setPaymentInformation(PaymentInformation paymentInformation) {
		this.paymentInformation = paymentInformation;
	}

	/**
	 * @return the selectedForCopy
	 */
	public boolean isSelectedForCopy() {
		return selectedForCopy;
	}

	/**
	 * @param selectedForCopy
	 *            the selectedForCopy to set
	 */
	public void setSelectedForCopy(boolean selectedForCopy) {
		this.selectedForCopy = selectedForCopy;
	}

	/**
	 * @return the copyDisabled
	 */
	public boolean isCopyDisabled() {
		return copyDisabled;
	}

	/**
	 * @param copyDisabled
	 *            the copyDisabled to set
	 */
	public void setCopyDisabled(boolean copyDisabled) {
		this.copyDisabled = copyDisabled;
	}

	/**
	 * @return the feeForServicePractitioners
	 */
	public List<Practitioner> getFeeForServicePractitioners() {
		return feeForServicePractitioners;
	}

	/**
	 * @param feeForServicePractitioners
	 *            the feeForServicePractitioners to set
	 */
	public void setFeeForServicePractitioners(List<Practitioner> feeForServicePractitioners) {
		this.feeForServicePractitioners = feeForServicePractitioners;
	}

	/**
	 * @return the shadowPractitioners
	 */
	public List<Practitioner> getShadowPractitioners() {
		return shadowPractitioners;
	}

	/**
	 * @param shadowPractitioners
	 *            the shadowPractitioners to set
	 */
	public void setShadowPractitioners(List<Practitioner> shadowPractitioners) {
		this.shadowPractitioners = shadowPractitioners;
	}

	/**
	 * @return the practitioners
	 */
	public List<Practitioner> getPractitioners() {
		return practitioners;
	}

	/**
	 * @param practitioners
	 *            the practitioners to set
	 */
	public void setPractitioners(List<Practitioner> practitioners) {
		this.practitioners = practitioners;
	}

	/**
	 * @return the profilePeriod
	 */
	public ProfilePeriod getProfilePeriod() {
		return profilePeriod;
	}

	/**
	 * @param profilePeriod
	 *            the profilePeriod to set
	 */
	public void setProfilePeriod(ProfilePeriod profilePeriod) {
		this.profilePeriod = profilePeriod;
	}

	/**
	 * @return the numberOfPractitionersInGroup
	 */
	public Long getNumberOfPractitionersInGroup() {
		return numberOfPractitionersInGroup;
	}

	/**
	 * @param numberOfPractitionersInGroup
	 *            the numberOfPractitionersInGroup to set
	 */
	public void setNumberOfPractitionersInGroup(Long numberOfPractitionersInGroup) {
		this.numberOfPractitionersInGroup = numberOfPractitionersInGroup;
	}

	/**
	 * @return the numberOfDroppedPractitionersInGroup
	 */
	public Long getNumberOfDroppedPractitionersInGroup() {
		return numberOfDroppedPractitionersInGroup;
	}

	/**
	 * @param numberOfDroppedPractitionersInGroup
	 *            the numberOfDroppedPractitionersInGroup to set
	 */
	public void setNumberOfDroppedPractitionersInGroup(Long numberOfDroppedPractitionersInGroup) {
		this.numberOfDroppedPractitionersInGroup = numberOfDroppedPractitionersInGroup;
	}

	/**
	 * @param numberOfPractitionersUsedForAverage
	 *            the numberOfPractitionersUsedForAverage to set
	 */
	public void setNumberOfPractitionersUsedForAverage(Long numberOfPractitionersUsedForAverage) {
		this.numberOfPractitionersUsedForAverage = numberOfPractitionersUsedForAverage;
	}

	/**
	 * @return the numberOfPractitionersUsedForAverage
	 */
	public Long getNumberOfPractitionersUsedForAverage() {
		return numberOfPractitionersUsedForAverage;
	}

	/**
	 * @return the healthServiceCodes
	 */
	public List<HealthServiceCode> getHealthServiceCodes() {
		return healthServiceCodes;
	}

	/**
	 * @param healthServiceCodes
	 *            the healthServiceCodes to set
	 */
	public void setHealthServiceCodes(List<HealthServiceCode> healthServiceCodes) {
		this.healthServiceCodes = healthServiceCodes;
	}

	public boolean isStateNew() {
		return (peerGroupID == null || peerGroupID == 0) ? true : false;
	}

	public String getProviderPeerGroupType() {
		return ProviderPeerGroupType;
	}

	public void setProviderPeerGroupType(String providerPeerGroupType) {
		ProviderPeerGroupType = providerPeerGroupType;
	}

	/**
	 * @return the shadowBillingIndicator
	 */
	public String getShadowBillingIndicator() {
		return shadowBillingIndicator;
	}

	/**
	 * @param shadowBillingIndicator
	 *            the shadowBillingIndicator to set
	 */
	public void setShadowBillingIndicator(String shadowBillingIndicator) {
		this.shadowBillingIndicator = shadowBillingIndicator;
	}

	/**
	 * @return the numberOfPatientsInGroup
	 */
	public Long getNumberOfPatientsInGroup() {
		return numberOfPatientsInGroup;
	}

	/**
	 * @param numberOfPatientsInGroup
	 *            the numberOfPatientsInGroup to set
	 */
	public void setNumberOfPatientsInGroup(Long numberOfPatientsInGroup) {
		this.numberOfPatientsInGroup = numberOfPatientsInGroup;
	}

	/**
	 * @return the numberOfServices
	 */
	public Long getNumberOfServices() {
		return numberOfServices;
	}

	/**
	 * @param numberOfServices
	 *            the numberOfServices to set
	 */
	public void setNumberOfServices(Long numberOfServices) {
		this.numberOfServices = numberOfServices;
	}

	/**
	 * @return the totalUnits
	 */
	public BigDecimal getTotalUnits() {
		return totalUnits;
	}

	/**
	 * @param totalUnits
	 *            the totalUnits to set
	 */
	public void setTotalUnits(BigDecimal totalUnits) {
		this.totalUnits = totalUnits;
	}

	/**
	 * @return the totalAmountPaid
	 */
	public BigDecimal getTotalAmountPaid() {
		return totalAmountPaid;
	}

	/**
	 * @param totalAmountPaid
	 *            the totalAmountPaid to set
	 */
	public void setTotalAmountPaid(BigDecimal totalAmountPaid) {
		this.totalAmountPaid = totalAmountPaid;
	}

	/**
	 * @return the servicesPerPatient
	 */
	public BigDecimal getServicesPerPatient() {
		return servicesPerPatient;
	}

	/**
	 * @param servicesPerPatient
	 *            the servicesPerPatient to set
	 */
	public void setServicesPerPatient(BigDecimal servicesPerPatient) {
		this.servicesPerPatient = servicesPerPatient;
	}

	/**
	 * @return the amountPaidPerPatient
	 */
	public BigDecimal getAmountPaidPerPatient() {
		return amountPaidPerPatient;
	}

	/**
	 * @param amountPaidPerPatient
	 *            the amountPaidPerPatient to set
	 */
	public void setAmountPaidPerPatient(BigDecimal amountPaidPerPatient) {
		this.amountPaidPerPatient = amountPaidPerPatient;
	}

	/**
	 * End of Grand total getters
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PeerGroup [peerGroupID=" + peerGroupID + ", yearEndDate=" + yearEndDate + ", peerGroupName="
				+ peerGroupName + ", modifiedBy=" + modifiedBy + ", lastModified=" + lastModified
				+ ", minimumTownPopulation=" + minimumTownPopulation + ", maximumTownPopulation="
				+ maximumTownPopulation + ", minimumEarnings=" + minimumEarnings + ", maximumEarnings="
				+ maximumEarnings + ", percentageSeEarnings=" + percentageSeEarnings + ", townCodeInclusive="
				+ townCodeInclusive + ", actMinimumEarnings=" + actMinimumEarnings + ", minimumShadowEarnings="
				+ minimumShadowEarnings + ", specialtyCriterias=" + specialtyCriterias + ", townCriterias="
				+ townCriterias + ", healthServiceCriterias=" + healthServiceCriterias + ", subcategoryTypes="
				+ subcategoryTypes + "]";
	}

	// Sorting order should be by Peer Group Name Ascending then Year End Date descending
	@Override
	public int compareTo(PeerGroup o) {

		// sorted by Id in ascending order first
		if (this.getPeerGroupID().compareTo(o.getPeerGroupID()) != 0) {

			return this.getPeerGroupID().compareTo(o.getPeerGroupID());
		}
		// if id is same
		else {

			// if year end date is not same sorted by name in descending order
			if (this.getYearEndDate().compareTo(o.getYearEndDate()) != 0) {
				return -(this.getYearEndDate().compareTo(o.getYearEndDate()));
			}

			// at last compare group name
			else {

				return this.getPeerGroupName().compareTo(o.getPeerGroupName());
			}

		}

	}

	public Date getNewYearEndDate() {
		return newYearEndDate;
	}

	public void setNewYearEndDate(Date newYearEndDate) {
		this.newYearEndDate = newYearEndDate;
	}

	/**
	 * @return the yearEndDateStr
	 */
	public String getYearEndDateStr() {
		return yearEndDateStr;
	}

	/**
	 * @param yearEndDateStr
	 *            the yearEndDateStr to set
	 */
	public void setYearEndDateStr(String yearEndDateStr) {
		this.yearEndDateStr = yearEndDateStr;
	}

	/**
	 * @return the healthServiceCriteriaTotals
	 */
	public HealthServiceCriteriaTotals getHealthServiceCriteriaTotals() {
		return healthServiceCriteriaTotals;
	}

	/**
	 * @param healthServiceCriteriaTotals the healthServiceCriteriaTotals to set
	 */
	public void setHealthServiceCriteriaTotals(HealthServiceCriteriaTotals healthServiceCriteriaTotals) {
		this.healthServiceCriteriaTotals = healthServiceCriteriaTotals;
	}

	/**
	 * @return the numberOfPractitionersUsedForAvgCalc
	 */
	public Long getNumberOfPractitionersUsedForAvgCalc() {
		return numberOfPractitionersUsedForAvgCalc;
	}

	/**
	 * @param numberOfPractitionersUsedForAvgCalc the numberOfPractitionersUsedForAvgCalc to set
	 */
	public void setNumberOfPractitionersUsedForAvgCalc(Long numberOfPractitionersUsedForAvgCalc) {
		this.numberOfPractitionersUsedForAvgCalc = numberOfPractitionersUsedForAvgCalc;
	}

}

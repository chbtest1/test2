package ca.medavie.nspp.audit.service.data;

import java.util.Date;

/**
 * Models a specialty object
 */
public class SpecialtyCriteria {

	/** The user who last edited the object */
	private String modifiedBy;
	/** Date that the object was last modified */
	private Date lastModified;
	/** specialtyCode */
	private String specialtyCode;
	/** The specialty */
	private String specialtyDesc;
	/** yearEndDate */
	private Date yearEndDate ;
	/** providerPeerGroupId */
	private long providerPeerGroupId;

	/**
	 * @return the specialty
	 */
	public String getSpecialtyDesc() {
		return specialtyDesc;
	}

	/**
	 * @param aSpecialty
	 *            the specialty to set
	 */
	public void setSpecialtyDesc(String aSpecialtyDesc) {
		this.specialtyDesc = aSpecialtyDesc;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the lastModified
	 */
	public Date getLastModified() {
		return lastModified;
	}
	
	/**
	 * @return string format of lastModified
	 */
	public String getLastModifiedString() {
		if (lastModified != null) {
			return DataUtilities.SDF_TIME.format(lastModified);
		}
		return "";
	}

	/**
	 * @param lastModified
	 *            the lastModified to set
	 */
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 * @return the specialtyCode
	 */
	public String getSpecialtyCode() {
		return specialtyCode;
	}

	/**
	 * @param specialtyCode
	 *            the specialtyCode to set
	 */
	public void setSpecialtyCode(String specialtyCode) {
		this.specialtyCode = specialtyCode;
	}

	public Date getYearEndDate() {
		return yearEndDate;
	}

	public void setYearEndDate(Date yearEndDate) {
		this.yearEndDate = yearEndDate;
	}

	public long getProviderPeerGroupId() {
		return providerPeerGroupId;
	}

	public void setProviderPeerGroupId(long providerPeerGroupId) {
		this.providerPeerGroupId = providerPeerGroupId;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((specialtyCode == null) ? 0 : specialtyCode.hashCode());
		result = prime * result + ((specialtyDesc == null) ? 0 : specialtyDesc.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SpecialtyCriteria other = (SpecialtyCriteria) obj;
		if (specialtyCode == null) {
			if (other.specialtyCode != null)
				return false;
		} else if (!specialtyCode.equals(other.specialtyCode))
			return false;
		if (specialtyDesc == null) {
			if (other.specialtyDesc != null)
				return false;
		} else if (!specialtyDesc.equals(other.specialtyDesc))
			return false;
		return true;
	}

	
}

package ca.medavie.nspp.audit.service.data;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;

/**
 * Models a search criteria for searching TownCriterias
 */
public class TownCriteriaSearchCriteria extends BaseSearchCriteria {

	private String townCodeName;
	private String countyCodeName;
	private String municipalityCodeName;
	private String healthRegionCodeName;

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.data.BaseSearchCriteria#resetSearchCriteria(java.lang.Object)
	 */
	@Override
	public void resetSearchCriteria() {
		super.reset(this);
	}

	/**
	 * @return the townCode
	 */
	public BigDecimal getTownCode() {

		if (townCodeName != null) {

			// Substring out the Town Code ID
			int subStringStart = (townCodeName.lastIndexOf("-") + 1);
			int subStringEnd = townCodeName.length();
			BigDecimal code = new BigDecimal(townCodeName.substring(subStringStart, subStringEnd));

			return code;
		}
		return BigDecimal.ZERO;
	}

	/**
	 * @return the townName
	 */
	public String getTownName() {

		if (townCodeName != null) {

			// Substring out the Town Name
			int subStringEnd = townCodeName.lastIndexOf("-");
			String name = townCodeName.substring(0, subStringEnd);
			return name;
		}
		return null;
	}

	/**
	 * @return the countyCode
	 */
	public BigDecimal getCountyCode() {

		if (countyCodeName != null) {

			// Substring out the County Code ID
			int subStringStart = (countyCodeName.lastIndexOf("-") + 1);
			int subStringEnd = countyCodeName.length();
			BigDecimal code = new BigDecimal(countyCodeName.substring(subStringStart, subStringEnd));

			return code;
		}
		return BigDecimal.ZERO;
	}

	/**
	 * @return the countyName
	 */
	public String getCountyName() {

		if (countyCodeName != null) {

			// Substring out the County Name
			int subStringEnd = countyCodeName.lastIndexOf("-");
			String name = countyCodeName.substring(0, subStringEnd);
			return name;
		}

		return null;
	}

	/**
	 * @return the municipalityCode
	 */
	public BigDecimal getMunicipalityCode() {

		if (municipalityCodeName != null) {

			// Substring out the Municipality Code ID
			int subStringStart = (municipalityCodeName.lastIndexOf("-") + 1);
			int subStringEnd = municipalityCodeName.length();
			BigDecimal code = new BigDecimal(municipalityCodeName.substring(subStringStart, subStringEnd));

			return code;
		}
		return BigDecimal.ZERO;
	}

	/**
	 * @return the municipalityName
	 */
	public String getMunicipalityName() {

		if (municipalityCodeName != null) {

			// Substring out the Municipality Name
			int subStringEnd = municipalityCodeName.lastIndexOf("-");
			String name = municipalityCodeName.substring(0, subStringEnd);
			return name;
		}
		return null;
	}

	/**
	 * @return the healthRegionCodeDesc
	 */
	public String getHealthRegionDesc() {

		if (healthRegionCodeName != null) {

			// Substring out the Health Region Name
			int subStringEnd = healthRegionCodeName.lastIndexOf("-");
			String name = healthRegionCodeName.substring(0, subStringEnd);
			return name;
		}
		return null;
	}

	/**
	 * @return healthRegionCode
	 */
	public BigDecimal getHealthRegionCode() {

		if (healthRegionCodeName != null) {

			// Substring out the County Code ID
			int subStringStart = (healthRegionCodeName.lastIndexOf("-") + 1);
			int subStringEnd = healthRegionCodeName.length();
			BigDecimal code = new BigDecimal(healthRegionCodeName.substring(subStringStart, subStringEnd));

			return code;
		}
		return BigDecimal.ZERO;
	}

	public String getTownCodeName() {
		return townCodeName;
	}

	public void setTownCodeName(String townCodeName) {
		this.townCodeName = townCodeName;
	}

	public String getCountyCodeName() {
		return countyCodeName;
	}

	public void setCountyCodeName(String countyCodeName) {
		this.countyCodeName = countyCodeName;
	}

	public String getMunicipalityCodeName() {
		return municipalityCodeName;
	}

	public void setMunicipalityCodeName(String municipalityCodeName) {
		this.municipalityCodeName = municipalityCodeName;
	}

	public String getHealthRegionCodeName() {
		return healthRegionCodeName;
	}

	public void setHealthRegionCodeName(String healthRegionCodeName) {
		this.healthRegionCodeName = healthRegionCodeName;
	}

	public boolean isTownCodeNameSelected() {
		return !StringUtils.isBlank(this.townCodeName);
	}

	public boolean isCountyCodeNameSelected() {
		return !StringUtils.isBlank(this.countyCodeName);
	}

	public boolean isMunicipalityCodeNameSelected() {
		return !StringUtils.isBlank(this.municipalityCodeName);
	}

	public boolean isHealthRegionCodeNameSelected() {
		return !StringUtils.isBlank(this.healthRegionCodeName);
	}

}

package ca.medavie.nspp.audit.service.data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Models a Practitioner object
 */
public class Practitioner implements Comparable<Practitioner> {

	// Base details of Practitioner
	@ColumnName(name="provider_number")
	private Long practitionerNumber;
	@ColumnName(name="provider_type")
	private String practitionerType;
	@ColumnName(name="last_name")
	private String practitionerName;
	private String gender;
	private Date birthDate;
	private Date optInDate;
	private Date optOutDate;
	private boolean dropIndicator;
	private String forcedIndicator;
	private BigDecimal licenseNumber;
	private Long numberOfPatients;
	private Long numberOfServices;
	private BigDecimal totalUnits;
	private BigDecimal totalAmountPaid;
	private BigDecimal totalAmountPaidFFS;
	private BigDecimal servicesPerPatient;
	private BigDecimal amountPaidPerPatient;

	// Business Arrangement details
	private Long businessArrangementNumber;
	private String businessArrangementDescription;
	
	private Long practitionerGroupNumber;
	private String practitionerGroupDescription;
	
	// Address details
	private String addressLine1;
	private String addressLine2;
	@ColumnName(name="city")
	private String city;
	private String country;
	private String provinceCode;
	private String postalCode;

	// PeerGroup this practitioner is in
	private PeerGroup peerGroup;
	// Age Distribution Information
	private Map<Integer, AgeDistribution> ageDistribution;
	// Holds payment information for the Pracititoner
	private PaymentInformation paymentInformation;

	// Holds all the HealthServicesCodes
	private List<HealthServiceCode> healthServiceCodes;

	/** Holds the totals for the HealthServiceCriteria performed */
	private HealthServiceCriteriaTotals healthServiceCriteriaTotals;

	/** yearEndDate */
	private Date yearEndDate;
	/** providerPeerGroupId */
	private long providerPeerGroupId;

	// Edit meta data
	private String modifiedBy;
	private Date lastModified;

	// Misc properties
	private String shadowBillingIndicator;
	private BigDecimal birthLocationCode;
	private String birthLocationDesc;
	private String chainName;
	private BigDecimal extension;
	private BigDecimal faxNumber;
	private BigDecimal oldMsiBillingNumber;
	private String healthWelfareId;
	private String organizationName;
	private BigDecimal phoneNumber;
	private String posProvCode;
	private String posPrvrId;
	private String posPrvrTypeCode;
	private String posRegionCode;
	private String prescriberIndicator;

	private String validAddressInfoIndicator;

	private String doh;

	/*************************************
	 * Properties for Audit result
	 ************************************* 
	 * **/
	// auditResults associated to this practitioner
	private List<AuditResult> auditResults;

	/**
	 * @return the dropIndicator
	 */
	public boolean isDropIndicator() {
		return dropIndicator;
	}

	/**
	 * @param dropIndicator
	 *            the dropIndicator to set
	 */
	public void setDropIndicator(boolean dropIndicator) {
		this.dropIndicator = dropIndicator;
	}

	/**
	 * @return the forcedIndicator
	 */
	public String getForcedIndicator() {
		return forcedIndicator;
	}

	/**
	 * @param forcedIndicator
	 *            the forcedIndicator to set
	 */
	public void setForcedIndicator(String forcedIndicator) {
		this.forcedIndicator = forcedIndicator;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the lastModified
	 */
	public Date getLastModified() {
		return lastModified;
	}

	/**
	 * @return lastModified formatted as a string
	 */
	public String getLastModifiedString() {
		if (lastModified != null) {
			return DataUtilities.SDF_TIME.format(lastModified);
		}
		return "";
	}

	/**
	 * @param lastModified
	 *            the lastModified to set
	 */
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 * @return the practitionerNumber
	 */
	public Long getPractitionerNumber() {
		return practitionerNumber;
	}

	/**
	 * @param practitionerNumber
	 *            the practitionerNumber to set
	 */
	public void setPractitionerNumber(Long practitionerNumber) {
		this.practitionerNumber = practitionerNumber;
	}

	/**
	 * @return the practitionerType
	 */
	public String getPractitionerType() {
		return practitionerType;
	}

	/**
	 * @param practitionerType
	 *            the practitionerType to set
	 */
	public void setPractitionerType(String practitionerType) {
		this.practitionerType = practitionerType;
	}

	/**
	 * @return the addressLine1
	 */
	public String getAddressLine1() {
		return addressLine1;
	}

	/**
	 * @param addressLine1
	 *            the addressLine1 to set
	 */
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	/**
	 * @return the addressLine2
	 */
	public String getAddressLine2() {
		return addressLine2;
	}

	/**
	 * @param addressLine2
	 *            the addressLine2 to set
	 */
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	/**
	 * @return the birthDate
	 */
	public Date getBirthDate() {
		return birthDate;
	}

	/**
	 * @return birthDate formatted as a string.
	 */
	public String getBirthDateString() {
		if (birthDate != null) {
			return DataUtilities.SDF.format(birthDate);
		}
		return "";
	}

	/**
	 * @param birthDate
	 *            the birthDate to set
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * @return the birthLocationCode
	 */
	public BigDecimal getBirthLocationCode() {
		return birthLocationCode;
	}

	/**
	 * @param birthLocationCode
	 *            the birthLocationCode to set
	 */
	public void setBirthLocationCode(BigDecimal birthLocationCode) {
		this.birthLocationCode = birthLocationCode;
	}

	/**
	 * @return the birthLocationDesc
	 */
	public String getBirthLocationDesc() {
		return birthLocationDesc;
	}

	/**
	 * @param birthLocationDesc
	 *            the birthLocationDesc to set
	 */
	public void setBirthLocationDesc(String birthLocationDesc) {
		this.birthLocationDesc = birthLocationDesc;
	}

	/**
	 * @return the chainName
	 */
	public String getChainName() {
		return chainName;
	}

	/**
	 * @param chainName
	 *            the chainName to set
	 */
	public void setChainName(String chainName) {
		this.chainName = chainName;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the extension
	 */
	public BigDecimal getExtension() {
		return extension;
	}

	/**
	 * @param extension
	 *            the extension to set
	 */
	public void setExtension(BigDecimal extension) {
		this.extension = extension;
	}

	/**
	 * @return the faxNumber
	 */
	public BigDecimal getFaxNumber() {
		return faxNumber;
	}

	/**
	 * @param faxNumber
	 *            the faxNumber to set
	 */
	public void setFaxNumber(BigDecimal faxNumber) {
		this.faxNumber = faxNumber;
	}

	/**
	 * @return the practitionerName
	 */
	public String getPractitionerName() {
		return practitionerName;
	}

	/**
	 * @param practitionerName
	 *            the practitionerName to set
	 */
	public void setPractitionerName(String practitionerName) {
		this.practitionerName = practitionerName;
	}

	/**
	 * @return the oldMsiBillingNumber
	 */
	public BigDecimal getOldMsiBillingNumber() {
		return oldMsiBillingNumber;
	}

	/**
	 * @param oldMsiBillingNumber
	 *            the oldMsiBillingNumber to set
	 */
	public void setOldMsiBillingNumber(BigDecimal oldMsiBillingNumber) {
		this.oldMsiBillingNumber = oldMsiBillingNumber;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the healthWelfareId
	 */
	public String getHealthWelfareId() {
		return healthWelfareId;
	}

	/**
	 * @param healthWelfareId
	 *            the healthWelfareId to set
	 */
	public void setHealthWelfareId(String healthWelfareId) {
		this.healthWelfareId = healthWelfareId;
	}

	/**
	 * @return the organizationName
	 */
	public String getOrganizationName() {
		return organizationName;
	}

	/**
	 * @param organizationName
	 *            the organizationName to set
	 */
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	/**
	 * @return the phoneNumber
	 */
	public BigDecimal getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber
	 *            the phoneNumber to set
	 */
	public void setPhoneNumber(BigDecimal phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the posProvCode
	 */
	public String getPosProvCode() {
		return posProvCode;
	}

	/**
	 * @param posProvCode
	 *            the posProvCode to set
	 */
	public void setPosProvCode(String posProvCode) {
		this.posProvCode = posProvCode;
	}

	/**
	 * @return the posPrvrId
	 */
	public String getPosPrvrId() {
		return posPrvrId;
	}

	/**
	 * @param posPrvrId
	 *            the posPrvrId to set
	 */
	public void setPosPrvrId(String posPrvrId) {
		this.posPrvrId = posPrvrId;
	}

	/**
	 * @return the posPrvrTypeCode
	 */
	public String getPosPrvrTypeCode() {
		return posPrvrTypeCode;
	}

	/**
	 * @param posPrvrTypeCode
	 *            the posPrvrTypeCode to set
	 */
	public void setPosPrvrTypeCode(String posPrvrTypeCode) {
		this.posPrvrTypeCode = posPrvrTypeCode;
	}

	/**
	 * @return the posRegionCode
	 */
	public String getPosRegionCode() {
		return posRegionCode;
	}

	/**
	 * @param posRegionCode
	 *            the posRegionCode to set
	 */
	public void setPosRegionCode(String posRegionCode) {
		this.posRegionCode = posRegionCode;
	}

	/**
	 * @return the prescriberIndicator
	 */
	public String getPrescriberIndicator() {
		return prescriberIndicator;
	}

	/**
	 * @param prescriberIndicator
	 *            the prescriberIndicator to set
	 */
	public void setPrescriberIndicator(String prescriberIndicator) {
		this.prescriberIndicator = prescriberIndicator;
	}

	/**
	 * @return the licenseNumber
	 */
	public BigDecimal getLicenseNumber() {
		return licenseNumber;
	}

	/**
	 * @param licenseNumber
	 *            the licenseNumber to set
	 */
	public void setLicenseNumber(BigDecimal licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	/**
	 * @return the provinceCode
	 */
	public String getProvinceCode() {
		return provinceCode;
	}

	/**
	 * @param provinceCode
	 *            the provinceCode to set
	 */
	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	/**
	 * @return the validAddressInfoIndicator
	 */
	public String getValidAddressInfoIndicator() {
		return validAddressInfoIndicator;
	}

	/**
	 * @param validAddressInfoIndicator
	 *            the validAddressInfoIndicator to set
	 */
	public void setValidAddressInfoIndicator(String validAddressInfoIndicator) {
		this.validAddressInfoIndicator = validAddressInfoIndicator;
	}

	/**
	 * @return the peerGroup
	 */
	public PeerGroup getPeerGroup() {
		return peerGroup;
	}

	/**
	 * @param peerGroup
	 *            the peerGroup to set
	 */
	public void setPeerGroup(PeerGroup peerGroup) {
		this.peerGroup = peerGroup;
	}

	/**
	 * @return the optInDate
	 */
	public Date getOptInDate() {
		return optInDate;
	}

	/**
	 * @return optInDate formatted as a string.
	 */
	public String getOptInDateString() {
		if (optInDate != null) {
			return DataUtilities.SDF.format(optInDate);
		}
		return "";
	}

	/**
	 * @param optInDate
	 *            the optInDate to set
	 */
	public void setOptInDate(Date optInDate) {
		this.optInDate = optInDate;
	}

	/**
	 * @return the optOutDate
	 */
	public Date getOptOutDate() {
		return optOutDate;
	}

	/**
	 * @return optOutDate formatted as a string.
	 */
	public String getOptOutDateString() {
		if (optOutDate != null) {
			return DataUtilities.SDF.format(optOutDate);
		}
		return "";
	}

	/**
	 * @param optOutDate
	 *            the optOutDate to set
	 */
	public void setOptOutDate(Date optOutDate) {
		this.optOutDate = optOutDate;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param postalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * @return the shadowBillingIndicator
	 */
	public String getShadowBillingIndicator() {
		return shadowBillingIndicator;
	}

	/**
	 * @param shadowBillingIndicator
	 *            the shadowBillingIndicator to set
	 */
	public void setShadowBillingIndicator(String shadowBillingIndicator) {
		this.shadowBillingIndicator = shadowBillingIndicator;
	}

	/**
	 * @return the numberOfPatients
	 */
	public Long getNumberOfPatients() {
		return numberOfPatients;
	}

	/**
	 * @param numberOfPatients
	 *            the numberOfPatients to set
	 */
	public void setNumberOfPatients(Long numberOfPatients) {
		this.numberOfPatients = numberOfPatients;
	}

	/**
	 * @return the numberOfServices
	 */
	public Long getNumberOfServices() {
		return numberOfServices;
	}

	/**
	 * @param numberOfServices
	 *            the numberOfServices to set
	 */
	public void setNumberOfServices(Long numberOfServices) {
		this.numberOfServices = numberOfServices;
	}

	/**
	 * @return the totalUnits
	 */
	public BigDecimal getTotalUnits() {
		return totalUnits;
	}

	/**
	 * @param totalUnits
	 *            the totalUnits to set
	 */
	public void setTotalUnits(BigDecimal totalUnits) {
		this.totalUnits = totalUnits;
	}

	/**
	 * @return the totalAmountPaid
	 */
	public BigDecimal getTotalAmountPaid() {
		return totalAmountPaid;
	}

	/**
	 * @param totalAmountPaid
	 *            the totalAmountPaid to set
	 */
	public void setTotalAmountPaid(BigDecimal totalAmountPaid) {
		this.totalAmountPaid = totalAmountPaid;
	}

	public Date getYearEndDate() {
		return yearEndDate;
	}

	public void setYearEndDate(Date yearEndDate) {
		this.yearEndDate = yearEndDate;
	}

	public long getProviderPeerGroupId() {
		return providerPeerGroupId;
	}

	public void setProviderPeerGroupId(long providerPeerGroupId) {
		this.providerPeerGroupId = providerPeerGroupId;
	}

	/**
	 * @return the paymentInformation
	 */
	public PaymentInformation getPaymentInformation() {
		return paymentInformation;
	}

	/**
	 * @param paymentInformation
	 *            the paymentInformation to set
	 */
	public void setPaymentInformation(PaymentInformation paymentInformation) {
		this.paymentInformation = paymentInformation;
	}

	public List<AuditResult> getAuditResults() {
		return auditResults;
	}

	public void setAuditResults(List<AuditResult> auditResults) {
		this.auditResults = auditResults;
	}

	public String getDoh() {
		return doh;
	}

	public void setDoh(String doh) {
		this.doh = doh;
	}

	/**
	 * @return the healthServiceCodes
	 */
	public List<HealthServiceCode> getHealthServiceCodes() {
		return healthServiceCodes;
	}

	/**
	 * @param healthServiceCodes
	 *            the healthServiceCodes to set
	 */
	public void setHealthServiceCodes(List<HealthServiceCode> healthServiceCodes) {
		this.healthServiceCodes = healthServiceCodes;
	}

	/**
	 * @return the ageDistribution
	 */
	public Map<Integer, AgeDistribution> getAgeDistribution() {
		return ageDistribution;
	}

	/**
	 * @param ageDistribution
	 *            the ageDistribution to set
	 */
	public void setAgeDistribution(Map<Integer, AgeDistribution> ageDistribution) {
		this.ageDistribution = ageDistribution;
	}

	/**
	 * @return the servicesPerPatient
	 */
	public BigDecimal getServicesPerPatient() {
		return servicesPerPatient;
	}

	/**
	 * @param servicesPerPatient
	 *            the servicesPerPatient to set
	 */
	public void setServicesPerPatient(BigDecimal servicesPerPatient) {
		this.servicesPerPatient = servicesPerPatient;
	}

	/**
	 * @return the amountPaidPerPatient
	 */
	public BigDecimal getAmountPaidPerPatient() {
		return amountPaidPerPatient;
	}

	/**
	 * @param amountPaidPerPatient
	 *            the amountPaidPerPatient to set
	 */
	public void setAmountPaidPerPatient(BigDecimal amountPaidPerPatient) {
		this.amountPaidPerPatient = amountPaidPerPatient;
	}

	/**
	 * @return the businessArrangementNumber
	 */
	public Long getBusinessArrangementNumber() {
		return businessArrangementNumber;
	}

	/**
	 * @param businessArrangementNumber
	 *            the businessArrangementNumber to set
	 */
	public void setBusinessArrangementNumber(Long businessArrangementNumber) {
		this.businessArrangementNumber = businessArrangementNumber;
	}

	/**
	 * @return the businessArrangementDescription
	 */
	public String getBusinessArrangementDescription() {
		return businessArrangementDescription;
	}

	/**
	 * @param businessArrangementDescription
	 *            the businessArrangementDescription to set
	 */
	public void setBusinessArrangementDescription(String businessArrangementDescription) {
		this.businessArrangementDescription = businessArrangementDescription;
	}

	public Long getPractitionerGroupNumber() {
		return practitionerGroupNumber;
	}

	public void setPractitionerGroupNumber(Long practitionerGroupNumber) {
		this.practitionerGroupNumber = practitionerGroupNumber;
	}

	public String getPractitionerGroupDescription() {
		return practitionerGroupDescription;
	}

	public void setPractitionerGroupDescription(String practitionerGroupDescription) {
		this.practitionerGroupDescription = practitionerGroupDescription;
	}

	/**
	 * @return the healthServiceCriteriaTotals
	 */
	public HealthServiceCriteriaTotals getHealthServiceCriteriaTotals() {
		return healthServiceCriteriaTotals;
	}

	/**
	 * @param healthServiceCriteriaTotals
	 *            the healthServiceCriteriaTotals to set
	 */
	public void setHealthServiceCriteriaTotals(HealthServiceCriteriaTotals healthServiceCriteriaTotals) {
		this.healthServiceCriteriaTotals = healthServiceCriteriaTotals;
	}

	/**
	 * @return the totalAmountPaidFFS
	 */
	public BigDecimal getTotalAmountPaidFFS() {
		return totalAmountPaidFFS;
	}

	/**
	 * @param totalAmountPaidFFS
	 *            the totalAmountPaidFFS to set
	 */
	public void setTotalAmountPaidFFS(BigDecimal totalAmountPaidFFS) {
		this.totalAmountPaidFFS = totalAmountPaidFFS;
	}

	/**
	 * End of Grand total getters
	 */

	@Override
	public String toString() {
		return "Practitioner [practitionerNumber=" + practitionerNumber + ", practitionerType=" + practitionerType
				+ ", practitionerName=" + practitionerName + ", gender=" + gender + ", birthDate=" + birthDate
				+ ", optInDate=" + optInDate + ", optOutDate=" + optOutDate + ", dropIndicator=" + dropIndicator
				+ ", forcedIndicator=" + forcedIndicator + ", licenseNumber=" + licenseNumber + ", addressLine1="
				+ addressLine1 + ", addressLine2=" + addressLine2 + ", city=" + city + ", country=" + country
				+ ", provinceCode=" + provinceCode + ", postalCode=" + postalCode + ", peerGroup=" + peerGroup
				+ ", ageDistribution=" + ageDistribution + ", yearEndDate=" + yearEndDate + ", providerPeerGroupId="
				+ providerPeerGroupId + ", modifiedBy=" + modifiedBy + ", lastModified=" + lastModified
				+ ", shadowBillingIndicator=" + shadowBillingIndicator + ", birthLocationCode=" + birthLocationCode
				+ ", birthLocationDesc=" + birthLocationDesc + ", chainName=" + chainName + ", extension=" + extension
				+ ", faxNumber=" + faxNumber + ", oldMsiBillingNumber=" + oldMsiBillingNumber + ", healthWelfareId="
				+ healthWelfareId + ", organizationName=" + organizationName + ", phoneNumber=" + phoneNumber
				+ ", posProvCode=" + posProvCode + ", posPrvrId=" + posPrvrId + ", posPrvrTypeCode=" + posPrvrTypeCode
				+ ", posRegionCode=" + posRegionCode + ", prescriberIndicator=" + prescriberIndicator
				+ ", validAddressInfoIndicator=" + validAddressInfoIndicator + "]";
	}

	@Override
	public int compareTo(Practitioner o) {

		return this.practitionerNumber.compareTo(o.practitionerNumber);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((practitionerNumber == null) ? 0 : practitionerNumber.hashCode());
		result = prime * result + ((practitionerType == null) ? 0 : practitionerType.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Practitioner other = (Practitioner) obj;
		if (practitionerNumber == null) {
			if (other.practitionerNumber != null)
				return false;
		} else if (!practitionerNumber.equals(other.practitionerNumber))
			return false;
		if (practitionerType == null) {
			if (other.practitionerType != null)
				return false;
		} else if (!practitionerType.equals(other.practitionerType))
			return false;
		return true;
	}

}

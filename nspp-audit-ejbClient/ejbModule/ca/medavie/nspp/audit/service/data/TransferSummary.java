package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;


/**
 * Holds a single transfer summary record
 * 
 * Records helps build up a DssClaimsHistoryTransferSummary record
 */
public class TransferSummary implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 123033739872619004L;
	private String seNumber;
	private String seSequenceNumber;
	private String responseTagNumber;
	private String comments;

	
	/**
	 * @return seNumber, enables value load with Aspose
	 */
	public String seNumber(){
		return seNumber;
	}
	
	/**
	 * @return the seNumber
	 */
	public String getSeNumber() {
		return seNumber;
	}

	/**
	 * @param seNumber
	 *            the seNumber to set
	 */
	public void setSeNumber(String seNumber) {
		this.seNumber = seNumber;
	}

	/**
	 * @return the seSequenceNumber
	 */
	public String getSeSequenceNumber() {
		return seSequenceNumber;
	}

	/**
	 * @param seSequenceNumber
	 *            the seSequenceNumber to set
	 */
	public void setSeSequenceNumber(String seSequenceNumber) {
		this.seSequenceNumber = seSequenceNumber;
	}

	/**
	 * @return the responseTagNumber
	 */
	public String getResponseTagNumber() {
		return responseTagNumber;
	}

	/**
	 * @param responseTagNumber
	 *            the responseTagNumber to set
	 */
	public void setResponseTagNumber(String responseTagNumber) {
		this.responseTagNumber = responseTagNumber;
	}


	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments
	 *            the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
}

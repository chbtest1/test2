package ca.medavie.nspp.audit.service.data;

import java.util.Date;

/**
 * Models a specialty object
 */
public class CodeEntry {
	/** Reference to this codeTableNumber */
	private Integer codeTableNumber;
	/** codeTableEntry */
	private Integer codeTableEntry;
	/** codeTableEntryDescription */
	private String codeTableEntryDescription;

	/** modifiedBy */
	private String modifiedBy;
	/** lastModifiedBy */
	private Date lastModified;

	public Integer getCodeTableNumber() {
		return codeTableNumber;
	}

	public void setCodeTableNumber(Integer codeTableNumber) {
		this.codeTableNumber = codeTableNumber;
	}

	public Integer getCodeTableEntry() {
		return codeTableEntry;
	}

	public void setCodeTableEntry(Integer codeTableEntry) {
		this.codeTableEntry = codeTableEntry;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getCodeTableEntryDescription() {
		return codeTableEntryDescription;
	}

	public void setCodeTableEntryDescription(String codeTableEntryDescription) {
		this.codeTableEntryDescription = codeTableEntryDescription;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codeTableEntry == null) ? 0 : codeTableEntry.hashCode());
		result = prime * result + ((codeTableNumber == null) ? 0 : codeTableNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CodeEntry other = (CodeEntry) obj;
		if (codeTableEntry == null) {
			if (other.codeTableEntry != null)
				return false;
		} else if (!codeTableEntry.equals(other.codeTableEntry))
			return false;
		if (codeTableNumber == null) {
			if (other.codeTableNumber != null)
				return false;
		} else if (!codeTableNumber.equals(other.codeTableNumber))
			return false;
		return true;
	}

}

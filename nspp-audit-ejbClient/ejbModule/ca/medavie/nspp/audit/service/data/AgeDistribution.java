package ca.medavie.nspp.audit.service.data;

import java.math.BigDecimal;

/**
 * Models a single age distribution record
 */
public class AgeDistribution {
	private String ageBracket;
	private BigDecimal practitionerAverage;
	private BigDecimal groupAverage;

	/**
	 * @return the ageBracket
	 */
	public String getAgeBracket() {
		return ageBracket;
	}

	/**
	 * @param ageBracket
	 *            the ageBracket to set
	 */
	public void setAgeBracket(String ageBracket) {
		this.ageBracket = ageBracket;
	}

	/**
	 * @return the practitionerAverage
	 */
	public BigDecimal getPractitionerAverage() {
		return practitionerAverage;
	}

	/**
	 * @param practitionerAverage
	 *            the practitionerAverage to set
	 */
	public void setPractitionerAverage(BigDecimal practitionerAverage) {
		this.practitionerAverage = practitionerAverage;
	}

	/**
	 * @return the groupAverage
	 */
	public BigDecimal getGroupAverage() {
		return groupAverage;
	}

	/**
	 * @param groupAverage
	 *            the groupAverage to set
	 */
	public void setGroupAverage(BigDecimal groupAverage) {
		this.groupAverage = groupAverage;
	}
}

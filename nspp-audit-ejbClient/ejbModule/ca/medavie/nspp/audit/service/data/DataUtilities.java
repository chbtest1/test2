package ca.medavie.nspp.audit.service.data;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map.Entry;

/**
 * Serves as a home for Utility methods for use by business objects
 */
public class DataUtilities {
	/** Does not include time in string conversion */
	public static final SimpleDateFormat SDF = new SimpleDateFormat("dd-MMM-yyyy");
	/** Includes time in string conversion */
	public static final SimpleDateFormat SDF_TIME = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");

	/**
	 * Utility method to update ids for PeerGroup BO
	 * 
	 * @param aPeerGroup
	 * @param aNewID
	 */
	public static void updateIDs4PeerGroups(PeerGroup aPeerGroup, Long aNewID) {

		aPeerGroup.setPeerGroupID(aNewID);

		for (SubcategoryType sts : aPeerGroup.getSubcategoryTypes()) {
			sts.setProviderPeerGroupId(aNewID);
		}

		for (SpecialtyCriteria scs : aPeerGroup.getSpecialtyCriterias()) {
			scs.setProviderPeerGroupId(aNewID);
		}

		for (TownCriteria tc : aPeerGroup.getTownCriterias()) {
			tc.setProviderPeerGroupId(aNewID);
		}

		for (HealthServiceCriteria hsc : aPeerGroup.getHealthServiceCriterias()) {
			hsc.setProviderPeerGroupId(aNewID);
		}

		for (Practitioner pt : aPeerGroup.getFeeForServicePractitioners()) {
			pt.setProviderPeerGroupId(aNewID);
		}

		for (Practitioner spt : aPeerGroup.getShadowPractitioners()) {
			spt.setProviderPeerGroupId(aNewID);
		}

	}

	/**
	 * Utility method to update to a new year end date for PeerGroup BO
	 * 
	 * @param aPeerGoup
	 * @param newDate
	 */
	public static void updateYearEndDate4PeerGroups(PeerGroup aPeerGoup, Date newDate) {

		aPeerGoup.setYearEndDate(newDate);

		for (SubcategoryType sts : aPeerGoup.getSubcategoryTypes()) {
			sts.setYearEndDate(newDate);
		}

		for (SpecialtyCriteria scs : aPeerGoup.getSpecialtyCriterias()) {
			scs.setYearEndDate(newDate);
		}

		for (TownCriteria tc : aPeerGoup.getTownCriterias()) {
			tc.setYearEndDate(newDate);
		}

		for (HealthServiceCriteria hsc : aPeerGoup.getHealthServiceCriterias()) {
			hsc.setYearEndDate(newDate);
		}

		for (Practitioner pt : aPeerGoup.getFeeForServicePractitioners()) {
			pt.setYearEndDate(newDate);
		}

		for (Practitioner spt : aPeerGoup.getShadowPractitioners()) {
			spt.setYearEndDate(newDate);
		}

	}

	/**
	 * Utility method to update last modified meta data for PeerGroup BO
	 * 
	 * @param aPeerGoup
	 */
	public static void updatePeerGroupsModifedMetadata(PeerGroup aPeerGoup) {

		Date modifiedDate = new Date();
		aPeerGoup.setLastModified(new Date());

		for (SubcategoryType sts : aPeerGoup.getSubcategoryTypes()) {
			if (sts.getLastModified() == null) {
				sts.setModifiedBy(aPeerGoup.getModifiedBy());
				sts.setLastModified(modifiedDate);
			}
		}

		for (SpecialtyCriteria scs : aPeerGoup.getSpecialtyCriterias()) {
			if (scs.getLastModified() == null) {
				scs.setModifiedBy(aPeerGoup.getModifiedBy());
				scs.setLastModified(modifiedDate);
			}
		}

		for (TownCriteria tc : aPeerGoup.getTownCriterias()) {
			if (tc.getLastModified() == null) {
				tc.setModifiedBy(aPeerGoup.getModifiedBy());
				tc.setLastModified(modifiedDate);
			}
		}

		for (HealthServiceCriteria hsc : aPeerGoup.getHealthServiceCriterias()) {
			if (hsc.getLastModified() == null) {
				hsc.setModifiedBy(aPeerGoup.getModifiedBy());
				hsc.setLastModified(modifiedDate);
			}
		}

		for (Practitioner pt : aPeerGoup.getFeeForServicePractitioners()) {
			if (pt.getLastModified() == null) {
				pt.setModifiedBy(aPeerGoup.getModifiedBy());
				pt.setLastModified(modifiedDate);
			}
		}

		for (Practitioner spt : aPeerGoup.getShadowPractitioners()) {
			if (spt.getLastModified() == null) {
				spt.setModifiedBy(aPeerGoup.getModifiedBy());
				spt.setLastModified(modifiedDate);
			}
		}
	}

	/**
	 * Utility method to update last modified meta data when bulk copying PeerGroups
	 * 
	 * Similar to the method above but we want to wipe out the last modified and modified by values for bulk copy
	 * regardless if it is set in the DB or not
	 * 
	 * @param aPeerGoup
	 */
	public static void updatePeerGroupsBulkCopyModifedMetadata(PeerGroup aPeerGoup) {

		Date modifiedDate = new Date();
		aPeerGoup.setLastModified(modifiedDate);

		for (SubcategoryType sts : aPeerGoup.getSubcategoryTypes()) {
			sts.setModifiedBy(aPeerGoup.getModifiedBy());
			sts.setLastModified(modifiedDate);
		}

		for (SpecialtyCriteria scs : aPeerGoup.getSpecialtyCriterias()) {
			scs.setModifiedBy(aPeerGoup.getModifiedBy());
			scs.setLastModified(modifiedDate);
		}

		for (TownCriteria tc : aPeerGoup.getTownCriterias()) {
			tc.setModifiedBy(aPeerGoup.getModifiedBy());
			tc.setLastModified(modifiedDate);
		}

		for (HealthServiceCriteria hsc : aPeerGoup.getHealthServiceCriterias()) {
			hsc.setModifiedBy(aPeerGoup.getModifiedBy());
			hsc.setLastModified(modifiedDate);
		}

		for (Practitioner pt : aPeerGoup.getFeeForServicePractitioners()) {
			pt.setModifiedBy(aPeerGoup.getModifiedBy());
			pt.setLastModified(modifiedDate);
		}

		for (Practitioner spt : aPeerGoup.getShadowPractitioners()) {
			spt.setModifiedBy(aPeerGoup.getModifiedBy());
			spt.setLastModified(modifiedDate);
		}
	}

	/**
	 * Convenience method to append ORDER BY to the provided SQL based on the search criteria sorting order
	 * 
	 * @param aQueryString
	 *            - QueryString to append ORDER BY statements to
	 * @param aSearchCriteria
	 */
	public static void appendSQLOrderByStatements(StringBuilder aQueryString, BaseSearchCriteria aSearchCriteria) {

		// Only process query if required arguments are provided.. otherwise do nothing
		if (aQueryString != null && aSearchCriteria != null) {

			// Append the ORDER by portion
			aQueryString.append("ORDER BY ");

			// Get iterator of sorting arguments
			Iterator<Entry<String, String>> iter = aSearchCriteria.getSortingArguments().entrySet().iterator();

			// Loop over sorting argument and apply to query
			while (iter.hasNext()) {

				// Extract key/value
				Entry<String, String> entry = iter.next();
				String columnName = entry.getKey();
				String sortType = entry.getValue();

				aQueryString.append(columnName + " " + sortType);

				if (iter.hasNext()) {
					aQueryString.append(",");
				}
			}
		}
	}

	/**
	 * Converts the given InputStream into a byte array.
	 * @param anInputStream - the given InputSteam to convert
	 * @return the InputStream as a byte array
	 * @throws IOException
	 */
	public static byte[] convertToByteArray(InputStream anInputStream) throws IOException {
		byte[] buffer = new byte[2048];
		int bytesRead;

		try {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream(2000);

			// read from InputStream then write to buffer
			while (-1 != (bytesRead = anInputStream.read(buffer, 0, buffer.length))) {
				outputStream.write(buffer, 0, bytesRead);
			}
			return outputStream.toByteArray();
		} catch (IOException e) {
			throw e;
		}
	}

}

package ca.medavie.nspp.audit.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import ca.medavie.nspp.audit.service.data.BaseSearchCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.PeerGroup;
import ca.medavie.nspp.audit.service.data.PeerGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.TownCriteria;
import ca.medavie.nspp.audit.service.data.TownCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.exception.PeerGroupServiceException;

/**
 * Service layer interface for PeerGroup business logic. Connects the front end controllers to the EJB layer.
 */
@Local
public interface PeerGroupService {

	/**
	 * @param peerGroupSearchCriteria
	 * @param startRow
	 * @param maxRows
	 * @return a list of peer groups by the given search criteria
	 */
	public List<PeerGroup> getSearchedPeerGroups(PeerGroupSearchCriteria peerGroupSearchCriteria, int startRow,
			int maxRows) throws PeerGroupServiceException;

	/**
	 * Counts how many records the entire PeerGroup search will return without pagination
	 * 
	 * @param peerGroupSearchCriteria
	 * @return total PeerGroups found
	 * @throws PeerGroupServiceException
	 */
	public Integer getSearchedPeerGroupsCount(PeerGroupSearchCriteria peerGroupSearchCriteria)
			throws PeerGroupServiceException;

	/**
	 * Loads Practitioners that match the provided search criteria (Searches all existing Practitioners)
	 * 
	 * @param aPractitionerSearchCriteria
	 *            - User specified search Criteria
	 * @return list of matching Practitioners
	 */
	public List<Practitioner> getSearchedPractitioners(PractitionerSearchCriteria aPractitionerSearchCriteria)
			throws PeerGroupServiceException;

	/**
	 * Save peer group to database when save button is clicked
	 * 
	 * @param selectedPeerGroup
	 *            a peer group to save
	 * @throws PeerGroupServiceException
	 * @throws ValidatorException
	 * 
	 */
	public void saveOrUpdatePeerGroup(PeerGroup selectedPeerGroup) throws PeerGroupServiceException,
			PeerGroupServiceException;

	/**
	 * Gets a list of PeerGroups by their latest YearEndDate. All previous version are ignored
	 * 
	 * @return - list of unique PeerGroups
	 */
	public List<PeerGroup> getLatestPeerGroups() throws PeerGroupServiceException;

	/**
	 * Performs Bulk Copy on the provided PeerGroups
	 * 
	 * @param aListOfPeerGroupsToCopy
	 *            - PeerGroups to copy
	 * @param aUserId
	 *            - ID of the user who requested the copy
	 * @throws PeerGroupServiceException
	 */
	public void copyPeerGroups(List<PeerGroup> aListOfPeerGroupsToCopy, String aUserId)
			throws PeerGroupServiceException;

	/**
	 * @param townSearchCriteria
	 *            - given town search criteria
	 * @return A list of town criteria
	 */
	public List<TownCriteria> getTownCriteriaSearchResults(TownCriteriaSearchCriteria townSearchCriteria)
			throws PeerGroupServiceException;

	/**
	 * @return map contains health service effective date from drop down menu
	 */
	public Map<String, String> getHealthServiceEFDropDown() throws PeerGroupServiceException;

	/**
	 * @param hscSearchCriteria
	 * @return
	 * @throws PeerGroupServiceException
	 */
	public List<HealthServiceCriteria> getHealthServiceCriteriaSearchResults(
			HealthServiceCriteriaSearchCriteria hscSearchCriteria) throws PeerGroupServiceException;

	/**
	 * Select a a peer group to edit
	 * 
	 * @param aPeerGroup
	 *            - a peer group being edited
	 * @return a peer group to edit.
	 * @throws PeerGroupServiceException
	 */
	public PeerGroup getPeerGroupDetails(PeerGroup aPeerGroup) throws PeerGroupServiceException;

	/**
	 * @param townSearchCriteria
	 * @param filtered
	 * @return
	 * @throws PeerGroupServiceException
	 */
	public Map<String, BigDecimal>[] getFilteredTownSCDropDowns(BaseSearchCriteria aSearchCriteria)
			throws PeerGroupServiceException;
}

package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.ejb.Local;
import javax.transaction.RollbackException;

import ca.medavie.nspp.audit.service.data.DssUnitValueBO;
import ca.medavie.nspp.audit.service.exception.DssUnitValueServiceException;

/**
 * Service layer interface for Accounting business logic. Connects the front end controllers to the EJB layer.
 */
@Local
public interface DssUnitValueService {

	/**
	 * Get list of DssUnitValueBO
	 * 
	 * @return List of DssUnitValues
	 */
	public List<DssUnitValueBO> getDssUnitValues() throws DssUnitValueServiceException;

	/**
	 * @param aNewDssUnitValue
	 * @throws ServicePersistenceException
	 * @throws RollbackException
	 */
	public DssUnitValueBO saveOrUpdateDssUnitValue(DssUnitValueBO aNewDssUnitValue) throws DssUnitValueServiceException;

	/**
	 * Deletes the specified Dss Unit Values from the system.
	 * 
	 * @param aListOfDssUnitValuesForDelete
	 * @throws DssUnitValueServiceException
	 */
	public void deleteDssUnitValues(List<DssUnitValueBO> aListOfDssUnitValuesForDelete)
			throws DssUnitValueServiceException;

	/**
	 * @param selectedDssValue
	 * @return
	 * @throws DssUnitValueServiceException
	 */
	public Boolean overlaps(DssUnitValueBO selectedDssValue) throws DssUnitValueServiceException;

}

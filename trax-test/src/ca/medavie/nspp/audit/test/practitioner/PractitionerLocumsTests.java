package ca.medavie.nspp.audit.test.practitioner;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import ca.medavie.nspp.audit.service.data.Locum;
import ca.medavie.nspp.audit.service.data.LocumSearchCriteria;
import ca.medavie.nspp.audit.service.repository.LocumRepository;
import ca.medavie.nspp.audit.service.repository.LocumRepositoryImpl;
import ca.medavie.nspp.audit.test.EntityManagement;

/**
 * Places the LocumRepository under test
 */
public class PractitionerLocumsTests {
	private static LocumRepository locumRepository;

	@BeforeClass
	public static void setUpClass() {
		locumRepository = new LocumRepositoryImpl(EntityManagement.getEntityManager());
	}

	/**
	 * Search for specified Locums
	 * 
	 * Verify expected data returned
	 */
	@Test
	public void getSearchedLocumsTest() {
		LocumSearchCriteria sc = new LocumSearchCriteria();
		sc.setPractitionerNumber("102233");

		List<Locum> results = locumRepository.getSearchedLocums(sc, 0, 20);
		// Verify we get results
		assertNotNull(results);
		assertFalse(results.isEmpty());
		// Only 1 record should return
		assertTrue(results.size() == 1);
		// Verify data returned
		for (Locum l : results) {
			assertTrue(l.getPractitionerNumber().equals(Long.valueOf(102233)));
			assertTrue(l.getPractitionerType().equals("PH"));
			assertTrue(l.getCity().trim().equals("HALIFAX"));
		}
	}

	/**
	 * Search Practitioners to be added to Locum
	 * 
	 * Verify expected data returned
	 */
	@Test
	public void getSearchedLocumPractitionersTest() {
		LocumSearchCriteria sc = new LocumSearchCriteria();
		sc.setPractitionerType("PH");

		List<Locum> results = locumRepository.getSearchedLocumPractitioners(sc);
		// Verify we get results
		assertNotNull(results);
		assertFalse(results.isEmpty());
		// Verify data
		for (Locum l : results) {
			assertTrue(l.getPractitionerType().equals("PH"));
		}
	}

	/**
	 * Load the details of the specified Locum
	 * 
	 * 1) Load a host Locum. 2) Load a visitor Locum
	 * 
	 * Verify the correct details return
	 */
	@Test
	public void getLocumDetailsTest() {
		// Host Locum
		Locum hostLocum = new Locum();
		hostLocum.setLocumHost(true);
		hostLocum.setPractitionerNumber(Long.valueOf(102276));
		hostLocum.setPractitionerType("PH");

		hostLocum = locumRepository.getLocumDetails(hostLocum);
		// Verify we get a result
		assertNotNull(hostLocum);
		// Verify details returned
		assertTrue(hostLocum.getPractitionerNumber().equals(Long.valueOf(102276)));
		assertTrue(hostLocum.getPractitionerType().equals("PH"));
		assertFalse(hostLocum.getLocums().isEmpty());
		for (Locum vl : hostLocum.getLocums()) {
			assertTrue(vl.getHostPractitionerNumber().equals(Long.valueOf(102276)));
		}

		// Visitor Locum
		Locum visitorLocum = new Locum();
		visitorLocum.setLocumHost(false);
		visitorLocum.setPractitionerNumber(Long.valueOf(102275));
		visitorLocum.setPractitionerType("PH");

		visitorLocum = locumRepository.getLocumDetails(visitorLocum);
		// Verify we get a result
		assertNotNull(visitorLocum);
		// Verify details returned
		assertTrue(visitorLocum.getPractitionerNumber().equals(Long.valueOf(102275)));
		assertTrue(visitorLocum.getPractitionerType().equals("PH"));
		assertFalse(visitorLocum.getLocums().isEmpty());
		for (Locum hl : visitorLocum.getLocums()) {
			assertTrue(hl.getVisitingPractitionerNumber().equals(Long.valueOf(102275)));
		}
	}

	/**
	 * Save a new Locum record
	 * 
	 * Verify save by loading the new record
	 */
	@Test
	public void saveLocumTest() {
		// TODO not sure how we are going to implement saving tests (Stub?)
	}

	/**
	 * Delete the specified Locum
	 * 
	 * Verify delete by attempting to load the deleted record
	 */
	@Test
	public void deleteLocumsTest() {
		// TODO not sure how we are going to implement deleting tests (Stub?)
	}
}

package ca.medavie.nspp.audit.test.audit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import ca.medavie.nspp.audit.service.data.HealthServiceCode;
import ca.medavie.nspp.audit.service.data.HealthServiceCodeSearchCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceGroup;
import ca.medavie.nspp.audit.service.data.HealthServiceGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceGroupSearchCriteria.HealthServiceGroupSearchTypes;
import ca.medavie.nspp.audit.service.data.HealthServiceProgram;
import ca.medavie.nspp.audit.service.data.Modifier;
import ca.medavie.nspp.audit.service.repository.HealthServiceGroupRepository;
import ca.medavie.nspp.audit.service.repository.HealthServiceGroupRepositoryImpl;
import ca.medavie.nspp.audit.test.BaseTest;

/**
 * Places the HealthServiceGroupRepository under test
 */
public class HealthServiceGroupsTests extends BaseTest {
	private static HealthServiceGroupRepository healthServiceGroupRepository;

	@BeforeClass
	public static void setUpClass() {
		healthServiceGroupRepository = new HealthServiceGroupRepositoryImpl(em);
	}

	/**
	 * Perform 2 HealthServiceGroup searches. 1) HEALTH_SERVICE_GROUP_SEARCH 2) HEALTH_SERVICE_GROUP_SEARCH
	 * 
	 * Search 1 should return a single results, search 2 should return three results
	 */
	@Test
	public void getSearchHealthServiceGroupsTest() {

		// Search style 1 test
		HealthServiceGroupSearchCriteria sc1 = new HealthServiceGroupSearchCriteria();
		sc1.setSearchTypeSelection(HealthServiceGroupSearchTypes.HEALTH_SERVICE_GROUP_SEARCH);
		sc1.setHealthServiceGroupId(Long.valueOf(20));

		List<HealthServiceGroup> results1 = healthServiceGroupRepository.getSearchHealthServiceGroups(sc1, 0, 20);

		// Verify we get results
		assertFalse(results1.isEmpty());
		// Verify we only get one result
		assertTrue(results1.size() == 1);
		// Verify the result is what requested
		for (HealthServiceGroup hsg : results1) {
			assertTrue(hsg.getHealthServiceGroupId().equals(sc1.getHealthServiceGroupId()));
		}

		// Search style 2 test
		HealthServiceGroupSearchCriteria sc2 = new HealthServiceGroupSearchCriteria();
		sc2.setSearchTypeSelection(HealthServiceGroupSearchTypes.HEALTH_SERVICES_SEARCH);
		sc2.setHealthServiceCode("01.01");

		List<HealthServiceGroup> results2 = healthServiceGroupRepository.getSearchHealthServiceGroups(sc2, 0, 20);

		// Verify we got results
		assertFalse(results2.isEmpty());
		// Verify we get 3 results
		assertTrue(results2.size() == 3);
	}

	/**
	 * Perform a HealthServiceGroup search.
	 * 
	 * Verify only the expected results returned
	 */
	@Test
	public void getSearchHealthServiceCodesTest() {
		HealthServiceCodeSearchCriteria sc = new HealthServiceCodeSearchCriteria();
		sc.setProgram("ALP");
		sc.setHealthServiceCode("R1971");

		List<HealthServiceCode> results = healthServiceGroupRepository.getSearchHealthServiceCodes(sc);

		// Verify we get results
		assertFalse(results.isEmpty());
		// Verify we get the results we expected
		for (HealthServiceCode hsc : results) {
			// Have to trim white space off due to how it is saved in DB
			assertTrue(hsc.getProgram().trim().equals(sc.getProgram()));
			assertTrue(hsc.getHealthServiceCode().equals(sc.getHealthServiceCode()));
		}
	}

	/**
	 * Load all the HealthServiceCodes belonging to the specified ID
	 * 
	 * Verify the we got results. (At the time of writing this there is over 15,000 codes returned)
	 */
	@Test
	public void getHealthServiceCodesTest() {
		List<HealthServiceCode> results = healthServiceGroupRepository.getHealthServiceCodes(Long.valueOf(10590));
		// Verify we get results
		assertFalse(results.isEmpty());
	}

	/**
	 * Save changes to an existing HealthServiceGroup
	 * 
	 * Verify by loading the new record and validating it's contents
	 */
	@Test
	public void saveHealthServiceGroupTest() {
		// First load the record we will be editing
		HealthServiceGroupSearchCriteria sc = new HealthServiceGroupSearchCriteria();
		sc.setSearchTypeSelection(HealthServiceGroupSearchTypes.HEALTH_SERVICE_GROUP_SEARCH);
		sc.setHealthServiceGroupId(Long.valueOf(-8888));

		List<HealthServiceGroup> hsgs = healthServiceGroupRepository.getSearchHealthServiceGroups(sc, 0, 20);

		// Verify we get results and that there is only 1
		assertNotNull(hsgs);
		assertFalse(hsgs.isEmpty());
		assertTrue(hsgs.size() == 1);

		HealthServiceGroup hsg = hsgs.get(0);
		hsg.setHealthServiceGroupName("Test has updated the name!");
		hsg.setModifiedBy("JUNIT");

		// Perform save
		em.getTransaction().begin();
		healthServiceGroupRepository.saveHealthServiceGroup(hsg, new ArrayList<HealthServiceCode>());
		em.getTransaction().commit();

		// Verify save by loading the record again
		hsgs.clear();
		hsgs = healthServiceGroupRepository.getSearchHealthServiceGroups(sc, 0, 20);
		// Verify we get results and that there is only 1
		assertNotNull(hsgs);
		assertFalse(hsgs.isEmpty());
		assertTrue(hsgs.size() == 1);

		hsg = hsgs.get(0);
		assertTrue(hsg.getHealthServiceGroupName().equals("Test has updated the name!"));
		assertTrue(hsg.getModifiedBy().trim().equals("JUNIT"));
	}

	/**
	 * Save a new HealthServiceGroup
	 * 
	 * Verify by loading the new record and validating it's contents
	 */
	@Test
	public void saveNewHealthServiceGroupTest() {
		// Create new HSG to save
		HealthServiceGroup hsg = new HealthServiceGroup();
		hsg.setHealthServiceGroupId(Long.valueOf(-7777));
		hsg.setHealthServiceGroupName("New test group");
		hsg.setLastModified(Calendar.getInstance().getTime());
		hsg.setModifiedBy("JUNIT");

		// Save new group
		em.getTransaction().begin();
		healthServiceGroupRepository.saveNewHealthServiceGroup(hsg);
		em.getTransaction().commit();

		// Verify save by loading the group
		HealthServiceGroupSearchCriteria sc = new HealthServiceGroupSearchCriteria();
		sc.setSearchTypeSelection(HealthServiceGroupSearchTypes.HEALTH_SERVICE_GROUP_SEARCH);
		sc.setHealthServiceGroupId(Long.valueOf(-7777));

		List<HealthServiceGroup> hsgs = healthServiceGroupRepository.getSearchHealthServiceGroups(sc, 0, 20);

		// Verify we get results and only 1 result
		assertNotNull(hsgs);
		assertFalse(hsgs.isEmpty());
		assertTrue(hsgs.size() == 1);

		// Verify the saved record
		HealthServiceGroup group = hsgs.get(0);
		assertTrue(group.getHealthServiceGroupName().equals("New test group"));
		assertTrue(group.getModifiedBy().trim().equals("JUNIT"));
	}

	/**
	 * Check if the specified HealthServiceGroupId exists in the system. Two checks are performed. One should return
	 * true and the other false
	 * 
	 * Check 1 should return true for duplicate, check 2 should return false for duplicate
	 */
	@Test
	public void isHealthServiceGroupDuplicateTest() {

		// Check 1 - Expected TRUE
		boolean check1 = healthServiceGroupRepository.isHealthServiceGroupDuplicate(Long.valueOf(1));
		assertTrue(check1);

		// Check 2 - Expected FALSE
		boolean check2 = healthServiceGroupRepository.isHealthServiceGroupDuplicate(Long.valueOf(-1111));
		assertFalse(check2);
	}

	/**
	 * Delete the specified HealthServiceCode from the group
	 * 
	 * Verify the HealthServiceCode is no longer in the group
	 */
	@Test
	public void deleteHealthServiceCodesFromGroupTest() {

		// Load the HealthServiceGroup Codes we wish to delete
		HealthServiceGroupSearchCriteria sc = new HealthServiceGroupSearchCriteria();
		sc.setSearchTypeSelection(HealthServiceGroupSearchTypes.HEALTH_SERVICE_GROUP_SEARCH);
		sc.setHealthServiceGroupId(Long.valueOf(-9999));

		List<HealthServiceGroup> hsgs = healthServiceGroupRepository.getSearchHealthServiceGroups(sc, 0, 20);

		// Verify we get results and only 1 record
		assertNotNull(hsgs);
		assertFalse(hsgs.isEmpty());
		assertTrue(hsgs.size() == 1);

		HealthServiceGroup hsg = hsgs.get(0);

		// Load the group codes
		hsg.setHealthServiceCodes(healthServiceGroupRepository.getHealthServiceCodes(hsg.getHealthServiceGroupId()));

		// Check we got group codes back
		assertFalse(hsg.getHealthServiceCodes().isEmpty());

		// Delete the codes from the system
		em.getTransaction().begin();
		healthServiceGroupRepository.deleteHealthServiceCodesFromGroup(hsg.getHealthServiceGroupId(),
				hsg.getHealthServiceCodes());
		em.getTransaction().commit();

		// Verify delete by loading the Group codes again and check none return
		List<HealthServiceCode> codes = healthServiceGroupRepository.getHealthServiceCodes(hsg
				.getHealthServiceGroupId());
		assertNotNull(codes);
		assertTrue(codes.isEmpty());
	}

	/**
	 * Test loading HealthServicePrograms
	 * 
	 * Verify that we get results back
	 */
	@Test
	public void getHealthServiceProgramsTest() {
		List<HealthServiceProgram> results = healthServiceGroupRepository.getHealthServicePrograms();
		// Verify we get results
		assertFalse(results.isEmpty());
	}

	/**
	 * Test loading Modifiers
	 * 
	 * Verify that we get results back
	 */
	@Test
	public void getModifiersTest() {
		List<Modifier> results = healthServiceGroupRepository.getModifiers();
		// Verify we get results
		assertFalse(results.isEmpty());
	}

	/**
	 * Test loading Implicit Modifiers
	 * 
	 * Verify that we get results back
	 */
	@Test
	public void getImplicitModifiersTest() {
		List<Modifier> results = healthServiceGroupRepository.getImplicitModifiers();
		// Verify we get results
		assertFalse(results.isEmpty());
	}

	/**
	 * Perform 2 Service filter tests.
	 * 
	 * 1) Check for services not in a group with year end date of 01-Jan-1653. There should be 0.
	 * 
	 * 2) Check for services not in a group with year end date of 31-Dec-1997. There should be many records
	 */
	@Test
	public void getServicesNotInGroupTest() {

		Calendar cal = Calendar.getInstance();

		// Filter 1 test
		cal.set(1653, 0, 1);
		List<HealthServiceCode> results1 = healthServiceGroupRepository.getServicesNotInGroup(cal.getTime());
		// Verify no records returned
		assertTrue(results1.isEmpty());

		// Filter 2 test
		cal.set(1997, 11, 31);
		List<HealthServiceCode> results2 = healthServiceGroupRepository.getServicesNotInGroup(cal.getTime());
		// Verify we get records
		assertFalse(results2.isEmpty());
	}
}

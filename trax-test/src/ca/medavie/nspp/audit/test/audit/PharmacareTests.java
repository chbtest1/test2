package ca.medavie.nspp.audit.test.audit;

import org.junit.BeforeClass;
import org.junit.Test;

import ca.medavie.nspp.audit.service.data.PharmacareAuditLetterResponseSearchCriteria;
import ca.medavie.nspp.audit.service.repository.PharmacareRepository;
import ca.medavie.nspp.audit.service.repository.PharmacareRepositoryImpl;
import ca.medavie.nspp.audit.test.EntityManagement;

/**
 * Places the PharmacareRepository under test
 * 
 * TODO need to create some data.. looking to use DB unit
 */
public class PharmacareTests {
	private static PharmacareRepository pharmacareRepository;

	@BeforeClass
	public static void setUpClass() {
		pharmacareRepository = new PharmacareRepositoryImpl(EntityManagement.getEntityManager());
	}

	/**
	 * Search for Audit Letter Responses
	 * 
	 * Verify we get the correct results
	 */
	@Test
	public void getSearchedPharmacareALRTest() {
		PharmacareAuditLetterResponseSearchCriteria sc = new PharmacareAuditLetterResponseSearchCriteria();

	}

	@Test
	public void savePharmacareALRTest() {
		// TODO
	}

	@Test
	public void getSearchPractitionerAuditCriteriasTest() {
		// TODO
	}

	@Test
	public void getDefaultAuditCriteriaByTypeTest() {
		// TODO
	}

	@Test
	public void getPractitionerAuditCriteriaByIdTest() {
		// TODO
	}

	@Test
	public void savePharmacareAuditCriteriaTest() {
		// TODO
	}

	@Test
	public void getSearchDinsTest() {
		// TODO
	}

	@Test
	public void saveIndividualExclusionsTest() {
		// TODO
	}

	@Test
	public void getIndividualExclusionsTest() {
		// TODO
	}
}

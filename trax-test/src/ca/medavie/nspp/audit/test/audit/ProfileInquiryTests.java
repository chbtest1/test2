package ca.medavie.nspp.audit.test.audit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import ca.medavie.nspp.audit.service.data.HealthServiceCode;
import ca.medavie.nspp.audit.service.data.PeerGroup;
import ca.medavie.nspp.audit.service.data.PeerGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.ProfilePeriod;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria.PractitionerTypes;
import ca.medavie.nspp.audit.service.data.SubcategoryType;
import ca.medavie.nspp.audit.service.repository.InquiryRepository;
import ca.medavie.nspp.audit.service.repository.InquiryRepositoryImpl;
import ca.medavie.nspp.audit.test.EntityManagement;

/**
 * Places the InquiryRepository under test
 */
public class ProfileInquiryTests {
	private static InquiryRepository inquiryRepository;

	@BeforeClass
	public static void setUpClass() {
		inquiryRepository = new InquiryRepositoryImpl(EntityManagement.getEntityManager());
	}

	/**
	 * Load Subcategory options
	 * 
	 * Verify we get options
	 */
	@Test
	public void getSubCategoryTypeDropDownTest() {
		Map<String, SubcategoryType> results = inquiryRepository.getSubCategoryTypeDropDown();
		// Verify we get results
		assertFalse(results.isEmpty());
	}

	/**
	 * Search for a specified Practitioner
	 * 
	 * Verify the correct Practitioner returned
	 */
	@Test
	public void getSearchedPractitionersTest() {
		PractitionerSearchCriteria sc = new PractitionerSearchCriteria();
		sc.setPractitionerNumber(Long.valueOf(100044));
		sc.setPeerGroupId(Long.valueOf(45));

		List<Practitioner> results = inquiryRepository.getSearchedPractitioners(sc, 0, 20);
		// Verify we got results
		assertFalse(results.isEmpty());
		// Verify we only got one results
		assertTrue(results.size() == 1);
		// Validate the result
		for (Practitioner p : results) {
			assertTrue(p.getPractitionerNumber().equals(sc.getPractitionerNumber()));
			assertTrue(p.getPeerGroup().getPeerGroupID().equals(sc.getPeerGroupId()));
		}

	}

	/**
	 * Perform a search of Practitioners, then pass it to the load Practitioner details
	 * 
	 * Verify we load the specified Practitioner
	 */
	@Test
	public void getPractitionerDetailsTest() {
		PractitionerSearchCriteria sc = new PractitionerSearchCriteria();
		sc.setPractitionerNumber(Long.valueOf(100044));
		sc.setPeerGroupId(Long.valueOf(45));

		List<Practitioner> results = inquiryRepository.getSearchedPractitioners(sc, 0, 20);
		// Verify we get results
		assertFalse(results.isEmpty());
		// Verify we only get 1 result
		assertTrue(results.size() == 1);

		// Get the Practitioner out of the result list
		Practitioner selectedPractitioner = results.get(0);
		// Load the in-depth details
		selectedPractitioner = inquiryRepository.getPractitionerDetails(selectedPractitioner);
		// Verify details loaded
		assertTrue(selectedPractitioner.getPractitionerNumber().equals(sc.getPractitionerNumber()));
		assertNotNull(selectedPractitioner.getAgeDistribution());
		assertNotNull(selectedPractitioner.getPaymentInformation());
		assertNotNull(selectedPractitioner.getPeerGroup());
		assertNotNull(selectedPractitioner.getHealthServiceCodes());
		assertNotNull(selectedPractitioner.getHealthServiceCriteriaTotals());
	}

	/**
	 * Perform a search of PeerGroups. We perform for a Fee For Service and Shadow search
	 * 
	 * Verify the search returned the correct data
	 */
	@Test
	public void getSearchPeerGroupsTest() {

		// Fee For Service search
		PeerGroupSearchCriteria ffsSc = new PeerGroupSearchCriteria();
		ffsSc.setProfileType(PractitionerTypes.FEE_FOR_SERVICE);
		ffsSc.setPeerGroupId(Long.valueOf(1));

		List<PeerGroup> ffsResults = inquiryRepository.getSearchPeerGroups(ffsSc, 0, 20);
		// Verify we get results
		assertFalse(ffsResults.isEmpty());
		// Verify returned data
		for (PeerGroup pg : ffsResults) {
			assertTrue(pg.getPeerGroupID().equals(ffsSc.getPeerGroupId()));
			assertTrue(pg.getShadowBillingIndicator().equals("N"));
		}

		// Shadow search
		PeerGroupSearchCriteria shadowSc = new PeerGroupSearchCriteria();
		shadowSc.setProfileType(PractitionerTypes.SHADOW);
		shadowSc.setPeerGroupId(Long.valueOf(1));

		List<PeerGroup> shadowResults = inquiryRepository.getSearchPeerGroups(shadowSc, 0, 20);
		// Verify we get results
		assertFalse(shadowResults.isEmpty());
		// Verify returned data
		for (PeerGroup pg : shadowResults) {
			assertTrue(pg.getPeerGroupID().equals(shadowSc.getPeerGroupId()));
			assertTrue(pg.getShadowBillingIndicator().equals("Y"));
		}
	}

	/**
	 * Load the PeerGroup details of the specified PG
	 * 
	 * Verify the details loaded
	 */
	@Test
	public void getPeerGroupDetailsTest() {
		PeerGroup pg = new PeerGroup();
		pg.setPeerGroupID(Long.valueOf(33));
		pg.setShadowBillingIndicator("N");

		// Create a fake profile period for PG
		ProfilePeriod pp = new ProfilePeriod();
		pp.setPeriodId(Long.valueOf(2));
		pg.setProfilePeriod(pp);

		// Create a fake Year End for PG
		Calendar cal = Calendar.getInstance();
		cal.set(1997, 11, 31); // December 31st, 1997
		pg.setYearEndDate(cal.getTime());

		// Load PeerGroup details
		pg = inquiryRepository.getPeerGroupDetails(pg);
		// Verify results
		assertTrue(pg.getPeerGroupID().equals(Long.valueOf(33)));
		assertNotNull(pg.getHealthServiceCriteriaTotals());
		assertNotNull(pg.getPaymentInformation());
	}

	/**
	 * Request the drill down details for the specified Practitioner HS
	 * 
	 * Verify the data drill down data
	 */
	@Test
	public void getHealthServiceDrillDownDetailsTest() {
		// Construct a fake Practitioner structure to request the drill down details
		Practitioner p = new Practitioner();
		p.setPractitionerNumber(Long.valueOf(100059));
		p.setPractitionerType("PH");
		p.setShadowBillingIndicator("N");

		PeerGroup pg = new PeerGroup();
		pg.setPeerGroupID(Long.valueOf(7));

		Calendar cal = Calendar.getInstance();
		cal.set(1997, 11, 31, 0, 0, 0); // December 31st, 1997
		cal.set(Calendar.MILLISECOND, 0);
		pg.setYearEndDate(cal.getTime());

		ProfilePeriod pp = new ProfilePeriod();
		pp.setPeriodId(Long.valueOf(2));
		cal.set(1996, 6, 1, 0, 0, 0); // July 1st, 1996
		cal.set(Calendar.MILLISECOND, 0);
		pp.setPeriodStartDate(cal.getTime());
		cal.set(2099, 0, 1, 0, 0, 0); // January 1st, 2099
		cal.set(Calendar.MILLISECOND, 0);
		pp.setPeriodEndDate(cal.getTime());

		pg.setProfilePeriod(pp);
		p.setPeerGroup(pg);

		// Construct a Fake HealthServiceCode structure
		HealthServiceCode hsc = new HealthServiceCode();
		hsc.setHealthServiceGroupId(BigDecimal.valueOf(10600));

		// Load the drill down details
		List<HealthServiceCode> results = inquiryRepository.getHealthServiceDrillDownDetails(p, hsc);
		// Verify results
		assertFalse(results.isEmpty());
		for (HealthServiceCode code : results) {
			assertTrue(code.getEffectiveFrom().before(pp.getPeriodEndDate()));
			assertTrue(code.getEffectiveTo().after(pp.getPeriodStartDate()));
		}
	}
}

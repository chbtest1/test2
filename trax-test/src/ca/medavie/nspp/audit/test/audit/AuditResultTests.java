package ca.medavie.nspp.audit.test.audit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.openjpa.persistence.ArgumentException;
import org.junit.BeforeClass;
import org.junit.Test;

import ca.medavie.nspp.audit.service.data.AuditResult;
import ca.medavie.nspp.audit.service.data.AuditResultAttachment;
import ca.medavie.nspp.audit.service.data.DataUtilities;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.repository.AuditResultRepository;
import ca.medavie.nspp.audit.service.repository.AuditResultRepositoryImpl;
import ca.medavie.nspp.audit.test.BaseTest;

/**
 * Places the AuditResultRepository under test
 */
public class AuditResultTests extends BaseTest {
	private static AuditResultRepository auditResultRepository;

	@BeforeClass
	public static void setUpClass() {
		auditResultRepository = new AuditResultRepositoryImpl(em);
	}

	/**
	 * Test search for Practitioner -7777.
	 * 
	 * Check that only one record returned and the number matches the search criteria
	 */
	@Test
	public void getPractitionerSearchResultTest() {
		PractitionerSearchCriteria sc = new PractitionerSearchCriteria();
		sc.setPractitionerNumber(Long.valueOf(-7777));

		List<Practitioner> results = auditResultRepository.getPractitionerSearchResult(sc);
		assertTrue(results != null && !results.isEmpty());

		// Check that we only got one record
		assertTrue(results.size() == 1);

		// Check that the record returned matches the specified Practitioner number
		assertTrue(results.get(0).getPractitionerNumber().equals(sc.getPractitionerNumber()));
	}

	/**
	 * Save a new Audit Result.
	 * 
	 * Verify success by checking for exception and loading record following save
	 */
	@Test
	public void setAuditResultTest() {

		AuditResult ar = new AuditResult();
		ar.setProviderNumber(Long.valueOf(900000));
		ar.setProviderType("PH");

		Calendar cal = Calendar.getInstance();
		ar.setAuditDate(cal.getTime());
		ar.setAuditPerson("ZZZ");
		ar.setAuditIndicator("A");
		ar.setFinalContactDate(cal.getTime());
		ar.setAuditRecoveryAmount(BigDecimal.valueOf(22.98));
		ar.setNumberOfServicesAudited(Integer.valueOf(5));
		ar.setInappropriatlyBilledServices(Integer.valueOf(1));
		ar.setAuditType("EA");
		ar.setLastModified(cal.getTime());
		ar.setModifiedBy("JUNIT");

		// Save Audit Result
		em.getTransaction().begin();
		auditResultRepository.setAuditResult(ar);
		em.getTransaction().commit();

		// Load the newly added record to verify save
		Practitioner p = new Practitioner();
		p.setPractitionerNumber(Long.valueOf(900000));
		p.setPractitionerType("PH");

		List<AuditResult> audits = auditResultRepository.getSelectedPractitionerAuditResultDetails(p);
		// Should only get 1 record back
		assertNotNull(audits);
		assertTrue(audits.size() == 1);
	}

	/**
	 * Test loading an Audit Result.
	 * 
	 * Verify object loaded is not null and contains specified search/load data
	 */
	@Test
	public void getSelectedPractitionerAuditResultDetailsTest() {

		Practitioner p = new Practitioner();
		p.setPractitionerNumber(Long.valueOf(-7777));
		p.setPractitionerType("PH");

		// Attempt to load audits for the specified Practitioner
		List<AuditResult> results = auditResultRepository.getSelectedPractitionerAuditResultDetails(p);

		// Check that there is at least 1 audit
		assertFalse(results.isEmpty());
		assertTrue(results.size() == 1);

		// Check that the Audits belong to the specified Practitioner
		for (AuditResult ar : results) {
			assertTrue(p.getPractitionerNumber().equals(ar.getProviderNumber())
					&& p.getPractitionerType().equals(ar.getProviderType()));
		}
	}

	/**
	 * Attempt to save an attachment with an empty audit
	 * 
	 * Verify attachments cannot be uploaded for non-existent audit results
	 */
	@Test
	public void saveEmptyAuditWithAttachment() {
		try {
			File file = new File("resources/test_image1.jpg");

			AuditResult audit = new AuditResult();
			AuditResultAttachment attachment = new AuditResultAttachment();

			attachment.setAttachmentId(new Long(-5555));
			attachment.setFilename(file.getName());
			attachment.setMimeType("image/jpeg");
			attachment.setLastModified(new Date());
			attachment.setModifiedBy("JUNIT");

			audit.getAttachments().add(attachment);

			auditResultRepository.setAuditResult(audit);

			byte[] fileBinary = DataUtilities.convertToByteArray(new FileInputStream(file));

			auditResultRepository.setAttachmentBinary(attachment.getAttachmentId(), fileBinary);

			fail();
		} catch (ArgumentException e) {
			// Do nothing; expected result
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	/**
	 * Uploads attachments with all valid MIME types
	 * 
	 * Verify all attachments are uploaded and retrieved successfully
	 * 
	 * NOTE: THIS TEST WILL FAIL IF RUN AS PART OF THE SUITE, RUNS SUCESSFULLY IF RUN BY ITSELF
	 */
	@Test
	public void saveAttachmentWithValidExtensions() {
		try {

			// Load test files
			File docFile = new File("resources/test_doc1.doc");
			File docxFile = new File("resources/test_doc2.docx");
			File pdfFile = new File("resources/test_doc3.pdf");
			File rtfFile = new File("resources/test_doc4.rtf");
			File txtFile = new File("resources/test_doc5.txt");
			File xpsFile = new File("resources/test_doc6.xps");

			File jpgFile = new File("resources/test_image1.jpg");
			File pngFile = new File("resources/test_image2.png");
			File tiffFile = new File("resources/test_image3.tiff");

			File xlsFile = new File("resources/test_sheet1.xls");
			File xlsmFile = new File("resources/test_sheet2.xlsm");
			File xlsxFile = new File("resources/test_sheet3.xlsx");

			Practitioner practitioner = new Practitioner();

			practitioner.setPractitionerNumber(new Long(-7777));
			practitioner.setPractitionerType("PH");

			// Retrieve the AuditResult
			AuditResult audit = auditResultRepository.getSelectedPractitionerAuditResultDetails(practitioner).get(0);

			// Create the attachment objects
			AuditResultAttachment docAttachment = new AuditResultAttachment();
			AuditResultAttachment docxAttachment = new AuditResultAttachment();
			AuditResultAttachment pdfAttachment = new AuditResultAttachment();
			AuditResultAttachment rtfAttachment = new AuditResultAttachment();
			AuditResultAttachment txtAttachment = new AuditResultAttachment();
			AuditResultAttachment xpsAttachment = new AuditResultAttachment();

			AuditResultAttachment jpgAttachment = new AuditResultAttachment();
			AuditResultAttachment pngAttachment = new AuditResultAttachment();
			AuditResultAttachment tiffAttachment = new AuditResultAttachment();

			AuditResultAttachment xlsAttachment = new AuditResultAttachment();
			AuditResultAttachment xlsmAttachment = new AuditResultAttachment();
			AuditResultAttachment xlsxAttachment = new AuditResultAttachment();

			docAttachment.setAuditId(audit.getAuditId());
			docAttachment.setAttachmentId(new Long(-7700));
			docAttachment.setFilename(docFile.getName());
			docAttachment.setMimeType("application/msword");
			docAttachment.setLastModified(new Date());
			docAttachment.setModifiedBy("JUNIT");

			docxAttachment.setAuditId(audit.getAuditId());
			docxAttachment.setAttachmentId(new Long(-7701));
			docxAttachment.setFilename(docxFile.getName());
			docxAttachment.setMimeType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
			docxAttachment.setLastModified(new Date());
			docxAttachment.setModifiedBy("JUNIT");

			pdfAttachment.setAuditId(audit.getAuditId());
			pdfAttachment.setAttachmentId(new Long(-7702));
			pdfAttachment.setFilename(pdfFile.getName());
			pdfAttachment.setMimeType("application/pdf");
			pdfAttachment.setLastModified(new Date());
			pdfAttachment.setModifiedBy("JUNIT");

			rtfAttachment.setAuditId(audit.getAuditId());
			rtfAttachment.setAttachmentId(new Long(-7703));
			rtfAttachment.setFilename(rtfFile.getName());
			rtfAttachment.setMimeType("application/msword");
			rtfAttachment.setLastModified(new Date());
			rtfAttachment.setModifiedBy("JUNIT");

			txtAttachment.setAuditId(audit.getAuditId());
			txtAttachment.setAttachmentId(new Long(-7704));
			txtAttachment.setFilename(txtFile.getName());
			txtAttachment.setMimeType("text/plain");
			txtAttachment.setLastModified(new Date());
			txtAttachment.setModifiedBy("JUNIT");

			xpsAttachment.setAuditId(audit.getAuditId());
			xpsAttachment.setAttachmentId(new Long(-7705));
			xpsAttachment.setFilename(xpsFile.getName());
			xpsAttachment.setMimeType("application/vnd.ms-xpsdocument");
			xpsAttachment.setLastModified(new Date());
			xpsAttachment.setModifiedBy("JUNIT");

			jpgAttachment.setAuditId(audit.getAuditId());
			jpgAttachment.setAttachmentId(new Long(-7706));
			jpgAttachment.setFilename(jpgFile.getName());
			jpgAttachment.setMimeType("image/jpeg");
			jpgAttachment.setLastModified(new Date());
			jpgAttachment.setModifiedBy("JUNIT");

			pngAttachment.setAuditId(audit.getAuditId());
			pngAttachment.setAttachmentId(new Long(-7707));
			pngAttachment.setFilename(pngFile.getName());
			pngAttachment.setMimeType("image/png");
			pngAttachment.setLastModified(new Date());
			pngAttachment.setModifiedBy("JUNIT");

			tiffAttachment.setAuditId(audit.getAuditId());
			tiffAttachment.setAttachmentId(new Long(-7708));
			tiffAttachment.setFilename(tiffFile.getName());
			tiffAttachment.setMimeType("image/tiff");
			tiffAttachment.setLastModified(new Date());
			tiffAttachment.setModifiedBy("JUNIT");

			xlsAttachment.setAuditId(audit.getAuditId());
			xlsAttachment.setAttachmentId(new Long(-7709));
			xlsAttachment.setFilename(xlsFile.getName());
			xlsAttachment.setMimeType("application/vnd.ms-excel");
			xlsAttachment.setLastModified(new Date());
			xlsAttachment.setModifiedBy("JUNIT");

			xlsmAttachment.setAuditId(audit.getAuditId());
			xlsmAttachment.setAttachmentId(new Long(-7710));
			xlsmAttachment.setFilename(xlsmFile.getName());
			xlsmAttachment.setMimeType("application/vnd.ms-excel.sheet.macroenabled.12");
			xlsmAttachment.setLastModified(new Date());
			xlsmAttachment.setModifiedBy("JUNIT");

			xlsxAttachment.setAuditId(audit.getAuditId());
			xlsxAttachment.setAttachmentId(new Long(-7711));
			xlsxAttachment.setFilename(xlsxFile.getName());
			xlsxAttachment.setMimeType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			xlsxAttachment.setLastModified(new Date());
			xlsxAttachment.setModifiedBy("JUNIT");

			// Get the file binaries
			byte[] docFileBinary = DataUtilities.convertToByteArray(new FileInputStream(docFile));
			byte[] docxFileBinary = DataUtilities.convertToByteArray(new FileInputStream(docxFile));
			byte[] pdfFileBinary = DataUtilities.convertToByteArray(new FileInputStream(pdfFile));
			byte[] rtfFileBinary = DataUtilities.convertToByteArray(new FileInputStream(rtfFile));
			byte[] txtFileBinary = DataUtilities.convertToByteArray(new FileInputStream(txtFile));
			byte[] xpsFileBinary = DataUtilities.convertToByteArray(new FileInputStream(xpsFile));

			byte[] jpgFileBinary = DataUtilities.convertToByteArray(new FileInputStream(jpgFile));
			byte[] pngFileBinary = DataUtilities.convertToByteArray(new FileInputStream(pngFile));
			byte[] tiffFileBinary = DataUtilities.convertToByteArray(new FileInputStream(tiffFile));

			byte[] xlsFileBinary = DataUtilities.convertToByteArray(new FileInputStream(xlsFile));
			byte[] xlsmFileBinary = DataUtilities.convertToByteArray(new FileInputStream(xlsmFile));
			byte[] xlsxFileBinary = DataUtilities.convertToByteArray(new FileInputStream(xlsxFile));

			// Attach the attachments to the AuditResult
			audit.getAttachments().add(docAttachment);
			audit.getAttachments().add(docxAttachment);
			audit.getAttachments().add(pdfAttachment);
			audit.getAttachments().add(rtfAttachment);
			audit.getAttachments().add(txtAttachment);
			audit.getAttachments().add(xpsAttachment);

			audit.getAttachments().add(jpgAttachment);
			audit.getAttachments().add(pngAttachment);
			audit.getAttachments().add(tiffAttachment);

			audit.getAttachments().add(xlsAttachment);
			audit.getAttachments().add(xlsmAttachment);
			audit.getAttachments().add(xlsxAttachment);

			em.getTransaction().begin();
			// Save the AuditResultAttachments to the database
			auditResultRepository.setAuditResult(audit);

			// Save the attachment file binaries to the database
			auditResultRepository.setAttachmentBinary(docAttachment.getAttachmentId(), docFileBinary);
			auditResultRepository.setAttachmentBinary(docxAttachment.getAttachmentId(), docxFileBinary);
			auditResultRepository.setAttachmentBinary(pdfAttachment.getAttachmentId(), pdfFileBinary);
			auditResultRepository.setAttachmentBinary(rtfAttachment.getAttachmentId(), rtfFileBinary);
			auditResultRepository.setAttachmentBinary(txtAttachment.getAttachmentId(), txtFileBinary);
			auditResultRepository.setAttachmentBinary(xpsAttachment.getAttachmentId(), xpsFileBinary);

			auditResultRepository.setAttachmentBinary(jpgAttachment.getAttachmentId(), jpgFileBinary);
			auditResultRepository.setAttachmentBinary(pngAttachment.getAttachmentId(), pngFileBinary);
			auditResultRepository.setAttachmentBinary(tiffAttachment.getAttachmentId(), tiffFileBinary);

			auditResultRepository.setAttachmentBinary(xlsAttachment.getAttachmentId(), xlsFileBinary);
			auditResultRepository.setAttachmentBinary(xlsmAttachment.getAttachmentId(), xlsmFileBinary);
			auditResultRepository.setAttachmentBinary(xlsxAttachment.getAttachmentId(), xlsxFileBinary);

			em.getTransaction().commit();

			// Create List of AuditResultAttachments to compare with the records coming back from the database
			List<AuditResultAttachment> attachments = new ArrayList<AuditResultAttachment>();
			attachments.add(docAttachment);
			attachments.add(docxAttachment);
			attachments.add(pdfAttachment);
			attachments.add(rtfAttachment);
			attachments.add(txtAttachment);
			attachments.add(xpsAttachment);

			attachments.add(jpgAttachment);
			attachments.add(pngAttachment);
			attachments.add(tiffAttachment);

			attachments.add(xlsAttachment);
			attachments.add(xlsmAttachment);
			attachments.add(xlsxAttachment);

			// Create list of attachment file binaries to compare with the binaries coming back from the database
			List<byte[]> binaries = new ArrayList<byte[]>();
			binaries.add(docFileBinary);
			binaries.add(docxFileBinary);
			binaries.add(pdfFileBinary);
			binaries.add(rtfFileBinary);
			binaries.add(txtFileBinary);
			binaries.add(xpsFileBinary);

			binaries.add(jpgFileBinary);
			binaries.add(pngFileBinary);
			binaries.add(tiffFileBinary);

			binaries.add(xlsFileBinary);
			binaries.add(xlsmFileBinary);
			binaries.add(xlsxFileBinary);

			// Retrieve the AuditResult with the AuditResultAttachments
			audit = auditResultRepository.getSelectedPractitionerAuditResultDetails(practitioner).get(0);

			List<byte[]> databaseBinaries = new ArrayList<byte[]>();

			// Compare the attachments against the AuditResultAttachments coming back from the database
			for (AuditResultAttachment attachment : audit.getAttachments()) {
				assertTrue(attachments.contains(attachment));
				byte[] attachmentBinary = auditResultRepository.getAttachmentBinary(attachment.getAttachmentId());
				databaseBinaries.add(attachmentBinary);
			}

			// Compare the attachment file binaries against the binaries coming back from the database
			boolean match = false;

			for (byte[] databaseBinary : databaseBinaries) {
				match = false;
				for (byte[] binary : binaries) {
					if (Arrays.equals(databaseBinary, binary)) {
						match = true;
						break;
					}
				}
				assertTrue(match);
			}

		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	/**
	 * Clones a heavy AuditResult object
	 * 
	 * Verify copying performance and integrity of the copy
	 */
	@Test
	public void cloneHeavyAuditResult() {
		try {
			File jpgFile = new File("resources/test_image1.jpg");
			Practitioner practitioner = new Practitioner();

			practitioner.setPractitionerNumber(new Long(-7777));
			practitioner.setPractitionerType("PH");

			// Retrieve the AuditResult
			AuditResult audit = auditResultRepository.getSelectedPractitionerAuditResultDetails(practitioner).get(0);

			// Create the attachment objects
			for (int i = -1; i >= -6; i--) {
				AuditResultAttachment newAttachment = new AuditResultAttachment();
				newAttachment.setAuditId(audit.getAuditId());
				newAttachment.setAttachmentId(new Long(i));
				newAttachment.setFilename(jpgFile.getName());
				newAttachment.setMimeType("image/jpeg");
				newAttachment.setLastModified(new Date());
				newAttachment.setModifiedBy("JUNIT");

				byte[] jpgFileBinary = DataUtilities.convertToByteArray(new FileInputStream(jpgFile));
				audit.getAttachments().add(newAttachment);

				em.getTransaction().begin();

				auditResultRepository.setAuditResult(audit);
				auditResultRepository.setAttachmentBinary(newAttachment.getAttachmentId(), jpgFileBinary);

				em.getTransaction().commit();
			}

			long startTime = System.currentTimeMillis();

			AuditResult auditCopy = SerializationUtils.clone(audit);

			long endTime = System.currentTimeMillis();

			long duration = endTime - startTime;

			System.out.println("Cloning took: " + duration + "ms");

			// Checks that they are not the same object
			assertTrue(auditCopy != audit);

			// Checks that simple object are not the same object
			assertTrue(auditCopy.getAuditId() != audit.getAuditId());

			// Checks that all variables have the same values
			assertTrue((auditCopy.getAuditId() == null && audit.getAuditId() == null)
					|| auditCopy.getAuditId().equals(audit.getAuditId()));
			assertTrue((auditCopy.getProviderNumber() == null && audit.getProviderNumber() == null)
					|| auditCopy.getProviderNumber().equals(audit.getProviderNumber()));
			assertTrue((auditCopy.getProviderType() == null && audit.getProviderType() == null)
					|| auditCopy.getProviderType().equals(audit.getProviderType()));
			assertTrue((auditCopy.getAuditDate() == null && audit.getAuditDate() == null)
					|| auditCopy.getAuditDate().equals(audit.getAuditDate()));
			assertTrue((auditCopy.getAuditPerson() == null && audit.getAuditPerson() == null)
					|| auditCopy.getAuditPerson().equals(audit.getAuditPerson()));
			assertTrue((auditCopy.getAuditType() == null && audit.getAuditType() == null)
					|| auditCopy.getAuditType().equals(audit.getAuditType()));
			assertTrue((auditCopy.getAuditReason() == null && audit.getAuditReason() == null)
					|| auditCopy.getAuditReason().equals(audit.getAuditReason()));
			assertTrue((auditCopy.getHealthServiceCode() == null && audit.getHealthServiceCode() == null)
					|| auditCopy.getHealthServiceCode().equals(audit.getHealthServiceCode()));
			assertTrue((auditCopy.getQualifier() == null && audit.getQualifier() == null)
					|| auditCopy.getQualifier().equals(audit.getQualifier()));
			assertTrue((auditCopy.getAuditSource() == null && audit.getAuditSource() == null)
					|| auditCopy.getAuditSource().equals(audit.getAuditSource()));
			assertTrue((auditCopy.getAuditStartDate() == null && audit.getAuditStartDate() == null)
					|| auditCopy.getAuditStartDate().equals(audit.getAuditStartDate()));
			assertTrue((auditCopy.getAuditStartDate() == null && audit.getAuditStartDate() == null)
					|| auditCopy.getAuditEndDate().equals(audit.getAuditEndDate()));
			assertTrue((auditCopy.getInterimDate() == null && audit.getInterimDate() == null)
					|| auditCopy.getInterimDate().equals(audit.getInterimDate()));
			assertTrue((auditCopy.getFinalContactDate() == null && audit.getFinalContactDate() == null)
					|| auditCopy.getFinalContactDate().equals(audit.getFinalContactDate()));
			assertTrue((auditCopy.getAuditRecoveryAmount() == null && audit.getAuditRecoveryAmount() == null)
					|| auditCopy.getAuditRecoveryAmount().equals(audit.getAuditRecoveryAmount()));
			assertTrue((auditCopy.getNumberOfServicesAudited() == null && audit.getNumberOfServicesAudited() == null)
					|| auditCopy.getNumberOfServicesAudited().equals(audit.getNumberOfServicesAudited()));
			assertTrue((auditCopy.getInappropriatlyBilledServices() == null && audit.getInappropriatlyBilledServices() == null)
					|| auditCopy.getInappropriatlyBilledServices().equals(audit.getInappropriatlyBilledServices()));
			assertTrue((auditCopy.getLastModified() == null && audit.getLastModified() == null)
					|| auditCopy.getLastModified().equals(audit.getLastModified()));
			assertTrue((auditCopy.getModifiedBy() == null && audit.getModifiedBy() == null)
					|| auditCopy.getModifiedBy().equals(audit.getModifiedBy()));
			assertTrue((auditCopy.getPharmType() == null && audit.getPharmType() == null)
					|| auditCopy.getPharmType().equals(audit.getPharmType()));
			assertTrue((auditCopy.getPharmSource() == null && audit.getPharmSource() == null)
					|| auditCopy.getPharmSource().equals(audit.getPharmSource()));
			assertTrue((auditCopy.getAuditIndicator() == null && audit.getAuditIndicator() == null)
					|| auditCopy.getAuditIndicator().equals(audit.getAuditIndicator()));

			// Checking that it is the same object instances
			for (int i = 0; i > audit.getNotes().size(); i++) {
				assertTrue(auditCopy.getNotes().get(i) == audit.getNotes().get(i));
			}
			for (int i = 0; i > audit.getAttachments().size(); i++) {
				assertTrue(auditCopy.getAttachments().get(i) == audit.getAttachments().get(i));
			}

		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
}

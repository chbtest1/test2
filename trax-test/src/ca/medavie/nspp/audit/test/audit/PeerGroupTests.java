package ca.medavie.nspp.audit.test.audit;

import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;


import org.junit.BeforeClass;
import org.junit.Test;

import ca.medavie.nspp.audit.service.data.PeerGroup;
import ca.medavie.nspp.audit.service.data.PeerGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.TownCriteria;
import ca.medavie.nspp.audit.service.data.TownCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.repository.PeerGroupRepository;
import ca.medavie.nspp.audit.service.repository.PeerGroupRepositoryImpl;
import ca.medavie.nspp.audit.test.EntityManagement;

public class PeerGroupTests {
	private static PeerGroupRepository peerGroupRepository;

	@BeforeClass
	public static void setUpClass() {
		peerGroupRepository = new PeerGroupRepositoryImpl(EntityManagement.getEntityManager());
	}

	/**
	 * Test searching for a specific PeerGroup
	 * 
	 * Verify all results match the specified PeerGroup
	 */
	@Test
	public void getSearchedPeerGroupsTest() {

		PeerGroupSearchCriteria sc = new PeerGroupSearchCriteria();
		sc.setPeerGroupId(Long.valueOf(1));

		List<PeerGroup> results = peerGroupRepository.getSearchedPeerGroups(sc, 0, 20);

		// Check that we got at least one result
		assertTrue(!results.isEmpty());

		// Check they all have the specified PeerGroup ID
		for (PeerGroup pg : results) {
			assertTrue(pg.getPeerGroupID().equals(sc.getPeerGroupId()));
		}
	}

	/**
	 * Search for Practitioner 100002.
	 * 
	 * Verify that the specified practitioner returns
	 */
	@Test
	public void getSearchedPractitionersTest() {

		PractitionerSearchCriteria sc = new PractitionerSearchCriteria();
		sc.setPractitionerNumber(Long.valueOf(100002));

		// Mock variables.. generally loaded from the active PeerGroup
		sc.setPeerGroupId(Long.valueOf(1));
		sc.setYearEndDate(new Date());

		List<Practitioner> results = peerGroupRepository.getSearchedPractitioners(sc);

		// Check that there is results
		assertTrue(!results.isEmpty());

		// Check that results have the specified Practitioner number
		for (Practitioner p : results) {
			assertTrue(p.getPractitionerNumber().equals(sc.getPractitionerNumber()));
		}
	}

	/**
	 * Loads the latest copies of available PeerGroups.
	 * 
	 * Verify they are the latest by loading all previous versions of each and compare dates.
	 */
	@Test
	public void getLatestPeerGroupsTest() {
		List<PeerGroup> results = peerGroupRepository.getLatestPeerGroups();

		// Check we got results
		assertTrue(!results.isEmpty());

		// Perform date checks
		for (PeerGroup pg : results) {
			// Create a search criteria and load all existing PG with the same ID
			PeerGroupSearchCriteria sc = new PeerGroupSearchCriteria();
			sc.setPeerGroupId(pg.getPeerGroupID());

			// Perform seach
			List<PeerGroup> relatedPgs = peerGroupRepository.getSearchedPeerGroups(sc, 0, 20);

			// Compare dates of each
			for (PeerGroup rpg : relatedPgs) {

				// Only compare if dates differ.. don't want to compare to itself
				if (!rpg.getYearEndDate().equals(pg.getYearEndDate())) {
					assertTrue(rpg.getYearEndDate().before(pg.getYearEndDate()));
				}
			}
		}
	}
	
	/**
	 * Search for a TownCriteria
	 * 
	 * Verify the specified TownCriteria returned
	 */
	@Test
	public void getTownCriteriaSearchByTownCodeName() {

		TownCriteriaSearchCriteria sc = new TownCriteriaSearchCriteria();
		
		String townCodeName = "1-BIRCH GROVE";
		sc.setTownCodeName(townCodeName);

		List<TownCriteria> results = peerGroupRepository.getTownCriteriaSearchResults(sc);

		// Verify results returned
		assertTrue(!results.isEmpty());
		// Verify the correct town returned
		for (TownCriteria tc : results) {
			assertTrue(tc.getTownCode().equals(sc.getTownCode()));
			assertTrue(tc.getTownName().equals(sc.getTownName()));
		}
	}
	
	
	/**
	 * Search for a TownCriteria
	 * 
	 * Verify the specified TownCriteria returned
	 */
	@Test
	public void getTownCriteriaSearchByCountyCodeName() {

		TownCriteriaSearchCriteria sc = new TownCriteriaSearchCriteria();
		
		String countyCodeName = "1-ANNAPOLIS";
		sc.setCountyCodeName(countyCodeName);

		List<TownCriteria> results = peerGroupRepository.getTownCriteriaSearchResults(sc);

		// Verify results returned
		assertTrue(!results.isEmpty());
		// Verify the correct town returned
		for (TownCriteria tc : results) {
			assertTrue(tc.getCountyCode().equals(sc.getCountyCode()));
			assertTrue(tc.getCountyName().equals(sc.getCountyName()));
		}
	}
	
	
	/**
	 * Search for a TownCriteria
	 * 
	 * Verify the specified TownCriteria returned
	 */
	@Test
	public void getTownCriteriaSearchByMunicipalityCodeName() {

		TownCriteriaSearchCriteria sc = new TownCriteriaSearchCriteria();
		
		String municipalityCodeName = "1-ANNAPOLIS, SUBD. D";
		sc.setMunicipalityCodeName(municipalityCodeName);

		List<TownCriteria> results = peerGroupRepository.getTownCriteriaSearchResults(sc);

		// Verify results returned
		assertTrue(!results.isEmpty());
		// Verify the correct town returned
		for (TownCriteria tc : results) {
			assertTrue(tc.getMunicipalityCode().equals(sc.getMunicipalityCode()));
			assertTrue(tc.getMunicipalityName().equals(sc.getMunicipalityName()));
		}
	}
	
	/**
	 * Search for a TownCriteria
	 * 
	 * Verify the specified TownCriteria returned
	 */
	@Test
	public void getTownCriteriaSearchByHealthRegionCodeName() {

		TownCriteriaSearchCriteria sc = new TownCriteriaSearchCriteria();
		
		String healthRegionCodeName = "1-WESTERN";
		sc.setHealthRegionCodeName(healthRegionCodeName);

		List<TownCriteria> results = peerGroupRepository.getTownCriteriaSearchResults(sc);

		// Verify results returned
		assertTrue(!results.isEmpty());
		// Verify the correct town returned
		for (TownCriteria tc : results) {
			assertTrue(tc.getHealthRegionCode().equals(sc.getHealthRegionCode()));
			assertTrue(tc.getHealthRegionDesc().equals(sc.getHealthRegionDesc()));
		}
	}
	
}
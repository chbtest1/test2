package ca.medavie.nspp.audit.test.audit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import ca.medavie.nspp.audit.service.data.AuditCriteria;
import ca.medavie.nspp.audit.service.data.AuditCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.BusinessArrangement;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.IndividualExclusion;
import ca.medavie.nspp.audit.service.data.MedicareAuditLetterResponse;
import ca.medavie.nspp.audit.service.data.MedicareAuditLetterResponseSearchCriteria;
import ca.medavie.nspp.audit.service.repository.MedicareRepository;
import ca.medavie.nspp.audit.service.repository.MedicareRepositoryImpl;
import ca.medavie.nspp.audit.test.BaseTest;

/**
 * Places the MedicareRepository under test
 */
public class MedicareTests extends BaseTest {
	private static MedicareRepository medicareRepository;

	@BeforeClass
	public static void setUpClass() {
		medicareRepository = new MedicareRepositoryImpl(em);
	}

	/**
	 * Search for the specified Health Service Code.
	 * 
	 * Verify the correct Health Service Codes return
	 */
	@Test
	public void getSearchedHealthServiceCriteriaDescriptionsTest() {
		HealthServiceCriteriaSearchCriteria sc = new HealthServiceCriteriaSearchCriteria();
		sc.setHealthServiceCode("80.19");

		List<HealthServiceCriteria> results = medicareRepository.getSearchedHealthServiceCriteriaDescriptions(sc);
		// Verify we get results
		assertNotNull(results);
		assertFalse(results.isEmpty());
		// Verify the results
		for (HealthServiceCriteria hsc : results) {
			assertTrue(hsc.getHealthServiceCode().equals(sc.getHealthServiceCode()));
		}
	}

	/**
	 * Save a new Health Service Criteria Description
	 * 
	 * Verify the save by loading the new record
	 */
	@Test
	public void saveHealthServiceCriteriaDescriptionsTest() {
		List<HealthServiceCriteria> hscs = new ArrayList<HealthServiceCriteria>();

		HealthServiceCriteria hsc1 = new HealthServiceCriteria();
		hsc1.setHealthServiceID("-99999");
		hsc1.setHealthServiceAuditDesc("Test description 1");
		hsc1.setModifiedBy("JUNIT");
		hsc1.setLastModified(Calendar.getInstance().getTime());

		HealthServiceCriteria hsc2 = new HealthServiceCriteria();
		hsc2.setHealthServiceID("-99995");
		hsc2.setHealthServiceAuditDesc("Test description 2");
		hsc2.setModifiedBy("JUNIT");
		hsc2.setLastModified(Calendar.getInstance().getTime());

		hscs.add(hsc1);
		hscs.add(hsc2);

		// Save the 2 descriptions
		em.getTransaction().begin();
		medicareRepository.saveHealthServiceCriteriaDescriptions(hscs);
		em.getTransaction().commit();

		// Unsure how to validate.. currently test passes if no error on save.. TODO
	}

	/**
	 * Load the current exclusions.
	 * 
	 * Verify we get results
	 */
	@Test
	public void getHealthServiceCriteriaExclusionsTest() {
		List<HealthServiceCriteria> results = medicareRepository.getHealthServiceCriteriaExclusions();
		// Verify we get results
		assertNotNull(results);
		assertFalse(results.isEmpty());
	}

	/**
	 * Saves a new exclusion
	 * 
	 * Verify save action by loading the new record
	 */
	@Test
	public void saveHealthServiceCriteriaExclusionsTest() {
		List<HealthServiceCriteria> hscs = new ArrayList<HealthServiceCriteria>();

		HealthServiceCriteria hsc = new HealthServiceCriteria();
		hsc.setHealthServiceID("31804");
		hsc.setModifiedBy("JUNIT");
		hsc.setLastModified(Calendar.getInstance().getTime());

		// Add to list
		hscs.add(hsc);

		// Save the exclusion
		em.getTransaction().begin();
		medicareRepository.saveHealthServiceCriteriaExclusions(hscs);
		em.getTransaction().commit();

		// Verify save by loading newly created record
		List<HealthServiceCriteria> currentExclusions = medicareRepository.getHealthServiceCriteriaExclusions();

		boolean newRecordFound = false;

		for (HealthServiceCriteria hsx : currentExclusions) {
			if (hsx.getHealthServiceID().trim().equals("31804")) {
				newRecordFound = true;
			}
		}

		// Test passes if we found the record
		assertTrue(newRecordFound);
	}

	/**
	 * Delete the specified Exclusion
	 * 
	 * Verify the delete by attempting to load the deleted record. Nothing should return
	 */
	@Test
	public void deleteHealthServiceCriteriaExclusionsTest() {
		// Verify the record we are deleting exists in the DB
		List<HealthServiceCriteria> currentExclusions = medicareRepository.getHealthServiceCriteriaExclusions();

		String targetId = "87308";
		boolean targetRecordFound = false;
		for (HealthServiceCriteria hsx : currentExclusions) {
			if (hsx.getHealthServiceID().trim().equals(targetId)) {
				targetRecordFound = true;
			}
		}

		// continue if the record has been found
		assertTrue(targetRecordFound);

		// Perform the delete
		List<HealthServiceCriteria> exclForDelete = new ArrayList<HealthServiceCriteria>();
		HealthServiceCriteria hsc = new HealthServiceCriteria();
		hsc.setHealthServiceID("87308");

		exclForDelete.add(hsc);
		em.getTransaction().begin();
		medicareRepository.deleteHealthServiceCriteriaExclusions(exclForDelete);
		em.getTransaction().commit();

		// Verify delete
		currentExclusions = medicareRepository.getHealthServiceCriteriaExclusions();
		boolean recordDeleted = true;
		for (HealthServiceCriteria hsx : currentExclusions) {
			if (hsx.getHealthServiceID().trim().equals("87308")) {
				recordDeleted = false;
			}
		}

		// Test passes if record is deleted
		assertTrue(recordDeleted);
	}

	/**
	 * Load the current Individual exclusions.
	 * 
	 * Verify we get results
	 */
	@Test
	public void getIndividualExclusionsTest() {
		List<IndividualExclusion> results = medicareRepository.getIndividualExclusions();
		// Verify we get results
		assertNotNull(results);
		assertFalse(results.isEmpty());
	}

	/**
	 * Save a new Individual Exclusion.
	 * 
	 * Verify save by loading the new record
	 */
	@Test
	public void saveIndividualExclusionsTest() {
		List<IndividualExclusion> exclusionsForSave = new ArrayList<IndividualExclusion>();

		IndividualExclusion ie = new IndividualExclusion();
		ie.setHealthCardNumber("0004338059");
		ie.setEffectiveFrom(Calendar.getInstance().getTime());
		ie.setModifiedBy("JUNIT");
		ie.setLastModified(Calendar.getInstance().getTime());
		exclusionsForSave.add(ie);

		// Save the exclusion
		em.getTransaction().begin();
		medicareRepository.saveIndividualExclusions(exclusionsForSave);
		em.getTransaction().commit();

		// Verify save
		List<IndividualExclusion> currentExclusions = medicareRepository.getIndividualExclusions();

		boolean newRecordFound = false;
		for (IndividualExclusion excl : currentExclusions) {
			if (excl.getHealthCardNumber().trim().equals("0004338059")) {
				newRecordFound = true;
			}
		}

		// Test pass if we find new record
		assertTrue(newRecordFound);
	}

	/**
	 * Search for a health card owners name by the specified health card number
	 * 
	 * Verify we get the correct name returned
	 */
	@Test
	public void getHealthCardNumberOwnerNameTest() {
		String result = medicareRepository.getHealthCardNumberOwnerName("9900007254");
		assertNotNull(result);
		assertTrue(result.equals("ITHRTSAGH, STUOHQKR"));
	}

	/**
	 * Save a new Audit Criteria
	 * 
	 * Verify save by loading new Audit Criteria record
	 */
	@Test
	public void saveMedicareAuditCriteriaTest() {
		// TODO not sure how we are going to implement saving tests (Stub?)
	}

	/**
	 * Search for the specified Practitioner name
	 * 
	 * Verify we get the correct name returned
	 */
	@Test
	public void getPractitionerNameTest() {
		String result = medicareRepository.getPractitionerName(Long.valueOf(102276), "PH");
		// Verify the name
		assertNotNull(result);
		assertTrue(result.equals("VALERIE ROSS ANNE"));
	}

	/**
	 * Search for the specified Audit Criteria
	 * 
	 * Verify the Audit Criteria returned is correct
	 */
	@Test
	public void getMedicarePractitionerAuditCriteriaTest() {
		Long practNum = new Long(106333);
		String subCat = "PH";
		String auditType = "SPEVI";

		// Load audit
		AuditCriteria audit = medicareRepository.getMedicarePractitionerAuditCriteria(practNum, subCat, auditType);
		// Verify we get a result
		assertNotNull(audit);
		// Verify it is the correct Audit
		assertTrue(audit.getProviderNumber().equals(practNum));
		assertTrue(audit.getProviderType().equals(subCat));
		assertTrue(audit.getAuditType().equals(auditType));
		assertTrue(audit.getDefaultHealthServiceDesc().equals("VISIT"));
	}

	/**
	 * Perform a Practitioner Audit criteria search
	 * 
	 * Verify we get the correct results
	 */
	@Test
	public void getSearchPractitionerAuditCriteriasTest() {
		AuditCriteriaSearchCriteria sc = new AuditCriteriaSearchCriteria();
		sc.setAuditType("SPEVI");
		sc.setAuditCriteriaId(Long.valueOf(106));
		sc.setPractitionerId(Long.valueOf(102208));

		// Load search results
		List<AuditCriteria> results = medicareRepository.getSearchPractitionerAuditCriterias(sc, 0, 20);
		// Verify we get results
		assertNotNull(results);
		assertFalse(results.isEmpty());
		// Validate results
		for (AuditCriteria ac : results) {
			assertTrue(ac.getAuditType().equals(sc.getAuditType()));
			assertTrue(ac.getAuditCriteriaId().equals(sc.getAuditCriteriaId()));
			assertTrue(ac.getProviderNumber().equals(sc.getPractitionerId()));
		}
	}

	/**
	 * Load an Audit Criteria by ID
	 * 
	 * Verify the correct Audit Criteria returned
	 */
	@Test
	public void getPractitionerAuditCriteriaByIdTest() {
		AuditCriteria result = medicareRepository.getPractitionerAuditCriteriaById(Long.valueOf(100));
		// Verify we get results
		assertNotNull(result);
		// Verify result
		assertTrue(result.getAuditCriteriaId().equals(Long.valueOf(100)));
		assertTrue(result.getAuditType().equals("SPEVI"));
		assertTrue(result.getProviderNumber().equals(Long.valueOf(104756)));
		assertTrue(result.getProviderType().equals("PH"));
	}

	/**
	 * Load a Default Audit Criteria by type
	 * 
	 * Verify we get the correct Audit Criteria returned
	 */
	@Test
	public void getDefaultAuditCriteriaByTypeTest() {
		AuditCriteria result = medicareRepository.getDefaultAuditCriteriaByType("MSIVI");
		// Verify we get a result
		assertNotNull(result);
		// Verify result
		assertTrue(result.getAuditCriteriaId().equals(Long.valueOf(226)));
		assertTrue(result.getAuditType().equals("MSIVI"));
		assertTrue(result.getDefaultHealthServiceDesc().equals("PROCEDURE"));
	}

	/**
	 * Search for Audit Letter Responses
	 * 
	 * Verify results are the correct ones
	 */
	@Test
	public void getSearchedMedicareALRTest() {
		MedicareAuditLetterResponseSearchCriteria sc = new MedicareAuditLetterResponseSearchCriteria();
		sc.setAuditRunNumber(Long.valueOf(998));
		sc.setPractitionerNumber(Long.valueOf(101816));
		sc.setPractitionerType("PH");

		List<MedicareAuditLetterResponse> results = medicareRepository.getSearchedMedicareALR(sc, 0, 20);
		// Verify we get results and only 1
		assertNotNull(results);
		assertTrue(results.size() == 1);
		// Verify results
		for (MedicareAuditLetterResponse res : results) {
			assertTrue(res.getSe_number().equals("FID201500550871"));
			assertTrue(res.getAudit_run_number().equals(sc.getAuditRunNumber()));
			assertTrue(Long.valueOf(res.getProvider_number()).equals(sc.getPractitionerNumber()));
			assertTrue(res.getProvider_type().equals(sc.getPractitionerType()));
			assertTrue(res.getHealth_card_number().trim().equals("0000463653"));
		}
	}

	/**
	 * Save a new Audit Letter Response
	 * 
	 * Verify save by loading new ALR record
	 */
	@Test
	public void saveMedicareALRTest() {
		// TODO not sure how we are going to implement saving tests (Stub?)
	}

	/**
	 * Get the letter count for the specified Run number and type
	 * 
	 * Verify we get the correct count back
	 */
	@Test
	public void getNumberOfLettersCreatedTest() {
		Long result = medicareRepository.getNumberOfLettersCreated(Long.valueOf(418), "MSIVI");
		// Verify we get a result
		assertNotNull(result);
		assertTrue(result.equals(Long.valueOf(3186)));
	}

	/**
	 * Test loading the specified Business Arrangement
	 * 
	 * Verify we get the correct data
	 */
	@Test
	public void getBusinessArrangementTest() {
		// TODO
	}

	/**
	 * Load all Business Arrangement Exclusions
	 * 
	 * Verify we get data back
	 */
	@Test
	public void getBusinessArrangementExclusionsTest() {
		List<BusinessArrangement> exclusions = medicareRepository.getBusinessArrangementExclusions();
		// Verify we get results
		assertNotNull(exclusions);
		assertFalse(exclusions.isEmpty());
	}

	/**
	 * Save a new Business Arrangement exclusion.
	 * 
	 * Verify save by loading new record
	 */
	public void saveBusinessArrangementExclusionTest() {
		// TODO
	}

	/**
	 * Delete the specified Exclusion
	 * 
	 * Verify the delete by attempting to load the deleted record. Nothing should return
	 */
	public void deleteBusinessArrangementExclusionsTest() {
		// TODO
	}
}
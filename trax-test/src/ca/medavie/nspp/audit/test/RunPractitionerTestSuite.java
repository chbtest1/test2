package ca.medavie.nspp.audit.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import ca.medavie.nspp.audit.test.practitioner.PractitionerDHATests;
import ca.medavie.nspp.audit.test.practitioner.PractitionerLocumsTests;

/**
 * Collection of all Junits for testing the Practitioner portion of TRAX. Simply right click and run as junit
 */
@RunWith(Suite.class)
@SuiteClasses({ PractitionerDHATests.class, PractitionerLocumsTests.class })
public class RunPractitionerTestSuite {
}

package ca.medavie.nspp.audit.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import ca.medavie.nspp.audit.test.accounting.ManualDepositTests;
import ca.medavie.nspp.audit.test.audit.AuditResultTests;
import ca.medavie.nspp.audit.test.audit.DenticareTests;
import ca.medavie.nspp.audit.test.audit.HealthServiceGroupsTests;
import ca.medavie.nspp.audit.test.audit.MedicareTests;
import ca.medavie.nspp.audit.test.audit.OutlierCriteriaTests;
import ca.medavie.nspp.audit.test.audit.PeerGroupTests;
import ca.medavie.nspp.audit.test.audit.PharmacareTests;
import ca.medavie.nspp.audit.test.audit.ProfileInquiryTests;
import ca.medavie.nspp.audit.test.dss.DssChequeReconciliationTests;
import ca.medavie.nspp.audit.test.dss.DssClaimsHistoryTransferTests;
import ca.medavie.nspp.audit.test.dss.DssConstantsTests;
import ca.medavie.nspp.audit.test.dss.DssGLNumbersTests;
import ca.medavie.nspp.audit.test.dss.DssUnitValueTests;
import ca.medavie.nspp.audit.test.practitioner.PractitionerDHATests;
import ca.medavie.nspp.audit.test.practitioner.PractitionerLocumsTests;

/**
 * Collection of all Junits in the test suite. Simply right click and run as junit
 */
@RunWith(Suite.class)
@SuiteClasses({ AuditResultTests.class, DenticareTests.class, HealthServiceGroupsTests.class, MedicareTests.class,
		OutlierCriteriaTests.class, PeerGroupTests.class, PharmacareTests.class, ProfileInquiryTests.class,
		ManualDepositTests.class, DssChequeReconciliationTests.class, DssClaimsHistoryTransferTests.class,
		DssConstantsTests.class, DssGLNumbersTests.class, DssUnitValueTests.class, PractitionerDHATests.class,
		PractitionerLocumsTests.class })
public class RunEntireTestSuite {
}

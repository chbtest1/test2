package ca.medavie.nspp.audit.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import ca.medavie.nspp.audit.test.dss.DssChequeReconciliationTests;
import ca.medavie.nspp.audit.test.dss.DssClaimsHistoryTransferTests;
import ca.medavie.nspp.audit.test.dss.DssConstantsTests;
import ca.medavie.nspp.audit.test.dss.DssGLNumbersTests;
import ca.medavie.nspp.audit.test.dss.DssUnitValueTests;

/**
 * Collection of all Junits for testing the DSS portion of TRAX. Simply right click and run as junit
 */
@RunWith(Suite.class)
@SuiteClasses({ DssChequeReconciliationTests.class, DssClaimsHistoryTransferTests.class, DssConstantsTests.class,
		DssGLNumbersTests.class, DssUnitValueTests.class })
public class RunDSSTestSuite {
}

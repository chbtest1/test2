package ca.medavie.nspp.audit.test;

import java.io.InputStream;
import java.util.Properties;

/**
 * A class to retrieve and hold the properties found in the environment.properties file.
 * 
 */
public class EnvironmentProperties {
	private static EnvironmentProperties instance;

	private static final String PROPERTIES_FILE_NAME = "environment.properties";
	private static final String ORACLE_USERNAME = "oracle_username";
	private static final String ORACLE_PASSWORD = "oracle_password";
	private static final String DOWNLOAD_LOCATION_KEY = "download_location";
	private static final String DATASET_LOCATION = "dataset_location";
	private static final String COMPOSITE_TABLE_LOCATION = "composite_table_location";
	private static final String CLEANUP_DATA_LOCATION = "cleanup_data_location";

	/** DB config information */
	private static final String DB_USER = "db_user";
	private static final String DB_PASSWORD = "db_password";
	private static final String DB_URL = "db_url";
	private static final String DB_SCHEMA = "db_schema";

	private Properties props;

	private EnvironmentProperties() {
		props = new Properties();
		try {
			InputStream inStream = this.getClass().getResourceAsStream(PROPERTIES_FILE_NAME);

			props.load(inStream);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @return The EnvironmentProperties instance
	 */
	public static EnvironmentProperties getInstance() {
		if (instance == null) {
			instance = new EnvironmentProperties();
		}
		return instance;
	}

	/**
	 * Return the oracle login username
	 */
	public String getOracleUsername() {
		return props.getProperty(ORACLE_USERNAME);
	}

	/**
	 * Return the oracle login password
	 */
	public String getOraclePassword() {
		return props.getProperty(ORACLE_PASSWORD);
	}

	/**
	 * @return The database Username
	 */
	public String getDBUser() {
		return props.getProperty(DB_USER);
	}

	/**
	 * 
	 * @return The database password
	 */
	public String getDBPassword() {
		return props.getProperty(DB_PASSWORD);
	}

	/**
	 * @return The database URL to connect to
	 */
	public String getDBUrl() {
		return props.getProperty(DB_URL);
	}

	/**
	 * @return The schema name in the database to use
	 */
	public String getDBSchema() {
		return props.getProperty(DB_SCHEMA);
	}

	/**
	 * @return The location for WebDriver to save files
	 */
	public String getDownloadLocation() {
		return props.getProperty(DOWNLOAD_LOCATION_KEY);
	}

	/**
	 * @return The file path for the xml file containing our test data set
	 */
	public String getDatasetLocation() {
		return props.getProperty(DATASET_LOCATION);
	}

	/**
	 * @return The file path for the xml file containing our composite key information
	 */
	public String getCompositeTableLocation() {
		return props.getProperty(COMPOSITE_TABLE_LOCATION);
	}

	/**
	 * @return The file path for the xml file containing our queries for data cleanup
	 */
	public String getCleanupDataLocation() {
		return props.getProperty(CLEANUP_DATA_LOCATION);
	}
}

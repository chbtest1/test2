package ca.medavie.nspp.audit.test.accounting;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import ca.medavie.nspp.audit.service.data.BusinessArrangementCode;
import ca.medavie.nspp.audit.service.data.ManualDeposit;
import ca.medavie.nspp.audit.service.data.ManualDepositSearchCriteria;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.ProviderGroup;
import ca.medavie.nspp.audit.service.data.ProviderGroupSearchCriteria;
import ca.medavie.nspp.audit.service.repository.ManualDepositsRepository;
import ca.medavie.nspp.audit.service.repository.ManualDepositsRepositoryImpl;
import ca.medavie.nspp.audit.test.EntityManagement;

/**
 * Places the ManualDepositsRepository under test
 */
public class ManualDepositTests {
	private static ManualDepositsRepository manualDepositsRepository;

	@BeforeClass
	public static void setUpClass() {
		manualDepositsRepository = new ManualDepositsRepositoryImpl(EntityManagement.getEntityManager());
	}

	/**
	 * Load Business Arrangements in 2 ways. 1) Load by Practitioner 2) Load by Group
	 * 
	 * Verify that both loads returned the expected data
	 */
	@Test
	public void getBusinessArrangementsTest() {

		// Load 1
		Practitioner p = new Practitioner();
		p.setPractitionerNumber(Long.valueOf(100806));
		p.setPractitionerType("PH");

		List<BusinessArrangementCode> load1Results = manualDepositsRepository.getBusinessArrangements(p, null);
		// Verify we get results
		assertNotNull(load1Results);
		assertFalse(load1Results.isEmpty());

		// Load 2
		ProviderGroup pg = new ProviderGroup();
		pg.setProviderGroupId(Long.valueOf(1182));

		List<BusinessArrangementCode> load2Results = manualDepositsRepository.getBusinessArrangements(null, pg);
		// Verify we get results
		assertNotNull(load2Results);
		assertFalse(load2Results.isEmpty());
	}

	/**
	 * Load a Business Arrangement by the specified Manual Deposit 2 ways
	 * 
	 * 1) Manual Deposit with Practitioner details 2) Manual Deposit with Group details
	 * 
	 * Verify we get the expected data
	 */
	@Test
	public void getBusinessArrangementsByManualDepositTest() {
		// Load 1
		ManualDeposit md1 = new ManualDeposit();
		md1.setPractitionerNumber(Long.valueOf(100806));
		md1.setPractitionerType("PH");

		List<BusinessArrangementCode> load1Results = manualDepositsRepository.getBusinessArrangements(md1);
		// Verify we get results
		assertNotNull(load1Results);
		assertFalse(load1Results.isEmpty());

		// Load 2
		ManualDeposit md2 = new ManualDeposit();
		md2.setGroupNumber(Long.valueOf(1182));

		List<BusinessArrangementCode> load2Results = manualDepositsRepository.getBusinessArrangements(md2);
		// Verify we get results
		assertNotNull(load2Results);
		assertFalse(load2Results.isEmpty());
	}

	/**
	 * Perform a Manual Deposits search
	 * 
	 * Verify data returned
	 */
	@Test
	public void getSearchedManualDepositsTest() {
		ManualDepositSearchCriteria sc = new ManualDepositSearchCriteria();
		sc.setPractitionerNumber("102220");

		List<ManualDeposit> results = manualDepositsRepository.getSearchedManualDeposits(sc, true, 0, 20);
		// Verify we get results
		assertNotNull(results);
		assertFalse(results.isEmpty());
		for (ManualDeposit md : results) {
			assertTrue(md.getPractitionerNumber().equals(Long.valueOf(sc.getPractitionerNumber())));
			assertTrue(md.getBusArrNumber().equals(Integer.valueOf(3154)));
		}
	}

	/**
	 * Load the details of the specified ManualDeposit
	 * 
	 * Verify the correct details loaded
	 */
	@Test
	public void getManualDepositDetailsTest() {
		ManualDeposit md = new ManualDeposit();
		md.setDepositNumber(Long.valueOf(897));

		md = manualDepositsRepository.getManualDepositDetails(md);
		// Verify the details
		assertNotNull(md);
		assertTrue(md.getDepositNumber().equals(Long.valueOf(897)));
		assertTrue(md.getBusArrNumber().equals(Integer.valueOf(3154)));
		assertTrue(md.getGlNumber().equals(Integer.valueOf(825)));
		assertTrue(md.getDepositNote().trim().equals("PER DONNA POTTERS AUDIT"));
	}

	/**
	 * Perform both an update of an existing Deposit and save a new Deposit
	 * 
	 * Verify both actions by loading the new/updated records
	 */
	@Test
	public void saveOrUpdateManualDepositTest() {
		// TODO not sure how we are going to implement saving tests (Stub?)
	}

	/**
	 * Delete the specified Manual Deposit
	 * 
	 * Verify delete by attempting to load the deleted record
	 */
	@Test
	public void deleteManualDepositTest() {
		// TODO not sure how we are going to implement deleting tests (Stub?)
	}

	/**
	 * Provide a Manual Deposit with Practitioner details for validation
	 * 
	 * Verify TRUE is returned from validation
	 */
	@Test
	public void verifyPractitionerTest() {
		ManualDeposit md = new ManualDeposit();
		md.setPractitionerNumber(Long.valueOf(102262));
		md.setPractitionerType("PH");

		// Verify result
		assertTrue(manualDepositsRepository.verifyPractitioner(md));
	}

	/**
	 * Perform 2 Practitioner searches
	 * 
	 * 1) Search with a specialty specified. 2) Search without a specialty specified
	 * 
	 * Verify results returned by both searches
	 */
	@Test
	public void getManualDepositSearchedPractitionersTest() {

		// Search 1
		PractitionerSearchCriteria sc1 = new PractitionerSearchCriteria();
		sc1.setSpecialty("OPHT");
		sc1.setPractitionerNumber(Long.valueOf(100523));

		List<Practitioner> results1 = manualDepositsRepository.getManualDepositSearchedPractitioners(sc1, 0, 20);
		// Verify we get results
		assertNotNull(results1);
		assertFalse(results1.isEmpty());
		// Verify results
		for (Practitioner p : results1) {
			assertTrue(p.getPractitionerNumber().equals(sc1.getPractitionerNumber()));
			assertTrue(p.getPractitionerType().equals("PH"));
			assertTrue(p.getCity().equals("SYDNEY") || p.getCity().equals("MIRA ROAD") || p.getCity().equals("HALIFAX")
					|| p.getCity().equals("DARTMOUTH"));
		}

		// Search 2
		PractitionerSearchCriteria sc2 = new PractitionerSearchCriteria();
		sc2.setPractitionerNumber(Long.valueOf(102390));

		List<Practitioner> results2 = manualDepositsRepository.getManualDepositSearchedPractitioners(sc2, 0, 20);
		// Verify we get results
		assertNotNull(results2);
		assertFalse(results2.isEmpty());
		// Verify results
		for (Practitioner p : results2) {
			assertTrue(p.getPractitionerNumber().equals(sc2.getPractitionerNumber()));
			assertTrue(p.getPractitionerType().equals("PH"));
			assertTrue(p.getCity().equals("HALIFAX"));
		}
	}

	/**
	 * Perform a search of ProviderGroups
	 * 
	 * Verify data returned
	 */
	@Test
	public void getSearchedProviderGroupsTest() {
		ProviderGroupSearchCriteria sc = new ProviderGroupSearchCriteria();
		sc.setProviderGroupId("1133");

		List<ProviderGroup> results = manualDepositsRepository.getSearchedProviderGroups(sc, 0, 20);
		// Verify we get results
		assertNotNull(results);
		assertFalse(results.isEmpty());
		// Verify data
		for (ProviderGroup pg : results) {
			assertTrue(pg.getProviderGroupId() == Long.valueOf(1133));
		}
	}
}

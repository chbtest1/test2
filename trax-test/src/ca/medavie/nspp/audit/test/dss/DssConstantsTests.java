package ca.medavie.nspp.audit.test.dss;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import ca.medavie.nspp.audit.service.data.DssConstantBO;
import ca.medavie.nspp.audit.service.repository.DssConstantsRepository;
import ca.medavie.nspp.audit.service.repository.DssConstantsRepositoryImpl;
import ca.medavie.nspp.audit.test.EntityManagement;

/**
 * Places the DssConstantsRepository under test
 */
public class DssConstantsTests {
	private static DssConstantsRepository constantsRepository;

	@BeforeClass
	public static void setUpClass() {
		constantsRepository = new DssConstantsRepositoryImpl(EntityManagement.getEntityManager());
	}

	/**
	 * Load all available Dss Constants
	 * 
	 * Verify the data returned
	 */
	@Test
	public void getDssConstantsTest() {
		List<DssConstantBO> results = constantsRepository.getDssConstants();
		// Verify we get results
		assertNotNull(results);
		assertFalse(results.isEmpty());
	}

	/**
	 * Save changes to DSS constant.
	 * 
	 * Verify save by loading record and checking for changes
	 */
	@Test
	public void updateDssConstantTest() {
		// TODO not sure how we are going to implement saving tests (Stub?)
	}
}
